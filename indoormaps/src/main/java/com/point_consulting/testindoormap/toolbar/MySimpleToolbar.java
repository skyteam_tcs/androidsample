package com.point_consulting.testindoormap.toolbar;

import android.content.Context;
import android.util.AttributeSet;

import com.point_consulting.testindoormap.R;

public class MySimpleToolbar extends MyBaseToolbar {

    public MySimpleToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(context.getResources().getColor(android.R.color.white));
        setNavigationIcon(R.drawable.ic_action_search);
    }
}
