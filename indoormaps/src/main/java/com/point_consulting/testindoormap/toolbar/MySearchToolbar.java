package com.point_consulting.testindoormap.toolbar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.point_consulting.testindoormap.R;

public class MySearchToolbar extends MyBaseToolbar {

    private EditText m_editText;

    public MySearchToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(context.getResources().getColor(android.R.color.white));
        setNavigationIcon(R.drawable.ic_action_back);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inflate(getContext(), R.layout.merge_search, this);
        m_editText = findViewById(R.id.toolbar_search_edittext);
    }

    @Override
    public void showContent() {
        super.showContent();
        m_editText.requestFocus();
    }

    public void clearText() {
        m_editText.setText(null);
    }

    public EditText getEditText() {return m_editText;}
}
