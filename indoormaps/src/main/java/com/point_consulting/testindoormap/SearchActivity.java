package com.point_consulting.testindoormap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.point_consulting.pc_indoormapoverlaylib.Coordinate3D;
import com.point_consulting.pc_indoormapoverlaylib.Manager;
import com.point_consulting.testindoormap.toolbar.MySearchToolbar;
import com.point_consulting.testindoormap.transition.FadeInTransition;
import com.point_consulting.testindoormap.transition.FadeOutTransition;
import com.point_consulting.testindoormap.transition.SimpleTransitionListener;

import java.util.Map;

public class SearchActivity extends AppCompatActivity {

    private class S1TextWatcher extends MySearch.STextWatcher {
        S1TextWatcher(MyApplication app) {
            super(app, m_searchbar.getEditText(), m_search);
        }

        @Override
        Coordinate3D mtw_c3dForSearch() {
            return null;
        }
        @Override
        void mtw_onTextChanged(EditText editText) {
        }
    }

    private class SSearch extends MySearch {
        SSearch(Activity activity, ListView table) {
            super(activity, table);
        }
        @Override
        void ms_onLocation(Manager.Location location, Map<String, Object> m) {
            final EditText editText = m_searchbar.getEditText();
            editText.removeTextChangedListener(m_textWatcher);
            editText.setText((String)m.get(MySearch.s_titleStr));

            final Intent intent = new Intent();
            intent.putExtra(MyAppUtils.s_extra_location, location);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private MySearch m_search;
    private MySearchToolbar m_searchbar;
    private S1TextWatcher m_textWatcher;
    //Added for the header navigation
    private TextView selectedAirportText;
    private TextView backNavigationText;
    private String selectedAirport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        selectedAirportText = findViewById(R.id.selectedAirportText);
        backNavigationText = findViewById(R.id.backtext);
        m_searchbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(m_searchbar);
        if (getIntent() != null && getIntent().getExtras() != null) {
            selectedAirport = getIntent().getStringExtra("airportName");
            selectedAirportText.setText(selectedAirport);
        }

        backNavigationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // make sure to check if this is the first time running the activity
        // we don't want to play the enter animation on configuration changes (i.e. orientation)
        if (isFirstTimeRunning(savedInstanceState)) {
            // Start with an empty looking Toolbar
            // We are going to fade its contents in, as long as the activity finishes rendering
            m_searchbar.hideContent();

            ViewTreeObserver viewTreeObserver = m_searchbar.getViewTreeObserver();
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    m_searchbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    // after the activity has finished drawing the initial layout, we are going to continue the animation
                    // that we left off from the MainActivity
                    showSearch();
                }

                private void showSearch() {
                    // use the TransitionManager to animate the changes of the Toolbar
                    TransitionManager.beginDelayedTransition(m_searchbar, FadeInTransition.createTransition());
                    // here we are just changing all children to VISIBLE
                    m_searchbar.showContent();
                }
            });
        }

        ListView table = findViewById(R.id.search_results_table);
        m_search = new SSearch(this, table);

        final MyApplication app = (MyApplication)getApplication();
        m_textWatcher = new S1TextWatcher(app);

        m_search.update(true);
    }

    private boolean isFirstTimeRunning(Bundle savedInstanceState) {
        return savedInstanceState == null;
    }

    @Override
    public void finish() {
        // when the user tries to finish the activity we have to animate the exit
        // let's start by hiding the keyboard so that the exit seems smooth
        MyAppUtils.HideKeyboard(this);

        // at the same time, start the exit transition
        exitTransitionWithAction(new Runnable() {
            @Override
            public void run() {
                // which finishes the activity (for real) when done
                SearchActivity.super.finish();

                // override the system pending transition as we are handling ourselves
                overridePendingTransition(0, 0);
            }
        });
    }

    private void exitTransitionWithAction(final Runnable endingAction) {

        Transition transition = FadeOutTransition.withAction(new SimpleTransitionListener() {
            @Override
            public void onTransitionEnd(Transition transition) {
                endingAction.run();
            }
        });

        TransitionManager.beginDelayedTransition(m_searchbar, transition);
        m_searchbar.hideContent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.action_clear) {
            m_searchbar.clearText();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
