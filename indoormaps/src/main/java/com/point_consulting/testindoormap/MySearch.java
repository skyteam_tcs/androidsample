package com.point_consulting.testindoormap;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.point_consulting.pc_indoormapoverlaylib.Manager;
import com.point_consulting.pc_indoormapoverlaylib.Mathe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class MySearch {

    private Activity m_activity;
    static final String s_titleStr = "title";
    private static final String s_subtitleStr = "subtitle";
    private static final String s_icon = "icon";
    private List<Map<String, Object> > m_list = new ArrayList<>();
    private SimpleAdapter m_adapter;

    static class GroupEntry_ {
        GroupEntry_(String categories[], String name, int iconId) {
            m_categories = categories;
            m_name = name;
            m_iconId = iconId;
        }
        String m_categories[];
        String m_name;
        int m_iconId;
    }

    static final GroupEntry_[] s_groups = new GroupEntry_[] {
            new GroupEntry_(new String[]{"SkyTeam Lounge"}, "Lounges", R.drawable.u_icon_small_lounge),
            new GroupEntry_(new String[]{"Transfer"}, "Transfer Desks", R.drawable.u_icon_small_transfer_desk),
            new GroupEntry_(new String[]{"Check-in"}, "Check-in Desks", R.drawable.u_icon_small_checkin),
            new GroupEntry_(new String[]{"Gate Area"}, "Gates", R.drawable.u_icon_small_gate),
            new GroupEntry_(new String[]{"Baggage Carousel"}, "Baggage Claim", R.drawable.u_icon_small_baggage),
            new GroupEntry_(new String[]{"Security Checkpoint", "Security"}, "Security", R.drawable.u_icon_small_security),
            new GroupEntry_(new String[]{"Cafeteria", "Specialty Food", "Restaurants", "Eating/Drinking"}, "Food", R.drawable.u_icon_small_restaurant),
            new GroupEntry_(new String[]{"Cafes", "Coffee & Tea", "Beer, Wine & Spirits"}, "Drink", R.drawable.u_icon_small_coffee),
            new GroupEntry_(new String[]{"Accessories", "Appliances", "Art Galleries", "Auto Parts & Supplies", "Automotive", "Books, Mags, Music & Video", "Cards & Stationery", "Children's Clothing",
                    "Cosmetics & Beauty Supply", "Department Stores", "Electronics", "Eyewear & Opticians",
                    "Fashion", "Florists", "Furniture Stores", "Grocery", "Hobby Shops", "Home & Garden",
                    "Jewelry", "Lingerie", "Luggage", "Men's Clothing",
                    "Nurseries & Gardening", "Pet Stores", "Photography Stores & Services", "Shoe Store", "Shoe Stores", "Shopping", "Sporting Goods",
                    "Toy Stores", "Watches", "Women's Clothing", "Pharmacy"}, "Shopping", R.drawable.u_icon_small_shopping),
            new GroupEntry_(new String[]{"Restroom"}, "Toilets", R.drawable.u_icon_small_toilets)
    };

    abstract void ms_onLocation(Manager.Location location, Map<String, Object> m);

    static abstract class STextWatcher extends MyTextWatcher {
        private MySearch m_search;

        STextWatcher(MyApplication app, EditText et, MySearch search) {
            super(app, et);
            m_search = search;
        }

        @Override
        void mtw_onIndoorFeatures(List<Manager.FeatureDesc> list) {
            m_search.m_featureDescList = list;
            m_search.update(false);
        }
        @Override
        void mtw_onOutdoorFeatures(List<MyApplication.OutdoorFeature> list) {
            m_search.m_outdoorFeatures = list;
            m_search.update(false);
        }
        @Override
        void mtw_onEmptySearch() {
            m_search.m_hasCurrentLocationRequest = false;
            m_search.m_featureDescList = null;
            m_search.m_outdoorFeatures = null;
            m_search.update(true);
        }
    }

    MySearch(Activity activity, ListView table) {
        m_activity = activity;

        final MyApplication app = (MyApplication)m_activity.getApplication();
        final Manager manager = app.m_manager;
        for (int i = 0; i < m_groupLocations.length; ++i) {
            m_groupLocations[i] = new Manager.Location(manager, -1, null, null, null);
        }

        m_adapter = new SimpleAdapter(m_activity, m_list, R.layout.directions_list, new String[]{s_titleStr, s_subtitleStr, s_icon}, new int[]{R.id.title_str_id, R.id.subtitle_str_id, R.id.icon_id});
        m_adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Object data, String textRepresentation) {
                if (view.getId() == R.id.subtitle_str_id) {
                    if (data == null) {
                        view.setVisibility(View.GONE);
                        return true;
                    } else {
                        view.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        });
        table.setAdapter(m_adapter);

        table.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Manager.Location location = m_locations.get(i);
                int idx = 0;
                for (Manager.Location gl : m_groupLocations) {
                    if (location == gl) {
                        findGroup(idx);
                        return;
                    }
                    ++idx;
                }
                final Map<String, Object> m = m_list.get(i);
                ms_onLocation(location, m);
            }
        });
    }

    private Manager.Location[] m_groupLocations = new Manager.Location[s_groups.length];
    private List<Manager.FeatureDesc> m_featureDescList;
    private List<MyApplication.OutdoorFeature> m_outdoorFeatures;
    private boolean m_hasCurrentLocationRequest;
    private List<Manager.Location> m_locations = new ArrayList<>();

    void update(boolean isEmptyText)
    {
        m_list.clear();
        m_locations.clear();

        if (m_hasCurrentLocationRequest)
        {
            m_locations.add(null);

            Map<String, Object> map = new HashMap<>();
            map.put(s_titleStr, m_activity.getString(R.string.current_location));

            m_list.add(map);
        }

        final MyApplication app = (MyApplication)m_activity.getApplication();
        final Manager manager = app.m_manager;

        if (m_featureDescList != null) {
            final Resources res = m_activity.getResources();
            final String packageName = m_activity.getPackageName();
            for (Manager.FeatureDesc fd : m_featureDescList) {
                final Manager.Location location = new Manager.Location(manager, fd.m_featureIndex, null, null, null);
                m_locations.add(location);

                Map<String, Object> map = new HashMap<>();
                map.put(s_titleStr, fd.m_name);

                if (manager != null) {
                    //final String mapLayer = manager.mapLayerForFeature(fd.m_featureIndex);
                    final Map<String, String> props = manager.propsForFeature(fd.m_featureIndex);
                    final String category = MyAppUtils.OptString(props, MyAppUtils.SUBTITLE_FIELD);
                    final String levelId = MyAppUtils.OptString(props, "LEVEL_ID");
                    final String levelName = app.nameForLevelId(levelId);
                    final String buildingName = app.buildingNameForLevelId(levelId);
                    if (buildingName == null) {
                        map.put(s_subtitleStr, String.format("%s - %s", category, levelName));
                    }
                    else
                    {
                        map.put(s_subtitleStr, String.format("%s - %s - %s", category, levelName, buildingName));
                    }
                    final int iconId = MyAppUtils.Icon_(props, true);
                    if (iconId != 0) {
                        map.put(s_icon, iconId);
                    }
                }

                m_list.add(map);
            }
        }

        if (m_outdoorFeatures != null) {
            for (MyApplication.OutdoorFeature oft : m_outdoorFeatures) {
                Mathe.MapPoint mp = new Mathe.MapPoint(0.0, 0.0);
                final Manager.Location location = new Manager.Location(manager, -1, null, oft.m_placeId, null);
                m_locations.add(location);

                Map<String, Object> map = new HashMap<>();
                map.put(s_titleStr, oft.m_title);
                map.put(s_subtitleStr, oft.m_subtitle);

                m_list.add(map);
            }
        }

        if (isEmptyText && m_list.isEmpty()) {
            // add group fields
            int idx = 0;
            for (Manager.Location loc: m_groupLocations) {
                m_locations.add(loc);

                final GroupEntry_ ge = s_groups[idx];

                Map<String, Object> map = new HashMap<>();
                map.put(s_titleStr, ge.m_name);
                map.put(s_icon, ge.m_iconId);
                m_list.add(map);
                ++idx;
            }
        }

        m_adapter.notifyDataSetChanged();
    }

    private void findGroup(int index) {
        final String[] groupCategories = s_groups[index].m_categories;
        final MyApplication app = (MyApplication)m_activity.getApplication();
        final Manager manager = app.m_manager;
        final List<Manager.FeatureDesc> list = new ArrayList<>();
        final List<Integer> ordinals = manager.getLevelOrdinals();
        for (int ord: ordinals) {
            manager.enumerateFeatures(ord, new Manager.EnumerateFeaturesCallback() {
                @Override
                public boolean onEnumeratingFeature(int featureIndex, String mapLayer, Map<String, String> props) {
                    final String category = MyAppUtils.OptString(props, "CATEGORY");
                    for (String gc: groupCategories) {
                        if (category.equals(gc)) {
                            final String name = MyAppUtils.OptString(props, "NAME");
                            list.add(new Manager.FeatureDesc(name, featureIndex));
                            break;
                        }
                    }
                    return false;
                }
            });
        }
        m_featureDescList = MyAppUtils.FilterOut_(manager, list);
        update(false);
    }

}