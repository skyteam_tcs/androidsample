package com.point_consulting.testindoormap;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextPaint;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.point_consulting.testindoormap.PopoverView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.maps.android.SphericalUtil;
import com.point_consulting.pc_indoormapoverlaylib.AbstractFolder;
import com.point_consulting.pc_indoormapoverlaylib.AssetsFolder;
import com.point_consulting.pc_indoormapoverlaylib.Coordinate3D;
import com.point_consulting.pc_indoormapoverlaylib.ExternalFolder;
import com.point_consulting.pc_indoormapoverlaylib.FeatureOptions;
import com.point_consulting.pc_indoormapoverlaylib.IMap;
import com.point_consulting.pc_indoormapoverlaylib.IMarker;
import com.point_consulting.pc_indoormapoverlaylib.IconOptions;
import com.point_consulting.pc_indoormapoverlaylib.IndoorCameraPosition;
import com.point_consulting.pc_indoormapoverlaylib.IndoorMap;
import com.point_consulting.pc_indoormapoverlaylib.IndoorPolygonOptions;
import com.point_consulting.pc_indoormapoverlaylib.IndoorPolylineOptions;
import com.point_consulting.pc_indoormapoverlaylib.Manager;
import com.point_consulting.pc_indoormapoverlaylib.MapImplGoogle;
import com.point_consulting.pc_indoormapoverlaylib.MapImplIndoor;
import com.point_consulting.pc_indoormapoverlaylib.Mathe;
import com.point_consulting.pc_indoormapoverlaylib.TextOptions;
import com.point_consulting.testindoormap.toolbar.MySimpleToolbar;
import com.point_consulting.testindoormap.transition.FadeInTransition;
import com.point_consulting.testindoormap.transition.FadeOutTransition;
import com.point_consulting.testindoormap.transition.SimpleTransitionListener;

import junit.framework.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, DownloadMaps.Delegate, MyApplication.GoogleLocationCallback, PopoverView.PopoverViewDelegate {
    private static Mathe.IndoorLatLng s_hackedCenter = new Mathe.IndoorLatLng(60.040291238122467, 30.392644503671253);

    private static final String s_droppedPinId = "dropped_pin";
    private static final String s_startPinId = "start_pin";
    private static final String s_endPinId = "end_pin";

    private static final int s_purplePinColor = 0xffda70d6;
    private static final int s_greenPinColor = 0xff00ff00;
    private static final int s_redPinColor = 0xffff0000;

    private static final String s_camPosLat = "s_camPosLat";
    private static final String s_camPosLon = "s_camPosLon";
    private static final String s_camPosZoom = "s_camPosZoom";
    private static final String s_camPosBearing = "s_camPosBearing";

    private static final int s_routeColorIndoor = 0xff337fff;
    private static final int s_routeColorOutdoor = 0xffff7f33;

    private static final int s_requestCodeInfo = 1;
    private static final int s_requestCodeDirections = 2;
    private static final int s_requestCodeSearch = 3;

    private List<MyAppUtils.PropDesc> m_propDesc = new ArrayList<>();
    private IMap m_map;
    private IMarker m_selectedMarker;
    private Manager m_manager;
    private final String m_routeIdIndoor = "routeIndoor";
    private final String m_routeIdOutdoor = "routeOutdoor";
    private IndoorCameraPosition m_cameraPosition;
    private boolean m_initialized;
    private List<Manager.IndoorMapStep> m_routeSteps;
    private int m_routeStepsIndex;
    private int m_toSelectFeatureIndex = -1;

    private TextView selectedAirportText;
    private TextView backNavigationText;

    private String selectedAirport;
    private String selectedAirportCode;
    private String airportMapIndex;


    private int m_toolbarMargin;
    private MySimpleToolbar m_toolbar;
    private Dialog m_popupDialog;
    private ViewGroup m_rootView;
    private PopoverView m_popoverView;
    private int m_popoverRefId;
    private View m_dim;
    private ViewGroup m_routeBar;
    private TextView m_labelStep;
    private ImageButton m_buttonPrev;
    private ImageButton m_buttonNext;
    private TextView m_descLabel;
    private TextView m_distTimeLabel;

    private ProgressDialog m_progressDialog;

    private boolean m_wantHaveRoute;
    private boolean m_wantRecalcRoute;

    private Typeface m_typeface;

    private static final boolean s_useServer = false;
    private static final boolean s_showUserTrackingModeButton = true;

    public void popoverViewWillShow(PopoverView view) {
        m_rootView.addView(m_dim);
    }

    public void popoverViewDidShow(PopoverView view){

    }

    public void popoverViewWillDismiss(PopoverView view) {

    }

    public void popoverViewDidDismiss(PopoverView view) {
        m_rootView.removeView(m_dim);
        m_popoverView = null;
        m_popoverRefId = 0;
    }

    private void startMap(AbstractFolder abstractFolder)
    {
        Log.e("Android Maps", "startMap");

        m_progressDialog.setTitle("Preparing maps...");
        m_progressDialog.show();
        Manager.InitializationCallback initializationCallback = new Manager.InitializationCallback() {
            public void onIndoorMapManagerInitialized()
            {
                final MyApplication app = (MyApplication)getApplication();

                m_progressDialog.dismiss();
                m_progressDialog = null;
                m_initialized = true;
                app.m_manager = m_manager;

                app.dropLevelNames();

                final List<Integer> ordinals = m_manager.getLevelOrdinals();
                if (ordinals != null) {

                    final Map<String, String> levelId2BuildingId = new HashMap<>();
                    final Map<String, String> buildingId2BuildingName = new HashMap<>();

                    for (int ord: ordinals) {
                        // illustrate enumerateFeatures
                        final int ordinal1 = ord;
                        m_manager.enumerateFeatures(ord, new Manager.EnumerateFeaturesCallback() {
                            @Override
                            public boolean onEnumeratingFeature(int featureIndex, String mapLayer, Map<String, String> props) {
                                switch (mapLayer) {
                                    case "Levels": {
                                        final String shortName = MyAppUtils.OptString(props, "SHORT_NAME");
                                        final String longName = MyAppUtils.OptString(props, "NAME");
                                        final String levelId = MyAppUtils.OptString(props, "LEVEL_ID");
                                        app.setLevelName(levelId, longName);
                                        app.setOrdinalShortName(ordinal1, shortName);
                                        app.setOrdinalLongName(ordinal1, longName);

                                        final String buildingId = MyAppUtils.OptString(props, "BLDG_ID");
                                        if (!buildingId.isEmpty()) {
                                            levelId2BuildingId.put(levelId, buildingId);
                                        }
                                        break;
                                    }
                                    case "Buildings": {
                                        final String buildingId = MyAppUtils.OptString(props, "BLDG_ID");
                                        final String buildingName = MyAppUtils.OptString(props, "NAME");
                                        if (!buildingId.isEmpty() && !buildingName.isEmpty()) {
                                            buildingId2BuildingName.put(buildingId, buildingName);
                                        }
                                        break;
                                    }
                                    case "Occupants": {
                                        final String name = MyAppUtils.OptString(props, "NAME");
                                        if (name.equals("Enterprise Rent-A-Car")) {
                                            m_toSelectFeatureIndex = featureIndex;
                                        }
                                        break;
                                    }
                                }
                                return false;
                            }
                        });
                    }

                    for (Map.Entry<String, String> e: levelId2BuildingId.entrySet())
                    {
                        final String levelId = e.getKey();
                        final String buildingId = e.getValue();
                        final String buildingName = buildingId2BuildingName.get(buildingId);
                        if (buildingName != null)
                        {
                            app.setBuildingName(levelId, buildingName);
                        }
                    }

                    final int n = ordinals.size();
                    if (n > 0) {
                        int ordinal = ordinals.get(0);
                        for (Integer ordN : ordinals) {
                            if (ordN == 0) {
                                ordinal = 0;
                                break;
                            }
                        }

                        final LatLng center = app.getCurrentVenueCenter();
                        m_cameraPosition = new IndoorCameraPosition(new Mathe.IndoorLatLng(center.latitude, center.longitude), 15.f, 0.f, 0.f);

                        m_map.setCameraPosition(m_cameraPosition, false);
                        m_manager.showOrdinal(ordinal);
                    }
                }
            }
        };

        final Map<String, Manager.TitleDesc> titleFieldsForMapLayer = new HashMap<>();
        titleFieldsForMapLayer.put("Units", new Manager.TitleDesc(new String[]{"CATEGORY"}, null));
        titleFieldsForMapLayer.put("Points", new Manager.TitleDesc(new String[]{"NAME", "SUITE"}, "CATEGORY"));
        titleFieldsForMapLayer.put("Occupants", new Manager.TitleDesc(new String[]{"NAME"}, "CATEGORY"));
        titleFieldsForMapLayer.put("Zones", new Manager.TitleDesc(new String[]{"NAME"}, null));

        m_manager.initializeAsync(abstractFolder, titleFieldsForMapLayer, 0, initializationCallback);
    }

    private void onFinishedDownload()
    {
        MyApplication app = (MyApplication)getApplication();

        AbstractFolder abstractFolder = null;

        if (s_useServer) {
            final String venueId = app.getCurrentVenueId();
            if (venueId != null) {
                File dir = DownloadMaps.GetDir(this, venueId);
                final File[] list = dir.listFiles();

                if (list != null && list.length > 0) {
                    abstractFolder = new ExternalFolder(dir);
                }
            }
        }

        if (abstractFolder == null)
        {
            final AssetManager assets = getAssets();
//            final String value=app.getCurrentVenueName();
            if (selectedAirportCode.equalsIgnoreCase("AMS")){
                app.m_currentVenueIndex = 0;
            } else if (selectedAirportCode.equalsIgnoreCase("IST")){
                app.m_currentVenueIndex = 1;
            }
            else if (selectedAirportCode.equalsIgnoreCase("LHR")){
                app.m_currentVenueIndex = 2;
            }
            else if (selectedAirportCode.equalsIgnoreCase("SYD")){
                app.m_currentVenueIndex = 3;
            }
            else if (selectedAirportCode.equalsIgnoreCase("FCO")){
                app.m_currentVenueIndex = 4;
            }
            else if (selectedAirportCode.equalsIgnoreCase("GVA")){
                app.m_currentVenueIndex = 5;
            }
            else{
                app.m_currentVenueIndex = 6;
            }
            abstractFolder = new AssetsFolder(assets, app.getCurrentVenueName());
        }

        // set bounds for outdoor search
        final LatLng center = app.getCurrentVenueCenter();
        final double radius = 20000;
        final LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        final LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
        app.m_bounds = new LatLngBounds(southwest, northeast);

        startMap(abstractFolder);
    }

    @Override
    public void onCheckMapUpdatesFinishedWithResult(int checkResult)
    {
        switch (checkResult)
        {
            case DownloadMaps.DOWNLOAD_MAPS_CHECK_RESULT_ERROR:
                onFinishedDownload();
                break;
            case DownloadMaps.DOWNLOAD_MAPS_CHECK_RESULT_READY:
                onFinishedDownload();
                break;
            default:
                Assert.assertEquals(checkResult, DownloadMaps.DOWNLOAD_MAPS_CHECK_RESULT_DOWNLOADING);
                m_progressDialog.setTitle("Downloading maps...");
                m_progressDialog.show();
                //self.m_downloadView.hidden = NO;
        }
    }

    @Override
    public void onDownloadMapsFinishedWithError(String error)
    {
        onFinishedDownload();
    }

    private void setRouteStepButtonEnabled(ImageButton button, boolean enabled)
    {
        final Resources resources = getResources();
        final Drawable drArrow = resources.getDrawable(R.drawable.arrow);
        drArrow.mutate();

        button.setEnabled(enabled);
        if (enabled) {
            final MyStateDrawable msd = new MyStateDrawable(new Drawable[]{drArrow});
            button.setImageDrawable(msd);
        }
        else
        {
            drArrow.setColorFilter(0xff7fb7df, PorterDuff.Mode.SRC_ATOP);
            button.setImageDrawable(drArrow);
        }
    }

    private void updateUserTrackingModeButton(int userTrackingMode)
    {
        if (!s_showUserTrackingModeButton)
        {
            return;
        }

        final View button = findViewById(R.id.userTrackingModeViewButton);
        if (button != null) {
            int id;
            switch (userTrackingMode)
            {
                case IMap.USER_TRACKING_MODE_NONE:
                    id = R.drawable.track_none_button;
                    break;
                case IMap.USER_TRACKING_MODE_FOLLOW:
                    id = R.drawable.track_heading_button;
                    break;
                default:
                    Assert.assertEquals(userTrackingMode, IMap.USER_TRACKING_MODE_FOLLOW_WITH_HEADING);
                    id = R.drawable.track_follow_button;
                    break;
            }
            button.setBackgroundResource(id);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (m_popoverView != null) {
            final int refId = m_popoverRefId;
            m_popoverView.dismissPopover(false);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showLevelSelector(refId);
                }
            }, 500);
        }
    }

    private void transitionToSearch() {
        // create a transition that navigates to search when complete
        Transition transition = FadeOutTransition.withAction(navigateToSearchWhenDone());

        // let the TransitionManager do the heavy work for us!
        // all we have to do is change the attributes of the toolbar and the TransitionManager animates the changes
        // in this case I am removing the bounds of the toolbar (to hide the blue padding on the screen) and
        // I am hiding the contents of the Toolbar (Navigation icon, Title and Option Items)
        TransitionManager.beginDelayedTransition(m_toolbar, transition);
        FrameLayout.LayoutParams frameLP = (FrameLayout.LayoutParams) m_toolbar.getLayoutParams();
        frameLP.setMargins(0, 0, 0, 0);
        m_toolbar.setLayoutParams(frameLP);
        m_toolbar.hideContent();
    }

    private Transition.TransitionListener navigateToSearchWhenDone() {
        return new SimpleTransitionListener() {
            @Override
            public void onTransitionEnd(Transition transition) {
                Intent intent = new Intent(MapsActivity.this, SearchActivity.class);
                intent.putExtra("airportName", selectedAirport);
                startActivityForResult(intent, s_requestCodeSearch);


                // we are handing the enter transitions ourselves
                // this line overrides that
                overridePendingTransition(0, 0);

                // by this point of execution we have animated the 'expansion' of the Toolbar and hidden its contents.
                // We are half way there. Continue to the SearchActivity to finish the animation
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("Android Maps", "onCreate");

        super.onCreate(savedInstanceState);

        m_typeface = Typeface.createFromAsset(getAssets(), "univers_extended.ttf");

        /*Mathe.MapPoint mpCenter = new Mathe.MapPoint(0);
        Mathe.MapPointFromLatLng(s_center.latitude, s_center.longitude, mpCenter);

        Mathe.MapPoint mpHackedCenter = new Mathe.MapPoint(0);
        Mathe.MapPointFromLatLng(s_hackedCenter.latitude, s_hackedCenter.longitude, mpHackedCenter);

        Utils.SetHack(mpHackedCenter.x - mpCenter.x, mpHackedCenter.y - mpCenter.y);
        s_center = s_hackedCenter;*/

        final float density = getResources().getDisplayMetrics().density;

        m_propDesc.add(new MyAppUtils.PropDesc(R.drawable.clock, "HOURS"));
        m_propDesc.add(new MyAppUtils.PropDesc(R.drawable.phone, "PHONE"));
        m_propDesc.add(new MyAppUtils.PropDesc(R.drawable.web, "WEBSITE"));

        if (savedInstanceState != null)
        {
            final double lat = savedInstanceState.getDouble(s_camPosLat);
            final double lon = savedInstanceState.getDouble(s_camPosLon);
            final float zoom = savedInstanceState.getFloat(s_camPosZoom);
            final float bearing = savedInstanceState.getFloat(s_camPosBearing);
            m_cameraPosition = new IndoorCameraPosition(new Mathe.IndoorLatLng(lat, lon), zoom, 0.f, bearing);
        }

        if (/*BuildConfig.isOverlay*/true) {
            setContentView(R.layout.activity_maps_google);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            com.google.android.gms.maps.SupportMapFragment mapFragment = (com.google.android.gms.maps.SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
        else
        {
            setContentView(R.layout.activity_maps_indoor);

            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            com.point_consulting.pc_indoormapoverlaylib.MapFragment mapFragment = (com.point_consulting.pc_indoormapoverlaylib.MapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            final IndoorMap indoorMap = mapFragment.getMap();
            indoorMap.setMaxZoom(23.0);
            indoorMap.setArrowParams(16, 20, s_routeColorIndoor, 30.f, 120.f);
            start(new MapImplIndoor(indoorMap));
        }

        selectedAirportText = findViewById(R.id.selectedAirportText);
        backNavigationText = findViewById(R.id.backtext);
        if (getIntent().getExtras() != null) {
            selectedAirport = getIntent().getStringExtra("airportName");
            selectedAirportText.setText(selectedAirport);
            selectedAirportCode = getIntent().getStringExtra("airportCode");
            airportMapIndex = getIntent().getStringExtra("indexValuesMaps");

        }
        backNavigationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        m_dim = new View(this);
        {
            final ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            m_dim.setLayoutParams(lp);
            m_dim.setBackgroundColor(0x12000000);
        }

        m_toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(m_toolbar);

        m_toolbarMargin = getResources().getDimensionPixelSize(R.dimen.toolbar_margin);
        m_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Prepare the keyboard as soon as the user touches the Toolbar
                // This will make the transition look faster
                MyAppUtils.ShowKeyboard(MapsActivity.this);
                transitionToSearch();
            }
        });

        m_rootView = findViewById(R.id.rootLayout);
        m_routeBar = findViewById(R.id.routeBar);
        m_descLabel = m_routeBar.findViewById(R.id.descLabel);
        m_labelStep = m_routeBar.findViewById(R.id.stepLabel);
        m_buttonPrev = m_routeBar.findViewById(R.id.route_prev);
        m_buttonNext = m_routeBar.findViewById(R.id.route_next);
        m_distTimeLabel = m_routeBar.findViewById(R.id.routeDistTime);

        if (!s_showUserTrackingModeButton)
        {
            findViewById(R.id.userTrackingModeView).setVisibility(View.INVISIBLE);
        }
    }

    private void cancelAll()
    {
        m_wantHaveRoute = false;
        m_manager.dropRoute(m_routeIdIndoor);
        m_manager.dropRoute(m_routeIdOutdoor);
        m_manager.dropMark(s_droppedPinId);
        m_manager.dropMark(s_startPinId);
        m_manager.dropMark(s_endPinId);

        m_routeBar.setVisibility(View.GONE);
    }

    private static String GetTimeString_(int timeSec)
    {
        timeSec = ((timeSec + 2) / 5) * 5;
        final int hours = timeSec / 3600;
        timeSec -= hours * 3600;
        final int minutes = timeSec / 60;
        timeSec -= minutes * 60;

        if (hours > 0)
        {
            return String.format(Locale.US, "%dh %dmin", hours, minutes);
        }
        else if (minutes > 0)
        {
            return String.format(Locale.US, "%dmin %ds", minutes, timeSec);
        }
        return String.format(Locale.US, "%ds", timeSec);
    }

    void gotoRouteStep(int stepIndex, boolean needCenter)
    {
        m_routeStepsIndex = stepIndex;
        if (stepIndex < m_routeSteps.size())
        {
            Manager.IndoorMapStep step = m_routeSteps.get(stepIndex);
            m_labelStep.setText(step.m_instructions);

            if (needCenter)
            {
                final List<Mathe.MapPoint> polyline = step.m_polyline;
                final int n = polyline.size();
                if (n > 0)
                {
                    Mathe.MapPoint point = polyline.get(0);
                    double xmin = point.x;
                    double ymin = point.y;
                    double xmax = xmin;
                    double ymax = ymin;
                    for (int i = 1; i < n; ++i)
                    {
                        Mathe.MapPoint p1 = polyline.get(i);

                        xmin = Math.min(xmin, p1.x);
                        xmax = Math.max(xmax, p1.x);
                        ymin = Math.min(ymin, p1.y);
                        ymax = Math.max(ymax, p1.y);
                    }
                    //final double s_margin = 10.;
                    m_map.setVisibleMapRect(new Mathe.MapRect(xmin, ymin, xmax - xmin, ymax - ymin), true);
                }
                m_manager.showOrdinal(step.m_ordinal);
            }
        }
        setRouteStepButtonEnabled(m_buttonNext, stepIndex + 1 < m_routeSteps.size());
        setRouteStepButtonEnabled(m_buttonPrev, stepIndex > 0);
    }

    private void recalcRoute(final boolean needCenter)
    {
        Assert.assertTrue(m_wantHaveRoute);
        m_wantRecalcRoute = true;
        if (!m_manager.canCalcRoute())
        {
            return;
        }
        m_wantRecalcRoute = false;

        Manager.Location locFrom1 = m_manager.markLocation(s_startPinId);
        if (null == locFrom1)
        {
            locFrom1 = ((MyApplication)getApplication()).getUserLocation(null);
            if (null == locFrom1)
            {
                return;
            }
        }
        Manager.Location locTo1 = m_manager.markLocation(s_endPinId);
        if (null == locTo1)
        {
            locTo1 = ((MyApplication)getApplication()).getUserLocation(null);
            if (null == locTo1)
            {
                return;
            }
        }

        final Manager.Location locFrom = locFrom1;
        final Manager.Location locTo = locTo1;

        m_manager.calcRoute(locFrom, locTo, new Manager.CalcRouteCallback() {
            @Override
            public void onIndoorMapManagerRouteCalculated(Manager.RouteResult[] results) {
                m_manager.dropRoute(m_routeIdIndoor);
                m_manager.dropRoute(m_routeIdOutdoor);
                m_routeBar.setVisibility(View.GONE);

                if (results == null || !m_wantHaveRoute)
                {
                    return;
                }

                final float density = getResources().getDisplayMetrics().density;

                List<Manager.IndoorMapStep> steps = new ArrayList<>();
                int duration = 0;
                int length = 0;
                for (Manager.RouteResult res : results)
                {
                    duration += res.m_duration;
                    length += res.m_length;

                    final IndoorPolylineOptions options = new IndoorPolylineOptions();
                    options.color(res.m_isIndoor ? s_routeColorIndoor : s_routeColorOutdoor).width(2.5f * density);
                    m_manager.createRoute(res.m_lines, options, res.m_isIndoor ? m_routeIdIndoor : m_routeIdOutdoor);
                    steps.addAll(res.m_steps);
                }

                final String timeStr = GetTimeString_(duration);
                m_distTimeLabel.setText(String.format("%1$dm / %2$s", length, timeStr));

                String titles[] = new String[2];
                m_manager.getTitleForLocation(locTo, titles);
                m_descLabel.setText(String.format(getString(R.string.route_string), titles[0]));

                m_routeSteps = steps;
                gotoRouteStep(0, needCenter);

                m_routeBar.setVisibility(View.VISIBLE);

                if (m_wantRecalcRoute)
                {
                    recalcRoute(needCenter);
                }
            }
        });
    }

    private void proceed(Manager.Location location)
    {
        final Coordinate3D c3d = location.m_coord3D;
        Mathe.IndoorLatLng center = Mathe.LatLngFromMapPoint(c3d.m_coordinate);
        m_cameraPosition = new IndoorCameraPosition(center, 21.f, 0.f, 0.f);
        m_map.setCameraPosition(m_cameraPosition, false);
        m_manager.showOrdinal(c3d.m_ordinal);

        createMark(location, s_redPinColor, true, s_endPinId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK) {
            if (requestCode == s_requestCodeInfo || requestCode == s_requestCodeDirections) {
                // ktodo is it cool? previously it was always needSelect = true here
                final Boolean needSelect = requestCode == s_requestCodeInfo;
                final Manager.Location start = data.getParcelableExtra(MyAppUtils.s_extra_start);
                final Manager.Location end = data.getParcelableExtra(MyAppUtils.s_extra_end);

                cancelAll();
                if (start != null) {
                    createMark(start, s_greenPinColor, needSelect, s_startPinId);
                } else {
                    m_manager.dropMark(s_startPinId);
                }
                if (end != null) {
                    createMark(end, s_redPinColor, needSelect, s_endPinId);
                } else {
                    m_manager.dropMark(s_endPinId);
                }

                m_wantHaveRoute = true;
                recalcRoute(true);
            }
            else if (requestCode == s_requestCodeSearch) {
                final Manager.Location location = data.getParcelableExtra(MyAppUtils.s_extra_location);
                proceed(location);
            }
        }
    }

    private Bitmap getBitmap(int id)
    {
        Drawable dr = ContextCompat.getDrawable(this, id);
        BitmapDrawable bdr = (BitmapDrawable)dr;
        return bdr.getBitmap();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        retryLoadMaps();
    }

    private static final int MY_REQUEST_PERMISSIONS = 1;
    private static final int MY_REQUEST_PERMISSION_PHONE = 2;

    private void showLevelSelector(int refId) {
        if (!m_initialized || m_popoverView != null) {
            return;
        }

        final List<Integer> ordinals = m_manager.getLevelOrdinals();
        if (ordinals == null) {
            return;
        }

        final int popoverDirection = refId == R.id.levelsChangeViewB ? PopoverView.PopoverArrowDirectionRight : PopoverView.PopoverArrowDirectionDown;

        final View buttonIcon = findViewById(refId);

        final MyApplication app = (MyApplication)getApplication();
        final float density = getResources().getDisplayMetrics().density;

        final float textSizeDp = 16.f;
        TextPaint tp = new TextPaint();
        tp.setTextSize(textSizeDp * density);

        float maxW = density * 38; // default
        for (Integer ord: ordinals) {
            final String levelShortName = app.shortNameForOrdinal(ord);
            maxW = Math.max(maxW, tp.measureText(levelShortName));
        }
        maxW += density * 12;

        final int ww = Math.round(maxW);
        final int hh = Math.round(density * 40);
        final int decW = Math.round(density * 7);
        final int hd = Math.round(density * 1);

        m_popoverRefId = refId;
        m_popoverView = new PopoverView(this, R.layout.popover_showed_view);
        final View popoverViewInternal = m_popoverView.getPopoverView();
        final LinearLayout ll = popoverViewInternal.findViewById(R.id.popover_layout);

        final int nO = ordinals.size();
        final ListIterator<Integer> li = ordinals.listIterator(nO);
        int i = 0;
        final int curOrdinal = m_manager.getOrdinal();
        View focus = null;
        while (li.hasPrevious())
        {
            final Integer val = li.previous();
            final String levelShortName = app.shortNameForOrdinal(val);

            TextView tv = new TextView(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ww, hh);
            tv.setLayoutParams(lp);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSizeDp);
            tv.setText(levelShortName);
            int textColor = 0xff000000;
            if (curOrdinal == val) {
                focus = tv;
                textColor = 0xff049fe1;
            }
            tv.setTextColor(textColor);
            tv.setBackgroundColor(0xffffffff);
            tv.setGravity(Gravity.CENTER);
            ll.addView(tv);
            final int position = i;
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showOrdinal(position);
                    m_popoverView.dismissPopover(false);
                }
            });

            if (i < nO - 1) {
                View sep = new View(this);
                sep.setBackgroundColor(0xffc8c7cc);
                LinearLayout.LayoutParams slp = new LinearLayout.LayoutParams(ww - decW, hd);
                slp.gravity = Gravity.CENTER_HORIZONTAL;
                sep.setLayoutParams(slp);
                ll.addView(sep);
            }

            ++i;
        }

        final ScrollView scrollView = popoverViewInternal.findViewById(R.id.popover_scroll_view);
        scrollView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        m_popoverView.setContentSizeForViewInPopover(new Point(scrollView.getMeasuredWidth(), scrollView.getMeasuredHeight()));
        m_popoverView.setDelegate(this);
        final Rect pr = PopoverView.getFrameForView(buttonIcon);        
        m_popoverView.showPopoverFromRectInViewGroup(m_rootView, pr, popoverDirection, false);

        if (focus != null) {
            final View focusC = focus;

            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    scrollView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    final Rect pr1 = PopoverView.getFrameForView(focusC);
                    final float y = pr.centerY();
                    scrollView.scrollBy(0, Math.round(pr1.centerY() - y));
                }
            });
        }
    }

    private void showOrdinal(int position) {
        final List<Integer> ordinals = m_manager.getLevelOrdinals();
        m_manager.showOrdinal(ordinals.get(ordinals.size() - 1 - position));
//        // illustrate setZoomLevels
//        m_manager.setZoomLevels(new Manager.SetZoomLevelsCallback() {
//            @Override
//            public void onMarker(String mapLayer, Map<String, String> props, boolean isIcon, float[] zoomLevels) {
//                final String category = MyAppUtils.OptString(props, "CATEGORY");
//                if (category.equals("Restaurants"))
//                {
//                    zoomLevels[0] = 10000.f;
//                }
//            }
//        });
    }

    private void showVenue(int position) {
        final MyApplication app = (MyApplication)getApplication();
        app.setCurrentVenueIndex(position);
        retryLoadMaps();
    }

    public void onButton(View view)
    {
        final int id = view.getId();
        if (id == R.id.button_end_route) {
            cancelAll();
        }
        else if (id == R.id.stepLabel)
        {
            gotoRouteStep(m_routeStepsIndex, true);
        }
        else if (id == R.id.route_next)
        {
            if (m_routeStepsIndex + 1 < m_routeSteps.size())
            {
                gotoRouteStep(m_routeStepsIndex + 1, true);
            }
        }
        else if (id == R.id.route_prev)
        {
            if (m_routeStepsIndex > 0)
            {
                gotoRouteStep(m_routeStepsIndex - 1, true);
            }
        }
        else if (id == R.id.userTrackingModeView)
        {
            final int curMode = m_map.getUserTrackingMode();
            int newMode;
            switch (curMode)
            {
                case IMap.USER_TRACKING_MODE_NONE:
                    newMode = IMap.USER_TRACKING_MODE_FOLLOW;
                    break;
                case IMap.USER_TRACKING_MODE_FOLLOW:
                    newMode = IMap.USER_TRACKING_MODE_FOLLOW_WITH_HEADING;
                    break;
                default:
                    Assert.assertEquals(curMode, IMap.USER_TRACKING_MODE_FOLLOW_WITH_HEADING);
                    newMode = IMap.USER_TRACKING_MODE_NONE;
            }
            m_map.setUserTrackingMode(newMode);
        }
        else if (id == R.id.levelsChangeView)
        {
            showLevelSelector(R.id.levelsChangeViewB);
        }
        else if (id == R.id.levelsLabelLayout) {
            showLevelSelector(R.id.levelsLabel);
        }
        else /*if (id == R.id.gotoOriginalMapView)*/
        {
            gotoOriginalMapView();
        }
    }

    private void gotoOriginalMapView() {
        final MyApplication app = (MyApplication)getApplication();
        final LatLng center = app.getCurrentVenueCenter();
        m_cameraPosition = new IndoorCameraPosition(new Mathe.IndoorLatLng(center.latitude, center.longitude), 15.f, 0.f, 0.f);
        m_map.setCameraPosition(m_cameraPosition, false);
    }

    private void retryLoadMaps()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED))
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, MY_REQUEST_PERMISSIONS);
            return;
        }

        m_map.setMyLocationEnabled(true);

        MyApplication app = (MyApplication)getApplication();
        app.setHaveAccessLocationPermission();

        m_progressDialog = new ProgressDialog(this);
        m_progressDialog.setCancelable(false);

        if (s_useServer) {
            final String venueId = app.getCurrentVenueId();
            if (venueId != null) {
                final DownloadMaps downloadMaps = new DownloadMaps(this, this);
                m_progressDialog.setTitle("Check for map updates...");
                m_progressDialog.show();
                downloadMaps.downloadMapId(venueId);
            }
        }
        else {
            onFinishedDownload();
        }
    }

    private void createMark(Manager.Location location, int pinColor, boolean needSelect, String userId)
    {
        final float w = getResources().getDimension(R.dimen.d3);
        m_manager.createMark(location, 0xff0000ff, w, pinColor, needSelect, userId, Manager.kIgnoreIntersectionsZIndex);
    }

    private void onCalloutDetailTapped(Manager.Location location)
    {
        final int featureIndex = location.m_featureIndex;
        Map<String, String> obj;
        ArrayList<MyAppUtils.PropDesc> props = new ArrayList<>();
        int headerColor = 0xffffffff;
        int logoId = 0;
        if (featureIndex != -1)
        {
            obj = m_manager.propsForFeature(featureIndex);
            for (MyAppUtils.PropDesc pd : m_propDesc)
            {
                final String text = MyAppUtils.OptString(obj, pd.m_string);
                if (!text.isEmpty())
                {
                    props.add(new MyAppUtils.PropDesc(pd.m_id, text));
                }
            }
            final String category = MyAppUtils.OptString(obj, "CATEGORY");
            if (category.equals("Accessories")||category.equals("Applicances")||category.equals("Art Galleries")||category.equals("Auto Parts & Supplies")||category.equals("Automotive")||category.equals("Beer, Wine & Spirits")||category.equals("Books, Mags, Music & Video")||category.equals("Cards & Stationery")||category.equals("Children's Clothing")||category.equals("Cosmetics & Beauty Supply")||category.equals("Department Stores")||category.equals("Dry Cleaning & Laundry")||category.equals("Eyewear & Opticians")||category.equals("Fashion")||category.equals("Florists")||category.equals("Shopping")||category.equals("Furniture Stores")||category.equals("Grocery")||category.equals("Shopping")||category.equals("Home & Garden")||category.equals("Jewelry")||category.equals("Shopping")||category.equals("Lingerie")||category.equals("Luggage")||category.equals("Shopping")||category.equals("Men's Clothin")||category.equals("Nurseries & Gardening")||category.equals("Pet Stores")||category.equals("Photography Stores & Services")||category.equals("Sewing & Alterations")||category.equals("Shoe Stores")||category.equals("Shopping")||category.equals("Sporting Goods")||category.equals("Toy Stores")||category.equals("Watches")||category.equals("Sporting Goods")||category.equals("Women's Clothing"))
            {
                headerColor = 0xffcce5ff;
            }
            else if (category.equals("Cafes")||category.equals("Coffee & Tea")||category.equals("Specialty Food")||category.equals("Restaurants"))
            {
                headerColor = 0xffa2dcb3;
            }
            else if (category.equals("Health & Medical")||category.equals("Pharmacy"))
            {
                headerColor = 0xfffccccf;
            }
            else if (category.equals("Banks & Credit Unions")||category.equals("Financial Services"))
            {
                headerColor = 0xffffe4c4;
            }
            else if (category.equals("Arts & Entertainment")||category.equals("Cinema")||category.equals("Landmarks & Historical")||category.equals("Buildings")||category.equals("Opera & Ballet")||category.equals("Performing Arts"))
            {
                headerColor = 0xffd45c5c;
            }
            else if (category.equals("Beauty & Spas")||category.equals("Car Rental")||category.equals("Education")||category.equals("Hotels")||category.equals("Libraries")||category.equals("Local Services")||category.equals("Post Offices")||category.equals("Professional Services")||category.equals("Property Management")||category.equals("Public Services & Government")||category.equals("Real Estate Agents")||category.equals("Real Estate Services")||category.equals("Travel Services")||category.equals("Veterinarians"))
            {
                headerColor = 0xff5ecfd2;
            }
            final String name = MyAppUtils.OptString(obj, "NAME");
            switch (name) {
                /*case "American Airlines":
                    logoId = R.drawable.aa;
                    break;
                case "Brighton":
                    logoId = R.drawable.brighton;
                    break;
                case "Brooks Brothers":
                    logoId = R.drawable.brooks;
                    break;*/
                case "Burger King":
                    logoId = R.drawable.burgerking;
                    break;
                /*case "Chili's Too":
                    logoId = R.drawable.chillis;
                    break;
                case "Ciao":
                    logoId = R.drawable.ciao;
                    break;
                case "CNBC":
                    logoId = R.drawable.cnbc;
                    break;
                case "Delta Air Lines":
                    logoId = R.drawable.delta;
                    break;
                case "Freshens":
                    logoId = R.drawable.freshens;
                    break;
                case "InMotion Entertainment":
                    logoId = R.drawable.inmotion;
                    break;
                case "Insight":
                    logoId = R.drawable.insight;
                    break;
                case "JetBlue":
                    logoId = R.drawable.jetblue;
                    break;
                case "Nathan's Famous":
                    logoId = R.drawable.nathans;
                    break;
                case "PGA TOUR Superstore":
                    logoId = R.drawable.pga;
                    break;
                case "Quiznos":
                    logoId = R.drawable.quiznos;
                    break;
                case "River City Travel Mart":
                    logoId = R.drawable.river_city_travel;
                    break;
                case "Sam Snead's Tavern":
                    logoId = R.drawable.samsneads;
                    break;
                case "SBARRO":
                    logoId = R.drawable.sbarros;
                    break;
                case "Shula's Bar & Grill":
                    logoId = R.drawable.shula;
                    break;
                case "Silver Airways":
                    logoId = R.drawable.silver;
                    break;
                case "Southwest Airlines":
                    logoId = R.drawable.southwest;
                    break;*/
                case "Starbucks":
                    logoId = R.drawable.starbucks;
                    break;
                /*case "United Airlines":
                    logoId = R.drawable.united;
                    break;
                case "Vino Volo":
                    logoId = R.drawable.vino_volo;
                    break;
                case "Alamo Rent a Car":
                    logoId = R.drawable.alamo;
                    break;
                case "Avis Rent a Car System":
                    logoId = R.drawable.avis;
                    break;
                case "Budget Rent a Car":
                    logoId = R.drawable.budget;
                    break;
                case "Enterprise Rent-A-Car":
                    logoId = R.drawable.enterprise;
                    break;
                case "The Hertz Corporation":
                    logoId = R.drawable.hertz;
                    break;
                case "National Car Rental":
                    logoId = R.drawable.nationalcar;
                    break;
                case "Dollar Rent A Car":
                    logoId = R.drawable.dollar;
                    break;*/
                case "Executive Conference Room":
                    logoId = R.drawable.conference;
                    break;
                /*case "Made-in-JAX":
                    logoId = R.drawable.made_in_jax;
                    break;*/
            }
        }

        final Intent intent = new Intent(MapsActivity.this, InfoActivity.class);
        intent.putExtra(MyAppUtils.s_extra_location, location);
        intent.putExtra(MyAppUtils.s_extra_color, headerColor);
        intent.putExtra(MyAppUtils.s_extra_logo, logoId);
        intent.putParcelableArrayListExtra(MyAppUtils.s_extra_propsMap, props);
        startActivityForResult(intent, s_requestCodeInfo);
    }

    private void showPopup(IMarker marker) {
        if (m_popupDialog != null) {
            m_popupDialog.dismiss();
            m_popupDialog = null;
        }

        String hours = "";
        String phone = "";
        String webSite = "";
        String category = null, levelName;
        Map<String, String> props = null;

        MyApplication app = (MyApplication) getApplication();
        final Manager.Location location = marker.getLocation();
        if (location.m_featureIndex >= 0) {
            props = m_manager.propsForFeature(location.m_featureIndex);
            hours = MyAppUtils.OptString(props, "HOURS");
            phone = MyAppUtils.OptString(props, "PHONE");
            webSite = MyAppUtils.OptString(props, "WEBSITE");
            category = MyAppUtils.OptString(props, "CATEGORY");
            final String levelId = MyAppUtils.OptString(props, "LEVEL_ID");
            levelName = app.nameForLevelId(levelId);
        } else {
            levelName = app.longNameForOrdinal(location.m_coord3D.m_ordinal);
        }

        ViewGroup ll = (ViewGroup)getLayoutInflater().inflate(R.layout.popup_callout, null);

        final ImageView iconView = ll.findViewById(R.id.pc_icon);
        final int iconId = MyAppUtils.Icon_(props, true);
        iconView.setImageResource(iconId);

        final TextView vTitle = ll.findViewById(R.id.pc_title);
        String titles[] = new String[2];
        m_manager.getTitleForLocation(location, titles);
        vTitle.setText(titles[0]);

        final TextView vSubTitle = ll.findViewById(R.id.pc_subtitle);
        String subtitle;
        if (category != null) {
            subtitle = String.format("%1$s\n%2$s", category, levelName);
        } else {
            subtitle = levelName;
        }
        vSubTitle.setText(subtitle);

        if (hours.isEmpty()) {
            final View vHoursLayout = ll.findViewById(R.id.pc_hours_layout);
            vHoursLayout.setVisibility(View.GONE);
            ll.findViewById(R.id.pc_sep1).setVisibility(View.GONE);
        } else {
            final TextView vHoursText = ll.findViewById(R.id.pc_hours_text);
            hours = hours.replaceAll(";\\s*", "\n");
            vHoursText.setText(hours);
        }

        final View vPhoneLayout = ll.findViewById(R.id.pc_phone_layout);
        if (phone.isEmpty()) {
            vPhoneLayout.setVisibility(View.GONE);
            ll.findViewById(R.id.pc_sep2).setVisibility(View.GONE);
        } else {
            final TextView vPhoneText = ll.findViewById(R.id.pc_phone_text);
            vPhoneText.setText(phone);
            final String phoneC = phone;
            vPhoneLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED))
                    {
                        ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.CALL_PHONE}, MY_REQUEST_PERMISSION_PHONE);
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);

                    intent.setData(Uri.parse("tel:" + phoneC));
                    try {
                        MapsActivity.this.startActivity(intent);
                    }
                    catch (SecurityException ex) {

                    }
                }
            });
        }

        if (webSite.isEmpty()) {
            final View vWebSiteLayout = ll.findViewById(R.id.pc_website_layout);
            vWebSiteLayout.setVisibility(View.GONE);
        } else {
            final TextView vWebSiteText = ll.findViewById(R.id.pc_website_text);
            vWebSiteText.setText(webSite);
        }

        final View directionsButton = ll.findViewById(R.id.pc_directions);
        if (!m_manager.canNavigate()) {
            directionsButton.setVisibility(View.GONE);
        }
        else {
            directionsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (m_popupDialog != null) {
                        m_popupDialog.dismiss();
                        m_popupDialog = null;
                    }
                    Intent intent = new Intent(MapsActivity.this, DirectionsActivity.class);
                    intent.putExtra(MyAppUtils.s_extra_end, location);
                    startActivityForResult(intent, s_requestCodeDirections);
                }
            });
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(ll);
        m_popupDialog = builder.create();

        Window window = m_popupDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        //wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        m_popupDialog.show();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setDimAmount(0.1f);
    }

    private SparseArray<Bitmap> m_bigDotBitmaps = new SparseArray<>();
    private SparseArray<Bitmap> m_smallDotBitmaps = new SparseArray<>();
    private Bitmap createDotBitmap(int size, int color) {
        final Bitmap bm = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bm);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        canvas.drawCircle(size/2, size/2 , size/2, paint);
        return bm;
    }

    private Bitmap dotBitmap(SparseArray<Bitmap> map, float sizeF, int color) {
        final float density = getResources().getDisplayMetrics().density;
        final int size = Math.round(sizeF * density);
        Bitmap bm = map.get(color);
        if (bm != null) {
            return bm;
        }
        bm = createDotBitmap(size, color);
        map.put(color, bm);
        return bm;
    }

    private Bitmap bigDotBitmap(int color) {
        return dotBitmap(m_bigDotBitmaps, 6.0f, color);
    }

    private Bitmap smallDotBitmap(int color) {
        return dotBitmap(m_smallDotBitmaps, 4.0f, color);
    }

    public void start(IMap map) {
        m_map = map;

        //final float density = getResources().getDisplayMetrics().density;
        //m_map.setCalloutFontSize(density * 12.f, density * 10.f);

        m_map.setCalloutListener(new IMap.CalloutListener() {
            @Override
            public View createCalloutView(IMarker marker) {
                // illustrate custom callout
                /*
                final Manager.Location location = marker.getLocation();
                Map<String, String> obj = m_manager.propsForFeature(location.m_featureIndex);
                final String title = Utils.OptString(obj, "NAME");
                final String subtitle = Utils.OptString(obj, "CATEGORY");

                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.setBackgroundColor(0xff00ff00);
                RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layout.setLayoutParams(llp);
                layout.setGravity(Gravity.CENTER_HORIZONTAL);

                TextView tvTitle = new TextView(context);
                tvTitle.setText(title);
                LinearLayout.LayoutParams llp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                tvTitle.setLayoutParams(llp1);
                layout.addView(tvTitle);

                if (subtitle.length() > 0) {
                    TextView tvSubtitle = new TextView(context);
                    tvSubtitle.setText(subtitle);
                    LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvSubtitle.setLayoutParams(llp2);
                    layout.addView(tvSubtitle);
                }

                layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        m_map.deselectMarker(m_selectedMarker);
                        MapsActivity.this.onCalloutDetailTapped(location);
                    }
                });

                return layout;
                */

                showPopup(marker);

                return null;
            }

            @Override
            public void onSelectedMarker(IMarker marker) {
                m_selectedMarker = marker;
            }

            @Override
            public void onDeselectedMarker(IMarker marker) {
                m_selectedMarker = null;
            }

            @Override
            public void onCalloutClick(IMarker marker) {
                MapsActivity.this.onCalloutDetailTapped(marker.getLocation());
            }
        });

        updateUserTrackingModeButton(m_map.getUserTrackingMode());
        m_manager = new Manager(map);
        m_manager.setOnChangedUserTrackingModeListener(new IMap.OnChangedUserTrackingModeListener() {
            @Override
            public void onChangedUserTrackingMode(int userTrackingMode) {
                updateUserTrackingModeButton(userTrackingMode);
            }
        });
        if (/*BuildConfig.isOverlay*/true)
        {
            m_manager.setGoogleApiKey(getString(R.string.google_maps_key));
        }
        m_manager.m_delegate = new Manager.Delegate() {
            public FeatureOptions getFeatureOptions(String mapLayer, Map<String, String> props)
            {
                //Log.e("Android Maps", "getFeatureOptions");

                final String category = MyAppUtils.OptString(props, "CATEGORY");
                final boolean selectable = mapLayer.equals("Units") && (category.equals("Elevator") || category.equals("Stairs") || category.equals("Escalator") || category.equals("Room") || category.startsWith("Restroom"));
                int priority = 9;
                switch (mapLayer) {
                    case "Occupants":
                        if (category.equals("Name")) {
                            priority = 58;
                        } else {
                            priority = 3;
                        }
                        break;
                    case "Openings":
                        priority = 7;
                        break;
                    case "Units":
                        priority = 100;
                        break;
                    case "Fixtures":
                        priority = 101;
                        break;
                    case "Zones":
                        priority = 102;
                        break;
                    case "Sections":
                        priority = 103;
                        break;
                }
                return new FeatureOptions(selectable, priority);
            }
            public void onLevelLoaded(int ordinal)
            {
                Log.e("Android Maps", "onLevelLoaded");

                MyApplication app = (MyApplication)getApplication();

                final String levelLongName = app.longNameForOrdinal(ordinal);
                TextView tv = findViewById(R.id.levelsLabel);
                tv.setText(levelLongName);
            }
            public IndoorPolylineOptions getPolylineOptions(String mapLayer, Map<String, String> props)
            {
                if (mapLayer.equals("Openings"))
                {
                    final float w = getResources().getDimension(R.dimen.d2);
                    return new IndoorPolylineOptions().width(w).color(0xffffffff);
                }
                return null;
            }
            public TextOptions getTextOptions(String mapLayer, Map<String, String> props, boolean forceNull[])
            {
                final float density = getResources().getDisplayMetrics().density;
                switch (mapLayer) {
                    case "Occupants": {
                        final float minZoomLevel = 18.f;
                        final float maxZoomLevel = 50.f;
                        final int padding = 0;
                        return new TextOptions(10.f * density, 0xff000000, 1, minZoomLevel, maxZoomLevel, 0, padding, padding, m_typeface);
                    }
                    case "Points": {
                        final String category = MyAppUtils.OptString(props, "CATEGORY");
                        if (category.equals("Pier")|| category.equals("Building")) {
                            final float minZoomLevel = 1.f;
                            final float maxZoomLevel = 16.f;
                            final int padding = Math.round(4 * density);
                            return new TextOptions(10.f * density, 0xffffffff, 5, minZoomLevel, maxZoomLevel, 0xff0b1761, padding, padding, m_typeface);
                        }
                        else if (category.equals("Gate Area")) {
                            final float minZoomLevel = 16.f;
                            final float maxZoomLevel = 35.f;
                            final int padding = Math.round(4 * density);
                            return new TextOptions(10.f * density, 0xffffffff, 5, minZoomLevel, maxZoomLevel, 0xff0b1761, padding, padding, m_typeface);
                        }
                        else if (category.equals("Baggage Carousel") || category.equals("Check-in") || category.equals("Transfer") || category.equals("SkyTeam Lounge")) {
                            final float minZoomLevel = 18.f;
                            final float maxZoomLevel = 35.f;
                            final int padding = 0;
                            return new TextOptions(10.f * density, 0xff000000, 1, minZoomLevel, maxZoomLevel, 0, padding, padding, m_typeface);
                        }
                        else if (category.equals("Check-in Area") || category.equals("Baggage Claim")) {
                            final float minZoomLevel = 10.f;
                            final float maxZoomLevel = 18.f;
                            final int padding = 0;
                            return new TextOptions(10.f * density, 0xff000000, 5, minZoomLevel, maxZoomLevel, 0, padding, padding, m_typeface);
                        }
                    }
                }
                forceNull[0] = true;
                return null;
            }
            /*
            public IconOptions getIconOptions(String mapLayer, Map<String, String> props)
            {
                final int bmId = MyAppUtils.Icon_(props, false);
                if (0 == bmId)
            }
            */
            public IconOptions[] getIconOptions(String mapLayer, Map<String, String> props)
            {
                if (props == null) {
                    return null;
                }
                final String category = MyAppUtils.OptString(props, "CATEGORY");
                final MyAppUtils.FeatureInfo featureInfo = MyAppUtils.GetFeatureInfo(category);
                final int bmId = featureInfo.m_bitmapId;
                if (0 == bmId)
                {
                    return null;
                }
                int minZoomLevel = 18;
                int maxZoomLevel = 50;
                final int priority = 3;
                IconOptions io1 = null, io2 = null;

                switch (featureInfo.m_category) {
                    case Escalator:
                    case Stairs:
                    case Elevator:
                        minZoomLevel = 18;
                        maxZoomLevel = 50;
                        break;
                    case Restroom:
                        minZoomLevel = 18;
                        maxZoomLevel = 50;
                        break;
                    case Baggage:
                    case Checkin:
                    case Transfer:
                        io1 = new IconOptions(bigDotBitmap(0xff0B1761), priority, 17, 18);
                        io2 = new IconOptions(smallDotBitmap(0xff0B1761), priority, 15, 17);
                        break;
                    case SkyTeamLounge:
                        minZoomLevel = 18;
                        maxZoomLevel = 50;
                        break;
                    case GateArea:
                        minZoomLevel = 1;
                        maxZoomLevel = 1;
                        break;
                    case Checkinarea:
                    case Baggageclaim:
                        minZoomLevel = 10;
                        maxZoomLevel = 18;
                        break;
                    case Security:
                        minZoomLevel = 10;
                        maxZoomLevel = 50;
                        break;
                    case Shopping:
                        io1 = new IconOptions(bigDotBitmap(0xffF4CE29), priority, 17, 18);
                        io2 = new IconOptions(smallDotBitmap(0xffF4CE29), priority, 15, 17);
                        break;

                    case Food:
                    case Drink:
                        io1 = new IconOptions(bigDotBitmap(0xffFFAD06), priority, 17, 18);
                        io2 = new IconOptions(smallDotBitmap(0xffF4CE29), priority, 15, 17);
                        break;
                }

                final IconOptions io0 = new IconOptions(getBitmap(bmId), priority, minZoomLevel, maxZoomLevel);
                if (io1 != null) {
                    return new IconOptions[]{io0, io1, io2};
                }
                return new IconOptions[]{io0};
            }

            public Manager.PinInfo getPinInfo(int color)
            {
                int bmId;
                switch (color)
                {
                    case 0: {
                        // this is blue dot
                        final Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.blue_dot);
                        return new Manager.PinInfo(bm, 0, 0, 0, 0);
                    }
                    case s_redPinColor:
                        bmId = R.drawable.pin_red;
                        break;
                    case s_greenPinColor:
                        bmId = R.drawable.pin_green;
                        break;
                    default:
                        Assert.assertEquals(color, s_purplePinColor);
                        bmId = R.drawable.pin_purple;
                }

                final Bitmap bm = BitmapFactory.decodeResource(getResources(), bmId);
                final int h = bm.getHeight();
                final float nW = 48.f;
                final float nH = 64.f;
                final float scale = (float)h / nH;
                return new Manager.PinInfo(bm, scale * (nW * 0.5f - 12.f), scale * (nH * 0.5f - 58.f), scale * 12.f, scale * 0.f);
            }

            public String getInstruction(List<Manager.FeatureParams> features, int ordinal)
            {
                MyApplication app = (MyApplication)getApplication();
                final String levelShortName = app.shortNameForOrdinal(ordinal);

                if (features != null && features.size() >= 1)
                {
                    Manager.FeatureParams p = features.get(0);
                    final String category = MyAppUtils.OptString(p.m_props, "CATEGORY");
                    return String.format("Take %1$s to level %2$s", category, levelShortName);
                }
                return String.format("Go to level %1$s", levelShortName);
            }

            public String getInstruction(Manager.Location destination)
            {
                String [] titles = new String[2];
                m_manager.getTitleForLocation(destination, titles);
                return String.format("Follow the route to %1$s", titles[0]);
            }

            public boolean isPointInsideVenue(Manager.Location location, boolean isDestination, boolean hint)
            {
                return hint;
            }

            public IndoorPolygonOptions getPolygonOptions(String mapLayer, Map<String, String> props)
            {
                final String category = MyAppUtils.OptString(props, "CATEGORY");
                switch (mapLayer) {
                    case "Levels": {
                        if (category.equals("Indoor")|| category.equals("Outdoor")) {
                            final float w = getResources().getDimension(R.dimen.d3);
                            return new IndoorPolygonOptions().fillColor(0xffffffff).strokeColor(0xffaaaaaa).strokeWidth(w).minZoomLevel(14).maxZoomLevel(35);
                        }
                        return null;
                    }
                    case "Sections": {
                        final String subcategory = MyAppUtils.OptString(props, "SUB_CATEGORY");
                        if (subcategory.equals("Security")) {
                            return new IndoorPolygonOptions().fillColor(0x7ff38e8e).minZoomLevel(14).maxZoomLevel(35).strokeColor(0);
                        }
                    }
                    case "Fixtures":
                        if (category.equals("F_Baggage Carousel") || category.equals("Check-in Desk") || category.equals("Kiosk")) {
                            final float w = getResources().getDimension(R.dimen.d2);
                            return new IndoorPolygonOptions().fillColor(0xffffffff).strokeColor(0xffd2e8f3).strokeWidth(w).minZoomLevel(18).maxZoomLevel(35);
                        }
                        return null;
                    case "Units": {
                        final float w = getResources().getDimension(R.dimen.d1);
                        IndoorPolygonOptions polygonOptions = new IndoorPolygonOptions().strokeColor(0xffffffff).strokeWidth(w);

                        switch (category) {
                            case "Walkway":
                                polygonOptions.fillColor(0xffffffff).minZoomLevel(14).maxZoomLevel(35);
                                break;
                            case "U_Elevator":
                            case "U_Stairs":
                            case "U_Escalator":
                            case "Moving Walkway":
                                polygonOptions.fillColor(0xffb8d5df).minZoomLevel(20).maxZoomLevel(35);
                                break;
                            case "Room":
                            case "Ramp":
                                polygonOptions.fillColor(0xffe6e6e6).strokeColor(0);
                                break;
                            case "Open to Below":
                                polygonOptions.fillColor(0xffd0caca).minZoomLevel(20).maxZoomLevel(35);
                                break;
                            case "Non-Public":
                                polygonOptions.fillColor(0xffece6d6);
                                break;
                            case "U_SkyTeam Lounge":
                                polygonOptions.fillColor(0xffd2e8f3);
                                break;
                        }
                        if (category.startsWith("U_Restroom")) {
                            polygonOptions.fillColor(0xffb8d5df);
                        }

                        return polygonOptions;
                    }
                }
                return null;
            }

            public void onLongPress(Manager.Location location)
            {
                createMark(location, s_purplePinColor, true, s_droppedPinId);
            }

            public void onShortClick(Mathe.MapPoint coordinate)
            {
            }
        };

        final String state = Environment.getExternalStorageState();
        if (!state.equals(Environment.MEDIA_MOUNTED))
        {
            Toast t = Toast.makeText(this, "Bad state = " + state, Toast.LENGTH_LONG);
            t.show();

            onFinishedDownload();

            return;
        }

        retryLoadMaps();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setIndoorEnabled(false);
        final UiSettings settings = googleMap.getUiSettings();
        settings.setIndoorLevelPickerEnabled(false);
        settings.setMapToolbarEnabled(false);
        settings.setCompassEnabled(false);
        //settings.setMyLocationButtonEnabled(false);
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_map_style));

        com.google.android.gms.maps.SupportMapFragment mapFragment = (com.google.android.gms.maps.SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        start(new MapImplGoogle(googleMap, mapFragment.getView(), getResources().getDisplayMetrics().density));
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        if (m_map != null) {
            final IndoorCameraPosition cp = m_map.getCameraPosition();
            bundle.putDouble(s_camPosLat, cp.target.latitude);
            bundle.putDouble(s_camPosLon, cp.target.longitude);
            bundle.putFloat(s_camPosZoom, cp.zoom);
            bundle.putFloat(s_camPosBearing, cp.bearing);
        }
    }

    @Override
    protected void onDestroy()
    {
        if (m_progressDialog != null) {
            m_progressDialog.dismiss();
            m_progressDialog = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        MyApplication app = (MyApplication)getApplication();
        app.m_googleLocationCallback = this;

        // when you are back from the SearchActivity animate the 'shrinking' of the Toolbar and
        // fade its contents back in
        fadeToolbarIn();

        // in case we are not coming here from the SearchActivity the Toolbar would have been already visible
        // so the above method has no effect
    }

    private void fadeToolbarIn() {
        TransitionManager.beginDelayedTransition(m_toolbar, FadeInTransition.createTransition());
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)m_toolbar.getLayoutParams();
        layoutParams.setMargins(m_toolbarMargin, m_toolbarMargin, m_toolbarMargin, m_toolbarMargin);
        m_toolbar.showContent();
        m_toolbar.setLayoutParams(layoutParams);
    }

    @Override
    protected void onPause()
    {
        MyApplication app = (MyApplication)getApplication();
        app.m_googleLocationCallback = null;
        super.onPause();
    }

    @Override
    public void onLocation()
    {
        if (m_wantHaveRoute && (m_manager.markLocation(s_startPinId) == null || m_manager.markLocation(s_endPinId) == null))
        {
            recalcRoute(false);
        }
        final MyApplication app = (MyApplication)getApplication();
        float[] bearing = {0};
        final Manager.Location location = app.getUserLocation(bearing);
        if (/*!BuildConfig.isOverlay*/true)
        {
            m_manager.setUserLocation(location, bearing[0]);
        }
    }
}
