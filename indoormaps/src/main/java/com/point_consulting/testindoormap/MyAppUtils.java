package com.point_consulting.testindoormap;

import android.app.Activity;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.inputmethod.InputMethodManager;

import com.point_consulting.pc_indoormapoverlaylib.Manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

final class MyAppUtils {

    final static String s_searchField = "CATEGORY";

    final static String s_extra_featuresSet = "s_extra_featuresSet";
    final static String s_extra_location = "s_extra_location";
    final static String s_extra_propsMap = "s_extra_propsMap";
    final static String s_extra_color = "s_extra_color";
    final static String s_extra_logo = "s_extra_logo";
    final static String s_extra_start = "s_extra_start";
    final static String s_extra_end = "s_extra_end";

    final static String s_selDestResult = "s_selDestResult";

    final static String SUBTITLE_FIELD = "CATEGORY";

    static String OptString(Map<String, String> map, String key)
    {
        final String value = map.get(key);
        if (null == value)
        {
            return "";
        }
        return value;
    }

    final static class PropDesc implements Parcelable
    {
        int m_id;
        String m_string;

        PropDesc(int id, String string)
        {
            m_id = id;
            m_string = string;
        }

        PropDesc(Parcel p)
        {
            m_id = p.readInt();
            m_string = p.readString();
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeInt(m_id);
            dest.writeString(m_string);
        }

        public static final Parcelable.Creator<PropDesc> CREATOR = new Parcelable.Creator<PropDesc>() {
            @Override
            public PropDesc createFromParcel(Parcel source) {
                return new PropDesc(source);
            }

            @Override
            public PropDesc[] newArray(int size) {
                return new PropDesc[size];
            }
        };
    }

    enum FeatureCategory {
        Other,
        Gate,
        GateArea,
        U_Restroom,
        U_Elevator,
        U_Stairs,
        U_Escalator,
        Restroom,
        Elevator,
        Stairs,
        Escalator,
        Room,
        Name,
        Entry,
        Indoor,
        Outdoor,
        Walkway,
        NonPublic,
        TransitPlatform,
        Ramp,
        OpenToBelow,
        Atm,
        LostAndFound,
        Baggage,
        F_Baggage,
        Baggageclaim,
        Security,
        Ticketing,
        WheelchairAssist,
        Checkin,
        Pet,
        Lounge,
        Shopping,
        Food,
        Drink,
        Medical,
        Financial,
        Entertainment,
        Services,
        CarRental,
        Checkindesk,
        Checkinarea,
        Kiosk,
        Pier,
        Info,
        MovingWalkway,
        Conference,
        SkyTeamLounge,
        U_SkyTeamLounge,
        Transfer
    };

    static class FeatureInfo {
        FeatureCategory m_category;
        int m_bitmapId;
        FeatureInfo(FeatureCategory category, int bitmapId) {
            m_category = category;
            m_bitmapId = bitmapId;
        }
    };

    static FeatureInfo GetFeatureInfo(String category) {
        switch (category) {
            case "Accessories":
            case "Appliances":
            case "Art Galleries":
            case "Auto Parts & Supplies":
            case "Automotive":
            case "Books, Mags, Music & Video":
            case "Cards & Stationery":
            case "Children's Clothing":
            case "Cosmetics & Beauty Supply":
            case "Department Stores":
            case "Electronics":
            case "Eyewear & Opticians":
            case "Fashion":
            case "Florists":
            case "Furniture Stores":
            case "Grocery":
            case "Hobby Shops":
            case "Home & Garden":
            case "Jewelry":
            case "Lingerie":
            case "Luggage":
            case "Men's Clothing":
            case "Nurseries & Gardening":
            case "Pet Stores":
            case "Photography Stores & Services":
            case "Shoe Store":
            case "Shoe Stores":
            case "Shopping":
            case "Sporting Goods":
            case "Toy Stores":
            case "Watches":
            case "Women's Clothing":
            case "Pharmacy":
            case "Health & Medical":
                return new FeatureInfo(FeatureCategory.Shopping, R.drawable.icon_small_shopping);
            case "Animal Relief Area":
            case "Pet Relief Area":
                return new FeatureInfo(FeatureCategory.Pet, R.drawable.u_icon_small_pet);
            case "Restroom":
                return new FeatureInfo(FeatureCategory.Restroom, R.drawable.u_icon_small_toilets);
            case "Restroom (Male)":
                return new FeatureInfo(FeatureCategory.Restroom, R.drawable.u_icon_small_toilets);
            case "Restroom (Female)":
                return new FeatureInfo(FeatureCategory.Restroom, R.drawable.u_icon_small_toilets);
            case "U_Restroom":
                return new FeatureInfo(FeatureCategory.U_Restroom, 0);
            case "Car Rental":
                return new FeatureInfo(FeatureCategory.CarRental, R.drawable.u_icon_small_car_rental);
            case "Travel Services":
                return new FeatureInfo(FeatureCategory.Services, R.drawable.u_icon_small_travels);
            case "Elevator":
                return new FeatureInfo(FeatureCategory.Elevator, R.drawable.u_icon_small_lift);
            case "Escalator":
                return new FeatureInfo(FeatureCategory.Escalator, R.drawable.u_icon_small_escalator);
            case "Stairs":
                return new FeatureInfo(FeatureCategory.Stairs, R.drawable.u_icon_small_stairs);
            case "Baggage Carousel":
                return new FeatureInfo(FeatureCategory.Baggage, R.drawable.u_icon_small_baggage);
            case "Baggage Claim":
                return new FeatureInfo(FeatureCategory.Baggageclaim, R.drawable.u_icon_small_baggage);
            case "Security Checkpoint":
                return new FeatureInfo(FeatureCategory.Security, R.drawable.u_icon_small_security);
            case "Security":
                return new FeatureInfo(FeatureCategory.Security, 0);
            case "Check-in":
                return new FeatureInfo(FeatureCategory.Checkin, R.drawable.u_icon_small_checkin);
            case "Check-in Area":
                return new FeatureInfo(FeatureCategory.Checkinarea, R.drawable.u_icon_small_checkin);
            case "Cafes":
            case "Coffee & Tea":
            case "Cafeteria":
            case "Beer, Wine & Spirits":
                return new FeatureInfo(FeatureCategory.Drink, R.drawable.u_icon_small_coffee);
            case "Specialty Food":
            case "Restaurants":
            case "Eating/Drinking":
                return new FeatureInfo(FeatureCategory.Food, R.drawable.u_icon_small_restaurant);
            case "SkyTeam Lounge":
                return new FeatureInfo(FeatureCategory.SkyTeamLounge, R.drawable.u_icon_small_lounge);
            case "U_SkyTeam Lounge":
                return new FeatureInfo(FeatureCategory.U_SkyTeamLounge, 0);
            case "Gate Area":
                return new FeatureInfo(FeatureCategory.GateArea, R.drawable.u_icon_small_gate);
            case "Transfer":
                return new FeatureInfo(FeatureCategory.Transfer, R.drawable.u_icon_small_transfer_desk);
        }
        return new FeatureInfo(FeatureCategory.Other, 0);
    }

    static int Icon_(Map<String, String> props, boolean inSearch)
    {
        if (props == null) {
            return inSearch ? R.drawable.u_icon_other : 0;
        }
        final String category = OptString(props, "CATEGORY");
        final int retVal = GetFeatureInfo(category).m_bitmapId;
        return retVal>0 ? retVal : (inSearch ? R.drawable.u_icon_other : 0);
    }

    static List<Manager.FeatureDesc> FilterOut_(final Manager manager, final List<Manager.FeatureDesc> fdl1)
    {
        if (fdl1 == null) {
            return null;
        }
        final List<Manager.FeatureDesc> fdl = new ArrayList<>(fdl1.size());
        for (Manager.FeatureDesc fd : fdl1) {
            final String mapLayer = manager.mapLayerForFeature(fd.m_featureIndex);
            if (mapLayer.equals("Units")) {
                final Map<String, String> props = manager.propsForFeature(fd.m_featureIndex);
                final String category = MyAppUtils.OptString(props, "CATEGORY");
                if (!category.startsWith("Restroom")) {
                    continue;
                }
            }
            fdl.add(fd);
        }

        Collections.sort(fdl, new Comparator<Manager.FeatureDesc>() {
            @Override
            public int compare(Manager.FeatureDesc o1, Manager.FeatureDesc o2) {
                return o1.m_name.compareTo(o2.m_name);
            }
        });

        return fdl;
    }

    static void HideKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    static void ShowKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }
}
