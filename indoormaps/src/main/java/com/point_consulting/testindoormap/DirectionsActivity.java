package com.point_consulting.testindoormap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.point_consulting.pc_indoormapoverlaylib.Coordinate3D;
import com.point_consulting.pc_indoormapoverlaylib.Manager;

import junit.framework.Assert;

import java.util.Map;

public class DirectionsActivity extends AppCompatActivity {

    private static class SItem
    {
        SItem(Manager.Location location)
        {
            m_location = location;
        }
        Manager.Location m_location;
    }

    private SItem m_selectedStart;
    private SItem m_selectedEnd;
    private Manager m_manager;

    private class DTextWatcher extends MySearch.STextWatcher {
        DTextWatcher(MyApplication app, EditText et) {super(app, et, m_search);}
        @Override
        Coordinate3D mtw_c3dForSearch() {
            if (m_selectedStart != null && m_selectedStart.m_location != null)
            {
                return m_selectedStart.m_location.m_coord3D;
            }
            if (m_selectedEnd != null && m_selectedEnd.m_location != null)
            {
                return m_selectedEnd.m_location.m_coord3D;
            }
            final Manager.Location userLocation = ((MyApplication)getApplication()).getUserLocation(null);
            return userLocation != null ? userLocation.m_coord3D : null;
        }
        @Override
        void mtw_onTextChanged(EditText editText) {
            if (editText == m_fromEditText)
            {
                m_selectedStart = null;
            }
            else
            {
                Assert.assertTrue(editText == m_toEditText);
                m_selectedEnd = null;
            }
            updateButtonEnabling();
        }
    }

    private class DSearch extends MySearch {
        DSearch(Activity activity, ListView table) {
            super(activity, table);
        }
        @Override
        void ms_onLocation(Manager.Location location, Map<String, Object> m) {
            if (m_fromEditText.isFocused())
            {
                m_fromEditText.removeTextChangedListener(m_fromTextWatcher);
                m_fromEditText.setText((String)m.get(MySearch.s_titleStr));
                m_fromEditText.addTextChangedListener(m_fromTextWatcher);
                m_selectedStart = new DirectionsActivity.SItem(location);
            }
            else
            {
                Assert.assertTrue(m_toEditText.isFocused());
                m_toEditText.removeTextChangedListener(m_toTextWatcher);
                m_toEditText.setText((String)m.get(MySearch.s_titleStr));
                m_toEditText.addTextChangedListener(m_toTextWatcher);
                m_selectedEnd = new DirectionsActivity.SItem(location);
            }
            updateButtonEnabling();
        }
    }

    private DTextWatcher m_fromTextWatcher;
    private DTextWatcher m_toTextWatcher;
    private EditText m_fromEditText;
    private EditText m_toEditText;
    private DSearch m_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);

        MyApplication app = (MyApplication)getApplication();
        m_manager = app.m_manager;

        ListView table = findViewById(R.id.directions_table);
        m_search = new DSearch(this, table);

        m_fromEditText = findViewById(R.id.directions_from);
        m_toEditText = findViewById(R.id.directions_to);

        final Intent intent = getIntent();

        final Manager.Location locStart = intent.getParcelableExtra(MyAppUtils.s_extra_start);
        if (locStart == null) {
//            final String curLocStr = getString(R.string.current_location);
//            if (!curLocStr.isEmpty()) {
//                m_selectedStart = new SItem(null);
//                m_fromEditText.setText(curLocStr);
//            }
        }
        else
        {
            m_selectedStart = new SItem(locStart);
            final String[] titles = new String[2];
            m_manager.getTitleForLocation(locStart, titles);
            m_fromEditText.setText(titles[0]);
        }

        final Manager.Location locEnd = intent.getParcelableExtra(MyAppUtils.s_extra_end);
        if (locEnd != null) {
            m_selectedEnd = new SItem(locEnd);
            final String[] titles = new String[2];
            m_manager.getTitleForLocation(locEnd, titles);
            m_toEditText.setText(titles[0]);
        }

        updateButtonEnabling();

        m_fromTextWatcher = new DTextWatcher(app, m_fromEditText);
        m_toTextWatcher = new DTextWatcher(app, m_toEditText);

        m_search.update(true);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (m_selectedStart == null)
        {
            m_fromEditText.requestFocus();
        }
        else if (m_selectedEnd == null)
        {
            m_toEditText.requestFocus();
        }
    }

    private void updateButtonEnabling()
    {
        final Button b = findViewById(R.id.directions_route);
        b.setEnabled(m_selectedStart != null && m_selectedEnd != null);
    }

    public void onSwap(View view)
    {
        final SItem tmp = m_selectedEnd;
        m_selectedEnd = m_selectedStart;
        m_selectedStart = tmp;

        final String t1 = m_fromEditText.getText().toString();
        m_fromEditText.removeTextChangedListener(m_fromTextWatcher);
        m_toEditText.removeTextChangedListener(m_toTextWatcher);
        m_fromEditText.setText(m_toEditText.getText().toString());
        m_toEditText.setText(t1);
        m_fromEditText.addTextChangedListener(m_fromTextWatcher);
        m_toEditText.addTextChangedListener(m_toTextWatcher);

        if (m_fromEditText.hasFocus())
        {
            m_toEditText.requestFocus();
        }
        else if (m_toEditText.hasFocus())
        {
            m_fromEditText.requestFocus();
        }
    }

    private void done(final Manager.Location startLocation, final Manager.Location endLocation)
    {
        final Intent intent = new Intent();
        intent.putExtra(MyAppUtils.s_extra_start, startLocation);
        intent.putExtra(MyAppUtils.s_extra_end, endLocation);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onRoute(View view)
    {
        final CheckBox cb = findViewById(R.id.directions_checkbox);
        if (!m_manager.setNavIndex(cb.isChecked() ? 1 : 0)) {
            return;
        }

        if (m_selectedStart != null && m_selectedEnd != null) {
            final Manager.Location startLocation = m_selectedStart.m_location;
            final Manager.Location endLocation = m_selectedEnd.m_location;
            final String pStart = startLocation != null ? startLocation.m_placeId : null;
            final String pEnd = endLocation != null ? endLocation.m_placeId : null;
            int c = 0;
            if (pStart != null)
            {
                ++c;
            }
            if (pEnd != null)
            {
                ++c;
            }
            if (c > 0)
            {
                int k = 0;
                final String[] ar = new String[c];
                if (pStart != null)
                {
                    ar[k++] = pStart;
                }
                if (pEnd != null)
                {
                    ar[k++] = pEnd;
                }
                MyApplication app = (MyApplication)getApplication();
                PendingResult<PlaceBuffer> pbuf = Places.GeoDataApi.getPlaceById(app.getGoogleApiClient(), ar);
                pbuf.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        int k = 0;
                        Manager.Location newStartLocation = startLocation;
                        Manager.Location newEndLocation = endLocation;
                        for (Place place : places)
                        {
                            final LatLng latLng = place.getLatLng();
                            final Coordinate3D coord3d = new Coordinate3D(latLng.latitude, latLng.longitude, 0);
                            final String placeName = place.getName().toString();
                            if (ar[k] == pStart)
                            {
                                newStartLocation = new Manager.Location(null, -1, coord3d, pStart, placeName);
                            }
                            else if (ar[k] == pEnd)
                            {
                                newEndLocation = new Manager.Location(null, -1, coord3d, pEnd, placeName);
                            }
                            ++k;
                        }
                        places.release();
                        done(newStartLocation, newEndLocation);
                    }
                });
            }
            else {
                done(startLocation, endLocation);
            }
        }
    }
}
