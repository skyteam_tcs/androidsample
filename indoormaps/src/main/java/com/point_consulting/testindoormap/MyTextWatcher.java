package com.point_consulting.testindoormap;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.point_consulting.pc_indoormapoverlaylib.Coordinate3D;
import com.point_consulting.pc_indoormapoverlaylib.Manager;

import java.util.List;

abstract class MyTextWatcher implements TextWatcher
{
    abstract Coordinate3D mtw_c3dForSearch();
    abstract void mtw_onIndoorFeatures(List<Manager.FeatureDesc> list);
    abstract void mtw_onOutdoorFeatures(List<MyApplication.OutdoorFeature> list);
    abstract void mtw_onEmptySearch();
    abstract void mtw_onTextChanged(EditText editText);

    private static final int s_minStrLen = 2;
    private MyApplication m_application;
    private EditText m_editText;

    MyTextWatcher(MyApplication application, EditText editText) {
        m_application = application;
        m_editText = editText;
        m_editText.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        final int len = charSequence.length();
        if (len >= s_minStrLen)
        {
            final Manager manager = m_application.m_manager;
//                final boolean c = getString(R.string.current_location).toLowerCase().contains(charSequence.toString().toLowerCase());
//                if (c != m_hasCurrentLocationRequest)
//                {
//                    m_hasCurrentLocationRequest = c;
//                    update(false);
//                }

            final String s = charSequence.toString();
            if (manager != null)
            {
                manager.searchAsync(s, MyAppUtils.s_searchField, mtw_c3dForSearch(), new Manager.SearchCallback() {
                    @Override
                    public void onSearchDone(List<Manager.FeatureDesc> list) {
                        mtw_onIndoorFeatures(MyAppUtils.FilterOut_(manager, list));
                    }
                });
            }

            m_application.searchOutdoorAsync(s, new MyApplication.SearchCallback() {
                @Override
                public void onOutdoorSearchDone(List<MyApplication.OutdoorFeature> features) {
                    mtw_onOutdoorFeatures(features);
                }
            });
        }
        else
        {
            mtw_onEmptySearch();
        }
        mtw_onTextChanged(m_editText);
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
