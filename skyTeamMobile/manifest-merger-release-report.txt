-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:installLocation
		ADDED from AndroidManifest.xml:6:5
uses-sdk
ADDED from AndroidManifest.xml:8:5
MERGED from AndroidStudioWorkSpace_Tag:menudrawer:unspecified:7:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:4.2.42:7:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:10:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:9:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
permission#org.skyteam.permission.MAPS_RECEIVE
ADDED from AndroidManifest.xml:12:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:14:9
	android:name
		ADDED from AndroidManifest.xml:13:9
uses-feature#0x00020000
ADDED from AndroidManifest.xml:16:5
	android:required
		ADDED from AndroidManifest.xml:18:9
	android:glEsVersion
		ADDED from AndroidManifest.xml:17:9
uses-permission#org.skyteam.permission.MAPS_RECEIVE
ADDED from AndroidManifest.xml:20:5
	android:name
		ADDED from AndroidManifest.xml:20:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:21:5
	android:name
		ADDED from AndroidManifest.xml:21:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:23:5
	android:name
		ADDED from AndroidManifest.xml:23:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:24:5
	android:name
		ADDED from AndroidManifest.xml:24:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:25:5
	android:name
		ADDED from AndroidManifest.xml:25:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:26:5
	android:name
		ADDED from AndroidManifest.xml:26:22
uses-permission#android.permission.GET_TASKS
ADDED from AndroidManifest.xml:27:5
	android:name
		ADDED from AndroidManifest.xml:27:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:28:5
	android:name
		ADDED from AndroidManifest.xml:28:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:29:5
	android:name
		ADDED from AndroidManifest.xml:29:22
application
ADDED from AndroidManifest.xml:32:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:36:9
	android:allowBackup
		ADDED from AndroidManifest.xml:33:9
	android:icon
		ADDED from AndroidManifest.xml:35:9
	android:theme
		ADDED from AndroidManifest.xml:37:9
	android:hardwareAccelerated
		ADDED from AndroidManifest.xml:34:9
activity#org.skyteam.activities.SkyteamWebviewActivity
ADDED from AndroidManifest.xml:38:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:42:13
	android:screenOrientation
		ADDED from AndroidManifest.xml:40:13
	android:theme
		ADDED from AndroidManifest.xml:41:13
	android:name
		ADDED from AndroidManifest.xml:39:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:43:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:44:17
	android:name
		ADDED from AndroidManifest.xml:44:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:46:17
	android:name
		ADDED from AndroidManifest.xml:46:27
activity#org.skyteam.activities.SkyTeamMapActivity
ADDED from AndroidManifest.xml:49:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:52:13
	android:screenOrientation
		ADDED from AndroidManifest.xml:53:13
	android:label
		ADDED from AndroidManifest.xml:51:13
	android:name
ADDED from AndroidManifest.xml:55:9
	android:configChanges
		ADDED from AndroidManifest.xml:56:13
	android:hardwareAccelerated
		ADDED from AndroidManifest.xml:57:13
	android:name
ADDED from AndroidManifest.xml:58:7
	android:exported
		ADDED from AndroidManifest.xml:60:11
	android:enabled
		ADDED from AndroidManifest.xml:59:11
	android:name
		ADDED from AndroidManifest.xml:58:17
intent-filter#com.android.vending.INSTALL_REFERER
ADDED from AndroidManifest.xml:62:13
action#com.android.vending.INSTALL_REFERER
ADDED from AndroidManifest.xml:62:29
	android:name
		ADDED from AndroidManifest.xml:62:37
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:65:9
	android:name
		ADDED from AndroidManifest.xml:66:13
	android:value
		ADDED from AndroidManifest.xml:67:13
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:70:9
	android:name
		ADDED from AndroidManifest.xml:71:13
	android:value
		ADDED from AndroidManifest.xml:72:13
