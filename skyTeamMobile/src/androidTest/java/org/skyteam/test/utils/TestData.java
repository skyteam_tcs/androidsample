package org.skyteam.test.utils;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TestData {
    //TODO:Need to change success spelling writing generic
    public static final String EXPECTED_RESULT_SUCCESS ="sucess";
    public static final String EXPECTED_RESULT_FAILURE="failure";
    public static final String EXPECTED_RESULT_DUPLICATE ="duplicate";

    //Insert AIrport Data Success
    public static final String AIRPORT_SAVE_SUCCESS =
            "{\"Airport_Name\":\"New York John F Kennedy International\",\"Location_Code\":\"JFK\",\"Country_Code\":\"US\",\"City_Name\":\"New York\",\"AirportDetails_URL\":\"https://services.staging.skyteam.com/airport?source=Mobile&airportcode=JFK&city=New York&countrycode=US&version=2\",\"Country_Name\":\"USA\",\"LocalTime\":\"01:49 GMT-5:00\",\"Temperature\":\"-14.91\",\"WeatherCode\":\"02n\",\"LoungesCount\":\"5\",\"SkyTipsCount\":\"20\",\"AirlinesCount\":\"undefined\"}";

    public static final String DEFAULT_SETTINGS=
            "24h,miles,fahrenheit,DD,on,En,NO";

    public static String formSettingsJson(){
        JSONObject result = new JSONObject();
        JSONArray settingsArr = new JSONArray();

        try {
            result.put("Time", "24h");
            result.put("Distance", "miles");
            result.put("Temperature", "celsius");
            result.put("Date",  "DD");
            result.put("SkyTips",  "on");
            result.put("Language", "Es");
            result.put("InitialLoad", "NO");

            return settingsArr.put(result).toString();
        }catch(Exception e){

        }
        return "";

    }

    public static final String EXPECTED_SETTINGS_DISTANCE ="miles";

    public static String formLoungeDataAirport(){

        JSONObject result = new JSONObject();

        try {
            result.put("airportCode", "AMS");
            result.put("airportName", "Amsterdam Schiphol (AMS), Netherlands");
            result.put("url", "https://services.staging.skyteam.com/lounges?airportcode=AMS");


            return result.toString();
        }catch(Exception e){

        }
        return "";
    }
    //SkyTeamMember Handler
    public static final String SKYTEAMMEMBER_AZ = "AZLogo";
    public static final String SKYTEAMMEMBER_EXPECTED_RESULT="member";

    //LoungeFinderHandler
    public static final String LOUNGEFINDER_EXPECTEDRESULT = "AMS";
    public static final String LOUNGEFINDER_QUERYSTRING= "AMS";
    public static final int LOUNGEFINDER_CORRECT_RESPONSE=1;
    public static final int LOUNGEFINDER_ERROR_RESPONSE=2;

    //SkyTips

    public static final String FILE_SKYTIPS_VALID_JSON = "SkyTipsHandler_json";
    public static final String FILE_SKYTIPS_INVALID_JSON = "SkyTipsHandler_partial_json";
    public static final String ERR_DEV_INACTIVE = "Developer_Inactive";

    public static String formSkyTips_ErrorMessage(){

        JSONObject result = new JSONObject();

        try {
            result.put("ErrorCode", "3303");
            result.put("ErrorMessage", "No Airport SkyTips Found");
            return result.toString();
        }catch(Exception e){

        }
        return "";
    }

    public static final String ERR_MSG="ERROR";

    //Flight Handler

    public static String fetchTestData(Context context, String Name){

        StringBuilder strTestData=new StringBuilder();
        try {
            InputStream is= context.getAssets().open(Name);
            BufferedReader bufreader=new BufferedReader(new InputStreamReader(is));
            String line="";
            while((line =bufreader.readLine() )!= null){
                strTestData.append(line);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return strTestData.toString();

    }

    public static InputStream fetchTestDataStream(Context context, String Name){

        InputStream is= null;
        try {
            is= context.getAssets().open(Name);


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d("FOTEST",""+ e.getMessage());
        }

        return is;

    }

    //AirportArrayAdapterTest

    public static final String CITY_JSON_FILE ="CityAirportListJsonTest";
    public static final String EXPECTED_AIRPORT_CODE ="ABA";

    //SkyTeamWebViewActivity

    public static final String EXPECTED_URL_HOMEPAGE="file:///android_asset/html/index.html#homePage";

}
