package org.skyteam.test.activities;

import android.app.Instrumentation;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.skyteam.activities.SkyTeamMapActivity;
import org.skyteam.data.AirportDeserilizer;
import org.skyteam.data.AirportDetails;
import org.skyteam.data.AirportsSearch;
import org.skyteam.fragments.AirportMapFragment;
import org.skyteam.fragments.InfoDialogFragment;
import org.skyteam.test.utils.TestData;
import org.skyteam.view.SideMenuView;

import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by DELL on 3/14/2015.
 */
public class SkyTeamMapActivityTests extends ActivityInstrumentationTestCase2<SkyTeamMapActivity> {

    private SkyTeamMapActivity mActivity;
    private Instrumentation mInstrumentation;
    private SideMenuView mSideMenuView;
    private Context mInstrumentationContext;
    public SkyTeamMapActivityTests() {

        super(SkyTeamMapActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mActivity = (SkyTeamMapActivity)getActivity();
        mInstrumentation= this.getInstrumentation();
        mInstrumentationContext= this.getInstrumentation().getContext();
    }

    public void testActivityNotNull()  {
        assertNotNull(mActivity);
    }

   public void testInfoDialogFragment()  {
        InfoDialogFragment dialog = InfoDialogFragment.newInstance("test",
                1, 0, 0);
        dialog.show(mActivity.getSupportFragmentManager(), "test");

        assertNotNull(dialog);
    }

    @UiThreadTest
    public void testMenuDrawer(){
        Typeface mFaceTnb = Typeface.createFromAsset(mActivity.getAssets(),
                "html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf");
        mSideMenuView =new SideMenuView(mActivity,"En");
      //  assertNotNull(mMenuDrawer);
    }

    public void testMapfragment() throws  UnsupportedEncodingException {
        AirportMapFragment mapFrag = (AirportMapFragment) mActivity.getSupportFragmentManager()
                .findFragmentByTag("MAP");
        List<Fragment> list =mActivity.getSupportFragmentManager().getFragments();
        assertNotNull(mapFrag);
        //mapFrag.setData(getTestData());
        //assertTrue(mapFrag..getCount()>0);

    }

    private List<AirportDetails> getTestData() throws UnsupportedEncodingException {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(
                        FieldNamingPolicy.UPPER_CAMEL_CASE)
                .registerTypeAdapter(AirportsSearch.class,
                        new AirportDeserilizer()).create();
        InputStreamReader ism = new InputStreamReader(TestData.fetchTestDataStream(mInstrumentationContext, "MapNearByAirport_json"),
                "UTF-8");

        AirportsSearch search = gson.fromJson(ism,
                AirportsSearch.class);
        List<AirportDetails> list = search.getAirportDetails();
        return list;

    }




}
