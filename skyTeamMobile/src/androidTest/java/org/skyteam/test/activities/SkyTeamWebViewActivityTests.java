package org.skyteam.test.activities;

import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.database.DBHelper;

import java.io.File;

public class SkyTeamWebViewActivityTests extends
        ActivityInstrumentationTestCase2<SkyteamWebviewActivity> {

	private static String CLASS_NAME = "org.skyteam.activities.SkyteamWebviewActivity";
	private SkyteamWebviewActivity mActivity;
	public static final String PREFS_NAME = "MyPrefsFile";

	private static final String VALID_INDEX_URL = "file:///android_asset/html/index.html";
	private static final String INVALID_INDEX_URL = "file:///android_asset/html/index11.html";
	private WebView mWebView;
	private WebViewClient mMockWebViewClient;
	private static Class activity_class;
    private Instrumentation mInstrumentation;
    private String urlresult = null;



	public SkyTeamWebViewActivityTests() {
		super("org.skyteam.test",SkyteamWebviewActivity.class);
		setName("SkyteamWebviewActivity");

	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
        mActivity = (SkyteamWebviewActivity)getActivity();
        mInstrumentation= this.getInstrumentation();
	}


	private String assertUrlLoading(String url) throws Throwable {


		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			fail("Unexpected timeout");
		}


		mActivity.loadURLinUIThread(url);
        getInstrumentation().waitForIdleSync();

        runTestOnUiThread(new Runnable() {

            @Override
            public void run() {


		mWebView = getActivity().getWebView();
		        urlresult = mWebView.getUrl();

		//assertEquals(mWebView.getUrl(), VALID_INDEX_URL);
		// assertTrue(!(mWebView.getProgress() < 100));
            }
        });



        return urlresult;

	}



    public void testOnNewIntentShouldLoadHome() throws Throwable {

        final Intent intent = new Intent();
        intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE,true);
        intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE,"homePage");


        //this.getInstrumentation().callActivityOnNewIntent(mActivity,intent);


        runTestOnUiThread(new Runnable() {

            @Override
            public void run() {

                mInstrumentation.callActivityOnNewIntent(mActivity, intent);
                mWebView = getActivity().getWebView();
                //assertEquals(TestData.EXPECTED_URL_HOMEPAGE, mWebView.getUrl());

            }
        });

    }

    public void testExitSplashShouldDisplayWebView(){
        mActivity.exitSplash();
        View view = mActivity.getWindow().getDecorView().getRootView();
        assertTrue(view.findViewById(org.skyteam.R.id.wv_skyteam) instanceof WebView);

    }

    public void testCheckLanguageShouldExists(){

        assertTrue(mActivity.checkLanguageExists());

    }

    public void testInitialLoadString(){
      String loadString= mActivity.getInitialLoadString();
        //Log.d("WEEE",""+loadString);
        assertTrue(loadString.contains("24h"));
    }

    public void testActivityNotNull()  {

        assertNotNull(mActivity);


    }

	public void  testLoadValidIndexShouldPassUrl() throws Throwable {

        //assertEquals(VALID_INDEX_URL,assertUrlLoading(VALID_INDEX_URL) );



	}

	public void testSettingsFileShouldExist() {


		File fileSettings = mActivity
				.getFileStreamPath(DBHelper.SettingsJSONFileName);
		assertTrue(fileSettings.exists());
	}

	public final void testLoadInvalidIndexshouldShowDefaultUrl() throws Throwable {


        //TODO

		assertEquals(INVALID_INDEX_URL, assertUrlLoading(INVALID_INDEX_URL));
		Log.d("ACTTEST", "invalid : " + INVALID_INDEX_URL);

	}


/*public void testgetCityAirportLangJSON() {

		JSONObject jsonObject = mActivity.getCityAirportLangJSON();
		try {
			assertEquals("Success", jsonObject.get("Result"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/












	/*public void atestgetAllLoungeJsonAsync() throws Throwable {
		// create CountDownLatch for which the test can wait.
		final CountDownLatch latch = new CountDownLatch(1);
		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		runTestOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					mActivity.updateAllLounges();
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yy");
					// get current date time with Date()
					Date date = new Date();
					String strToday = dateFormat.format(date);
					Log.d("ACTTEST",
							"CAL TIME : " + dateFormat.format(cal.getTime()));

					SharedPreferences settings = mActivity
							.getSharedPreferences(PREFS_NAME,
									mActivity.MODE_PRIVATE);
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy",
							Locale.US);
					Date today;
					today = sdf.parse(dateFormat.format(date));
					Log.d("ACTTEST", "Todays date str : " + strToday);

					Calendar savedCal = Calendar.getInstance();
					savedCal.setTimeInMillis(settings.getLong(
							"LoungesUpdationDate", 0));
					String savedDay = String.valueOf(savedCal
							.get(Calendar.DAY_OF_MONTH));
					String sMonth = String
							.valueOf(savedCal.get(Calendar.MONTH));
					String sYear = String.valueOf(savedCal.get(Calendar.YEAR));
					SimpleDateFormat sdfSaved = new SimpleDateFormat(
							"dd/MM/yy", Locale.US);
					Date savedDate;
					Log.d("ACTTEST", "savedDate" + savedDay + "/" + sMonth
							+ "/" + sYear);

					savedDate = sdf
							.parse(savedDay + "/" + sMonth + "/" + sYear);
					assertEquals(today, savedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	public void atestAllLoungeJsonAsyncErrorShouldNotUpdateDate()
			throws Throwable {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		runTestOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					new GetAllLoungeJsonAsyncStub().execute();
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yy");
					// get current date time with Date()
					Date date = new Date();
					String strToday = dateFormat.format(date);
					Log.d("ACTTEST",
							"CAL TIME : " + dateFormat.format(cal.getTime()));

					SharedPreferences settings = mActivity
							.getSharedPreferences(PREFS_NAME,
									mActivity.MODE_PRIVATE);
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy",
							Locale.US);
					Date today;
					today = sdf.parse(dateFormat.format(date));
					Log.d("ACTTEST", "Todays date str : " + strToday);

					Calendar savedCal = Calendar.getInstance();
					savedCal.setTimeInMillis(settings.getLong(
							"LoungesUpdationDate", 0));
					String savedDay = String.valueOf(savedCal
							.get(Calendar.DAY_OF_MONTH));
					String sMonth = String
							.valueOf(savedCal.get(Calendar.MONTH));
					String sYear = String.valueOf(savedCal.get(Calendar.YEAR));
					SimpleDateFormat sdfSaved = new SimpleDateFormat(
							"dd/MM/yy", Locale.US);
					Date savedDate;
					Log.d("ACTTEST", "savedDate" + savedDay + "/" + sMonth
							+ "/" + sYear);

					savedDate = sdf
							.parse(savedDay + "/" + sMonth + "/" + sYear);

					if (today != savedDate) {
						assertTrue("Date not Updated", true);
					} else {
						assertTrue("Date  Updated", false);
					}

					// assertEquals(today, savedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	public void atestDefaultSkyTipsAsync() throws Throwable {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		runTestOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					mActivity.updateSkytips();
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yy");
					// get current date time with Date()
					Date date = new Date();
					String strToday = dateFormat.format(date);
					Log.d("ACTTEST",
							"CAL TIME : " + dateFormat.format(cal.getTime()));

					SharedPreferences settings = mActivity
							.getSharedPreferences(PREFS_NAME,
									mActivity.MODE_PRIVATE);
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy",
							Locale.US);
					Date today;
					today = sdf.parse(dateFormat.format(date));
					Log.d("ACTTEST", "Todays date str : " + strToday);

					Calendar savedCal = Calendar.getInstance();
					savedCal.setTimeInMillis(settings.getLong(
							"SkytipsUpdationDate", 0));
					String savedDay = String.valueOf(savedCal
							.get(Calendar.DAY_OF_MONTH));
					String sMonth = String
							.valueOf(savedCal.get(Calendar.MONTH));
					String sYear = String.valueOf(savedCal.get(Calendar.YEAR));
					SimpleDateFormat sdfSaved = new SimpleDateFormat(
							"dd/MM/yy", Locale.US);
					Date savedDate;
					Log.d("ACTTEST", "savedDate" + savedDay + "/" + sMonth
							+ "/" + sYear);

					savedDate = sdf
							.parse(savedDay + "/" + sMonth + "/" + sYear);
					assertEquals(today, savedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	// TODO Stub Asynctask for Error Condition
	public class GetAllLoungeJsonAsyncStub extends
			AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (result) {
				mActivity.updatePrefs("LoungesUpdationDate");
			}
		}
	}*/

}
