package org.skyteam.test.handlers;

import org.skyteam.database.DBHelper;
import org.skyteam.handlers.LoungeFinderHandler;
import org.skyteam.test.utils.TestData;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

public class LoungeFinderHandlerTest extends AndroidTestCase {
	private Context mContext;
	private DBHelper dbHelper;
	private LoungeFinderHandler mLoungeFinderHandler;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// mContext = this.getContext();
		mContext = getContext();
		assertNotNull(mContext);
		dbHelper = DBHelper.getInstance(mContext);
		assertNotNull(dbHelper);
		mLoungeFinderHandler = new LoungeFinderHandler(mContext);
		dbHelper.open();
	}

    @SmallTest
    public void testAsyncTask(){

        String result = mLoungeFinderHandler.nativeHandler(TestData.formLoungeDataAirport());
        //Log.d("LoungeG",""+result);
    }
	@SmallTest
	public void testFetchLounges(){
		
		String result = mLoungeFinderHandler.nativeHandler(TestData.formLoungeDataAirport());
		//Log.d("LoungeG",""+result);
	}


}
