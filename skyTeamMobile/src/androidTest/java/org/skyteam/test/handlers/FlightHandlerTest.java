package org.skyteam.test.handlers;

import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.database.DBHelper;

import org.skyteam.test.utils.TestData;
import org.skyteam.handlers.FlightHandler;

import com.google.gson.JsonObject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;


public class FlightHandlerTest  extends InstrumentationTestCase{
	private Context mContext,mInstrumentationContext;
	private DBHelper dbHelper;
	private FlightHandler mFlightHandlerHandlertest;
	private SQLiteDatabase db;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// mContext = this.getContext();
		mInstrumentationContext= this.getInstrumentation().getContext();
		mContext = getInstrumentation().getTargetContext();
		dbHelper = DBHelper.getInstance(mContext);
		assertNotNull(dbHelper);
		mFlightHandlerHandlertest = new FlightHandler(mContext);
		dbHelper.open();
		db =dbHelper.getWritableDatabase();
	}

	//@SmallTest
	public void testPreconditions() {

		//dbHelper = DBHelper.getInstance(mContext);
		//assertNotNull(dbHelper);
		
		

	}

	@LargeTest
	public void testSaveAirportShouldInsertData() {
		
		if (mFlightHandlerHandlertest != null) {			
			String result = mFlightHandlerHandlertest.nativeHandler(
					StringConstants.CMD_SAVE_FLIGHTS,
					TestData.fetchTestData(mInstrumentationContext,"FlightHandler_json"));

				if (result != null) {
					if (result.contains(TestData.EXPECTED_RESULT_FAILURE)) {						
						assertTrue(false);
					} else if (result.contains(TestData.EXPECTED_RESULT_SUCCESS)) {						
						assertTrue(true);
					}else if (result.contains(TestData.EXPECTED_RESULT_DUPLICATE)) {						
						assertTrue(false);
					}
				} else {
					assertTrue(false);
				}
			
		}
		deleteDataafterinserting();

	}
	@LargeTest
	public void testAirportShouldnotInsertDataDuplicate(){
		//insertDataforTesting();
		
		
		if (mFlightHandlerHandlertest != null) {			
			String result = mFlightHandlerHandlertest.nativeHandler(
					StringConstants.CMD_SAVE_FLIGHTS,
					TestData.fetchTestData(mInstrumentationContext,"FlightHandler_json"));
			String result1 = mFlightHandlerHandlertest.nativeHandler(
					StringConstants.CMD_SAVE_FLIGHTS,
					TestData.fetchTestData(mInstrumentationContext,"FlightHandler_json"));

				if (result != null) {
					if (result.contains(TestData.EXPECTED_RESULT_FAILURE)) {						
						assertTrue(false);
					} else if (result.contains(TestData.EXPECTED_RESULT_SUCCESS)) {						
						assertTrue(false);
					} else if (result.contains(TestData.EXPECTED_RESULT_DUPLICATE)) {						
						assertTrue(true);
					}
				} else {
					assertTrue(false);
				}
			
		}
		deleteDataafterinserting();
		
	}
	
	@LargeTest
	public void testAirportShouldDeleteSavedData() {		
		if (mFlightHandlerHandlertest != null) {
			insertDataforTesting();
			
			//Log.d("FOTEST", "" + getColIdtoDelete());
			String id = getRowIdtoDelete();
			String result = mFlightHandlerHandlertest.nativeHandler(
					StringConstants.CMD_DELETE_AIRPORT,id);			
			
				if (result != null) {
				
					if(result.contains("url")){
						assertTrue(true);
					}else{
						assertTrue(false);
				}

			}
			
		}

	}
	
	private String getRowIdtoDelete(){
	
		String SELECT_SAVED_AIRPORTS = "select Id from SavedFlights_test";
		
		Cursor cur = db.rawQuery(SELECT_SAVED_AIRPORTS, null);
		String id ="1";
		if (cur == null || cur.getCount() == 0) {

		} else {
			cur.moveToFirst();
			id = cur.getString(0);
			cur.close();
			
		}
		return id;
	}
	
	private void insertDataforTesting(){
	   JSONObject obj =dbHelper.insertIntoSavedFlights(TestData.fetchTestData(mInstrumentationContext,"FlightHandler_json"),dbHelper.checkSavedFlightsCount());
	   Log.d("FOTEST","res insertdata :: "+ obj);
	}
	
	private void deleteDataafterinserting(){
		String query = "delete from SavedFlights_test" ;
		db.execSQL(query);
				
	}


}
