package org.skyteam.test.handlers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import org.skyteam.StringConstants;
import org.skyteam.database.DBHelper;
import org.skyteam.handlers.AirportHandler;
import org.skyteam.test.utils.TestData;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;
import android.database.Cursor;




public class AirportHandlerTest extends AndroidTestCase {
	private Context mContext;
	private DBHelper mDbHelper;
	private AirportHandler mAirportHandlertest;
    private SQLiteDatabase mDb;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// mContext = this.getContext();
		mContext = getContext();
		assertNotNull(mContext);
		mDbHelper = DBHelper.getInstance(mContext);
		assertNotNull(mDbHelper);
		mAirportHandlertest = new AirportHandler(mContext);
        mDbHelper.open();
        mDb = mDbHelper.getWritableDatabase();
	}

	//@SmallTest
	public void testPreconditions() {

		//mDbHelper = DBHelper.getInstance(mContext);
		//assertNotNull(mDbHelper);
		
		

	}

	//@SmallTest
	public void testSaveAirportShouldInsertData() {
		
		if (mAirportHandlertest != null) {			
			String result = mAirportHandlertest.nativeHandler(
					StringConstants.CMD_SAVE_AIRPORTS,
					TestData.AIRPORT_SAVE_SUCCESS);
			
				if (result != null) {					
				

					if (result.contains(TestData.EXPECTED_RESULT_FAILURE)) {						
						assertTrue(false);
					} else if (result.contains(TestData.EXPECTED_RESULT_SUCCESS)) {						
						assertTrue(true);
					}else if (result.contains(TestData.EXPECTED_RESULT_DUPLICATE)) {						
						assertTrue(false);
					}
				} else {
					assertTrue(false);
				}
			
		}
		deleteDataafterinserting();

	}
	//@SmallTest
	public void testAirportShouldnotInsertDuplicate(){
		
		if (mAirportHandlertest != null) {	
			mAirportHandlertest.nativeHandler(
					StringConstants.CMD_SAVE_AIRPORTS,
					TestData.AIRPORT_SAVE_SUCCESS);
			
			String result = mAirportHandlertest.nativeHandler(
					StringConstants.CMD_SAVE_AIRPORTS,
					TestData.AIRPORT_SAVE_SUCCESS);
					
				if (result != null) {
				

					if (result.contains(TestData.EXPECTED_RESULT_FAILURE)) {						
						assertTrue(true);
					} else if (result.contains(TestData.EXPECTED_RESULT_SUCCESS)) {						
						assertTrue(false);
					}else if(result.contains(TestData.EXPECTED_RESULT_DUPLICATE)){
						assertTrue(true);
					}
				} else {
					assertTrue(false);
				}
			
		}
		deleteDataafterinserting();
		
	}
	@LargeTest
     public void testAirportShouldDeleteSavedData() {
        if (mAirportHandlertest != null) {
            insertDataforTesting();

            String result = mAirportHandlertest.nativeHandler(StringConstants.CMD_DELETE_AIRPORT,getRowIdtoDelete());

            if (result != null) {

                if(result.contains("url")){
                    assertTrue(true);
                }else{
                    assertTrue(false);
                }

            }

        }

    }

    private String getRowIdtoDelete(){

        String SELECT_SAVED_AIRPORTS = "select Id from SavedAirports_test";

        Cursor cur = mDb.rawQuery(SELECT_SAVED_AIRPORTS, null);

        String id ="1";
        if (cur == null || cur.getCount() == 0) {

        } else {
            cur.moveToFirst();
            id = cur.getString(0);
            cur.close();

        }
        return id;
    }
	
	private void insertDataforTesting(){
		mDbHelper.insertIntoSavedAirports(TestData.AIRPORT_SAVE_SUCCESS, mDbHelper.checkSavedAirportsCount());
		
	}
	
	private void deleteDataafterinserting(){
		String query = "delete from SavedAirports_test " ;
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
				db.execSQL(query);
				db.close();
	}


}
