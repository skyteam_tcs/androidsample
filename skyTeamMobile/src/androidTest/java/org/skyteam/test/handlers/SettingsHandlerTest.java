package org.skyteam.test.handlers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.database.DBHelper;
import org.skyteam.handlers.AirportHandler;
import org.skyteam.handlers.SettingsHandler;
import org.skyteam.test.utils.TestData;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

public class SettingsHandlerTest extends AndroidTestCase {
	private Context mContext;
	
	private DBHelper dbHelper;
	private SettingsHandler mSettingsHandler;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// mContext = this.getContext();
		mContext = getContext();
		assertNotNull(mContext);
		dbHelper = DBHelper.getInstance(mContext);
		assertNotNull(dbHelper);
		mSettingsHandler = new SettingsHandler(mContext);
		dbHelper.open();
	}
	
	@SmallTest
	public void testInsertSettings(){
		mSettingsHandler.nativeHandler(TestData.DEFAULT_SETTINGS);
		JSONArray master = new JSONArray();
		JSONObject JSONobj=new JSONObject();
		try{
			String  dirDir = mContext.getFilesDir() + "/";			
			InputStream is = new FileInputStream(dirDir+ DBHelper.SettingsJSONFileName);			
			Scanner s = new Scanner(is).useDelimiter("\\A");			
			String str = s.hasNext() ? s.next() : "";	
			if (str != null && str.length() > 0) {
				master = new JSONArray(str);
			} else {
				master = new JSONArray("ERROR");
			}
			JSONobj=master.getJSONObject(0);
			String strDistance = JSONobj.getString("Distance");
			assertEquals(strDistance, TestData.EXPECTED_SETTINGS_DISTANCE);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
			
		
	}

	
}
