package org.skyteam.test.handlers;

import org.skyteam.StringConstants;
import org.skyteam.database.DBHelper;
import org.skyteam.handlers.SettingsHandler;
import org.skyteam.handlers.SkyTeamMemberHandler;
import org.skyteam.test.utils.TestData;

import android.content.Context;
import android.test.AndroidTestCase;

public class SkyTeamMemberHandlerTest extends AndroidTestCase {
	
private Context mContext;
	
	private DBHelper dbHelper;
	private SkyTeamMemberHandler mSkyTeamMemberHandler;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// mContext = this.getContext();
		mContext = getContext();
		assertNotNull(mContext);
		dbHelper = DBHelper.getInstance(mContext);
		assertNotNull(dbHelper);
		mSkyTeamMemberHandler = new SkyTeamMemberHandler(mContext);
		dbHelper.open();
	}
	
	
	public void testMemberHandler(){
		
		String result = mSkyTeamMemberHandler.nativeHandler(StringConstants.CMD_OPEN_SKYTEAM_MEMEBR,TestData.SKYTEAMMEMBER_AZ);
		
		assertTrue(result.contains(TestData.SKYTEAMMEMBER_EXPECTED_RESULT));
		
	}
	

}
