package org.skyteam.test;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.activities.SkyteamWebviewActivity.GetAllLoungeJsonAsync;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.Utils;
import org.skyteam.utils.WebServiceInteraction;

import com.google.gson.JsonObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.test.ActivityUnitTestCase;
import android.util.Log;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class SkyTeamWebViewActivityTests extends
		ActivityUnitTestCase<org.skyteam.activities.SkyteamWebviewActivity> {

	private static String CLASS_NAME = "org.skyteam.activities.SkyteamWebviewActivity";
	private org.skyteam.activities.SkyteamWebviewActivity mActivity;
	public static final String PREFS_NAME = "MyPrefsFile";

	private static final String VALID_INDEX_URL = "file:///android_asset/html/index.html";
	private static final String VALID_INVALID_INDEX_URL = "file:///android_asset/html/index11.html";
	private WebView mWebView;
	private WebViewClient mMockWebViewClient;
	private static Class activity_class;

	/*static {
		try {
			activity_class = Class.forName(CLASS_NAME);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}*/

	public SkyTeamWebViewActivityTests() {
		super(org.skyteam.activities.SkyteamWebviewActivity.class);

		setName("SkyteamWebviewActivity");

	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testActivityNotNull() throws Throwable {
		runTestOnUiThread(new Runnable() {
		
		
		@Override
		public void run() {
			Intent intent = new Intent(getInstrumentation().getTargetContext(),
					org.skyteam.activities.SkyteamWebviewActivity.class);
			startActivity(intent, null, null);

			mActivity = getActivity();
			assertNotNull(mActivity);
			
		}});
	}

	public final void atestLoadValidIndexShouldPassUrl() {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		assertUrlLoading(VALID_INVALID_INDEX_URL);

	}

	public void atestSettingsFileShouldExist() {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();
		File fileSettings = mActivity
				.getFileStreamPath(DBHelper.SettingsJSONFileName);
		assertTrue(fileSettings.exists());
	}

	public final void atestLoadInvalidIndexshouldShowDefaultUrl() {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();
		// It is now returning null instaed of index.html

		assertUrlLoading(VALID_INVALID_INDEX_URL);

	}

	private void assertUrlLoading(String url) {

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			fail("Unexpected timeout");
		}
		mActivity.LoadURLInWebView();
		mActivity.webViewLoadURL(url);

		getInstrumentation().waitForIdleSync();
		mWebView = getActivity().getWebView();
		//Log.d("ACTTEST", "" + mWebView.getProgress());
		//Log.d("ACTTEST", "get : " + mWebView.getUrl());
		assertEquals(mWebView.getUrl(), VALID_INDEX_URL);
		// assertTrue(!(mWebView.getProgress() < 100));

	}

	public void atestgetCityAirportLangJSON() {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();
		JSONObject jsonObject = mActivity.getCityAirportLangJSON();
		try {
			assertEquals("Success", jsonObject.get("Result"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void atestgetAllLoungeJsonAsync() throws Throwable {
		// create CountDownLatch for which the test can wait.
		final CountDownLatch latch = new CountDownLatch(1);
		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		runTestOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					mActivity.updateAllLounges();
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yy");
					// get current date time with Date()
					Date date = new Date();
					String strToday = dateFormat.format(date);

					SharedPreferences settings = mActivity
							.getSharedPreferences(PREFS_NAME,
									mActivity.MODE_PRIVATE);
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy",
							Locale.US);
					Date today;
					today = sdf.parse(dateFormat.format(date));
					Log.d("ACTTEST", "Todays date str : " + strToday);

					Calendar savedCal = Calendar.getInstance();
					savedCal.setTimeInMillis(settings.getLong(
							"LoungesUpdationDate", 0));
					String savedDay = String.valueOf(savedCal
							.get(Calendar.DAY_OF_MONTH));
					String sMonth = String
							.valueOf(savedCal.get(Calendar.MONTH));
					String sYear = String.valueOf(savedCal.get(Calendar.YEAR));
					SimpleDateFormat sdfSaved = new SimpleDateFormat(
							"dd/MM/yy", Locale.US);
					Date savedDate;
					Log.d("ACTTEST", "savedDate" + savedDay + "/" + sMonth
							+ "/" + sYear);

					savedDate = sdf
							.parse(savedDay + "/" + sMonth + "/" + sYear);
					assertEquals(today, savedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	public void atestAllLoungeJsonAsyncErrorShouldNotUpdateDate()
			throws Throwable {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		runTestOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					new GetAllLoungeJsonAsyncStub().execute();
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yy");
					// get current date time with Date()
					Date date = new Date();
					String strToday = dateFormat.format(date);
					/*Log.d("ACTTEST",
							"CAL TIME : " + dateFormat.format(cal.getTime()));*/

					SharedPreferences settings = mActivity
							.getSharedPreferences(PREFS_NAME,
									mActivity.MODE_PRIVATE);
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy",
							Locale.US);
					Date today;
					today = sdf.parse(dateFormat.format(date));
					Log.d("ACTTEST", "Todays date str : " + strToday);

					Calendar savedCal = Calendar.getInstance();
					savedCal.setTimeInMillis(settings.getLong(
							"LoungesUpdationDate", 0));
					String savedDay = String.valueOf(savedCal
							.get(Calendar.DAY_OF_MONTH));
					String sMonth = String
							.valueOf(savedCal.get(Calendar.MONTH));
					String sYear = String.valueOf(savedCal.get(Calendar.YEAR));
					SimpleDateFormat sdfSaved = new SimpleDateFormat(
							"dd/MM/yy", Locale.US);
					Date savedDate;
					Log.d("ACTTEST", "savedDate" + savedDay + "/" + sMonth
							+ "/" + sYear);

					savedDate = sdf
							.parse(savedDay + "/" + sMonth + "/" + sYear);

					if (today != savedDate) {
						assertTrue("Date not Updated", true);
					} else {
						assertTrue("Date  Updated", false);
					}

					// assertEquals(today, savedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	public void atestDefaultSkyTipsAsync() throws Throwable {

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				org.skyteam.activities.SkyteamWebviewActivity.class);
		startActivity(intent, null, null);
		mActivity = getActivity();

		runTestOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					mActivity.updateSkytips();
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yy");
					// get current date time with Date()
					Date date = new Date();
					String strToday = dateFormat.format(date);
					Log.d("ACTTEST",
							"CAL TIME : " + dateFormat.format(cal.getTime()));

					SharedPreferences settings = mActivity
							.getSharedPreferences(PREFS_NAME,
									mActivity.MODE_PRIVATE);
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy",
							Locale.US);
					Date today;
					today = sdf.parse(dateFormat.format(date));
					Log.d("ACTTEST", "Todays date str : " + strToday);

					Calendar savedCal = Calendar.getInstance();
					savedCal.setTimeInMillis(settings.getLong(
							"SkytipsUpdationDate", 0));
					String savedDay = String.valueOf(savedCal
							.get(Calendar.DAY_OF_MONTH));
					String sMonth = String
							.valueOf(savedCal.get(Calendar.MONTH));
					String sYear = String.valueOf(savedCal.get(Calendar.YEAR));
					SimpleDateFormat sdfSaved = new SimpleDateFormat(
							"dd/MM/yy", Locale.US);
					Date savedDate;
					Log.d("ACTTEST", "savedDate" + savedDay + "/" + sMonth
							+ "/" + sYear);

					savedDate = sdf
							.parse(savedDay + "/" + sMonth + "/" + sYear);
					assertEquals(today, savedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	// TODO Stub Asynctask for Error Condition
	public class GetAllLoungeJsonAsyncStub extends
			AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (result) {
				mActivity.updatePrefs("LoungesUpdationDate");
			}
		}
	}

}
