package org.skyteam.test.adapters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.skyteam.R;
import org.skyteam.adapters.AirportArrayAdapter;
import org.skyteam.data.Location;
import org.skyteam.database.DBHelper;
import org.skyteam.test.utils.TestData;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;

public class AirportArrayAdapterTest extends InstrumentationTestCase {
	
	
	    private AirportArrayAdapter mAdapter;
	    private Context mContext,mInstrumentationContext ;
	    private DBHelper dbHelper;
	

	   
	    public AirportArrayAdapterTest() {
	        super();
	    }

	    protected void setUp() throws Exception {
	        super.setUp();
            mInstrumentationContext= this.getInstrumentation().getContext();
            mContext = getInstrumentation().getTargetContext();
			assertNotNull(mContext);
			dbHelper = DBHelper.getInstance(mContext);
			
			assertNotNull(dbHelper);
			
			dbHelper.open();
	        mAdapter = new AirportArrayAdapter(mContext, R.layout.autocomplete_list);
	      
	    	mAdapter.addAll(getTestAirportsFromJson());
	    }
	    @LargeTest
	    public void testGetItemID() {
            //Item ID should be same as position
            assertEquals("ItemIdshould be 0", 0, mAdapter.getItemId(0));

	       
	    }
	   
	    public void testGetItem() {
            assertTrue("Airport Code is not correct", mAdapter.getItem(0).getAirportCode().equals(TestData.EXPECTED_AIRPORT_CODE));
	    }

	    public void testGetCount() {
	    	
	        assertTrue("Size of items not greater than 0", mAdapter.getCount()>0);
	    }

	   
	   public void testGetView() {
	        View view = mAdapter.getView(0, null, null);
            assertNotNull(view);


	        
	    }

    public ArrayList<Location> getTestAirportsFromJson() {

        Gson gson = new Gson();
        HashMap<String,Location> map = new HashMap<String,Location>();
        try {
            List<Location> airports = new ArrayList<Location>();

            JsonReader reader=new JsonReader(new InputStreamReader(mInstrumentationContext.getAssets().open(TestData.CITY_JSON_FILE)));
            reader.beginArray();
            while (reader.hasNext()) {
                Location airport = gson.fromJson(reader, Location.class);
                if( airport != null && (airport.getLocationType().equalsIgnoreCase("Airport"))){
                    map.put(airport.getAirportCode(), airport);
                }
            }
            reader.endArray();



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(MalformedJsonException e)
        {
            e.printStackTrace();
        } catch(JsonSyntaxException e)
        {
            e.printStackTrace();
        }
        catch (IOException e) {

            e.printStackTrace();
        }
        ArrayList<Location> list = new ArrayList<Location>(map.values());
        Collections.sort(list, new Comparator<Location>(){

            @Override
            public int compare(Location lhs, Location rhs) {
                return lhs.getAirportName().compareTo(rhs.getAirportName());
            }

        });

               return list;


    }
	}


