package org.skyteam.test.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.test.InstrumentationTestCase;
import android.view.View;

import org.skyteam.adapters.MenuAdapter;
import org.skyteam.data.Category;
import org.skyteam.data.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sowmya on 3/14/2015.
 */
public class MenuAdapterTest extends InstrumentationTestCase   {

    private Context mContext,mInstrumentationContext ;
    private MenuAdapter mMenuAdapter;

    private static final  int MENU_CATEGORY= 1;
    private static final  int MENU_ITEM =0 ;

    public MenuAdapterTest() {
        super();
    }

    protected void setUp() throws Exception {
        super.setUp();
        mInstrumentationContext= this.getInstrumentation().getContext();
        mContext = getInstrumentation().getTargetContext();
        assertNotNull(mContext);
        mMenuAdapter = new MenuAdapter(mContext, fetchMenuTestData(), Typeface.createFromAsset(mContext.getAssets(),
                "html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf"),"En");

    }

    public void testGetCount(){
        assertTrue(mMenuAdapter.getCount() == fetchMenuTestData().size());
    }

    public void testGetItem(){
        assertNotNull( mMenuAdapter.getItem(0));
    }

    public void testGetItemId(){
        assertTrue(mMenuAdapter.getItemId(0) == 0);
    }

    public void testgetItemViewType(){
        assertTrue( mMenuAdapter.getItemViewType(1) == MENU_ITEM);
    }

    public void testgetItemViewTypCategorye(){
        assertTrue( mMenuAdapter.getItemViewType(0) == MENU_CATEGORY);
    }

    public void testgetViewTypeCount(){
        assertTrue(mMenuAdapter.getViewTypeCount() == 2);
    }

    public void testNotEnabled(){
        assertFalse(mMenuAdapter.isEnabled(0));
    }
    public void testEnabled(){
        assertTrue(mMenuAdapter.isEnabled(1));
    }

    public void testAreAllItemsenabled(){
        assertTrue(mMenuAdapter.areAllItemsEnabled());
    }

    public void testGetViewItem(){
        View view =mMenuAdapter.getView(1,null,null);
        assertNotNull(view);

    }

    public void testGetViewCategory(){
        View view =mMenuAdapter.getView(0,null,null);
        assertNotNull(view);
    }

    private List<Object> fetchMenuTestData(){
        List<Object> items = new ArrayList<Object>();
        items.add(new Category(mContext.getString(org.skyteam.R.string.sky_menu)));
        items.add(new Item(mContext.getString(org.skyteam.R.string.sky_home),
                org.skyteam.R.drawable.ic_home_menu));
        items.add(new Item(mContext.getString(org.skyteam.R.string.sky_flight_finder),
                org.skyteam.R.drawable.ic_flightsearch_menu));
        return items;
    }
}
