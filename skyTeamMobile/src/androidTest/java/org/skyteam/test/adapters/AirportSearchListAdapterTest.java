package org.skyteam.test.adapters;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.view.View;
import android.widget.TextView;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;

import org.skyteam.R;
import org.skyteam.adapters.AirportSearchListAdapter;
import org.skyteam.data.Location;
import org.skyteam.data.AirportDeserilizer;
import org.skyteam.data.AirportDetails;
import org.skyteam.data.AirportsSearch;
import org.skyteam.database.DBHelper;
import org.skyteam.test.utils.TestData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class AirportSearchListAdapterTest extends InstrumentationTestCase {


	    private AirportSearchListAdapter mAdapter;
	    private Context mContext,mInstrumentationContext ;
	    private DBHelper dbHelper;



	    public AirportSearchListAdapterTest() {
	        super();
	    }

	    protected void setUp() throws Exception {
	        super.setUp();
            mInstrumentationContext= this.getInstrumentation().getContext();
            mContext = getInstrumentation().getTargetContext();
			assertNotNull(mContext);
			dbHelper = DBHelper.getInstance(mContext);
			
			assertNotNull(dbHelper);
			
			dbHelper.open();

	        mAdapter = new AirportSearchListAdapter(mContext, getTestData());

	    }

    private List<AirportDetails> getTestData() throws UnsupportedEncodingException {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(
                        FieldNamingPolicy.UPPER_CAMEL_CASE)
                .registerTypeAdapter(AirportsSearch.class,
                        new AirportDeserilizer()).create();
        InputStreamReader ism = new InputStreamReader(TestData.fetchTestDataStream(mInstrumentationContext,"MapNearByAirport_json"),
                "UTF-8");

        AirportsSearch search = gson.fromJson(ism,
                AirportsSearch.class);
        List<AirportDetails> list = search.getAirportDetails();
        return list;

    }


	    public void testGetCount() {
	    	
	        assertTrue("Size of items not greater than 0", mAdapter.getCount()>0);
	    }

       public void testAirportDetails()throws UnsupportedEncodingException {
           List<AirportDetails> list = getTestData();
           assertNotNull(list.get(0).getAirportCode());
           assertNotNull(list.get(0).getAirportDetailsURL());
           assertNotNull(list.get(0).getAirportName());
           assertNotNull(list.get(0).getCityCode());
           assertNotNull(list.get(0).getCityName());
           assertNotNull(list.get(0).getCountryCode());
           assertNotNull(list.get(0).getCountryName());
           assertNotNull(list.get(0).getDistance());
           assertNotNull(list.get(0).getId());
           assertNotNull(list.get(0).getLatitude());
           assertNotNull(list.get(0).getLongitude());
           assertNotNull(list.get(0).getLocationType());


       }

	   
	   public void testGetView() {
	        View view = mAdapter.getView(0, null, null);
            assertNotNull(view);
           TextView tvAirportName = (TextView)view.findViewById(R.id.txt_airport_name);
           TextView tvDistance = (TextView) view.findViewById(R.id.txt_distance);
           assertNotNull(tvAirportName);
           assertNotNull(tvDistance);




	        
	    }

    public ArrayList<Location> getTestAirportsFromJson() {

        Gson gson = new Gson();
        HashMap<String,Location> map = new HashMap<String,Location>();
        try {
            List<Location> airports = new ArrayList<Location>();

            JsonReader reader=new JsonReader(new InputStreamReader(mInstrumentationContext.getAssets().open(TestData.CITY_JSON_FILE)));
            reader.beginArray();
            while (reader.hasNext()) {
                Location airport = gson.fromJson(reader, Location.class);
                if( airport != null && (airport.getLocationType().equalsIgnoreCase("Airport"))){
                    map.put(airport.getAirportCode(), airport);
                }
            }
            reader.endArray();



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(MalformedJsonException e)
        {
            e.printStackTrace();
        } catch(JsonSyntaxException e)
        {
            e.printStackTrace();
        }
        catch (IOException e) {

            e.printStackTrace();
        }
        ArrayList<Location> list = new ArrayList<Location>(map.values());
        Collections.sort(list, new Comparator<Location>(){

            @Override
            public int compare(Location lhs, Location rhs) {
                return lhs.getAirportName().compareTo(rhs.getAirportName());
            }

        });

               return list;


    }
	}


