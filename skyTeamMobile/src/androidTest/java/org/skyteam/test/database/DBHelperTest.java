package org.skyteam.test.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.data.Location;
import org.skyteam.database.DBHelper;
import org.skyteam.handlers.AirportHandler;
import org.skyteam.test.utils.TestData;

import java.io.File;
import java.util.ArrayList;

public class DBHelperTest extends InstrumentationTestCase{
	
	private Context mContext,mInstrumentationContext;
	private DBHelper mDbHelper;
	private AirportHandler mAirportHandlertest;
	
	private static final String DB_NAME = "skt.sqlite";

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// mContext = this.getContext();
		mContext = this.getInstrumentation().getTargetContext();
		mInstrumentationContext= this.getInstrumentation().getContext();
		assertNotNull(mContext);
		mDbHelper = DBHelper.getInstance(mContext);
		assertNotNull(mDbHelper);
		mDbHelper.open();
	}
	
	
	public void testDBExists() {
		assertTrue("Database does not exist",DBHelper.checkDataBase());
		
	}
	
	
	public void testSettingFileExists() {
		assertTrue("Settings does not exist",DBHelper.checkFlatFileSettings());
		
	}
	
	public void testCityListFileExists() {
		assertTrue("CityList does not exist",DBHelper.checkFlatFileCityAirport());
		
	}

    public void testCheckFlatFileSkyTipsCityList(){
        assertTrue("SkyTips CityList does not exist",DBHelper.checkFlatFileSkyTipsCityList());
    }
	
	
	public void testGetSavedAirportsNFlights() {
		assertTrue(mDbHelper.getSavedAirportsNFlights().has("url"));
		
	}
	
	
	
	public void testCityListShouldContainAirport() {
		boolean boolAirport = true;
		ArrayList<Location> list=mDbHelper.getAllAirportsFromJson();
		for(Location air : list){
			if(!air.getLocationType().contains("Airport")){
				boolAirport =false;
				break;
			}
		}
		
		assertTrue("CityList contains Airport",boolAirport);
		
		
	}
	
	
	public void testgetCityAirport() {
		
		JSONObject cityObj=mDbHelper.getCityAirport();
		
		
		assertTrue("CityList contains Airport",cityObj.has("AirPorts_lang"));
		
		
	}
	
	
	public void testUpdatedCityAirport() {
		
		JSONObject cityObj=mDbHelper.getUpdatedCityAirport();
		assertTrue("CityList contains Airport",cityObj.has("Result"));
		
		
	}


    public void testdeleteSavedFlights() throws JSONException {

        //precondition
        deleteFlightData();
        insertFlightDataforTesting();
        JSONObject flightJson = mDbHelper.getSavedFlightslist();


        mDbHelper.deleteSavedFlights(getFlightRowIdToDelete());
        JSONObject flightJson1 = mDbHelper.getSavedFlightslist();

       // assertEquals( ((JSONArray) flightJson.get("SavedFlights_test")).length(),0);


    }
    @MediumTest
    public void testGetSavedFlightslistShoudbeEmpty() throws JSONException {
        deleteFlightData();
        JSONObject flightJson = mDbHelper.getSavedFlightslist();
        Log.d("LOTEST", "" +((JSONArray) flightJson.get("SavedFlights_test")).length());
        assertEquals(0,((JSONArray) flightJson.get("SavedFlights_test")).length());

    }

    public void testGetSavedFlightslistShouldHaveRecord() throws JSONException {
        deleteFlightData();
        insertFlightDataforTesting();
        JSONObject flightJson = mDbHelper.getSavedFlightslist();
       // Log.d("LOTEST", "" +((JSONArray) flightJson.get("SavedFlights_test")).length());
        assertEquals( 1,((JSONArray) flightJson.get("SavedFlights_test")).length());
        deleteFlightData();

    }

    public void testGetAllAirportsFromJson(){
        ArrayList list =mDbHelper.getAllAirportsFromJson();
        assertTrue(list.size() >0 );
    }

    public void testGetSavedAirportslistShouldHaveCountZero() throws JSONException{
        deleteAirportData();
        assertEquals(0,((JSONArray)mDbHelper.getSavedAirportslist().get("SavedAirports_test")).length());

    }

    public void testGetSavedAirportslistShouldHaveData() throws JSONException {
        insertAirportData();
        assertTrue(((JSONArray) mDbHelper.getSavedAirportslist().get("SavedAirports_test")).length() > 0);
        deleteAirportData();

    }

    public void testGetLoungesNoData() throws JSONException {
        deleteFromLounges();
        assertTrue(((int) mDbHelper.getLounges("AMS").get("Count"))== 0);

    }

    public void testGetLoungeShouldHaveData() throws JSONException {
        insertDataIntoLounges();
        assertTrue(((int) mDbHelper.getLounges("AMS").get("Count"))> 0);
        deleteFromLounges();

    }

    public void testDeleteLounges() throws JSONException {
        deleteFromLounges();
        insertDataIntoLounges();
        mDbHelper.deleteFromLounges("AMS");
        assertTrue(((int) mDbHelper.getLounges("AMS").get("Count"))== 0);
    }

    public void testShouldNotInsertLoungeData() throws JSONException {
        deleteFromLounges();
        mDbHelper.insertIntoLounges("");
        assertTrue(((int) mDbHelper.getLounges("AMS").get("Count"))== 0);

    }

    public void testShouldInsertLoungeData() throws JSONException {
        deleteFromLounges();
        mDbHelper.insertIntoLounges(TestData.fetchTestData(mInstrumentationContext, "Loungehandler_json"));
        assertTrue(((int) mDbHelper.getLounges("AMS").get("Count")) > 0);

    }

    public void testCheckFlatFileCityAirportOthers(){
        assertFalse(mDbHelper.checkFlatFileCityAirportOthers());
    }

    public void testInsertIntoCityAirport(){
        String CityAirportJSONFilePath = mContext.getFilesDir() + "/";
       String filePath= CityAirportJSONFilePath + DBHelper.CityAirportJSONFileName;
        Log.d("LOTEST","filepath : "+filePath);
        File outFile = new File(filePath);
        if (outFile.exists()) {
            outFile.delete();
        }
        mDbHelper.insertIntoCityAirport(TestData.fetchTestData(mInstrumentationContext,"CityAirportListJson"));
        assertTrue("City Airport List not created with new Data",outFile.exists());
    }

    public void testGetCityAirportFile(){
        assertNotNull(mDbHelper.getCityAirportFile());
    }

    public void testGetCityAirportURL(){
        assertNotNull(mDbHelper.getCityAirportURL());
    }

    public void testGetStoredLounges() throws JSONException{
        deleteAirportsList_Lounges();
        JSONObject obj= mDbHelper.getStoredLounges();
       String str= obj.getJSONArray("AirportsList_Lounges").getString(0);
        assertEquals("Error",str);
    }

    public void testGetStoredSkyTips() throws JSONException {
        JSONObject tempObj = new JSONObject(TestData.fetchTestData(mInstrumentationContext,"SkyTipsResults_json"));
        mDbHelper.insertIntoSkytips(tempObj);
        String[] strArr={"https://services.staging.skyteam.com/skytips?version=3&airportcode=LAX","LAX"};
        JSONObject obj=mDbHelper.getStoredSkyTips(strArr);
        obj.getInt("Count");
       // Log.d("SKYTEST","testGetStoredSkyTips" + obj.getInt("Count"));
        assertTrue(obj.getInt("Count") >0 );


    }
    public void testGetSavedFlightsHomeScreenError() throws JSONException{
        deleteFlightData();
       JSONObject flightObj= mDbHelper.getSavedFlightsHomeScreen();
        assertEquals("ERROR",((JSONArray)flightObj.get("SavedFlights_test")).get(0));
       // Log.d("SKYTEST","testGetSavedFlightsHomeScreen : "+((JSONArray)flightObj.get("SavedFlights_test")).get(0));
    }

    public void testGetSettingFileObjForHTML() throws JSONException{
        JSONObject  obj=mDbHelper.getSettingFileObjForHTML();

        assertTrue(obj.has("InitialLoad"));


    }

    public void testGetSkyTipsHubsError() throws JSONException{
        deleteAirportHubs();
        JSONObject obj =mDbHelper.getSkyTipsHubs();
        assertEquals("ERROR",(obj.get("SkyTipshubs")));
    }

    private void testDeleteFromSkytips() throws  JSONException{
        String[] strArr={"","LAX"};
        mDbHelper.deleteFromSkytips(strArr);
        JSONObject obj = mDbHelper.getStoredSkyTips(strArr);
        assertEquals("No Offline Data",obj.getJSONArray("SkyTips").getString(0));

    }

    public void testgetSkyTipsCityList()throws JSONException {
        JSONObject obj=mDbHelper.getSkyTipsCityList();

        assertTrue( ((JSONArray)obj.getJSONArray("AirPorts_test")).length()>0);
    }

    public void testSelectDistinctTipTheme(){
        JSONArray jsonArray = mDbHelper.selectDistinctTipTheme();
        assertTrue(jsonArray.length()>0);
    }






    public void deleteAirportsList_Lounges(){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String query = "delete from AirportsList_Lounges";
        db.execSQL(query);
        db.close();

    }


    // Delete all Lounges Data
    public void deleteFromLounges() {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String query = "delete from Lounges";
        db.execSQL(query);
        db.close();

    }

    public void insertDataIntoLounges() {
        mDbHelper.insertIntoLounges(TestData.fetchTestData(mInstrumentationContext,"Loungehandler_json"));

    }




    private String getAirportRowIdtoDelete(){

        String SELECT_SAVED_AIRPORTS = "select Id from SavedAirports_test";
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cur = db.rawQuery(SELECT_SAVED_AIRPORTS, null);

        String id ="1";
        if (cur == null || cur.getCount() == 0) {

        } else {
            cur.moveToFirst();
            id = cur.getString(0);
            cur.close();

        }
        return id;
    }

    private void insertAirportData()
    {
        mDbHelper.insertIntoSavedAirports(TestData.AIRPORT_SAVE_SUCCESS, mDbHelper.checkSavedAirportsCount());

    }

    private void deleteAirportData(){
        String query = "delete from SavedAirports_test " ;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    private void deleteAirportHubs(){
        String query = "delete from AirPorts_test " ;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }



    private void insertFlightDataforTesting(){
        JSONObject obj =mDbHelper.insertIntoSavedFlights(TestData.fetchTestData(mInstrumentationContext, "FlightHandler_json"),mDbHelper.checkSavedFlightsCount());

    }

    private void deleteFlightData(){
        String query = "delete from SavedFlights_test" ;
        mDbHelper.getWritableDatabase().execSQL(query);

    }

    private String getFlightRowIdToDelete(){

        String SELECT_SAVED_AIRPORTS = "select Id from SavedFlights_test";

        Cursor cur = mDbHelper.getWritableDatabase().rawQuery(SELECT_SAVED_AIRPORTS, null);
      //  Log.d("LOTEST", "DEL id : " + cur.getCount());
        String id ="1";
        if (cur == null || cur.getCount() == 0) {

        } else {
            cur.moveToFirst();
            id = cur.getString(0);
            cur.close();

        }
        return id;
    }

    public void testGetSkyTeamMemberData() throws JSONException{
        JSONArray memberArray=mDbHelper.getSkyTeamMemberData(TestData.SKYTEAMMEMBER_AZ);
        assertTrue(((JSONObject)memberArray.get(0)).has("Skyteam_Member"));

    }

    public void testGetSkyTeamMemberDataforEmptyData() throws JSONException{
        JSONArray memberArray=mDbHelper.getSkyTeamMemberData("");
        assertTrue(((String) memberArray.get(0)).contains("ERROR"));

    }













}
