var iSU = iSU == null ? {} : iSU;

var strIsNative, strLang, strCommand, res,
	openalertsokbuttonFlag = false,
	isFirstTime = true,
	displayAllAirports = false,
	isFromSkyTips = false,
	xhrajax = [],
	SkyAirportNames = [],
	strIsFrom = '';

sessionStorage.loungeFlag = 0;



/* Get airport - city list */
iSU.testData = function() {
	//Need to be removed,unused
	return sessionStorage.keyval;
    
}
/*End of get airport - city list */


/*
 * Function:Native setting function call back
 * Param:
 * 		setting details JSON object
 */
iSU.getSettings = function(nativeResponse){

    var settingsResponse = nativeResponse;
    localStorage.time = settingsResponse.Time;
    localStorage.date = settingsResponse.Date;
    localStorage.distance = settingsResponse.Distance;
    localStorage.temparature = settingsResponse.Temperature;
    localStorage.skytips = settingsResponse.SkyTips;
    localStorage.language=settingsResponse.Language;
    localStorage.InitialLoad=settingsResponse.InitialLoad;
    localStorage.loadlatitude=settingsResponse.latitude;
     localStorage.loadlongitude=settingsResponse.longitude;
    getAirportNamelatlon(localStorage.loadlatitude, localStorage.loadlongitude);
}
/*End of Function:Native setting function call back*/


/* Calling Airport Details Page from Native Map Implementation starts*/
iSU.airportDetails=function(nativeResponse){
	
	var obj = {};
	obj.airportName = nativeResponse.airportName;
	obj.countryName = nativeResponse.countryName;
	obj.countryCode = nativeResponse.countryCode;
	obj.airportCode = nativeResponse.airportCode;
	obj.airportDetails_URL = nativeResponse.airportDetailsURL;
	obj.cityName = nativeResponse.cityName;
	
	sessionStorage.airportDetailsObj = '';
	sessionStorage.airportDetailsObj = JSON.stringify(obj);
	
	var obj1 = {};
	obj1.previousPage = "airportSearchPage";
	pushPageOnToBackStack(obj1);
	
	sessionStorage.airport_code = nativeResponse.airportCode;
	sessionStorage.airportName = nativeResponse.airportName;
	sessionStorage.countryCode = nativeResponse.countryCode;
	sessionStorage.countryName = nativeResponse.countryName;
	sessionStorage.airportDetails_URL = nativeResponse.airportDetailsURL;
	
	sessionStorage.goNativeMapFromDetails="YES";
	sessionStorage.transRequired = "No";
	if(isFirstTime){
        
        window.location.href = "index.html#airportDetailsViewViaMap";
        isFirstTime = false;
    }
    else {
        
        require(["router"], function(init){
                
                init.airportDetailsViewViaMap();
                
                });
    }
	
	require(['jquery','pageslide'], function($,pageslide){
	
		$.pageslide.close();
	
	});
	
		
}
/* Calling Airport Details Page from Native Map Implementation ends*/


/* Calling HTML Page from Native Map Implementation starts*/

iSU.callHTMLPage = function(nativeResponse){
    if("userMenu" === nativeResponse.HTMLPageKey){
		showActivityIndicator($.i18n.map.sky_please);
		sessionStorage.goNativeMapFromDetails = "NO";
		sessionStorage.currentPage = nativeResponse.currentPage;
        if(sessionStorage.currentPage === "scanPage"){/* no need of transition from scan view*/
        			sessionStorage.currentPage = "flightstatus";
        			sessionStorage.isFromScan = "yes";
        			}
        $('#brief img').show();
		$('#brief').trigger('click');
		
		require(['jquery','pageslide'], function($,pageslide){
			
			$.pageslide.close();
		
		    });
		
	}else{
		
		if('skyTipsFinder' === nativeResponse.HTMLPageKey){
	        if (localStorage.cityList_Skytips == 'undefined' || localStorage.cityList_Skytips == null || localStorage.cityList_Skytips == undefined) {
	            window.open(iSC.skyTipsCityList,'_self');
	        }
	        else
	        {
	           	  var temp=JSON.parse(localStorage.cityList_Skytips);
	              if(temp.AirPorts_test[0].hasOwnProperty('Airport_Name_Es')){
	            	  window.location.href = 'index.html#'+nativeResponse.HTMLPageKey;
	              }
	              else{
	            	  localStorage.removeItem('cityList_Skytips');
	            	  window.open(iSC.skyTipsCityList);
	              }
	        }
    }
    
    if('loungesSearch' === nativeResponse.HTMLPageKey){
        
	    if ('undefined'	==	localStorage.loungesAirportList || null	==	localStorage.loungesAirportList){
	        setTimeout(function(){
	                   window.open(iSC.loungeList,'_self');
	                   },1500);
	        sessionStorage.isCalledLounges = 'YES';
	    }
    }
    
    if('skyTipsFinder' !== nativeResponse.HTMLPageKey){
         if(nativeResponse.HTMLPageKey === "skyPriorityFinder"){
                sessionStorage.removeItem("airportCode");
                sessionStorage.removeItem("airportName");
                sessionStorage.isFromSaved = "no";
         }
         if(nativeResponse.HTMLPageKey === "flightstatus")
         {
                 sessionStorage.flightNo ='';
                 sessionStorage.airline = '';
                 sessionStorage.removeItem('searchairports');
                 sessionStorage.statusFor='';
         }
         if(nativeResponse.HTMLPageKey === "searchFlights")
         {
                 sessionStorage.isFromHome="";
                 sessionStorage.navToAirportDetails="";
                 if(sessionStorage.previousPages)
                 {
                         sessionStorage.removeItem('previousPages');
                 }
                 if(sessionStorage.searchData != undefined && sessionStorage.searchData != null){
                         sessionStorage.removeItem('searchData');
                 }
                 sessionStorage.sixDays="no";
                 sessionStorage.sixDaysOnward="false";
                 sessionStorage.sixDaysReturn ="false";
         }
    	window.location.href = 'index.html#'+nativeResponse.HTMLPageKey;
    }
	
	require(['jquery','pageslide'], function($,pageslide){
            $.pageslide.close();
	});
	
	}

}

/* Calling HTML Page from Native Map Implementation ends*/


/* delete selected flight */
iSU.deleteFlight = function() {
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = 'deleteFlightId';
		obj.command = sessionStorage.deletedflightID;
		window.callNative.showStr(sessionStorage.deletedflightID);

		window.callNative.notifyNative(JSON.stringify(obj));
	}
	else if ('iPhone' ===  sessionStorage.deviceInfo) {
		return sessionStorage.deletedflightID;
	}
}
/* End of delete selected flight */

/* Get skyteam member details */
iSU.openmemberdetails = function() {
	if ('android' === sessionStorage.deviceInfo) {

		var obj = {};
		obj.control = 'openMemberURL';
		obj.command = sessionStorage.memberDetailsURL;

		window.callNative.notifyNative(JSON.stringify(obj));

	}
	else if ('iPhone' === sessionStorage.deviceInfo) {
		return sessionStorage.memberDetailsURL;
	}
    
}
/* End of Get skyteam member details */

/* function for deleting selected airport */
iSU.deleteAirports = function() {
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = 'deleteAirportId';
		obj.command = sessionStorage.deletedAirportID;
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
        
		return sessionStorage.deletedAirportID;
	}
}
/* End of delete selected flight */

/* To get results of serach flights */
iSU.searchString = function() {
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = 'searchString';
        obj.command = sessionStorage.url;
	} else if ('iPhone' === sessionStorage.deviceInfo) {
        return sessionStorage.url;
    }

}
/* End of search results */

/* To insert saved airport to Database */
iSU.insertSavedAirport = function() {
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = 'saveAirport';
		obj.command = sessionStorage.saveAirportReqData;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone'	===	sessionStorage.deviceInfo) {
        
		return sessionStorage.saveAirportReqData;
	}
    
}
/* End of saved airport insertion */

/* To get the lounges for airport */
iSU.loungeString = function() {
    
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = 'loungeFinderURL';
		obj.command = sessionStorage.loungesData;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
        
		return sessionStorage.loungesData;
	}
    
}
/* End of lounges */

/* To get skytips for an airport */
iSU.skyTips = function() {

	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = "skytipsURL";
		obj.command = sessionStorage.skytipsReqData;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
        
		return sessionStorage.skytipsReqData;
	}
    
}
/* End of skytips */
/* To get number of member airlines and details */
iSU.memberdetails = function() {
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = "memberDetailsURL";
		obj.command = sessionStorage.memberName;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
        
		return sessionStorage.memberName;
	}
}
/* End of member airlines*/

/* To insert saved flight to Database */
iSU.insertSavedFlight = function() {

                                       var res = JSON.parse(sessionStorage.flightDetailsObject);

                                       var flightNo = " ";
                                       var flightUrl = " ";
                                       var saveFlightObj = {};
                                       if (res.details.flights.length == 1) {
                                           var i=0;

                                           saveFlightObj.Departure = res.details.flights[i].origin.airportCode;

                                           saveFlightObj.Arrival = res.details.flights[i].destination.airportCode;


                                           if(res.details.totalNoTransfers){

                                               saveFlightObj.No_of_Stops = res.details.totalNoTransfers;
                                           }
                                           else{
                                               saveFlightObj.No_of_Stops=0;

                                           }


                                           saveFlightObj.Departure_Date = res.details.flights[i].flightLegs[i].departureDetails.scheduledDate;

                                           saveFlightObj.Arrival_Date = res.details.flights[i].flightLegs[res.details.flights[i].flightLegs.length-1].arrivalDetails.scheduledDate;

                                           saveFlightObj.Departure_Time = res.details.flights[i].flightLegs[i].departureDetails.scheduledTime.split('+')[i].split(':')[i] + ":" + res.details.flights[i].flightLegs[i].departureDetails.scheduledTime.split('+')[i].split(':')[1];

                                           saveFlightObj.Arrival_Time = res.details.flights[i].flightLegs[res.details.flights[i].flightLegs.length-1].arrivalDetails.scheduledTime.split('+')[i].split(':')[i] + ":" + res.details.flights[i].flightLegs[res.details.flights[i].flightLegs.length-1].arrivalDetails.scheduledTime.split('+')[i].split(':')[1];

                                           saveFlightObj.Flight_Number = res.details.flights[i].marketingFlightNumber;

                                           saveFlightObj.Departure_City_Name = getAirportName(res.details.flights[i].origin.airportCode);

                                           saveFlightObj.Arrival_City_Name = getAirportName(res.details.flights[i].destination.airportCode);

                                           saveFlightObj.Details = sessionStorage.flightDetailsObject;

                                           if(sessionStorage.insertSaveFlightFrom == "FlightStatusDetails"){
                                               if(res.details.flights[i].flightLegs[0].flightStatusUrl	!= undefined)
                                               {
                                                   // to be done -preethi - complete
                                                   saveFlightObj.FlightStatusURL   = res.details.flights[i].flightLegs[0].flightStatusUrl;
                                               }
                                               else
                                               {
                                                   // to be done -preethi - complete
                                                   saveFlightObj.FlightStatusURL = sessionStorage.flightStatusUrl;
                                               }
                                           }
                                           else if(sessionStorage.insertSaveFlightFrom == "FlightDetails"){
                                               // to be done -preethi - complete

                                               saveFlightObj.FlightStatusURL = res.details.flights[i].flightLegs[0].flightStatusUrl;
                                           }

                                           //saveFlightObj.FlightStatusURL= "https://services.skyteam.com/flights/KL1000/status?source=Mashery Proxy&origin=LHR&destination=AMS&departuredate=2015-11-25";


                                       } else if (res.details.flights.length > 1) {

                                           var i=0;

                                           flightNo = res.details.flights[i].marketingFlightNumber;
                                           flightUrl = res.details.flights[0].flightLegs[0].flightStatusUrl;

                                           //to be done - preethi - complete
                                           //flightUrl = res.details.flights[0].flightLegs[0].flightStatusUrl;
                                           //flightUrl = "https://services.skyteam.com/flights/KL1000/status?source=Mashery Proxy&origin=LHR&destination=AMS&departuredate=2015-11-25";

                                           for (var p = 1,maxLen=res.details.flights.length; p <maxLen ; p++) {
                                               flightNo = flightNo + "/" + res.details.flights[p].marketingFlightNumber;
                                               //to be done - preethi- complete
                                               flightUrl = flightUrl + "," + res.details.flights[p].flightLegs[0].flightStatusUrl;
                                               //flightUrl = flightUrl + "," + "https://services.skyteam.com/flights/KL1000/status?source=Mashery Proxy&origin=LHR&destination=AMS&departuredate=2015-11-25";
                                           }

                                           saveFlightObj.Departure = res.details.flights[i].origin.airportCode;
                                           saveFlightObj.Arrival = res.details.flights[res.details.flights.length - 1].destination.airportCode;
                                           saveFlightObj.No_of_Stops = res.details.totalNoTransfers;


                                           saveFlightObj.Departure_Date = res.details.flights[i].flightLegs[i].departureDetails.scheduledDate;

                                           saveFlightObj.Arrival_Date = res.details.flights[res.details.flights.length - 1].flightLegs[res.details.flights[res.details.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledDate;

                                           saveFlightObj.Departure_Time = res.details.flights[i].flightLegs[i].departureDetails.scheduledTime.split('+')[i].split(':')[i] + ":" + res.details.flights[i].flightLegs[i].departureDetails.scheduledTime.split('+')[i].split(':')[1];

                                           saveFlightObj.Arrival_Time = res.details.flights[res.details.flights.length - 1].flightLegs[res.details.flights[res.details.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledTime.split('+')[i].split(':')[i] + ":" + res.details.flights[res.details.flights.length - 1].flightLegs[res.details.flights[res.details.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledTime.split('+')[i].split(':')[1];


                                           saveFlightObj.Flight_Number = res.details.flights[i].marketingFlightNumber;

                                           saveFlightObj.Departure_City_Name = getAirportName(res.details.flights[i].origin.airportCode);

                                           saveFlightObj.Arrival_City_Name = getAirportName(res.details.flights[res.details.flights.length - 1].destination.airportCode);

                                           //to be done -preethi
                                           // if(flightUrl.indexOf("undefined") !=-1)
                                           // {

                                           //    saveFlightObj.FlightStatusURL   = res.flightStatusUrl;
                                           // }
                                           // else
                                           //{
                                           //  saveFlightObj.FlightStatusURL = flightUrl;
                                           //}

                                           saveFlightObj.FlightStatusURL= flightUrl;

                                           saveFlightObj.Details = sessionStorage.flightDetailsObject;
                                       }

                                       if (sessionStorage.deviceInfo == "android") {
                                           var obj = {};
                                           obj.control = "saveFlight";
                                           obj.command = saveFlightObj;

                                           window.callNative.notifyNative(JSON.stringify(obj));
                                       } else if (sessionStorage.deviceInfo == "iPhone") {

                                           return JSON.stringify(saveFlightObj);
                                       }
                                   }
/* End of saved flight insertion */

/* Get status of a flight */
iSU.flightstatusURL = function() {
    
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = "flightStatus";
		obj.command = sessionStorage.flightStatusUrl;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
        
		return sessionStorage.flightStatusUrl;
	}
    
}
/* End of flight status */
/* Insert the user selected settings to DB */
iSU.settings = function() {
    
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = "settings";
		obj.command = sessionStorage.settingsURL;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
		return sessionStorage.settingsURL;
	}
    
}
/* End of settings insertion*/
iSU.savedFlightStatusURL = function() {
    if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = "savedFlightStatus";
		obj.command = sessionStorage.savedFlightStatusURL;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
		return sessionStorage.savedFlightStatusURL;
	}
}


//*Flight Finder without Native*/

iSU.processFlightFinderData = function(nativeResponse) {


	localStorage.searchResults = JSON.stringify(nativeResponse);



	if ("Network Error" === (JSON.parse(localStorage.searchResults)).Connections) {
		hideActivityIndicator();
		openAlertWithOk($.i18n.map.sky_notconnected);
	} else if ("ERROR" === (JSON.parse(localStorage.searchResults)).Connections) {
		hideActivityIndicator();
		openAlertWithOk($.i18n.map.sky_unload);
	}
	if ("undefined" != localStorage.searchResults &&  null != localStorage.searchResults) {

		hideActivityIndicator();
		var locationPath = window.location;
		locationPath = locationPath.toString();

		var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);

		if ( "roundtrip" == nativeResponse.journey) {

            nativeResponse.Onward = '';
            nativeResponse.Return = '';
			if (nativeResponse.Onward.ErrorCode && nativeResponse.Return.ErrorCode ) {
				if(nativeResponse.Onward.ErrorCode == "3203" && nativeResponse.Return.ErrorCode == "3204"){
                    openAlertWithOk($.i18n.map.sky_flight_unavailable);
                }
                else if(nativeResponse.Onward.ErrorCode == "3010" && nativeResponse.Return.ErrorCode == "3010"){
                    openAlertWithOk($.i18n.map.sky_flight_unavailable);
                }
                else{
                    openAlertWithOk($.i18n.map.sky_requestfailed);
                }
				if ("serachflights" === spath) {

					var newFragment = Backbone.history.getFragment($(this).attr('href'));
					if (Backbone.history.fragment == newFragment) {
						// need to null out Backbone.history.fragement because
						// navigate method will ignore when it is the same as newFragment
						Backbone.history.fragment = null;
						Backbone.history.navigate(newFragment, true);
					}
				}
			}

			 else if(nativeResponse.Onward.ErrorCode){
                 if (spath == "searchresults") {

                     var newFragment = Backbone.history.getFragment($(this).attr('href'));
                     if (Backbone.history.fragment == newFragment) {
                         Backbone.history.fragment = null;
                         Backbone.history.navigate(newFragment, true);
                     }
                 }
                 var obj = {};
                 obj.previousPage = "searchFlights";
                 obj.flightSearch=sessionStorage.searchData;
                 pushPageOnToBackStack(obj);
                 /* checking for the available flight on the selected developers*/
                 sessionStorage.sixDaysReturn = checkFlightForTheDay(nativeResponse.schedules.returnSchedules,sessionStorage.selectedRetDay);
                 sessionStorage.isSixDaysReturnPopupShown = "no";//for checking whether 7 days popup for return is shown to user or not;
                 /* checking for the available flight on the selected developers*/
                 window.location.href = 'index.html#searchResults';

             }
             else if(nativeResponse.Return.ErrorCode){
                 if (spath == "searchresults") {

                     var newFragment = Backbone.history.getFragment($(this).attr('href'));
                     if (Backbone.history.fragment == newFragment) {
                         Backbone.history.fragment = null;
                         Backbone.history.navigate(newFragment, true);
                     }
                 }
                 var obj = {};
                 obj.previousPage = "searchFlights";
                 obj.flightSearch=sessionStorage.searchData;
                 pushPageOnToBackStack(obj);
                 /* checking for the available flight on the selected developers*/
                 sessionStorage.sixDaysOnward = checkFlightForTheDay(nativeResponse.schedules.onwardSchedules,sessionStorage.selectedDepDay);
                 window.location.href = 'index.html#searchResults';

             }
			 else {
				if ("searchresults" === spath) {

					var newFragment = Backbone.history.getFragment($(this).attr('href'));
					if (Backbone.history.fragment == newFragment) {
						// need to null out Backbone.history.fragement because
						// navigate method will ignore when it is the same as newFragment
						Backbone.history.fragment = null;
						Backbone.history.navigate(newFragment, true);
					}
				}
				var obj = {};
				obj.previousPage = "searchFlights";
				obj.flightSearch=sessionStorage.searchData;
				pushPageOnToBackStack(obj);



				/* checking for the available flight on the selected developers*/
				//if (nativeResponse.schedules.onwardSchedules.Errors == undefined) {
                //sessionStorage.sixDaysOnward = checkFlightForTheDay(nativeResponse.schedules.onwardSchedules,sessionStorage.selectedDepDay);

              //  }
              //  if (nativeResponse.schedules.returnSchedules.Errors == undefined) {
               // sessionStorage.sixDaysReturn = checkFlightForTheDay(nativeResponse.schedules.returnSchedules,sessionStorage.selectedRetDay);

               // }
              //  sessionStorage.isSixDaysReturnPopupShown = "no";/*for checking whether 7 days popup for return is shown to user or not;*/
                /* checking for the available flight on the selected developers*/
                window.location.href = "index.html#searchResults";



			}
		} else if ("oneway" == nativeResponse.journey) {

		if (nativeResponse.schedules.onwardSchedules.Errors != undefined) {
		if (nativeResponse.schedules.onwardSchedules.Errors[0].Business_Error_Code == "30401") {
		var obj = {};
                            obj.previousPage = "searchFlights";
                            obj.flightSearch=sessionStorage.searchData;
                            pushPageOnToBackStack(obj);
                            sessionStorage.sixDaysOnward ="true";
                            window.location.href = 'index.html#searchResults';

       }

        }
			else if (nativeResponse.schedules != undefined) {
                if(nativeResponse.schedules.onwardSchedules){
                    if(nativeResponse.schedules.onwardSchedules.trips){

                        var locationPath = window.location;
                        locationPath = locationPath.toString();
                        var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                        if (spath == "searchresults") {

                            var newFragment = Backbone.history.getFragment($(this).attr('href'));
                            if (Backbone.history.fragment == newFragment) {
                                // need to null out Backbone.history.fragement because
                                // navigate method will ignore when it is the same as newFragment
                                Backbone.history.fragment = null;
                                Backbone.history.navigate(newFragment, true);
                            }
                        }
                        var obj = {};
                        obj.previousPage = "searchFlights";
                        obj.flightSearch=sessionStorage.searchData;
                        pushPageOnToBackStack(obj);
                        /* start of new filter logic needs to come here*/
                        list = JSON.parse(localStorage.searchResults);

                        var flightAvailable=checkFlightForTheDay(list.schedules.onwardSchedules,sessionStorage.selectedDepDay);


                        /* start of new filter logic needs to come here*/
                        if(flightAvailable === "true"){
                            sessionStorage.sixDaysOnward ="true";
                            window.location.href = 'index.html#searchResults';
                        }
                        else{
                          //  var userPrfered6Days=openSixDaysSuggestion(list);
                             window.location.href = 'index.html#searchResults';

                        }
                    }
                }

            } else if (nativeResponse.ErrorCode == "3202") {
				openAlertWithOk($.i18n.map.sky_flight_unavailable);
				if (spath == "searchflights") {
					var newFragment = Backbone.history.getFragment($(this).attr('href'));
					if (Backbone.history.fragment == newFragment) {
						// need to null out Backbone.history.fragement because
						// navigate method will ignore when it is the same as newFragment
						Backbone.history.fragment = null;
						Backbone.history.navigate(newFragment, true);
					}
				}
			} else {
				openAlertWithOk($.i18n.map.sky_flight_unavailable);
			}

		}

	}


}

/* opening skyteam in website/facebook */
iSU.openSkyTeamWebSite = function() {
	if ('android' === sessionStorage.deviceInfo) {
		var obj = {};
		obj.control = "openSkyTeamURL";
		obj.command = sessionStorage.memberDetailsURL;
        
		window.callNative.notifyNative(JSON.stringify(obj));
	} else if ('iPhone' === sessionStorage.deviceInfo) {
		return sessionStorage.memberDetailsURL;
	}
    
}
/* opening skyteam website */

/* For getting the response from native */

iSU.processData = function(nativeResponse) {
 	if (nativeResponse != undefined) {
		var currentUrl = nativeResponse.url;
		switch(currentUrl) {
		case  iSC.getAirports:
			if (typeof nativeResponse.AirPorts_lang != "undefined" && nativeResponse.AirPorts_lang!=null){

				var tempArr=[];
                tempArr=nativeResponse.AirPorts_lang;
                tempArr.sort(function(a,b){
                             return (a.airportName > b.airportName) ? 1 : -1;
                             });
                  localStorage.cityList=JSON.stringify(tempArr);
			}
		//	localStorage.cityList = JSON.stringify(nativeResponse.AirPorts_lang);
			if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {

				//	mergeCityList();

				hideActivityIndicator();     
			}
			window.getAirportsCallback();
			break;
		case iSC.getLang:
			var currentResult=nativeResponse.Result;
			if(currentResult=="Success")
			{


			//	localStorage.cityList_others = JSON.stringify(nativeResponse.AirPorts_lang);
				var tempArr=[];
                tempArr=nativeResponse.AirPorts_lang;
                tempArr.sort(function(a,b){
                             return (a.airportName > b.airportName) ? 1 : -1;
                             });
                  localStorage.cityList=JSON.stringify(tempArr);
                  window.getLangCallback();

			}
			else
			{
				hideActivityIndicator();
				$("#blackshade").hide();
				localStorage.language=localStorage.previouslang;
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+localStorage.skytips+','+localStorage.language+','+'NO';
//				iSU.settings(); //updating in SettingsJson File
                setTimeout(function(){
                                           window.open('settings:'+sessionStorage.settingsURL,"_self");
                                },500);
				if(currentResult !="CANCEL")
				{
					openAlertWithOk($.i18n.map.sky_requestfailed);
				}
			}

			break;		
		case  iSC.citylistUpdate:
			if (nativeResponse.Result == "Success")
			{	if (typeof nativeResponse.AirPorts_lang != "undefined" && nativeResponse.AirPorts_lang!=null){
					localStorage.cityListUpdatedData=JSON.stringify(nativeResponse.AirPorts_lang);
					localStorage.cityListUpdateAvailable = "YES";
				}
			}
			else    {
				localStorage.cityListUpdateAvailable = "NO";
			}
			break;
		case iSC.skyTipsCityList:
			if(nativeResponse.Status == "Success")
			{
				localStorage.cityList_Skytips = JSON.stringify(nativeResponse);

			}
			hideActivityIndicator();
			window.location.href = 'index.html#skyTipsFinder';
			break;
		case  iSC.savedAirports:
			localStorage.savedFlightsAirports = JSON.stringify(nativeResponse);
			if (localStorage.savedFlightsAirports != 'undefined' && localStorage.savedFlightsAirports != null) {
				hideActivityIndicator();
				$('#rightSidetext').off('click');
                $('#brief img').hide();
				window.location.href = 'index.html#savedFlightsOrAirports';
			}
			break;
		case  iSC.skyTipsLocation:
			// for lat lang of current location
			localStorage.currentLocation = JSON.stringify(nativeResponse);
			window.skyTipsLocationAjaxCallback();
			break;
		case  iSC.deleteFlights:

			localStorage.retrieveFlights = JSON.stringify(nativeResponse);
			setTimeout(function() {
				hideActivityIndicator();
			}, 1000);
			break;
		case  iSC.deleteAirports:
			localStorage.retrieveAirports = JSON.stringify(nativeResponse);

			setTimeout(function() {
				hideActivityIndicator();
			}, 1000);

			break;

		case iSC.loungeFinder:

			hideActivityIndicator();
			localStorage.loungeFinderMenuDisplay = "";
			if (sessionStorage.lounge == "airportDetails") {

				if (nativeResponse.LoungeData == "No Offline Data") {
					var currentobj = JSON.parse(sessionStorage.previousPages);

					popPageOnToBackStack();

					setTimeout(function() {
						window.open("analytics://_trackPageview#LoungeScreen_NoLoungesOffline", "_self");
					}, 100);
					openAlertWithOk($.i18n.map.sky_lounge_unavailable);

				}
				else if(nativeResponse.LoungeData == "ERROR"){
					openAlertWithOk($.i18n.map.sky_check);
				}
				else if(nativeResponse.LoungeData == "CANCEL"){  
					popPageOnToBackStack();
				}
				else if (nativeResponse.LoungeData != undefined) {
					if (sessionStorage.loungeFlag == 0) {
						localStorage.loungeFinderMenuDisplay = JSON.stringify(nativeResponse);
						if (localStorage.loungeFinderMenuDisplay != undefined) {
							$('.main_header ul li:eq(0) img').off('click');

							window.location.href = 'index.html#loungeFinderResult';
							
						}


					} else if (sessionStorage.loungeFlag == 1) {

						localStorage.loungeFinderMenuDisplay = JSON.stringify(nativeResponse);

						var newFragment = Backbone.history.getFragment($(this).attr('href'));
						if (Backbone.history.fragment == newFragment) {
							$('.main_header ul li:eq(0) img').off('click');
							Backbone.history.fragment = null;
							Backbone.history.navigate(newFragment, true);
						}
					}

				} else if (nativeResponse.ErrorMessage == "No Lounges Found") {
					var currentobj = JSON.parse(sessionStorage.previousPages);
					setTimeout(function() {
						window.open("analytics://_trackPageview#LoungeScreen_NoLounges", "_self");
					}, 100);
					var locationPath = window.location;
					locationPath = locationPath.toString();
					var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
					if (spath == "loungesFinderMenuView") {
					} else {
						popPageOnToBackStack();
					}

					sessionStorage.lastPage = "airportSearchPage";
					openAlertWithOk($.i18n.map.sky_lounge_unavailable);

				}

				else {
					var currentobj = JSON.parse(sessionStorage.previousPages);
					window.location.href = "index.html#" + currentobj[0].previousPage;
					popPageOnToBackStack();
					$('.main_header ul li:eq(0) img').on('click');
					setTimeout(function() {
						window.open("analytics://_trackPageview#LoungeScreen_NoLounges", "_self");
					}, 100);
					openAlertWithOk(nativeResponse.ErrorMessage);
				}

			}
			else if(nativeResponse.LoungeData == "ERROR"){
				openAlertWithOk($.i18n.map.sky_check);
			}
			else if(nativeResponse.LoungeData == "CANCEL"){
				popPageOnToBackStack();
			}
			else {

				if (nativeResponse.LoungeData == "No Offline Data") {
					setTimeout(function() {
						window.open("analytics://_trackPageview#LoungeScreen_NoLoungesOffline", "_self");
					}, 100);
					openAlertWithOk($.i18n.map.sky_lounge_unavailable);
					//}
				} else if (nativeResponse.LoungeData != undefined) {
					localStorage.loungeFinderMenuDisplay = JSON.stringify(nativeResponse);
					if (nativeResponse.LoungeData == "Network Error") {
						popPageOnToBackStack();
						openAlertWithOk($.i18n.map.sky_notconnected);
						break;
					} else {
						localStorage.loungeFinderMenuDisplay = JSON.stringify(nativeResponse);
						window.location.href="index.html#loungeFinderResult";
					}
				} else if (nativeResponse.ErrorMessage == "No Lounges Found") {
					setTimeout(function() {
						window.open("analytics://_trackPageview#LoungeScreen_NoLounges", "_self");
					}, 100);
					openAlertWithOk($.i18n.map.sky_lounge_unavailable);

				} else {
					if (sessionStorage.previousPage == undefined) {
						var currentobj = JSON.parse(sessionStorage.previousPages);
						window.location.href = "index.html#" + currentobj[0].previousPage;
						popPageOnToBackStack();
						$('.main_header ul li:eq(0) img').on('click');
					} else {
						openAlertWithOk(nativeResponse.ErrorMessage);
					}
				}
			}

			break;
		case iSC.savedFlights:

			localStorage.savedFlightDetails = JSON.stringify(nativeResponse);
			var settingsResponse = nativeResponse;
			localStorage.time = settingsResponse.settings[0].Time;
			localStorage.date = settingsResponse.settings[0].Date;
			localStorage.distance = settingsResponse.settings[0].Distance;
			localStorage.temparature = settingsResponse.settings[0].Temperature;
			localStorage.skytips = settingsResponse.settings[0].SkyTips;
			localStorage.language=settingsResponse.settings[0].Language;
			localStorage.InitialLoad=settingsResponse.settings[0].InitialLoad;

			if("homePage" === viewNavigator.currentPage.id){//if home page call saved flights callback
                window.savedFlightsCallBack();
            }
            else{//invalidate saved flights callback function
                window.savedFlightsCallBack = function() {
                };
            }
			break;
		case iSC.memberdetails:
			localStorage.skyTeamMemberDisplay = JSON.stringify(nativeResponse);
			var obj = {};
			obj.previousPage = "aboutSkyTeam";
			pushPageOnToBackStack(obj);
			window.location.href = 'index.html#skyTeamMember';
			break;
		case iSC.skytips:
			localStorage.skyTipsDetails = "";

			if (nativeResponse.SkyTips != undefined) {
				if (nativeResponse.SkyTips == "Network Error") {
					popPageOnToBackStack();
					hideActivityIndicator();
					openAlertWithOk($.i18n.map.sky_notconnected);
					break;
				} else if (nativeResponse.SkyTips == "No Offline Data") {
					//popPageOnToBackStack();
					hideActivityIndicator();
					openAlertWithOk($.i18n.map.sky_skytips_unavailable);
//					window.location.href = 'index.html#skyTipsResults';
				} 
				else if (nativeResponse.SkyTips == "CANCEL") {
					popPageOnToBackStack();
				}
				else {
					function comp(a, b) {
						return new Date(b.Date).getTime() - new Date(a.Date).getTime();
					}
					if(nativeResponse.SkyTips instanceof Array && nativeResponse.SkyTips.length>1)
					{nativeResponse.SkyTips.sort(comp);}
					localStorage.skyTipsDetails = JSON.stringify(nativeResponse);
					var skyTipsResponse = JSON.parse(localStorage.skyTipsDetails);
					skyTipsResponse.tipTheme = _.sortBy(skyTipsResponse.tipTheme, function(kk) {
						return kk.toUpperCase();
					});
					localStorage.skyTipsDetails = JSON.stringify(skyTipsResponse);
					window.location.href = 'index.html#skyTipsResults';
				}
			} else if (nativeResponse.ErrorMessage == "No Tips Found") {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_skytips_unavailable);
//				window.location.href = 'index.html#skyTipsResults';
			}else if(nativeResponse.ErrorCode == "3303") {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_skymoment);
				window.location.href = 'index.html#skyTipsFinder';
			}
			else {
				openAlertWithOk(nativeResponse.ErrorMessage);
			}
			break;
		case iSC.insertSavedAirport:
			if (nativeResponse.status == "sucess") {
				hideActivityIndicator();
				showToastMessage($.i18n.map.sky_myskyteam_airport);
				//openAlertWithOk($.i18n.map.sky_myskyteam_airport);

			} else if (nativeResponse.status == "duplicate") {
				hideActivityIndicator();
				showToastMessage($.i18n.map.sky_duplicate);
				//openAlertWithOk($.i18n.map.sky_duplicate);

			} else if (nativeResponse.status == "failure") {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_unload);

			}

			break;
		case iSC.insertSaveFlight:
			if (nativeResponse.status == "sucess") {
				hideActivityIndicator();
				showToastMessage($.i18n.map.sky_flight_inskyteam);
				//openAlertWithOk($.i18n.map.sky_flight_inskyteam);
			} else if (nativeResponse.status == "duplicate") {
				hideActivityIndicator();
				showToastMessage($.i18n.map.sky_duplicate);
				//openAlertWithOk($.i18n.map.sky_duplicate);

			} else if (nativeResponse.status == "failure") {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_unload);

			}
			break;
		case iSC.openmemberdetails:
			break;

		case  iSC.Network:
			localStorage.networkStatus = JSON.stringify(nativeResponse);
			if(window.networkStatCallBack){
				window.networkStatCallBack();
			}
			break;
		case iSC.skyTipsHub:

			if (nativeResponse.SkyTipshubs != undefined) {
				localStorage.skyTipsAirports = JSON.stringify(nativeResponse);
				hideActivityIndicator();
				window.location.href = 'index.html#skyTipsFinder';

			} 
			break;
		case iSC.loungeList:
			if (nativeResponse.AirportsList_Lounges != undefined) {
				localStorage.loungesAirportList = JSON.stringify(nativeResponse);
				hideActivityIndicator();
				window.location.href = 'index.html#loungesSearch';

			}
			break;
		case iSC.flightstatusURL:
                

                if(!nativeResponse.Errors){

                    if(sessionStorage.statusFor == "flightNumber"){

                        var temp = {
                            "schedules": {
                                "trips": [nativeResponse]
                            }
                        };

                        nativeResponse = temp;
                    }
                }

                                else
                                if(sessionStorage.statusFor == "flightNumber"){

                                    hideActivityIndicator();
                                    openAlertWithOk($.i18n.map.sky_noflight_number);
                                    break;
                                }

                
			localStorage.flightStatusResults = JSON.stringify(nativeResponse);
			if (nativeResponse.FlightStatus == "Network Error" || nativeResponse.Connections == "Network Error") {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_notconnected);
				break;
			} else if (nativeResponse.FlightStatus == "ERROR" || nativeResponse.Connections == "ERROR") {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_unload);
				break;
			}
			else if (nativeResponse.Connections  == "CANCEL") {
				popPageOnToBackStack();
				break;
			}
			if (nativeResponse.schedules != undefined && nativeResponse.Connections != "Network Error" && nativeResponse.Connections != "ERROR") {
				localStorage.flightStatusResults = JSON.stringify(nativeResponse);
				if (localStorage.flightStatusResults != 'undefined' && localStorage.flightStatusResults != null) {

					hideActivityIndicator();
					if (sessionStorage.statusFor == "flightNumber") {
						var res = nativeResponse.schedules.trips[0];

						var flightStatusObj = {};

						flightStatusObj.flightDetailsID = "status_0";
						flightStatusObj.details = res;
						flightStatusObj.flightStatusUrl = sessionStorage.flightStatusUrl;
						flightStatusObj.totalNoTransfers =0;


						sessionStorage.flightStatusObj = JSON.stringify(flightStatusObj);
						var pageObj = {};
						pageObj.previousPage = "flightstatus";

						pushPageOnToBackStack(pageObj);
						window.location.href = "index.html#flightStatusDetails";
					} else if (sessionStorage.statusFor = "airports") {
						var pageObj = {};
						pageObj.previousPage = "flightstatus";

						pushPageOnToBackStack(pageObj);
						showActivityIndicator($.i18n.map.sky_please);
						window.location.href = 'index.html#flightstatusresults';
					}
				} else {
					hideActivityIndicator();
					openAlertWithOk($.i18n.map.sky_noflight_moment);
				}
			} else if (nativeResponse.ErrorCode) {
				hideActivityIndicator();
				if (sessionStorage.statusFor == "flightNumber") {
					openAlertWithOk($.i18n.map.sky_noflight_number);
				}
				else if (sessionStorage.statusFor = "airports") {
					openAlertWithOk($.i18n.map.sky_checkdate);
				}
				var newFragment = Backbone.history.getFragment($(this).attr('href'));
	//			if (Backbone.history.fragment == newFragment) {
	//				$('.main_header ul li:eq(0) img').off('click');
					// need to null out Backbone.history.fragement because
					// navigate method will ignore when it is the same as newFragment
	//				Backbone.history.fragment = null;
	//				Backbone.history.navigate(newFragment, true);
	//			}
			} else {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_unload);

			}

			break;

		case iSC.settings:
			break;
		case iSC.savedFlightStatusURL:

			localStorage.savedFlightStatus = "";
			localStorage.savedFlightStatus = JSON.stringify(nativeResponse);
			window.location.href = "index.html#FlightDetailsPage";
			break;

		}
	} else {
		hideActivityIndicator();
		openAlertWithOk($.i18n.map.sky_network_failure);
	}

}
/** End of native response **/

/*  Code for loading indicator */
function showActivityIndicator(textToDisplay) {
	if ($('.modal').css('display') == 'none') {
		$('*').bind('touchmove', function(e) {
			e.preventDefault();
		});
		$('.modal').show();
	}
	$('.modal .shade').css('min-height', '120px');
	$('.modal .shade').css('height', '120px');
	$('.modal .shade').css('background-position','50% 70%');
	$('#okbtnPopUp').hide();
	$('#loadingMsg').html(textToDisplay);
	$('.modal p').css('color', '#ADADAD','font-family',' "HNL", sans-serif');
}

function showActivityIndicatorForAjax(isNative,command,lang) {
	if ($('.modal').css('display') == 'none') {
		$('*').bind('touchmove', function(e) {
			e.preventDefault();
		});

		$('.modal').show();
	}
	$('.modal .shade').css('min-height', '180px');
	$('.modal .shade').css('height', 'auto');
	$('.modal .shade').css('background-position','50% 43%');
	$('.modal .searchButton').css('margin-top','28%');
	$('#loadingMsg').html($.i18n.map.sky_please_data);
	$('#cancelButton').html($.i18n.map.sky_cancel);
	$('.modal p').css('color', '#ADADAD','font-family',' "HNL", sans-serif');
	if($('#okbtnPopUp').css('display')== 'none')
	{
		$('#okbtnPopUp').show();
	}
	$('#okbtnPopUp').css('display','block');
	
	strIsNative=isNative;
	if($.type(lang) == "undefined" || lang == undefined){
		strLang = localStorage.language;	
	}
	else{
		strLang=lang;
	}
	strCommand=command;
	$('#okbtnPopUp').unbind('click').click(function() {
		if(strIsNative=='false' && xhrajax != ""){
			xhrajax.abort();
			xhrajax=[];
		}
		else if(strIsNative=='true')
		{
			if ('android' === sessionStorage.deviceInfo) {

				if(strCommand != "undefined" && strCommand ==="langURLcancel" ){
					window.open(iSC.cancelReuest,'_self');
				}else{
					var obj = {};
					obj.control = "cancelAsyncTask";
					obj.command="true";

					window.callNative.notifyNative(JSON.stringify(obj));
				}

			}else{
				window.open(iSC.cancelReuest,'_self');
			}
			if(strLang != "undefined"){
//			    localStorage.language = strLang;
//				setTimeout(function() {
//				    sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+localStorage.skytips+','+strLang+','+'NO';
//					window.open('settings:'+sessionStorage.settingsURL,"_self");
//				}, 1000);
			}

		}
		$('*').unbind('touchmove');

		hideActivityIndicator();
		if($('#blackshade').is(':visible')){
                $('#blackshade').hide();
            }
           viewNavigator.currentPage.delegateEvents();
		strCommand='';
	});
}

/* Implementation of hideActivityIndicator */
function hideActivityIndicator() {

	if ($('.modal').css('display') == 'block') {
		//setTimeout(function() {
		$('*').unbind('touchmove');
		$('.modal').hide();
	}

}

/* End of hideActivityIndicator */

/* Code for custom alerts/popups*/
function showToastMessage(message)
{


    $('.toastmsg').html(message);
    $('.toast_dialog').show();
    $('.popUpShade').show();
    setTimeout(function(){
               $('.toast_dialog').hide();
               $('.popUpShade').hide();
               },3000);

}
function openAlertWithOk(message,isFrom) {
	openalertsokbuttonFlag=true;
	$('*').bind('touchmove', function(e) {
		e.preventDefault();
	});
	$('.popUp').removeClass('test_popUp');
        $('.popUpMessage1').removeClass('test_popUpmessage');$('.img_popUp').removeClass('test_popUpImage');
	$('.popUpShade').show();
	$('.popUp').show();
$('.img_popUp').css('display','none');

	$('.popUpMessage1').html(message);
	$('#okButton').prop("disabled", false);//enabling the ok button
	$('#okButton').text($.i18n.map.sky_ok);
	$('.popUpMessage1').css('text-align','center');
	$('.popUpShade').css({
		opacity : 0.5,

	});

	$('.popUp button').hide();
	$('.popUp button').show();
	strIsFrom=isFrom;
	$('.popUp button').click(function() {

		openalertsokbuttonFlag=false;
		if(sessionStorage.currentPage === "skyTipsResults"){
			$("#tipThemesSelect").removeClass("degradeZindex");
		}
		else if(strIsFrom == 'airports')
		{
			strIsFrom='';
			sessionStorage.goNativeMapFromDetails="NO";
			window.open("app:nativeMapOld","_self");
			setTimeout(function() {
                window.open("analytics://_trackPageview#airportSearchPage", "_self");
            }, 1000);
		}
		$('*').unbind('touchmove');
		$('.popUpShade').hide();
		$('.popUp').hide();
		$('.popUp button:eq(2)').remove();
		$('.popUp button:eq(1)').remove();
		$("#blackPopUpShade").css("display","none");
		if(strIsFrom == 'skytips')
		{
			strIsFrom='';
			window.location.href = 'index.html#skyTipsFinder';
		}
		else if(strIsFrom == 'airportDetails')
		{
			strIsFrom='';
			$('.main_header ul li:eq(1) img').trigger('click');
		}
		 viewNavigator.currentPage.delegateEvents();

	});

}

function openAlert(message) {
	$('*').bind('touchmove', function(e) {
		e.preventDefault();
	});
	$('.popUp').removeClass('test_popUp');
        $('.popUpMessage1').removeClass('test_popUpmessage');$('.img_popUp').removeClass('test_popUpImage');
	$('.popUpShade').show();
	$('.popUp').show();
	$('.popUp button').hide();
	$('.img_popUp').css('display','none');

	$('.popUpMessage1').html(message);
	$('.popUpMessage1').css('text-align','center');
	$('.popUpShade').css({
		'opacity' : 0.5,
		'right' : 0,
		'bottom' : 0,
		'top' : 0,
		'left' : 0,

	});
	setTimeout(function() {
		$('.popUpShade').hide();
		$('*').unbind('touchmove');
		$('.popUp').hide();
	}, 3000);
}

/*End of custom alerts/popups*/

/*Function to return the city name*/
function getAirportName(airportCode) {

	
		if (localStorage.cityList != undefined) {
			var list = JSON.parse(localStorage.cityList);
			for (var i = 0,maxlen=list.length; i < maxlen; i++) {
				if (airportCode == list[i].airportCode) {
					return list[i].cityName;
				}
			}
		}
}

function getAirportNameOld(airportCode) {
/* changing the cityName as City_Code(cityCode) w.r.t the  airportfinder service api version=2*/
	if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);
		for (var i = 0,maxlen=list.length; i < maxlen; i++) {
			if (airportCode == list[i].airportCode) {
				//return list[i].cityName;
                return list[i].cityCode;
			}
		}
	} 
}

/* End of retrieving city name*/

/*Function to return the country name*/
function getCountryName(airportCode) {
	
		if (localStorage.cityList != undefined) {
			var list = JSON.parse(localStorage.cityList);

			for (var i = 0,maxlen=list.length; i < maxlen; i++) {
				if (airportCode == list[i].airportCode) {
					return ((list[i].countryName) + "_" + (list[i].countryCode));
				}
			}
		}
}

function getCountryNameOld(airportCode) {

	if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);

		for (var i = 0,maxlen=list.length; i < maxlen; i++) {
			if (airportCode == list[i].airportCode) {
				return ((list[i].countryName) + "_" + (list[i].countryCode));
			}
		}
	} 
}
function getCountryFullName(airportCode) {

	if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);

		for (var i = 0,maxlen=list.length; i < maxlen; i++) {
			if (airportCode == list[i].airportCode) {
				return (list[i].countryName);
			}
		}
	} 


}




/* End of retrieving country name */
/*Function to return the airport name*/
function getAirportFullName(airportCode) {
	
		if (localStorage.cityList != undefined) {
			var list = JSON.parse(localStorage.cityList);

			for (var i = 0,maxlen=list.length; i < maxlen; i++) {
				if (airportCode == list[i].airportCode) {
					return list[i].airportName;
				}
			}
		}
}

/*Funtion to retrieve Airport name with type - Airport*/

function getAirportFullNameNew(airportCode) {

    if (localStorage.cityList != undefined) {

        var list = JSON.parse(localStorage.cityList);



        for (var i = 0,maxLen=list.length; i < maxLen; i++) {

            if ((airportCode == list[i].airportCode)&&(list[i].locationType != "CTY")) {

                return list[i].airportName;

            }



        }

    }

}

function getAirportFullNameOld(airportCode) {

	if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);

		for (var i = 0,maxlen= list.length; i < maxlen; i++) {
			if (airportCode == list[i].airportCode) {
				return (list[i].airportName);
			}
		}
	} 
}




/* Maintenance Message */

function maintenance_message(){

    var responseMaintenance,enableCheck,maintenanceMessage,urlCMS_maintenance;
    if(localStorage.language=="Pt"){
        urlCMS_maintenance="http://content.skyteam.com/contentapi/api/pages/GetStaticContent?pageURL=/Site/maintenance-message-mobile,pt-BR";
    }
    else if(localStorage.language=="zh-Hant"){
        urlCMS_maintenance="http://content.skyteam.com/contentapi/api/pages/GetStaticContent?pageURL=/Site/maintenance-message-mobile,zh-TW";
    }
    else{
        urlCMS_maintenance="http://content.skyteam.com/contentapi/api/pages/GetStaticContent?pageURL=/Site/maintenance-message-mobile,"+localStorage.language;
    }

    $.ajax({
           type: "GET",
           dataType:'json',
           url: urlCMS_maintenance,
           crossDomain: true,
           async:false,
           headers:{'cmsapikey':'575fbcdd-0e55-4ac8-a7d7-633248fdcd91'},
           success: function(data)
           {
           responseMaintenance=data;
           //alert(data);
           }
           });
    maintenanceMessage=JSON.parse(responseMaintenance);
    if(maintenanceMessage.EnableMessage=="yes"){
        openAlertWithOk(maintenanceMessage.DescriptionText);
    }

}
/* Maintenance Message */





/*Function to return the city name*/
function getAirportCityName(airportCode) {

    if (localStorage.cityList != undefined) {
        var list = JSON.parse(localStorage.cityList);
        for (var i = 0,maxLen=list.length; i < maxLen; i++) {
            if (airportCode == list[i].airportCode) {
                return list[i].cityName;
            }
        }
    }
}
//function to get the Country code and city name based on airport code
function getAirportCityCountryName(airportCode){
	
		if (localStorage.cityList != undefined) {
			var list = JSON.parse(localStorage.cityList);

			for (var i = 0,maxlen=list.length; i < maxlen; i++) {
				if (airportCode == list[i].airportCode) {
					return ((list[i].cityName) + "_" + (list[i].countryCode));
				}
			}
		}
	
}



/* Code for converting date format*/
function convertDate(dateValue) {
	//var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var months=[$.i18n.map.sky_jan,$.i18n.map.sky_feb,$.i18n.map.sky_mar,$.i18n.map.sky_apr,$.i18n.map.sky_may,$.i18n.map.sky_jun,$.i18n.map.sky_jul,$.i18n.map.sky_aug,$.i18n.map.sky_sep,$.i18n.map.sky_oct,$.i18n.map.sky_nov,$.i18n.map.sky_dec];
	var d = dateValue.split('-')[2];

	var y = dateValue.split('-')[0];
	var m = (dateValue.split('-')[1]) > 09 ? ((dateValue.split('-')[1]) - 1) : (((dateValue.split('-')[1]).substring(1)) - 1);
	return (d + ' ' + months[m] + ' ' + y.substring(2, 4));
}

function parseDate(str) {

	var mdy = str.split('-');
	return new Date(mdy[0], mdy[1] - 1, mdy[2]);
}

function daydiff(first, second) {

	return (second - first) / (1000 * 60 * 60 * 24);
}

/* End of date conversion */
/* getter and setters for results */
function setResults(results) {
	res = results;
}

function getResults() {
	return res;
}

/* End of getters & setters */
/* Code for converting temparature format*/
function temparatureConvertor(value) {
	var temp = value;
	var tempFa = temp * (9 / 5) + 32;
	tempF = Math.round(tempFa * 10) / 10;
	return tempF;
}

/* End of temparature conversion*/
/* Code for converting time format*/
function timeConvertor(value) {
	var time = value;
	var hours = time.split(':')[0];
	var min = time.split(':')[1];
	if (hours < 12) {
		time_12h = hours + ':' + min + ' AM';
		return time_12h;
	} else if (hours == 12) {
		time_12h = hours + ':' + min + ' PM';
		return time_12h;
	} else {
		hours = hours - 12;
		if (hours < 10) {
			hours = '0' + hours;
		} else {
			hours = hours;
		}

		time_12h = hours + ':' + min + ' PM';
		return time_12h;
	}
}

/* End of time conversion*/
/* Code for converting distance format*/
function distanceConvertor(value) {
	var distance = value;
	var dist_mi = distance * 0.62137;
	dist_m = Math.round(dist_mi * 10) / 10;
	return dist_m;
}

/* End of distance conversion*/
/* Code for converting date format*/
function convertDateMMM(dateValue) {
	//var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var months=[$.i18n.map.sky_jan,$.i18n.map.sky_feb,$.i18n.map.sky_mar,$.i18n.map.sky_apr,$.i18n.map.sky_may,$.i18n.map.sky_jun,$.i18n.map.sky_jul,$.i18n.map.sky_aug,$.i18n.map.sky_sep,$.i18n.map.sky_oct,$.i18n.map.sky_nov,$.i18n.map.sky_dec];
	var d = dateValue.split('-')[2];

	var y = dateValue.split('-')[0];
	var m = (dateValue.split('-')[1]) > 09 ? ((dateValue.split('-')[1]) - 1) : (((dateValue.split('-')[1]).substring(1)) - 1);
	return (months[m] + ' ' + d + ' ' + y.substring(2, 4));
}

/* End of date conversion*/
/* Code for converting skytips date format*/
function convertSkyTipsDate(dateValue) {
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var d = dateValue.split('/')[1];

	var y = dateValue.split('/')[2];
	var m = (dateValue.split('/')[0]) > 09 ? ((dateValue.split('/')[0]) - 1) : (((dateValue.split('/')[0]).substring(1)) - 1);
	return (d + ' ' + months[m] + ' ' + y);
}

function convertSkyTipsDateMMM(dateValue) {
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var d = dateValue.split('/')[1];

	var y = dateValue.split('/')[2];
	var m = (dateValue.split('/')[0]) > 09 ? ((dateValue.split('/')[0]) - 1) : (((dateValue.split('/')[0]).substring(1)) - 1);
	return (months[m] + ' ' + d + ' ' + y);
}

/* End of skytips date conversion*/
/* Code for converting list to array */
function listToAray(fullString, separator) {
	var fullArray = [];
	if (fullString !== undefined) {
		if (fullString.indexOf(separator) == -1) {
			/*if (fullString == "GA") {
				fullArray.push('');
			} else*/
			fullArray.push(fullString);
		} else {

			fullArray = fullString.split(separator);
		}
	}

	return fullArray;
}

/* End of  list to array conversion*/
/* Code for app back functionality*/

function pushPageOnToBackStack(pageObject) {
	if (sessionStorage) {
		var previousPages;

		if (sessionStorage.previousPages == undefined)
			previousPages = [];
		else
			previousPages = JSON.parse(sessionStorage.previousPages);

		if (previousPages.length > 0)
		{
			if(pageObject.previousPage != previousPages[0].previousPage)
				previousPages.splice(0, 0, pageObject);
		}
		else
			previousPages.push(pageObject);

		sessionStorage.previousPages = JSON.stringify(previousPages);
	}
}

function popPageOnToBackStack() {
	if (sessionStorage) {
		var previousPages = [], pageObject;
		if (sessionStorage.previousPages != undefined)
			previousPages = JSON.parse(sessionStorage.previousPages);

		pageObject = previousPages[previousPages.length - 1];
		previousPages.splice(0, 1);
		sessionStorage.previousPages = JSON.stringify(previousPages);
		return pageObject;
	}
}

function getPagesCountOnToBackStack() {
	var previousPages = [];
	if (sessionStorage.previousPages != undefined)
		previousPages = JSON.parse(sessionStorage.previousPages);
	return previousPages.length;
}


function lineAdjustment(){
	$(".dep_data").each(function(){

		if($(this).index()==0){
			$(".line ul li:first").css("height",$(".dep_data:first").height()-17);
		}
		else
		{
			$(".line ul li:eq("+$(this).index()+")").css("height",$(".dep_data:eq(" + (($(this).index() )/2)+ ")").height()-17);
		}

	});
}
/* End of back functionality */
/* android back functionality code */
function androidHardwareBack() {

	if($("input[type=text]").is(':focus') || $("input[type=tel]").is(':focus'))

	{
		$('.tabarea').css('margin-top','0');
		$("input[type=text]").blur();
		$("input[type=tel]").blur();
		return false;
	}
	if($('#blackshade').css('display')== 'block' && $('#depatureDate-popup').css('display') == 'block')
	{
		$('.depDate-ok').trigger('click');
	}
	else if(openalertsokbuttonFlag)
	{

		$('*').unbind('touchmove');
		$('.popUpShade').hide();
		$('.popUp').hide();
		$('.popUp button:eq(2)').remove();
		$('.popUp button:eq(1)').remove();
	}
	else if($('.popUpShade').css('display') == 'block'){
		$('*').unbind('touchmove');
		$('.popUpShade').hide();
		$('.popUp').hide();
		$('.popUp button:eq(2)').remove();
		$('.popUp button:eq(1)').remove();
	}
	else if($('.modal').css('display') == 'block') {
	}
	else if ($('#rightSideText').html() == "Done") {

	}
	else if($('#blackshade').css('display')== 'block' && $('#tipThemesSelect').css('display') == 'block')
	{
		$('.themes-ok').trigger('click');
		$('#blackshade').hide();

	}
	else if($('#blackshade').css('display')== 'block' && $('.moreOptionsDetails').css('display') == 'block')
	{
		$('#blackshade').hide();


		$('.moreOptionsDetails').hide();
		$('#moreOptions-ok').hide();

		$('.moreOptDiv').remove();
	}
	else if($('#blackshade').css('display')== 'block' && $('#moreOptionsPopup').css('display') == 'block')
	{

		$('.moreOptionsDetails').show();
		$('#moreOptions-ok').show();
		$('#moreOptionsPopup').hide();
		$('#operator-ok').hide()
		$('#blackshade').show();
	}
	else if($('#blackshade').css('display')== 'block' && $('.date-popup').css('display') == 'block')
	{
		$('#blackshade').hide();


		$('.date-popup').empty();

		$('.tripDetails').css('overflow-y', 'auto');
		$('.date-popup').hide();
		$('#ok').hide();

	}

	else if($('#blackshade').css('display')== 'block' && $('#members-popup').css('display') == 'block')
	{
		$('#blackshade').hide();

		$('#members-popup').hide();
		$('.members-ok').hide();
	}
	else if($("#pageslide").css('display') == "block"){
		require(['jquery','pageslide'], function($,pageslide){

			$.pageslide.close();

		});
	}

	else{
		if(sessionStorage.previousPages){
			var prevPage = JSON.parse(sessionStorage.previousPages);
			if(prevPage.length != 0){
				$('.main_header ul li:eq(1) img').trigger('click');
			} else {
				navToHistory();
			}
		} else {
			navToHistory();
		}
	}



	openalertsokbuttonFlag=false;
}
function navToHistory(){
	var page = ["searchFlights", "loungesSearch", "aboutSkyTeam", "settings", "flightstatus", "airportSearchPage", "skyTipsFinder","skyPriorityFinder","addSkyTipsPage"];
	var flagValue = false;
	for (var i = 0,maxlen=page.length; i < maxlen; i++) {

		if (sessionStorage.currentPage === page[i]) {
			flagValue = true;
			sessionStorage.removeItem('previousPages');
		}

	}
	if (flagValue){
		//window.open("app:showActivityIndicator", "_self");
		//setTimeout(function(){
		window.location.href = "index.html#homePage";
		//},50);
	}
	if (sessionStorage.currentPage == "homePage") {

		var obj = {};

		obj.control = "shouldOverrideBackbtn";

		obj.command = "true";

		window.callNative.notifyNative(JSON.stringify(obj));

	}
	return false;
}

/* End of android back functionality code */

/*AutoComplete to make the matched letters bold*/
function toHighlightAutoComplete(val1,val2) {
    displayAllAirports=val1;
    isFromSkyTips=val2;
    var oldFn = $.ui.autocomplete.prototype._renderItem;
    $.ui.autocomplete.prototype._renderItem = function( ul, item) {
        if(item.label == "No result found" ){
            return $( "<li style='background:#eeeeee;'></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        }
        else{
            var re = new RegExp(this.term, "ig") ;
            var t = item.label.replace(re,"<span style='font-weight:bold;'>$&</span>");
            return $( "<li style='background:#eeeeee;'></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
        }
    };


	$.ui.autocomplete.filter = function (array, term) {
		var matcher = new RegExp("^"+ $.ui.autocomplete.escapeRegex(term), "i");
		var airportNameArray=[];
		var airportCodeArray=[];
		var cityNameArray=[];

		/*     var arra = $.grep(array, function (value) {
                      var airportCode,airportName;
                          var row=[];
                          row=value.split(';, ');
                      var val =[];
                      val = row[0].split('| ');
                    var pos = value.indexOf("(") + 1;
                      airportCode=val[0].substr(pos,3);
                      airportName=val[0].split(" (");

                          var showAirport=true;
                          if(displayAllAirports !=undefined && displayAllAirports == true  )
                          {
                            if(!row[1].match("Airport"))
                          {
                          showAirport=false;
                          }
                          }
                          if(showAirport){
                          if(matcher.test(airportName)){

                          airportNameArray.push(row[0].replace('|',','));

                          }
                          else if(matcher.test(airportCode)){
                          airportCodeArray.push(row[0].replace('|',','));
                          }
                          else if(matcher.test(val[1])){
                          cityNameArray.push(row[0].replace('|',','));
                          }
                          else{

                          }
                          }
                          return matcher.test(airportName);// || matcher.test(airportCode) || matcher.test(val[1]);

                          });

		 */

		if(term.length == 1 && checkCJKCompliedLanguage()){
			var regex = /^([a-zA-Z])*$/;
			if(regex.test(term)){
				sessionStorage.keyboardLang="En";
			}
			else
			{
				sessionStorage.keyboardLang=localStorage.language;
			}
		}
		else if(checkCJKCompliedLanguage() !== true)
		{
			sessionStorage.keyboardLang=localStorage.language;
		}
		if(checkCJKCompliedLanguage() && sessionStorage.keyboardLang == "En")
		{
			array.sort(function(a, b) {
				return (a.airportName_En > b.airportName_En) ? 1 : -1;
			});

		}



		var arra = $.grep(array, function (value) {
			var ptrnAirportName=new RegExp("\\b"+term,"i");
			var airportCode,airportName,country;
			airportCode=value.airportCode;
            locType = value.locationType;
			var val;

			if(checkCJKCompliedLanguage() && localStorage.language == "En"){
				airportName=value.airportName_En;
				country = value.countryName_En;
			}
			else if(isFromSkyTips != undefined && isFromSkyTips == true ){
				airportName=value["airportName_"+localStorage.language];
				country = value["countryName_"+localStorage.language];
			}
			else
			{
				airportName=value.airportName;
				country = value.countryName;
			}
			if(isFromSkyTips != undefined && isFromSkyTips == true ){
				val =value["airportName_"+localStorage.language] + ' (' + airportCode + '), '+value["countryName_"+localStorage.language];
			}
			else

			{
				val =value.airportName + ' (' + airportCode + '), '+value.countryName;
				//         val ={value:value.Airport_Name + ' (' + airportCode + '), '+value.countryName,label:Airport_Name + ' (' + airportCode + '), '+value.countryName,Airport_Code:airportCode}
			}
			var obj = {airportCode:airportCode,label:val,locationType:locType};
			var showAllAirports=true;
			if(displayAllAirports !=undefined && displayAllAirports==true){
				if(!value.locationType.match("APT")){
					showAllAirports=false;
				}
			}
			if(showAllAirports){
				if(airportName.match(ptrnAirportName) || matcher.test(airportName)){
					airportNameArray.push(obj);
				}
				else if(matcher.test(airportCode)){
					airportCodeArray.push(obj);
				}
				else if(country.match(ptrnAirportName) || matcher.test(country)){
					cityNameArray.push(obj);
				}
				else
				{
				}
			}
			return matcher.test(airportName);// || matcher.test(airportCode) || matcher.test(val[1]);
		});
		arra=[];
		arra=arra.concat(airportNameArray);
		arra=arra.concat(airportCodeArray);
		arra=arra.concat(cityNameArray);

		if(arra.length == 0){
			var ae=[{label:$.i18n.map.sky_no_results_found,value:""}];
			return ae;
		}else{
			return arra;
		}
	};
};


function firstToUpperCase( str ) {
	return str.substr(0, 1).toUpperCase() + str.substr(1);
}
function fromHereOrToHere(airportCode) {

	if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);

		for (var i = 0,maxlen=list.length; i < maxlen; i++) {
			if (airportCode == list[i].airportCode) {
				return (getAirportFullName(list[i].airportCode)+" ("+list[i].airportCode+"), "+getAirportCountryName(list[i].airportCode) );
			}
		}
	}
}

function getStatusbar(Flight_Detail_Status,status){


        var result= status;

        if(localStorage.language !== "En"){

                if(status == "Scheduled"){

                result=$.i18n.map.sky_scheduled;

            }

            else if(status == "Cancelled"){

                result=$.i18n.map.sky_cancelled;

            }

            else if(status == "Delayed"){

                result=$.i18n.map.sky_delayed;

            }

            else if(status =="On Time"){

                result=$.i18n.map.sky_ontime;

            }

            else if(status == "Landed"){

                result=$.i18n.map.sky_landed;

            }

            else{

                result =Flight_Detail_Status;

            }

        }
 if(result==""){
 result="No Status"
 }
        return result;

    }



function getStatus(status){


        var result= status;

        if(localStorage.language !== "En"){



            if(status == "Scheduled"||status == $.i18n.map.sky_scheduled){

                result=$.i18n.map.sky_scheduled;

            }

            else if(status == "Cancelled"||status == "CX"){

                result=$.i18n.map.sky_cancelled;

            }

            else if(status == "Delayed"||status == "DY"){

                result=$.i18n.map.sky_delayed;

            }

            else if(status =="On Time"||status =="OT"){

                result=$.i18n.map.sky_ontime;

            }

            else if(status == "Landed"||status == "ON"){

                result=$.i18n.map.sky_landed;

            }

            else{

                result ='No Status';

            }

        }

        else if(localStorage.language == "En"){

                    if(status == "Scheduled"||status == $.i18n.map.sky_scheduled){

                        result=$.i18n.map.sky_scheduled;

                    }

                    else if(status == "Cancelled"||status == "CX"){

                        result=$.i18n.map.sky_cancelled;

                    }

                    else if(status == "Delayed"||status == "DY"){

                        result=$.i18n.map.sky_delayed;

                    }

                    else if(status =="On Time"||status =="OT"){

                        result=$.i18n.map.sky_ontime;

                    }

                    else if(status == "Landed"||status == "ON"){

                        result=$.i18n.map.sky_landed;

                    }

                    else{

                        result ='No Status';

                    }

                }

        return result;

    }

/*End */
//function to get the Country code and city name based on airport code
function getAirportCityCountryNameOld(airportCode){
	if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);

		for (var i = 0,maxlen=list.length; i < maxlen; i++) {
			if (airportCode == list[i].airportCode) {
				return ((list[i].cityName) + "_" + (list[i].countryCode));
			}
		}
	}
}

//function to get the Country name based on airport code
function getAirportCountryName(airportCode){

		if (localStorage.cityList != undefined) {
			var list = JSON.parse(localStorage.cityList);

			for (var i = 0,maxlen=list.length; i < maxlen; i++) {
				if (airportCode == list[i].airportCode) {
					return (list[i].countryName);
				}
			}
		} 
}

/* reset the add sky tips page when skytips got posted */
function resetSkyTips(){
	$("#airport_text").val('');
	$('#cancel_skytips').css("visibility","hidden");
	$("#skytip_desc").val('');
	$("#skytip_tite").val('');
	$('#cancel_skytip_tite').css("visibility","hidden");
	$("#skytip_fname").val('');
	$('#cancel_skytip_fname').css("visibility","hidden");
	$("#skytip_lname").val('');
	$('#cancel_skytip_lname').css("visibility","hidden");
	$("#skytip_email").val('');
	$('#cancel_skytip_email').css("visibility","hidden");
	$("#skytip_country").val('');
	$('#cancel_skytip_country').css("visibility","hidden");

	var theme=$.i18n.map.sky_select_theme;
	$(".themeSelectVal").html(theme);
	$('#ThemeSelect p').html(theme);
	$('.scrollforThemes :checked').attr('checked',false);
	$('#checkone').attr('checked',false);
	$('#checktwo').attr('checked',false);

}
//**** start-Function to generate airlines for sky priority **

function getAirlines(response){
	var airlinesArray, radioElement;
	var airlineGroup='';

	airlinesArray=response.split(",");

	radioElement='<label class="radiobutton"><input type="radio" class="groupCheckbox" id="{%airlinenameid}" value="{%value}" name="operatorsGroup" /><span></span></label><h6><span class="airlineName" for="{%airlinename}" >{%name}</span></h6>';

	for(var i=0,maxlen=airlinesArray.length;i<maxlen;i++){
		var airlineName = airlinesArray[i].trim().replace(/\s/g, '');
		airlineGroup=airlineGroup+radioElement.replace('{%value}',airlinesArray[i]).replace('{%airlinenameid}', airlineName).replace('{%airlinename}', airlineName).replace('{%name}',$.i18n.map['sky_'+[airlinesArray[i]]]);
	}

	return airlineGroup;

}

//**** end-Function to generate airlines for sky priority **

function getskyPriorityAdvancedDetailsAjax(history){

	var spFinderAdvanceURL=URLS.SKYPRIORITY_ADVANCE+sessionStorage.skypriorityAdvancedDetails;
	//showActivityIndicatorForAjax('false');
    var acceptlanguage= (localStorage.language == "zh")?'zh-Hans':localStorage.language;
	xhrajax=$.ajax({
		url : spFinderAdvanceURL,
		type: 'GET',
		timeout: 30000,
		async: true,
		 beforeSend: function (request)
			{
				request.setRequestHeader("Accept-Language", acceptlanguage);
				request.setRequestHeader("api_key", API_KEY);
				request.setRequestHeader("source", "SkyApp");
			},
		success: function(response)
		{
			hideActivityIndicator();
			if(response.SkyPriorityEligibility === "Y"){
				sessionStorage.SPAdvance_response= JSON.stringify(response);
				pushPageOnToBackStack(history);
				window.location.href="index.html#skyPriorityAdvancedDetails";
			}
			else{
				openAlertWithOk($.i18n.map.sky_priority_no_benefits);
				//popPageOnToBackStack();
			}
		},
		error: function(xhr,exception){
			if(exception == "timeout"){
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_unload);
				//popPageOnToBackStack();
			}

			else if(exception == "error"){
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_notconnected);
				//popPageOnToBackStack();
			}
			else{

			}
		},
		failure: function(){
			hideActivityIndicator();
			//popPageOnToBackStack();
		}

	});





}
/* Converting date object into YYYY-MM-DD(chinese date format) - start */
Date.prototype.yyyymmdd=function(){
	var date,month;
	month=this.getMonth()+1;
	date =this.getDate();
	month=(month <10)? "0"+month:month;
	date=(date <10)? "0"+date:date;
	return (this.getFullYear()+"-"+month+"-"+date);
}
/* Converting date object into YYYY-MM-DD(chinese date format) - end*/
/* get day from sting YYYY-MM-DD- start */
function getDay_YYYYMMDD(input){
	var inputArray=input.split("-");
	var inputDate=new Date(inputArray[0],inputArray[1],inputArray[2]);
	return inputDate.getDay();
}
/* get day from sting YYYY-MM-DD - end*/

/* Merge city data list */
/*function mergeCityList()
{

	if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
	 var AirportNames = JSON.parse(localStorage.cityList);

     var arrEnglish=AirportNames.slice(0);
     arrEnglish.sort(function(a,b){
     	return (a.airportCode > b.airportCode)? 1 : -1;
     });

var names=[];
	        	for(var k=0; k < arrEnglish.length; k++)
        	{
        			var str=arrEnglish[k].airportName + " (" + arrEnglish[k].airportCode + "), "+arrEnglish[k].countryName+ " ,: "+ arrEnglish[k].airportName + " (" + arrEnglish[k].airportCode + "), "+arrEnglish[k].countryName;
        			names.push(str);
        	}

     names.sort();
     localStorage.cityairportNames = JSON.stringify(names);

	   if (typeof localStorage.cityList_others != "undefined" && localStorage.cityList_others != null && localStorage.cityList_others != "" ){


           var  AirportNames_others = JSON.parse(localStorage.cityList_others);;

           AirportNames_others.sort(function(a,b){
				return (a.airportCode > b.airportCode)? 1 : -1;
			});

           var names_others=[];
           var i=0,j=0;
        	for(i=0; i < arrEnglish.length; i++)
        	{
        		for (j = 0; j < AirportNames_others.length; j++) {
        			var str;
        			if(arrEnglish[i].airportCode == AirportNames_others[j].airportCode)
        			{
        				str=arrEnglish[i].airportName + " (" + arrEnglish[i].airportCode + "), "+arrEnglish[i].countryName+ " ,: "+ AirportNames_others[j].airportName + " (" + AirportNames_others[j].Airport_Code + "), "+AirportNames_others[j].countryName;
        				names_others.push(str);
        				AirportNames_others.splice(j,1);
        				break;
        			}		
      			}
        	}

          names_others.sort();

          localStorage.cityairportNames_others = JSON.stringify(names_others);
	   }
	}

}*/
function mergeCityList()
{

	if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
		var AirportNames = JSON.parse(localStorage.cityList);

		var arrEnglish=AirportNames.slice(0);


		var names=[];
		for(var k=0,maxlen=arrEnglish.length; k < maxlen; k++)
		{
			var str=arrEnglish[k].airportName + " (" + arrEnglish[k].airportCode + ")| "+arrEnglish[k].countryName+";, "+arrEnglish[k].locationType;
			names.push(str);
		}

		names.sort();
		localStorage.cityairportNames = JSON.stringify(names);

		if (typeof localStorage.cityList_others != "undefined" && localStorage.cityList_others != null && localStorage.cityList_others != "" ){


			var  AirportNames_others = JSON.parse(localStorage.cityList_others);



			var names_others=[];
			var i=0,j=0;
			for (j = 0,maxlen=AirportNames_others.length; j < maxlen; j++) {
				var str= AirportNames_others[j].airportName + " (" + AirportNames_others[j].airportCode + ")| "+AirportNames_others[j].countryName+";, "+AirportNames_others[j].locationType;
				names_others.push(str);
			}


			names_others.sort();

			localStorage.cityairportNames_others = JSON.stringify(names_others);
		}
	}

}
/* Ends Merge city data list */
function getDaysInEnglish(value){

	var days= ["MON","TUE","WED","THU","FRI","SAT","SUN"];
	switch(value){

	case $.i18n.map.sky_MON: return days[0];
	break;
	case $.i18n.map.sky_TUE: return days[1];
	break;
	case $.i18n.map.sky_WED: return days[2];
	break;
	case $.i18n.map.sky_THU: return days[3];
	break;
	case $.i18n.map.sky_FRI: return days[4];
	break;
	case $.i18n.map.sky_SAT: return days[5];
	break;
	case $.i18n.map.sky_SUN: return days[6];
	break;
	default:

	}
}
function getDateInLocaleFormat(value,withoutDay){

	//checking passed value is date / string
	if($.type(value) === "string")//if string converting it as date
		value = new Date(value);

	var shortMon=[$.i18n.map.sky_jan,$.i18n.map.sky_feb,$.i18n.map.sky_mar,$.i18n.map.sky_apr,$.i18n.map.sky_may,$.i18n.map.sky_jun,$.i18n.map.sky_jul,$.i18n.map.sky_aug,$.i18n.map.sky_sep,$.i18n.map.sky_oct,$.i18n.map.sky_nov,$.i18n.map.sky_dec];
	//var shortDay=[$.i18n.map.sky_SUN,$.i18n.map.sky_MON,$.i18n.map.sky_TUE,$.i18n.map.sky_WED,$.i18n.map.sky_THU,$.i18n.map.sky_FRI,$.i18n.map.sky_SAT];
	var shortDay=[$.i18n.map.sky_sunday,$.i18n.map.sky_monday,$.i18n.map.sky_tuesday,$.i18n.map.sky_wednesday,$.i18n.map.sky_thursday,$.i18n.map.sky_friday,$.i18n.map.sky_saturday]
	var date,day,month,year;
	date = value.getDate();
	day = shortDay[value.getDay()];
	month=value.getMonth();
	year =value.getFullYear().toString();
	date=(date <10)? "0"+date:date;

	if(localStorage.date == "DD"){
		if(withoutDay != "undefined" && withoutDay){
			return date+" "+shortMon[month]+" "+year.substr(2,2);}
		else{
			return day+" "+date+" "+shortMon[month]+" "+year.substr(2,2);}
	}
	else if(localStorage.date == "MMM"){
		if(withoutDay != "undefined"&& withoutDay){
			return shortMon[month]+" "+date+" "+year.substr(2,2);}	
		else{	
			return day+" "+shortMon[month]+" "+date+" "+year.substr(2,2);}
	}
	else{//for yyyy-mm-dd
		month = month+1;
		month=(month <10)? "0"+month:month;
		if(withoutDay != "undefined" && withoutDay){
			return value.getFullYear()+"-"+month+"-"+date;}
		else{
			return day+" "+value.getFullYear()+"-"+month+"-"+date;}
	}
}
function getTravelTime(input)
{

	input=input.toLowerCase();
//	if(checkCJKCompliedLanguage())
//	{
		return input.replace("d",$.i18n.map.sky_daysshort).replace("h",$.i18n.map.sky_hoursshort).replace("m",$.i18n.map.sky_minshort);

//	}
//	else{//for english & spanish
//		return input;
//	}
}
function getDateInDatePicker(value){

	var shortMon=[$.i18n.map.sky_jan,$.i18n.map.sky_feb,$.i18n.map.sky_mar,$.i18n.map.sky_apr,$.i18n.map.sky_may,$.i18n.map.sky_jun,$.i18n.map.sky_jul,$.i18n.map.sky_aug,$.i18n.map.sky_sep,$.i18n.map.sky_oct,$.i18n.map.sky_nov,$.i18n.map.sky_dec];
	var shortDay=[$.i18n.map.sky_SUN,$.i18n.map.sky_MON,$.i18n.map.sky_TUE,$.i18n.map.sky_WED,$.i18n.map.sky_THU,$.i18n.map.sky_FRI,$.i18n.map.sky_SAT];
	//var shortDay=[$.i18n.map.sky_sunday,$.i18n.map.sky_monday,$.i18n.map.sky_tuesday,$.i18n.map.sky_wednesday,$.i18n.map.sky_thursday,$.i18n.map.sky_friday,$.i18n.map.sky_saturday]
	var date,day,month,year;
	date = value.getDate();
	day = shortDay[value.getDay()];
	month=value.getMonth();
	year =value.getFullYear().toString();
	date=(date <10)? "0"+date:date;

	if(localStorage.date == "DD"){
		return day+" "+date+" "+shortMon[month]+" "+year.substr(2,2);}

	else if(localStorage.date == "MMM"){
		return day+" "+shortMon[month]+" "+date+" "+year.substr(2,2);}

	else{//for yyyy-mm-dd
		month = month+1;
		month=(month <10)? "0"+month:month;
		return day+" "+value.getFullYear()+"-"+month+"-"+date;}

}



/*instead of getAirportNameOld()*/
function getAirportFinderURL(airportCode){
   
    if (localStorage.cityList != undefined) {
		var list = JSON.parse(localStorage.cityList);
		for (var i = 0,maxlen=list.length; i <maxlen ; i++) {
			if (airportCode == list[i].airportCode) {
				return URLS.AIRPORT_DETAILS+"airportcode="+airportCode+"&city="+list[i].cityCode+"&countrycode="+list[i].countryCode;
			}
		}
	}
}


/*Get Status for Flight - Logic */
function getStatusForFlight(statusArray){
    //debugger


    var result="NS";

    var lengthArray=statusArray.length;

    var lastIndex=lengthArray-1;

    if(jQuery.inArray("CX", statusArray) != -1)
    {
        result="CX";

    }
    else if(jQuery.inArray("DY", statusArray) != -1)
    {
        result="DY";

    }
    else if(statusArray[lastIndex]=="ON")
    {
        result="ON";

    }
    else if(statusArray[lastIndex]=="OT")
    {
        result="OT";

    }
    else if(jQuery.inArray("NS", statusArray) != -1)
    {
        result="NS";

    }
    else if(jQuery.inArray("OT", statusArray) != -1)
    {
        result=$.i18n.map.sky_ontime;

    }
    else{
        result="NS";

    }
    return result;

}
function renderStatusfromAPI(res){
    var savedData;
    //alert("renderStatusfromAPI");
    //debugger
    savedData=JSON.parse(res);
    if (savedData.SavedFlights_test != "ERROR") {
        //Getting Live Status from API


       //debugger
        var StatusCode="NS";
        var completeStatus="No Status";



                var listofStatusURL= savedData.SavedFlights_test[0].FlightStatusURL .split(',');
                var countofStatusURL=listofStatusURL.length;

                if(countofStatusURL > 0)
                {

                    var arrIndividualStatus=new Array();
                    for(j=0; j<countofStatusURL; j++)
                    {
                        var url=listofStatusURL[j];
                        var AcceptLanguage=localStorage.language;
                        var responseR='';

                        if(AcceptLanguage == "zh"){
                            AcceptLanguage='zh-Hans';
                        }
                        //var k=0;

                        (function(j)
                         {
                        $.ajax({
                               type: "GET",
                               dataType:'json',
                               url: url,
                               timeout:30000,
                               crossDomain: true,
                               async:true,
                               headers:{'api_key':'3mr52xszzjkp854j4wjshwmz','Accept-Language':AcceptLanguage},
                               success: function(data)
                               {
                               responseR=data;

                               if(responseR.CurrentStatusCode)
                               {
                               arrIndividualStatus[j]=(responseR.CurrentStatusCode);

                               }
                               //alert(data);
                               if(arrIndividualStatus.length==countofStatusURL){

                                    StatusCode=getStatusForFlight(arrIndividualStatus);

                                    completeStatus=getStatus(StatusCode);

                               //alert("Status URL available");
                               $('#statusSection').html(completeStatus);

                               if((StatusCode == "DY") || (StatusCode == "CX")){
                               $('#statusSection').css('color','#D7862F');

                               }
                               else {
                               $('#statusSection').css('color','#0B1761');

                               }
                               }


                               },error: function( xhr, exception ) {

                                 						$('#statusSection').html("No Status");
                                 						$('#statusSection').css('color','##0B1761');


                                 						}
                               });

                         })(j);
                        //
                        //                    if(responseR.CurrentStatusCode)
                        //                    {
                        //                        arrIndividualStatus[j]=responseR.CurrentStatusCode;
                        //                    }

                    }


                }
                else{
                    $('#statusSection').html(completeStatus);
                    if((StatusCode == "DY") || (StatusCode == "CX")){
                        $('#statusSection').css('color','#D7862F');
                    }
                    else {
                        $('#statusSection').css('color','#0B1761');
                    }
                }







        //End of Getting Live Status from API


        //Yellow Colour Code for Negative Case
//        if((StatusCode == "DY") || (StatusCode == "CX")){
//            $('#statusSection').css('color','#D7862F');
//        }
//        else {
//            $('#statusSection').css('color','#0B1761');
//        }
    }

    //$('#statusSection').html("No Status");



}




/*Function to check valid string values */
function isValid(value){
return !( value === ""  || value === "undefined" || $.type(value) === "undefined"  || $.type(value) === "null");
}
/*Function to check valid string values */

/*
 *param:search flights list
 *return :
 *		true if user prefered for next 6 days flight when no flight available for the current day
 *		otherwise false
 */
function openSixDaysSuggestion(list) {
    openalertsokbuttonFlag=true;
    $('*').bind('touchmove', function(e) {
                e.preventDefault();
                });
    $('.popUpShade').show();
    $('.popUp').show();

    $('.popUpMessage1').html($.i18n.map.sky_notavailable);
    
    $('.popUpMessage1').css('text-align','center');
    $('.popUpShade').css({
                         opacity : 0.5,
                         
                         });
    var buttons='</button><button id="noButton"></button><button id="yesButton">';
    $('.popUp .popup_button').html(buttons);
    $('.popup_button #yesButton').text($.i18n.map.sky_yes);
    $('.popup_button #noButton').text($.i18n.map.sky_no);
    
    $('.popUp button').css('disabled','false');
    $('.popUp button').click(function(e) {
                             
                             e.preventDefault();
                             e.stopPropagation();
                             $('*').unbind('touchmove');
                             $('.popUpShade').hide();
                             $('.popUp').hide();
                             $('.popUp button:eq(2)').remove();
                             $('.popUp button:eq(1)').remove();
                             if(this.id === "noButton"){
                             $('#noButton').attr('id','okButton');
                             sessionStorage.sixDays=false;
                             }
                             else if(this.id === "yesButton"){
                             $('#noButton').attr('id','okButton');
                             //sessionStorage.Res =JSON.stringify(list.Connections);
                             sessionStorage.sixDays="yes";
                             window.location.href = 'index.html#searchResults';
                             

                             
                             }
                             });
    
}
/*
 *param: flight search results list,day
 *return :
 *		true if its find atleast one result for the day
 *		otherwise false
 */
function checkFlightForTheDay(list,dayString){
    if(list.trips instanceof Array){
        for(var i=0,maxConn=list.trips.length;i<maxConn;i++){//multiple connections
            var connectionObj = list.trips[i];
            if(connectionObj.runOnDays instanceof Array){//more than one day run
                for(var j=0 , maxLen=connectionObj.runOnDays.length; j < maxLen;j++){
                    if(dayString == connectionObj.runOnDays[j]){
                        return  "true";
                    }
                }
            }
            else{
				if(dayString == connectionObj.runOnDays){//one day run
					return  "true";
				}
            }
        }
    }
    else{//single connection
		var connectionObj = list.trips ;
		if(connectionObj.runOnDays instanceof Array){//more than one day run
			for(var j=0,maxLen=connectionObj.runOnDays.length;j<maxLen;j++){
				if(dayString == connectionObj.runOnDays[j]){
					return "true";
				}
			}
		}
		else{
            if(dayString == connectionObj.runOnDays){//one day run
                return "true";
            }
		}
	}
    return "false";
}
function getAirlineForFlightNum(value){
	var flightNum=[];
	flightNum=value.split("/");
	var flightnumLength = flightNum.length;
	var flightStr = "";
	var flightNumStr= "";
	var airlineStr="";
	if(flightnumLength > 1)
	{
		var count=0;
		for(var i=0,maxlen=flightnumLength; i < maxlen; i++)
		{
			if(count < flightnumLength && count !=0)
			{
				flightNumStr +="/";
				
			}
			flightNumStr += flightNum[i];

			
			if(count !=0 && flightNum[count].substr(0,2) != flightNum[count-1].substr(0,2))
			{
				airlineStr+= "/"+getAirlineName(flightNum[i].substr(0,2));
			}else if(count==0){
				airlineStr += getAirlineName(flightNum[i].substr(0,2));
			}
			count++;
		}
		flightStr=$.i18n.map.sky_flights_searchResults;

		return flightStr+": "+flightNumStr+" - "+airlineStr;
	}
	else
	{
		flightStr=$.i18n.map.sky_flight_searchResults;

		return  flightStr+": "+flightNum[0]+" - "+getAirlineName(flightNum[0].substr(0,2))
	}
	
}
function getAirlineName(value)
{
	var airlineName;
	switch(value)
	{
		case 'SU':
			airlineName= $.i18n.map.sky_SU;
			break;
		case 'AR':
			airlineName= $.i18n.map.sky_AR;
			break;
		case 'AM':
			airlineName=$.i18n.map.sky_AM;
			break;
		case 'UX':
			airlineName=$.i18n.map.sky_UX;
			break;
		case 'AF':
			airlineName=$.i18n.map.sky_AF;
			break;
		case 'AZ':
			airlineName=$.i18n.map.sky_AZ;
			break;
		case 'CI':
			airlineName=$.i18n.map.sky_CI;
			break;
		case 'MU':
			airlineName=$.i18n.map.sky_MU;
			break;
		case 'CZ':
			airlineName=$.i18n.map.sky_CZ;
			break;
		case 'OK':
			airlineName=$.i18n.map.sky_OK;
			break;
		case 'DL':
			airlineName=$.i18n.map.sky_DL;
			break;
		case 'KQ':
			airlineName=$.i18n.map.sky_KQ;
			break;
		case 'KL':
			airlineName=$.i18n.map.sky_KL;
			break;
		case 'KE':
			airlineName=$.i18n.map.sky_KE;
			break;
		case 'ME':
			airlineName=$.i18n.map.sky_ME;
			break;
		case 'SV':
			airlineName=$.i18n.map.sky_SV;
			break;
		case 'RO':
			airlineName=$.i18n.map.sky_RO;
			break;
		case 'VN':
			airlineName=$.i18n.map.sky_VN;
			break;
		case 'MF':
			airlineName=$.i18n.map.sky_MF;
			break;
		case 'GA':
			airlineName=$.i18n.map.sky_GA;
			break;
		default:
			airlineName= "nil";
	}
	return airlineName;
}

/*
 * Function: to get the path of the image for given theme
 * Param: theme
 * Return param : path of the image for the theme	
 */
function getImagePathForTheme( theme )
{
	var imgpath;
	switch( theme )
		{
			case $.i18n.map.sky_e_connectivity:
				imgpath = 'images/skytips/e-connect.png';
				break;
			case $.i18n.map.sky_fast_track:
				imgpath = 'images/skytips/fastforward.png';
				break;
			case $.i18n.map.sky_food_drink:
				imgpath = 'images/skytips/food&drink.png';
				break;
			case $.i18n.map.sky_leisure:
				imgpath = 'images/skytips/leisure.png';
				break;
			case $.i18n.map.sky_lounge_theme:
				imgpath = 'images/skytips/lounges.png';
				break;
			case $.i18n.map.sky_other:
				imgpath = 'images/skytips/other.png';
				break;
			case $.i18n.map.sky_relaxation:
				imgpath = 'images/skytips/relax.png';
				break;
			case $.i18n.map.sky_shopping:
				imgpath = 'images/skytips/shopping.png';
				break;
			case $.i18n.map.sky_transportation:
				imgpath = 'images/skytips/transportation.png';
				break;
			default:
				imgpath = 'images/skytips/other.png';
				break;
	 }
	return imgpath;
}


/** function to remove background/src images **/
/*
 * Purpose:
 * Param:DOM element,which contain image
 * 
 */
function removeBackgroundImage(element){
	setTimeout(function () {
		 delete element.attr("background-image");
		 element = null;
		}, 0);
}

///* function for validating & getting the airport code from suggestion text */
//function validateAirportTextField ( suggestionText, airportCode ) {
//	var db = TAFFY( localStorage.cityList );
//	var response = { isValid: false, Code: '', Name: '' };
//	var result = db(). filter({Airport_Code:airportCode}).get();
//	if(result.length === 1 && (  suggestionText === (result[0].airportName+" ("+airportCode+"), "+result[0].countryName)   )){
//		response.isValid = true;
//		response.Code = airportCode;
//		response.Name = result[0].airportName;
//	}
//	return response;
//}


/* function for validating & getting the airport code from suggestion text for skytips */

function validateAirportTextFieldForSkyTips ( suggestionText, airportCode ) {

	var tempCityList =  JSON.parse(localStorage.cityList_Skytips);
	var db = TAFFY( JSON.stringify(tempCityList.AirPorts_test) );
	var response = { isValid: false, Code: '', Name: '' };
	var result = db(). filter({Airport_Code:airportCode}).get();

	if(result.length === 1 && (  suggestionText === (result[0]["airportName"+localStorage.language]+" ("+airportCode+"), "+result[0]["countryName_"+localStorage.language])   ) ){
		response.isValid = true;
		response.Code = airportCode;
		response.Name = result[0]["Airport_Name_"+localStorage.language];
	}
	return response;
}


/* function for validating & getting the airport code from suggestion text */
function skytipsvalidateAirportTextField ( suggestionText ) {

    //ios 9 has issues with Taffy
    //var db = TAFFY( localStorage.cityList );

    var db = JSON.parse(localStorage.cityList_Skytips).AirPorts_test;


    var filterIndex = 0;
    var response = { isValid: false, Code: '', Name: '' };

    do {
        filterIndex = suggestionText.indexOf( "(" ,filterIndex);


        if(filterIndex !== -1){
            filterIndex = filterIndex + 1;

            var code = suggestionText.substr( filterIndex, 3 );

            //var result = db(). filter({airportCode:code}).get();

            var result = db.filter(function (a) {return a.airportCode === code });



            var airportNameKey = "airportName_"+localStorage.language;
            var countryNameKey = "countryName_"+localStorage.language;
            if(result.length === 1 && (  code.toUpperCase().toString() === (result[0].airportCode.toUpperCase().toString().toString()))){
                    response.isValid = true;
                    response.Code = code;
                response.Name = (result[0][airportNameKey].toString()+" ("+code+"), "+result[0][countryNameKey].toString());
                    break;

                }




            //            if(result.length === 1 && (  suggestionText === (result[0].airportName+" ("+code+"), "+result[0].countryName)   )){
            //                response.isValid = true;
            //                response.Code = code;
            //                response.Name = result[0].airportName;
            //                break;
            //
            //            }



        }

    } while(filterIndex > 0);

    return response;
}



/* function for validating & getting the airport code from suggestion text */
function validateSkyTipsAirportTextField ( suggestionText ) {

    
    //    var skyTipsList =JSON.parse(localStorage.cityList_Skytips).AirPorts_test;
    //    var db = TAFFY( skyTipsList );
    
    var db =JSON.parse(localStorage.cityList_Skytips).AirPorts_test;
    
    var filterIndex = 0;
    var response = { isValid: false, Code: '', Name: '' };
    
    do {
        filterIndex = suggestionText.indexOf( "(", filterIndex );
        
        if(filterIndex !== -1){
            filterIndex = filterIndex + 1;
            
            var code = suggestionText.substr( filterIndex, 3 );
            
            // var result = db(). filter({airportCode:code}).get();
            
            var result = db.filter(function (a) {return a.airportCode === code })
            
            var airportNameKey = "airportName_"+localStorage.language;
            var countryNameKey = "countryName_"+localStorage.language;
            if(result.length === 1 && (  suggestionText === (result[0][airportNameKey]+" ("+code+"), "+result[0][countryNameKey])   )){
                response.isValid = true;
                response.Code = code;
                response.Name = result[0].airportName;
                break;
                
            }
        }
        
    } while(filterIndex > 0);
    
    return response;
}
/* Function to add the page transition Animation */
function slideThePage(direction)
{
   if(sessionStorage.transRequired == undefined || sessionStorage.transRequired === "Yes"){
    
    $('.dummy').attr('class', 'dummy page');
        if(direction == "Left")
        {
            $('#skyteam').attr('class', 'slide in reverse');
        }
        else
        {
            $('#skyteam').attr('class', 'slide in ');
        }
        
        
        
        setTimeout(function() {
                   $(".dummy").remove();
                    $('#skyteam').attr('class', '');
                  // $('.search_btn').attr('class','search_btn positionFixed');
        }, 342);
   }
    sessionStorage.navDirection = "Right";
    sessionStorage.transRequired ="Yes";

}
function hasValue (value) {
    return !(_.isNull(value) || _.isUndefined(value) || (_.isString(value) && $.trim(value) === '') || (_.isArray(value) && _.isEmpty(value)));
};

function getSkyTipImage(TipTheme){

var imgName ="images/skytips/" ;
			if(TipTheme === $.i18n.map.sky_e_connectivity ){
				imgName = imgName+"e-connect.png";
			}
			else if(TipTheme === $.i18n.map.sky_food_drink){
				imgName = imgName+"fastforward.png";
			}
			else if(TipTheme === $.i18n.map.sky_food_drink){
				imgName = imgName+"food&drink.png";
			}
			else if(TipTheme === $.i18n.map.sky_lounge_theme){
				imgName = imgName+"lounges.png";
			}
			else if(TipTheme === $.i18n.map.sky_relaxation){
				imgName = imgName+"relax.png";
			}
			else if(TipTheme === $.i18n.map.sky_shopping){
				imgName = imgName+"shopping.png";
			}
			else if(TipTheme === $.i18n.map.sky_transportation){
				imgName = imgName+"transportation.png";
			}
			else{
				imgName = imgName+"other.png";
			}
return imgName
}


/*
 *Function  to get the tip theme name from tip theme id
 *Param: tiptheme id
 *return tiptheme name
 */
function getTipThemeName(themeId){
    var tipThemeID =0;
    switch (themeId) {
        case "1":
        tipThemeID = $.i18n.map.sky_fast_track;// 1
        break;
        case "3":
        tipThemeID = $.i18n.map.sky_food_drink;// 3
        break;
        case "4":
        tipThemeID = $.i18n.map.sky_relaxation;// 4
        break;
        case "5":
        tipThemeID = $.i18n.map.sky_transportation;// 5
        break;
        case "6":
        tipThemeID = $.i18n.map.sky_leisure;// 6
        break;
        case "7":
        tipThemeID = $.i18n.map.sky_e_connectivity;// 7
        break;
        case "8":
        tipThemeID = $.i18n.map.sky_shopping;// 8
        break;
        case "17":
        tipThemeID = $.i18n.map.sky_other;// 9
        break;
        case "18":
        tipThemeID = $.i18n.map.sky_lounge;//18
        break;
        default:
        tipThemeID = $.i18n.map.sky_other;

    }
    return tipThemeID;
}
/* Function  to get the tip theme name from tip theme id*/

/* Function to check whether the language is CJK complied */
function checkCJKCompliedLanguage()
{
    if(localStorage.language === "zh" || localStorage.language === "Ja" || localStorage.language === "Ko"|| localStorage.language === "zh-Hant"){
        return true;
    }
    return false;
}
/* End - Function to check whether the language is CJK complied */
/*
 *Function  to get the tip theme name array from  array of tip theme id
 *Param: tiptheme id/array of theme id's
 *return array of tiptheme name's
 */
function getSetOfTipThemeName(themeIds){

    var tipThemeNames=[];
    if(_.isArray(themeIds)){//if its not an array then converting to array
        themeIds=$.makeArray(themeIds);
    }
    for(var i=0,len=themeIds.length;i<len;i++){
        tipThemeNames.push(getTipThemeName(themeIds[i]));
    }
    return tipThemeNames;
}
/*Function  to get the tip theme name array from  array of tip theme id*/

/* making weather details service call */
function openWeatherInfoPage()
{
    var results=null;
    var obj = JSON.parse(sessionStorage.airportDetailsObj);
    var language = (localStorage.language === "zh") ? "zh-Hans" : localStorage.language;
    xhrajax=$.ajax({

       url:URLS.WEATHER_DETAILS+'airportcode='+obj.airportCode+'&countrycode='+obj.countryCode,
       datatype:JSON,
       timeout: 500,
       async: false,
       headers:{'api_key':API_KEY, 'source': 'SkyApp', 'Accept-Language' : language},
       success:function(results){
           results = JSON.parse(results);
           sessionStorage.weatherDetailsResult = JSON.stringify(results);
           $('.main_header ul li:eq(1) img').off('click');
           window.location.href = 'index.html#weatherDetails';
       },

       error: function(xhr,exception,status){
            hideActivityIndicator();
           if(exception == "timeout")
           {
                openAlertWithOk($.i18n.map.sky_unload);
           }
           else if(status != 'abort'){
                openAlertWithOk($.i18n.map.sky_retrievefailed);
           }
           else {}
            popPageOnToBackStack();

       },
       failure: function(){
           hideActivityIndicator();
            popPageOnToBackStack();
       }
    });
}

function loadCustomCss(){

    if($('head').find('#customCss')){
        $('head').find('#customCss').remove();
    }

    switch(localStorage.language)
       {
           case "Ru":
           case "It":
           case "Ru":
           case "Fr":
           case "Ar":
           case "Vi":
           case "De":
           case "Pt":
               $('head').append('<link id="customCss" rel="stylesheet" href="css/styles/styles_'+localStorage.language+'.css">');
               break;
           default:
               break;
      }
}
/* Function to get the Flight Status Details from Barcode Scanning */
iSU.getFlightDetailsFromScan = function (nativeResponse){
    if(sessionStorage.currentPage === "scanPage"){
    sessionStorage.flightStatusUrl ='';
    var barCodeType = nativeResponse.symbology;

    barCodeType = barCodeType.toUpperCase();

    if(_.contains(APP_SUPPORTED_BARCODES,barCodeType)){

    var barCodeString,numberOfLegs;
    barCodeString = nativeResponse.barcode;


    var orgin, destination, dateOfFlight;
    var flights=[],connectionAirports =[];
    if (barCodeString.charAt(0) == '<') {
        barCodeString = barCodeString.substring(1, barCodeString.length - 1);
    }

    //Converting to Upper case
    barCodeString = barCodeString.toUpperCase();

    if(barCodeString.length >= 58) {

        if (barCodeString.charAt(1) == '1') {//One leg
            flights.push(parseFirstLegData(barCodeString));
            numberOfLegs=1;
        }
        else{
            numberOfLegs=barCodeString.charAt(1);
            var ItemSixHex = barCodeString.substr(58, 2);
            var ItemSixDec = parseInt(ItemSixHex, 16);
            var firstLeg = barCodeString.substring(0, 60 + ItemSixDec);
            flights.push(parseFirstLegData(barCodeString));

            if (barCodeString.charAt(1) == '2') {//Two leg

                var fromSecondLeg = barCodeString.substring(60 + ItemSixDec,
                                                            barCodeString.length);

                var SecondItemSixHex = fromSecondLeg.substring(35, 37);
                var SecondItemSixDec = parseInt(SecondItemSixHex, 16);
                var secondLeg = fromSecondLeg.substring(0, 37 + SecondItemSixDec);

                flights.push(getFlightNumberFromBarCode(secondLeg));

            }
            else if (barCodeString.charAt(1) == '3') {
                var	fromSecondLeg = barCodeString.substring(60 + ItemSixDec,
                                                            barCodeString.length);
                var SecondItemSixHex = fromSecondLeg.substring(35, 37);
                var SecondItemSixDec = parseInt(SecondItemSixHex, 16);
                var secondLeg = fromSecondLeg.substring(0, 37 + SecondItemSixDec);

                connectionAirports.push(secondLeg.substring(10, 13));

                flights.push(getFlightNumberFromBarCode(secondLeg));

                var secondLegLength = secondLeg.length;
                var fromThirdLeg = barCodeString.substring(60 + ItemSixDec
                                                           + secondLegLength, barCodeString.length);
                var thirdItemSixHex = fromThirdLeg.substring(35, 37);
                var thirdItemSixDec = parseInt(thirdItemSixHex, 16);
                var thirdLeg = fromThirdLeg.substring(0, 37 + thirdItemSixDec);

                flights.push(getFlightNumberFromBarCode(thirdLeg));
            }
            else if (barCodeString.charAt(1) == '4') {
                var fromSecondLeg = barCodeString.substring(60 + ItemSixDec,
                                                            barCodeString.length);
                var SecondItemSixHex = fromSecondLeg.substring(35, 37);
                var SecondItemSixDec = parseInt(SecondItemSixHex, 16);
                var secondLeg = fromSecondLeg.substring(0, 37 + SecondItemSixDec);

                flights.push(getFlightNumberFromBarCode(secondLeg));

                var secondLegLength = secondLeg.length;
                var fromThirdLeg = barCodeString.substring(60 + ItemSixDec
                                                           + secondLegLength, barCodeString.length)
                var thirdItemSixHex = fromThirdLeg.substring(35, 37);
                var thirdItemSixDec = parseInt(thirdItemSixHex, 16);
                var thirdLeg = fromThirdLeg.substring(0, 37 + thirdItemSixDec);

                flights.push(getFlightNumberFromBarCode(thirdLeg));

                var thirdLegLength = thirdLeg.length;
                var fromFourthLeg = barCodeString.substring(60 + ItemSixDec
                                                            + secondLegLength + thirdLegLength, barCodeString.length)
                var fourthItemSixHex = fromFourthLeg.substring(35, 37);
                var fourthItemSixDec = parseInt(fourthItemSixHex, 16);
                var fourthLeg = fromFourthLeg.substring(0, 37 + fourthItemSixDec);

                flights.push(getFlightNumberFromBarCode(fourthLeg));
            }
            else if (barCodeString.charAt(1) == '5') {

                var fromSecondLeg = barCodeString.substring(60 + ItemSixDec,
                                                            barCodeString.length);
                var SecondItemSixHex = fromSecondLeg.substring(35, 37);
                var SecondItemSixDec = parseInt(SecondItemSixHex, 16);
                var secondLeg = fromSecondLeg.substring(0, 37 + SecondItemSixDec);

                flights.push(getFlightNumberFromBarCode(secondLeg));

                var secondLegLength = secondLeg.length;
                var fromThirdLeg = barCodeString.substring(60 + ItemSixDec
                                                           + secondLegLength, barCodeString.length)
                var thirdItemSixHex = fromThirdLeg.substring(35, 37);
                var thirdItemSixDec = parseInt(thirdItemSixHex, 16);
                var thirdLeg = fromThirdLeg.substring(0, 37 + thirdItemSixDec);

                flights.push(getFlightNumberFromBarCode(thirdLeg));

                var thirdLegLength = thirdLeg.length;
                var fromFourthLeg = barCodeString.substring(60 + ItemSixDec
                                                            + secondLegLength + thirdLegLength, barCodeString.length)
                var fourthItemSixHex = fromFourthLeg.substring(35, 37);
                var fourthItemSixDec = parseInt(fourthItemSixHex, 16);
                var fourthLeg = fromFourthLeg.substring(0, 37 + fourthItemSixDec);

                flights.push(getFlightNumberFromBarCode(fourthLeg));


                var fourthLegLength = fourthLeg.length;
                var fromFifthLeg = barCodeString.substring(60 + ItemSixDec
                                                           + secondLegLength + thirdLegLength + fourthLegLength,
                                                           barCodeString.length)
                var fifthItemSixHex = fromFifthLeg.substring(35, 37);
                var fifthItemSixDec = parseInt(fifthItemSixHex, 16);
                var fifthLeg = fromFifthLeg.substring(0, 37 + fifthItemSixDec);


                flights.push(getFlightNumberFromBarCode(fifthLeg));

            }

        }

    }

        /* number of legs should be integer
         * number of legs should be greater than 0 & less than 6
         * should have atleast one flight object
         *  with carrier and date & data length should be 10(YYYY-MM-DD)
         */
        if(numberOfLegs && $.isNumeric(numberOfLegs) && (parseInt(numberOfLegs) >0) && (parseInt(numberOfLegs) <=5) && hasValue(flights) && (flights.length >0) && hasValue(flights[0].carrier) && hasValue(flights[0].date) && flights[0].date.length ===10 ){
            try{
                var journeyDate = new Date(flights[0].date);
                var today = new Date();
                var datDiff = journeyDate.getDate()- today.getDate();

                //journer date should be either yesterday/today/tomorrow
                //if((journeyDate.getMonth() === today.getMonth()) && (Math.abs(datDiff) <=1 ) ){//date range check
                sessionStorage.statusFor = "flightNumber";
                numberOfLegs = parseInt(numberOfLegs);
                sessionStorage.flightStatusUrl = URLS.FLIGHT_STATUS;
//                if(1 === numberOfLegs){
//                    sessionStorage.flightStatusUrl = sessionStorage.flightStatusUrl+'flightnumber=' + flights[0].carrier + '&departuredate=' + flights[0].date;
//                }
//                else{
                    for(var i=0;i<numberOfLegs;i++){
                        if(i==0){
                        sessionStorage.flightStatusUrl = sessionStorage.flightStatusUrl+'flightnumber='+flights[i].carrier+'&departuredate='+flights[i].date+'&origin='+flights[i].orgin+'&destination='+flights[i].destination;
                        }
                        else{
                        sessionStorage.flightStatusUrl = sessionStorage.flightStatusUrl+'&flightnumber='+flights[i].carrier+'&departuredate='+flights[i].date+'&origin='+flights[i].orgin+'&destination='+flights[i].destination;
                        }
                    }
//                }
                window.open(iSC.flightstatusURL, "_self");
                /*}
                else{//date range failed
                openAlertWithOk("Flight status is not available for the scanned boarding pass");
                }*/
            }
            catch(e){//
                openAlertWithOk($.i18n.map.sky_invalid_barcode);
                 window.open(iSC.closeScanView, "_self");
                 sessionStorage.currentPage = "flightstatus";
            }

        }//filter condition end
        else{
        openAlertWithOk($.i18n.map.sky_invalid_barcode);
         window.open(iSC.closeScanView, "_self");
         sessionStorage.currentPage = "flightstatus";
        }
    }
    else{//bar cide type is not supported format
        openAlertWithOk($.i18n.map.sky_invalid_barcode);
         window.open(iSC.closeScanView, "_self");
         sessionStorage.currentPage = "flightstatus";
    }
    }
 }

/***************************************/

function JulianToCalanderdate(jdate) {

    var calanderDate;
    var jd = Math.floor(jdate);
    if (jd > 0) {
        var date = new Date();
        year = date.getFullYear();
        var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        var m_names = ["01", "02", "03", "04", "05", "06",
                       "07", "08", "09", "10", "11", "12"];

        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)){
            days[1] = 29;
        }

        var caldate = 0;
        for ( var i = 0; i < 12; i++) {
            if (caldate < jd) {
                caldate += days[i];
            } else
                break;
        }

        var tempday = caldate - jdate;
        var day = days[i - 1] - tempday;
        var deptDate = "";
        if (day < 10) {
            var deptDate =   year + "-" + m_names[i - 1] + "-" + "0" + day ;
        } else {
            var deptDate =  year + "-" + m_names[i - 1]  + "-" + day ;
        }

        calanderDate = deptDate;
    } else

    {
    }
    return calanderDate;
}

function parseFirstLegData(legData){

    var departureStation = legData.substring(30, 33);
    var arrivalStation = legData.substring(33, 36);
    var msgOperatingAirline = legData.substring(36, 39).replace(/\s+/g, "");
    var flightNumber = legData.substring(39, 44).replace(/\s+/g, "");
    var dateOfFlight = legData.substring(44, 47).replace(/\s+/g, "");
    var travellingdate = JulianToCalanderdate(dateOfFlight);
    return {orgin:departureStation,destination:arrivalStation,carrier:msgOperatingAirline+flightNumber,date:travellingdate};
}

function getFlightNumberFromBarCode(legData)
{
    var departureStation = legData.substring(7, 10);
    var arrivalStation = legData.substring(10, 13);
    var msgOperatingAirline = legData.substring(13, 16).replace(/\s+/g, "");
    var flightNumber = legData.substring(16, 21).replace(/\s+/g, "");
    var dateOfFlight = legData.substring(21, 24).replace(/\s+/g, "");
    var travellingdate = JulianToCalanderdate(dateOfFlight);

    return {orgin:departureStation,destination:arrivalStation,carrier:msgOperatingAirline+flightNumber,date:travellingdate};
}
/* Function to get the Flight Status Details from Barcode Scanning */
function getAirlineNameForFlightNumber(value){
	var flightNum=[];
	flightNum=value.split("/");
	var flightnumLength = flightNum.length;
	var flightNumStr= "";
	var airlineStr="";
	if(flightnumLength > 1)
	{
		var count=0;
		for(var i=0,maxLen=flightnumLength; i <maxLen ; i++)
		{
			if(count < flightnumLength && count !=0)
			{
				flightNumStr +="/";

			}
			flightNumStr += flightNum[i];


			if(count !=0 && flightNum[count].substr(0,2) != flightNum[count-1].substr(0,2))
			{
				airlineStr+= "/"+getAirlineName(flightNum[i].substr(0,2));
			}else if(count==0){
				airlineStr += getAirlineName(flightNum[i].substr(0,2));
			}
			count++;
		}

		return flightNumStr+" - "+airlineStr;
	}
	else
	{
		return  flightNum[0]+" - "+getAirlineName(flightNum[0].substr(0,2))
	}

}
function getAirlineNameForFlightNumber_notairline(value){
	var flightNum=[];
	flightNum=value.split("/");
	var flightnumLength = flightNum.length;
	var flightNumStr= "";
	var airlineStr="";
	if(flightnumLength > 1)
	{
		var count=0;
		for(var i=0,maxLen=flightnumLength; i <maxLen ; i++)
		{
			if(count < flightnumLength && count !=0)
			{
				flightNumStr +="/";

			}
			flightNumStr += flightNum[i];


			if(count !=0 && flightNum[count].substr(0,2) != flightNum[count-1].substr(0,2))
			{
				airlineStr+= "/"+getAirlineName(flightNum[i].substr(0,2));
			}else if(count==0){
				airlineStr += getAirlineName(flightNum[i].substr(0,2));
			}
			count++;
		}

		return flightNumStr;
	}
	else
	{
		return  flightNum[0];
	}

}
/* get flight running days in locale format*/
function getRunOnDaysInLocale(days){

    var localeDays=[];
    if(!_.isArray(days)){//if days is in object then converting to array
       days = $.makeArray(days);
    }
   for(var i=0,total=days.length ;i<total;i++){
   localeDays.push($.i18n.map["sky_"+days[i]]);
   }
       return localeDays;
}

/* function for validating & getting the airport code from suggestion text */
function validateAirportTextField ( suggestionText ) {

    //ios 9 has issues with Taffy
    //var db = TAFFY( localStorage.cityList );
    
    var db = JSON.parse(localStorage.cityList);
    
    
    var filterIndex = 0;
    var response = { isValid: false, Code: '', Name: '' };
    
    do {
        filterIndex = suggestionText.indexOf( "(", filterIndex );
        
        
        if(filterIndex !== -1){
            filterIndex = filterIndex + 1;
            
            var code = suggestionText.substr( filterIndex, 3 );
            
            //var result = db(). filter({airportCode:code}).get();
            
            var result = db.filter(function (a) {return a.airportCode === code });

            for(i=0;i<result.length;i++){
           // if(  suggestionText.replace(/ /g,'') === (result[i].airportName+" ("+code+"), "+result[i].countryName).replace(/ /g,'')){
              if(code === result[i].airportCode){
                response.isValid = true;
                response.Code = code;
                response.Name = result[i].airportName;
                break;
            }
            }
            
        }
        
    } while(filterIndex > 0);
    
    return response;
}



/* function for validating & getting the airport code from suggestion text */
function validateAirportWithLocType ( suggestionText,locType ) {

    //ios 9 has issues with Taffy
    //var db = TAFFY( localStorage.cityList );
    
    var db = JSON.parse(localStorage.cityList);
    
    
    var filterIndex = 0;
    var response = { isValid: false, Code: '', Name: '',locationType: '' };
    
    do {
        filterIndex = suggestionText.indexOf( "(", filterIndex );
        
        
        if(filterIndex !== -1){
            filterIndex = filterIndex + 1;
            
            var code = suggestionText.substr( filterIndex, 3 );
            
            //var result = db(). filter({airportCode:code}).get();
            
            var result = db.filter(function (a) {return a.airportCode === code && a.locationType === locType });
            
            if(result.length === 1 && (  suggestionText === (result[0].airportName+" ("+code+"), "+result[0].countryName)   )){
                response.isValid = true;
                response.Code = code;
                response.Name = result[0].airportName;
                response.locationType = result[0].locationType
                break;
                
            }
            
            
        }
        
    } while(filterIndex > 0);
    
    return response;
}

function addSegmentIdentifier(flightResult){

/******* interline */
interlineDepCount=0;
interlineRetCount=0;
var remDupl=[];
var remduplchk="";
if(sessionStorage.isinterline == "yes"){
if(typeof flightResult.schedules.onwardSchedules != 'undefined'){
        if(typeof flightResult.schedules.onwardSchedules.trips  != 'undefined'){
            for (i=0;i<flightResult.schedules.onwardSchedules.trips.length;i++){
remduplchk="";
                remduplchk=
                flightResult.schedules.onwardSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledDate+flightResult.schedules.onwardSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledTime;

            if($.inArray(remduplchk, remDupl) == -1 && flightResult.schedules.onwardSchedules.trips[i].isInterline !== "undefined" && flightResult.schedules.onwardSchedules.trips[i].isInterline=="N"){
                           remDupl.push(remduplchk);
                           }
}}}}

/******* interline */

    if(typeof flightResult.schedules.onwardSchedules != 'undefined'){
        if(typeof flightResult.schedules.onwardSchedules.trips  != 'undefined'){
            for (i=0;i<flightResult.schedules.onwardSchedules.trips.length;i++){
                flightResult.schedules.onwardSchedules.trips[i].segmentIdentifier = 'onwardSegment_'+i;

           /******* interline */
 if(sessionStorage.isinterline == "yes"){


               remduplchk="";
                remduplchk=
                flightResult.schedules.onwardSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledDate+flightResult.schedules.onwardSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledTime;
            if($.inArray(remduplchk, remDupl) == -1 && flightResult.schedules.onwardSchedules.trips[i].isInterline !== "undefined" && flightResult.schedules.onwardSchedules.trips[i].isInterline=="Y"){
               flightResult.schedules.onwardSchedules.trips[i].interDuplicate = 'false';
               interlineDepCount=interlineDepCount+1;
            }
            else if(flightResult.schedules.onwardSchedules.trips[i].isInterline !== "undefined" && flightResult.schedules.onwardSchedules.trips[i].isInterline=="N"){
                           flightResult.schedules.onwardSchedules.trips[i].interDuplicate = 'false';
                           interlineDepCount=interlineDepCount+1;
                        }
            else{
            flightResult.schedules.onwardSchedules.trips[i].interDuplicate = 'true';
            }
 }

              /******* interline */
        }
    }
    }


/******* interline */

remDupl=[];
remduplchk="";
if(sessionStorage.isinterline == "yes"){

        if(typeof flightResult.schedules.returnSchedules != 'undefined'){
        if(typeof flightResult.schedules.returnSchedules.trips != 'undefined'){
            for (i=0;i<flightResult.schedules.returnSchedules.trips.length;i++){
               remduplchk="";
               remduplchk=flightResult.schedules.returnSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledDate+flightResult.schedules.returnSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledTime;
               if($.inArray(remduplchk, remDupl) == -1 && flightResult.schedules.returnSchedules.trips[i].isInterline !== "undefined" && flightResult.schedules.returnSchedules.trips[i].isInterline=="N"){
              remDupl.push(remduplchk);
              }

            }}}}

/******* interline */

    if(typeof flightResult.schedules.returnSchedules != 'undefined'){
        if(typeof flightResult.schedules.returnSchedules.trips != 'undefined'){
            for (i=0;i<flightResult.schedules.returnSchedules.trips.length;i++){
                flightResult.schedules.returnSchedules.trips[i].segmentIdentifier = 'returnSegment_'+i;


                /******* interline */
 if(sessionStorage.isinterline == "yes"){
                             remduplchk="";
                             remduplchk=
                             flightResult.schedules.returnSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledDate+flightResult.schedules.returnSchedules.trips[i].flights[0].flightLegs[0].departureDetails.scheduledTime;
                            if($.inArray(remduplchk, remDupl) == -1 && flightResult.schedules.returnSchedules.trips[i].isInterline !== "undefined" && flightResult.schedules.returnSchedules.trips[i].isInterline=="Y"){
flightResult.schedules.returnSchedules.trips[i].interDuplicate = 'false';
interlineRetCount=interlineRetCount+1;
}
else if(flightResult.schedules.returnSchedules.trips[i].isInterline !== "undefined" && flightResult.schedules.returnSchedules.trips[i].isInterline=="N"){
flightResult.schedules.returnSchedules.trips[i].interDuplicate = 'false';
interlineRetCount=interlineRetCount+1;
}
else{
flightResult.schedules.returnSchedules.trips[i].interDuplicate = 'true';

}
            }
            /******* interline */
        }
    }
    }
    return flightResult;
}
/******* interline */
var interlineDepCount=0;
var interlineRetCount=0;
/******* interline */