define(['jquery','ji18n','underscore', 'backbone', 'utilities', 'constants', 'view/homePage/home', 'view/menu/menuPage', 'view/flights/savedFlightView',
        'view/flights/searchFlightView', 'view/flights/searchFlightsViewResultsMVVM', 'view/flights/SavedFlights_AirportsView',
        'view/flights/flightDetailsView', 'view/flights/airportDetailsViewMVVM','view/flights/airportDetailsViewViaMap', 'models/flights/searchPage', 'view/settings/settingsView',
        'view/skyTips/skyTipsView', 'view/aboutSkyTeam/aboutSkyTeamView', 'view/flights/searchAirportView', 'view/aboutSkyTeam/skyTeamMemberViewMVVM',
        'view/airports/weatherDetailsView', 'view/flights/flightStatusView', 'view/airports/loungeFinderResultViewMVVM',
        'view/airports/loungeFinderSearchView', 'view/settings/disclaimerView', 'view/settings/privacyPolicyView',
        'view/flights/flightStatusResultsView', 'view/flights/flightStatusDetails', 'view/airports/loungesAccessPolicy','view/skyTips/skyTipsResultsView','view/skyPriority/skyPriorityFinderView','view/skyPriority/skyPriorityDetailsView',
        'view/skyPriority/skyPriorityAdvancedDetailsView','view/skyTips/skyTipsAddNewView','view/skyTips/skyTipsTermsAndConditions','view/transportation/transportationView','view/skyTips/skyTipsResultMVVM','view/skyTips/skyTipsViewMVVM','view/skyTips/skyTipsAddNewViewMVVM','view/skyPriority/skyPriorityFinderViewMVVM','view/skyPriority/skyPriorityDetailsViewMVVM',
        'view/skyPriority/skyPriorityAdvancedDetailsViewMVVM','view/airports/loungeFinderSearchViewMVVM','view/airports/loungeFinderResultViewMVVM','view/flights/flightDetailsViewMVVM','appView','view/menu/newmenuPage'],
        function($,i18n, _, Backbone, utilities, constants, homePage, menuPage, savedFlights, searchFlights, searchResults,
        		savedFlightsOrAirports, FlightDetailsPage, airportDetails,airportDetailsViewViaMap, searchPage, settings, skyTipsFinder, aboutSkyteam, airportSearchPage,
        		skyTeamMemberMVVM, weatherDetails, flightstatus, loungeFinderResult, loungesSearch, disclaimer, privacyPolicy,
        		flightstatusresults, flightStatusDetails, accessPolicy, skyTipsResults, skyPriorityFinder,skyPriorityDetails,skyPriorityAdvancedDetails, addSkyTipsPage, skytipsTerms,transportationPage,skyTipsResultMVVM,skyTipsViewMVVM,skyTipsAddNewViewMVVM, skyPriorityFinderViewMVVM, skyPriorityDetailsViewMVVM,skyPriorityAdvancedDetailsViewMVVM,loungeFinderSearchViewMVVM,loungeFinderResultViewMVVM,flightDetailsViewMVVM,appView,newmenuPage) {

	var myviews = [];
	    //var isSkytipsResults;
    var controllerView = appView.extend({
                                                  
              el: '#skyteam',
              direction:'Right',
              goto: function (view) {
              
              var previous = this.currentPage || null;
              var next = view;
              //removing the aiorline popups

              this.$el.find('.proferedline-popup').remove();
              this.undelegateEvents();
              if (previous) {
              previous.transitionOut(function () {

                                  previous.undelegateEvents();
                                  $(previous.el).removeData().unbind();
                                  $(previous.el).empty();
                                  previous.remove();
                                     //this.direction = "Left";
                                     });
              }

              next.render({ page: true });
                this.$el.append( next.$el );
              next.transitionIn();
              this.currentPage = next;
//
//               //Flight Suggestion 23-08-2017
//               if($('div.searchresultslist p').hasClass('class_service_failed')){
//                $('.searchresultstabs').css('visibility', 'hidden');
//                 $('#oneWayFilter').css('visibility', 'hidden');
//
//                                                                                             }else{
//                                                                                                  $('.searchresultstabs').css('visibility', 'visible');
//                                                                                                  $('#oneWayFilter').css('visibility', 'visible');
//
//                                                                                             }

              
              },
                moveTo:function(view){
                var next = view;
                //rendering the next view
                next.render({ page: true });
                if(this.currentPage){
                    var previous = this.currentPage || null;
                    previous.undelegateEvents();
                    $(previous.el).removeData().unbind();
                    //remove the previous el
                    previous.remove();
                    }
                    //adding the new el to the container
                    this.$el.append( next.$el );
                    this.currentPage = next;
                    sessionStorage.navDirection = "Right";
                }
              
      });
       window.viewNavigator = new controllerView();
       
	var mainRouter = Backbone.Router.extend({
		routes : {

			"menu" : "menuPage",
			"homePage" : "homePage",
			"searchFlights" : "searchFlights",
			"searchResults" : "searchResults",
			"savedFlightsOrAirports" : "savedFlightsOrAirports",
			"FlightDetailsPage" : "FlightDetailsPage",
			"airportDetails" : "airportDetails",
			"airportDetailsViewViaMap" : "airportDetailsViewViaMap",
			"settings" : "settings",
			"skyTipsFinder" : "skyTipsFinder",
			"airportSearchPage" : "airportSearchPage",
			"aboutSkyTeam" : "aboutSkyteam",
			"memberAirlines" : "aboutSkyteam",
			"weatherDetails" : "weatherDetails",
			"skyTeamMember" : "skyTeamMember",
			"flightstatus" : "flightstatus",
			"loungeFinderResult" : "loungeFinderResult",
			"loungesSearch" : "loungesSearch",
			"disclaimer" : "disclaimer",
			"privacyPolicy" : "privacyPolicy",
			"flightstatusresults" : "flightstatusresults",
			"flightStatusDetails" : "flightStatusDetails",
			"accessPolicy" : "accessPolicy",
			"skyTipsResults" : "skyTipsResults",
			"addSkyTipsPage" : "addSkyTipsPage",
			"skyPriorityFinder":"skyPriorityFinder",
			"skyPriorityDetails":"skyPriorityDetails",
			"skyPriorityAdvancedDetails":"skyPriorityAdvancedDetails",
			"skytipsTerms":"skytipsTerms",
			"transportationPage" : "transportationPage",
			"yogaVideoPage" : "yogaVideoPage",
			"*path" : "defaultRoute"


		},



	});

	var initialize = function() {
		var mySwiper;

		var routrObj = new mainRouter;


		/* Function to navigate My Skyteam Page */
		$('#brief').click(function() {

			if ("none" !== $('#brief img').css('display')) {
				if (sessionStorage.lastPage == "airportSearchPage") {
					sessionStorage.currentPage = "airportDetails";
				}
				sessionStorage.history = sessionStorage.currentPage;
				var obj = {};
				obj.previousPage = sessionStorage.currentPage;
				if ("FlightDetailsPage" === sessionStorage.currentPage) {
					obj.pageIsFrom = sessionStorage.previousPage;
					obj.flightData = sessionStorage.flightDetailsObject;
					if("homePage" === sessionStorage.previousPage)
						obj.data = sessionStorage.savedFlightDetails;
				} else if ("skyTipsResults" === sessionStorage.currentPage) {
					obj.pageIsFrom = sessionStorage.skyTipsFrom;
					obj.theme = sessionStorage.themeSelected;
					obj.data = localStorage.skyTipsDetails;
					obj.themeSelect = sessionStorage.theme;
					obj.airportName = sessionStorage.skyTipsAirportName;
				} else if ("loungeFinderResult" === sessionStorage.currentPage) {
					obj.pageIsFrom = sessionStorage.lounge;
					obj.airportName = sessionStorage.airportName;
					obj.Loungesdetails = localStorage.loungeFinderMenuDisplay;
					obj.airportCode=sessionStorage.airport_code;
					obj.data = sessionStorage.airportDetailsObj;


				}
				else if("loungesSearch" === sessionStorage.currentPage){
					obj.pageIsFrom = sessionStorage.lounge;
					obj.airportName = sessionStorage.airportName;
					/*var check = validateSkyTipsAirportTextField(viewNavigator.currentPage.$el.find('#airport_text').val());
                      if(check.isValid){
                      sessionStorage.airport_code = check.Code;
                      }
                      else{
                      sessionStorage.airport_code ='';
                      }*/
				}else if ("airportDetails" === sessionStorage.currentPage) {
					obj.data = sessionStorage.airportDetailsObj;
				}
				else if ("airportSearchPage" === sessionStorage.currentPage) {
					obj.airportName = sessionStorage.searchAirport;

				}
				else if("weatherDetails" === sessionStorage.currentPage){
					obj.weatherAirportDetails = sessionStorage.airportDetailsObj;
				}
				else if("skyPriorityFinder" == sessionStorage.currentPage){
                  /* remove Sky priority banner image from DOM*/
                  removeBackgroundImage($('div.sp_banner-img'));
                }

				pushPageOnToBackStack(obj);

				window.open(iSC.savedAirports, "_self");
			}

		});
		/* End of function */
		 $('.main_header ul li:eq(1) img').on("mousedown",function() {
		               sessionStorage.navDirection = "Left";
         });

		$(window).bind('hashchange', function(){
		/* home page to myskyteam to saving form data for back logic*/
			if(sessionStorage.currentPage === "searchFlights" && window.location.hash === "#savedFlightsOrAirports"){
              var obj = {}, isFrom = "";
              if($.i18n.map.sky_round_trip === viewNavigator.currentPage.$el.find('.onHover a').html().trim()){
              isFrom = "roundtrip";
              }
              else{
              isFrom = "oneway";
              }

              obj.Src = viewNavigator.currentPage.$el.find('#source').val();
              obj.Dest = viewNavigator.currentPage.$el.find('#destination').val();
              obj.isFrom = isFrom != "" ? isFrom : "";
              obj.TwoWayDep = sessionStorage.depDate;
              obj.TwoWayArrival = sessionStorage.returndate;
              sessionStorage.searchData = JSON.stringify(obj);
           }
			/*
			 * Removing Large images from DOM ,before changing the DOM  
			 * */
			if(sessionStorage.currentPage === "skyPriorityFinder"){
				removeBackgroundImage($(".sp_banner-img"));//div containing skypriority banner image
			}
			else if(sessionStorage.currentPage === "loungesSearch"){
				removeBackgroundImage($(".lounge_banner-img"));//div containing lounge banner image
			}
			else if(sessionStorage.currentPage === "airportDetails" || sessionStorage.currentPage === "airportDetailsViewViaMap" ){
				removeBackgroundImage($(".airports_image"));//div containing airport details image
			}
			
			if(localStorage.cityListUpdateAvailable != undefined && localStorage.cityListUpdateAvailable == "YES")
			{
				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if(spath == "searchFlights" || spath == "flightstatus" || spath =="loungesSearch" || spath == "skyPriorityFinder") {
					showActivityIndicator($.i18n.map.sky_please);
					var tempArr=[];
					tempArr=JSON.parse(localStorage.cityListUpdatedData);
					tempArr.sort(function(a,b){
						return (a.airportName > b.airportName) ? 1 : -1;
					});
					localStorage.cityList=JSON.stringify(tempArr);
					localStorage.cityListUpdateAvailable = "NO";
					hideActivityIndicator();
				}
			}
			this.checkUrl;
		});
		/**** weather***/
		routrObj.on('route:weatherDetails', function(actions) {
			var weatherDetailsMainView = new weatherDetails();
                                viewNavigator.goto(weatherDetailsMainView);
            			window.open("analytics://_trackPageview#weatherDetails", "_self");
		});
		/**** weather end***/
		
		/*** searchResults ***/
		routrObj.on('route:searchResults', function(actions) {

			 var filterRes;
            if(sessionStorage.Res != undefined){
                filterRes = JSON.parse(sessionStorage.Res);
            }
            var searchflightsresultsView = new searchResults();
                    searchflightsresultsView.arguments = filterRes;

            viewNavigator.goto(searchflightsresultsView);
            window.open("analytics://_trackPageview#searchResults", "_self");

		});
		/** end of searchResults ***/
		
		/*** savedFlightsOrAirports ***/
		routrObj.on('route:savedFlightsOrAirports', function(actions) {

			
            var prevPage = sessionStorage.currentPage;

            if(prevPage === "scanPage" || prevPage === "airportSearchPage" || prevPage === "yogaVideoPage")	{
               sessionStorage.isFromScan = "no";
            	viewNavigator.moveTo(new savedFlightsOrAirports());
			}
			else{
			viewNavigator.goto(new savedFlightsOrAirports());
			}
			window.open("analytics://_trackPageview#savedFlightsOrAirports", "_self");


		});
		/*** End of savedFlightsOrAirports ***/
		
		/*** FlightDetailsPage ***/
		routrObj.on('route:FlightDetailsPage', function(actions) {
			var flightsMainView = new flightDetailsViewMVVM();
             viewNavigator.goto(flightsMainView);
                        window.open("analytics://_trackPageview#FlightDetailsPage", "_self");
		});
		/*** End of FlightDetailsPage ***/
		
		/*** airportDetails ***/
		routrObj.on('route:airportDetails', function(actions) {
			 var airportsMainView = new airportDetails();
            viewNavigator.goto(airportsMainView);

            window.open("analytics://_trackPageview#airportDetails", "_self");
		});
		/*** End of airportDetails ***/

		/*** airportDetails ***/
		routrObj.on('route:airportDetailsViewViaMap', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
        			sessionStorage.movingToMap = "No";
        	}

			var airportsMainView = new airportDetailsViewViaMap();
			if(sessionStorage.navDirection === "Left"){
                               viewNavigator.goto(airportsMainView);
                        }
                        else{
                       viewNavigator.moveTo(airportsMainView);
                        }

			window.open("analytics://_trackPageview#airportDetails", "_self");


		});
		/*** End of airportDetails ***/

		/*** searchFlights ***/
		routrObj.on('route:searchFlights', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                            			sessionStorage.transRequired = "No";
                            			sessionStorage.movingToMap = "No";
                            	}

			var searchflightsMainView = new searchFlights();
			searchflightsMainView.arguments = new Date();
                        viewNavigator.goto(searchflightsMainView);

			window.open("analytics://_trackPageview#searchFlights", "_self");


		});
		/*** End of searchFlights ***/
		
		/*** settings ***/
		routrObj.on('route:settings', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                            			sessionStorage.movingToMap = "No";
                            	}

			
			var settingsPageView = new settings();
			viewNavigator.goto(settingsPageView);
			window.open("analytics://_trackPageview#settings", "_self");


		});
		/*** End of settings ***/
		
		/*** skytips ***/
		routrObj.on('route:skyTipsFinder', function(actions) {
		 if(sessionStorage.movingToMap === "Yes"){
                    sessionStorage.movingToMap = "No";

                    }

                    var skyTipsFinderView = new skyTipsViewMVVM();

                            if("addSkyTipsPage" !== sessionStorage.currentPage){
                            viewNavigator.goto(skyTipsFinderView);
                            }
                            else{/*From addSkyTips view no transition*/
                            viewNavigator.moveTo(skyTipsFinderView);
                            }
                    window.open("analytics://_trackPageview#skyTipsFinder", "_self");


		});
		/*** End of skytips ***/
		
		/*** skytipsResults ***/
		routrObj.on('route:skyTipsResults', function(actions) {
			 var skytipsResultsPageView = new skyTipsResultMVVM();
                         //isSkytipsResults = "Yes";
                         viewNavigator.goto(skytipsResultsPageView);

             			window.open("analytics://_trackPageview#skyTipsResults", "_self");

		});
		/*** End of skytipsResults ***/
		
		/*** AddSkyTipsPage ***/
		routrObj.on('route:addSkyTipsPage', function(actions) {

			var addSkyTipsMainView = new skyTipsAddNewViewMVVM();
                            if("skyTipsFinder" !== sessionStorage.currentPage){
                                viewNavigator.goto(addSkyTipsMainView);
                            }
                            else{
                                viewNavigator.moveTo(addSkyTipsMainView);
                            }

            			window.open("analytics://_trackPageview#addSkyTipsPage", "_self");

		});
		/*** End of AddSkyTipsPage ***/
		
		/*** transportation page ***/
		routrObj.on('route:transportationPage', function(actions) {
			var transportationView = new transportationPage();
                                viewNavigator.goto(transportationView);
                        window.open("analytics://_trackPageview#transportationPage", "_self");

		});
		/*** End of transportation page ***/
		
		/*** aboutSkyteam ***/
		routrObj.on('route:aboutSkyteam', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                            			sessionStorage.movingToMap = "No";
                            	}
			var aboutSkyteamPageView = new aboutSkyteam();
			viewNavigator.goto(aboutSkyteamPageView);
			window.open("analytics://_trackPageview#aboutSkyteam", "_self");
			

		});
		/*** End of aboutSkyteam ***/
		
		/*** skyTeamMember ***/
		routrObj.on('route:skyTeamMember', function(actions) {
			var skyTeamMembersMVVM = new skyTeamMemberMVVM();
			 viewNavigator.goto(skyTeamMembersMVVM);

			window.open("analytics://_trackPageview#skyTeamMember", "_self");
			

		});
		/*** End of skyTeamMember ***/
		
		/*** airportSearchPage ***/
		routrObj.on('route:airportSearchPage', function(actions) {

             sessionStorage.navDirection = "Right";
            sessionStorage.movingToMap = "Yes";
			if('YES' === sessionStorage.goNativeMapFromDetails){
				sessionStorage.goNativeMapFromDetails='NO';	
				window.open("app:nativeMapOld","_self");	
			}else{
				if(undefined != sessionStorage.fromHomeScreen){
					window.open("app:nativeMap#"+sessionStorage.fromHomeScreen,"_self");
				}else{
					window.open("app:nativeMap","_self");
				}
			}

			setTimeout(function() {
				window.open("analytics://_trackPageview#airportSearchPage", "_self");
			}, 3000);

		});
		/*** End of airportSearchPage ***/

		/*** loungesFinderMenuView ***/
		routrObj.on('route:loungeFinderResult', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                                    			sessionStorage.movingToMap = "No";
                                    	}
			
			var loungeFinderResultsView = new loungeFinderResultViewMVVM();
			viewNavigator.goto(loungeFinderResultsView);
			
			window.open("analytics://_trackPageview#loungeFinderResult", "_self");
			

		});
		/*** End of loungesFinderMenuView ***/
		
		/*** loungesSearch result ***/
		routrObj.on('route:loungesSearch', function(actions) {
			var loungeFinderSearchResultView = new loungeFinderSearchViewMVVM();
            viewNavigator.goto(loungeFinderSearchResultView);
			window.open("analytics://_trackPageview#loungesSearch", "_self");
			
		});
		/*** End of loungesSearch result ***/
		
		/*** flightstatus ***/
		routrObj.on('route:flightstatus', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                            			sessionStorage.movingToMap = "No";
                            	}
			var flightstatusView = new flightstatus();

                                viewNavigator.goto(flightstatusView);
			window.open("analytics://_trackPageview#flightstatus", "_self");


		});
		/*** End of flightstatus ***/
		
		/*** flightstatusresults ***/
		routrObj.on('route:flightstatusresults', function(actions) {

			var flightstatusresultsView = new flightstatusresults();
            viewNavigator.goto(flightstatusresultsView);
			window.open("analytics://_trackPageview#flightstatusresults", "_self");


		});
		/*** End of flightstatusresults ***/
		
		/*** flightStatusDetails ***/
		routrObj.on('route:flightStatusDetails', function(actions) {
            var prevPage = sessionStorage.currentPage;
			var flightStatusDetailsView = new flightStatusDetails();
			if( prevPage === "scanPage"){
            viewNavigator.moveTo(flightStatusDetailsView);
            }
            else{
            viewNavigator.goto(flightStatusDetailsView);
            }
			window.open("analytics://_trackPageview#flightStatusDetails", "_self");

		});
		/*** End of flightStatusDetails ***/
		
		/*** disclaimer ***/
		routrObj.on('route:disclaimer', function(actions) {

			var settingsdisclaimerView = new disclaimer();
			viewNavigator.goto(settingsdisclaimerView);

			window.open("analytics://_trackPageview#disclaimer", "_self");


		});
		/*** End of disclaimer ***/
		
		/*** privacyPolicy ***/
		routrObj.on('route:privacyPolicy', function(actions) {
			var settingsPrivacyPolicyView = new privacyPolicy();
			viewNavigator.goto(settingsPrivacyPolicyView);

			window.open("analytics://_trackPageview#privacyPolicy", "_self");


		});
		/*** End of privacyPolicy ***/
		
		/*** accessPolicy ***/
		routrObj.on('route:accessPolicy', function(actions) {
			
			var accessPolicyView = new accessPolicy();
            viewNavigator.goto(accessPolicyView);
			window.open("analytics://_trackPageview#accessPolicy", "_self");
			
		});
		/*** End of accessPolicy ***/
		
		/*** skytips terms & conditions ***/
		routrObj.on('route:skytipsTerms', function(actions) {

			var skyTipsTermsView = new skytipsTerms();
			viewNavigator.goto(skyTipsTermsView);
			window.open("analytics://_trackPageview#skytipsTerms", "_self");


		});
		/*** End of skytips terms & conditions ***/
		
		/*** skyPriority Finder ***/
		routrObj.on('route:skyPriorityFinder', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                            			sessionStorage.movingToMap = "No";
                            	}

			var skyPriorityFinderView = new skyPriorityFinderViewMVVM();
			viewNavigator.goto(skyPriorityFinderView);
			window.open("analytics://_trackPageview#skyPriorityFinder", "_self");


		});
		/*** End of skyPriority Finder ***/
		
		/*** skyPriority Details ***/
		routrObj.on('route:skyPriorityDetails', function(actions) {

			var skyPriorityDetailsView = new skyPriorityDetailsViewMVVM();
			viewNavigator.goto(skyPriorityDetailsView);

			window.open("analytics://_trackPageview#skyPriorityDetails", "_self");


		});
		/*** End of skyPriority Details ***/
		
		/*** skyPriority Advanced Details ***/
		routrObj.on('route:skyPriorityAdvancedDetails', function(actions) {

			var skyPriorityDetailsView = new skyPriorityAdvancedDetailsViewMVVM();
			viewNavigator.goto(skyPriorityDetailsView);
			window.open("analytics://_trackPageview#skyPriorityAdvancedDetails", "_self");

		});
		/*** End of skyPriority Advanced Details ***/

        /*** YogaVideoPage ***/

               routrObj.on('route:yogaVideoPage', function(actions) {

                             sessionStorage.navDirection = "Right";
                           sessionStorage.movingToMap = "Yes";
                            sessionStorage.currentPage='yogaVideoPage';
                            if(  sessionStorage.isFromSavedAirportsFlights != undefined && sessionStorage.isFromSavedAirportsFlights === "YES")
                            {
                                 window.open("app:nativeYogaVideo#YES","_self");
                            }
                           else
                           {
                                window.open("app:nativeYogaVideo","_self");
                           }
                        sessionStorage.isFromSavedAirportsFlights = "NO";

                           setTimeout(function() {
                                      window.open("analytics://_trackPageview#yogaVideoPage", "_self");
                                      }, 1000);
                           });
               /*** End of YogaVideoPage ***/

		/*** homePage ***/

		routrObj.on('route:homePage', function(actions) {
		if(sessionStorage.movingToMap === "Yes"){
                            			sessionStorage.movingToMap = "No";
                            	}
                            	 var prevPage = sessionStorage.currentPage;
			var homePageMainView = new homePage();
			if(prevPage === "airportSearchPage"){
                                    viewNavigator.moveTo(homePageMainView);
                                }
                                else{
			viewNavigator.goto(homePageMainView)
}
			$('.main_buttons').css('height', '28%');
			$('.savedFlights-container').show();
			window.savedFlightsCallBack = function() {

				if ($('#savedFlightsLoader').length > 0) {
					$('#savedFlightsLoader').remove();
					if ('undefined' != localStorage.savedFlightDetails && null != localStorage.savedFlightDetails && ("ERROR" != JSON.parse(localStorage.savedFlightDetails).SavedFlights_test)) {

						homePageMainView.renderSavedFlights(localStorage.savedFlightDetails);
						//gif loader

                                           $("#statusSection").append($('<img id="ajaxLoad"></img>').attr('src', './images/AjaxLoader.gif'));
                                           //Getting Status From API
                                           renderStatusfromAPI(localStorage.savedFlightDetails);
						var mySwiper6 = new Swiper('.savedFlights-container', {
							loop : false,
							grabCursor : true,
							autoplay:3000,
							speed:2000

						});

					} else {
						$('.main_buttons').css('height', '50%');
						$('.savedFlights-container').hide();
					}
				}
				window.savedFlightsCallBack = function() {
				};
			};
            setTimeout(function(){
			window.open(iSC.savedFlights, "_self");
            },1000);

			sessionStorage.home = "no";
			mySwiper = new Swiper('.swiper-container', {
				pagination : '.pagination',
				loop : true,
				grabCursor : true,
				paginationClickable : true,
				autoplay:3000,
				speed:2000

			});

			setTimeout(function() {

				window.open("analytics://_trackPageview#homePage", "_self");
			}, 1500);


		});
		/*** End of homePage ***/

		/*** defaultRoute ***/

		routrObj.on('route:defaultRoute', function(actions) {


			if(undefined != sessionStorage.previousPages)
			{
				sessionStorage.removeItem('previousPages');
			}


			/*	Checking the network connectivity	*/
			setTimeout(function(){
			    window.networkStatCallBack = function(){};
				window.open(iSC.Network, "_self");
			},500);
			/*	Checking the network connectivity	*/
            var strLang = localStorage.language == "zh-Hant" ? "zh_TW" : localStorage.language;

			$.i18n.properties({
				name:'Messages', 
				path:'bundle/', 
				mode:'map',
				language:strLang,
				callback: function(e) { 
				}
			});


			/*Code to load Language ends */            
			var homePageMainView = new homePage();
			viewNavigator.moveTo(homePageMainView);
			$('.main_buttons').css('height', '28%');
			$('.savedFlights-container').show();
			window.savedFlightsCallBack = function() {
				if ($('#savedFlightsLoader').length > 0) {
					$('#savedFlightsLoader').remove();
					if ('undefined' != localStorage.savedFlightDetails && null != localStorage.savedFlightDetails && "ERROR" != (JSON.parse(localStorage.savedFlightDetails).SavedFlights_test)) {

						homePageMainView.renderSavedFlights(localStorage.savedFlightDetails);
						//gif loader

                                           $("#statusSection").append($('<img id="ajaxLoad"></img>').attr('src', './images/AjaxLoader.gif'));
                                           //Getting Status From API
                                           renderStatusfromAPI(localStorage.savedFlightDetails);
						var mySwiper6 = new Swiper('.savedFlights-container', {
							loop : false,
							grabCursor : true,
							autoplay:3000,
							speed:2000

						});

					} else {
						$('.main_buttons').css('height', '50%');
						$('.savedFlights-container').hide();
					}
				}
				window.savedFlightsCallBack = function() {
				};
			};


			window.open(iSC.savedFlights, "_self");

			sessionStorage.home = "no";

			mySwiper = new Swiper('.swiper-container', {
				pagination : '.pagination',
				loop : true,
				grabCursor : true,
				paginationClickable : true,
				autoplay:3000,
				speed:2000
			});

			setTimeout(function() {

				window.open("analytics://_trackPageview#homePage", "_self");
			}, 1500);
			setTimeout(function() {

				if ('undefined' == localStorage.cityList || null == localStorage.cityList) {

					window.getAirportsCallback = function() {

					}


					window.open(iSC.getAirports, "_self");

				}
			}, 500);

			setTimeout(function() {
				hideActivityIndicator(); },1000);
		});
		/** End of default route ***/
		Backbone.history.start();
	};

	return {
		initialize : initialize,
		//Added  to implement separate Airport Details page when calling from Native Map Location
		airportDetailsViewViaMap: function(actions) { 
			if(sessionStorage.movingToMap === "Yes"){
                    			sessionStorage.transRequired = "No";
                    			sessionStorage.movingToMap = "No";
                                             	}
                    	else {
                    	sessionStorage.transRequired = "Yes";
            			}
			var airportsMainView = new airportDetailsViewViaMap();
			sessionStorage.navDirection = "Right";
			viewNavigator.moveTo(airportsMainView);


			window.open("analytics://_trackPageview#airportDetails", "_self");

		},
		getSettings:function(actions) {

           //Binding new language text  for menu
           new newmenuPage();
           sessionStorage.isMenuAdded = "YES";
           //setting header change
           $('#headingText h2').html($.i18n.map.sky_settings);
           //refreshing the settings page
           viewNavigator.currentPage.render();
           window.open("analytics://_trackPageview#settings", "_self");


		}
	}
});