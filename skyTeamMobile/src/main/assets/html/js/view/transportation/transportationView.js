define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html', 'text!template/transportationTemplate.html','appView'], function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate, transportationTemplate,appView) {


       var self, transportationView = appView.extend({

                                                     id: 'transportation-page',

                                                     initialize: function() {

                                                     $('#backButtonList').show();
                                                     $('#headingText img').hide();
                                                     $('#headingText h2').show();
                                                     $('#headingText h2').text($.i18n.map.sky_transportation);
                                                     hideActivityIndicator();
                                                     if (sessionStorage.savedflights == "yes") {
                                                      $('#brief img').show();
                                                      $('#brief h2').hide();
                                                      }

                                                     //$(".menuItems").pageslide();
                                                     sessionStorage.currentPage = "transportationPage";
                                                     self = this;
                                                     },

                                                     render: function() {
                                                     /* Back button click event */
                                                     $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                                                                                                       if($('#pageslide').css('display') === "block"){
                                                                                                       e.preventDefault();
                                                                                                       e.stopImmediatePropagation();
                                                                                                       $.pageslide.close();
                                                                                                       }
                                                                                                       else
                                                                                                       {var currentobj = JSON.parse(sessionStorage.previousPages);
                                                                                                       showActivityIndicator($.i18n.map.sky_please);


                                                                                                       if (currentobj[0].previousPage == "airportDetails" || currentobj[0].previousPage == "airportDetailsViewViaMap" ) {

                                                                                                       if (currentobj[0].isFrom == "homePage") {
                                                                                                       sessionStorage.savedFlightDetails = currentobj[0].data;
                                                                                                       }
                                                                                                       sessionStorage.currentPage = currentobj[0].isFrom;

                                                                                                       }

                                                                                                       sessionStorage.removeItem('lastPage');
                                                                                                       e.preventDefault();
                                                                                                       window.location.href = "index.html#" + currentobj[0].previousPage;
                                                                                                       popPageOnToBackStack();
                                                                                                       $(self.el).undelegate();
                                                                                                       }
                                                                                                       });
                                                     /* End of back functionality */
                                                     var locationPath, spath;
                                                     locationPath = window.location.toString();
                                                     spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                     var compiledTemplate = _.template(transportationTemplate, $.i18n.map);
                                                     $(this.el).html(compiledTemplate);

                                                     var transportationVariables = sessionStorage.detailsTransportation;
                                                     var values = JSON.parse(transportationVariables);
                                                     if(values.TransferTerminals !=undefined){
                                                     self.$el.find('#transportationListTransferTerminal_high p').text(values.TransferTerminals);
                                                     }
                                                     else
                                                     {
                                                     self.$el.find(".transferTerminal").addClass('noclass');
                                                     }
                                                     if(values.TransferCity !=undefined){
                                                     self.$el.find('#transportationListTransferCity_high p').text(values.TransferCity);}
                                                     else
                                                     {
                                                     self.$el.find(".transferCity").addClass('noclass');
                                                     }
                                                     if(values.CarRental !=undefined){
                                                     self.$el.find('#transportationListCarRental_high p').text(values.CarRental);}
                                                     else{
                                                     self.$el.find(".carRental").addClass('noclass');
                                                     }
                                                     if(values.Taxi !=undefined){
                                                     self.$el.find('#transportationListTaxi_high p').text(values.Taxi);
                                                     }
                                                     else{
                                                     self.$el.find(".taxi").addClass('noclass');
                                                     }

                                                     if(values.CarParking !=undefined){
                                                     self.$el.find('#transportationListCarParking_high p').text(values.CarParking);}
                                                     else
                                                     {
                                                     self.$el.find(".carParking").addClass('noclass');
                                                     }
                                                     //self.$el.find('.transportationPage li:not(.noclass):first').trigger('click');
                                                     self.$el.find('.transportationPage li:not(.noclass):first div').toggleClass('noclass');
                                                     $.pageslide.close();

                                                     return appView.prototype.render.apply(this, arguments);

                                                     },
                                                     events: {

                                                   'click .transferTerminal' : 'transferDetail',
                                                     'click .transferCity' : 'transferCityDetail',
                                                      'click .carRental' : 'carRentalDetail',
                                                    'click .taxi' : 'taxiDetail',
                                                     'click .carParking' : 'carParkingDetail',
                                                     'touchstart *' : function() {
                                                     $.pageslide.close();
                                                     }

                                                     },
                                                   transferDetail : function(e){



                                                     if($('#transportationListTransferTerminal_small').hasClass('noclass') && e.target.parentElement.parentNode.className =="transferTerminal"){
                                                     $('#transportationListTransferTerminal_small').removeClass('noclass');
                                                     $('#transportationListTransferTerminal_high').addClass('noclass');
                                                     }

                                                     else{
                                                     $('#transportationListTransferTerminal_small').addClass('noclass');
                                                     $('#transportationListTransferTerminal_high').removeClass('noclass');

                                                     }
                                                     },
                                                     transferCityDetail : function(e){

                                                     if($('#transportationListTransferCity_small').hasClass('noclass') && e.target.parentElement.parentNode.className =="transferCity"){
                                                     $('#transportationListTransferCity_small').removeClass('noclass');
                                                     $('#transportationListTransferCity_high').addClass('noclass');
                                                     }
                                                     else{
                                                     $('#transportationListTransferCity_small').addClass('noclass');
                                                     $('#transportationListTransferCity_high').removeClass('noclass');

                                                     }

                                                     },
                                                       carRentalDetail:function(e){

                                                     if($('#transportationListCarRental_small').hasClass('noclass') &&  e.target.parentElement.parentNode.className =="carRental"){
                                                     $('#transportationListCarRental_small').removeClass('noclass');
                                                     $('#transportationListCarRental_high').addClass('noclass');
                                                     }
                                                     else{
                                                     $('#transportationListCarRental_small').addClass('noclass');
                                                     $('#transportationListCarRental_high').removeClass('noclass');

                                                     }
                                                     },
                                                     taxiDetail:function(e){

                                                     if($('#transportationListTaxi_small').hasClass('noclass') && e.target.parentElement.parentNode.className == "taxi"){
                                                     $('#transportationListTaxi_small').removeClass('noclass');
                                                     $('#transportationListTaxi_high').addClass('noclass');
                                                     }
                                                     else{
                                                     $('#transportationListTaxi_small').addClass('noclass');
                                                     $('#transportationListTaxi_high').removeClass('noclass');

                                                     }
                                                     },

                                                     carParkingDetail:function(e){

                                                     if($('#transportationListCarParking_small').hasClass('noclass') && e.target.parentElement.parentNode.className == "carParking"){
                                                     $('#transportationListCarParking_small').removeClass('noclass');
                                                     $('#transportationListCarParking_high').addClass('noclass');
                                                     }
                                                     else{
                                                     $('#transportationListCarParking_small').addClass('noclass');
                                                     $('#transportationListCarParking_high').removeClass('noclass');
                                                     }
                                                     }

                                                     });
       return transportationView;
       });
