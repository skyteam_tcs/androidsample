define(['jquery', 'backbone', 'underscore', 'text!template/menuTemplate.html'],
		function($, Backbone, _, menuPageTemplate) {

	var list = {};
	var menuPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'menu-page',

		initialize : function() {
			if("menuItems" === $('.main_header ul li:eq(0) a').attr('class')){

				$(".menuItems").pageslide();
				var template = _.template(menuPageTemplate,$.i18n.map);
				$('#pageslide').append(template);
			}
			$('#flightSearch').click(function() {
				var locationPath = window.location;

				locationPath = locationPath.toString();
				 sessionStorage.isFromHome="";
                 sessionStorage.navToAirportDetails="";
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if (spath == "searchflights") {
					$(".menuItems").trigger('click');
				} else {

					sessionStorage.navToSearch = "yes";
				}
			});

			$('#airportsSearch').click(function() {
				$.pageslide.close();
			});
			$('#home').click(function() {
				if(undefined != sessionStorage.previousPages)
				{
					sessionStorage.removeItem('previousPages');
				}
				window.location.href = 'index.html#homePage';

			});
		},

		render : function() {
		},
		events : {
			'touchstart *':function(){
				$.pageslide.close();
			}
		}
	});
	return menuPageView;

}); 