define(['jquery', 'backbone', 'underscore', 'text!template/menuTemplate.html'],
		function($, Backbone, _, menuPageTemplate) {

	var list = {};
	var menuPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'menu-page',

		initialize : function() {
			//if($('.main_header ul li:eq(0) a').attr('class') == "menuItems"){

				$(".menuItems").pageslide();
				var template = _.template(menuPageTemplate,$.i18n.map);
				$('#pageslide').html(template);
			//}

			$('#home').click(function() {
				if(sessionStorage.previousPages)
				{
					sessionStorage.removeItem('previousPages');
				}
                sessionStorage.transRequired = "No";

				window.location.href = 'index.html#homePage';

			});

			/* Navigate to Flight Finder Page */
			$('#flightSearch').click(function() {
				if(sessionStorage.previousPages)
				{
						sessionStorage.removeItem('previousPages');
				}
				if((undefined != sessionStorage.searchData) && (null != sessionStorage.searchData)){
					sessionStorage.removeItem('searchData');

				}

                sessionStorage.transRequired = "No";
                sessionStorage.isFromHome="";
                sessionStorage.navToAirportDetails="";
				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if (spath === "searchFlights") {

					$(".menuItems").trigger('click');
				} else {
					$('#brief img').show();

					if ( 'undefined' == localStorage.cityList || null == localStorage.cityList) {
						window.getAirportsCallback = function() {
						}
						window.open(iSC.getAirports);
					}

					window.location.href = 'index.html#searchFlights';

					sessionStorage.navToSearch = "yes";
				}
			});
			/* End of Navigate to Flight Finder Page */

			/* Navigate to Flight Status Page */
			$('#flightStatus').click(function() {
			    if(hasValue(sessionStorage.searchairports)){
                sessionStorage.removeItem('searchairports');
                }
                if(hasValue(sessionStorage.statusFor)){
                 sessionStorage.removeItem('statusFor');
                }
				if(undefined != sessionStorage.previousPages)
				{
					if(0 === JSON.parse(sessionStorage.previousPages).length){
						sessionStorage.removeItem('previousPages');
					}
				}
				if ((undefined != sessionStorage.flightNo) && (null != sessionStorage.flightNo) ) {
					sessionStorage.removeItem('flightNo');
				}
				if((undefined != sessionStorage.airline) || (null != sessionStorage.airline)){
					sessionStorage.removeItem('airline');
				}
                sessionStorage.transRequired = "No";
                sessionStorage.isFromHome="";
                sessionStorage.navToAirportDetails="";
				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if ("flightstatus" === spath) {

					$(".menuItems").trigger('click');
				} else {
					$('#brief img').show();
					if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
						window.getAirportsCallback = function() {
						}
						window.open(iSC.getAirports);
					}

					window.location.href = 'index.html#flightstatus';
					sessionStorage.navToflightStatus = "yes";
				}
			});
			/* End of Navigate to Flight Status Page */

			/* Navigate to Airports Page */
			$('#airportsSearch').click(function() {
				if(undefined != sessionStorage.previousPages)
				{
					if(0 === JSON.parse(sessionStorage.previousPages).length){
						sessionStorage.removeItem('previousPages');
					}
				}
                sessionStorage.transRequired = "No";
                sessionStorage.isFromHome="";
                sessionStorage.navToAirportDetails="";
				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if ("airportSearchPage" === spath) {
					$(".menuItems").trigger('click');
				} else {
					$('#brief img').show();
					/*$('#pageslide').css('z-index', '-1');*/
					sessionStorage.searchAirport = "";
					showActivityIndicator($.i18n.map.sky_please);
					sessionStorage.goNativeMapFromDetails="NO";
					window.location.href = 'index.html#airportSearchPage';
					sessionStorage.navToAirportSearch = "yes";
					$.pageslide.close();
				}
			});
			/* End of Navigate to Airports Page */

			/* Navigate to Settings Page */
			$('#settingsPage').click(function() {
				if(undefined != sessionStorage.previousPages)
				{
						sessionStorage.removeItem('previousPages');
				}
                sessionStorage.transRequired = "No";

				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if ("settings" === spath) {
					$(".menuItems").trigger('click');
				} else {
					$('#brief img').show();
					window.location.href = 'index.html#settings';
					sessionStorage.navToSettings = "yes";
				}
			});
			/* End of Navigate to Settings Page */

			/* Navigate to SkyTips Page */
			$('#skyTips').click(function() {
				if(undefined != sessionStorage.previousPages)
				{
					if(0 === JSON.parse(sessionStorage.previousPages).length){
						sessionStorage.removeItem('previousPages');
					}
				}
				sessionStorage.removeItem("skyTipsFullAirportName");
                sessionStorage.transRequired = "No";

				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if ("skyTipsFinder" === spath) {
					$(".menuItems").trigger('click');
				} else {

					$('#brief img').show();
					if ('undefined' == localStorage.cityList_Skytips || null == localStorage.cityList_Skytips || undefined == localStorage.cityList_Skytips) {

						window.open(iSC.skyTipsCityList);
					}
					else {
						var temp=JSON.parse(localStorage.cityList_Skytips);
						if(temp.AirPorts_test[0].hasOwnProperty("Airport_Name_Es"))
						{
							window.location.href = 'index.html#skyTipsFinder';
						}
						else{
							localStorage.removeItem("cityList_Skytips");
							window.open(iSC.skyTipsCityList);

						}
					}
					sessionStorage.navToskyTipsFinder = "yes";
				}
			});
			/* End of Navigate to SkyTips Page */

			/* Navigate to About SkyTeam Page */
			$('#aboutSkyteam').click(function() {
				if(undefined != sessionStorage.previousPages)
				{
					if(0 === JSON.parse(sessionStorage.previousPages).length){
						sessionStorage.removeItem('previousPages');
					}
				}
                sessionStorage.transRequired = "No";

				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if(undefined !== sessionStorage.members && "AirportDetails" === sessionStorage.members)
				{
					sessionStorage.members = "menu";
					$('#brief img').show();
					sessionStorage.members = "menu";
					window.location.href = 'index.html#memberAirlines';
				}
				else if ("aboutSkyTeam" === spath) {
					sessionStorage.members = "menu";
					$(".menuItems").trigger('click');
				} else {
					sessionStorage.members = "menu";
					$('#brief img').show();
					window.location.href = 'index.html#aboutSkyTeam';
					sessionStorage.navToaboutSky = "yes";
				}
			});
			/* End of Navigate to About SkyTeam Page */

			/* Navigate to Lounge Finder Page */                                
			$('#loungeFinderMenu').click(function() {
                sessionStorage.removeItem('airportName');
                sessionStorage.removeItem('airportCode');
				if(undefined != sessionStorage.previousPages)
				{
					if(0 === JSON.parse(sessionStorage.previousPages).length){
						sessionStorage.removeItem('previousPages');
					}
				}
				sessionStorage.lounge = "menu";
                sessionStorage.transRequired = "No";

				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if ("loungesSearch" === spath) {
					$(".menuItems").trigger('click');
				} else {
                                         $('#brief img').css('display', 'block');
                                         if (localStorage.getItem('loungesAirportList') === null || localStorage.loungesAirportList == 'undefined' || localStorage.loungesAirportList == null) {//checking for existence of lounge airport list
                                              showActivityIndicator($.i18n.map.sky_please);
                                              window.open(iSC.loungeList, "_self");
                                           }
                                         else {// checking for valid lounge airport list
                                              var temp =JSON.parse(localStorage.loungesAirportList);
                                              if(_.isArray(temp.AirportsList_Lounges)){
                                               window.location.href = 'index.html#loungesSearch';
                                              }
                                              else{
                                               localStorage.removeItem("loungesAirportList");
                                               showActivityIndicator($.i18n.map.sky_please);
                                               window.open(iSC.loungeList, "_self");
                                              }
                                          }
                                           sessionStorage.navToloungesFinderMenuView = "yes";


                       				}
			});
			/* End of Navigate to Lounge Finder Page */

			/* Navigate to SkyPriorityFinder Page */
			$('#skyPriority').click(function() {
			    sessionStorage.isFromSaved = "no";
				if(undefined != sessionStorage.previousPages)
				{
					if(0 === JSON.parse(sessionStorage.previousPages).length){
						sessionStorage.removeItem('previousPages');
					}
				}
                sessionStorage.transRequired = "No";

				var locationPath = window.location;
				locationPath = locationPath.toString();
				var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
				if ("skyPriorityFinder" === spath) {
					$(".menuItems").trigger('click');
				} else {
					$('#brief img').show();
					sessionStorage.skyPriorityAirlines ='';
					sessionStorage.airport_code ='';
					window.location.href = 'index.html#skyPriorityFinder';

				}
			});
			/* End of Navigate to SkyPriorityFinder Page */

			/* Navigate to yogaVideo Page */
                        $('#yogaVideo').click(function() {
                                                   pageStack = [];
                                                   if(sessionStorage.previousPages != undefined)
                                                   {
                                                   if(JSON.parse(sessionStorage.previousPages).length == 0){
                                                   sessionStorage.removeItem('previousPages');
                                                   }
                                                   }
                                                   sessionStorage.isFromHome="";
                                                   sessionStorage.navToAirportDetails="";
                                                   var locationPath = window.location;
                                                   locationPath = locationPath.toString();
                                                   var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                   if (spath == "yogaVideoPage") {
                                                   $(".menuItems").trigger('click');

                                                   } else {
                                                   /*$('#pageslide').css('z-index', '-1');*/
                                                   sessionStorage.searchAirport = "";
                                                   sessionStorage.fromHomeScreen="NO";
                                                   /*showActivityIndicator($.i18n.map.sky_please);*/

                                                   $('#brief img').css('display', 'block');
                                                   window.location.href = 'index.html#yogaVideoPage';
                                                   $.pageslide.close();
                                                   }
                                                   });
                        /* End of Navigate to yogaVideo Page */
		},


		render : function() {
		},
		events : {
			'touchstart *':function(){
				$.pageslide.close();
			}
		}
	});
	return menuPageView;

}); 