/* Disclaimer Funtionality */
define(['jquery', 'backbone', 'underscore', 'text!template/disclaimerTemplate.html', 'text!template/menuTemplate.html','appView'],
		function($, Backbone, _,disclaimerTemplate, menuPageTemplate,appView) {
	var settingsDisclaimerView = appView.extend({

		id : 'disclaimer-page',
		initialize : function() {
			$('#backButtonList').show();
			$('#headingText img').hide();
			$('#headingText h2').show();
			$('#headingText h2').css('margin-right','42px');

			$("#main_header_ul").hide(1,function(){
				$('#headingText h2').html($.i18n.map.sky_legal_disclaimer);
				$("#main_header_ul").show();

			});


			$('#brief img').hide();
			$('.main_header ul li:eq(1) img').off('click').click(function(e) {

				e.preventDefault();
				e.stopImmediatePropagation();

		          sessionStorage.navDirection = "Left";

				if("block" === $('#pageslide').css('display')){
					$.pageslide.close();
				}                                       
				if ("disclaimer" === sessionStorage.currentPage)
					sessionStorage.savedflights = "yes";

				$('#brief img').show();
				var currentobj=JSON.parse(sessionStorage.previousPages);
				window.location.href = "index.html#"+currentobj[0].previousPage;
				popPageOnToBackStack();
				$(this.el).undelegate();
			});
			/* End of Back button click event */
			if ("yes" === sessionStorage.savedflights) {
				$('#brief h2').hide();
			}
			sessionStorage.savedflights = "yes";
		},

		render : function() {
			sessionStorage.From = "";
			var locationPath = window.location;
			locationPath = locationPath.toString();

			var compiledTemplate = _.template(disclaimerTemplate,$.i18n.map);
			$(this.el).html(compiledTemplate);
			return appView.prototype.render.apply(this, arguments);

		},

		events : {
			'click #privacypolicy' : 'openPrivacyPolicy',
			'touchstart *' : function() {
				$.pageslide.close();
			}
		},

		/* Navigation to PrivacyPolicy Page */
		openPrivacyPolicy : function(e){
			var pageObj={};
			pageObj.previousPage="disclaimer";
			pushPageOnToBackStack(pageObj);
			window.location.href='index.html#privacyPolicy';
		}
		/* End of Navigation to PrivacyPolicy Page */
	});
	return settingsDisclaimerView;
});
/* End of Disclaimer Funtionality */
