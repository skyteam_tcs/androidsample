/* Settings Functionality */
define(['jquery','ji18n','backbone', 'underscore', 'text!template/settingsTemplate.html', 'text!template/menuTemplate.html','view/menu/newmenuPage','appView','text!template/settingsLanguagePopup.html'],
		function($,i18n, Backbone, _,settingsTemplate, menuPageTemplate,menu,appView,settingsLanguagePopup) {
	var time = "24h",
		distance = "kilometers",
		temparature = "celsius",
		date = "DD",
		skytips = "on",
		language="En";
		var self,langugeView,dateView;
		       var datePopup = Backbone.View.extend({
                                                    el : '#skyteam',
                                                    initialize : function() {
                                                        dateView=this;
                                                    },
                                                    events:{
                                                        'click .spanDateFormat' : 'checkSelectedDateFormat',
                                                        'click #operator-ok1' : 'closeDatePopup',
                                                    },
                                                    render:function(){

                                                    $(dateView.el).append(self.$el.find('script[name=datepopupTemplate]').text());
                                                    $('#blackshade').show();
                                                    dateView.$el.find('#date-popup').show();
                                                    dateView.$el.find('#operator-ok1').show();
                                                    if("DD" === localStorage.date){
                                                    dateView.$el.find('#DD').prop('checked', true);

                                                    }
                                                    else if("MMM" === localStorage.date){
                                                    dateView.$el.find('#MMM').prop('checked', true);
                                                    }
                                                    else if("YYYY" === localStorage.date){
                                                    dateView.$el.find('#YYYY').prop('checked', true);
                                                    }
                                                    else{
                                                    }

                                                    },
                                                    checkSelectedDateFormat: function(e) {
                                                    var forid = '#' + $(e.target).attr('for');
                                                    $(forid).prop("checked", true);
                                                    e.preventDefault();
                                                    },
                                                    closeDatePopup:function(e){
                                                    e.preventDefault();
                                                    e.stopImmediatePropagation();
                                                    localStorage.date = dateView.$el.find('input:radio.groupCheckbox:checked').val();

                                                    if(localStorage.date === "YYYY"){
                                                    self.$el.find('#dateSelect').val($.i18n.map.sky_chinese_date_format);

                                                    }
                                                    else if(localStorage.date == "DD"){
                                                    self.$el.find('#dateSelect').val($.i18n.map.sky_date_format);
                                                    }
                                                    else{
                                                    self.$el.find('#dateSelect').val($.i18n.map.sky_otherdate_format);
                                                    }
                                                    sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
                                                    window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");


                                                    dateView.$el.find('#date-popup').remove();
                                                    $('#blackshade').hide();
                                                    }
                                                    });

               dateView = new datePopup();

               var languagePopup = Backbone.View.extend({
                           el : '#skyteam',
                           initialize : function() {
                           langugeView=this;
                           },
                           events:{
                           'click #operator-ok' : 'closeLanguagePopup',
                           'click .langText' : 'checkSelectedLanguage',
                           },
                           render:function(){
                           var template = _.template(settingsLanguagePopup,$.i18n.map);
                           $('#blackshade').show();
                           $(langugeView.el).append(template);
                            $('#members-popup').show();
                            $('#operator-ok').show();


                            if(localStorage.language != undefined || localStorage.language !== "undefined")
                            {
                                langugeView.$el.find("#lang-"+localStorage.language.toLowerCase()).prop('checked',true);
                            }
                            else {
                                langugeView.$el.find('#lang-en').prop('checked', true);
                            }



                           },
                            checkSelectedLanguage: function(e) {
                            var forid = '#' + $(e.target).attr('for');
                            $(forid).prop("checked", true);
                            e.preventDefault();

                            },
                           closeLanguagePopup:function(e){
                               e.preventDefault();
                               e.stopImmediatePropagation();
                               var selectedLanguage;
                                selectedLanguage = langugeView.$el.find('input:radio.groupCheckbox:checked').val();
                                localStorage.previouslang = localStorage.language;

                                if(localStorage.previouslang !== selectedLanguage){
                                /************** start ***************/
                                localStorage.language = selectedLanguage;
                                sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';

                                langugeView.$el.find('#members-popup').hide();
                                setTimeout(function(){
                                           window.open('settings:'+sessionStorage.settingsURL,"_self");
                                },500);

                                        window.networkStatCallBack = function(){
                                        var netStatus =JSON.parse(localStorage.networkStatus);
                                        if(netStatus.network == "online"){
                                        showActivityIndicatorForAjax('true',"langURLcancel");
                                        window.getLangCallback = function() {
                                         var strLanguage = localStorage.language;
                                         if(localStorage.language === "zh-Hant")
                                         {
                                              strLanguage = "zh_TW";
                                         }
                                        $.i18n.properties({
                                                          name:'Messages',
                                                          path:'bundle/',
                                                          mode:'map',
                                                          language:strLanguage,
                                                          callback: function(e) {

                                                          sessionStorage.globalRefresh="yes";
                                                          require(["router"], function(init){
                                                                  window.open("analytics://_trackLanguage#" + localStorage.language, "_self");

                                                                  setTimeout(function(){
                                                                             init.getSettings();
                                                                             },1000);
                                                                  loadCustomCss();
                                                                  $("#blackshade").hide();
                                                                  });

                                                          }
                                                          });

                                        }

                                        setTimeout(function(){ window.open(iSC.getLang,"_self"); //calling native to store language cache in native
                                                   },500);
                                        }
                                        else if(netStatus.network == "offline")
                                        {
                                        localStorage.language=localStorage.previouslang;
                                        sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
                                        iSU.settings(); //updating in SettingsJson File
                                        openAlertWithOk($.i18n.map.sky_notconnected);
                                        $("#blackshade").hide();

                                        }
                                        };
                                        window.open(iSC.Network, "_self");
                                    }
                                    else{
                                      $("#blackshade").hide();
                                      langugeView.$el.find('#members-popup').remove();
                                    }
                                    }

                            });
               langugeView = new languagePopup();
               /* Language popup */


	var settingsView = appView.extend({

		id : 'settings-page',
		initialize : function() {
			$('#backButtonList').hide();
			$('#headingText h2').css('margin-right','');
			if(undefined != sessionStorage.previousPages)
			{
				if(0 === JSON.parse(sessionStorage.previousPages).length){
					sessionStorage.removeItem('previousPages');
				}
			}
			if ('menuItems' != $('.main_header ul li:eq(0) a').attr('class')) {
				$('.main_header ul li:eq(0) img').remove();
				$('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

			}


			$('#headingText img').css('display', 'none');
			$('#headingText h2').css('display', 'block');
			$("#main_header_ul").hide(1,function(){
				$('#headingText h2').html($.i18n.map.sky_settings);
				$("#main_header_ul").show();

			});


			if ("yes" === sessionStorage.savedflights) {
				$('#brief img').css('display', 'block');
				$('#brief h2').css('display', 'none');
				$(".menuItems").pageslide();
				var template = _.template(menuPageTemplate,$.i18n.map);
				$('#pageslide').append(template);                                               
				sessionStorage.savedflights = "no";
			}

			if ("yes" === sessionStorage.globalRefresh){

				$('#pageslide').empty();
				new menu();
				sessionStorage.globalRefresh = "no";
			}
			self = this;

		},

		render : function() {
			//invalidating Flight finder departure date
			sessionStorage.FS_departureDate ="";

			sessionStorage.skyTipsAirportName = "";
			setTimeout(function() {
				hideActivityIndicator();
			}, 500);
			sessionStorage.From = "Menu";
			sessionStorage.skytipstheme = 1;
			var locationPath = window.location;
			locationPath = locationPath.toString();
			var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);

			sessionStorage.currentPage = "settings";

			if ("settings" === spath && "yes" === sessionStorage.navToSettings) {
				$(".menuItems").trigger('click');
				sessionStorage.navToSettings = "no";
			}


			var compiledTemplate = _.template(settingsTemplate,$.i18n.map);
			$(this.el).html(compiledTemplate);
			/*var settingsResponse = JSON.parse(localStorage.savedFlightDetails);
			time = settingsResponse.settings[0].Time;
			date = settingsResponse.settings[0].Date;
			distance = settingsResponse.settings[0].Distance;
			temparature = settingsResponse.settings[0].Temperature;
			skytips = settingsResponse.settings[0].SkyTips;
			language=settingsResponse.settings[0].Language;*/
			time = localStorage.time;
            date = localStorage.date;
            distance = localStorage.distance;
            temparature = localStorage.temparature;
            skytips = localStorage.skytips;
            language = localStorage.language;
			if("24h" === localStorage.time){
				self.$el.find('#hours24').removeClass('offHover').addClass('onHover');
				self.$el.find('#hours12').removeClass('onHover').addClass('offHover');
			}
			if("12h" === localStorage.time){
				self.$el.find('#hours12').removeClass('offHover').addClass('onHover');
				self.$el.find('#hours24').removeClass('onHover').addClass('offHover');
			}
			if("kilometers" === localStorage.distance){
				self.$el.find('#kilometers').removeClass('offHover').addClass('onHover');
				self.$el.find('#miles').removeClass('onHover').addClass('offHover');
			}
			if("miles" === localStorage.distance){
				self.$el.find('#miles').removeClass('offHover').addClass('onHover');
				self.$el.find('#kilometers').removeClass('onHover').addClass('offHover');
			}
			if("celsius" === localStorage.temparature){
				self.$el.find('#celsius').removeClass('offHover').addClass('onHover');
				self.$el.find('#fahrenheit').removeClass('onHover').addClass('offHover');
			}
			if("fahrenheit" === localStorage.temparature){
				self.$el.find('#fahrenheit').removeClass('offHover').addClass('onHover');
				self.$el.find('#celsius').removeClass('onHover').addClass('offHover');
			}
			//Setting the selected DateFormat in visual start
			if("DD" === localStorage.date){
				self.$el.find('#dateSelect').val($.i18n.map.sky_date_format);
			}
			else if("MMM" === localStorage.date){
				self.$el.find('#dateSelect').val($.i18n.map.sky_otherdate_format);
			}
			else if("YYYY" === localStorage.date){
				self.$el.find('#dateSelect').val($.i18n.map.sky_chinese_date_format);
			}
			else{
			}
			//Setting the selected DateFormat in visual end
			if("on" === localStorage.skytips){
				self.$el.find('#on').removeClass('offHover').addClass('onHover');
				self.$el.find('#off').removeClass('onHover').addClass('offHover');
			}
			if("off" === localStorage.skytips){
				self.$el.find('#off').removeClass('offHover').addClass('onHover');
				self.$el.find('#on').removeClass('onHover').addClass('offHover');
			}

			self.$el.find('#languageSelect').val($.i18n.map.sky_lang);
return appView.prototype.render.apply(this, arguments);
		},

		events : {
			'click #hours24' : 'openHours24',
			'click #hours12' : 'openHours12',
			'click #formatDD' : 'openFormatDD',
			'click #formatMM' : 'openFormatMM',
			'click #kilometers' : 'openKilometers',
			'click #miles' : 'openMiles',
			'click #celsius' : 'openCelsius',
			'click #fahrenheit' : 'openFahrenheit',
			'click #disclamer' : 'openDisclaimer',
			'click #languageSelect' : 'openPopup',
			'click #operator-ok' : 'closeLanguagePopup',
			'click #dateSelect' : 'openDatePopup',
			'click #operator-ok1' : 'closeDatePopup',
			'click #skyTeamSite' : 'openSkyteam',
			'click .langText' : 'checkSelectedLanguage',
			'click #on' : 'openOn',
			'click #off' : 'openOff',
			'click .spanDateFormat' : 'checkSelectedDateFormat',
			'touchstart *':function(){
				$.pageslide.close();
			}
		},

		/* Navigation to SkyTeam WebSite */
		openSkyteam : function(e){
			if("En" === localStorage.language){
				sessionStorage.memberDetailsURL = "http://www.skyteam.com";
			}
			else if (localStorage.language == "zh-Hant"){
                 sessionStorage.memberDetailsURL = "http://www.skyteam.com/zh-TW/";
            }
			else
			{
				sessionStorage.memberDetailsURL = "http://www.skyteam.com/"+localStorage.language+"/";
			}
			window.open(iSC.openmemberdetails, "_self");
		},
		/* End of Navigation to SkyTeam WebSite */

		/* Date format selection open - start*/
		openDatePopup : function(e){
			dateView.render();
		},
		/* Date format selection open- end */

		/* Localization */
		openPopup : function(e){
		langugeView.render();

		},
		/* End of Localization */

			/* Navigation to Disclaimer Page */
			openDisclaimer:function(e){
				var pageObj={};
				pageObj.previousPage="settings";
				pushPageOnToBackStack(pageObj);
				window.location.href='index.html#disclaimer';
			},
			/* End of Navigation to Disclaimer Page */

			/* Change Time to 24 hour Format */
			openHours24 : function() {
				self.$el.find('#hours24').removeClass('offHover').addClass('onHover');
				self.$el.find('#hours12').removeClass('onHover').addClass('offHover');
				localStorage.time="24h";
				time = "24h";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");

			},
			/* End of Change Time to 24 hour Format */

			/* Change Time to 12 hour Format */
			openHours12 : function() {
				self.$el.find('#hours12').removeClass('offHover').addClass('onHover');
				self.$el.find('#hours24').removeClass('onHover').addClass('offHover');
				localStorage.time="12h";
				time = "12h";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");

			},
			/* End of Change Time to 12 hour Format */

			/* Change Date Format to DD MMM YY */
			openFormatDD : function() {
				self.$el.find('#formatDD').removeClass('offHover').addClass('onHover');
				self.$el.find('#formatMM').removeClass('onHover').addClass('offHover');
				localStorage.date="DD";
				date = "DD";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips;
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips,"_self");
				//  $('.version span').html("15 Nov 13");
			},
			/* End of Change Date Format to DD MMM YY */

			/* Change Date Format to MMM DD YY */
			openFormatMM : function() {
				self.$el.find('#formatMM').removeClass('offHover').addClass('onHover');
				self.$el.find('#formatDD').removeClass('onHover').addClass('offHover');
				localStorage.date="MMM";
				date = "MMM"
					sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");

				// $('.version span').html("Nov 15 13");
			},
			/* End of Change Date Format to MMM DD YY */

			/* Change Distance Format to Kilometers */
			openKilometers : function() {
				self.$el.find('#kilometers').removeClass('offHover').addClass('onHover');
				self.$el.find('#miles').removeClass('onHover').addClass('offHover');
				localStorage.distance="kilometers";
				distance = "kilometers";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");

			},
			/* End of Change Distance Format to Kilometers */

			/* Change Distance Format to Miles */
			openMiles : function() {
				self.$el.find('#miles').removeClass('offHover').addClass('onHover');
				self.$el.find('#kilometers').removeClass('onHover').addClass('offHover');
				localStorage.distance="miles";
				distance = "miles";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");
			},
			/* End of Change Distance Format to Miles */

			/* Change Temperature Format to Celsius */
			openCelsius : function() {
				self.$el.find('#celsius').removeClass('offHover').addClass('onHover');
				self.$el.find('#fahrenheit').removeClass('onHover').addClass('offHover');
				localStorage.temparature="celsius";
				temparature="celsius";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");
			},
			/* End of Change Temperature Format to Celsius */

			/* Change Temperature Format to Fahrenheit */
			openFahrenheit : function() {
				self.$el.find('#fahrenheit').removeClass('offHover').addClass('onHover');
				self.$el.find('#celsius').removeClass('onHover').addClass('offHover');
				localStorage.temparature="fahrenheit";
				temparature="fahrenheit";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");
			},
			/* End of Change Temperature Format to Fahrenheit */

			/* On Skytips */
			openOn : function() {
				self.$el.find('#on').removeClass('offHover').addClass('onHover');
				self.$el.find('#off').removeClass('onHover').addClass('offHover');
				localStorage.skytips="on";
				skytips="on";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");
			},
			/* End of On Skytips */

			/* Off Skytips*/
			openOff : function() {
				self.$el.find('#off').removeClass('offHover').addClass('onHover');
				self.$el.find('#on').removeClass('onHover').addClass('offHover');
				localStorage.skytips="off";
				skytips="off";
				sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO';
				window.open('settings:'+localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+skytips+','+localStorage.language+','+'NO',"_self");
			},
			/* End of Off Skytips */
	});

	return settingsView;
});
/* End of Settings Functionality */