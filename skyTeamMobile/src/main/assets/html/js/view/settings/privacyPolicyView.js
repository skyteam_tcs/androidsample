/* Privacy Policy Functionality */
define(['jquery', 'backbone', 'underscore', 'text!template/PrivacyPolicyTemplate.html', 'text!template/menuTemplate.html','appView'],
		function($, Backbone, _,privacyPolicyTemplate, menuPageTemplate,appView) {
	var settingsPrivacyPolicyView = appView.extend({

		id : 'privacyPolicy-page',
		initialize : function() {
			$('#headingText img').hide();
			$('#headingText h2').show();
			$("#main_header_ul").hide(1,function(){
				$('#headingText h2').html($.i18n.map.sky_privacypolicy);
				$("#main_header_ul").show();

			});

			$('#brief img').hide();
			$('.main_header ul li:eq(1) img').off('click').on('click',function(e) {
		         e.preventDefault();
                 e.stopImmediatePropagation();
		       sessionStorage.navDirection = "Left";

				var currentobj=JSON.parse(sessionStorage.previousPages);
				window.location.href = "index.html#"+currentobj[0].previousPage;

				popPageOnToBackStack();
				$('.main_header ul li:eq(0) img').off('click');
				if ("yes" === sessionStorage.savedflights) {
					$('#brief img').hide();
					$('#brief h2').hide();
				}
			});
			/* End of Back button click event */
		},

		render : function() {
			sessionStorage.From = "";
			var locationPath = window.location;
			locationPath = locationPath.toString();

			var compiledTemplate = _.template(privacyPolicyTemplate,$.i18n.map);
			$(this.el).html(compiledTemplate);
			return appView.prototype.render.apply(this, arguments);
		}
	});
	return settingsPrivacyPolicyView;
});
/* End of Privacy Policy Functionality */