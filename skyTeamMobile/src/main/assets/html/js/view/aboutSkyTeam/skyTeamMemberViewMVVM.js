/* Functionality to display Skyteam member airlines facts & figures,details,etc    */
define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'models/aboutSkyteam/memberAirlineModel','text!template/member_info.html', 'text!template/menuTemplate.html','appView'], function($,_, Backbone, utilities, constants,memberAirlineModel, skyTeamMemberInfoTemplate, menuPageTemplate,appView) {
       var self;
var skyTeamMemberInfoView = appView.extend({
            id : 'skyTeamMembers-page',
            initialize : function()
                            {
                            $('#backButtonList').show();
                            var site;
                            $('#headingText img').hide();
                            $('#headingText h2').show();


                            $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                            var currentobj=JSON.parse(sessionStorage.previousPages);
                            e.preventDefault();
                            e.stopImmediatePropagation();
                            window.location.href = "index.html#"+currentobj[0].previousPage;
                            popPageOnToBackStack();
                            if(sessionStorage.lounge=="airportDetails"){}

                            $('.main_header ul li:eq(1) img').off('click');
                            });
                            if (sessionStorage.isFromSaved == "yes") {
                            $('#brief img').hide();
                            $('#brief h2').hide();
                            }
                            else
                            {
                            $('#brief img').show();
                            $('#brief h2').hide();
                            }
                            if( sessionStorage.savedflights == "yes"){
                            sessionStorage.savedflights = "no";
                            }
                            sessionStorage.skyTeamMember="yes";
                            sessionStorage.currentPage="skyTeamMember";
                                           self =this;
                            },
            render : function()
            {
                            hideActivityIndicator();
                            if(sessionStorage.From == "searchResults"){
                            sessionStorage.From = "searchResults";
                            }else{
                            sessionStorage.From = "";
                            }
                            var locationPath = window.location;
                            locationPath = locationPath.toString();

                            var dataModel = new memberAirlineModel();

                            var memberModelData = dataModel.get(sessionStorage.memberName);
                            var i18nModel = $.i18n.map;
                            compiledData = _.template(JSON.stringify(memberModelData),i18nModel);
                            compiledData = JSON.parse(compiledData);


                            for (var key in i18nModel){
                            compiledData[key] = i18nModel[key];
                            }

                            var compiledTemplate = _.template(skyTeamMemberInfoTemplate,compiledData);
                            $(this.el).html(compiledTemplate);
                            $('#headingText h2').html(compiledData.Member_Name);
                            $('.main_header ul li:eq(0) img').on('click');
                            var skyTeamMemberInfoResponse = JSON.parse(localStorage.skyTeamMemberDisplay);
                            for (var i = 0, memberLength= skyTeamMemberInfoResponse.member.length; i <memberLength ; i++)
                            {
                                self.$el.find('.white_button p').html($.i18n.map.sky_websitevisit);

                                self.$el.find(".member_name ul li:eq(0)").css("background", "url('images/members/"+skyTeamMemberInfoResponse.member[i].Image+"-Logo.png') no-repeat center");
                                self.$el.find('.mem_details ul li:eq(15) p:eq(0)').html(skyTeamMemberInfoResponse.member[i].Designation);
                                site=skyTeamMemberInfoResponse.member[i].Url;
                            }

                            return appView.prototype.render.apply(this, arguments);
                            },
            events:
            {
            'click .white_button':'openMemberSite',
            'touchstart *':function(){
            $.pageslide.close();
            }
            },
            /* Function to open Skyteam Member airlines website from application  */
            openMemberSite:function(e){
            sessionStorage.memberDetailsURL=site;
            window.open(iSC.openmemberdetails, "_self");
            },
            /* End of function */
            });

return skyTeamMemberInfoView;
});