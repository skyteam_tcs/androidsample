/*     About Skyteam(from Menu) & Member Airlines(from AirportDetails) functionality  */

define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'text!template/AboutSkyteam.html', 'text!template/menuTemplate.html','appView'], function($, _, Backbone, utilities, constants, aboutSkyteamTemplate, menuPageTemplate,appView) {
       var self;
       var aboutSkyteamView = appView.extend({
                                                   
                                                   id : 'aboutSkyteam-page',
                                                   
                                                   initialize : function() {
                                                   $('#backButtonList').hide();
                                                   $('#headingText h2').css('margin-right', '');
                                                   self = this;
                                                   if(sessionStorage.previousPages != undefined)
                                                   {
                                                   if(JSON.parse(sessionStorage.previousPages).length == 0){
                                                   sessionStorage.removeItem('previousPages');
                                                   }
                                                   }
                                                   $('#headingText img').css('display', 'none');
                                                   $('#headingText h2').css('display', 'block');
                                                   if (sessionStorage.members == "AirportDetails") {
                                                   $('#backButtonList').show();
                                                    $('#brief h2').css('display', 'none');
                                          
                                                   
                                                   $("#main_header_ul").hide(1,function(){
                                                                             $('#headingText h2').text($.i18n.map.sky_member_airlines);
                                                                             $("#main_header_ul").show();
                                                                             
                                                                             });
                                                   
                                                   
                                                   $('.main_header ul li:eq(1) img').off('click').click(function(e) {
                                                                                                        sessionStorage.members = "menu";
                                                                                                        var currentobj = JSON.parse(sessionStorage.previousPages);
                                                                                                        showActivityIndicatorForAjax('false');
                                                                                                        e.preventDefault();
                                                                                                        e.stopImmediatePropagation();
                                                                                                        window.location.href = "index.html#" + currentobj[0].previousPage;
                                                                                                        popPageOnToBackStack();
                                                                                                        $('#brief img').css('display', 'block');
                                                                                                        
                                                                                                   
                                                                                                        
                                                                                                        if (sessionStorage.isFromSaved == "yes") {
                                                                                                        $('#brief img').css('display', 'none');
                                                                                                        $('#brief h2').css('display', 'none');
                                                                                                        }
                                                                                                        });
                                                   }
                                                   else {
                                                   //changes for s2 4.0.1 issue
                                                   $("#main_header_ul").hide(1,function(){
                                                                             $('#headingText h2').html($.i18n.map.sky_about);
                                                                             $("#main_header_ul").show();
                                                                             
                                                                             });
                                                   // end of changes for s2 4.0.1 issue
                                                   
                                                 
                                                   if (sessionStorage.savedflights == "yes") {
                                                   $('#brief img').css('display', 'block');
                                                   $('#brief h2').css('display', 'none');
                                                   $(".menuItems").pageslide();
                                          
                                                   sessionStorage.savedflights = "no";
                                                   }
                                                   if (sessionStorage.isFromSaved == "yes") {
                                                   $('#brief img').css('display', 'none');
                                                
                                                   }
                                                   else
                                                   {
                                                   $('#brief img').css('display', 'block');
                                                  
                                                   }
                                                      $('#brief h2').css('display', 'none');
                                                   }
                                                   
                                                   if (sessionStorage.skyTeamMember == "yes") {
                                                 
                                                   $(".menuItems").pageslide();
                                         
                                                   sessionStorage.savedflights = "no";
                                                   sessionStorage.skyTeamMember = "no";
                                                   }
                                                   if (sessionStorage.members == "menu" || sessionStorage.isFromSaved != "yes") {
                                                        $('#brief img').css('display', 'block');
                                                   }
                                                   else{
                                                   $('#brief img').css('display', 'none');
                                                   $('#brief h2').css('display', 'none');
                                                   }
                                                  
                                                   sessionStorage.currentPage = "aboutSkyTeam";
                                                   },
                                                   
                                                   render : function()
                                                   {
                                                   sessionStorage.skyTipsAirportName = "";
                                                   setTimeout(function() {hideActivityIndicator();}, 500);
                                                   if (sessionStorage.members == "AirportDetails" && sessionStorage.currentPage == "savedflights-page") {
                                                   sessionStorage.currentPage = "savedflights-page";
                                                   }
                                                   else if (sessionStorage.members == "menu") {
                                                   sessionStorage.From = "Menu";
                                                   sessionStorage.currentPage = "aboutSkyTeam";
                                                   }
                                                   else {
                                                   if (sessionStorage.From == "searchResults") {
                                                   sessionStorage.From = "searchResults";
                                                   }
                                                   else {
                                                   sessionStorage.From = "";
                                                   }
                                                   }
                                                   sessionStorage.skytipstheme = 1;
                                                   var locationPath = window.location;
                                                   locationPath = locationPath.toString();
                                                   var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                   if ((spath == "aboutSkyTeam" || spath == "memberAirlines") && sessionStorage.navToaboutSky == "yes") {
                                                   $(".menuItems").trigger('click');
                                                   sessionStorage.navToaboutSky = "no";
                                                   }
                                                   var compiledTemplate = _.template(aboutSkyteamTemplate,$.i18n.map);
                                                   $(this.el).html(compiledTemplate);
                                                   if (sessionStorage.members == "AirportDetails") {
                                                   self.$el.find(".abt_skyteam ul li:eq(1)").css('display', 'none');
                                                   self.$el.find(".abt_skyteam ul li:eq(2)").css('display', 'none');
                                                   self.$el.find(".abt_skyteam ul li:eq(3)").css('display', 'none');
                                                   self.$el.find(".members").css('display', 'none');

// adding opertaing airlines tab to DOM
						 
							 $('<div\>',{class: 'operatingAir'})
									.append( $('<div\>',{class:'airlineBtn'})
											.append( $('<h3\>',{class:'airlineTitle'})))
									.append( $('<div\>',{
											class: 'members'})
											.append( $('<ul\>',{
												class:'members',
												 id: 'membersList'})))
									 .appendTo(self.$el.find(".skyteam_about"));
					self.$el.find(".abt_skyteam ul li:eq(0)").html(sessionStorage.airportName+' '+$.i18n.map.sky_airport+' ('+ sessionStorage.airport_code+')');
// adding marketing airlines tab to DOM

						$('<div\>', {class: 'marketingAir'})
							.append($('<div\>',{class: 'airlineBtn'})
									.append($('<h3\>', {class: 'airlineTitleMkt'})))
							.append($('<div\>', {class:'members'})
									.append($('<ul\>',{id :'membersListMarket'})))
							.appendTo(self.$el.find(".skyteam_about"));
                                                   var airlines = JSON.parse(sessionStorage.airlines);
                                                   var strOperating=airlines.Operating.Airlines;
                                                   var myOperatingArray = listToAray(strOperating, ',');
                                                   var strMarketing=airlines.Marketing.Airlines;
                                                   var myMarketingArray = listToAray(strMarketing, ',');
                                                   var i,j;
                                                   for(i=0,maxlen=myOperatingArray.length; i<maxlen ; i++)
                                                   {
                                                   for(j=0,maxlength=myMarketingArray.length; j <maxlength ; j++)
                                                        {
                                                                if(myOperatingArray[i] == myMarketingArray[j])
                                                                {
                                                                    myMarketingArray.splice(j,1);
                                                                    break;
                                                                }
                                                        }
                                                   }
                                                 
                                                   self.$el.find('.airlineTitle').html($.i18n.map.sky_airline_title_operating+' - ' + sessionStorage.airport_code);
                                                   self.$el.find('.airlineTitleMkt').html($.i18n.map.sky_airline_title_marketing+' - '+sessionStorage.airport_code);
                                                   
                                                   var oper_height= (((myOperatingArray.length / 2) + (myOperatingArray.length % 2 ? 1 : 0.5)) * 65 )+ 5;
                                                   var marke_height= ((( myMarketingArray.length / 2) + (myMarketingArray.length % 2 ? 1 : 0.5)) * 65 )+ 30;
                                                   self.$el.find(".operatingAir").css('height',oper_height);
                                                   self.$el.find(".marketingAir").css('height',marke_height);

                                                   
                                                   for (var i = 0,maxlen= myOperatingArray.length; i <maxlen; i++) {
                                                   $('<li\>',{id:myOperatingArray[i] +"Logo"})
										.css("background", "#fff url('images/members/" + myOperatingArray[i] + "-Logo.png') no-repeat center")
									.appendTo(self.$el.find("#membersList"));
							
                                                  }
                                                    for (var i = 0, arrLength=myMarketingArray.length; i <arrLength ; i++) {
                                                   $('<li\>',{id: myMarketingArray[i] + "Logo"})
							.css("background", "#fff url('images/members/" + myMarketingArray[i] + "-Logo.png') no-repeat center")
							.appendTo(self.$el.find("#membersListMarket"));
                                                   }
                                                   if(myOperatingArray.length == 0)
                                                   {
                                                        self.$el.find("#operatingAir").css('display','none');
                                                   }
                                                   if(myMarketingArray.length == 0)
                                                   {
                                                        self.$el.find("#marketingAir").css('display','none');
                                                   }
                                                   }
                                                   return appView.prototype.render.apply(this, arguments);
                                                   },
                                                   events : {
                                                   'click .members ul li' : 'openMembers',
                                                   'click .abt_skyteam ul li:eq(2)' : 'openSkyTeamSite',
                                                   'click .abt_skyteam ul li:eq(3)' : 'openFacebook',
                                                   'touchstart *' : function() {
                                                   $.pageslide.close();
                                                   }
                                                   },
                                                   /* Function to navigate to skyteam member airlines  */
                                                   openMembers : function(e, id) {
                                                   $('.main_header ul li:eq(0) img').off('click');
                                                   sessionStorage.memberName = e.currentTarget.id;
                                                   
                                                   window.open(iSC.memberdetails, "_self");
                                                   },
                                                   /* End of function */
                                                   
                                                   /* Function to open Skyteam website from application */
                                                   openSkyTeamSite : function(e) {

                                                   if(localStorage.language == "En"){
                                                        sessionStorage.memberDetailsURL = "http://www.skyteam.com";
                                                   }
                                                   else if (localStorage.language == "zh-Hant"){
                                                        sessionStorage.memberDetailsURL = "http://www.skyteam.com/zh-TW/";
                                                   }
                                                   else {
                                                        sessionStorage.memberDetailsURL = "http://www.skyteam.com/"+localStorage.language+"/";
                                                   }
                                                   window.open(iSC.openSkyTeamWebSite, "_self");
                                                   },
                                                   /* End of function */
                                                   /* Function to open Facebook from application */
                                                   openFacebook : function(e) {

                                                   sessionStorage.memberDetailsURL = "http://facebook.com/skyteam";
                                                   window.open(iSC.openSkyTeamWebSite, "_self");
                                                   },
                                                   /* End of function */
                                                   });
       return aboutSkyteamView;
       });