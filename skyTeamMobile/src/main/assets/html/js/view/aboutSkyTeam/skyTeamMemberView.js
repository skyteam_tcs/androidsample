/* Functionality to display Skyteam member airlines facts & figures,details,etc    */
define(
		['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'text!template/member_info.html', 'text!template/menuTemplate.html'],
		function($,_, Backbone, utilities, constants, skyTeamMemberInfoTemplate, menuPageTemplate) {
			
			var skyTeamMemberInfoView = Backbone.View.extend({
				
				el : '#skyteam',
				id : 'skyTeamMembers-page',
				
				initialize : function() {
				
					$('#backButtonList').show();
					$('#headingText img').hide();
					$('#headingText h2').show();
					var site;
					
					$('.main_header ul li:eq(1) img')
						.off('click')
						.on('click', function(e) {
							e.preventDefault();
							e.stopImmediatePropagation();
                            sessionStorage.navDirection = "Left";

							if ( $('#pageslide').css('display') === "block" ) {
								$.pageslide.close();
							} else {  
								var currentobj = JSON.parse( sessionStorage.previousPages );
								window.location.href = "index.html#" + currentobj[0].previousPage;
								popPageOnToBackStack();
								if ( sessionStorage.lounge === "AirportDetails" ) {
									$('.main_header ul li:eq(1)').append($('<img></img>').attr('src', './images/back.png'));
								} else if ( sessionStorage.members !== "menu" ) {
										$('.main_header ul li:eq(1) img').remove();
										$('.main_header ul li:eq(1)').append($('<img></img>').attr('src', './images/back.png'));
								} else {}
								$('.main_header ul li:eq(1) img').off('click');
							}
						});
					
					if ( sessionStorage.isFromSaved === "yes" ) {
						$('#brief img').hide();
						$('#brief h2').hide();
					} else {
						$('#brief img').show();
						$('#brief h2').hide();
					}
					
					if ( sessionStorage.savedflights === "yes" ) {
						sessionStorage.savedflights = "no";
					}
					sessionStorage.skyTeamMember = "yes";
					sessionStorage.currentPage = "skyTeamMember";
				},
				
				render : function()
				{
					hideActivityIndicator();
					if ( sessionStorage.From === "searchResults" ) {
						sessionStorage.From = "searchResults";
					} else {
						sessionStorage.From = "";
					}
					
					var locationPath = window.location;
					locationPath = locationPath.toString();
					var compiledTemplate = _.template( skyTeamMemberInfoTemplate, $.i18n.map );
					$(this.el).html( compiledTemplate );
					$('.main_header ul li:eq(0) img').on('click');
					var skyTeamMemberInfoResponse = JSON.parse( localStorage.skyTeamMemberDisplay );
					
					for (var i = 0, memLen = skyTeamMemberInfoResponse.member.length; i < memLen; i++ ) {

						$('#headingText h2').css('margin-right', '57px');
						$('.white_button p').html( $.i18n.map.sky_websitevisit );
						$(".member_name ul li:eq(0)").css("background", "url('images/members/" + skyTeamMemberInfoResponse.member[i].Image + "-Logo.png') no-repeat center");
						$('.mem_details ul li:eq(15) p:eq(0)').html( skyTeamMemberInfoResponse.member[i].Designation );
						site = skyTeamMemberInfoResponse.member[i].Url;

						switch( sessionStorage.memberName ) {
						case 'SULogo':
							$('#headingText h2').html($.i18n.map.sky_SU);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_SU);
							$('.member_name ul li:eq(1) span').html('Aeroflot Russian Airlines');
							$('.mem_details ul li:eq(0) p:eq(1)').html("131");
							$('.mem_details ul li:eq(1) p:eq(1)').html("500");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_4_4_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1923");
							$('.mem_details ul li:eq(4) p:eq(1)').html("55");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_20_9_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Moscow);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2006");
							$('.mem_details ul li:eq(8) p:eq(1)').html("143");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_4_2_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("129");
							$('.mem_details ul li:eq(11) p:eq(1)').html("17,800");
							$('.mem_details ul li:eq(12) p:eq(1)').html("SVO");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Aeroflot_Bonus);
							$('.mem_details ul li:eq(14) p:eq(1)').html("AEROFLOT.RU");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Vitaly_Saveliev);
							$('.mem_info ul li span').html($.i18n.map.sky_SULogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_SU);
							break;
						case 'ARLogo':
							$('#headingText h2').html($.i18n.map.sky_AR);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_AR);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_AR);
							$('.mem_details ul li:eq(0) p:eq(1)').html("58");
							$('.mem_details ul li:eq(1) p:eq(1)').html("248");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_1_9_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1950");
							$('.mem_details ul li:eq(4) p:eq(1)').html("13");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_8_3_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Buenos_Aires);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2012");
							$('.mem_details ul li:eq(8) p:eq(1)').html("46+(22)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_2_1_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("14");
							$('.mem_details ul li:eq(11) p:eq(1)').html("11,584");
							$('.mem_details ul li:eq(12) p:eq(1)').html("EZE, AEP");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Aerolíneas_Plus);
							$('.mem_details ul li:eq(14) p:eq(1)').html("AEROLINEAS.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Mariano_Recalde);
							$('.mem_info ul li span').html($.i18n.map.sky_ARLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_AR);
							break;
						case 'AMLogo':
							$('#headingText h2').html($.i18n.map.sky_AM);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_AM);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_AM);
							$('.mem_details ul li:eq(0) p:eq(1)').html("79");
							$('.mem_details ul li:eq(1) p:eq(1)').html("600");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_2_1_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1934");
							$('.mem_details ul li:eq(4) p:eq(1)').html("20");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_15_5_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Mexico_City);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2000");
							$('.mem_details ul li:eq(8) p:eq(1)').html("60+(60)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_3_4_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("11");
							$('.mem_details ul li:eq(11) p:eq(1)').html("13,165");
							$('.mem_details ul li:eq(12) p:eq(1)').html("MEX, MTY, GDL, HMO");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Club_Premier);
							$('.mem_details ul li:eq(14) p:eq(1)').html("AEROMEXICO.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Andrés_Conesa);
							$('.mem_info ul li span').html($.i18n.map.sky_AMLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_AM);
							break;
						case 'UXLogo':
							$('#headingText h2').html($.i18n.map.sky_UX);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_UX);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_UX);
							$('.mem_details ul li:eq(0) p:eq(1)').html("42");
							$('.mem_details ul li:eq(1) p:eq(1)').html("184");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_1_5_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1986");
							$('.mem_details ul li:eq(4) p:eq(1)').html("19");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_9_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Llucmajor_Palma);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2007");
							$('.mem_details ul li:eq(8) p:eq(1)').html("43");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_24_5_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("37");
							$('.mem_details ul li:eq(11) p:eq(1)').html("3,150");
							$('.mem_details ul li:eq(12) p:eq(1)').html("MAD");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Flying_Blue);
							$('.mem_details ul li:eq(14) p:eq(1)').html("AIREUROPA.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Maria_Jose_Hidalgo);
							$('.mem_info ul li span').html($.i18n.map.sky_UXLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_UX);
							break;
						case 'AFLogo':
							$('#headingText h2').html($.i18n.map.sky_AF);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_AF);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_AF);
							$('.mem_details ul li:eq(0) p:eq(1)').html("191");
							$('.mem_details ul li:eq(1) p:eq(1)').html("1,500");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_25_6_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1933");
							$('.mem_details ul li:eq(4) p:eq(1)').html("95");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_51_6_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Paris);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2000");
							$('.mem_details ul li:eq(8) p:eq(1)').html("238+(112)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_24_5_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("139");
							$('.mem_details ul li:eq(11) p:eq(1)').html("65,324");
							$('.mem_details ul li:eq(12) p:eq(1)').html("CDG, LYS");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Flying_Blue);
							$('.mem_details ul li:eq(14) p:eq(1)').html("AIRFRANCE.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Frédéric_Gagey);
							$('.mem_info ul li span').html($.i18n.map.sky_AFLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_AF);
							break;
						case 'AZLogo':
							$('#headingText h2').html($.i18n.map.sky_AZ);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_AZ);
							$('.member_name ul li:eq(1) span').html('Alitalia - Compagnia Aerea Italiana');
							$('.mem_details ul li:eq(0) p:eq(1)').html("123");
							$('.mem_details ul li:eq(1) p:eq(1)').html("638");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_3_1_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("2009");
							$('.mem_details ul li:eq(4) p:eq(1)').html("49");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_23_2_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Fiumicino);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2009");
							$('.mem_details ul li:eq(8) p:eq(1)').html("102+(32)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_4_4_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("91");
							$('.mem_details ul li:eq(11) p:eq(1)').html("13,721");
							$('.mem_details ul li:eq(12) p:eq(1)').html("FCO, MXP");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_MilleMiglia);
							$('.mem_details ul li:eq(14) p:eq(1)').html("ALITALIA.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Gabriele_Del_Torchio);
							$('.mem_info ul li span').html($.i18n.map.sky_AZLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_AZ);
							break;
						case 'CILogo':
							$('#headingText h2').html($.i18n.map.sky_CI);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_CI);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_CI);
							$('.mem_details ul li:eq(0) p:eq(1)').html("95");
							$('.mem_details ul li:eq(1) p:eq(1)').html("238");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_4_6_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1959");
							$('.mem_details ul li:eq(4) p:eq(1)').html("29");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_14_1_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Taipei);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2011");
							$('.mem_details ul li:eq(8) p:eq(1)').html("80+(8)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_2_52_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("82");
							$('.mem_details ul li:eq(11) p:eq(1)').html("11,778");
							$('.mem_details ul li:eq(12) p:eq(1)').html("TPE, KHH");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Dynasty_Flyer);
							$('.mem_details ul li:eq(14) p:eq(1)').html("CHINA-AIRLINES.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Huang_Hsiang_Sun);
							$('.mem_info ul li span').html($.i18n.map.sky_CILogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_CI);
							break;
						case 'MULogo':
							$('#headingText h2').html($.i18n.map.sky_MU);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_MU);
							$('.member_name ul li:eq(1) span').html('China Eastern Airlines Corporation Limited');
							$('.mem_details ul li:eq(0) p:eq(1)').html("217");
							$('.mem_details ul li:eq(1) p:eq(1)').html("1,880");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_RMB_88_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1988");
							$('.mem_details ul li:eq(4) p:eq(1)').html("26");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_79_1_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Shanghai);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2011");
							$('.mem_details ul li:eq(8) p:eq(1)').html("238+(227)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_21_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("26");
							$('.mem_details ul li:eq(11) p:eq(1)').html("68,874");
							$('.mem_details ul li:eq(12) p:eq(1)').html("PVG, KMG, XIY");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Eastern_Miles);
							$('.mem_details ul li:eq(14) p:eq(1)').html("FLYCHINAEASTERN.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Xulun_Ma);
							$('.mem_info ul li span').html($.i18n.map.sky_MULogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_MU);
							break;
						case 'CZLogo':
							$('#headingText h2').html($.i18n.map.sky_CZ);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_CZ);
							$('.member_name ul li:eq(1) span').html('China Southern Airlines');
							$('.mem_details ul li:eq(0) p:eq(1)').html("190");
							$('.mem_details ul li:eq(1) p:eq(1)').html("1,930");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_RMB_98_5_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1987");
							$('.mem_details ul li:eq(4) p:eq(1)').html("40");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_86_5_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Guangzhou);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2007");
							$('.mem_details ul li:eq(8) p:eq(1)').html("579+(476)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_17_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("44");
							$('.mem_details ul li:eq(11) p:eq(1)').html("90,000");
							$('.mem_details ul li:eq(12) p:eq(1)').html("CAN, PEK, URC, CKG");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Sky_Pearl_Club);
							$('.mem_details ul li:eq(14) p:eq(1)').html("FLYCHINASOUTHERN.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Wan_geng_Tan);
							$('.mem_info ul li span').html($.i18n.map.sky_CZLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_CZ);
							break;
						case 'OKLogo':
							$('#headingText h2').html($.i18n.map.sky_OK);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_OK);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_OK);
							$('.mem_details ul li:eq(0) p:eq(1)').html("40");
							$('.mem_details ul li:eq(1) p:eq(1)').html("54");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_512_million);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1923");
							$('.mem_details ul li:eq(4) p:eq(1)').html("22");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_2_8_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Prague);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2001");
							$('.mem_details ul li:eq(8) p:eq(1)').html("24");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_0_6_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("30");
							$('.mem_details ul li:eq(11) p:eq(1)').html("925");
							$('.mem_details ul li:eq(12) p:eq(1)').html("PRG");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_OK_Plus);
							$('.mem_details ul li:eq(14) p:eq(1)').html("CZECHAIRLINES.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Philippe_M_Moreels);
							$('.mem_info ul li span').html($.i18n.map.sky_OKLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_OK);
							break;
						case 'DLLogo':
							$('#headingText h2').html($.i18n.map.sky_DL);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_DL);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_DL);
							$('.mem_details ul li:eq(0) p:eq(1)').html("342");
							$('.mem_details ul li:eq(1) p:eq(1)').html("5,152");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_37_7_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1928");
							$('.mem_details ul li:eq(4) p:eq(1)').html("69");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_165_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Atlanta);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2000");
							$('.mem_details ul li:eq(8) p:eq(1)').html("722+(512)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_90_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("100");
							$('.mem_details ul li:eq(11) p:eq(1)').html("80,000");
							$('.mem_details ul li:eq(12) p:eq(1)').html("ATL, CVG, DTW, LAX, MSP, JFK, LGA, SLC, SEA, AMS, CDG, NRT");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Delta_SkyMiles);
							$('.mem_details ul li:eq(14) p:eq(1)').html("DELTA.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Richard_Anderson);
							$('.mem_info ul li span').html($.i18n.map.sky_DLLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_DL);
							break;
						case 'KQLogo':
							$('#headingText h2').html($.i18n.map.sky_KQ);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_KQ);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_KQ);
							$('.mem_details ul li:eq(0) p:eq(1)').html("62");
							$('.mem_details ul li:eq(1) p:eq(1)').html("154");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_KES_104_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1977");
							$('.mem_details ul li:eq(4) p:eq(1)').html("48");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_3_7_milion);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Nairobi);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2007");
							$('.mem_details ul li:eq(8) p:eq(1)').html("43+(3)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_24_5_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("28");
							$('.mem_details ul li:eq(11) p:eq(1)').html("3,986");
							$('.mem_details ul li:eq(12) p:eq(1)').html("NBO");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Flying_Blue);
							$('.mem_details ul li:eq(14) p:eq(1)').html("KENYA-AIRWAYS.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Titus_Naikuni);
							$('.mem_info ul li span').html($.i18n.map.sky_KQLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_KQ);
							break;
						case 'KLLogo':
							$('#headingText h2').html($.i18n.map.sky_KL);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_KL);
							$('.member_name ul li:eq(1) span').html('KLM Royal Dutch Airlines');
							$('.mem_details ul li:eq(0) p:eq(1)').html("144");
							$('.mem_details ul li:eq(1) p:eq(1)').html("616");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_25_2_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1919");
							$('.mem_details ul li:eq(4) p:eq(1)').html("74");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_26_6_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Amstelveen);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2004");
							$('.mem_details ul li:eq(8) p:eq(1)').html("113+(88)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_24_5_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("153");
							$('.mem_details ul li:eq(11) p:eq(1)').html("32,505");
							$('.mem_details ul li:eq(12) p:eq(1)').html("AMS");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Flying_Blue);
							$('.mem_details ul li:eq(14) p:eq(1)').html("KLM.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_KLM_CEO);
							$('.mem_info ul li span').html($.i18n.map.sky_KLLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_KL);
							break;
						case 'KELogo':
							$('#headingText h2').html($.i18n.map.sky_KE);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_KE);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_KE);
							$('.mem_details ul li:eq(0) p:eq(1)').html("127");
							$('.mem_details ul li:eq(1) p:eq(1)').html("420");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_KRW_11_8_trillion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1969");
							$('.mem_details ul li:eq(4) p:eq(1)').html("45");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_23_4_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Seoul);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2000");
							$('.mem_details ul li:eq(8) p:eq(1)').html("155+(11)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_21_7_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("113");
							$('.mem_details ul li:eq(11) p:eq(1)').html("20,702");
							$('.mem_details ul li:eq(12) p:eq(1)').html("ICN, GMP, PUS, CJU");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_SKYPASS);
							$('.mem_details ul li:eq(14) p:eq(1)').html("KOREANAIR.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Yang_Ho_Cho);
							$('.mem_info ul li span').html($.i18n.map.sky_KELogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_KE);
							break;
						case 'MELogo':
							$('#headingText h2').html($.i18n.map.sky_ME);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_ME);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_ME);
							$('.mem_details ul li:eq(0) p:eq(1)').html("31");
							$('.mem_details ul li:eq(1) p:eq(1)').html("62");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_514_million);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1945");
							$('.mem_details ul li:eq(4) p:eq(1)').html("21");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_2_3_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Beirut);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2012");
							$('.mem_details ul li:eq(8) p:eq(1)').html("17");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_0_2_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("28");
							$('.mem_details ul li:eq(11) p:eq(1)').html("1,863");
							$('.mem_details ul li:eq(12) p:eq(1)').html("BEY");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Cedar_Miles);
							$('.mem_details ul li:eq(14) p:eq(1)').html("MEA.COM.LB");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Mohamad_A_El_Hout);
							$('.mem_info ul li span').html($.i18n.map.sky_MELogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_ME);
							break;
						case 'SVLogo':
							$('#headingText h2').html($.i18n.map.sky_SV);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_SV);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_SV);
							$('.mem_details ul li:eq(0) p:eq(1)').html("79");
							$('.mem_details ul li:eq(1) p:eq(1)').html("506");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_5_3_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1945");
							$('.mem_details ul li:eq(4) p:eq(1)').html("36");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_25_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Jeddah);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2012");
							$('.mem_details ul li:eq(8) p:eq(1)').html("117");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_3_1_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("51");
							$('.mem_details ul li:eq(11) p:eq(1)').html("14,363");
							$('.mem_details ul li:eq(12) p:eq(1)').html("JED, RUH, DMM");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Alfursan);
							$('.mem_details ul li:eq(14) p:eq(1)').html("SAUDIAIRLINES.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Alhazmi_Abdulaziz);
							$('.mem_info ul li span').html($.i18n.map.sky_SVLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_SV);
							break;
						case 'ROLogo':
							$('#headingText h2').html($.i18n.map.sky_RO);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_RO);
							$('.member_name ul li:eq(1) span').html('TAROM-Romanian Air Transport');
							$('.mem_details ul li:eq(0) p:eq(1)').html("40");
							$('.mem_details ul li:eq(1) p:eq(1)').html("100");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_348_million);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1954");
							$('.mem_details ul li:eq(4) p:eq(1)').html("24");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_2_2_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Henri_Coanda_International_Airport);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2010");
							$('.mem_details ul li:eq(8) p:eq(1)').html("24");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_24_5_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("43");
							$('.mem_details ul li:eq(11) p:eq(1)').html("2,005");
							$('.mem_details ul li:eq(12) p:eq(1)').html("OTP");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Flying_Blue);
							$('.mem_details ul li:eq(14) p:eq(1)').html("TAROM.RO");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Christian_Edouard_Heinzmann);
							$('.mem_info ul li span').html($.i18n.map.sky_ROLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_RO);
							break;
						case 'VNLogo':
							$('#headingText h2').html($.i18n.map.sky_VN);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_VN);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_VN);
							$('.mem_details ul li:eq(0) p:eq(1)').html("47");
							$('.mem_details ul li:eq(1) p:eq(1)').html("308");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_EUR_1_8_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1993");
							$('.mem_details ul li:eq(4) p:eq(1)').html("18");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_15_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Hanoi);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2010");
							$('.mem_details ul li:eq(8) p:eq(1)').html("82");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_0_7_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("45");
							$('.mem_details ul li:eq(11) p:eq(1)').html("10,755");
							$('.mem_details ul li:eq(12) p:eq(1)').html("HAN, SGN");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Golden_Lotus_Plus);
							$('.mem_details ul li:eq(14) p:eq(1)').html("VIETNAMAIRLINES.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Pham_Ngoc_Minh);
							$('.mem_info ul li span').html($.i18n.map.sky_VNLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_VN);
							break;
						case 'MFLogo':
							$('#headingText h2').html($.i18n.map.sky_MF);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_MF);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_MF);
							$('.mem_details ul li:eq(0) p:eq(1)').html("63");
							$('.mem_details ul li:eq(1) p:eq(1)').html("646");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_RMB_16_1_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1984");
							$('.mem_details ul li:eq(4) p:eq(1)').html("9");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_18_5_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Xiamen);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2012");
							$('.mem_details ul li:eq(8) p:eq(1)').html("104");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_4_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("34");
							$('.mem_details ul li:eq(11) p:eq(1)').html("12,000");
							$('.mem_details ul li:eq(12) p:eq(1)').html("XMN, FOC");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Egret_Club);
							$('.mem_details ul li:eq(14) p:eq(1)').html("XIAMENAIR.COM");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Che_Shanglun);
							$('.mem_info ul li span').html($.i18n.map.sky_MFLogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_MF);
							break;
						case 'GALogo':
							$('#headingText h2').html($.i18n.map.sky_GA);
							$('.member_name ul li:eq(1) p').html($.i18n.map.sky_GA);
							$('.member_name ul li:eq(1) span').html($.i18n.map.sky_GA);
							$('.mem_details ul li:eq(0) p:eq(1)').html("52");
							$('.mem_details ul li:eq(1) p:eq(1)').html("534");
							$('.mem_details ul li:eq(2) p:eq(1)').html($.i18n.map.sky_USD_2_9_billion);
							$('.mem_details ul li:eq(3) p:eq(1)').html("1949");
							$('.mem_details ul li:eq(4) p:eq(1)').html("12");
							$('.mem_details ul li:eq(5) p:eq(1)').html($.i18n.map.sky_25_million);
							$('.mem_details ul li:eq(6) p:eq(1)').html($.i18n.map.sky_Jakarta);
							$('.mem_details ul li:eq(7) p:eq(1)').html("2014");
							$('.mem_details ul li:eq(8) p:eq(1)').html("110+(30)");
							$('.mem_details ul li:eq(9) p:eq(1)').html($.i18n.map.sky_1_million);
							$('.mem_details ul li:eq(10) p:eq(1)').html("57");
							$('.mem_details ul li:eq(11) p:eq(1)').html("7,191");
							$('.mem_details ul li:eq(12) p:eq(1)').html("CGK, DPS, UPG, KNO, BPN, SUB");
							$('.mem_details ul li:eq(13) p:eq(1)').html($.i18n.map.sky_Garuda_Miles);
							$('.mem_details ul li:eq(14) p:eq(1)').html("garuda-indonesia.com");
							$('.mem_details ul li:eq(15) p:eq(1)').html($.i18n.map.sky_Emirsyah_Satar);
							$('.mem_info ul li span').html($.i18n.map.sky_GALogo);
							$('.mem_info ul li p:eq(0)').html($.i18n.map.sky_aboutt+" "+$.i18n.map.sky_GA);
							break;
						default :
							break;
						}
					}slideThePage(sessionStorage.navDirection);
				},
				
				events:
				{
					'click .white_button':'openMemberSite',
					'touchstart *': function() {
						$.pageslide.close();
					}
				},
				
				/* Function to open Skyteam Member airlines website from application  */
				openMemberSite:function(e){
					sessionStorage.memberDetailsURL=site;
					window.open(iSC.openmemberdetails, "_self");
				},
				/* End of function */
			});

			return skyTeamMemberInfoView;
		});
