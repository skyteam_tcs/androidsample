/* Home Page Functionality */
define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'pageslide', 'slider', 'text!template/homeTemplate.html', 'text!template/menuTemplate.html', 'text!template/searchAirportTemplate.html','appView','text!template/settingsLanguagePopup.html'], function($, _, Backbone, utilities, constants, pageslide, Swiper, homePageTemplate, menuPageTemplate,  airportSearch, appView,settingsLanguagePopup) {
       var list = {};
       var that;
       var savedData,langugeView;

       var languagePopup = Backbone.View.extend({
                            el : '#skyteam',
                            initialize : function() {
                            langugeView=this;
                            },
                            events:{
                            'click #operator-ok' : 'closeLanguagePopup',
                            'click .langText' : 'checkSelectedLanguage',
                            },
                            render:function(){
                            var template = _.template(settingsLanguagePopup,$.i18n.map);
                            $('#blackshade').show();
                            $(langugeView.el).append(template);
                            $('#members-popup').show();
                            $('#operator-ok').show();


                            if(localStorage.language != undefined || localStorage.language !== "undefined")
                            {
                            langugeView.$el.find("#lang-"+localStorage.language.toLowerCase()).prop('checked',true);
                            }
                            else {
                            langugeView.$el.find('#lang-en').prop('checked', true);
                            }

                                                langugeView.$el.find('#operator-ok').off('click').on('click',function(e){

                                                                                                    langugeView.closeLanguagePopup(e)
                                                                                                     });

                            },
                                                checkSelectedLanguage: function(e) {
                                                var forid = '#' + $(e.target).attr('for');
                                                $(forid).prop("checked", true);
                                                e.preventDefault();

                                                },
                                                closeLanguagePopup:function(e){
                                                                           e.preventDefault();
                                                e.stopImmediatePropagation();
                                                localStorage.previouslang=localStorage.language;
                                                if(langugeView.$el.find("input[name=operatorsGroup]:radio:checked").val() != "" &&  langugeView.$el.find("input[name=operatorsGroup]:radio:checked").val() != localStorage.language){

                                                localStorage.language=langugeView.$el.find("input[name=operatorsGroup]:radio:checked").val();

                                                langugeView.$el.find('#members-popup').remove();
                                                langugeView.$el.find('.members-ok').remove();


                                                var time =localStorage.time;
                                                var temparature=localStorage.temparature;
                                                var distance=localStorage.distance;
                                                var skytips=localStorage.skytips;
                                                var date=localStorage.date;
                                                var language=localStorage.language;

                                                /*If network is available,call the webservice*/

                                                sessionStorage.settingsURL = time+','+distance+','+temparature+','+date+','+skytips+','+language+','+'NO';
                                                setTimeout(function(){
                                                           window.open('settings:'+sessionStorage.settingsURL,"_self");
                                                           },500);
                                                $('#blackshade').hide();
//                                                if(localStorage.language=="En"){
//                                                $.i18n.properties({
//                                                                  name:'Messages',
//                                                                  path:'bundle/',
//                                                                  mode:'map',
//                                                                  language:localStorage.language,
//                                                                  callback: function(e) {
//
//                                                                  window.location.href='index.html';
//                                                                  window.open("analytics://_trackLanguage#"+localStorage.language, "_self");
//
//                                                                  }
//                                                                  });
//                                                }else{
                                                window.networkStatCallBack = function(){
                                                var netStatus =JSON.parse(localStorage.networkStatus);

                                                if(netStatus.network == "online")
                                                {
                                                showActivityIndicatorForAjax('true',"langURLcancel");

                                                window.getLangCallback = function() {
                                                 var strLanguage = localStorage.language;
                                                 if(localStorage.language === "zh-Hant")
                                                 {
                                                        strLanguage = "zh_TW";
                                                 }
                                                $.i18n.properties({
                                                                  name:'Messages',
                                                                  path:'bundle/',
                                                                  mode:'map',
                                                                  language:strLanguage,
                                                                  callback: function(e) {

                                                                  window.open("analytics://_trackLanguage#"+localStorage.language, "_self");

                                                                  }
                                                                  });

                                                setTimeout(function() {
                                                           sessionStorage.isMenuAdded = "NO";
                                                           hideActivityIndicator();
                                                           window.location.href='index.html';


                                                           }, 500);
                                                }

                                                setTimeout(function(){
                                                           window.open(iSC.getLang,"_self"); //calling native to store language cache in native
                                                           },1000);

                                                } else if(netStatus.network == "offline")
                                                {
                                                localStorage.language=localStorage.previouslang;
                                                sessionStorage.settingsURL = localStorage.time+','+localStorage.distance+','+localStorage.temparature+','+localStorage.date+','+localStorage.skytips+','+localStorage.language+','+'NO';
                                                setTimeout(function(){
                                                           window.open('settings:'+sessionStorage.settingsURL,"_self");
                                                           },500);
                                                openAlertWithOk($.i18n.map.sky_notconnected);

                                                }
                                                };
                                                window.open(iSC.Network, "_self");

//                                                }


                                                }else{
                                                langugeView.$el.find('#members-popup').remove();
                                                $('#blackshade').hide();
                                                }
                            }

                            });

       var homePageView = appView.extend({

                                               id : 'homePage',
                                               initialize : function() {
                                               $('#backButtonList').hide();

                                               var time =localStorage.time;
                                               var temparature=localStorage.temparature;
                                               var distance=localStorage.distance;
                                               var skytips=localStorage.skytips;
                                               var date=localStorage.date;
                                               var language=localStorage.language;

                                              if(localStorage.InitialLoad == 'YES' ){
                                               localStorage.InitialLoad = 'NO';
                                               sessionStorage.settingsURL = time+','+distance+','+temparature+','+date+','+skytips+','+language+','+'NO';

                                              setTimeout(function(){
                                                        window.open('settings:'+sessionStorage.settingsURL,"_self");
                                                         },1500);


                                               sessionStorage.isInitialLoad="YES";


                                               }
                                               else{
                                               sessionStorage.isInitialLoad="NO";
                                               }



                                               if(sessionStorage.previousPages != undefined)
                                               {
                                               if(JSON.parse(sessionStorage.previousPages).length == 0){
                                               sessionStorage.removeItem('previousPages');
                                               }
                                               }

                                               sessionStorage.currentPage = this.id;
                                               sessionStorage.From = "Menu";
                                               if (sessionStorage.homePage == "yes") {
                                               $('.main_header ul li:eq(0) img').remove();
                                               $('.main_header ul li:eq(0) a').remove();
                                               $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));
                                               }
                                               sessionStorage.homePage = "";

                                               if(sessionStorage.isMenuAdded == undefined || sessionStorage.isMenuAdded !== "YES"){
                                               if ($('.main_header ul li:eq(0) a').attr('class') == "menuItems") {
                                               $(".menuItems").pageslide();
                                               var template = _.template(menuPageTemplate,$.i18n.map);
                                               $('#pageslide').html(template);
                                                sessionStorage.isMenuAdded = "YES";
                                               }

                                               }
                                               if($('#pageslide').children().length === 0)
                                               {
                                               $(".menuItems").pageslide();
                                               var template = _.template(menuPageTemplate,$.i18n.map);
                                               $('#pageslide').html(template);
                                               sessionStorage.isMenuAdded = "YES";
                                               }
                                               $.pageslide.close();



                                               $('#headingText img').css('display', 'block');
                                               $('#headingText h2').css('display', 'none');

                                               $('#brief img').css('display', 'block');
                                               $('#brief h2').css('display', 'none');
                                         that =this;
                                               /* Navigate to Flight Finder Page */
                                               $('#flightSearch').click(function() {
                                                                        sessionStorage.isFromHome="";
                                                                        sessionStorage.navToAirportDetails="";
                                                                        if(sessionStorage.previousPages)
                                                                        {
                                                                        sessionStorage.removeItem('previousPages');
                                                                        }
                                                                        if(sessionStorage.searchData != undefined && sessionStorage.searchData != null){
                                                                        sessionStorage.removeItem('searchData');

                                                                        }
                                                                        sessionStorage.transRequired = "No";
                                                                        var locationPath = window.location;
                                                                        locationPath = locationPath.toString();
                                                                        var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                        if (spath == "searchFlights") {

                                                                        $(".menuItems").trigger('click');
                                                                        } else {
                                                                        if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
                                                                        window.getAirportsCallback = function() {
                                                                        }

                                                                        window.open(iSC.getAirports);
                                                                        }
                                                                        sessionStorage.sixDays="no";
                                                                        sessionStorage.sixDaysOnward="false";
                                                                        sessionStorage.sixDaysReturn ="false";
                                                                        window.location.href = 'index.html#searchFlights';

                                                                        sessionStorage.navToSearch = "yes";
                                                                        }
                                                                        });
                                               /* End of Navigate to Flight Finder Page */

                                               /* Navigate to Flight Status Page */
                                               $('#flightStatus').click(function() {
                                                                        if(hasValue(sessionStorage.searchairports)){
                                                                        sessionStorage.removeItem('searchairports');
                                                                        }
                                                                        if(hasValue(sessionStorage.statusFor)){
                                                                         sessionStorage.removeItem('statusFor');
                                                                        }
                                                                        if(sessionStorage.previousPages)
                                                                        {
                                                                        sessionStorage.removeItem('previousPages');
                                                                        }
                                                                        if (sessionStorage.flightNo != undefined && sessionStorage.flightNo != null) {
                                                                        sessionStorage.removeItem('flightNo');
                                                                        }
                                                                        if(sessionStorage.airline != undefined || sessionStorage.airline != null){
                                                                        sessionStorage.removeItem('airline');
                                                                        }
                                                                        sessionStorage.transRequired = "No";

                                                                        var locationPath = window.location;
                                                                        locationPath = locationPath.toString();
                                                                        var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                        if (spath == "flightstatus") {

                                                                        $(".menuItems").trigger('click');
                                                                        } else {
                                                                        if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
                                                                        window.getAirportsCallback = function() {
                                                                        }
                                                                        window.open(iSC.getAirports);
                                                                        }
                                                                        /* invalidating previously selected airline , flight No & dates*/ //oct10-583152
                                                                        sessionStorage.flightNo ='';
                                                                        sessionStorage.airline = '';
                                                                        sessionStorage.FS_departureDate ='';
                                                                        /* invalidating previously selected airline , flight No & dates*/

                                                                        window.location.href = 'index.html#flightstatus';
                                                                        sessionStorage.navToflightStatus = "yes";
                                                                        }
                                                                        });
                                               /* End of Navigate to Flight Status Page */

                                               /* Navigate to Airports Page */
                                               $('#airportsSearch').click(function() {
                                                                          if(sessionStorage.previousPages != undefined)
                                                                          {
                                                                          if(JSON.parse(sessionStorage.previousPages).length == 0){
                                                                          sessionStorage.removeItem('previousPages');
                                                                          }
                                                                          }
                                                                          sessionStorage.transRequired = "No";
                                                                          sessionStorage.isFromHome="";
                                                                          sessionStorage.navToAirportDetails="";
                                                                          var locationPath = window.location;
                                                                          locationPath = locationPath.toString();
                                                                          var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                          if (spath == "airportSearchPage") {
                                                                          $(".menuItems").trigger('click');

                                                                          } else {
                                                                            sessionStorage.isMenuAdded = "NO";
                                                                          /*$('#pageslide').css('z-index', '-1');*/
                                                                          sessionStorage.searchAirport = "";
                                                                           sessionStorage.fromHomeScreen="NO";
                                                                          sessionStorage.goNativeMapFromDetails="NO";
                                                                          window.location.href = 'index.html#airportSearchPage';
                                                                              $.pageslide.close();
                                                                          sessionStorage.navToAirportSearch = "yes";
                                                                          }
                                                                          });
                                               /* End of Navigate to Airports Page */

                                               /* Navigate to Home Page */
                                               $('#home').click(function() {
                                                                if(sessionStorage.previousPages)
                                                                {

                                                                sessionStorage.removeItem('previousPages');

                                                                }
                                                                sessionStorage.transRequired = "No";

                                                                var locationPath = window.location;
                                                                locationPath = locationPath.toString();
                                                                var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                              if (locationPath.indexOf('#') === -1 || spath == "homePage") {
                                                                $(".menuItems").trigger('click');
                                                                }
                                                                else{
                                                                window.location.href = 'index.html#homePage';
                                                                }                                                            });
                                               /* End of Navigate to Home Page */

                                               /* Navigate to Settings Page */
                                               $('#settingsPage').click(function() {
                                                                        if(sessionStorage.previousPages)
                                                                        {
                                                                        sessionStorage.removeItem('previousPages');
                                                                        }
                                                                        sessionStorage.transRequired = "No";

                                                                        var locationPath = window.location;
                                                                        locationPath = locationPath.toString();
                                                                        var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                        if (spath == "settings") {
                                                                        $(".menuItems").trigger('click');
                                                                        } else {
                                                                          $('#brief img').css('display', 'block');
                                                                        window.location.href = 'index.html#settings';
                                                                        sessionStorage.navToSettings = "yes";
                                                                        sessionStorage.goNativeMapFromDetails = "NO";
                                                                        }
                                                                        });
                                               /* End of Navigate to Settings Page */

                                               /* Navigate to SkyTips Page */
                                               $('#skyTips').click(function() {
                                                                   if(sessionStorage.previousPages)
                                                                   {
                                                                   sessionStorage.removeItem('previousPages');
                                                                   }
                                                                   sessionStorage.removeItem("skyTipsFullAirportName");
                                                                   sessionStorage.transRequired = "No";

                                                                   var locationPath = window.location;
                                                                   locationPath = locationPath.toString();
                                                                   var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                   if (spath == "skyTipsFinder") {
                                                                   $(".menuItems").trigger('click');
                                                                   } else {

                                                                     $('#brief img').css('display', 'block');
                                                                   if (localStorage.cityList_Skytips == 'undefined' || localStorage.cityList_Skytips == null || localStorage.cityList_Skytips == undefined) {

                                                                   window.open(iSC.skyTipsCityList);
                                                                   }

                                                              else {
                                                                   var temp=JSON.parse(localStorage.cityList_Skytips);
                                                                   if(temp.AirPorts_test[0].hasOwnProperty("Airport_Name_Es"))
                                                                   {
                                                                    window.location.href = 'index.html#skyTipsFinder';

                                                                   }
                                                                   else{
                                                                   localStorage.removeItem("cityList_Skytips");
                                                                   window.open(iSC.skyTipsCityList);
                                                                   }
                                                                   }
                                                                   sessionStorage.navToskyTipsFinder = "yes";
                                                                   }
                                                                   });
                                               /* End of Navigate to SkyTips Page */
                                               /* Navigate to SkyPriorityFinder Page */
                                               $('#skyPriority').click(function() {
                                                   sessionStorage.isFromSaved = "no";
                                                   if(sessionStorage.previousPages)
                                                   {
                                                   sessionStorage.removeItem('previousPages');
                                                   }
                                                   sessionStorage.transRequired = "No";

                                                   var locationPath = window.location;
                                                   locationPath = locationPath.toString();
                                                   var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                  if (spath == "skyPriorityFinder") {
                                                   $(".menuItems").trigger('click');
                                                   } else {
                                                   /* invalidating previously selected skypriority airport & airlines */
                                                   sessionStorage.airport_code  ='';
                                                   sessionStorage.skyPriorityAirlines ='';
                                                    $('#brief img').css('display', 'block');
                                                   window.location.href = 'index.html#skyPriorityFinder';


                                                   }
                                                });
                                               /* End of Navigate to SkyPriorityFinder Page */


                                               /* Navigate to About SkyTeam Page */
                                               $('#aboutSkyteam').click(function() {
                                                                        if(sessionStorage.previousPages)
                                                                        {
                                                                        sessionStorage.removeItem('previousPages');
                                                                        }
                                                                        sessionStorage.transRequired = "No";

                                                                        var locationPath = window.location;
                                                                        locationPath = locationPath.toString();
                                                                        var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                        if(sessionStorage.members !== undefined && sessionStorage.members ==="AirportDetails")
                                                                        {
                                                                                	sessionStorage.members = "menu";
                                                                        window.location.href = 'index.html#memberAirlines';
                                                                        sessionStorage.navToaboutSky = "yes";

                                                                        }
                                                                        else if (spath == "aboutSkyTeam" ) {
                                                                        	sessionStorage.members = "menu";
                                                                        $(".menuItems").trigger('click');
                                                                        } else {
                                                                        	sessionStorage.members = "menu";
                                                                          $('#brief img').css('display', 'block');
                                                                        window.location.href = 'index.html#aboutSkyTeam';
                                                                        sessionStorage.navToaboutSky = "yes";
                                                                        }
                                                                        });
                                               /* End of Navigate to About SkyTeam Page */

                                               /* Navigate to Lounge Finder Page */
                                               $('#loungeFinderMenu').click(function() {
                                                    sessionStorage.airportName ='';
                                                    sessionStorage.airport_code ='';
                                                    if(sessionStorage.previousPages)
                                                    {
                                                    sessionStorage.removeItem('previousPages');
                                                    }
                                                    sessionStorage.transRequired = "No";

                                                    sessionStorage.lounge = "menu";
                                                    var locationPath = window.location;
                                                    locationPath = locationPath.toString();
                                                    var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                    if (spath == "loungesSearch") {
                                                    $(".menuItems").trigger('click');
                                                    } else {
                                                       $('#brief img').css('display', 'block');
                                                       if (localStorage.getItem('loungesAirportList') === null || localStorage.loungesAirportList == 'undefined' || localStorage.loungesAirportList == null) {//checking for existence of lounge airport list
                                                       showActivityIndicator($.i18n.map.sky_please);
                                                       window.open(iSC.loungeList, "_self");
                                                       }
                                                       else {// checking for valid lounge airport list
                                                       var temp =JSON.parse(localStorage.loungesAirportList);
                                                       if(_.isArray(temp.AirportsList_Lounges)){
                                                       window.location.href = 'index.html#loungesSearch';
                                                       }
                                                       else{
                                                       localStorage.removeItem("loungesAirportList");
                                                       showActivityIndicator($.i18n.map.sky_please);
                                                       window.open(iSC.loungeList, "_self");
                                                       }
                                                       }
                                                       sessionStorage.navToloungesFinderMenuView = "yes";
                                                     }
                                                    });
                                               /* End of Navigate to Lounge Finder Page */

                                               /* Navigate to yogaVideo Page */
                                               $('#yogaVideo').click(function() {
                                                                     if(sessionStorage.previousPages != undefined)
                                                                     {
                                                                     if(JSON.parse(sessionStorage.previousPages).length == 0){
                                                                     sessionStorage.removeItem('previousPages');
                                                                     }
                                                                     }
                                                                     sessionStorage.isFromHome="";
                                                                     sessionStorage.navToAirportDetails="";
                                                                     var locationPath = window.location;
                                                                     locationPath = locationPath.toString();
                                                                     var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                                     if (spath == "yogaVideoPage") {
                                                                     $(".menuItems").trigger('click');

                                                                     } else {
                                                                     /*$('#pageslide').css('z-index', '-1');*/
                                                                     sessionStorage.searchAirport = "";
                                                                     sessionStorage.fromHomeScreen="NO";
                                                                     showActivityIndicator($.i18n.map.sky_please);
                                                                     $('#brief img').css('display', 'block');
                                                                     window.location.href = 'index.html#yogaVideoPage';
                                                                     $.pageslide.close();
                                                                     }
                                                                     });
                                               /* End of Navigate to yogaVideo Page */
                                               },

                                               render : function() {
                                               $('#main_content').css('min-width',$(window).width());
                                               sessionStorage.home="no";
                                               var locationPath = window.location;
                                               locationPath = locationPath.toString();
                                               var compiledTemplate = _.template(homePageTemplate,$.i18n.map);
                                               $(this.el).html(compiledTemplate);

                                               if(sessionStorage.isInitialLoad==="YES"){
                                               sessionStorage.isInitialLoad="NO";


                                               //$("#members-popup").trigger('click');
                                         that.openLanguagePopup();
                                               }
                                               hideActivityIndicator();
                                               //slideThePage(sessionStorage.navDirection);
                                              loadCustomCss();
                                              return appView.prototype.render.apply(this, arguments);

                                               },
                                               events : {

                                               'click .main_buttons button:eq(0)' : 'hideAndShow',
                                               'click .main_buttons button:eq(1)' : 'searchFlights',
                                               'click .main_buttons button:eq(2)' : 'flightStatus',
                                               'click .main_buttons button:eq(3)' : 'openSearchAirport',
                                               'click #savedFlights' : 'openFlightDetails',
                                               'click #members-popup' : 'openLanguagePopup',
                                               'click #rightArrow' : 'openFlightDetails',

                                               'touchstart *' : function() {
                                               $.pageslide.close();
                                               }
                                               },
                                               /* Navigate to Flight Finder Page when user click on Flight Finder Button in home page */
                                               searchFlights : function(e) {



                                               if(sessionStorage.searchData != undefined && sessionStorage.searchData != null){
                                                sessionStorage.removeItem('searchData');

                                                }
                                                if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
                                                showActivityIndicator($.i18n.map.sky_please);
                                                window.getAirportsCallback = function() {
                                                }
                                                window.open(iSC.getAirports);
                                                }
                                               if(sessionStorage.previousPages != undefined)
                                               {
                                               if(JSON.parse(sessionStorage.previousPages).length != 0){
                                               sessionStorage.removeItem('previousPages');
                                               sessionStorage.removeItem('previousPage');
                                               }
                                               }
                                               var pageObj = {};
                                               pageObj.previousPage = "homePage";
                                               pushPageOnToBackStack(pageObj);
                                                sessionStorage.isFromHome="yes";
                                               window.location.href = 'index.html#searchFlights';

                                               },


                                               /* Navigate to Flight Status Page */
                                               flightStatus:function(e) {
                                            	   sessionStorage.isFromHome="yes";
                                               if(sessionStorage.previousPages != undefined)
                                               {
                                               if(JSON.parse(sessionStorage.previousPages).length != 0){
                                               sessionStorage.removeItem('previousPages');
                                               sessionStorage.removeItem('previousPage');
                                               }
                                               }
                                               var pageObj = {};
                                               pageObj.previousPage = "homePage";
                                               pushPageOnToBackStack(pageObj);
                                             /* invalidating previously selected airline , flight No & dates*/
                                             sessionStorage.flightNo ='';
                                             sessionStorage.airline = '';
                                             sessionStorage.FS_departureDate ='';
                                             if(hasValue(sessionStorage.searchairports)){
                                             sessionStorage.removeItem('searchairports');
                                             }
                                             if(hasValue(sessionStorage.statusFor)){
                                             sessionStorage.removeItem('statusFor');
                                             }
                                             /* invalidating previously selected airline , flight No & dates*/
                                                                        window.location.href = 'index.html#flightstatus';
                                                                        },
                                               /* End of Navigate to Flight Status Page */


                                               /* Navigate to Flight Details Page when user click on saved flight */
                                               openFlightDetails : function(e) {

                                               sessionStorage.saved = "no";
                                               sessionStorage.currentPage = this.id;
                                               sessionStorage.FromhomePage = "yes";
                                               var pageObj={};
                                               pageObj.previousPage="homePage";
                                               pageObj.isFrom="homePage";
                                               pushPageOnToBackStack(pageObj);
                                               window.networkStatCallBack = function(){};

                                               window.open(iSC.Network,"_self");
                                               sessionStorage.isFromSaved = "yes";
                                               sessionStorage.home="yes";
                                               sessionStorage.savedFlightDetails = JSON.stringify(savedData.SavedFlights_test[0].Details);
                                               sessionStorage.flightDetailsObject=savedData.SavedFlights_test[0].Details;
                                               if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
                                               window.getAirportsCallback = function() {
                                               window.location.href = "index.html#FlightDetailsPage";
                                               }
                                               window.open(iSC.getAirports);
				showActivityIndicator($.i18n.map.sky_please);
                                               } else {
                                               window.location.href = "index.html#FlightDetailsPage";
                                               }
                                               },
                                               /* End of Navigate to Flight Details Page when user click on saved flight */

                                               /* Navigate to Flight Finder Page when user click on Flight Finder Button in home page */
                                               hideAndShow : function(e) {

                                               if($('#back1').hasClass("down_width")){
                                               $('#flightshide').slideUp();
                                               $('#back1').css('-webkit-transform','rotate(0deg)','margin-top','3%');
                                               $('#back1').removeClass("down_width");
                                               //$('#flightshide').slideDown();//.addClass('positionAbsolute');
                                               }
                                               else{
                                               $('#back1').addClass("down_width");
                                               //rotating the arrow down
                                               $('#back1').css('-webkit-transform','rotate(90deg)','margin-top','0%');
                                               //$('#flightshide').css('position','absolute');
                                               $('#flightshide').slideDown().show();


                                               }


                                               },
                                               /* End of Navigate to Flight Finder Page when user click on Flight Finder Button in home page */

                                               /* Navigate to My SkyTeam Page */
                                               openSavedFlightsOrAirports : function(e) {
                                               window.location.href = 'index.html#savedFlightsOrAirports';
                                               },
                                               /* End of Navigate to My SkyTeam Page */

                                               /* Navigate to Airport Finder Page when user click on Airport Finder Button in home page */
                                               openSearchAirport : function(e) {

                                               sessionStorage.searchAirport = "";
                                               sessionStorage.isFromHome="";
                                               sessionStorage.navToAirportDetails="";
                                               sessionStorage.fromHomeScreen="YES";
                                                 sessionStorage.isMenuAdded = "NO";
                                               window.location.href = 'index.html#airportSearchPage';
                                               },
                                               /* End of Navigate to Airport Finder Page when user click on Airport Finder Button in home page */
                                               /* Navigate to Airport Finder Page when user click on Airport Finder Button in home page */
                                               openLanguagePopup : function(e) {
                                                 var view =new languagePopup();
                                                 view.render();

                                               },

                                               /* Form the saved flights section */
                                               renderSavedFlights :function(res) {

                                                 this.undelegateEvents();
                                               if(localStorage.skytips == "on"){
                                              /*santosh*/ that.$el.find('.main_buttons').css('height', '30%');
                                               that.$el.find('#savedFlightsMainContainer').css('display','block');
                                               }
                                               else if(localStorage.skytips == "off"){
                                               that.$el.find('.main_buttons').css('height', '50%');
                                               that.$el.find('#savedFlightsMainContainer').css('display','none');
                                               }
                                               that.$el.find('#savedFlightsMainContainer').append($('<div>').attr('id', 'savedFlights').addClass('swiper-wrapper'));

                                               savedData = JSON.parse(res);
                                               /* Display saved flight in Home Page */
                                               if (savedData.SavedFlights_test != "ERROR") {
                                               sessionStorage.home = "yes";
                                               var dep_date;
                                               var arr_date;
                                               var dep_time;
                                               var arr_time;
                                               for (var i = 0,maxlen=savedData.SavedFlights_test.length; i < maxlen; i++) {


                                               /* Change date format to DD MMM YY */
                                               dep_date =getDateInLocaleFormat( savedData.SavedFlights_test[i].Departure_Date,true);
                                               arr_date = getDateInLocaleFormat(savedData.SavedFlights_test[i].Arrival_Date,true);
                                               /* End of Change date format to DD MMM YY */

                                               /* Change time format to 12 hour */
                                               if (localStorage.time == "12h") {
                                               dep_time = timeConvertor(savedData.SavedFlights_test[i].Departure_Time);
                                               arr_time = timeConvertor(savedData.SavedFlights_test[i].Arrival_Time);
                                               }
                                               /* End of Change time format to 12 hour */

                                               /* Change time format to 24 hour */
                                               else {
                                               dep_time = savedData.SavedFlights_test[i].Departure_Time;
                                               arr_time = savedData.SavedFlights_test[i].Arrival_Time;
                                               }
                                         var detailObj= JSON.parse(savedData.SavedFlights_test[i].Details);
						var totalstoppage;
                                         
                                         if(detailObj.details.flights)
                                         {
                                         var flightcount = detailObj.details.flights.length;
                                         var normalstops = parseInt(savedData.SavedFlights_test[i].No_of_Stops);
                                         totalstoppage =normalstops;
                                         for(iter = 0; iter<flightcount; iter++)
                                         {
                                         totalstoppage = totalstoppage + detailObj.details.flights[iter].totalTechnicalStops;
                                         }
                                         }else{
                                         totalstoppage = savedData.SavedFlights_test[i].No_of_Stops;
                                         }

                                         


                                               /* End of Change time format to 24 hour */
                                               sessionStorage.removeItem('isFromSaved');

                                               //that.$el.find('#savedFlights').append($('<div></div>').attr('class', 'swiper-slide').append($('<div></div>').attr('class', 'savedflightsresults1 data'+i).append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header3').html()))).append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header6').html(savedData.SavedFlights_test[i].Departure))).append($('<li></li>').append($('<img></img>').attr('src', './images/stops-' + totalstoppage + '-icon.png')).append($('<p></p>').attr('class', 'header3').html(((savedData.SavedFlights_test[i].Flight_Status != "No Flight Status Found") || (savedData.SavedFlights_test[i].Flight_Status != "Network Error") || (savedData.SavedFlights_test[i].Flight_Status != "NS"))? getStatus(savedData.SavedFlights_test[i].Flight_Status) : ""))).append($('<li></li>').append($('<p></p>').attr('class', 'header6').html(savedData.SavedFlights_test[i].Arrival)))).append($('<ul></ul').append($('<li></li>').append($('<p></p>').html(savedData.SavedFlights_test[i].Departure_City_Name))).append($('<li></li>').append($('<p></p>').html(savedData.SavedFlights_test[i].Arrival_City_Name)))).append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(dep_date))).append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(dep_time))).append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(arr_time))).append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(arr_date)))).append($('<ul></ul>').append($('<li></li>').append($('<span></span>').attr('class', 'header3')).append($('<span></span>').attr('class', 'header3').html(savedData.SavedFlights_test[i].Flight_Number+' - '+getAirlineName(savedData.SavedFlights_test[i].Flight_Number.substr(0,2))))))).append($('<div></div>').attr('class', 'arrow').append($('<img></img>').attr('id', 'rightArrow').attr('src', './images/right_arrow.png').css('display','none'))));
                                               that.$el.find('#savedFlights').append($('<div></div>').attr('class', 'swiper-slide').append($('<div></div>').attr('class', 'savedflightsresults1 data'+i).append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header3').html()))).append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header6').html(savedData.SavedFlights_test[i].Departure))).append($('<li></li>').append($('<img></img>').attr('src', './images/stops-' + totalstoppage + '-icon.png')).append($('<p id="statusSection"></p>').attr('class', 'header3').html(((savedData.SavedFlights_test[i].Flight_Status != "No Status") || (savedData.SavedFlights_test[i].Flight_Status != "Network Error"))? "" : ""))).append($('<li></li>').append($('<p></p>').attr('class', 'header6').html(savedData.SavedFlights_test[i].Arrival)))).append($('<ul></ul').append($('<li></li>').append($('<p></p>').html(savedData.SavedFlights_test[i].Departure_City_Name))).append($('<li></li>').append($('<p></p>').html(savedData.SavedFlights_test[i].Arrival_City_Name)))).append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(dep_date))).append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(dep_time))).append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(arr_time))).append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(arr_date)))).append($('<ul></ul>').append($('<li></li>').append($('<span></span>').attr('class', 'header3')).append($('<span></span>').attr('class', 'header3').html(savedData.SavedFlights_test[i].Flight_Number+' - '+getAirlineName(savedData.SavedFlights_test[i].Flight_Number.substr(0,2))))))).append($('<div></div>').attr('class', 'arrow').append($('<img></img>').attr('id', 'rightArrow').attr('src', './images/right_arrow.png').css('display','none'))));
//                                               if((savedData.SavedFlights_test[i].Flight_Status == "DY") || (savedData.SavedFlights_test[i].Flight_Status == "CX")){
//
////                                               if((savedData.SavedFlights_test[0].Status_Object[0].CurrentStatusCode == "DY") || (savedData.SavedFlights_test[0].Status_Object[0].CurrentStatusCode == "CX")){
//                                               that.$el.find('.data'+i+' ul:eq(1) li:eq(1) p').css('color','#D7862F');
//                                               }
//                                               else {
//                                               that.$el.find('.data'+i+' ul:eq(1) li:eq(1) p').css('color','#0B1761');
//                                               }
//
//                                               }
//
//                                               if(that.$el.find('#savedFlights div div ul:eq(1) li:eq(1) p').html() != ""){
                                               that.$el.find('#savedFlights div div ul:eq(1) li:eq(1) p').css('display','block');
                                               that.$el.find('#savedFlights div div ul:eq(1) li:eq(1) img').css('display','none');
//                                               }
//                                               else{
//                                               that.$el.find('#savedFlights div div ul:eq(1) li:eq(1) p').css('display','none');
//                                               that.$el.find('#savedFlights div div ul:eq(1) li:eq(1) img').css('display','block');
//                                               }
}
                                               } else {
                                               that.$el.find('.savedFlights-container').css('display', 'none');
                                               }
                                               this.delegateEvents();
                                               /* End of Form the saved flights section */
                                               }


                                               });
       return homePageView;

       });
/* End of Home Page Functionality */