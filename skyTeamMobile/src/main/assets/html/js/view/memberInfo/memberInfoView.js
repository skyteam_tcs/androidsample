define(['jquery', 'backbone', 'underscore', 'text!template/member_info.html', 'text!template/menuTemplate.html'], 
function($, Backbone, _,memberInfoTemplate, menuPageTemplate) {
	var memberInfoView = Backbone.View.extend({
		el : '#skyteam',
		id : 'memberInfo-page',

		initialize : function() {
			//that=this;
			$('.main_header ul li:eq(0) a').remove();
				$('.main_header ul li:eq(0)').append($('<img></img>').attr('src','./images/back.png'));
				$('.main_header ul li:eq(0) img').off('click').click(function(){
                                                     sessionStorage.navDirection = "Left";

                                                        var currentobj=JSON.parse(sessionStorage.previousPages);
                                                        showActivityIndicator($.i18n.map.sky_please);
                                                        window.location.href = "index.html#"+currentobj[0].previousPage;
                                                        popPageOnToBackStack();
                                                        
                                                       
			$('.main_header ul li:eq(0) img').remove();		
				$('.main_header ul li:eq(0)').append($('<a></a>').attr('class','menuItems').attr('href','#nav').append($('<img></img>').attr('src','./images/menu.png')));
	
			});
			$('#headingText img').hide();
			$('#headingText h2').show();
			$('#headingText h2').html($.i18n.map.sky_member_info);
			
		
		},
		render : function() {

			var locationPath = window.location;
			locationPath = locationPath.toString();
           
			var compiledTemplate = _.template(memberInfoTemplate,$.i18n.map);
			//this.$el.append(compiledTemplate);
            $(this.el).html(compiledTemplate);
	
	slideThePage(sessionStorage.navDirection);
		},
		events : {
		'touchstart *' : function() {
				$.pageslide.close();
			}
			
		}	
});
return memberInfoView;
});
