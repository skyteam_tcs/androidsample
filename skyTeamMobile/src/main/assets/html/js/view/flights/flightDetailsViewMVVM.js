define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui','models/flights/flightDetailsModel', 'text!template/flightDetailsTemplateMVVM.html','appView', 'text!template/flightDetailsTemplateMVVM-old.html'], function($, _, Backbone, utilities, constants, jqueryUi, flightDetailsModel, flightDetailsTemplate,appView,flightDetailsTemplateold) {
           var self;
           var flightStatus;
           var flightDetailsPageView = appView.extend({
               id : 'flight-details',
               isToHideSaveButton:false,
               initialize : function() {
               /*back button*/
               $('#backButtonList').show();

               $('#headingText img').hide();
               $('#headingText h2').show();

              $('#headingText h2').html('<p class="flightDetails-FD">'+$.i18n.map.sky_flight_details+'</p>');
                var hTxt='<div class="newheaderrow"><div class="newdepname-FD">';
                             hTxt+='<div id="mbl-dep-code" class="mbl-dep-code-FD">'+sessionStorage.source+'</div></div><div class="headerflighticon-FD">';
                             hTxt+='<div class="icon-flight head" id="mblfr-FD">';
                             hTxt+='</div></div><div class="newarrname-FD"><div id="mbl-ret-code" class="mbl-ret-code-FD">'+sessionStorage.destination+'</div></div></div>';
                            $('#headingText h2').append(hTxt);



               $('#brief h2').hide();

               if (sessionStorage.savedflights == "yes" || sessionStorage.isFromSaved == "yes") {
               $('#brief img').show();
               sessionStorage.savedflights = "no";
               sessionStorage.isFromSaved = "no";
               }
               var currentPage=JSON.parse(sessionStorage.previousPages);
               if(currentPage[0].previousPage == "savedFlightsOrAirports"){
               $('#brief img').hide();
               }
               self = this;
               },
               render : function() {

               /* Back button functionality */
               $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                     e.preventDefault();
                     e.stopImmediatePropagation();
                     var currentobj=JSON.parse(sessionStorage.previousPages);
                     if(currentobj[0].previousPage == "savedFlightsOrAirports"){
                     localStorage.removeItem('savedFlightsAirports');
                     if(localStorage.savedFlightsAirports == undefined || localStorage.savedFlightsAirports == null)
                     {
                     window.open(iSC.savedAirports, "_self");
                     popPageOnToBackStack();
                     $(self.el).undelegate();
                     }
                     }
                    else if (currentobj[0].isFrom == "homePage"){
                      sessionStorage.isMenuAdded = "NO";
                      window.location.href = "index.html#"+currentobj[0].previousPage;
                      popPageOnToBackStack();
                      $(self.el).undelegate();
                     }
                     else if (sessionStorage.currentPage == "searchResults"){
                       sessionStorage.savedflights = "yes";
                       window.location.href = "index.html#"+currentobj[0].previousPage;
                       popPageOnToBackStack();
                       $(self.el).undelegate();

                     }
                     else if (currentobj[0].previousPage == "searchResults"){
                      sessionStorage.showList = currentobj[0].fromList;
                      window.location.href = "index.html#"+currentobj[0].previousPage;
                      popPageOnToBackStack();
                      $(self.el).undelegate();
                     }

                 });

               /* end of back functinality */

               showActivityIndicator($.i18n.map.sky_please);
               if (sessionStorage.currentPage == "searchResults" || sessionStorage.currentPage == "skyPriorityAdvancedDetails") {
               res = JSON.parse(sessionStorage.flightDetailsObject);
               }
               else if (sessionStorage.currentPage == "savedflights-page") {

                   if(sessionStorage.previousPage != "homePage"){
                    sessionStorage.previousPage = "savedflights-page";
                   }
                  res = JSON.parse(sessionStorage.savedFlightDetails);
                  if(typeof(res) === "string"){
                    res = JSON.parse(res);
                  }
                  if( "yes" === sessionStorage.saved ){
                  var statusObjArray =[];
                  sessionStorage.saved ="no";
                  var status = JSON.parse(localStorage.savedFlightStatus);
                  if(!_.isArray(status.Flight_Detail_Status)){
                  status.Flight_Detail_Status = $.makeArray(status.Flight_Detail_Status);
                  }
                  for(var i=0,len=status.Flight_Detail_Status.length;i<len;i++){
                  var flightStatus = status.Flight_Detail_Status[i]
                  var statusObj;
                  if(flightStatus== "Delayed"){
                  var delay = status.Status_Object[i].DelayedBy;
                  statusObj ={'Status':flightStatus,'delay':delay};
                  }
                  else{
                  statusObj ={'Status':flightStatus};
                  }
                  statusObjArray.push(statusObj);

                  }
                   res["CurrentDetail"] = statusObjArray;
                  }
                    sessionStorage.savedFlightDetails = JSON.stringify(res);
                    this.isToHideSaveButton = true;
               }
               else if (sessionStorage.currentPage == "homePage") {
                   sessionStorage.isFrom = "homePage";
                   sessionStorage.previousPage = "homePage";
                   res = JSON.parse(sessionStorage.savedFlightDetails);
                      if(typeof(res) === "string"){
                      res = JSON.parse(res);
                      }
                      if(!_.isArray(res.details.Segments)){
                        res.details.Segments =$.makeArray(res.details.Segments);
                      }
                      var statusObjArray =[];
                      for(var i=0,len=res.details.Segments.length;i<len;i++){
                        if(res.details.Segments[i].FlightStatus){
                              var flightStatus = res.details.Segments[i].FlightStatus.CurrentStatus;
                              var statusObj;
                          if(flightStatus== "Delayed"){
                          var delay = res.details.Segments[i].FlightStatus.DelayedBy;
                          statusObj ={'Status':flightStatus,'delay':delay};
                          }
                          else{
                          statusObj ={'Status':flightStatus};
                          }
                          statusObjArray.push(statusObj);
                        }
                          else{
                           break;
                          }
                      }
                    res["CurrentDetail"] = statusObjArray;
                    sessionStorage.savedFlightDetails = JSON.stringify(res);
                   this.isToHideSaveButton = true;
               }

               var dataModel = new flightDetailsModel(res);
               dataModel.resetData();
               flightStatus = dataModel.get('CurrentDetail');
               dataModel.set($.i18n.map);
               this.model = dataModel;
                                                      
                                                      if (this.model.attributes.details.flights){
                                                         var compiledTemplate = _.template(flightDetailsTemplate,this.model.toJSON());
                                                       }else{

                                                          var compiledTemplate = _.template(flightDetailsTemplateold,this.model.toJSON());
                                                      }
                                                      
               sessionStorage.previousPage = sessionStorage.currentPage;
               sessionStorage.currentPage = "FlightDetailsPage";
               sessionStorage.saved = "no";

                $(this.el).html(compiledTemplate);
                if(this.isToHideSaveButton){
                this.$el.find('.saveflight_btn').hide();//hidding the save button
                this.$el.find('.saveflight_btn_book').hide();
                $('#brief img').hide();//hiding the brief image
                this.isToHideSaveButton = false;//resetting the flag
                }
                setTimeout(function(){
                           lineAdjustment();
                           hideActivityIndicator();
                           },500);
               //slideThePage(sessionStorage.navDirection);
               //return this;
               return appView.prototype.render.apply(this, arguments);
               },

               events : {
               'click #skypriority':'skyPriority',
               'click .openairport' : 'navigateToAirport',
               'click #saveflightDetails' : 'saveFlightDetails',
               'click .booking_class' : 'booking_function',
               'touchstart *' : function() {
               $.pageslide.close();
               }

               },

               /* function to skyPriority page details */
                                                      skyPriority : function(e) {
                                                      showActivityIndicatorForAjax('false');
                                                      window.networkStatCallBack = function(){
                                                      var netStatus =JSON.parse(localStorage.networkStatus);
                                                      /*If network is available,call the webservice*/
                                                      if("online" === netStatus.network){
                                                      var flightDetails,spDetails;
                                                      flightDetails=JSON.parse(sessionStorage.flightDetailsObject);
                                              if( typeof flightDetails.details.flights  == 'undefined') {
                 if($.type(flightDetails.details.Segments) ==="array" ){
                                         var connectionAirports=[];
                                         $.each(flightDetails.details.Segments,function(index,object){
                                                if(index>0){
                                                connectionAirports.push({transferairport:object.Origin,operatingcarrier:object.OperatorCode});
                                                if(flightDetails.details.Segments.length === index+1 ){
                                                spDetails.tripdestination=object.Destination;
                                                spDetails.transferdetails=connectionAirports;
                                                }
                                                }
                                                else{
                                                spDetails={triporigin:object.Origin,operatingcarrier:object.OperatorCode,transferdetails:{},tripdestination:object.Destination};
                                                }
                                                });
                                         }
                                         else{
                                         var flightNo,flightSource,flightDestination,operatingAirline;
                                         flightNo=flightDetails.details.Segments.FlightNumber;
                                         flightSource=flightDetails.details.Segments.Origin
                                         flightDestination=flightDetails.details.Segments.Destination
                                         operatingAirline=flightDetails.details.Segments.OperatorCode;
                                         spDetails={triporigin:flightSource,operatingcarrier:operatingAirline,transferdetails:[],tripdestination:flightDestination};
                                         }
                                              }





                                   if( typeof flightDetails.details.flights  != 'undefined') {

  if($.type(flightDetails.details.flights) ==="array" ){
                                                      var connectionAirports=[];
                                                      $.each(flightDetails.details.flights,function(index,object){
                                                             if(index>0){
                                                             connectionAirports.push({transferairport:object.origin.airportCode,operatingcarrier:object.operatingAirlineCode});
                                                             if(flightDetails.details.flights.length === index+1 ){
                                                             spDetails.tripdestination=object.destination.airportCode;
                                                             spDetails.transferdetails=connectionAirports;
                                                             }
                                                             }
                                                             else{
                                                             spDetails={triporigin:object.origin.airportCode,operatingcarrier:object.operatingAirlineCode,transferdetails:{},tripdestination:object.destination.airportCode};
                                                             }
                                                             });
                                                      }
                                                      else{
                                                      var flightNo,flightSource,flightDestination,operatingAirline;


if( typeof flightDetails.details.flights  == 'undefined')
{
flightNo=flightDetails.details.Segments.FlightNumber;
}else{
 flightNo=flightDetails.details.flights.marketingFlightNumber;
}

if(typeof flightDetails.details.flights  == 'undefined') {
  flightSource=flightDetails.details.Segments.Origin;
}else{
  flightSource=flightDetails.details.flights.origin.airportCode;
}


if(typeof flightDetails.details.flights  == 'undefined') {
flightDestination=flightDetails.details.Segments.Destination;
}else{
   flightDestination=flightDetails.details.flights.destination.airportCode;
}


if(typeof flightDetails.details.flights  == 'undefined') {
 operatingAirline=flightDetails.details.Segments.OperatorCode;
}else{
  operatingAirline=flightDetails.details.flights.operatingAirlineCode;
}

 spDetails={triporigin:flightSource,operatingcarrier:operatingAirline,transferdetails:[],tripdestination:flightDestination};
                                                      }

}







                                                      sessionStorage.skypriorityAdvancedDetails=JSON.stringify(spDetails);

                                                      var obj1 = {};
                                                      obj1.flightData = sessionStorage.flightDetailsObject;
                                                      obj1.previousPage = "FlightDetailsPage";
                                                      //pushPageOnToBackStack(obj1);
                                                      setTimeout(function(){getskyPriorityAdvancedDetailsAjax(obj1)},1000);
                                                      }
                                                      else
                                                      {
                                                      hideActivityIndicator();
                                                      openAlertWithOk($.i18n.map.sky_notconnected);
                                                      }
                                                      };
                                                      window.open(iSC.Network, "_self");

                                                      },
               /* end of function to skyPriority page details*/


booking_function:function(e){



e.preventDefault();
e.stopPropagation();

       if($(e.currentTarget).data('bookingurl')==''){
         return;
       }
var book_Ori=sessionStorage.source;
var book_Des=sessionStorage.destination;
var book_OriDate=sessionStorage.depDate;
var book_RetDate=sessionStorage.returndate;
var book_lan=localStorage.language;
var oneortwo_way=sessionStorage.oneway_or_return;
var marketing_Code=$(e.currentTarget).data('marketingcode');
var bookingURL = $(e.currentTarget).data('bookingurl');

                                              var analyticsBtn="analytics://_trackEvent#"+"initiatebooking#"+"button_press#bookbtn-"+marketing_Code+"/"+localStorage.language+"#"+bookingURL;

                                                                                                   setTimeout(function() {

                                                                                                              window.open(analyticsBtn, "_self");

                                                                                                              }, 1000)

if(marketing_Code=="KQ" || marketing_Code=="CI" || marketing_Code=="MF"){
if(marketing_Code=="KQ"){
var kenyaLanguage = ['FR', 'EN', 'SP', 'IT', 'CN'];
                              if (book_lan.toLocaleLowerCase() == "zh" || book_lan.toLocaleLowerCase() == "zh-tw") {
                              book_lan = "CN";
                              }
                              if (book_lan.toLocaleLowerCase() == "es") {
                               book_lan = "SP";
                                }

                             if(kenyaLanguage.indexOf(book_lan.toLocaleUpperCase())<0){
                             book_lan="EN";
                             }

book_lan=book_lan.toLocaleUpperCase()

window.MyJSClient.getStringFromJS(bookingURL,book_Ori,book_Des,book_OriDate,book_RetDate,book_lan,oneortwo_way,marketing_Code);
                             } else if(marketing_Code=="CI"){
                              window.MyJSClient.getStringFromJS(bookingURL,book_Ori,book_Des,book_OriDate,book_RetDate,book_lan,oneortwo_way,marketing_Code);
                              }else if(marketing_Code=="MF"){
                               var locale = "En-eu";//var locale =urlM;
                               alert(urlM);
                                                                            var response='';
                                                                            // var apiUrl = 'http://www.xiamenair.com/';
                                                                            var apiUrl = 'https://www.xiamenair.com/v1/booking/xiamen?';
                                                                            //var apiUrl = 'https://services.skyteam.com/v1/booking/xiamen?'; //( WITH XIAMEN TIBCO URL)
                                                                            var postUrlMF='';
                                                                            if (oneortwo_way=="roundtrip") {
                                                                                $.ajax({
                                                                                                               url: apiUrl + 'uni-svc/Home/RedirectRoundWay?Nation='+ locale +'&AdultNum=1&ChildNum=0&InfantNum=0&CabinLevel=ECONOMY&OrgCity='+ book_Ori +'&DstCity=' + book_Des + '&DepartDate='+ book_OriDate +'&ReturnDate='+ book_RetDate +'&ExpireDate=000000&FZRA=00000003',
                                                                                                               type: 'GET',
                                                                                                               dataType: "json",
                                                                                                                async:false,
                                                                                                               success: function (data) {

                                                                                                                   response=data;
                                                                                                                  
                                                                                                                  

                                                                                                               }
                                                                                                           });


                                                                                                }
                                                                                            else {
                                                                                                    $.ajax({
                                                                                                                                   url: apiUrl + 'uni-svc/Home/RedirectOneWay?Nation=' + locale + '&AdultNum=1&ChildNum=0&InfantNum=0&CabinLevel=ECONOMY&OrgCity=' + book_Ori + '&DstCity=' + book_Des + '&DepartDate=' + book_OriDate + '&ExpireDate=000000&FZRA=00000003',
                                                                                                                                   type: 'GET',
                                                                                                                                   dataType: "json",
                                                                                                                                   async:false,
                                                                                                                                   success: function (data) {
                                                                                                           response=data;
                                                                                                                                   }
                                                                                                                               });

                                                                                                }
                                                                            var EMBEDDED_TRANSACTION=response.url.split('&')[1].split('=')[1];
                                                                            var SITE=response.url.split('&')[2].split('=')[1];
                                                                            var ENCT=response.url.split('&')[3].split('=')[1];
                                                                            var LANGUAGE=response.url.split('&')[4].split('=')[1];
                                                                            var ENC=response.url.split('&')[5].split('=')[1];

                                                                            postUrlMF="EMBEDDED_TRANSACTION="+EMBEDDED_TRANSACTION+"&SITE="+SITE+"&ENCT="+ENCT+"&LANGUAGE="+LANGUAGE+"&ENC="+ENC;

                               window.MyJSClient.getStringFromJS("https://global-et.xiamenair.com/plnext/XiamenAirB2C/Override.action",EMBEDDED_TRANSACTION,SITE,ENCT,LANGUAGE,ENC,oneortwo_way,marketing_Code);
                                                            }



}else{
if(marketing_Code=="KE"){
sessionStorage.memberDetailsURL=bookingURL;
}else if(marketing_Code=="SV"){
 bookingURL = bookingURL.replace(/(\r\n|\n|\r)/gm,"");
 sessionStorage.memberDetailsURL=bookingURL;
 }else{
sessionStorage.memberDetailsURL=encodeURI(bookingURL);
}
//window.open(iSC.openSkyTeamWebSite, "_self");
window.MyJSClient.getStringFromJS(bookingURL,"","","","","","",marketing_Code);
}


},

               /* function to save selected flight */
               saveFlightDetails : function(e) {
e.preventDefault();
e.stopPropagation();
               sessionStorage.insertSaveFlightFrom = "FlightDetails";
               window.open(iSC.insertSaveFlight);
               },
               /*End of save flight */
               /* Code for opening the airport details */
               navigateToAirport : function(e) {
               window.networkStatCallBack = function(){
               var netstat = JSON.parse(localStorage.networkStatus);
               if(netstat.network == "offline"){
               hideActivityIndicator();
               setTimeout(function(){
                          openAlertWithOk($.i18n.map.sky_notconnected);
                          },100);
               } else {
               sessionStorage.isFrom = "flightdetails";
               var pageObj={};
               pageObj.previousPage="FlightDetailsPage";
               pageObj.isFrom=sessionStorage.previousPage;
               if(sessionStorage.previousPage == "homePage")
               pageObj.data = sessionStorage.savedFlightDetails;
               pageObj.flightDetails=sessionStorage.flightDetailsObject;
               pageObj.flightResults=localStorage.searchResults;
               pageObj.isFrom=sessionStorage.previousPage;
               pageObj.status =JSON.stringify(flightStatus);
               pushPageOnToBackStack(pageObj);

               showActivityIndicatorForAjax('false');
               var id = $(e.currentTarget).attr('id');
               var airportcode = $('#' + id + ' li:eq(0) p').html();
               var obj={};
               sessionStorage.airportName = getAirportFullName(airportcode);
               sessionStorage.airport_code= airportcode;
               obj.airportName = getAirportFullName(airportcode);
               obj.airportCode = airportcode;
               obj.countryName = (getCountryName(airportcode)).split('_')[0];
               obj.countryCode = (getCountryName(airportcode)).split('_')[1];
               var countryCodeOld;
               countryCodeOld =getCountryNameOld(airportcode);


               obj.airportDetails_URL =getAirportFinderURL(airportcode);

               obj.cityName = getAirportName(airportcode);
               sessionStorage.currentPage = "airportDetails";
               sessionStorage.airportDetailsObj=JSON.stringify(obj);

               window.location.href = "index.html#airportDetails";

               }
               window.networkStatCallBack = function(){}
               }
               window.open(iSC.Network, "_self");

               }
               /* End of airport navigation */
       });

       return flightDetailsPageView;
       });
