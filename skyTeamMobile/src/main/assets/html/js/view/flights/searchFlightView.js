define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html', 'text!template/searchFlightList.html', 'models/flights/searchFlight','appView'], function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate, searchFlightList, searchFlightModel,appView) {


       var list = {};
       var jsonValue;
       var userNames;
       var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
       var dateObj;
       var keyval = "";
       sessionStorage.keyval = "";
       var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
       var currentDay;
       var tomorrowday;
       var year;
       var twoWayArrDate;
       var oneWayDepDate;
       var AirportNames;
       var destinationFlag = 0;
       var sourceFlag = 0;
       var isFlyFromOrToHere="";
       var strFlyFromHere="";
       var strFlyToHere="";
       var strKeyboardLang="";
       
       var isCancelBtnClicked=false;
       var TwoWayDep, TwoWayArrival, oneWayDep;
       var Source="";
       var Dest="";
       var isFrom = "";
       var saveData="";
       var twoWayDepartureDate;
       var arrivalDate;
       var oneWayDepartureDate;
       var searchFlightViewPageObject;
       var srcAirportCode, destAirportCode, self;
       var srcAirportName,destAirportName;
        var checkedAirlineStoring_onward=[];
        var checkedAirlineStoring_return=[];
       var flightPageView = appView.extend({
                                                 
                                                 id : 'flight-page',
                                                 initialize : function() {
                                                 $('#backButtonList').hide();
                                                 $('#headingText h2').css('margin-right','');
                                                 $('#headingText img').css('display', 'none');
                                                 $('#headingText h2').css('display', 'block');
                                                 $('#headingText h2').html($.i18n.map.sky_flight_finder);
                                                 if (sessionStorage.isFromSaved == "yes" || sessionStorage.savedflights == "yes") {
                                                 $('#brief img').css('display', 'block');
                                                 $('#brief h2').css('display', 'none');
                                                 sessionStorage.savedflights = "no";
                                                 
                                                 sessionStorage.isFromSaved = "no";
                                                 }
                                                 if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
                                                 $('.main_header ul li:eq(0) img').remove();
                                                 $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));
                                                 }
                                                 
                                                 //$(".menuItems").pageslide();
                                                 sessionStorage.removeItem('rdate');
                                                 
                                                 searchFlightViewPageObject =this;
                                                 
                                                 if($('#pageslide').children().length === 0)
                                                 {
                                                        $(".menuItems").pageslide();
                                                        var template = _.template(menuPageTemplate,$.i18n.map);
                                                        $('#pageslide').html(template);
                                                        sessionStorage.isMenuAdded = "YES";
                                                 }
                                                 
                                                 if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
                                                 hideActivityIndicator();
                                                 
                                                 }
                                                 else
                                                 {
                                                 window.getAirportsCallback = function() {
                                                 hideActivityIndicator();
                                                 
                                                 }
                                                 window.open(iSC.getAirports);
                                                 
                                                 }
                                               self = this;
                                                 },
                                                 
                                                 render : function() {
                                                 /******* interline */
                                                 sessionStorage.isinterline = "no";
                                                 /******* interline */
                                                 var dateToday = this.arguments;
												 //filter option show days of searchFlightViewResults invalidation
												 sessionStorage.show7DaysArrival ="no";
                                                 sessionStorage.show7DaysDeparture ="no";
                                                 sessionStorage.showList = "Onward";
                                                                                                 /* Back button click event */
                                                 $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {

                                                                                                   e.preventDefault();
                                                                                                   e.stopImmediatePropagation();
                                                                                                   var currentobj = JSON.parse(sessionStorage.previousPages);
                                                                                                   if("homePage" === currentobj[0].previousPage)
                                                                                                   {
                                                                                                   
                                                                                                   
                                                                                                   window.location.href = "index.html#homePage";
                                                                                                   popPageOnToBackStack();
                                                                                                   
                                                                                                   }
                                                                                                   else{
                                                                                                      if (sessionStorage.previousPages != 'undefined' && sessionStorage.previousPages != null) {
                                                                                                   
                                                                                                  
                                                                                                    if (currentobj[0].previousPage == "airportDetails" || currentobj[0].previousPage == "airportDetailsViewViaMap" ) {
                                                                                               
                                                                                                   showActivityIndicatorForAjax('false');
                                                                                                   sessionStorage.currentPage = currentobj[0].isFrom;
                                                                                                   sessionStorage.airportDetailsObj=currentobj[0].airportObj;
                                                                                                   sessionStorage.removeItem('lastPage');
                                                                                                   
                                                                                                   window.location.href = "index.html#" + currentobj[0].previousPage;
                                                                                                      popPageOnToBackStack();
                                                                                                   }
                                                                                                
                                                                                                   }
                                                                                                   }
                                                                                           
                                                                                              
                                                                                                   $(self.el).undelegate();
                                                                                                   });
                                                 /* End of back functionality */
                                                 sessionStorage.skyTipsAirportName = "";
                                                 sessionStorage.From = "Menu";
                                                 sessionStorage.airpor = "no";
                                                 sessionStorage.skytipstheme = 1;
                                                 datetodayReset = dateToday;
                                                 for (var i = 0,maxLeng=days.length; i < maxLeng; i++) {
                                                 if (i == dateToday.getDay()) {
                                                 
                                                 if (i == (days.length - 1)) {
                                                 
                                                 currentDay = days[i];
                                                 tomorrowday = days[0];
                                                 } else {
                                                 currentDay = days[i];
                                                 tomorrowday = days[i + 1];
                                                 }
                                                 }
                                                 }
                                                 
                                                 var locationPath = window.location;
                                                 locationPath = locationPath.toString();
                                                 
                                                 var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                 
                                                 sessionStorage.currentPage = "searchFlights";
                                                 
                                                 if (spath == "searchFlights" && sessionStorage.navToSearch == "yes") {
                                                 sessionStorage.isFromHome="";
                                                 $(".menuItems").trigger('click');
                                                 sessionStorage.navToSearch = "no";
                                                 sessionStorage.removeItem('searchData');
                                                 }
                                                 
                                                 var compiledTemplate = _.template(searchFlightList,$.i18n.map);
                                                 $(this.el).html(compiledTemplate);
                                                 var osVersion;
                                                 if(sessionStorage.deviceInfo == "iPhone"){
                                                 if ((screen.width == 320) && (screen.height == 480)) {
                                                 osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                 osVersion = osVersion[0];
                                                 osVersion = osVersion.replace(/_/g, '.');
                                                 osVersion = osVersion.replace('OS ', '');
                                                 if((osVersion.split('.')[0]) == "7"){
                                                 self.$el.find('#source').css('margin-top','10px');
                                                 self.$el.find('#destination').css('margin-top','10px');
                                                 self.$el.find('.datecalender button').css('margin-top','10px');
                                                 }
                                                 }
                                                 }
                                                 else if(sessionStorage.deviceInfo == "android"){
                                                 if ((screen.width == 480) && (screen.height == 800)) {
                                                 self.$el.find('#source').css('margin-top','10px');
                                                 self.$el.find('#destination').css('margin-top','10px');
                                                 self.$el.find('.datecalender button').css('margin-top','10px');
                                                 }
                                                 }
												 
												 //arrival date button visual style change
												 if(localStorage.language != "En")
                                                 self.$el.find(".arrival button li:nth-child(2)").css("margin-right", "0px");
												 
                                                 if (sessionStorage.searchData != undefined && sessionStorage.searchData != null) {
                                                 var dataobj = JSON.parse(sessionStorage.searchData);
                                         
                                                 Source = dataobj.Src;
                                                 Dest = dataobj.Dest;
                                                 sessionStorage.depDate = dataobj.TwoWayDep;
                                                 sessionStorage.returndate = dataobj.TwoWayArrival;
                                                 TwoWayDep = this.getDateAsDatePicker(new Date(dataobj.TwoWayDep));
                                                 TwoWayArrival = this.getDateAsDatePicker(new Date(dataobj.TwoWayArrival));
                                                 isFrom = dataobj.isFrom;
                                                 
                                                 }
                                                 else{
                                                 isFrom="";
                                                 }
                                                 if (sessionStorage.isFromHome == "yes") {
                                                 $('#backButtonList').show();
                                                 sessionStorage.navToHomePage="yes";
                                                 
                                                 }

                                                 if (isFrom == "oneway") {
                                                 self.$el.find('#source').val(Source);
                                                 self.$el.find('#destination').val(Dest);
                                                 self.$el.find('#departure').html(TwoWayDep);
                                                 self.$el.find('#arrival').html(TwoWayArrival);
                                                 saveData = "oneway";
                                                 this.oneWayTravel();
                                                 } 
												 else if (isFrom == "roundtrip") {
                                                 self.$el.find('#source').val(Source);
                                                 saveData = "roundtrip";
                                                 self.$el.find('#destination').val(Dest);
                                                 self.$el.find('#departure').html(TwoWayDep);
                                                 self.$el.find('#arrival').html(TwoWayArrival);
                                                 this.roundTripTravel();
                                                 } 
												 else {//setting the today date to departure & next date to arrival
													 //arrival date set
													 var tDay= new Date();
													 sessionStorage.selectedDepDay=days[tDay.getDay()];
													 sessionStorage.depDate = tDay.yyyymmdd();
													 self.$el.find("#departure").html(this.getDateAsDatePicker(tDay));
													 sessionStorage.getYear = tDay.getFullYear();
													 //departure date set
													 tDay.setDate(tDay.getDate()+1);
													 sessionStorage.selectedRetDay =days[tDay.getDay()];
													 sessionStorage.returndate = tDay.yyyymmdd();
													 self.$el.find("#arrival").html(this.getDateAsDatePicker(tDay));
													 
													if (sessionStorage.getReturnYear == undefined)
													 sessionStorage.getReturnYear = dateToday.getFullYear();
                                                 
                                                 }
                                        
                                                 
                                               if(sessionStorage.navToAirportDetails != undefined)
                                                 {
                                                 if(sessionStorage.navToAirportDetails == "yes")
                                                	{
                                                 sessionStorage.navToAirportDetails="";
                                                 sessionStorage.navToHomePage="";
                                                 $('#backButtonList').show();
                                                	}
                                                 }
                                           //showing backbutton for when coming from fly from here / fly to here
                                           try{
                                           if(sessionStorage.previousPages){
                                           var pageHistory = JSON.parse(sessionStorage.previousPages);
                                           if( hasValue(pageHistory) && pageHistory.length >0 && ("airportDetails" === pageHistory[0].previousPage) || ("airportDetailsViewViaMap" === pageHistory[0].previousPage)){
                                           $('#backButtonList').show();
                                           }
                                           }
                                           }
                                           catch(e){
                                           
                                           }
                                           
                                                 if (sessionStorage.getReturnYear == undefined)
                                                 sessionStorage.getReturnYear = dateToday.getFullYear();
                                                 
                                              
                                                 if (self.$el.find('#destination').val() != '') {
													 if(self.$el.find('#destination').val() != "Arriving at..."){
													 self.$el.find('.cancel_Destination').css('visibility', 'visible');
													 self.$el.find('#search_icon_dest').css('visibility', 'hidden');
													 }
                                                 }
                                                 else{
													 self.$el.find('.cancel_Destination').css('visibility', 'hidden');
													 self.$el.find('#search_icon_dest').css('visibility', 'visible');
                                                 }
                                                 if (self.$el.find('#source').val() != '') {
													 self.$el.find('.cancel_Source').css('visibility', 'visible');
													 self.$el.find('#search_icon_source').css('visibility', 'hidden');
                                                 }
                                                 else{
													 self.$el.find('.cancel_Source').css('visibility', 'hidden');
													 self.$el.find('#search_icon_source').css('visibility', 'visible');
                                                 }



                                                 //********************************************************************************

                                                                                            if(latLonAirportName.length){

                                                                                               autocomplete_autofill_inlanguagechange();
                                                                                              if(comingfromflyhere==1){
                                                                                              $('.cancel_Destination').css('visibility', 'hidden');
                                                                                                                         $('#search_icon_dest').css('visibility', 'visible');

                                                                                                                                       }else if(comingfromflyto==1){
                                                                                                                                        $('.cancel_Source').css('visibility', 'hidden');
                                                                                                                                        $('#search_icon_source').css('visibility', 'visible');
                                                                                                                                         }else{
                                                                                                                                          self.$el.find('.cancel_Source').css('visibility', 'visible');
                                                                                                                                          self.$el.find('#search_icon_source').css('visibility', 'hidden');

                                                                                                                                         }


                                                                                                                                       if(comingfromflyhere==0 && comingfromflyto==0 && backbuttonlog==0)
                                                                                                                                       {
                                                                                                                                       self.$el.find('#source').val(conte);


                                                                                            comingfromflyhere=0;
                                                                                             backbuttonlog=0;
                                                                                             srcAirportCode = latLonAirportCode;

                                                                                              srcLocationType = "APT";
                                                                                               comingfromflyto=0;
                                                                                                                                       }else{



                                                                                                                                       comingfromflyhere=0;
                                                                                                                                       comingfromflyto=0;
                                                                                                                                        backbuttonlog=0;
                                                                                                                                       }

                                                                                            isFrom = "";


                                                                                            }
                                                                                            //***********************************************************************************

//                                                 slidePage();
                                                 //slideThePage(sessionStorage.navDirection);


                                                 return appView.prototype.render.apply(this, arguments);

                                                 },//end of  render
                                                 events : {
                                                 'mousedown .cancel_Source' : 'crossImage_Source',
                                                 'mousedown .cancel_Destination' : 'crossImage_Destination',
                                                 'click #search_icon_source' : 'searchImage_Source',
                                                 'click #search_icon_dest' : 'searchImage_Destination',
                                                 'focusin #destination' : 'changeHeight',
                                                 'focusout #destination' : 'retainHeight',
                                                 'focusin #source' : 'changeHeightSource',
                                                 'focusout #source' : 'retainHeight',
                                                 'click .departure' : 'twoWayDepCalender',
                                                 'click .arrival' : 'returnJourneyCalender',
                                                 'click #oneway' : 'oneWayTravel',
                                                 'click #roundtrip' : 'roundTripTravel',
                                                 'click #searchFlights' : 'searchResults',
                                                 'click .okButton' : 'closePopUp',
                                                 'click #blackshade' : 'closeCalendar',
                                                 'keydown :input' : 'logkey',
                                                 'touchstart *' : function(e) {
                                                     $.pageslide.close();
                                                    
                                                 }
                                                 },
                                                 
                                                 retainHeight : function(e) {

                                                 $('.tabarea').css('margin-top','0');
                                                 if(sessionStorage.deviceInfo == "android"){
                                                 if ((screen.width == 480) && (screen.height == 800)) {
                                                 $(".tabarea").css('margin-top', '0px');
                                                 
                                                 }
                                                 }
												
												 if(isCancelBtnClicked)
												 {
													 isCancelBtnClicked=false;
													 $('#'+e.target.id).focus();
												 }
                                                
                                                 },

                                               
                                                 changeHeight : function(e){
                                                 var osVersion;
                                                 if(sessionStorage.deviceInfo == "iPhone"){
                                                 if ((screen.width == 320) && (screen.height == 480)) {
                                                 osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                 osVersion = osVersion[0];
                                                 osVersion = osVersion.replace(/_/g, '.');
                                                 osVersion = osVersion.replace('OS ', '');
                                                 if((osVersion.split('.')[0]) == "7" || (osVersion.split('.')[0]) == "8"){
                                                 $('.tabarea').css('margin-top','-70px');
                                                 }
                                                 }
                                                 }
                                                 else if(sessionStorage.deviceInfo == "android"){
                                                 if ((screen.width == 480) && (screen.height == 800)) {
                                                 $(".tabarea").css('margin-top', '-70px');
                                                 
                                                 }
                                                 }
                                                 },
                                                 changeHeightSource : function(e){
                                                 var osVersion;
                                                 if(sessionStorage.deviceInfo == "iPhone"){
                                                 if ((screen.width == 320) && (screen.height == 480)) {
                                                 osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                 osVersion = osVersion[0];
                                                 osVersion = osVersion.replace(/_/g, '.');
                                                 osVersion = osVersion.replace('OS ', '');
                                                 if((osVersion.split('.')[0]) == "7"){
                                                 $('.tabarea').css('margin-top','-60px');
                                                 }
                                                 }
                                                 }
                                                 },
                                                 searchImage_Source :function(e){
                                                	 $("#source").val("");
                                                     $("#source").focus();
                                                     isCancelBtnClicked=true;
                                                     var osVersion;
                                                     if(sessionStorage.deviceInfo == "iPhone"){
                                                     if ((screen.width == 320) && (screen.height == 480)) {
                                                     osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                     osVersion = osVersion[0];
                                                     osVersion = osVersion.replace(/_/g, '.');
                                                     osVersion = osVersion.replace('OS ', '');
                                                     if((osVersion.split('.')[0]) == "7"){
                                                     $('.tabarea').css('margin-top','-60px');
                                                     }
                                                     }
                                                     }
                                                     $(".cancel_Source").css("visibility", "hidden");
                                                     $('#search_icon_source').css('visibility', 'visible');
                                                 },
                                                 searchImage_Destination:function(e){
                                                	 $("#destination").val("");
                                                     $("#destination").focus();
                                                     isCancelBtnClicked=true;
                                                     var osVersion;
                                                     if(sessionStorage.deviceInfo == "iPhone"){
                                                     if ((screen.width == 320) && (screen.height == 480)) {
                                                     osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                     osVersion = osVersion[0];
                                                     osVersion = osVersion.replace(/_/g, '.');
                                                     osVersion = osVersion.replace('OS ', '');
                                                     if((osVersion.split('.')[0]) == "7"){
                                                     $('.tabarea').css('margin-top','-110px');
                                                     }
                                                     }
                                                     }
                                                     else if(sessionStorage.deviceInfo == "android"){
                                                     if ((screen.width == 480) && (screen.height == 800)) {
                                                     $(".tabarea").css('margin-top', '-70px');
                                                     }
                                                     }
                                                     $(".cancel_Destination").css("visibility", "hidden");
                                                     $('#search_icon_dest').css('visibility', 'visible');
                                                      
                                                 },
                                                 //To delete source..
                                                 crossImage_Source : function(e) {
                                                 $("#source").val("");
                                                 $("#source").focus();
                                                 isCancelBtnClicked=true;
                                                 var osVersion;
                                                 if(sessionStorage.deviceInfo == "iPhone"){
                                                 if ((screen.width == 320) && (screen.height == 480)) {
                                                 osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                 osVersion = osVersion[0];
                                                 osVersion = osVersion.replace(/_/g, '.');
                                                 osVersion = osVersion.replace('OS ', '');
                                                 if((osVersion.split('.')[0]) == "7"){
                                                 $('.tabarea').css('margin-top','-60px');
                                                 }
                                                 }
                                                 }
                                                 $(".cancel_Source").css("visibility", "hidden");
                                                 $('#search_icon_source').css('visibility', 'visible');
                                                 
                                                 },
                                                 //To delete destination content..
                                                 crossImage_Destination : function(e) {
                                                 $("#destination").val("");
                                                 $("#destination").focus();
                                                 isCancelBtnClicked=true;
                                                 var osVersion;
                                                 if(sessionStorage.deviceInfo == "iPhone"){
                                                 if ((screen.width == 320) && (screen.height == 480)) {
                                                 osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                 osVersion = osVersion[0];
                                                 osVersion = osVersion.replace(/_/g, '.');
                                                 osVersion = osVersion.replace('OS ', '');
                                                 if((osVersion.split('.')[0]) == "7"){
                                                 $('.tabarea').css('margin-top','-110px');
                                                 }
                                                 }
                                                 }
                                                 else if(sessionStorage.deviceInfo == "android"){
                                                 if ((screen.width == 480) && (screen.height == 800)) {
                                                 $(".tabarea").css('margin-top', '-70px');
                                                 }
                                                 }
                                                 $(".cancel_Destination").css("visibility", "hidden");
                                                 $('#search_icon_dest').css('visibility', 'visible');
                                                 
                                                 },
                                                 //To close date popup
                                                 closePopUp : function(e) {
                                                 
                                                 $('.date-popup').empty();
                                                 $("#blackshade").css('display', 'none');
                                                 $('.tripDetails').css('overflow-y', 'auto');
                                                 $('.date-popup').css({
                                                                      'display' : 'none'
                                                                      });
                                                 $('#ok').css({
                                                              'display' : 'none'
                                                              });
                                                 
                                                 },
                                                 //End of close date pop-up
                                                 closeCalendar : function(e) {
                                                 $('#departure').html(twoWayDepartureDate);
                                                 $("#arrival").html(arrivalDate);
                                                 var ele = $(e.toElement);
                                                 if (!ele.hasClass("hasDatepicker") && !ele.hasClass("ui-datepicker") && !ele.hasClass("ui-icon") && !$(ele).parent().parents(".ui-datepicker").length) {
                                                 $('.date-popup').empty();
                                                 $("#blackshade").css('display', 'none');
                                                 $('.tripDetails').css('overflow-y', 'auto');
                                                 $('.date-popup').css({
                                                                      'display' : 'none'
                                                                      });
                                                 $('#ok').css({
                                                              'display' : 'none'
                                                              });
                                                 
                                                 }
                                                 },
                                                 //Code for setting today's date
                                                 setToToday : function(e) {
                                                 
                                                 var currentDay;
                                                 for (var i = 0,maxLeng=days.length; i < maxLeng; i++) {
                                                 if (i == dateToday.getDay()) {
                                                 currentDay = days[i - 1];
                                                 }
                                                 }
                                                 
                                                 if (localStorage.date == "MMM") {
                                                 $("#" + texid).html(currentDay + " " + months[dateToday.getMonth()] + ' ' + dateToday.getDate() + ' ' + (dateToday.getYear() - 100));
                                                 } else {
                                                 $("#" + texid).html(currentDay + " " + dateToday.getDate() + ' ' + months[dateToday.getMonth()] + ' ' + (dateToday.getYear() - 100));
                                                 }
                                                 
                                                 $('.date-popup').empty();
                                                 $("#blackshade").css('display', 'none');
                                                 $('.tripDetails').css('overflow-y', 'auto');
                                                 $('.date-popup').css({
                                                                      'display' : 'none'
                                                                      });
                                                 $('#ok').css({
                                                              'display' : 'none'
                                                              });
                                                 
                                                 },
                                                 //End of setting today's date
                                                 /*Code for key event in source/destination fields */
                                                 logkey : function(e) {
                                                 
                                                 $('#source').keyup(function() {
                                                                    if ($('#source').val() != '') {
                                                                    $('.cancel_Source').css('visibility', 'visible');
                                                                    $('#search_icon_source').css('visibility', 'hidden');
                                                                    $('#source').css("border-color", "#d7d7d7");
                                                                    } else {
                                                                    $('.cancel_Source').css('visibility', 'hidden');
                                                                    $('#search_icon_source').css('visibility', 'visible');
                                                                    }
                                                                    
                                                                    });
                                                 
                                                 $('#destination').keyup(function() {
                                                                         
                                                                         if ($('#destination').val() != '') {
                                                                         $('.cancel_Destination').css('visibility', 'visible');
                                                                         $('#search_icon_dest').css('visibility', 'hidden');
                                                                         $('#destination').css("border-color", "#d7d7d7");
                                                                         } else {
                                                                         $('.cancel_Destination').css('visibility', 'hidden');
                                                                         $('#search_icon_dest').css('visibility', 'visible');
                                                                         }
                                                                         });
                                                 if (e.keyCode == 8) {
                                                        keyval = $('#' + e.target.id).val();
                                          
                                                 } else {
                                                    keyval = keyval + String.fromCharCode(e.keyCode);
                                                 }
                                                 if (keyval != "" && e.keyCode != 8) {
                                                 //sessionStorage.keyval = keyval;
                                           
                                                var minLenVal=3;
                                                 
                                                 var   names=[];
                                                 names=JSON.parse(localStorage.cityList);
                                                if(checkCJKCompliedLanguage())
                                                 {
                                                 
                                                 minLenVal=1;
                                                 
                                                 }
                                                 
                                                 
                                                 
                                                 if (names != null && names != undefined) {
                                                 toHighlightAutoComplete();
                                                 var departureWidget = $(".Departure_text").autocomplete({
                                                                                                         source : names,
                                                                                                         minLength : minLenVal,
                                                                                                         open : function(event, ui) {
                                                                                                         $(".ui-autocomplete").scrollTop(0);
                                                                                                         var menuHeight = $(window).height() - $(this).offset().top - $(this).height() - 30;
                                                                                                         if (menuHeight > 50) {
                                                                                                         $('.ui-autocomplete').css({
                                                                                                                                   "max-height" : "" + menuHeight + "px !important;"
                                                                                                                                   });
                                                                                                         } else {
                                                                                                         
                                                                                                         }
                                                                                                         
                                                                                                         },
                                                                                                         select : function(event, ui) {
                                                                                                         isFrom = "";
                                                                                                         
                                                                                                         var osVersion;
                                                                                                         if (sessionStorage.deviceInfo == "iPhone") {
                                                                                                         osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                                                                         osVersion = osVersion[0];
                                                                                                         osVersion = osVersion.replace(/_/g, '.');
                                                                                                         osVersion = osVersion.replace('OS ', '');
                                                                                                         if ((osVersion.split('.')[0]) == "8") {
                                                                                                         $(this).blur();
                                                                                                         }
                                                                                                         else if ((osVersion.split('.')[0]) == "7") {
                                                                                                         $(this).blur();
                                                                                                         }
                                                                                                         else if ((osVersion.split('.')[0]) == "6") {
                                                                                                         if(this.id !== "source"){
                                                                                                         $(this).blur();
                                                                                                         }
                                                                                                         }
                                                                                                         }
                                                                                                         
                                                                                                         if (sessionStorage.deviceInfo == "android" ) {
                                                                                                         $(this).blur();
                                                                                                        
                                                                                                         }
                                                                                                         if(this.id == "source"){
                                                                                                         	srcAirportCode = ui.item.airportCode;
                                                                                                         srcLocationType = ui.item.locationType;
                                                                                                         srcAirportName=ui.item.label.split('(')[0];
                                                                                                         setTimeout(function(){
                                                                                                                    $("#destination").focus();
                                                                                                                    },1);
                                                                                                         }
                                                                                                         if("destination" === this.id){
                                                                                                         destAirportCode = ui.item.airportCode;
                                                                                                         desLocationType = ui.item.locationType;
                                                                                                         destAirportName=ui.item.label.split('(')[0];
                                                                                                         }
                                                                                                         }
                                                                                                         });
                                                 
                                                 departureWidget._renderMenu = function(ul,items)
                                                 {
                                                 } 
                                                 departureWidget._renderItem = function(ul,item)
                                                 {
                                                 } 
                                                 
                                                 }
                                                 }
                                                 if (sessionStorage.deviceInfo == "android") {
                                                 if (e.keyCode == 13) {
                                                 $("#skyteam").css('margin-top', '0px');
                                                 $("#brief").css('visibility', 'visible');
                                                 $(this).blur();
                                                 this.searchResults();
                                                 }
                                                 }
                                                 },
                                                 /*End of key events */

                                                 /*To get search results */
                                                 searchResults : function(e) {



                                           e.preventDefault();
                                           e.stopImmediatePropagation();

sessionStorage.slider_return_stops="";
sessionStorage.slider_onward_stops="";
sessionStorage.slider_return_time0="";
sessionStorage.slider_return_time1="";
sessionStorage.slider_onward_time0="";
sessionStorage.slider_onward_time1="";
checkedAirlineStoring_onward = [];
checkedAirlineStoring_return = [];


                                                 sessionStorage.saved = "no";
                                                 var month;
                                                 var seldate;
                                                 var sep;
                                                 var formatteddate;
                                                 var returndate;
                                                 year = sessionStorage.getYear;
                                                 var yearReturn = sessionStorage.getReturnYear;
                                                 if (self.$el.find('#source').val() == "" || self.$el.find('#destination').val() == "") {
                                                 openAlertWithOk($.i18n.map.sky_necessary);
                                                 if (self.$el.find('#source').val() == "") {
                                                 self.$el.find('#source').css("border-color", "red");
                                                 }
                                                 if (self.$el.find('#destination').val() == "") {
                                                 self.$el.find('#destination').css("border-color", "red");
                                                 }
                                                 } else {

 if($('#source').val()!=undefined){
                                   sessionStorage.sourceValue = $('#source').val();
                                   sessionStorage.destinationValue = $('#destination').val();
                                   }

                                               try {
                                                  var tst=srcLocationType;
                                               }
                                               catch(err) {
                                                  srcLocationType="APT";
                                               }

                                                 var objSrc = validateAirportWithLocType(sessionStorage.sourceValue, srcLocationType);
                                                 var objDest = validateAirportWithLocType(sessionStorage.destinationValue, desLocationType);
                                                 
                                                 if(objSrc.isValid){
                                                 sourceFlag = 1;
                                                 srcAirportCode = objSrc.Code;
                                                 srcAirportName = objSrc.Name;
                                                srcLocationType = objSrc.locationType
                                                 }
                                                 if(objDest.isValid){
                                                 destinationFlag = 1;
                                                 destAirportCode = objDest.Code;
                                           desLocationType = objDest.locationType
                                           destAirportName = objDest.Name;
                                                 }
                                               
                                                 

                                                 
                                                 if (sourceFlag == 0 || destinationFlag == 0) {
                                                 openAlertWithOk($.i18n.map.sky_ncessary_airport);
                                                 if (sourceFlag == 0) {
                                                 self.$el.find('#source').css("border-color", "red");
                                                 }
                                                 if (destinationFlag == 0) {
                                                 self.$el.find('#destination').css("border-color", "red");
                                                 }
                                                  destinationFlag = 0;
                                                   sourceFlag = 0;
                                                 } else {
                                                 destinationFlag = 0;
                                                 sourceFlag = 0;
                                                 var source = srcAirportCode;
                                                 sessionStorage.source = srcAirportCode;
                                                 sessionStorage.sourceAirportName=srcAirportName;
                                                 sessionStorage.sourceLocationType=srcLocationType;

                                                 var destination = destAirportCode;
                                                 sessionStorage.destination = destAirportCode;
                                                 sessionStorage.destinationAirportName=destAirportName;
                                                 sessionStorage.destinationLocationType=desLocationType;
                                                 if (source == destination) {
                                                 openAlertWithOk($.i18n.map.sky_depart_arrival);

                                                 } else {
if(self.$el.find('.onHover a').html()!=undefined){
sessionStorage.onetwo_route=self.$el.find('.onHover a').html();
}


/******* interline */

var queryinterline="";
if (sessionStorage.isinterline == "no"){
queryinterline="isinterline=false&";
}else{
queryinterline="isinterline=true&";
}

/******* interline */
                                                 if (sessionStorage.onetwo_route == $.i18n.map.sky_one_way) {
                                                 var monthVal;
												 isFrom = "oneway";
                                           //to update destination and origin type - Boopathy
                                                 sessionStorage.url = URLS.FLIGHT_SEARCH + queryinterline + 'origin=' + source + '&origintype=' + srcLocationType + '&destination=' + destination + '&destinationtype=' + desLocationType + '&departuredate=' + sessionStorage.depDate;
                                                 }
                                                 else if (sessionStorage.onetwo_route == $.i18n.map.sky_round_trip) {
                                                 var monthVal;
                                                 isFrom = "roundtrip";
                                                 sessionStorage.url = URLS.FLIGHT_SEARCH + queryinterline + 'origin=' + source + '&origintype=' + srcLocationType + '&destination=' + destination +  '&destinationtype=' + desLocationType + '&departuredate=' + sessionStorage.depDate + '&returndate=' + sessionStorage.returndate;
                                                 }
                                                 var obj = {};

                                                 obj.Src = sessionStorage.sourceValue;
                                                 obj.Dest = sessionStorage.destinationValue;
                                                 obj.isFrom = isFrom != "" ? isFrom : "";
                                                 obj.TwoWayDep = sessionStorage.depDate;
                                                 obj.TwoWayArrival = sessionStorage.returndate;
                                                 sessionStorage.searchData = JSON.stringify(obj);
                                                 sessionStorage.successindicator="0";
                                                 showActivityIndicatorForAjax('false');
                                                 /*For checking internet connectivity*/
                                                 window.networkStatCallBack = function(){
                                                var netStatus =JSON.parse(localStorage.networkStatus);
                                                 /*If network is available,call the webservice*/

                                                 if(netStatus.network == "online"){
                                                 var acceptHeaderLang = localStorage.language;
                                            console.log("URL: " + sessionStorage.url);
                                                 if(localStorage.language == "zh"){
                                                 acceptHeaderLang = 'zh-Hans'
                                                 }
                                                 xhrajax=$.ajax({
                                                        type: "GET",
                                                        url: sessionStorage.url,
                                                                timeout:30000,
                                                                beforeSend:function(request){
                                                                request.setRequestHeader('Accept-Language',acceptHeaderLang);
                                                                request.setRequestHeader('api_key', API_KEY);

                                                                //request.setRequestHeader('api_key','3mr52xszzjkp854j4wjshwmz');
                                                                //request.setRequestHeader('source','SkyApp');
                                                                },
                                                             success:function (flightResults) {
sessionStorage.successindicator="1";
                                                                console.log("response: " + flightResults);
                                                                sessionStorage.filter = "no";

                                                               if(isFlyFromOrToHere == "yes" ){
                                                                if( strFlyToHere == self.$el.find("#destination").val() || strFlyFromHere == self.$el.find("#source").val())
                                                                {
                                                                sessionStorage.navToAirportDetails="yes";
                                                                sessionStorage.navToHomePage="";
                                                                }
                                                                else if(sessionStorage.isFromHome == "yes")
                                                                {
                                                                sessionStorage.navToHomePage="yes";
                                                                sessionStorage.navToAirportDetails="";
                                                                }
                                                                }

                                		                        try{

                                                                var flightFinderResults;
                                                                if (typeof(flightResults)=='object'){
                                                                flightFinderResults = flightResults;
                                                                }
                                                                else {
                                                                flightFinderResults = JSON.parse(flightResults);
                                                                }
                                                                
                                                                /*if(!flightFinderResults.hasOwnProperty("schedules")){0

                                                                console.log('Schedule not available');
                                                                    var tempObj = flightFinderResults;
                                                                    flightFinderResults = {};
                                                                    flightFinderResults.schedules = tempObj;
                                                                }*/
                                                                
                                                            if (flightFinderResults.schedules.hasOwnProperty("returnSchedules")) {
                                                            
                                                            flightFinderResults.journey="roundtrip";


                                                            } else {
                                                            
                                                            flightFinderResults.journey="oneway";
                                                            }

                                                                flightFinderResults = addSegmentIdentifier(flightFinderResults);
                                                            iSU.processFlightFinderData(flightFinderResults);


                                                        }
                                                        catch(exception){

                                                                hideActivityIndicator();

                                                            openAlertWithOk($.i18n.map.sky_requestfailed_flightfinder);
                                                        }
                                                        },
                                                                error : function(xhr, status, error) {


                                                                 hideActivityIndicator();
                                                                if(status != 'abort'){

                                                                openAlertWithOk($.i18n.map.sky_requestfailed_flightfinder);
                                                                }
                                                                },
                                                                failure : function() {
                                                                hideActivityIndicator();
                                                                }

                                                        });
                                                 }
                                                 else{
                                                 /*If network is not available,throw respective alert message*/
                                                 
                                                 var flightFinderResults = new Object();
                                                 flightFinderResults.Count = 0;
                                                 flightFinderResults.Connections = "Network Error";							    	
                                                 iSU.processFlightFinderData(flightFinderResults);
                                                 }
                                                 };

                                                 window.open(iSC.Network, "_self");

                                                 }
                                                 }
                                                 }

                                         /* Flight Finder Button Id - Tracking Google Analytics */
                                           var analyticsBtn="analytics://_trackEvent,"+"FindFlightsSearch,"+"button_press,SearchButton-"+localStorage.language+","+source+"-"+destination;
                                           setTimeout(function() {
                                                      window.open(analyticsBtn, "_self");
                                                      }, 1000);
                                           /* Flight Finder Button Id - Tracking Google Analytics */

                                                 },
                                                 /*End of search results */
                                                 /*Code for departure calendar in roundtrip */
                                                 twoWayDepCalender : function(e) {
                                                 twoWayDepartureDate=$('#departure').html();
                                                 arrivalDate=$("#arrival").html();
                                                 if (dateObj) {
                                                 dateObj.render($('.departure button li:eq(2)').attr('id'));
                                                 
                                                 } else {
                                                 require(['view/flights/subViews/datePopupView'], function(dateView) {
                                                         dateObj = new dateView();
                                                         dateObj.render($('.departure button li:eq(2)').attr('id'));
                                                         });
                                                 }
                                                 },
                                                 /*End of calendar popup*/
                                                 /*Code for return journey calendar in roundtrip */
                                                 returnJourneyCalender : function(val) {
                                                 
                                                 var depDate = $('.departure button li:eq(2)').html();
                                                 
                                                 if (dateObj) {
                                                 dateObj.render($('.arrival button li:eq(2)').attr('id'), depDate);
                                                 } else {
                                                 require(['view/flights/subViews/datePopupView'], function(dateView) {
                                                         dateObj = new dateView();
                                                         dateObj.render($('.arrival button li:eq(2)').attr('id'), depDate);
                                                         });
                                                 }
                                                 
                                                 },
                                                 /*End of return calendar */
                                                 /*To open oneway tab */
                                                 oneWayTravel : function() {
                                                 twoWayArrDate=self.$el.find("#arrival").html();
                                                 sessionStorage.tripType = "oneway";
                                                 self.$el.find('#oneway').removeClass('offHover').addClass('onHover');
                                                 self.$el.find('#roundtrip').removeClass('onHover').addClass('offHover');
                                                 self.$el.find('.arrival').hide();


                                                 },
                                                 /*End of oneway tab */
                                                 /*Open roundtrip tab */
                                                 roundTripTravel : function() {
                                                 sessionStorage.tripType = "roundtrip";
                                                 
                                                 self.$el.find('#roundtrip').removeClass('offHover').addClass('onHover');
                                                 self.$el.find('#oneway').removeClass('onHover').addClass('offHover');
                                                 self.$el.find('.arrival').show();

                                                 },
                                                 /*End of roundtrip */
												 getDateAsDatePicker:function(value){
													var shortMon=[$.i18n.map.sky_jan,$.i18n.map.sky_feb,$.i18n.map.sky_mar,$.i18n.map.sky_apr,$.i18n.map.sky_may,$.i18n.map.sky_jun,$.i18n.map.sky_jul,$.i18n.map.sky_aug,$.i18n.map.sky_sep,$.i18n.map.sky_oct,$.i18n.map.sky_nov,$.i18n.map.sky_dec];
													var shortDay=[$.i18n.map.sky_SUN,$.i18n.map.sky_MON,$.i18n.map.sky_TUE,$.i18n.map.sky_WED,$.i18n.map.sky_THU,$.i18n.map.sky_FRI,$.i18n.map.sky_SAT];

													var date,day,month,year;
													date = value.getDate();
													day = shortDay[value.getDay()];
													month=value.getMonth();
													year =value.getFullYear().toString();
													date=(date <10)? "0"+date:date;

													if(localStorage.date == "DD"){
													 return day+" "+date+" "+shortMon[month]+" "+year.substr(2,2);
													}
													else if(localStorage.date == "MMM"){
													 return day+" "+shortMon[month]+" "+date+" "+year.substr(2,2);
													}
													else{//for yyyy-mm-dd
													 month = month+1;
													 month=(month <10)? "0"+month:month;
													 return day+" "+value.getFullYear()+"-"+month+"-"+date;
													}
												}
                                                 });
       
       return flightPageView;
       
       });

var fullconte="";
var backbuttonlog=0;
var comingfromflyhere=0;
var comingfromflyto=0;
var comingfromairportskypriority=0;
var conte="";
var lattt="";
var longg="";
var latLonAirportCode="";
var latLonAirportName="";
var latLonAirportCountry="";
var options = {
enableHighAccuracy: true,
timeout: 5000,
maximumAge: 0
};


//navigator.geolocation.getCurrentPosition(success, error, options);
function error(err) {

};

/* Maintenance Message */
maintenance_message();

function success(pos) {
    var crd = pos.coords;
    lattt=crd.latitude;
    longg=crd.longitude;
    getAirportNamelatlon(lattt,longg);
};
//getAirportNamelatlon('12.9711843','80.1939652')
function getAirportNamelatlon(latitude, longitude) {

//API key change Start

var Url='https://api.skyteam.com/airportsearch?source=mobile&latitude='+latitude+'&longitude='+longitude+'&radius=300';
var api_key='3mr52xszzjkp854j4wjshwmz';


//var Url='https://services.staging.skyteam.com/airportsearch?source=mobile&latitude='+latitude+'&longitude='+longitude+'&radius=300';
//var api_key='5m49bjnak3nsrv5hdkf4aab6';
//API key change End


$.support.cors=true;
$.ajax({
                              url : Url,
                              type: 'POST',
                              dataType: 'json',
                              headers:{'api_key':api_key},
                             //  headers:{'api_key':api_key,'Accept-Language':acceptLanguage, 'source': 'SkyApp'},
//                              data : JSON.stringify(formData),
//                              contentType:"application/json; charset=utf-8",

                              success: function(data, textStatus, jqXHR)
                              {
                              displayAirportName(data);
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                              }
                              });





//
//    var urlofgeo='https://services.skyteam.com/airportsearch?source=mobile&latitude=' + latitude + '&longitude=' + longitude + '&radius=300';
//    $.support.cors = true;
//    $.ajax({
//           url : urlofgeo,
//           type: 'POST',
//           timeout: 30000,
//           dataType: 'json',
//           crossDomain: true,
//           headers:{'api_key':'3mr52xszzjkp854j4wjshwmz'},
//           success: function(data)
//           {
//           displayAirportName(data);
//           }
//           });
}

function displayAirportName(res) {

    var result = res;
    latLonAirportCode=result.AirportDetails[0].AirportCode;
    latLonAirportName=result.AirportDetails[0].AirportName;
    latLonAirportCountry=result.AirportDetails[0].CountryName;
    fullconte=latLonAirportName + ' ('+latLonAirportCode+'), '+latLonAirportCountry;
}
function autocomplete_autofill_inlanguagechange(){
    suggestionText=fullconte;
    var db = JSON.parse(localStorage.cityList);
    var filterIndex = 0;
    var response = { isValid: false, Code: '', Name: '',locationType: '' };

    do {
        filterIndex = suggestionText.indexOf( "(", filterIndex );


        if(filterIndex !== -1){
            filterIndex = filterIndex + 1;

            var code = suggestionText.substr( filterIndex, 3 );

            //var result = db(). filter({airportCode:code}).get();

            var result = db.filter(function (a) {return a.airportCode === code});
            var airportNameKey = "airportName" //_"+localStorage.language;
            var countryNameKey = "countryName" //_"+localStorage.language;
            if(result.length === 1){
                response.isValid = true;
                response.Code = code;
                response.Name = result[0].airportName;
                response.locationType = result[0].locationType
                response.fullname=result[0][airportNameKey]+" ("+code+"), "+result[0][countryNameKey];
                break;

            }


        }

    } while(filterIndex > 0);

    conte= response.fullname;


}
