define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'text!template/menuTemplate.html', 'text!template/savedFlightList.html'], 
function($, _, Backbone, utilities, constants,menuPageTemplate, savedFlightList) {

	var list = {};
	var flightPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'flight-page',

		initialize : function() {
		
		},

		render : function() {
			var compiledTemplate = _.template(savedFlightList,$.i18n.map);
            $(this.el).html(compiledTemplate);
				
	
		},
		events :{
		'touchstart *' : function() {
				$.pageslide.close();
			}
		
	}	
	});
	return flightPageView;

});
