define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'pageslide', 'slider', 'text!template/homeTemplate.html', 'text!template/menuTemplate.html', 'text!template/briefcaseTemplate.html','appView'], function($, _, Backbone, utilities, constants, pageslide, Swiper, homePageTemplate, menuPageTemplate, savedFlightAirportsTemplate,appView) {
       var that;
       var list = {};
       var savedData;
       var previousPage;
       var noOfFlights;
       var noOfAirports;
       var flightStatus = [];
       var skyTips=0;
       var rightSideButtonText = "";
       var savedFlightsAirportsView = appView.extend({

                                                           id : 'savedflights-page',

                                                           initialize : function() {
                                                           $('#backButtonList').show();

                                                           that = this;
                                                           $('#headingText img').css('display', 'none');
                                                           $('#headingText h2').css('display', 'block');
                                                           $('#mySkyTeamIcon').css('display', 'none');

                                                           $('#headingText h2').html($.i18n.map.sky_myskyteam);

                                                           if(sessionStorage.goNativeMapFromDetails === 'NO'){
                                                           previousPage="airportSearchPage";
                                                           sessionStorage.goNativeMapFromDetails='YES';
                                                           }
                                                           else
                                                           {
                                                           previousPage = sessionStorage.currentPage;
                                                           }
                                                           sessionStorage.tipstheme = 1;
                                                           if (previousPage == "searchResults") {
                                                           sessionStorage.From = "searchResults";

                                                           };
                                                           $('#rightSideText').html("<img id='deleteIcon' src='images/delete.png' />");
                                                            rightSideButtonText = "Edit";
                                                        },

                                                           render : function() {
                                                           sessionStorage.saved = "no";
                                                           var that = this, locationPath = window.location;
                                                           locationPath = locationPath.toString();

                                                           /*Back button functionality */
                                                           $('.main_header ul li:eq(1) img').off('click').on('click', function(e){

                                                                                                             sessionStorage.saved = "no";
                                                                                                             sessionStorage.removeItem('openedfrom');
                                                                                                             $('.flightsBriefcase').css('display', 'none');
                                                                                                             $('.airportsBriefcase').css('display', 'none');
                                                                                                             $('#rightSideText').hide();
                                                                                                             rightSideButtonText = "";
                                                                                                             $('#rightSideText').off('click');
                                                                                                             var currentobj = JSON.parse(sessionStorage.previousPages);
                                                                                                             showActivityIndicator($.i18n.map.sky_please);

                                                                                                             if (currentobj[0].previousPage == "homePage") {
                                                                                                             sessionStorage.homePage = "yes";
                                                                                                             //localStorage.removeItem("savedFlightDetails");
                                                                                                             var appSavedData= JSON.parse(localStorage.savedFlightDetails);
                                                                                                             appSavedData.SavedFlights_test = "ERROR";
                                                                                                             localStorage.savedFlightDetails = JSON.stringify(appSavedData);
                                                                                                             $('#headingText img').css('display', 'block');
                                                                                                             $('#headingText h2').css('display', 'none');
                                                                                                             hideActivityIndicator();
                                                                                                             $('#mySkyTeamIcon').css('display', 'block');
                                                                                                             $('#brief h2').css('display', 'none');
                                                                                                             sessionStorage.isMenuAdded = "NO";
                                                                                                             } else if (currentobj[0].previousPage == "aboutSkyTeam") {
                                                                                                             } else if (currentobj[0].previousPage == "FlightDetailsPage") {
                                                                                                             sessionStorage.flightDetailsObject = currentobj[0].flightData;
                                                                                                             sessionStorage.currentPage = currentobj[0].pageIsFrom;
                                                                                                             if(sessionStorage.currentPage == "homePage")
                                                                                                             sessionStorage.savedFlightDetails = currentobj[0].data ;
                                                                                                             } else if (currentobj[0].previousPage == "skyTipsResults") {
                                                                                                             $('#brief h2').hide();
                                                                                                             $('#mySkyTeamIcon').show();/*skyTips=1;
                                                                                                                                     sessionStorage.themeSelected = currentobj[0].theme;
                                                                                                                                     localStorage.skyTipsDetails = currentobj[0].data;
                                                                                                                                     sessionStorage.theme = currentobj[0].themeSelect;
                                                                                                                                     sessionStorage.skyTipsAirportName = currentobj[0].airportName;*/
                                                                                                             } else if (currentobj[0].previousPage == "loungesSearch") {
                                                                                                             sessionStorage.previousPage = "savedFlightsOrAirports";

                                                                                                             }
                                                                                                             else if (currentobj[0].previousPage == "yogaVideoPage") {
                                                                                                                 sessionStorage.isFromSavedAirportsFlights = "YES";
                                                                                                             }
                                                                                                             else if(currentobj[0].previousPage =="loungeFinderResult"){
                                                                                                             sessionStorage.lounge = currentobj[0].pageIsFrom;
                                                                                                             sessionStorage.airportName = currentobj[0].airportName;
                                                                                                             localStorage.loungeFinderMenuDisplay = currentobj[0].Loungesdetails;
                                                                                                             sessionStorage.airport_code=currentobj[0].airportCode;
                                                                                                             sessionStorage.airportDetailsObj = currentobj[0]. data;
                                                                                                             $('.main_header ul li:eq(1) img').off('click');
                                                                                                             } else if (currentobj[0].previousPage == "airportDetails") {
                                                                                                             sessionStorage.airportDetailsObj = currentobj[0].data;
                                                                                                             }
                                                                                                             else if(currentobj[0].previousPage == "weatherDetails"){
                                                                                                             sessionStorage.airportDetailsObj = currentobj[0].weatherAirportDetails;
                                                                                                             }
                                                                                                             else if(currentobj[0].previousPage == "skyPriorityAdvancedDetails" ||currentobj[0].previousPage == "addSkyTipsPage" ){
                                                                                                             hideActivityIndicator();
                                                                                                             }
                                                                                                             else if(currentobj[0].previousPage == "skyPriorityFinder"){
                                                                                                             sessionStorage.skyPriorityAirlines ='';
                                                                                                             sessionStorage.previousPage = "savedFlightsOrAirports";
                                                                                                             }

                                                                                                             e.preventDefault();
                                                                                                             e.stopImmediatePropagation();

                                                                                                             window.location.href = "index.html#" + currentobj[0].previousPage;
                                                                                                             sessionStorage.savedflights = "yes";
                                                                                                             sessionStorage.From ="home";
                                                                                                             if (sessionStorage.From == "Menu") {
                                                                                                             if(skyTips!=1){
                                                                                                             $('.main_header ul li:eq(0) img').remove();
                                                                                                             $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));
                                                                                                             }
                                                                                                             }
                                                                                                             sessionStorage.removeItem('history');
                                                                                                             popPageOnToBackStack();
                                                                                                             $(that.el).undelegate();

                                                                                                             });

                                                           /* End of Back functionality */
                                                           var compiledTemplate = _.template(savedFlightAirportsTemplate,$.i18n.map);

                                                           $(this.el).html(compiledTemplate);

                                                           if(sessionStorage.openedfrom != undefined || sessionStorage.openedfrom != null)
                                                           {
                                                           if(sessionStorage.openedfrom == "flights")
                                                           {
                                                           that.openFlightsTab();
                                                           }
                                                           else if(sessionStorage.openedfrom == "airports")
                                                           {
                                                           that.openAirportsTab();
                                                           }

                                                           }
                                                           else{
                                                           that.$el.find('#airportList').css('display','none');
                                                           that.$el.find('.airportsBriefcase').css('display', 'none');
                                                           }
                                                           savedData = JSON.parse(localStorage.savedFlightsAirports);

                                                           if (savedData.SavedFlights_test.length == 0) {
                                                           this.defaultFlightText();

                                                           } else {
                                                           $('#brief h2').css('display', 'block');

                                                           this.loadSavedFlightContent(savedData);
                                                           }
                                                           if (savedData.SavedAirports_test.length == 0) {
                                                           this.defaultAirportText();
                                                           } else {
                                                           $('#brief h2').css('display', 'block');
                                                           this.loadSavedAirportContent(savedData);
                                                           }
                                                           //setTimeout(function() {
                                                                      hideActivityIndicator();

                                                             //         }, 2000);
                                                        setTimeout(function(){
                                                      /* edit /done button functinality */
                                                     $('.main_header ul li #rightSidetext').off('click').on('click', function(e){
                                                        e.preventDefault();
                                                        e.stopImmediatePropagation();
                                                        if (rightSideButtonText === "Edit") {

                                                        $('#backButtonList').hide();

                                                        that.$el.find('.savedflightsresults_flight').off('click', that.openFlightDetails);
                                                        that.$el.find('.airportdiv').off('click', that.openAirportDetails);
                                                        $('#brief h2').html($.i18n.map.sky_done);
                                                        rightSideButtonText = "Done";
                                                        var results = JSON.parse(localStorage.savedFlightsAirports);

                                                        that.$el.find('.deleteClass').css('display','block');
                                                        that.$el.find('.deleteClass_airports').css('display','block');
                                                        that.$el.find(".savedflightsresults_flight ul li p").addClass("disableText");
                                                        that.$el.find('.deleteClass p').removeClass("disableText");
                                                        that.$el.find(".enableArrow").css('display', 'none');
                                                        that.$el.find(".disableArrow").css('display', 'block');
                                                        that.$el.find(".airportdiv li").addClass("disableText");

                                                        } else {
                                                        $('#brief h2').html("<img id='deleteIcon' src='images/delete.png' />");
                                                        rightSideButtonText = "Edit";
                                                        $('#mySkyTeamIcon').hide();
                                                        $('#backButtonList').show();
                                                        that.$el.find('.savedflightsresults_flight').on('click', that.openFlightDetails);
                                                        that.$el.find('.airportdiv').on('click', that.openAirportDetails);

                                                        var results = JSON.parse(localStorage.savedFlightsAirports);

                                                        that.$el.find('.deleteClass').css('display','none');
                                                        that.$el.find('.deleteClass_airports').css('display','none');
                                                        that.$el.find(".savedflightsresults_flight ul li p").removeClass("disableText");
                                                        that.$el.find(".enableArrow").css('display', 'block');
                                                        that.$el.find(".disableArrow").css('display', 'none');
                                                        that.$el.find(".airportdiv li").removeClass("disableText");
                                                        for (var j = 0,maxLeng=results.SavedAirports_test.length; j < maxLeng; j++) {
                                                        that.$el.find('#deleteAirportItem_' + results.SavedAirports_test[j].Id).remove();
                                                        }
//
                                                        }

                                                     });
                                                     /* edit /done button functinality */
                                                       },1000);

                                                     setTimeout(function(){
                                                            var fHeightAirports = parseInt(that.$el.find("#airports").css("height"));
                                                            var fHeightFlights = parseInt(that.$el.find("#flights").css("height"));
                                                            if(fHeightAirports > fHeightFlights)
                                                            {
                                                                that.$el.find("#flights").css("height",""+fHeightAirports);
                                                            }
                                                            else if(fHeightFlights > fHeightAirports)
                                                            {
                                                                that.$el.find("#airports").css("height",""+fHeightFlights);
                                                            }
                                                        },1);


                                                            return appView.prototype.render.apply(this, arguments);

                                                           },
                                                           events : {

                                                           'click .deleteClass' : 'deleteElement',
                                                           'click .deleteAirport p' : 'deleteSelectedAirport',
                                                           'click #flights' : 'openFlightsTab',
                                                           'click #airports' :'openAirportsTab',
                                                           'touchstart *' : function() {
                                                           $.pageslide.close();
                                                           }


                                                           },
                                                           openFlightsTab : function(){
                                                           that.$el.find('#flights').removeClass('offHover').addClass('onHover');
                                                           that.$el.find('#airports').removeClass('onHover').addClass('offHover');
                                                           that.$el.find('#flightList').css('display','block');
                                                           that.$el.find('#airportList').css('display','none');
                                                           },
                                                           openAirportsTab : function(){
                                                           that.$el.find('#airports').removeClass('offHover').addClass('onHover');
                                                           that.$el.find('#flights').removeClass('onHover').addClass('offHover');
                                                           that.$el.find('#airportList').css('display','block');
                                                           that.$el.find('.airportsBriefcase').css('display', 'block');
                                                           that.$el.find('#flightList').css('display','none');
                                                           },
                                                           /* To display default text when we do not have any saved flight*/
                                                           defaultFlightText : function(e) {
                                                           that.$el.find('#flightList').append($('<div class="offlinemsg"></div>').html($.i18n.map.sky_offline_features));
                                                           sessionStorage.savedflights = "yes";
                                                           },
                                                           /* End of function */
                                                           /* To display default text when we do not have any saved airport*/
                                                           defaultAirportText : function(e) {
                                                           that.$el.find('#airportList').append($('<div class="offlinemsg"></div>').html($.i18n.map.sky_findairport));
                                                           sessionStorage.savedflights = "yes";
                                                           },
                                                           /* End of function */
                                                           /*To open selected saved airport details*/
                                                           openAirportDetails : function(e) {
                                                           window.networkStatCallBack = function(){};
                                                           window.open(iSC.Network, "_self");
                                                           sessionStorage.openedfrom="airports";
                                                           sessionStorage.isFromSaved = "yes";
                                                           sessionStorage.savedAirportDetails = "yes";
                                                           var pageObj = {};
                                                           pageObj.previousPage = "savedFlightsOrAirports";
                                                           pageObj.isFrom = "savedflights-page";

                                                           pushPageOnToBackStack(pageObj);

                                                           if (sessionStorage.airportDetailsObj != null || sessionStorage.airportDetailsObj != undefined)
                                                           sessionStorage.tempObj = sessionStorage.airportDetailsObj;

                                                           var airportcode = $('#' + this.id + " li:eq(0) p").html();
                                                           sessionStorage.previousPage = sessionStorage.currentPage;
                                                           sessionStorage.currentPage = that.id;

                                                           var saved_airport = JSON.parse(localStorage.savedFlightsAirports);

                                                                                                                $.each(saved_airport.SavedAirports_test, function(key) {
                                                                                                                       if (saved_airport.SavedAirports_test[key].Location_Code == airportcode) {
                                                                                                                       sessionStorage.airport_code = saved_airport.SavedAirports_test[key].Location_Code;
                                                                                                                       sessionStorage.airportName = saved_airport.SavedAirports_test[key].Airport_Name;
                                                                                                                       var obj = {};
                                                                                                                       obj.airportName = saved_airport.SavedAirports_test[key].Airport_Name;
                                                                                                                       obj.countryName = saved_airport.SavedAirports_test[key].Country_Name;
                                                                                                                       obj.airportCode = saved_airport.SavedAirports_test[key].Location_Code;
                                                                                                                       obj.countryCode = saved_airport.SavedAirports_test[key].Country_Code;
                                                                                                                       obj.airportDetails_URL = getAirportFinderURL(sessionStorage.airport_code ); // saved airports from older versions of app contains old airport details url - there fore to point to new url 'getAirportFinderURL' function is used
                                                                                                                       obj.cityName = saved_airport.SavedAirports_test[key].City_Name;
                                                                                                                       sessionStorage.airportDetailsObj = JSON.stringify(obj);

                                                                  } else {

                                                                  }

                                                                  });
                                                           $('.flightsBriefcase').css('display', 'none');
                                                           $('.airportsBriefcase').css('display', 'none');

                                                           showActivityIndicatorForAjax('false');
                                                           setTimeout(function() {
                                                                      window.location.href = "index.html#airportDetails";
                                                                      }, 1000);

                                                           },
                                                           /* End of Airport details */
                                                           /*To show selected saved flight details */
                                                           openFlightDetails : function(e) {
                                                           showActivityIndicator($.i18n.map.sky_please);
                                                           sessionStorage.home = "no";
                                                           sessionStorage.openedfrom="flights";
                                                           sessionStorage.saved = "yes";
                                                           sessionStorage.isFromSaved = "yes";
                                                           var pageObj = {};
                                                           pageObj.previousPage = "savedFlightsOrAirports";
                                                           pageObj.isFrom = "savedflights-page";
                                                           pushPageOnToBackStack(pageObj);

                                                           if (sessionStorage.airportDetailsObj != null || sessionStorage.airportDetailsObj != undefined)
                                                           sessionStorage.tempObj = sessionStorage.airportDetailsObj;

                                                           sessionStorage.FromhomePage = "";
                                                           if (sessionStorage.From == "searchResults") {
                                                           sessionStorage.From = "searchResults";
                                                           }
                                                           if (rightSideButtonText === "Edit") {
                                                           sessionStorage.previousPage = sessionStorage.currentPage;
                                                           sessionStorage.currentPage = that.id;
                                                           if (sessionStorage.previousPage == "searchFlights") {
                                                           sessionStorage.isFrom = "searchFlights";
                                                           }
                                                           var selectedFlightId = ($(e.currentTarget).attr('id')).split('_')[1];
                                                           for (var i = 0,maxLeng=savedData.SavedFlights_test.length; i < maxLeng; i++) {
                                                           if (selectedFlightId == savedData.SavedFlights_test[i].Id) {
                                                           sessionStorage.savedFlightDetails = JSON.stringify(savedData.SavedFlights_test[i].Details);
                                                           sessionStorage.flightDetailsObject=savedData.SavedFlights_test[i].Details;
                                                           flightStatus = JSON.stringify(savedData.SavedFlights_test[i]);
                                                           }
                                                           }
                                                           var flightStatusURL = JSON.parse(flightStatus).FlightStatusURL;
                                                           sessionStorage.savedFlightStatusURL = flightStatusURL;

                                                           if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
                                                           window.getAirportsCallback = function() {
                                                           window.open(iSC.savedFlightStatusURL, "_self");
                                                           }
                                                           showActivityIndicator($.i18n.map.sky_please);
                                                           window.open(iSC.getAirports);
                                                           } else {
                                                           window.open(iSC.savedFlightStatusURL, "_self");

                                                           }

                                                           }
                                                           },
                                                           /* End of saved flights details */
                                                           /* To load saved flight content after deleting any flight from list*/

                                                           loadSavedFlightContent : function(savedData) {
                                                           sessionStorage.removeItem('isFromSaved');
                                                           noOfFlights = savedData.SavedFlights_test.length;
                                                           sessionStorage.savedflights = "yes";
                                                           if (savedData.SavedFlights_test.length == 0 && (noOfAirports == 0 || noOfAirports == undefined)) {
                                                           $('#brief h2').html("<img id='deleteIcon' src='images/delete.png' />");
                                                           rightSideButtonText = "Edit";
                                                           this.defaultFlightText();
                                                           $('#brief h2').css('display', 'none');
                                                           $('#backButtonList').show();
                                                           }
                                                           var dep_date;
                                                           var arr_date;
                                                           var dep_time;
                                                           var arr_time;
                                                           for (var i = 0,maxLeng=savedData.SavedFlights_test.length; i < maxLeng; i++) {

                                                           dep_date = getDateInLocaleFormat(savedData.SavedFlights_test[i].Departure_Date,true);
                                                           arr_date = getDateInLocaleFormat(savedData.SavedFlights_test[i].Arrival_Date,true);
                                                           if (localStorage.time == "12h") {
                                                           dep_time = timeConvertor(savedData.SavedFlights_test[i].Departure_Time);
                                                           arr_time = timeConvertor(savedData.SavedFlights_test[i].Arrival_Time);
                                                           } else {
                                                           dep_time = savedData.SavedFlights_test[i].Departure_Time;
                                                           arr_time = savedData.SavedFlights_test[i].Arrival_Time;
                                                           }
                                                           
                                                          var detailObj= JSON.parse(savedData.SavedFlights_test[i].Details);
//                                                     var flightcount = detailObj.details.flights.length;
//                                                     var normalstops = parseInt(savedData.SavedFlights_test[i].No_of_Stops);
//                                                     var totalstoppage =normalstops;
//                                                     
//                                                     for(iter = 0; iter<flightcount; iter++)
//                                                     {
//                                                     totalstoppage = totalstoppage + detailObj.details.flights[iter].totalTechnicalStops;
//                                                     
//                                                     }
//                                                     
                                                     var totalstoppage;
                                                      var detailObj= JSON.parse(savedData.SavedFlights_test[i].Details);
                                                     if(detailObj.details.flights)
                                                     {
                                                     var flightcount = detailObj.details.flights.length;
                                                     var normalstops = parseInt(savedData.SavedFlights_test[i].No_of_Stops);
                                                     totalstoppage =normalstops;
                                                     
                                                     for(iter = 0; iter<flightcount; iter++)
                                                     {
                                                     totalstoppage = totalstoppage + detailObj.details.flights[iter].totalTechnicalStops;
                                                     }
                                                     
                                                     }else{
                                                     totalstoppage = savedData.SavedFlights_test[i].No_of_Stops;
                                                     }

                                                           that.$el.find('#flightList').append($('<div></div>').attr('class', 'savedflightsresults_flight')
                                                                                   .attr('id', 'flights_' + savedData.SavedFlights_test[i].Id).append($('<ul></ul>').append($('<li></li>')
                                                                                                                                                                            .append($('<p></p>').attr('class', 'header2').html(savedData.SavedFlights_test[i].Departure)))
                                                                                                                                                      .append($('<li></li>').append($('<img></img>').attr('id', 'deleteFlightItem_' + savedData.SavedFlights_test[i].Id)
                                                                                                                                                                                    .attr('src', './images/stops-' + totalstoppage + '-icon.png'))).append($('<li></li>')
                                                                                                                                                                                                                                                                                        .append($('<p></p>').attr('class', 'header2').html(savedData.SavedFlights_test[i].Arrival)))).append($('<ul></ul')
                                                                                                                                                                                                                                                                                                                                                                                             .append($('<li></li>').append($('<p></p>').html(getAirportName(savedData.SavedFlights_test[i].Departure))))
                                                                                                                                                                                                                                                                                                                                                                                             .append($('<li></li>').append($('<p></p>').html(getAirportName(savedData.SavedFlights_test[i].Arrival)))))
                                                                                   .append($('<ul></ul>').append($('<li></li>').append($('<p></p>').attr('class', 'header3').css('font-size','12px').html(dep_date)))
                                                                                           .append($('<li></li>').append($('<p></p>').attr('class', 'header3').html(dep_time))).append($('<li></li>')
                                                                                                                                                                                       .append($('<p></p>').attr('class', 'header3').html(arr_time))).append($('<li></li>')
                                                                                                                                                                                                                                                             .append($('<p></p>').attr('class', 'header3').css('font-size','12px').html(arr_date)))).append($('<ul></ul>')
                                                                                                                                                                                                                                                                                                                                                            .append($('<li></li>').append($('<p></p>').attr('class', 'header3').html())).append($('<li></li>')
                                                                                                                                                                                                                                                                                                                                                                                                                                                .append($('<p></p>').attr('class', 'header3').html(savedData.SavedFlights_test[i].Flight_Number+' - '+getAirlineName(savedData.SavedFlights_test[i].Flight_Number.substr(0,2)))))
                                                                                                                                                                                                                                                                                                                                                            .append($('<li></li>').css('display', 'none').append($('<p></p>').attr('class', 'header3').html($.i18n.map.sky_fileson)
                                                                                                                                                                                                                                                                                                                                                                                                                 .append($('<label></label>').html($.i18n.map.sky_monday)).append($('<label></label>').html($.i18n.map.sky_tuesday))
                                                                                                                                                                                                                                                                                                                                                                                                                 .append($('<label></label>').html($.i18n.map.sky_wednesay)).append($('<label></label>').html($.i18n.map.sky_thursday)).append($('<label></label>').html($.i18n.map.sky_friday))
                                                                                                                                                                                                                                                                                                                                                                                                                 .append($('<label></label>').html($.i18n.map.saturday)).append($('<label></label>').html($.i18n.map.sky_sunday)))))
                                                                                   .append($('<ul></ul>').append($('<li></li>').attr('id', 'deleteFlightItem_' + savedData.SavedFlights_test[i].Id)
                                                                                                                 .attr('class','deleteClass deleteButton').append($('<p></p>').html($.i18n.map.sky_delete)))).click(that.openFlightDetails));

                                                           }

                                                           if (rightSideButtonText === "Done") {
                                                           that.$el.find('.savedflightsresults_flight').off('click', that.openFlightDetails);
                                                           that.$el.find('.airportdiv').off('click', that.openAirportDetails);
                                                           if (savedData.SavedFlights_test.length == 0) {
                                                           this.defaultFlightText();
                                                           if (noOfAirports == 0)
                                                           $('#brief h2').html("<img id='deleteIcon' src='images/delete.png' />");
                                                           rightSideButtonText = "Edit";
                                                           } else {

                                                           that.$el.find('.deleteClass').css('display','block');
                                                           that.$el.find('.deleteClass_airports').css('display','block');
                                                           }
                                                           that.$el.find(".savedflightsresults_flight  ul li p").addClass("disableText");
                                                           that.$el.find(".enableArrow").css('display', 'none');
                                                           that.$el.find(".disableArrow").css('display', 'block');
                                                           that.$el.find(".airportdiv li").addClass("disableText");
                                                           }

                                                           },
                                                           /* End of loading saved content */
                                                           /* To load saved airports list after deleting any airport from list */

                                                           loadSavedAirportContent : function(savedData) {
                                                           sessionStorage.removeItem('isFromSaved');
                                                           noOfAirports = savedData.SavedAirports_test.length;
                                                           sessionStorage.savedflights = "yes";
                                                           if (savedData.SavedAirports_test.length == 0 && (noOfFlights == 0 || noOfFlights == undefined)) {

                                                           $('#brief h2').css('display', 'none');
                                                           $('#backButtonList').show();
                                                           }

                                                           for (var i = 0,maxLeng=savedData.SavedAirports_test.length; i < maxLeng; i++) {

                                                           that.$el.find('#airportList').append($('<ul></ul>').attr('class', 'airportdiv').attr('id', 'airports_' + savedData.SavedAirports_test[i].Id)
                                                                                    .append($('<li></li>').attr('class', 'airportinfo')
                                                                                            .append($('<p></p>').html(savedData.SavedAirports_test[i].Location_Code))).append($('<li></li>').attr('class', 'fullairportinfo')
                                                                                                                                                                              .html(getAirportFullName(savedData.SavedAirports_test[i].Location_Code))).append($('<li></li>').attr('class', 'arrowimg')
                                                                                                                                                                                                                                                               .append($('<img></img>').attr('class', 'enableArrow').attr('src', './images/arrow.png'))
                                                                                                                                                                                                                                                               .append($('<img></img>').attr('class', 'disableArrow').attr('src', './images/arrow32.png'))).click(this.openAirportDetails)
                                                                                    .append($('<li></li>').attr('class','deleteClass_airports deleteAirport').append($('<p></p>').attr('id', 'deleteAirportImgItem_' + savedData.SavedAirports_test[i].Id).html($.i18n.map.sky_delete))));

                                                           }

                                                           if (rightSideButtonText === "Done") {
                                                           that.$el.find('.savedflightsresults_flight').off('click', that.openFlightDetails);
                                                           that.$el.find('.airportdiv').off('click', that.openAirportDetails);
                                                           if (savedData.SavedAirports_test.length == 0) {
                                                           this.defaultAirportText();
                                                           if (noOfFlights == 0)
                                                           $('#brief h2').html("<img id='deleteIcon' src='images/delete.png' />");
                                                           rightSideButtonText = "Edit";

                                                           } else {
                                                           that.$el.find('.deleteClass').css('display','block');
                                                           that.$el.find('.deleteClass_airports').css('display','block');				}
                                                           that.$el.find(".savedflightsresults_flight ul li p").addClass("disableText");
                                                           that.$el.find(".enableArrow").css('display', 'none');
                                                           that.$el.find(".disableArrow").css('display', 'block');
                                                           that.$el.find(".airportdiv li").addClass("disableText");
                                                           }
                                                           },
                                                           /* End of loading saved airport content */
                                                           /* To delete any selected saved airport from list*/

                                                           deleteSelectedAirport : function(e) {
                                                           e.preventDefault();

                                                           var targetName = $(e.currentTarget).attr("id").split('_')[0];
                                                           var id = $(e.currentTarget).attr("id").split('_')[1];
                                                           $('.popUp').append($('<button class="yesButton">'+$.i18n.map.sky_no+'</button>')).append($('<button class="noButton">'+$.i18n.map.sky_yes+'</button>'));
                                                           $('.popUpShade').css('display', 'block');
                                                           $('.popUp').css('display', 'block');

                                                           $('.popUpMessage1').html($.i18n.map.sky_delete_confirm);
                                                           $('.popUp button').css('display', 'block');
                                                           $('.popUp button:eq(0)').css('display', 'none');
                                                           $('.popUpShade').css({
                                                                                opacity : 0.5,

                                                                                });

                                                           $('.popUp button:eq(1)').click(function() {
                                                                                          $('.popUp button:eq(2)').remove();
                                                                                          $('.popUp button:eq(1)').remove();
                                                                                          $('.popUpShade').css('display', 'none');
                                                                                          $('.popUp').css('display', 'none');
                                                                                          });
                                                           $('.popUp button:eq(2)').click(function() {
                                                                                          $('.popUp button:eq(2)').remove();
                                                                                          $('.popUp button:eq(1)').remove();
                                                                                          $('.popUpShade').css('display', 'none');
                                                                                          $('.popUp').css('display', 'none');

                                                                                          if (targetName == "deleteAirportImgItem") {
                                                                                          $(".savedflightsresults_flight ul li p").removeClass("disableText");
                                                                                          $('#airportList').html(" ");
                                                                                          sessionStorage.deletedAirportID = $(e.currentTarget).attr("id").split('_')[1];

                                                                                          window.open(iSC.deleteAirports, "_self");
                                                                                          showActivityIndicator($.i18n.map.sky_please);
                                                                                          setTimeout(function() {
                                                                                                     that.loadSavedAirportContent(JSON.parse(localStorage.retrieveAirports));
                                                                                                     }, 1000);
                                                                                          $(".savedflightsresults_flight ul li p").addClass("disableText");
                                                                                          $(".airportdiv li").addClass("disableText");
                                                                                          }
                                                                                          });

                                                           },
                                                           /* End of deleting airport */
                                                           /* To delete any selected saved flight from list*/

                                                           deleteElement : function(e) {
                                                           sessionStorage.saved = "no";
                                                           e.preventDefault();
                                                           var targetName = $(e.currentTarget).attr("id").split('_')[0];
                                                           var id = $(e.currentTarget).attr("id").split('_')[1];

                                                           $('.popUp').append($('<button class="yesButton">'+$.i18n.map.sky_no+'</button>')).append($('<button class="noButton">'+$.i18n.map.sky_yes+'</button>'));
                                                           $('.popUpShade').css('display', 'block');
                                                           $('.popUp').css('display', 'block');

                                                           $('.popUpMessage1').html($.i18n.map.sky_delete_confirm);
                                                           $('.popUp button').css('display', 'block');
                                                           $('.popUp button:eq(0)').css('display', 'none');
                                                           $('.popUpShade').css({
                                                                                opacity : 0.5,

                                                                                });

                                                           $('.popUp button:eq(1)').click(function() {
                                                                                          $('.popUp button:eq(2)').remove();
                                                                                          $('.popUp button:eq(1)').remove();
                                                                                          $('.popUpShade').css('display', 'none');
                                                                                          $('.popUp').css('display', 'none');
                                                                                          });
                                                           $('.popUp button:eq(2)').click(function() {
                                                                                          $('.popUp button:eq(2)').remove();
                                                                                          $('.popUp button:eq(1)').remove();
                                                                                          $('.popUpShade').css('display', 'none');
                                                                                          $('.popUp').css('display', 'none');

                                                                                          //localStorage.removeItem("savedFlightDetails");
                                                                                          var appSavedData= JSON.parse(localStorage.savedFlightDetails);
                                                                                          appSavedData.SavedFlights_test = "ERROR";
                                                                                          localStorage.savedFlightDetails = JSON.stringify(appSavedData);

                                                                                          if (targetName == "deleteFlightItem") {
                                                                                          $(".savedflightsresults_flight ul li p").removeClass("disableText");
                                                                                          var id = $(e.currentTarget).attr("id").split('_')[1];
                                                                                          if (targetName == "deleteFlightItem") {
                                                                                          $('#flightList').html(" ");
                                                                                          sessionStorage.deletedflightID = $(e.currentTarget).attr("id").split('_')[1];
                                                                                          window.open(iSC.deleteFlights, "_self");
                                                                                          showActivityIndicator($.i18n.map.sky_please);
                                                                                          setTimeout(function() {
                                                                                                     that.loadSavedFlightContent(JSON.parse(localStorage.retrieveFlights));
                                                                                                     }, 1000);
                                                                                          }
                                                                                          $(".savedflightsresults_flight ul li p").addClass("disableText");
                                                                                          $(".airportdiv li").addClass("disableText");
                                                                                          }
                                                                                          });
                                                           }
                                                           /* End of delete flight from list */
                                                           });

       return savedFlightsAirportsView;

       });