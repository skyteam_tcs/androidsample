define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/flightStatusDetailsTemplate.html','appView'], function($, _, Backbone, utilities, constants, jqueryUi, flightStatusDetailsTemplate,appView) {
       var statusResults
       var flightStatusDetailsPageView = appView.extend({
                                                        
                                                        id : 'flight-details',
                                                        
                                                        initialize : function() {
                                                        $('#backButtonList').show();
                                                        $('#headingText img').css('display', 'none');
                                                        $('#headingText h2').css('display', 'block');
                                                        $('#headingText h2').html($.i18n.map.sky_flightstatus);
                                                        if (sessionStorage.savedflights == "yes") {
                                                        $('#brief img').css('display', 'block');
                                                        $('#brief h2').css('display', 'none');
                                                        }
                                                        },
                                                        render : function() {
                                                        var that = this;
                                                        showActivityIndicator($.i18n.map.sky_please);
                                                        /*Back button functionality */
                                                        $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                                                                                                          var currentobj = JSON.parse(sessionStorage.previousPages);
                                                                                                          showActivityIndicator($.i18n.map.sky_please);
                                                                                                          e.preventDefault();
                                                                                                          e.stopImmediatePropagation();
                                                                                                          window.location.href = "index.html#" + currentobj[0].previousPage;
                                                                                                          popPageOnToBackStack();
                                                                                                          $(that.el).undelegate();
                                                                                                          });
                                                        /* End of Back functionality */
                                                        sessionStorage.currentPage = "flightStatusDetails";
                                                        sessionStorage.From = "";
                                                        
                                                        statusResults = JSON.parse(sessionStorage.flightStatusObj);
                                                        $.i18n.map.statusResults=statusResults
                                                        var compiledTemplate = _.template(flightStatusDetailsTemplate,$.i18n.map);
                                                        
                                                        
                                                        
                                                        $(this.el).html(compiledTemplate);
                                                        //$('.main_header ul li:eq(1) img').on('click');
                                                        //$('#skypriority').text($.i18n.map.sky_priority);
                                                        setTimeout(function(){
                                                                   lineAdjustment();
                                                                   hideActivityIndicator();
                                                                   },1000);
                                                        
                                                        
                                                        return appView.prototype.render.apply(this, arguments);
                                                        
                                                        },
                                                        
                                                        events : {
                                                        'click #skypriority':'skyPriority',
                                                        'click #saveStatusFlight' : 'saveResults',
                                                        'click .getAirportDetails' : 'displaySelectedAirportDetails',
                                                        'touchstart *' : function() {
                                                        $.pageslide.close();
                                                        }
                                                        
                                                        },
                                                        /* function to skyPriority page details */
                                                        skyPriority : function(e) {
                                                        showActivityIndicatorForAjax('false');
                                                        window.networkStatCallBack = function(){
                                                        var netStatus =JSON.parse(localStorage.networkStatus);
                                                        /*If network is available,call the webservice*/
                                                        if("online" === netStatus.network){
                                                        var flightDetails,spDetails;
                                                        flightDetails=JSON.parse(sessionStorage.flightStatusObj);
                                                        
                                                        if($.type(flightDetails.details.flights) ==="array" ){
                                                        var connectionAirports=[];
                                                        $.each(flightDetails.details.flights,function(index,object){
                                                               if(index>0){
                                                               connectionAirports.push({transferairport:object.origin.airportCode,operatingcarrier:object.operatingAirlineCode});
                                                               if(flightDetails.details.flights.length === index+1 ){
                                                               spDetails.tripdestination=object.destination.airportCode;
                                                               spDetails.transferdetails=connectionAirports;
                                                               }
                                                               }
                                                               else{
                                                               spDetails={triporigin:object.origin.airportCode,operatingcarrier:object.operatingAirlineCode,transferdetails:{},tripdestination:object.destination.airportCode};
                                                               }
                                                               });
                                                        }
                                                        else{
                                                        var flightNo,flightSource,flightDestination,operatingAirline;
                                                        flightNo=flightDetails.details.flights.marketingFlightNumber;
                                                        flightSource=flightDetails.details.flights.origin.airportCode
                                                        flightDestination=flightDetails.details.flights.destination.airportCode
                                                        operatingAirline=flightDetails.details.flights.operatingAirlineCode;
                                                        spDetails={triporigin:flightSource,operatingcarrier:operatingAirline,transferdetails:[],tripdestination:flightDestination};
                                                        }
                                                        sessionStorage.skypriorityAdvancedDetails=JSON.stringify(spDetails);
                                                        
                                                        var obj1 = {};
                                                        obj1.flightData = sessionStorage.flightDetailsObject;
                                                        obj1.previousPage = "flightStatusDetails";
                                                        //pushPageOnToBackStack(obj1);
                                                        setTimeout(function(){getskyPriorityAdvancedDetailsAjax(obj1)},1000);
                                                        }
                                                        else
                                                        {
                                                        hideActivityIndicator();
                                                        openAlertWithOk($.i18n.map.sky_notconnected);
                                                        }
                                                        };
                                                        window.open(iSC.Network, "_self");
                                                        
                                                        },
                                                        
                                                        /* end of function to skyPriority page details*/
                                                        /*To save flight */
                                                        saveResults : function() {
                                                        sessionStorage.flightDetailsObject = sessionStorage.flightStatusObj;
                                                        sessionStorage.insertSaveFlightFrom = "FlightStatusDetails";
                                                        window.open(iSC.insertSaveFlight);
                                                        
                                                        },
                                                        /*End of save flight */
                                                        /*To display selected airport */
                                                        displaySelectedAirportDetails : function(e){
                                                        
                                                        sessionStorage.isFrom = "flightStatusDetails";
                                                        var pageObj={};
                                                        pageObj.previousPage="flightStatusDetails";
                                                        pageObj.isFrom=sessionStorage.previousPage;
                                                        pushPageOnToBackStack(pageObj);
                                                        window.networkStatCallBack = function(){
                                                        
                                                        if (localStorage.networkStatus != undefined) {
                                                        
                                                        netstat = JSON.parse(localStorage.networkStatus);
                                                        
                                                        if (netstat.network == "offline") {
                                                        openAlertWithOk($.i18n.map.sky_notconnected);popPageOnToBackStack();
                                                        }else{
                                                        
                                                        //	$('.flightdetails_view').css('display','none');
                                                        showActivityIndicatorForAjax('false');
                                                        var id = $(e.currentTarget).attr('id');
                                                        var airportcode = $('#' + id + ' li:eq(0) p').html();
                                                        var obj={};
                                                        sessionStorage.airportName = getAirportFullName(airportcode);
                                                        sessionStorage.airport_code= airportcode;
                                                        obj.airportName = getAirportFullName(airportcode);
                                                        obj.airportCode = airportcode;
                                                        obj.countryName = (getCountryName(airportcode)).split('_')[0];
                                                        obj.countryCode = (getCountryName(airportcode)).split('_')[1];
                                                        obj.airportDetails_URL =getAirportFinderURL(airportcode);
                                                        obj.cityName = getAirportName(airportcode);
                                                        sessionStorage.currentPage = "airportDetails";
                                                        sessionStorage.airportDetailsObj=JSON.stringify(obj);
                                                        
                                                        //	setTimeout(function(){
                                                        window.location.href = "index.html#airportDetails";
                                                        //               },1000);
                                                        }
                                                        }
                                                        
                                                        };
                                                        window.open(iSC.Network, "_self");
                                                        }
                                                        /*End of airport details */
                                                        });
       
       return flightStatusDetailsPageView;
       });
