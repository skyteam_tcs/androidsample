/* Airport Details Functionality */
var self;
define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'text!template/menuTemplate.html', 'text!template/airportsMVVM.html','models/flights/airportDetailsModel','appView','serviceInteraction'], function($, _, Backbone, utilities, constants, menuPageTemplate, airportsHTML,airportDetailsModel,appView,serviceInteraction) {

       var list = {};
       var netstat,self;
       var check = 0;
       var weather_Image={
       "01d": "Weather_Sunny_b",
       "02d": "Weather_PartlyCloud_d_b",
       "03d":"Weather_PartlyCloud_d_b" ,
       "04d":"Weather_PartlyCloud_d_b",
       "09d":"Weather_Rainy_b",
       "10d":"Weather_Rainy_b",
       "11d":"thunder_storms_b",
       "13d":"Weather_Snow_b",
       "50d":"Weather_Fog_b",
       "01n": "Weather_Sunny_b",
       "02n": "Weather_PartlyCloud_d_b",
       "03n":"Weather_Cloudy_b" ,
       "04n":"Weather_Cloudy_b",
       "09n":"Weather_Rainy_b",
       "10n":"Weather_Rainy_b",
       "11n":"thunder_storms_b",
       "13n":"Weather_Snow_b",
       "50n":"Weather_Fog_b",
       }
       var airportDetailsPageView = appView.extend({
                                                         id : 'flight-page',
                                                           transitionRequired:true,
                                                           backToAirportDetails:false,
                                                         initialize : function() {
                                                         $('#backButtonList').show();

                                                         $('#headingText img').hide();
                                                         $('#headingText h2').show();
                                                         $('#headingText h2').html($.i18n.map.sky_airportdetails);
                                                         if (sessionStorage.savedflights == "yes") {
                                                         $('#brief img').show();
                                                         $('#brief h2').hide();
                                                         sessionStorage.savedflights = "no";
                                                         }


                                                         var currentPage = JSON.parse(sessionStorage.previousPages);

                                                         if (currentPage[0].previousPage == "savedFlightsOrAirports") {
                                                         sessionStorage.isFromSaved = "yes";
                                                         $('#brief img').hide();
                                                         $('#brief h2').hide();
                                                         } else if (currentPage[0].previousPage == "FlightDetailsPage") {
                                                         if (currentPage[1].previousPage == "savedFlightsOrAirports") {
                                                         sessionStorage.isFromSaved = "yes";
                                                         $('#brief img').hide();
                                                         $('#brief h2').hide();
                                                         }
                                                         }

                                                         if($('#pageslide').children().length === 0)
                                                         {
                                                         $(".menuItems").pageslide();
                                                         var template = _.template(menuPageTemplate,$.i18n.map);
                                                         $('#pageslide').html(template);
                                                         sessionStorage.isMenuAdded = "YES";
                                                         }
                                                         self = this;
                                                         },

                                                         render : function() {
//             setTimeout(function() { }, 1000);

                                                         showActivityIndicator($.i18n.map.sky_please);
                                                   var dataModel = new airportDetailsModel();

                                                   if(hasValue(self.arguments)){

                                                   response = JSON.parse(self.arguments);
                                                   dataModel.set(response);
                                                   var objDetails={"AirportCode":sessionStorage.airport_code,"AirportDetails":response};
                                                   sessionStorage.AirportDetails_Response = JSON.stringify(objDetails);

                                                   }
                                                   else{
                                                   if(sessionStorage.getItem("AirportDetails_Response") !== null){
                                                   var cacheData = JSON.parse(sessionStorage.AirportDetails_Response);
                                                   if(sessionStorage.airport_code === cacheData.AirportCode){
                                                   dataModel.set(cacheData.AirportDetails);
                                                   this.backToAirportDetails = true;
                                                   }
                                                   }

                                                   }
                                                   dataModel.set({'AirportCode':sessionStorage.airport_code});
                                                   dataModel.resetData();
                                                   dataModel.set($.i18n.map);
                                                   this.model = dataModel;
                                                   var compiledTemplate = _.template(airportsHTML,dataModel.toJSON());
                                                   $(this.el).html(compiledTemplate);
                                                   /* checking the existence of hub image,If yes then changing the required image*/
                                                      if(_.contains(AIRPORT_HUB_IMAGES_LIST,this.model.get('AirportCode'))){
                                                            self.$el.find('.airports_image').css('background-image','url(images/hub_airports/Hub_'+self.model.get('AirportCode')+'.jpg)');
                                                      }
                                                   if (localStorage.getItem("savedFlightsAirports") !== null) {
                                                   var savedairports = JSON.parse(localStorage.savedFlightsAirports);
                                                   $.each(savedairports.SavedAirports_test, function(key) {
                                                          if (savedairports.SavedAirports_test[key].Location_Code == sessionStorage.airport_code) {
                                                          self.$el.find(".save_buttons").hide()
                                                          }
                                                          });
                                                   sessionStorage.savedAirportDetails = "no";
                                                   }

                                                         /* Back button click event */
                                                         $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                                                                   var currentobj;
                                                                   e.preventDefault();
                                                                    sessionStorage.navDirection = "Right";
                                                                   sessionStorage.goNativeMapFromDetails='YES';
                                                                    sessionStorage.movingToMap = "Yes";

                                                                   window.open("app:nativeMapOld","_self");
                                                                   setTimeout(function() {
                                                                              window.open("analytics://_trackPageview#airportSearchPage", "_self");
                                                                              }, 1000);


                                                                   $(self.el).undelegate();
                                                           });
                                                         /* End of back functionality */


                                                   /* End of back functionality */
                                                   if(this.transitionRequired){
                                                   //slideThePage(sessionStorage.navDirection);
                                                   this.transitionRequired = false;
                                                   if(!this.backToAirportDetails){//while visiting again airport details page
                                                   var endPointURL ='';
                                                   if(sessionStorage.getItem("airportDetailsObj") !== null){
                                                   var savedObj = JSON.parse(sessionStorage.airportDetailsObj);
                                                   endPointURL = savedObj.airportDetails_URL;
                                                   }
                                                   var requestData = requestBuilder(endPointURL);
                                                   showActivityIndicatorForAjax('false');
                                                   callService(requestData,self.serviceCallback);
                                                   }
                                                   else{hideActivityIndicator();
                                                   }
                                                   }
                                                   else{
                                                   hideActivityIndicator();
                                                   }



                                                 sessionStorage.airpor = "yes";
                                                 sessionStorage.isFromAirports = "yes";
                                                 sessionStorage.currentPage="airportDetailsViewViaMap";
                                                 return appView.prototype.render.apply(this, arguments);

                                                         },
                                                   serviceCallback:function(response){
                                                   self.arguments = response;
                                                   self.render();
                                                   },
                                                   events : {
                                                   'click #lounges' : 'openLoungeFinder',
                                                   'click #skyTips' : 'openSkyTips',
                                                   'click #fly_here' : 'openfromsearchFlights',
                                                   'click #fly_to' : 'opentosearchFlights',
                                                   'click #airport_save' : 'saveairport',
                                                   'click #weatherReport' : 'displayWeatherInfo',
                                                   'click #memberAirlines' : 'openMemberAirlines',
                                                   'click #skyPriority' : 'openSkyPriority',
                                                   'click #transportation' : 'openTransportation',
                                                   'click #airportmaps' : 'openAirportMaps',
                                                   'touchstart *' : function() {
                                                   $.pageslide.close();
                                                   }

                                                   },
                                                         openTransportation : function(e){
                            var obj1 = {};
              obj1.previousPage = "airportDetailsViewViaMap";
              pushPageOnToBackStack(obj1);
                                                   sessionStorage.detailsTransportation = JSON.stringify(self.model.get('Details'));
              window.location.href="index.html#transportationPage";
			},

			                                               openAirportMaps : function(e) {

                                                                                                                     
                                                                          e.preventDefault();
                                                                                                                        
                                                                         e.stopImmediatePropagation();
                                                                                                                       
                                                                         var obj1 = {};
                                                                                                                        
                                                                         obj1.previousPage = "airportDetails";
                                                                                                                        
                                                                        pushPageOnToBackStack(obj1);
                                                                                                                        
                                                                        var airportMapNative = "app:nativeAirportMap-"+sessionStorage.airport_code;
                                                                                                                       
                                                                        //alert(airportMapNative);
                                                                         window.open(airportMapNative,"_self");
                                                                        },




                                                         /* function to skyPriority page details */
                                                         openSkyPriority : function(e) {
                                                         sessionStorage.goNativeMapFromDetails="NO";
                                                         var obj1 = {};
                                                         obj1.previousPage = "airportDetailsViewViaMap";
                                                         pushPageOnToBackStack(obj1);
                                                         sessionStorage.skyPriorityAirlines = self.model.get('SkyPriorityAirlines');;
                                                        comingfromairportskypriority=1;
                                                         window.location.href="index.html#skyPriorityFinder"
                                                         },
                                                         /* end of function to skyPriority page details*/
                                                         /* To get into lounges page */
                                                         openLoungeFinder : function(e) {
                                                     //      alert(1);
                                                   e.preventDefault();
                                                    //   alert(2);
                                                   e.stopImmediatePropagation();
                                                   //    alert(3);
                                                   if(parseInt(self.model.get('LoungesCount')) >0){

                                                         var obj = {};
                                                           //  alert(4);
                                                         obj.previousPage = "airportDetailsViewViaMap";
                                                         obj.airportName = sessionStorage.airportName;
                                                         sessionStorage.goNativeMapFromDetails="NO";
                                                         sessionStorage.airport_newName=obj.airportName;
                                                           //  alert(5);
                                                         pushPageOnToBackStack(obj);
                                                           //  alert(6);
                                                         sessionStorage.airportCode = sessionStorage.airport_code;
                                                         sessionStorage.airportName = sessionStorage.airportName;
                                                         sessionStorage.lastPage = "";
                                                         sessionStorage.lounge = "airportDetailsViewViaMap";
                                                         sessionStorage.loungeFlag = 0;
                                                           //  alert(7);
                                                         if (sessionStorage.savedAirportDetails == "no") {
                                                         sessionStorage.savedAirportDetails = "yes";
                                                           //  alert(8);
                                                         } else {
                                                         sessionStorage.savedAirportDetails = "";
                                                         //  alert(9);
                                                         }
                                                         sessionStorage.airpor = "no";

                                                         var airportCode = $('.airports_temp ul li:eq(0) p').html();

  //  alert(10);
                                                       var obj = {};
                                                       obj.airportCode = self.model.get('AirportCode');
                                                       obj.url = URLS.LOUNGE_SEARCH+'airportcode='+obj.airportCode;
                                                       sessionStorage.loungesData = JSON.stringify(obj);
 //   alert(11);
                                                       showActivityIndicatorForAjax('true');
                                                   window.open(iSC.loungeFinder, "_self");
                                                    //   alert(12);
                                                   }
                                                   else{
                                                   openAlertWithOk($.i18n.map.sky_lounge_unavailable);
                                                   }

                                                         },
                                                         /** End of lounges **/

                                                         /** navigate to skytips page **/
                                                         openSkyTips : function(e) {
                                                           e.preventDefault();
                                                           e.stopImmediatePropagation();
                                                         if(parseInt(self.model.get('SkyTipsCount')) >0){
                                                         sessionStorage.airportFlag=1;
                                                         sessionStorage.theme = "no";
                                                         sessionStorage.goNativeMapFromDetails="NO";
                                                         var pageObj = {};
                                                         pageObj.airportObj=sessionStorage.airportDetailsObj;
                                                         pageObj.previousPage = "airportDetailsViewViaMap";
                                                         pushPageOnToBackStack(pageObj);
                                                         sessionStorage.airpor = "no";
                                                         sessionStorage.lastPage = "";
                                                         if (sessionStorage.history != "skytips") {
                                                         localStorage.skyTipsDetails = "";
                                                         }
                                                         if (sessionStorage.savedAirportDetails == "no") {
                                                         sessionStorage.savedAirportDetails = "yes";
                                                         } else {
                                                         sessionStorage.savedAirportDetails = "";
                                                         }
                                                         var airportCode = self.model.get('AirportCode');
                                                         var obj = {};
                                                         obj.airportCode = airportCode;
                                                         obj.url = URLS.SKYTIP_SEARCH+'airportcode=' + obj.airportCode;

                                                         sessionStorage.skytipsReqData = JSON.stringify(obj);
                                                         showActivityIndicatorForAjax('true');
                                                          if(sessionStorage.themeSelected != undefined){
                                                         sessionStorage.removeItem("themeSelected");
                                                         }
                                                         window.open(iSC.skytips,"_self");
                                                           }else{
                                                           openAlertWithOk($.i18n.map.sky_skymoment);
                                                           }
                                                         },
                                                         /* end of skytips */

                                                         /* to open search flights page with source airport */
                                                         openfromsearchFlights : function(e) {
                                                         var pageObj = {};
                                                         pageObj.airportObj=sessionStorage.airportDetailsObj;
                                                         pageObj.previousPage = "airportDetailsViewViaMap";
                                                         pushPageOnToBackStack(pageObj);
sessionStorage.goNativeMapFromDetails="NO";
                                                         sessionStorage.airpor = "no";
                                                         sessionStorage.lastPage = "";
                                                         if (sessionStorage.savedAirportDetails == "no") {
                                                         sessionStorage.savedAirportDetails = "yes";
                                                         } else {
                                                         sessionStorage.savedAirportDetails = "";
                                                         }
                                                         var airportDetail= JSON.parse(sessionStorage.airportDetailsObj);
                                                            var dataobj ={
                                                            Src:airportDetail.airportName+" ("+airportDetail.airportCode+"), "+airportDetail.countryName,
                                                            Dest:'',
                                                            isFrom:"roundtrip"
                                                            };
                                                            //setting the current day for departure
                                                            var now=new Date();
                                                            dataobj.TwoWayDep = now.yyyymmdd();

                                                            //setting the next day for arrival
                                                            now.setDate(now.getDate()+1);
                                                            dataobj.TwoWayArrival = now.yyyymmdd();

                                                            sessionStorage.searchData = JSON.stringify(dataobj);
                                                        comingfromflyhere=1;
                                                         sessionStorage.removeItem('Res');
                                                         window.location.href = 'index.html#searchFlights';

                                                         },
                                                         /* End of serach flights */

                                                         /* To open search flights with destination */
                                                         opentosearchFlights : function(e) {
                                                         var pageObj = {};
                                                         pageObj.airportObj=sessionStorage.airportDetailsObj;
                                                         pageObj.previousPage = "airportDetailsViewViaMap";
                                                         pushPageOnToBackStack(pageObj);
                                                         sessionStorage.goNativeMapFromDetails="NO";
                                                         sessionStorage.airpor = "no";
                                                         sessionStorage.lastPage = "";
                                                         if (sessionStorage.savedAirportDetails == "no") {
                                                         sessionStorage.savedAirportDetails = "yes";
                                                         } else {
                                                         sessionStorage.savedAirportDetails = "";
                                                         }
                                                         var airportDetail= JSON.parse(sessionStorage.airportDetailsObj);
                                                            var dataobj ={
                                                            Src:'',
                                                            Dest:airportDetail.airportName+" ("+airportDetail.airportCode+"), "+airportDetail.countryName,
                                                            isFrom:"roundtrip"
                                                            };
                                                            //setting the current day for departure
                                                            var now=new Date();
                                                            dataobj.TwoWayDep = now.yyyymmdd();

                                                            //setting the next day for arrival
                                                            now.setDate(now.getDate()+1);
                                                            dataobj.TwoWayArrival = now.yyyymmdd();

                                                            sessionStorage.searchData = JSON.stringify(dataobj);
                                                         sessionStorage.removeItem('Res');
                                                          comingfromflyto=1;

                                                         window.location.href = 'index.html#searchFlights';

                                                         },
                                                         /* end of search flights */

                                                         /* to save an airport */
                                                         saveairport : function(e) {
                                                   var save_airport = {};

                                                   var obj = JSON.parse(sessionStorage.airportDetailsObj);
                                                            save_airport.Airport_Name = obj.airportName;
                                                   			save_airport.Location_Code = obj.airportCode;
                                                   			save_airport.Country_Code = obj.countryCode;
                                                   			save_airport.City_Name = obj.cityName;
                                                   			save_airport.AirportDetails_URL = obj.airportDetails_URL;
                                                   			save_airport.Country_Name = obj.countryName;

                                                   var airportDetailsResponse =JSON.parse(sessionStorage.AirportDetails_Response).AirportDetails;
                                                   save_airport.LocalTime = airportDetailsResponse.LocalTime;
                                                   save_airport.Temperature = airportDetailsResponse.Temperature;
                                                   save_airport.WeatherCode = airportDetailsResponse.WeatherCode;
                                                   save_airport.LoungesCount = airportDetailsResponse.LoungesCount;
                                                   save_airport.SkyTipsCount = airportDetailsResponse.SkyTipsCount;
                                                   save_airport.AirlinesCount = airportDetailsResponse.AirlineDetails.Operating.Count;

                                                   sessionStorage.saveAirportReqData = JSON.stringify(save_airport);                                                         window.open(iSC.insertSavedAirport, "_self")
                                                         },
                                                         /* End of airport saving */

                                                         /* To display weather information */
                                                         displayWeatherInfo : function(e) {
                                                   e.preventDefault();e.stopImmediatePropagation();
                                                         var serviceCallBack = function(response){

                                                         sessionStorage.weatherDetailsResult = response;

                                                         var obj = {};
                                                         obj.previousPage = "airportDetailsViewViaMap";
                                                         pushPageOnToBackStack(obj);

                                                         sessionStorage.goNativeMapFromDetails="NO";
                                                         //sessionStorage.weatherfrom = "AirportDetails";
                                                         sessionStorage.lastPage = "";
                                                         sessionStorage.airpor = "yes";

                                                         if (sessionStorage.savedAirportDetails == "no") {
                                                         sessionStorage.savedAirportDetails = "yes";
                                                         } else {
                                                         sessionStorage.savedAirportDetails = "";
                                                         }
                                                               window.location.href = 'index.html#weatherDetails';
                                                         }
                                                        showActivityIndicatorForAjax('false');
                                                         setTimeout(function(){
                                                              weatherDetailsService(serviceCallBack);
                                                         },1);
                                                         },
                                                         /* End of display weather info */

                                                         /* Display skyteam member airlines */
                                                         openMemberAirlines : function(e) {
                                                   e.preventDefault();
                                                   e.stopImmediatePropagation();
                                                   sessionStorage.airlines =JSON.stringify(self.model.get('AirlineDetails'));

                                                       sessionStorage.savedflights = "yes";
                                                       sessionStorage.lastPage = "";
                                                       if (sessionStorage.savedAirportDetails == "no") {
                                                       sessionStorage.savedAirportDetails = "yes";
                                                       } else {
                                                       sessionStorage.savedAirportDetails = "";
                                                       }
                                                       sessionStorage.members = "AirportDetails";
                                                       if (parseInt(self.model.get('AirlineDetails').Operating.Count) >0 ) {
                                                       var obj = {};
                                                       obj.previousPage = "airportDetailsViewViaMap";
                                                       pushPageOnToBackStack(obj);
                                                       sessionStorage.goNativeMapFromDetails="NO";
                                                       window.location.href = 'index.html#aboutSkyTeam';

                                                       } else {
                                                       openAlertWithOk($.i18n.map.sky_sorry);
                                                       }                                                         }
                                                         /* end of member airlines */
                                                         });
       return airportDetailsPageView;

       });
/* to display selected airport details */