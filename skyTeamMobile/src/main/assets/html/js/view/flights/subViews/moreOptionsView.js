/*  Functionality for filter options  */
define(['jquery',
        'backbone',
        'underscore',
        'touchpunch',
        'view/flights/searchFlightView',
        'text!template/moreOptionsTemplate.html'
        ], function($, Backbone, _,touchPunch,searchFlightView,optionsTemplate) {
       var that;
       var searchResultsView;
       var position1;
       var position2;
       var checkedAirline =[];
       var previousList =[];
       var isprev7daysScheduleShown = "no";//flag to track is 7 days schedule is shown before opening the filter
       var flightPageView = Backbone.View.extend({
                                                 el : '#skyteam',
                                                 initialize : function() {
                                                 that=this;
                                                 },
                                                 events:{
                                                 'click #moreOptions-ok':'closePopUp',
                                                 'click  #dropdown' : 'openOperators',
                                                 'click  #moreOptions-airlineok' : 'closeOperators',
                                                 'click .airlineName' : 'checkSelectedAirline',
                                                 'click .scheduleChkBox' : 'checkScheduleCheckBox',
                                                 'click .interlineChkBox' : 'checkInterlineCheckBox',
                                                 'click  .groupCheckbox' : 'deselectcheck',
                                                 'click  #select' : 'selectAllCheckboxes',
//                                                 filter changes
                                                 'click #moreOptions-cancel':'closeFilter',
//                                                 filter changes
                                                 },
                                                 render : function(parentView) {

                                                 searchResultsView = parentView;
                                                 var currentJourney = searchResultsView.model.get('currentTravel');
                                                 if(currentJourney === "Return"){//return
                                                 if(searchResultsView.model.get('returnFlights').Count > 0){
                                                 previousList = searchResultsView.model.get('returnFlights').trips;
                                                 }
                                                 else{
                                                 previousList = searchResultsView.model.get('schedules').returnSchedules.trips;
                                                 }
//                                                 filter changes
//                                                 sessionStorage.show7DaysArrival = "no";//resetting the seven days schedule
//filter changes
                                                 }
                                                 else{// Onward (default)
                                                 if(searchResultsView.model.get('departureFlights').Count > 0){
                                                 previousList = searchResultsView.model.get('departureFlights').trips;
                                                 }
                                                 else{
                                                 previousList = searchResultsView.model.get('schedules').onwardSchedules.trips;
                                                 }
//                                                 filter changes
//                                                 sessionStorage.show7DaysDeparture = "no";//resetting the seven days schedule
//                                                 filter changes
                                                 }

                                                 var moreOptionsTemplate = _.template(optionsTemplate,$.i18n.map);
                                                 $(this.el).append(moreOptionsTemplate);
                                                 $('#blackshade').show();
                                                 //defaultly all airlines are selected
                                                 checkedAirline = ["SU", "AR", "AM", "UX", "AF", "AZ", "CI", "MU", "CZ", "OK", "DL", "GA", "KQ", "KL", "KE", "ME", "SV", "RO", "VN", "MF"]

                                                 /* Number of stops slider function */
                                                 this.$el.find("#slider").slider({
                                                                                 value : 0.5,
                                                                                 min : 0.5,
                                                                                 max : 1.7,
                                                                                 step : 0.4,
                                                                                 range : "min",
                                                                                 });
                                                 this.$el.find( "#slider" ).slider( "value", 1.7 );
                                                 /* End of function */
                                                 /* Time slider function */
                                                 this.$el.find("#slider2").slider({
                                                                                  range : true,
                                                                                  min : 0,
                                                                                  max : 1490,
                                                                                  values : [0, 1490],
                                                                                  slide :that.create,
                                                                                  change:that.getTime,
                                                                                  });
                                                 this.$el.find("#slider2").on( "mousemove", function( event, ui ) {
                                                                              var value = that.$el.find("#slider2").slider( "option", "values" );
                                                                              var hours0 = parseInt(value[0] / 60 , 10);
                                                                              var hours1 = parseInt(value[1] / 60 , 10);
                                                                              if (hours0 == 0) {hours0 = 0;}
                                                                              if (hours0 > 12) {hours0 = hours0;}
                                                                              if (hours1 == 0) {hours1 = 0;}
                                                                              if (hours1 > 12) {hours1 = hours1;}
                                                                              var $left_slider_reference = $(that.$el.find("#slider2 a")[0]),
                                                                              $right_slider_reference = $(that.$el.find("#slider2 a")[1]);
                                                                              var left_slider_position = ($left_slider_reference.position().left),
                                                                              right_slider_position = ($right_slider_reference.position().left);
                                                                              var time1 = document.getElementById("time1");
                                                                              var time2 = document.getElementById("time");
                                                                              time1.innerHTML = hours0+":"+"00";
                                                                              time1.style.left = left_slider_position - 45;
                                                                              time2.innerHTML = hours1+":"+"00";
                                                                              time2.style.left = right_slider_position;
                                                                              } );

                                                 position1 = this.$el.find("#slider2").children("a:nth-child(2)").position();
                                                 this.$el.find("#time1").css("left",position1.left-45);
                                                 this.$el.find("#time1").text("00"+":"+"00");
                                                 position2 = this.$el.find("#slider2").children("a:nth-child(3)").position();
                                                 this.$el.find("#time").css("left",position2.left-0);
                                                 this.$el.find("#time").text("24"+":"+"00");
                                                 var currentJourney = searchResultsView.model.get('currentTravel');
                                                 if(currentJourney === "Return" && sessionStorage.show7DaysArrival === "yes"){
                                                 this.$el.find('#schedule').prop('checked',true);
                                                 }
                                                 else if(currentJourney === "Onward" && sessionStorage.show7DaysDeparture === "yes"){
                                                 this.$el.find('#schedule').prop('checked',true);
                                                 }
/******* interline */
                                                 if(sessionStorage.isinterline == "yes"){
                                                 this.$el.find('#interline').prop('checked',true);
                                                 }
/******* interline */
                                                  if(currentJourney === "Return"){
                                                  if (sessionStorage.slider_return_stops != undefined && sessionStorage.slider_return_stops != "") {

                                                  that.$el.find("#slider").slider("value",sessionStorage.slider_return_stops);
                                                  }

if (sessionStorage.slider_return_time1 != undefined && sessionStorage.slider_return_time1 != "" && sessionStorage.slider_return_time0 != undefined && sessionStorage.slider_return_time0 != "" ) {

that.$el.find("#slider2").slider( "values", 0,sessionStorage.slider_return_time0 )
that.$el.find("#slider2").slider( "values", 1,sessionStorage.slider_return_time1 )
}


                                                  }else{

                                                  if (sessionStorage.slider_onward_stops != undefined && sessionStorage.slider_onward_stops != "") {

                                                  that.$el.find("#slider").slider("value",sessionStorage.slider_onward_stops);
                                                  }

                                                  if (sessionStorage.slider_onward_time1 != undefined && sessionStorage.slider_onward_time1 != "" && sessionStorage.slider_onward_time0 != undefined && sessionStorage.slider_onward_time0 != "" ) {

that.$el.find("#slider2").slider( "values", 0,sessionStorage.slider_onward_time0 )
that.$el.find("#slider2").slider( "values", 1,sessionStorage.slider_onward_time1 )
                                                  }
                                                  }



                                                 that.$el.find('.airlineName').off('click').on('click',that.checkSelectedAirline);

                                                 },





                                                 closePopUp:function(e){
                                                 e.preventDefault();
                                                 e.stopImmediatePropagation();
                                                 var list =[];
                                                 var maxTime, minTime;
                                                 var currentJourney = searchResultsView.model.get('currentTravel');
                                                 var isShowSevendaySchedule = that.$el.find('#schedule').prop('checked');
                                                 if(isShowSevendaySchedule){//all day flights needs to shown

                                                 if(currentJourney === "Return"){
                                                 list = searchResultsView.model.get('schedules').returnSchedules.trips;
                                                 sessionStorage.show7DaysArrival = "yes";
                                                 }
                                                 else{//"Onward journey is default"
                                                 list = searchResultsView.model.get('schedules').onwardSchedules.trips;
                                                 sessionStorage.show7DaysDeparture = "yes";
                                                 }
                                                 }
                                                 else{// user selected journey flights needs to be shown
                                                 if(currentJourney === "Return"){
                                                 if(searchResultsView.model.get('returnFlights').Count > 0){
                                                 list = searchResultsView.model.get('returnFlights').trips;
                                                 }
                                                 else{
                                                 list = searchResultsView.model.get('schedules').returnSchedules.trips;
                                                 }
                                                 //filter changes
                                                 sessionStorage.show7DaysArrival = "no";
                                                 //filter changes
                                                 }
                                                 else{//"Onward journey is default"
                                                 if(searchResultsView.model.get('departureFlights').Count > 0){
                                                 list = searchResultsView.model.get('departureFlights').trips;
                                                 }
                                                 else{
                                                 list = searchResultsView.model.get('schedules').onwardSchedules.trips;
                                                 }
                                                 //filter changes
                                                 sessionStorage.show7DaysDeparture = "no";
                                                 //filter changes
                                                 }
                                                 }
                                                 searchResultsView.model.set({'currentList':list});
                                                 var filteredList =[];
                                                 if(checkedAirline.length >0){
                                                 /* no need to filter for,when all airlines are selected (or) nothing selected */
                                                 if(checkedAirline.length!==20 && checkedAirline.length >0){
                                                 for(var i=0,airlineCount = checkedAirline.length;i<airlineCount;i++){
                                                 var airlineCode = checkedAirline[i];
                                                 var currentList =[];
                                                 currentList = $.grep(list,function(currentList,ind){
                                                                      if(currentList.flights.length === 1){//Single segment
                                                                      return currentList.flights[0].marketingFlightNumber.substring(0, 2) === airlineCode;
                                                                      }
                                                                      else{//Multiple Segment
                                                                      for(var j=0,segmentLength = currentList.flights.length;j<segmentLength;j++){
                                                                      if(currentList.flights[j].marketingFlightNumber.substring(0, 2) === airlineCode)//if airline matched in any segment
                                                                      return true;
                                                                      }
                                                                      return false;//if no airline has matched
                                                                      }
                                                                      });
                                                 filteredList = $.merge(filteredList,currentList);
                                                 }
                                                 }
                                                 else if(checkedAirline.length === 20){//if all airline has selected
                                                 filteredList = list;
                                                 }
                                                 //searchResultsView.model.set({'flightList':filteredList});



                                                 var noOfStops = 5;//maximum stops(no need to filter);

                                                 maxTime = parseInt(that.$el.find('#time').text().split(':')[0]);
                                                 minTime = parseInt(that.$el.find('#time1').text().split(':')[0]);


                                                 filteredList = $.grep(filteredList,function(currentList,ind){
                                                                       return (parseInt(currentList.flights[0].flightLegs[0].departureDetails.scheduledTime.split(":")[0]) >= minTime) && (parseInt(currentList.flights[0].flightLegs[0].departureDetails.scheduledTime.split(":")[0]) <= maxTime)
                                                                       });


                                                 if (that.$el.find("#slider").slider("value") == 0.5) {
                                                 noOfStops = 0;
                                                 sessionStorage.slider_stops = 0.5;
                                                 } else if (that.$el.find("#slider").slider("value") == 0.9) {
                                                 noOfStops = 1;
                                                 sessionStorage.slider_stops = 0.9;
                                                 } else if (that.$el.find("#slider").slider("value") == 1.3) {
                                                 sessionStorage.slider_stops = 1.3;
                                                 noOfStops = 2;
                                                 } else if (that.$el.find("#slider").slider("value") == 1.7) {
                                                 sessionStorage.slider_stops = 1.7;
                                                 noOfStops = 5;//maximum stops(no need to filter)
                                                 }

                                                 if(currentJourney === "Return"){
                                                 sessionStorage.slider_return_stops=sessionStorage.slider_stops
                                                 var value = $("#slider2").slider( "option", "values" );
                                                 sessionStorage.slider_return_time0=value[0];
                                                 sessionStorage.slider_return_time1=value[1];

                                                 }else{
                                                 sessionStorage.slider_onward_stops=sessionStorage.slider_stops
                                                 var value = $("#slider2").slider( "option", "values" );
                                                 sessionStorage.slider_onward_time0=value[0];
                                                 sessionStorage.slider_onward_time1=value[1];

                                                 }


                                                 if(noOfStops < 5){
                                                 filteredList = $.grep(filteredList,function(currentList,ind){
                                                                       return (parseInt(currentList.totalNoTransfers) <= noOfStops);
                                                                       });
                                                 }
                                                 }

                                                 if(filteredList.length >0){
                                                 searchResultsView.model.set({'currentList':filteredList});

                                                 searchResultsView.bindFlightListTemplate(filteredList);
                                                 }
                                                 else{//if filtered list is has 0 zero flights
                                                 if(currentJourney === "Return"){
                                                 sessionStorage.show7DaysArrival = "no";
                                                 }
                                                 else{//"Onward journey is default"
                                                 sessionStorage.show7DaysDeparture = "no";
                                                 }
                                                 searchResultsView.bindFlightListTemplate(previousList);

                                                 openAlertWithOk($.i18n.map.sky_selectedcriteria);
                                                 }



 /******* interline */

                                                 var isinterlineselected=sessionStorage.isinterline;
                                                 if (isinterlineselected === undefined || isinterlineselected === null) {
                                                     isinterlineselected="no";
                                                 }
                                                 var isinterline = that.$el.find('#interline').prop('checked');
                                                 if(isinterline){
                                                 if(sessionStorage.isinterline=="no"){
                                                 sessionStorage.slider_return_stops="";
                                                                                                  sessionStorage.slider_onward_stops="";
                                                                                                  sessionStorage.slider_return_time0="";
                                                                                                  sessionStorage.slider_return_time1="";
                                                                                                  sessionStorage.slider_onward_time0="";
                                                                                                  sessionStorage.slider_onward_time1="";
                                                                                                  checkedAirlineStoring_onward = [];
                                                                                                  checkedAirlineStoring_return = [];
                                                 }

                                                 sessionStorage.isinterline = "yes";

                                                 }else{
                                                 if(sessionStorage.isinterline=="yes"){
                                                                                                  sessionStorage.slider_return_stops="";
                                                                                                                                                   sessionStorage.slider_onward_stops="";
                                                                                                                                                   sessionStorage.slider_return_time0="";
                                                                                                                                                   sessionStorage.slider_return_time1="";
                                                                                                                                                   sessionStorage.slider_onward_time0="";
                                                                                                                                                   sessionStorage.slider_onward_time1="";
                                                                                                                                                   checkedAirlineStoring_onward = [];
                                                                                                                                                   checkedAirlineStoring_return = [];
                                                                                                  }

                                                 sessionStorage.isinterline = "no";
                                                 }
                                                 if(isinterlineselected!=sessionStorage.isinterline){
                                                 var dataModelsearchFlightView = new searchFlightView();
                                                               dataModelsearchFlightView.searchResults(e);
                                                               wait();
                                                               }


/******* interline */
 $('.moreOptDiv').remove();
                                                 $("#blackshade").hide();
                                                 },

//filter changes

                                                 closeFilter:function(e){

                                                                                                         $('.moreOptDiv').remove();
                                                                                                         $("#blackshade").hide();
                                                                                                  },
//filter changes

                                                 create :function(event,ui){
                                                 var time = null;
                                                 },

                                                 getTime : function(hours0,  hours1) {
                                                 var value = $("#slider2").slider( "option", "values" );
                                                 hours0 = parseInt(value[0] / 60 , 10);
                                                 hours1 = parseInt(value[1] / 60 , 10);
                                                 if (hours0 == 0) {hours0 = 0;}
                                                 if (hours0 > 12) {hours0 = hours0  }
                                                 if (hours1 == 0) {hours1 = 0;}
                                                 if (hours1 > 12) {hours1 = hours1  }
                                                 position1 = $("#slider2").children("a:nth-child(2)").position();
                                                 $("#time1").css("left",position1.left-45);
                                                 $("#time1").text(hours0+":"+"00");
                                                 position2 = $("#slider2").children("a:nth-child(3)").position();
                                                 $("#time").css("left",position2.left);
                                                 $("#time").text((hours1)+":"+"00");
                                                 that.checkMax(position1,position2);
                                                 that.create;
                                                 },

                                                 checkMax:function(pos1,pos2){
                                                 $("#sliderRange").position('value',pos1,pos2);
                                                 },
                                                 openOperators : function(e) {
                                                 e.preventDefault();
                                                 e.stopImmediatePropagation();
                                                 that.$el.find('.proferedline-popup').show();
                                                 //that.$el.find('#operator-ok').show();
                                                 that.$el.find('#select').prop("checked", true);
                                                 that.$el.find('.groupCheckbox').prop("checked", true);


                                                  var currentJourney = searchResultsView.model.get('currentTravel');
                                                   if(currentJourney === "Return"){
                                                   if(checkedAirlineStoring_return.length>0)
                                                                                                    {
                                                                                                   that.$el.find('.groupCheckbox').prop("checked", false);
                                                                                                    that.$el.find('#select').prop("checked", false);
                                                                                                    }
                                                                                                    that.$el.find('.scrollforoperators :not(:checked)').each(function() {
                                                                                                    for(var i = 0; i < checkedAirlineStoring_return.length; i++) {
                                                                                                    if (checkedAirlineStoring_return[i]==$(this).val()){
                                                                                                      $(this).prop("checked", true);

                                                                                                     } }
                                                                                                     });
                                                   }else{

                                                 if(checkedAirlineStoring_onward.length>0)
                                                 {
                                                 that.$el.find('#select').prop("checked", false);
                                                that.$el.find('.groupCheckbox').prop("checked", false);
                                                 }
                                                 that.$el.find('.scrollforoperators :not(:checked)').each(function() {
                                                 for(var i = 0; i < checkedAirlineStoring_onward.length; i++) {
                                                 if (checkedAirlineStoring_onward[i]==$(this).val()){
                                                   $(this).prop("checked", true);

                                                  } }
                                                  });
                                                  }





                                                 },
                                                 closeOperators : function(e) {
                                                 e.preventDefault();
                                                 e.stopImmediatePropagation();

                                                 checkedAirline = [];//resetting the checked airline count

                                                 that.$el.find('.scrollforoperators :checked').each(function() {
                                                                                                    checkedAirline.push($(this).val());
                                                                                                    });


                                                  var currentJourney = searchResultsView.model.get('currentTravel');
                                                 if(currentJourney === "Return"){
                                                 checkedAirlineStoring_return=checkedAirline;
                                                 }else{
                                                 checkedAirlineStoring_onward=checkedAirline;
                                                  }


                                                 that.$el.find('.proferedline-popup').hide();
                                                 //that.$el.find('#operator-ok').hide();

                                                 },
                                                 /* function to check the checkbox(theme) when corresponding label(theme name) is tapped */
                                                 checkSelectedAirline: function(e) {
                                                 e.preventDefault();
                                                 e.stopImmediatePropagation();
                                                 var forid ='#' + that.$el.find(e.target).attr('for');
                                                 if ( that.$el.find(forid).prop('checked') ) {
                                                 that.$el.find(forid).prop("checked", false);
                                                 that.$el.find('#select').prop('checked', false);
                                                 } else {
                                                 that.$el.find(forid).prop("checked", true);
                                                 var totalAirlinesCount = that.$el.find('.groupCheckbox').length;
                                                 var selectedAirlinesCount =that.$el.find('.groupCheckbox:checked').length;
                                                 if(totalAirlinesCount === selectedAirlinesCount){//if all airlines checked then,checking the select all
                                                 that.$el.find('#select').prop("checked", true);}
                                                 }
                                                 if ( forid === '#select' ) {
                                                 if (that.$el.find('#select').prop("checked") == true) {
                                                 that.$el.find('.groupCheckbox').prop("checked", true);
                                                 } else {

                                                 that.$el.find('.groupCheckbox').prop("checked", false);
                                                 }
                                                 }
                                                 },

                                                 checkScheduleCheckBox: function(e) {/* 7 days schedule */
                                                 e.preventDefault();
                                                 e.stopImmediatePropagation();

                                                 var forid ='#' + that.$el.find(e.target).attr('for');
                                                 if ( that.$el.find(forid).prop('checked') ) {
                                                 that.$el.find(forid).prop("checked", false);
                                                 } else {
                                                 that.$el.find(forid).prop("checked", true);
                                                 }
                                                 },
                                                 /******* interline */
                                                  checkInterlineCheckBox: function(e) {

                                                                                                  e.preventDefault();
                                                                                                  e.stopImmediatePropagation();
                                                                                                  var forid ='#' + that.$el.find(e.target).attr('for');
                                                                                                  if ( that.$el.find(forid).prop('checked') ) {
                                                                                                  that.$el.find(forid).prop("checked", false);
                                                                                                  } else {
                                                                                                  that.$el.find(forid).prop("checked", true);
                                                                                                  }
                                                                                                  },
                                                                                                  /******* interline */

                                                 deselectcheck : function() {
                                                 that.$el.find('#select').prop("checked", false);
                                                 },
                                                 /*For selecting all airlines in prefered airline popup */
                                                 selectAllCheckboxes : function(e) {
                                                 if (that.$el.find('#select').prop("checked") == true) {
                                                 that.$el.find('.groupCheckbox').prop("checked", true);
                                                 } else {
                                                 that.$el.find('.groupCheckbox').prop("checked", false);
                                                 }
                                                 },
                                                 /* End of function */
                                                 });
       return flightPageView;
       });

        /******* interline */
function wait(){
  if (sessionStorage.successindicator==0){
    setTimeout(wait,100);
  } else {
    var newFragment = Backbone.history.getFragment($(this).attr('href'));
                           if (Backbone.history.fragment == newFragment) {
                               // need to null out Backbone.history.fragement because
                               // navigate method will ignore when it is the same as newFragment
                               Backbone.history.fragment = null;
                               Backbone.history.navigate(newFragment, true);
                           };
                           if(($('#outboundFlightsBtn').css('display') == 'block') && ($('#returnFlightsBtn').css('display') == 'block'))
                           {
$('#returnFlightsBtn').trigger('click');
                           }
  }
}
 /******* interline */