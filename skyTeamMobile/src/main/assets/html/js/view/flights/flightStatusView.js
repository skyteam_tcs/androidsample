define(['jquery', 'backbone', 'underscore', 'text!template/flightStatus.html', 'text!template/menuTemplate.html','appView','text!template/flightStatusPopup.html'], function($, Backbone, _, flightstatusTemplate, menuPageTemplate,appView,flightStatusPopup) {
	//var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
       var airlines = ["AF", "AM", "AR", "AZ", "CI", "CZ", "DL", "KE", "KL", "KQ", "ME", "MF", "MU", "OK", "RO", "SU", "SV", "UX", "VN"];
	var getStatusOf = "flightNumber";
	var keyval = "";
    var self,names;
    var AirportNames;
    var today_date;
    var memberSelected='';
    var isCancelBtnClicked=false;
    var strKeyboardLang="";
    var srcAirportCode, destAirportCode;
       var popupView;
       var srcLocationType;
       var desLocationType;
     
       sessionStorage.airports_Depdate = "";
       sessionStorage.flightNumber_Depdate = "";
       
       var statusPopupModel = Backbone.Model.extend({
                defaults:{
                    'yes_date':'',
                    'today_date':'',
                    'tmrw_date':'',
                    'yes_date_val':'',
                    'today_date_val':'',
                    'tmrw_date_val':'',
                                                    
                },
                resetData:function(){
                    var d = new Date();
                    this.set({'today_date' : getDateInLocaleFormat(d)});
                    this.set({'today_date_val' : this.get('sky_today') + " " +this.get('today_date') + "/" + d.yyyymmdd()});
                                                    
                    var yes = new Date();
                    yes.setDate(yes.getDate() - 1);
                    this.set({'yes_date':getDateInLocaleFormat(yes)});
                    this.set({'yes_date_val' : this.get('sky_yesterday')+ " " +this.get('yes_date')+ "/" + yes.yyyymmdd()});
       
                    var tmrw = new Date();
                    tmrw.setDate(tmrw.getDate() + 1);
                   this.set({'tmrw_date' : getDateInLocaleFormat(tmrw)});
                   this.set({'tmrw_date_val': this.get('sky_tomorrow')  + " "  +this.get('tmrw_date')+ "/" + tmrw.yyyymmdd()});
                }
       });
       
    /* End of departure date & airline popup*/
       var flightStatusPopView = Backbone.View.extend({
                                               el : '#skyteam',
                                               initialize : function() {
                                               popupView=this;
                                               },
                                               events:{
                                               'click .depDate-ok' : 'closeDeparturePopup',
                                               'click #airline-ok':'closeAirlineList',
                                              'click .airlineName' : 'checkSelectedAirlineOrDepartDate',
                                              'click .spanDepartDate' : 'checkSelectedAirlineOrDepartDate',
                                               },
                                               render:function(){
                                               this.model = new statusPopupModel($.i18n.map),
                                               this.model.resetData();
                                               this.template = _.template(flightStatusPopup,this.model.toJSON());
                                               
                                               
                                               },
                                               openDatePopUP:function(e){
                                                      
                                                      $('#blackshade').show();
                                                      popupView.$el.find('.proferedline-popup').remove();
                                                      $(popupView.el).append(popupView.template);
                                                      var previousDate;
                                                      if(getStatusOf === "flightNumber"){
                                                          if(hasValue(sessionStorage.flightNumber_Depdate)){
                                                                previousDate = sessionStorage.flightNumber_Depdate;
                                                          }
                                                          else{//default value current date
                                                          previousDate = popupView.model.get('today_date_val');
                                                          }
                                                      }
                                                      else{//airports
                                                          if(hasValue(sessionStorage.airports_Depdate)){
                                                          previousDate = sessionStorage.airports_Depdate;
                                                             }
                                                          else{//default value current date
                                                          previousDate = popupView.model.get('today_date_val');
                                                          }
                                                      }
                                                      
                                                      
                                                      popupView.$el.find('.departureDate  input[value="' + previousDate + '"]').prop('checked', true);
                                                      popupView.$el.find('#depatureDate-popup').show();
                                                      },
                                               closeDeparturePopup:function(e){
                                               e.preventDefault();
                                               e.stopImmediatePropagation();
                                                      sessionStorage.FS_departureDate = popupView.$el.find('.departureDate :checked').val();
                                                      if(getStatusOf === "flightNumber"){
                                                      sessionStorage.flightNumber_Depdate = sessionStorage.FS_departureDate;
                                                      }
                                                      else{
                                                      sessionStorage.airports_Depdate = sessionStorage.FS_departureDate;
                                                      }
                                               popupView.$el.find('.departureListLi p').html(sessionStorage.FS_departureDate.split("/")[0]);
                                               popupView.$el.find('#depatureDate-popup').remove();
                                               $('#blackshade').hide();
                                               },
                                                  openAirlineList:function(e){
                                                  $('#blackshade').show();
                                                      popupView.$el.find('.proferedline-popup').remove();
                                                  $(popupView.el).append(popupView.template);
                                                      if(hasValue(memberSelected)){
                                                      popupView.$el.find('.scrollforflightsname  input[value="' + memberSelected + '"]').prop('checked', true);
                                                      }
                                                      
                                                  popupView.$el.find('#members-popup').show();
                                                  },
                                                  closeAirlineList:function(e){
                                                  e.preventDefault();
                                                  e.stopImmediatePropagation();
                                                  
                                                   
                                                      if(popupView.$el.find('.scrollforflightsname :checked').val() != memberSelected){
                                                      popupView.$el.find('#airport_flightNumber').val('');
                                                      popupView.$el.find(".cancel_flightNumber").css("visibility", "hidden");
                                                      }
                                                      memberSelected = popupView.$el.find('.scrollforflightsname :checked').val();
                                                      popupView.$el.find('#airlinesList p').html($.i18n.map['sky_'+memberSelected]);
                                                      popupView.$el.find('#airlinesListValue').html($('.scrollforflightsname :checked').val());
                                                      popupView.$el.find('#airlinesList').css('border', 'none');
                                                      popupView.$el.find('#airlinesListValue').css('border','none');
                                                      
                                                      
                                                      
                                                      
                                                      
                                                  popupView.$el.find('#members-popup').remove();
                                                  $('#blackshade').hide();
                                                  },
                                                  checkSelectedAirlineOrDepartDate: function(e)
                                                  {
                                                      e.preventDefault();
                                                      e.stopImmediatePropagation();
                                                  var forid = '#' + $(e.target).attr('for');
                                                  $(forid).prop("checked", true);
                                                  
                                                  
                                                  }
                                               });
       /* End of departure date & airline popup*/
       
       
       
       popupView = new flightStatusPopView();
       
       
       
       

       
	var flightstatusView = appView.extend({
		
		id : 'flightstatus-page',

		initialize : function() {
			 $('#backButtonList').hide();
                                                   $('#headingText h2').css('margin-right','');
                                                $('#headingText h2').css('margin-right','');
                                                
			if (sessionStorage.previousPages != undefined) {
				if (JSON.parse(sessionStorage.previousPages).length == 0) {
					sessionStorage.removeItem('previousPages');
				}
			}
			$('#headingText img').css('display', 'none');
			$('#headingText h2').css('display', 'block');
			$('#headingText h2').html($.i18n.map.sky_flight_status);

			if (sessionStorage.savedflights == "yes") {
				$('#brief img').css('display', 'block');
				$('#brief h2').css('display', 'none');

			}
			if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
				$('.main_header ul li:eq(0) img').remove();
				$('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

			}
			//$(".menuItems").pageslide();
            memberSelected='';
                                                if($('#pageslide').children().length === 0)
                                                {
                                                $(".menuItems").pageslide();
                                                var template = _.template(menuPageTemplate,$.i18n.map);
                                                $('#pageslide').html(template);
                                                sessionStorage.isMenuAdded = "YES";
                                                }
                                                
            if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
                hideActivityIndicator();
            }
            else
            {
                window.getAirportsCallback = function() {
                    hideActivityIndicator();
                };
                window.open(iSC.getAirports);
            }
                                          self =this;
		},

		render : function() {
			hideActivityIndicator();
            popupView.render();
			 /*Back button functionality */
			$('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
				 sessionStorage.isFromHome="";
		
				 e.preventDefault();
		         e.stopImmediatePropagation();
                                                              if("block" === $('#pageslide').css('display')){
                                                              $.pageslide.close();
                                                              }
                                                              else{
                                                              if(undefined != sessionStorage.previousPages){
                                                              var currentobj =  JSON.parse(sessionStorage.previousPages);
                                                              if("homePage" === currentobj[0].previousPage)
                                                              {
                                                              window.location.href = "index.html#homePage";
                                                              popPageOnToBackStack();
                                                              }

                                                              }
                                                              }

			});
			/* End of Back functionality */
            sessionStorage.skyTipsAirportName = "";
			sessionStorage.From = "Menu";
			sessionStorage.currentPage = "flightstatus";
			var locationPath = window.location;
			locationPath = locationPath.toString();
			var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
			
		

			if (spath == "flightstatus" && sessionStorage.navToflightStatus == "yes") {
                sessionStorage.isFromHome="";
				$(".menuItems").trigger('click');
				sessionStorage.navToflightStatus = "no";
				sessionStorage.removeItem('flightNo');
				sessionStorage.removeItem('searchairports');
				sessionStorage.removeItem('statusFor');

			}
            if (sessionStorage.isFromHome == "yes") {
                    $('#backButtonList').show();
            }
			$.i18n.map.airlines=airlines;
            var compiledTemplate = _.template(flightstatusTemplate,$.i18n.map);

			$(this.el).html(compiledTemplate);
        var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7"){
        (self.$el.find('.airports_info ul li').css('margin-top','10px'));
        self.$el.find('.datecalender button').css('margin-top','10px');
        }
        }
        }
        else if(sessionStorage.deviceInfo == "android"){
               if ((screen.width == 480) && (screen.height == 800)) {
				  self.$el.find('#airlinesList').css('margin-top','10px');
                  self.$el.find('.datecalender button').css('margin-top','10px');
				}
             }
                                                
			if (hasValue(sessionStorage.flightNo)) {
				self.$el.find('#airport_flightNumber').val(sessionStorage.flightNo);
			}
                                             
            if(hasValue(sessionStorage.airline)){
              self.$el.find('#airlinesListValue').html(sessionStorage.airline);
              memberSelected = sessionStorage.airline;
              self.$el.find('#airlinesList p').html($.i18n.map["sky_"+memberSelected]);
                                          
            }
            
			
              if(hasValue(sessionStorage.FS_departureDate) && (sessionStorage.FS_departureDate.search('/') >0)){
              self.$el.find('.departureListLi p').html(sessionStorage.FS_departureDate.split("/")[0]);
              }
              else{
              var d = new Date();
              sessionStorage.FS_departureDate = $.i18n.map.sky_today+" "+getDateInLocaleFormat(d)+ "/" + d.yyyymmdd();
              self.$el.find('.departureListLi p').html(sessionStorage.FS_departureDate.split("/")[0]);
              }

			if (sessionStorage.statusFor == "airports") {
                sessionStorage.airports_Depdate = sessionStorage.FS_departureDate;
				this.openAirprots();
			}
            else{
            getStatusOf = "flightNumber";
               sessionStorage.flightNumber_Depdate = sessionStorage.FS_departureDate;
               
            }
            if(self.$el.find('#airport_flightNumber').val() == ""){
            self.$el.find(".cancel_flightNumber").css("visibility", "hidden");
            }
            else{
            self.$el.find(".cancel_flightNumber").css("visibility", "visible");
            }
            
            
            
          return appView.prototype.render.apply(this, arguments);
            
		},
		events : {
            'focusin #destinationAirport':'changeHeight',
            'focusout #destinationAirport':'retainHeight',
            'focusin #sourceAirport':'changeHeightSource',
            'focusout #sourceAirport':'retainHeight',
			'click #flightNumber' : 'openFlightNumber',
			'click #airPorts' : 'openAirprots',
			'click #searchStatus' : 'getFlightStatusResults',
			'keydown :input' : 'logkey',
			'mouseup .cancel_flightNumber' : 'crossImage_FlightNumber',
			'mousedown .cancel_from' : 'crossImage_From',
			'mousedown .cancel_to' : 'crossImage_To',
			'click .search_from' : 'createFocusFrom',
			'click .search_to' : 'createFocusTo',
            'click .departureListLi' : 'openDeparturePopup',
            //'click .depDate-ok' : 'closeDeparturePopup',
            'click #airlinesList' : 'openMembersPopup',
            'click .members-ok' : 'closeMembersPopup',
            'keyup #airport_flightNumber' : 'valueEntered',
             'click #scanTicket' : 'openScanTIcket',
			'touchstart *' : function() {
				$.pageslide.close();
			}
		},
        closeDeparturePopup : function(e){
        $('.departureListLi p').html($('#operators :checked').val().split("/")[0]);
        $('#depatureDate-popup').css('display','none');
        $('.depDate-ok').css('display','none');
        $("#blackshade").css('display', 'none');
        },
        openDeparturePopup : function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
        popupView.openDatePopUP();
        },
        openScanTIcket : function(e){
		  e.preventDefault();
          sessionStorage.currentPage='scanPage';
          window.open("app:nativeScan","_self");
          setTimeout(function() {
                     window.open("analytics://_trackPageview#flightstatusScan", "_self");
                     }, 1000);
        },
        closeMembersPopup : function(e){
                                          popupView.closeAirlineList();
        },
        openMembersPopup : function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          popupView.openAirlineList();
			
        },
        retainHeight : function(e){
         $('.tabarea').css('margin-top','0');
         if(sessionStorage.deviceInfo == "android"){
               if ((screen.width == 480) && (screen.height == 800)) {
					$(".tabarea").css('margin-top', '0px');
				
				}
				$("#flightStatusScan").scrollTop(0)
             }
         if(isCancelBtnClicked)
        {
        	 isCancelBtnClicked=false;
        	 $("#"+e.target.id).focus();
        }
        },
        changeHeight : function(e){
        var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7" || (osVersion.split('.')[0]) == "8"){
        $('#airlinesList').css('margin-top','10px')
        $('.tabarea').css('margin-top','-70px');
        }
        }
        }
        else if(sessionStorage.deviceInfo == "android"){
               if ((screen.width == 480) && (screen.height == 800)) {
					$(".tabarea").css('margin-top', '-70px !important');
				   
				}
				$("#flightStatusScan").scrollTop(150)
             }

        },
        changeHeightSource : function(e){
        var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7"){
       $('.tabarea').css('margin-top','-60px');
        }
        }
        }else{
        $("#flightStatusScan").scrollTop(150)
        }
        },
                
		// To delete flight number
		crossImage_FlightNumber : function(e) {
			$('#airport_flightNumber').val("");
            $('#airport_flightNumber').focus();
			$(".cancel_flightNumber").css("visibility", "hidden");
		},
        createFocusTo : function(e){
        	$("#destinationAirport").val("");
            $("#destinationAirport").focus();
            isCancelBtnClicked=true;
            var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7"){
        $('#airlinesList').css('margin-top','10px')
        $('.tabarea').css('margin-top','-110px');
        }
        }
        }
        else if(sessionStorage.deviceInfo == "android"){
               if ((screen.width == 480) && (screen.height == 800)) {
					$(".tabarea").css('margin-top', '-70px !important');
				   
				}
             }
			$(".cancel_to").css("visibility", "hidden");
			$(".search_to").css("visibility", "visible");	
        },
        createFocusFrom : function(e){
        	$("#sourceAirport").val("");
            $("#sourceAirport").focus();
            isCancelBtnClicked=true;
            var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7"){
       $('.tabarea').css('margin-top','-60px');
        }
        }
        }
			$(".cancel_from").css("visibility", "hidden");
			$(".search_from").css("visibility", "visible");
        },
		//To delete departing from..
		crossImage_From : function(e) {
			$("#sourceAirport").val("");
            $("#sourceAirport").focus();
            isCancelBtnClicked=true;
            var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7"){
       $('.tabarea').css('margin-top','-60px');
        }
        }
        }
			$(".cancel_from").css("visibility", "hidden");
			$(".search_from").css("visibility", "visible");
		},
		//To delete arriving at..
		crossImage_To : function(e) {
			$("#destinationAirport").val("");
            $("#destinationAirport").focus();
            isCancelBtnClicked=true;
            var osVersion;
        if(sessionStorage.deviceInfo == "iPhone"){
        if ((screen.width == 320) && (screen.height == 480)) {
        osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
        osVersion = osVersion[0];
        osVersion = osVersion.replace(/_/g, '.');
        osVersion = osVersion.replace('OS ', '');
        if((osVersion.split('.')[0]) == "7"){
        $('#airlinesList').css('margin-top','10px')
        $('.tabarea').css('margin-top','-110px');
        }
        }
        }
        else if(sessionStorage.deviceInfo == "android"){
               if ((screen.width == 480) && (screen.height == 800)) {
					$(".tabarea").css('margin-top', '-70px !important');
				   
				}
             }
			$(".cancel_to").css("visibility", "hidden");
			$(".search_to").css("visibility", "visible");
		},
		// Called when key pressed in flight number,from/to fields
		valueEntered : function(e){
 
			var v = $('#airport_flightNumber').val();
			var maxLen = 4;
           if(v.length<=maxLen){
			for(i=0,maxlen=v.length; i< maxlen;i++)
			{
			  if(v.charCodeAt(i)<48 || v.charCodeAt(i)>57)
			  {
			  	v=v.replace(v[i],"");
                                               
                if(i==0)
                    $(".cancel_flightNumber").css("visibility", "hidden");
                                               
				i=0;
			  }
			}
			$('#airport_flightNumber').val(v); 
		}
			else{
				$('#airport_flightNumber').val($('#airport_flightNumber').val().substr(0,maxLen));
				
				}
				
			
		},
		logkey : function(e) {
			$('#airport_flightNumber').keyup(function() {
			
				if ($('#airport_flightNumber').val() != ''){
					$('.cancel_flightNumber').css('visibility', 'visible');
                    $('#airport_flightNumber').css("border-color", "#d7d7d7");
                    }
				else
					$('.cancel_flightNumber').css('visibility', 'hidden');
			});
			
			$('#sourceAirport').keyup(function() {
				if ($('#sourceAirport').val() != '') {
					$('.cancel_from').css('visibility', 'visible');
					$(".search_from").css("visibility", "hidden");
                    $('#sourceAirport').css("border-color", "#d7d7d7");
				} else {
					$('.cancel_from').css('visibility', 'hidden');
					$(".search_from").css("visibility", "visible");
				}
			});
			$('#destinationAirport').keyup(function() {
				if ($('#destinationAirport').val() != '') {
					$('.cancel_to').css('visibility', 'visible');
					$(".search_to").css("visibility", "hidden");
                    $('#destinationAirport').css("border-color", "#d7d7d7");
				} else {
					$('.cancel_to').css('visibility', 'hidden');
					$(".search_to").css("visibility", "visible");
				}
			});
			if (e.keyCode == 8) {
				keyval = $('#' + e.target.id).val();
                                      
			} else {
				keyval = keyval + String.fromCharCode(e.keyCode);
			}
			if (keyval != "" && e.keyCode != 8) {
				//sessionStorage.keyval = keyval;

                                                
                                                var minLenVal=3;
                                                
                                                var   names=[];
                                                names=JSON.parse(localStorage.cityList);
                                                if(checkCJKCompliedLanguage())
                                                {
                                                
                                                minLenVal=1;
                                                
                                                }
                                                
				if (names != null) {
                                                toHighlightAutoComplete();
					$(".Departure_text").autocomplete({

						source : names,
						minLength : minLenVal,
						open : function(event, ui) {
                         $(".ui-autocomplete").scrollTop(0);
						},
						select : function(event, ui) {
                            var osVersion;
                            if (sessionStorage.deviceInfo == "iPhone") {
                                osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                osVersion = osVersion[0];
                                osVersion = osVersion.replace(/_/g, '.');
                                osVersion = osVersion.replace('OS ', '');
                                if ((osVersion.split('.')[0]) == "8") {
                                    $(this).blur();
                                }
                                if ((osVersion.split('.')[0]) == "7") {
                                    $(this).blur();
                                }
                                if ((osVersion.split('.')[0]) == "6") {
                                if(this.id !== "sourceAirport"){
                                    $(this).blur();
                                    }
                                }
                            }
                        
                          if (sessionStorage.deviceInfo == "android") {
                          $(this).blur();
                          }
                                                    if(this.id == "destinationAirport"){
                                                      destAirportCode = ui.item.airportCode;
                                                      desLocationType = ui.item.locationType;
                                                      }
                                                      if(this.id == "sourceAirport"){
                                                      srcAirportCode = ui.item.airportCode;
                                                      srcLocationType = ui.item.locationType;
                                                      
                                                      setTimeout(function(){
                                                                 $("#destinationAirport").focus();
                                                                 },1);
                                                      }
                                                      
						}
					});

				}
			}
                                                if (sessionStorage.deviceInfo == "android") {
                                                if(e.keyCode == 13){
                                                $("#skyteam").css('margin-top','0px');
                                                $("#brief").css('visibility','visible');
                                                $(this).blur();
                                                this.getFlightStatusResults();
                                                }
                                                }
		},
		/* End of key press */
		/* To open flight tab */
		openFlightNumber : function() {
            
            if(hasValue(sessionStorage.flightNumber_Depdate)){
                $('.departureListLi p').html(sessionStorage.flightNumber_Depdate.split("/")[0]);
            }
            else{
				var tday=new Date().yyyymmdd();
				if(localStorage.date == "YYYY"){
				tday = $.i18n.map.sky_today+" "+tday+"/"+tday;
				}
				else if(localStorage.date == "DD"){
				tday = $.i18n.map.sky_today+" "+convertDate(tday)+"/"+tday;
				}
				else{
				tday = $.i18n.map.sky_today+" "+convertDateMMM(tday)+"/"+tday;
				}
                $('.departureListLi p').html(tday.split("/")[0]);
            }
            $('#sourceAirport').css("border-color", "#D7d7d7");
            $('#destinationAirport').css("border-color", "#D7d7d7");
			$('#flightNumber').removeClass('offHover').addClass('onHover');
			$('#airPorts').removeClass('onHover').addClass('offHover');
			if (sessionStorage.flightNo != undefined && sessionStorage.flightNo != null) {
				$('#airport_flightNumber').val(sessionStorage.flightNo);
                $('.cancel_flightNumber').css('visibility', 'visible');
			}
            $('.airports_info ul li:eq(0)').css('display','block');
			$('.airports_info ul li:eq(1)').css('display', 'block');
			$(".airports").css('display', 'none');
			getStatusOf = "flightNumber";
		},
		/*End of flight tab */
		/*to open Airport tab */
		openAirprots : function() {
        sessionStorage.flightNo = self.$el.find('#airport_flightNumber').val();


                                          //********************************************************************************

                                          if(latLonAirportName.length){
                                          autocomplete_autofill_inlanguagechange();
                                          self.$el.find('#sourceAirport').val(conte);
                                          srcAirportCode = latLonAirportCode;
                                          if(srcLocationType== undefined){

                                                                                    srcLocationType = "APT";

                                                                                       }

                                                                                    if(srcLocationType!=undefined){

                                                                                    if(!srcLocationType.length){

                                                                                      srcLocationType = "APT";

                                                                                    }

                                                                                    }



                                                                                    isFrom = "";

                                                                                    self.$el.find('.cancel_from').css('visibility', 'visible');

                                                                                    self.$el.find('.search_from').css('visibility', 'hidden');



                                                                                    }
                                          //***********************************************************************************


        if(hasValue(sessionStorage.airports_Depdate)){
                self.$el.find('.departureListLi p').html(sessionStorage.airports_Depdate.split("/")[0]);
        }
        else{
				var tday=new Date().yyyymmdd();
				if(localStorage.date == "YYYY"){
				tday = $.i18n.map.sky_today+" "+tday+"/"+tday;
				}
				else if(localStorage.date == "DD"){
				tday = $.i18n.map.sky_today+" "+convertDate(tday)+"/"+tday;
				}
				else{
				tday = $.i18n.map.sky_today+" "+convertDateMMM(tday)+"/"+tday;
				}
				self.$el.find('.departureListLi p').html(tday.split("/")[0]);
        }
        self.$el.find('#airlinesListValue').css('border','');
        self.$el.find('#airlinesList').css("border","");
        self.$el.find('#airport_flightNumber').css("border-color", "#D7d7d7");
        self.$el.find('#airPorts').removeClass('offHover').addClass('onHover');
        self.$el.find('#flightNumber').removeClass('onHover').addClass('offHover');
			if (sessionStorage.searchairports != undefined && sessionStorage.searchairports != null) {
				var airportsList = JSON.parse(sessionStorage.searchairports);

				self.$el.find('#sourceAirport').val(airportsList.source);
				self.$el.find('#destinationAirport').val(airportsList.destination);

				self.$el.find('.cancel_from').css('visibility', 'visible');
				self.$el.find('.search_from').css('visibility', 'hidden');
				self.$el.find('.cancel_to').css('visibility', 'visible');
				self.$el.find('.search_to').css('visibility', 'hidden');
			}
            self.$el.find('.airports_info ul li:eq(0)').css('display','none');
			self.$el.find('.airports_info ul li:eq(1)').css('display', 'none');
			self.$el.find(".airports").css('display', 'block');
			getStatusOf = "airports";
		},
		/*End of airport tab */
		/* To get search results for flight status */
		getFlightStatusResults : function() {

			var validstring;
			var dd = sessionStorage.FS_departureDate.split("/")[1];
            
			if (getStatusOf == "flightNumber") {
				sessionStorage.statusFor = "flightNumber";
                                          
            if((hasValue(memberSelected)) && ($('#airport_flightNumber').val()) == ""){//checks for the airline name & flight number
                    $('#airport_flightNumber').css("border-color", "red");
					openAlertWithOk($.i18n.map.sky_date_required);
					$('#airport_flightNumber').val("");
                    $('#airlinesList').css('border', '');
                    $('#airlinesListValue').css('border','');
                    $('#airlinesList').css("border-color", "red");
                    $('#airlinesListValue').css("border-color", "red");
            }
          else{

                if( $('#airlinesList p').html() === $.i18n.map.sky_airline_select){//checking the airline dropdown
                openAlertWithOk($.i18n.map.sky_incorrect);
                        $('#airlinesList').val("");
                        $('#airlinesList').css('border', '');
                        $('#airlinesList').css("border-color", "red");
                        $('#airlinesListValue').val("");
                        $('#airlinesListValue').css('border', '');
                        $('#airlinesListValue').css("border-color", "red");
                }
                else{//checks for the valid flight number
                    var flightNumber = memberSelected+$('#airport_flightNumber').val();
                                                   
                    if (dd.length === 10 && flightNumber.length >=4 ) {
                    

                            showActivityIndicatorForAjax('true');

                            sessionStorage.flightNo = $('#airport_flightNumber').val();
                            sessionStorage.airline=memberSelected;

                            sessionStorage.flightStatusUrl = URLS.FLIGHT_STATUS+'flightnumber=' + flightNumber + '&departuredate=' + dd;
                            window.open(iSC.flightstatusURL, "_self");
                        
                    }
                    else {
                      openAlertWithOk($.i18n.map.sky_incorrect_flight);
                      $('#airport_flightNumber').val("");
                      $(".cancel_flightNumber").css("visibility", "hidden");
                      $('#airport_flightNumber').css("border-color", "red");
                    }
                }
			}
         }//end of status through flight number
          else if (getStatusOf == "airports") {
                var destinationAirport = 0;
                var sourceAirport = 0;
				sessionStorage.statusFor = "airports";
				var source = $('#sourceAirport').val();
				var destination = $('#destinationAirport').val();

                if(source != "") {
                        var objSource = validateAirportWithLocType($('#sourceAirport').val(), srcLocationType);
                                          
                        if(objSource.isValid) {
                                sourceAirport = 1;
                                srcAirportCode = objSource.Code;
                                srcLocationType = objSource.locationType

                        }
                }
                
                                              
                if (destination != "") {
                            var objDest = validateAirportWithLocType($('#destinationAirport').val(), desLocationType);
                            if(objDest.isValid) {
                                        destinationAirport = 1;
                                        destAirportCode = objDest.Code;
                                        desLocationType = objDest.locationType

                            }
                }
                                              
				if (source != "" && destination != "" && dd != "Select a departure date") {
       
                    if(sourceAirport == 1 && destinationAirport == 1){
					var airportsList = {};
					airportsList.source = source;
					airportsList.destination = destination;
					sessionStorage.searchairports = JSON.stringify(airportsList);
			        showActivityIndicatorForAjax('true');

                    sessionStorage.flightStatusUrl = URLS.FLIGHT_STATUS+'origin=' + srcAirportCode + '&origintype=' + srcLocationType + '&destination=' + destAirportCode + '&destinationtype=' + desLocationType + '&departuredate=' + dd;

					window.open(iSC.flightstatusURL, "_self");
                                                }
                    else{
                    openAlertWithOk($.i18n.map.sky_valid_source);
                    if(sourceAirport != 1){
                    $('#sourceAirport').val("");
                    $('#sourceAirport').css("border-color", "red");
                    $(".cancel_from").css("visibility", "hidden");
                    $(".search_from").css("visibility", "visible");
                    }
                    if(destinationAirport != 1){
                    $('#destinationAirport').val("");
                    $('#destinationAirport').css("border-color", "red");
                    $(".cancel_to").css("visibility", "hidden");
                    $(".search_to").css("visibility", "visible");
                    }
                    }
                    }
                    else {
                    if(source == ""){
                    $('#sourceAirport').css("border-color", "red");
                    $(".cancel_from").css("visibility", "hidden");
                    $(".search_from").css("visibility", "visible");
                    }
                    else{
                    	 
                    if(sourceAirport != 1){
                    $('#sourceAirport').css("border-color", "red");
                    $('#sourceAirport').val("");
                    $(".cancel_from").css("visibility", "hidden");
                    $(".search_from").css("visibility", "visible");
                    }
                    }
                    if(destination == ""){
                    $('#destinationAirport').css("border-color", "red");
                    $(".cancel_to").css("visibility", "hidden");
                    $(".search_to").css("visibility", "visible");
                    }
                    else{
                    	
                    if(destinationAirport != 1){
                    $('#destinationAirport').css("border-color", "red");
                    $('#destinationAirport').val("");
                    $(".cancel_to").css("visibility", "hidden");
                    $(".search_to").css("visibility", "visible");
                    }
                    }
					openAlertWithOk($.i18n.map.sky_fromto);
				}
			}

		},
		/*End of search results */
                                                
                    
	});
	return flightstatusView;
});
