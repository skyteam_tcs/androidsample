define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/searchResults.html', 'text!template/flightDetailsTemplate.html', 'models/flights/skyTeamdb'], 
		function($, _, Backbone, utilities, constants, jqueryUi, searchResults, flightDetails, skyTeamdb) {

	var details, res, list = {}, that, 
	flightDetailsPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'flight-details',
		tabViews : {},
		initialize : function() {
			$('#backButtonList').show();
			$('#brief').show();
			$('#headingText img').hide();
			$('#headingText h2').show();
			$('#headingText h2').html($.i18n.map.sky_flight_details);

			$('#headingText h2').css('margin-right','37px');

			if ( "yes" === sessionStorage.savedflights || "yes" === sessionStorage.isFromSaved ) {
				$('#brief img').show();
				$('#brief h2').hide();
				sessionStorage.savedflights = "no";
				sessionStorage.isFromSaved = "no";

			}
			var currentPage=JSON.parse(sessionStorage.previousPages);
			if( "savedFlightsOrAirports" === currentPage[0].previousPage ){
				$('#brief img').hide();
				$('#brief h2').hide();
			}

		},
		render : function() {
			that = this;

			if($('.departure_details div:eq(0)').hasClass('line_single'))
				$('.departure_details div:eq(0)').removeClass('line_single');
			if($('.status').hasClass('mem_flightForSingle'))
				$('.status').removeClass('mem_flightForSingle');

			/* Back button functionality */
			$('.main_header ul li:eq(1) img').off('click').on('click', function(e) {

				e.preventDefault();
				e.stopImmediatePropagation();
			   sessionStorage.navDirection = "Left";

				if( "block" === $('#pageslide').css('display') ){
					$.pageslide.close();
				}
				else{         
					var currentobj=JSON.parse(sessionStorage.previousPages);
					if( currentobj[0].previousPage === "savedFlightsOrAirports" ){
						localStorage.removeItem('savedFlightsAirports');
						if( undefined == localStorage.savedFlightsAirports || null == localStorage.savedFlightsAirports )
						{
							window.open(iSC.savedAirports, "_self");
						}
					}
					if( "block" === $('#pageslide').css('display') ){
						$.pageslide.close();
					}
					else if ( "homePage" === currentobj[0].isFrom ){
						$('.main_header ul li:eq(0) img').remove();
						$('<a\>',{class:'menuItems',
							       href:'#nav'})
							  .append($('<img\>',{src:'./images/menu.png'}))
						.appendTo($('.main_header ul li:eq(0)'));
						
						//$('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

						window.location.href = "index.html#homePage";
					}
					else{
						if ( "searchResults" === currentobj[0].previousPage){

							sessionStorage.showList = currentobj[0].fromList;
							window.location.href = "index.html#"+currentobj[0].previousPage;

						}

					}
					if("searchResults" === sessionStorage.currentPage)
					{
						sessionStorage.savedflights = "yes";
					}

					popPageOnToBackStack();
					// remove all events associated with this particular view;
					$(that.el).undelegate();
				}
			});

			/* end of back functinality */
			var compiledTemplate = _.template(flightDetails,$.i18n.map);
			$(this.el).html(compiledTemplate);

			if ( "searchResults" === sessionStorage.currentPage ) {
				setTimeout(function(){
					lineAdjustment();
					hideActivityIndicator();},1000);

				res = JSON.parse(sessionStorage.flightDetailsObject);
			} else if ( "savedflights-page" === sessionStorage.currentPage) {
				showActivityIndicator($.i18n.map.sky_please);
				setTimeout(function(){
					lineAdjustment();
					hideActivityIndicator();},1000);
				if( "homePage" !== sessionStorage.previousPage )
				{  sessionStorage.previousPage = "savedflights-page";
				}

				$('.saveflight_btn').hide();
				$('.saveflight_btn_book').hide();
				$('#brief img').hide();
				$('#brief h2').hide();

				res = JSON.parse(JSON.parse(sessionStorage.savedFlightDetails));
			} else if ( "homePage" === sessionStorage.currentPage ) {
				showActivityIndicator($.i18n.map.sky_please);
				setTimeout(function(){
					lineAdjustment();
					hideActivityIndicator();},1200);
				sessionStorage.isFrom = "homePage";
				sessionStorage.previousPage = "homePage";
				$('.saveflight_btn').hide();
				$('.saveflight_btn_book').hide();
				$('#brief img').show();
				$('#brief h2').hide();

				res = JSON.parse(JSON.parse(sessionStorage.savedFlightDetails));

			}
			else if( "skyPriorityAdvancedDetails" === sessionStorage.currentPage ){
				showActivityIndicator($.i18n.map.sky_please);
				setTimeout(function(){
					lineAdjustment();
					hideActivityIndicator();},2000);
			}
			sessionStorage.previousPage = sessionStorage.currentPage;
			sessionStorage.currentPage = "FlightDetailsPage";
			/* Code for single segment  */
			if ( undefined == res.details.Segments.length ) {
				var dep_date;
				var arr_date;
				var dep_time;
				var arr_time;
				for (var i = 0, numberOfStops=res.details.NoOfStops; i < numberOfStops + 1; i++) {

					dep_date = getDateInLocaleFormat(new Date(res.details.Segments.DepartureDate));
					arr_date = getDateInLocaleFormat(new Date(res.details.Segments.ArrivalDate));   
					if ( "12h" === localStorage.time ) {
						dep_time = timeConvertor(res.details.Segments.DepartureTime.split(':')[0] + ":" + res.details.Segments.DepartureTime.split(':')[1]);
						arr_time = timeConvertor(res.details.Segments.ArrivalTime.split(':')[0] + ":" + res.details.Segments.ArrivalTime.split(':')[1]);
					} else {
						dep_time = res.details.Segments.DepartureTime.split(':')[0] + ":" + res.details.Segments.DepartureTime.split(':')[1];
						arr_time = res.details.Segments.ArrivalTime.split(':')[0] + ":" + res.details.Segments.ArrivalTime.split(':')[1];
					}

					$('.flight_info ul li:eq(0) p').html((getAirportName(res.details.Segments.Origin)) + " "+($.i18n.map.sky_to)+" " + (getAirportName(res.details.Segments.Destination)));
					$('.flight_info ul li:eq(1) p span').html(dep_date);
					$('.flight_info ul li:eq(2) p span').html(arr_date);
					if(res.details.hasOwnProperty('TotalDuration')){
						if( "" != res.details.TotalDuration ){
							$('.flight_info ul li:eq(3) p span:first').html(getTravelTime((res.details.TotalDuration).toLowerCase()));
						}
					}
					else{
						$('.flight_info ul li:eq(3) p span:first').html("");
					}
					$('.line ul').append($('<li></li>'));

					$('.status').append($('<div></div>').attr('class', 'dep_data').append($('<ul></ul>').attr('class', 'departure_zone openairport').attr('id', 'departure_zone').append($('<li></li>').append($('<p></p>').html(res.details.Segments.Origin)).append($('<span></span').html(dep_time))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_departure)).append($('<span></span').html(getAirportFullName(res.details.Segments.Origin)))).append($('<li></li>').append($('<img></img>').attr('src', './images/arrow.png')))).append($('<ul></ul>').attr('class', 'mem_flight').append($('<li></li>').append($('<p></p>').html(res.details.Segments.FlightNumber)).append($('<span></span>')).append($('<p></p>'))).append($('<li></li>').append($('<ul></ul>').append($('<li></li>').append($('<img></img>').attr('class', 'operatorImg'))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_operatedby+": " ).append($('<span></span>').html(res.details.Segments.OperatedBy)))).append($('<li></li>').append($('<p></p>').append($('<span></span>')))).append($('<li></li>').hide().append($('<p></p>').html($.i18n.map.sky_terminal+' ').append($('<span></span>').html()))).append($('<li></li>').hide().append($('<p></p>').html($.i18n.map.sky_baggagebelt).append($('<span></span>').html()))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_aircraft+": " ).append($('<span></span>').html(res.details.Segments.AircraftType))))))).append($('<ul></ul>').attr('class', 'last_arrow openairport').attr('id', 'arrival_zone').append($('<li></li>').append($('<p></p>').html(res.details.Segments.Destination)).append($('<span></span').html(arr_time))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_arrival)).append($('<span></span').html(getAirportFullName(res.details.Segments.Destination)))).append($('<li></li>').append($('<img></img>').attr('src', './images/arrow.png')))));

					$('.operatorImg').attr('src','./images/members2/'+((res.details.Segments.FlightNumber).substring(0, 2)) + '-Logo.png');
					if(res.details.Segments.hasOwnProperty('TravelTime')){
						if( "" != res.details.Segments.TravelTime ){
							$('.mem_flight li:eq(1) ul li:eq(2) p').width("80%");
							$('.mem_flight li:eq(1) ul li:eq(2) p').html($.i18n.map.sky_duration +": " +getTravelTime((res.details.Segments.TravelTime).toLowerCase()));
						}
					}
					if( "yes" === sessionStorage.home ){
						if( "" != localStorage.savedFlightDetails && undefined != localStorage.savedFlightDetails ){
							var results=JSON.parse(localStorage.savedFlightDetails);
							for (var m = 0,savedFgtLength=results.SavedFlights_test.length; m < savedFgtLength; m++) {
								if(results.SavedFlights_test[m].Flight_Detail_Status != ""){
									if( "No Flight Status Found" != results.SavedFlights_test[m].Flight_Detail_Status[0] && "Status Not Available" != results.SavedFlights_test[m].Flight_Detail_Status[0] ){
										if( "Delayed" == results.SavedFlights_test[m].Flight_Detail_Status[0] ){
											$('.mem_flight li:eq(0) span:eq(0)').html(getStatus(results.SavedFlights_test[m].Flight_Detail_Status[0]));
											var delay = results.SavedFlights_test[m].Status_Object[0].DelayedBy;
											if(  undefined != delay && "" != delay ){
												if(delay.match('H') && delay.match('M')){
													var delayH = delay.split('H')[0];
													var delayM = (delay.split('M')[0]).split('H')[1];
													if( 1 === delayH.length ){
														delayH = "0" + delayH + "h";
													}
													else{
														delayH = delayH + "h";
													}
													if( 1 === delayM.length ){
														delayM = "0" + delayM + "m";
													}
													else{
														delayM = delayM + "m";
													}
													delay = delayH + delayM;

												}
												else{
													if(delay.match('H')){
														var delayH = delay.split('H')[0];
														if( 1 === delayH.length ){
															delayH = "0" + delayH+"h00m";
														}
														else{
															delayH = delayH+"h00m";
														}
														delay = delayH;
													}
													if(delay.match('M')){
														var delayM = delay.split('M')[0]
														if( 1 === delayM.length ){
															delayM = "00h0" + delayM+"m";
														}
														else{
															delayM = "00h"+delayM+"m";
														}
														delay = delayM;
													}
												}
											}
											$('.mem_flight li:eq(0) p:eq(1)').html(delay);
										}
										else{
											$('.mem_flight li:eq(0) span:eq(0)').html(getStatus(results.SavedFlights_test[m].Flight_Detail_Status[0]));
										}
									}
									if( $.i18n.map.sky_delayed === ($('.mem_flight li:eq(0) span:eq(0)').html()) ){
										$('.mem_flight li:eq(0) span:eq(0)').css('color','#D7862F');
										$('.mem_flight li:eq(0) p:eq(1)').css('color','#D7862F');
									}
									else if( $.i18n.map.sky_cancelled === ($('.mem_flight li:eq(0) span:eq(0)').html()) ){
										$('.mem_flight li:eq(0) span:eq(0)').css('color','#D7862F');
									}
									else {
										$('.mem_flight li:eq(0) span:eq(0)').css('color','#0B1761');
									}
								}
								if( "" != results.SavedFlights_test[m].Status_Object && results.SavedFlights_test[m].hasOwnProperty('Status_Object')){
									if(results.SavedFlights_test[m].Status_Object[0].Terminal != undefined && results.SavedFlights_test[m].Status_Object[0].BaggageBelt != undefined)
									{
										$('.departure_details div:eq(0)').addClass('line_changeheight');
										$('.status').addClass('mem_flightchange');
										$('.mem_flight li:eq(1) ul:eq(0) li:eq(3)').show();
										$('.mem_flight li:eq(1) ul:eq(0) li:eq(3) span').html(results.SavedFlights_test[m].Status_Object[0].Terminal);
										$('.mem_flight li:eq(1) ul:eq(0) li:eq(4)').show();
										$('.mem_flight li:eq(1) ul:eq(0) li:eq(4) span').html(results.SavedFlights_test[m].Status_Object[0].BaggageBelt);

									}
									else if( undefined != results.SavedFlights_test[m].Status_Object[0].Terminal )
									{$('.departure_details div:eq(0)').addClass('line_single');
									$('.status').addClass('mem_flightForSingle');
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(3)').show();
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(3) span').html(results.SavedFlights_test[m].Status_Object[0].Terminal);
									}
									else if( undefined != results.SavedFlights_test[m].Status_Object[0].BaggageBelt )
									{
										$('.departure_details div:eq(0)').addClass('line_single');
										$('.status').addClass('mem_flightForSingle');
										$('.mem_flight li:eq(1) ul:eq(0) li:eq(4)').show();
										$('.mem_flight li:eq(1) ul:eq(0) li:eq(4) span').html(results.SavedFlights_test[m].Status_Object[0].BaggageBelt);
									}
								}
							}
						}
					}
					if( "yes" === sessionStorage.saved ){
						var status = JSON.parse(localStorage.savedFlightStatus);

						if( "" != status ){
							if( "" != status.Flight_Status && undefined != status && status.hasOwnProperty('Status_Object')){

								if( "No Flight Status Found" != status.Status_Object[0].CurrentStatus && "Status Not Available" != status.Status_Object[0].CurrentStatus ){
									if( "Delayed" === status.Status_Object[0].CurrentStatus ){
										$('.mem_flight li:eq(0) span:eq(0)').html(getStatus(status.Status_Object[0].CurrentStatus));
										var delay = status.Status_Object[0].DelayedBy;
										if( undefined != delay && "" != delay ){
											if(delay.match('H') && delay.match('M')){
												var delayH = delay.split('H')[0];
												var delayM = (delay.split('M')[0]).split('H')[1];
												if( 1 === delayH.length ){
													delayH = "0" + delayH + "h";
												}
												else{
													delayH = delayH + "h";
												}
												if( 1 === delayM.length ){
													delayM = "0" + delayM + "m";
												}
												else{
													delayM = delayM + "m";
												}
												delay = delayH + delayM;

											}
											else{
												if(delay.match('H')){
													var delayH = delay.split('H')[0];
													if( 1 === delayH.length ){
														delayH = "0" + delayH+"h00m";
													}
													else{
														delayH = delayH+"h00m";
													}
													delay = delayH;
												}
												if(delay.match('M')){
													var delayM = delay.split('M')[0]
													if( 1 === delayM.length ){
														delayM = "00h0" + delayM+"m";
													}
													else{
														delayM = "00h"+delayM+"m";
													}
													delay = delayM;
												}
											}
										}
										$('.mem_flight li:eq(0) p:eq(1)').html(delay);
									}
									else{
										$('.mem_flight li:eq(0) span:eq(0)').html(getStatus(status.Status_Object[0].CurrentStatus));
									}
								}
								if( ("Delayed" == status.Status_Object[0].CurrentStatus) || ("Cancelled" == status.Status_Object[0].CurrentStatus)){
									$('.mem_flight li:eq(0) span:eq(0)').css('color','#D7862F');
									$('.mem_flight li:eq(0) p:eq(1)').css('color','#D7862F');
								}
								else {
									$('.mem_flight li:eq(0) span:eq(0)').css('color','#0B1761');
								}

								if(status.Status_Object[0].hasOwnProperty('Terminal') && status.Status_Object[0].hasOwnProperty('BaggageBelt')){
									$('.departure_details div:eq(0)').addClass('line_changeheight');
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(3)').show();
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(3) span').html(status.Status_Object[0].Terminal);
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(4)').show();
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(4) span').html(status.Status_Object[0].BaggageBelt);
									$('.status').addClass('mem_flightchange');

								}
								else if(status.Status_Object[0].hasOwnProperty('Terminal'))
								{
									$('.departure_details div:eq(0)').addClass('line_single');
									$('.status').addClass('mem_flightForSingle');
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(3)').show();
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(3) span').html(status.Status_Object[0].Terminal);
								}
								else if(status.Status_Object[0].hasOwnProperty('BaggageBelt'))
								{
									$('.departure_details div:eq(0)').addClass('line_single');
									$('.status').addClass('mem_flightForSingle');
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(4)').show();
									$('.mem_flight li:eq(1) ul:eq(0) li:eq(4) span').html(status.Status_Object[0].BaggageBelt);
								}

							}

						}
						else{
							$('#mem_flight li:eq(0) span:eq(0)').hide();
						}
					}
				}


			}

			/* end of single segment */
			/*Code for multiple segments */
			else {
				var j = res.details.Segments.length - 1;
				var dep_date;
				var arr_date;
				var dep_time;
				var arr_time;
				for (var i = 0,segmentsLength=res.details.Segments.length; i < segmentsLength; i++) {
					dep_date = getDateInLocaleFormat(new Date(res.details.Segments[0].DepartureDate));
					arr_date = getDateInLocaleFormat(new Date(res.details.Segments[j].ArrivalDate));	   
					if ("12h" === localStorage.time ) {
						dep_time = timeConvertor(res.details.Segments[i].DepartureTime.split(':')[0] + ":" + res.details.Segments[i].DepartureTime.split(':')[1]);
						arr_time = timeConvertor(res.details.Segments[i].ArrivalTime.split(':')[0] + ":" + res.details.Segments[i].ArrivalTime.split(':')[1]);
					} else {
						dep_time = res.details.Segments[i].DepartureTime.split(':')[0] + ":" + res.details.Segments[i].DepartureTime.split(':')[1];
						arr_time = res.details.Segments[i].ArrivalTime.split(':')[0] + ":" + res.details.Segments[i].ArrivalTime.split(':')[1];
					}

					$('.flight_info ul li:eq(0) p').html(getAirportName(res.details.Segments[0].Origin) + " "+($.i18n.map.sky_to)+" " + getAirportName(res.details.Segments[j].Destination));
					$('.flight_info ul li:eq(1) p span').html(dep_date);
					$('.flight_info ul li:eq(2) p span').html(arr_date);
					if(res.details.hasOwnProperty('TotalDuration')){
						if(res.details.TotalDuration != ""){
							$('.flight_info ul li:eq(3) p span:first').html(getTravelTime((res.details.TotalDuration).toLowerCase()));
						}
					}
					else{
						$('.flight_info ul li:eq(3) p span:first').html("");
					}
					$('.line ul').append($('<li></li>'));

					$('.status').append($('<div></div>').attr('class', 'dep_data').append($('<ul></ul>').attr('class', 'departure_zone openairport').attr('id', 'departure_zone_' + i).append($('<li></li>').append($('<p></p>').html(res.details.Segments[i].Origin)).append($('<span></span').html(dep_time))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_departure)).append($('<span></span').html(getAirportFullName(res.details.Segments[i].Origin)))).append($('<li></li>').append($('<img></img>').attr('src', './images/arrow.png')))).append($('<ul></ul>').attr('class', 'mem_flight').attr('id','mem_flight'+i).append($('<li></li>').append($('<p></p>').html(res.details.Segments[i].FlightNumber)).append($('<span></span>')).append($('<p></p>'))).append($('<li></li>').append($('<ul></ul>').append($('<li></li>').append($('<img></img>').attr('class', 'operatorImg'))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_operatedby+": " ).append($('<span></span>').html(res.details.Segments[i].OperatedBy)))).append($('<li></li>').append($('<p></p>').append($('<span></span>')))).append($('<li></li>').hide().append($('<p></p>').html($.i18n.map.sky_terminal+' ').append($('<span></span>').html("")))).append($('<li></li>').hide().append($('<p></p>').html("Bagage belt: ").append($('<span></span>').html("")))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_aircraft+": " ).append($('<span></span>').html(res.details.Segments[i].AircraftType))))))).append($('<ul></ul>').attr('class', 'last_arrow openairport').attr('id', 'arrival_zone_' + i).append($('<li></li>').append($('<p></p>').html(res.details.Segments[i].Destination)).append($('<span></span').html(arr_time))).append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_arrival)).append($('<span></span').html(getAirportFullName(res.details.Segments[i].Destination)))).append($('<li></li>').append($('<img></img>').attr('src', './images/arrow.png')))));


					if(res.details.Segments[i].hasOwnProperty('TravelTime')){
						if( "" != res.details.Segments[i].TravelTime ){
							$('#mem_flight'+i+' li:eq(1) ul li:eq(2) p').width("90%");
							$('#mem_flight'+i+' li:eq(1) ul li:eq(2) p').html($.i18n.map.sky_duration +": "+getTravelTime((res.details.Segments[i].TravelTime).toLowerCase()));
						}
					}
					if( "yes" === sessionStorage.home){
						if( "" != localStorage.savedFlightDetails && undefined != localStorage.savedFlightDetails ){
							var results=JSON.parse(localStorage.savedFlightDetails);
							for (var m = 0,savedFgtLen =results.SavedFlights_test.length; m < savedFgtLen; m++) {
								if( "" != results.SavedFlights_test[m].Flight_Detail_Status ){
									if( "No Flight Status Found" != results.SavedFlights_test[m].Flight_Detail_Status[i] && "Status Not Available" != results.SavedFlights_test[m].Flight_Detail_Status[i]){
										if( "Delayed" == results.SavedFlights_test[m].Flight_Detail_Status[i]){
											$('#mem_flight'+i+' li:eq(0) span:eq(0)').html(getStatus(results.SavedFlights_test[m].Flight_Detail_Status[i]));
											var delay = results.SavedFlights_test[m].Status_Object[i].DelayedBy;
											if( undefined != delay && "" != delay ){
												if(delay.match('H') && delay.match('M')){
													var delayH = delay.split('H')[0];
													var delayM = (delay.split('M')[0]).split('H')[1];
													if( 1=== delayH.length ){
														delayH = "0" + delayH + "h";
													}
													else{
														delayH = delayH + "h";
													}
													if( 1 === delayM.length ){
														delayM = "0" + delayM + "m";
													}
													else{
														delayM = delayM + "m";
													}
													delay = delayH + delayM;

												}
												else{
													if(delay.match('H')){
														var delayH = delay.split('H')[0];
														if( 1 === delayH.length ){
															delayH = "0" + delayH+"h00m";
														}
														else{
															delayH = delayH+"h00m";
														}
														delay = delayH;
													}
													if(delay.match('M')){
														var delayM = delay.split('M')[0]
														if( 1 === delayM.length ){
															delayM = "00h0" + delayM+"m";
														}
														else{
															delayM = "00h"+delayM+"m";
														}
														delay = delayM;
													}
												}
											}
											$('#mem_flight'+i+' li:eq(0) p:eq(1)').html(delay);
										}
										else{
											$('#mem_flight'+i+' li:eq(0) span:eq(0)').html(getStatus(results.SavedFlights_test[m].Flight_Detail_Status[i]));
										}
									}
									if(($('#mem_flight'+i+' li:eq(0) span:eq(0)').html() == $.i18n.map.sky_delayed) || ($('#mem_flight'+i+' li:eq(0) span:eq(0)').html() == $.i18n.map.sky_cancelled)){
										$('#mem_flight'+i+' li:eq(0) span:eq(0)').css('color','#D7862F');
										$('#mem_flight'+i+' li:eq(0) p:eq(1)').css('color','#D7862F');
									}
									else {
										$('#mem_flight'+i+' li:eq(0) span:eq(0)').css('color','#0B1761');
									}
								}
								if( "" != results.SavedFlights_test[m].Status_Object && results.SavedFlights_test[m].hasOwnProperty('Status_Object')){
									if( undefined != results.SavedFlights_test[m].Status_Object[i].Terminal && undefined != results.SavedFlights_test[m].Status_Object[i].BaggageBelt )
									{
										$('.status').addClass('mem_flightchange');
										$('.departure_details div:eq(0)').addClass('line_changeheight');
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4)').show();
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4) span').html(results.SavedFlights_test[m].Status_Object[i].BaggageBelt);

										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3)').show();
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3) span').html(results.SavedFlights_test[m].Status_Object[i].Terminal);
									}

									else if( undefined != results.SavedFlights_test[m].Status_Object[i].Terminal )
									{
										$('.departure_details div:eq(0)').addClass('line_single');
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3)').show();

										$('.status').addClass('mem_flightForSingle');
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3) span').html(results.SavedFlights_test[m].Status_Object[i].Terminal);
									}
									else if( undefined != results.SavedFlights_test[m].Status_Object[i].BaggageBelt )
									{
										$('.departure_details div:eq(0)').addClass('line_single');
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4)').show();
										$('.status').addClass('mem_flightForSingle');
										$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4) span').html(results.SavedFlights_test[m].Status_Object[i].BaggageBelt);
									}
								}
							}
						}
					}
					if( "yes" === sessionStorage.saved ){
						var status = JSON.parse(localStorage.savedFlightStatus);

						if( "" != status){
							if( "" != status.Flight_Status && undefined != status && status.hasOwnProperty('Status_Object')){
								if( "No Flight Status Found" != status.Status_Object[i].CurrentStatus && "Status Not Available" != status.Status_Object[i].CurrentStatus ){
									if("Delayed" == status.Status_Object[i].CurrentStatus ){
										$('#mem_flight'+i+' li:eq(0) span:eq(0)').html(getStatus(status.Status_Object[i].CurrentStatus));
										var delay = status.Status_Object[i].DelayedBy;
										if( undefined != delay && "" != delay ){
											if(delay.match('H') && delay.match('M')){
												var delayH = delay.split('H')[0];
												var delayM = (delay.split('M')[0]).split('H')[1];
												if( 1 === delayH.length ){
													delayH = "0" + delayH + "h";
												}
												else{
													delayH = delayH + "h";
												}
												if( 1 === delayM.length ){
													delayM = "0" + delayM + "m";
												}
												else{
													delayM = delayM + "m";
												}
												delay = delayH + delayM;

											}
											else{
												if(delay.match('H')){
													var delayH = delay.split('H')[0];
													if( 1 === delayH.length){
														delayH = "0" + delayH+"h00m";
													}
													else{
														delayH = delayH+"h00m";
													}
													delay = delayH;
												}
												if(delay.match('M')){
													var delayM = delay.split('M')[0]
													if( 1 === delayM.length ){
														delayM = "00h0" + delayM+"m";
													}
													else{
														delayM = "00h"+delayM+"m";
													}
													delay = delayM;
												}
											}
										}
										$('#mem_flight'+i+' li:eq(0) p:eq(1)').html(delay);
									}
									else{
										$('#mem_flight'+i+' li:eq(0) span:eq(0)').html(getStatus(status.Status_Object[i].CurrentStatus));
									}
								}
								if( ("Delayed" == status.Status_Object[i].CurrentStatus) || ("Cancelled" == status.Status_Object[i].CurrentStatus) ){
									$('#mem_flight'+i+' li:eq(0) span:eq(0)').css('color','#D7862F');
									$('#mem_flight'+i+' li:eq(0) p:eq(1)').css('color','#D7862F');
								}
								else {
									$('#mem_flight'+i+' li:eq(0) span:eq(0)').css('color','#0B1761');
								}
								if(status.Status_Object[i].hasOwnProperty('Terminal') && status.Status_Object[i].hasOwnProperty('BaggageBelt')){
									$('.departure_details div:eq(0)').addClass('line_changeheight');
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3)').show();
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3) span').html(status.Status_Object[i].Terminal);
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4)').show();
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4) span').html(status.Status_Object[i].BaggageBelt);
									$('.status').addClass('mem_flightchange');

								}
								else if(status.Status_Object[i].hasOwnProperty('Terminal'))
								{
									$('.departure_details div:eq(0)').addClass('line_single');
									$('.status').addClass('mem_flightForSingle');
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3)').show();
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(3) span').html(status.Status_Object[i].Terminal);
								}
								else if(status.Status_Object[i].hasOwnProperty('BaggageBelt'))
								{
									$('.departure_details div:eq(0)').addClass('line_single');
									$('.status').addClass('mem_flightForSingle');
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4)').show();
									$('#mem_flight'+i+' li:eq(1) ul:eq(0) li:eq(4) span').html(status.Status_Object[i].BaggageBelt);
								}

							}
						}
						else{
							$('#mem_flight'+i+' li:eq(0) span:eq(0)').hide();
						}
					}
					if (i < j) {
						$('.line ul').append($('<li></li>'));

						$('.status').append($('<div></div>').attr('class', 'stopover').append($('<ul></ul>').attr('class', 'stop').append($('<li></li>').append($('<p></p>').html($.i18n.map.sky_connectiontime+": ").append($('<span></span')
								.html(typeof (res.details.StopTimes)=='string'? getTravelTime(((res.details.StopTimes).toLowerCase())) :
									getTravelTime(((res.details.StopTimes[i]).toLowerCase()))
								))))));

					}
					$('.operatorImg').attr('src','./images/members2/'+((res.details.Segments[i].FlightNumber).substring(0, 2)) + '-Logo.png');

				}


			}
			sessionStorage.saved = "no";
			/* End of multiple segments */

			$('#skypriority').text($.i18n.map.sky_priority);
			slideThePage(sessionStorage.navDirection);
		},

		events : {
			'click #skypriority':'skyPriority',
			'click .openairport' : 'navigateToAirport',
			'click #saveflightDetails' : 'saveFlightDetails',
			'touchstart *' : function() {
				$.pageslide.close();
			}

		},

		/* function to skyPriority page details */
		skyPriority : function(e) {
            showActivityIndicatorForAjax('false');
						/*For checking internet connectivity*/
            window.networkStatCallBack = function(){
				var netStatus =JSON.parse(localStorage.networkStatus);
		/*If network is available,call the webservice*/
				if("online" === netStatus.network){

		        	var flightDetails,spDetails;
		        	flightDetails=JSON.parse(sessionStorage.flightDetailsObject);

		        	if( "array" === $.type(flightDetails.details.Segments) ){
		        		var connectionAirports=[];
			        	$.each(flightDetails.details.Segments,function(index,object){
				        	if(index>0){
					    	    connectionAirports.push({transferairport:object.Origin,operatingcarrier:object.OperatorCode});
					    	    //spDetails.transferdetails.push(connectionAirport);
					    	    if(flightDetails.details.Segments.length === index+1 ){
							      spDetails.tripdestination=object.Destination;
							      spDetails.transferdetails=connectionAirports;
						         }
					         }
					        else{
					        	spDetails={triporigin:object.Origin,operatingcarrier:object.OperatorCode,transferdetails:{},tripdestination:''};
				        	}
				        });
			        }
		        	else{
			        	var flightNo,flightSource,flightDestination,operatingAirline;
			        	flightNo=flightDetails.details.Segments.FlightNumber;
			        	flightSource=flightDetails.details.Segments.Origin
			        	flightDestination=flightDetails.details.Segments.Destination
			        	operatingAirline=flightDetails.details.Segments.OperatorCode;
			        	spDetails={triporigin:flightSource,operatingcarrier:operatingAirline,transferdetails:[],tripdestination:flightDestination};
			        }
			        sessionStorage.skypriorityAdvancedDetails=JSON.stringify(spDetails);

			         var obj1 = {};
		    	       obj1.previousPage = "FlightDetailsPage";
			         pushPageOnToBackStack(obj1);


			         setTimeout(function(){getskyPriorityAdvancedDetailsAjax()},1000);
		    	}
		    	else
		    	{
		    	      hideActivityIndicator();
                      openAlertWithOk($.i18n.map.sky_notconnected);
		    	}
		};
        window.open(iSC.Network, "_self");
		},
		/* end of function to skyPriority page details*/

		/* function to save selected flight */
		saveFlightDetails : function(e) {
			sessionStorage.insertSaveFlightFrom = "FlightDetails";
			window.open(iSC.insertSaveFlight);
		},
		/*End of save flight */

		/* Code for opening the airport details */
		navigateToAirport : function(e) {
			window.networkStatCallBack = function(){
				var netstat = JSON.parse(localStorage.networkStatus);
				if( "offline" === netstat.network ){
					hideActivityIndicator();
					setTimeout(function(){
						openAlertWithOk($.i18n.map.sky_notconnected);
					},100);
				} else {
					sessionStorage.isFrom = "flightdetails";
					var pageObj={};
					pageObj.previousPage="FlightDetailsPage";
					pageObj.isFrom=sessionStorage.previousPage;
					if(sessionStorage.previousPage == "homePage")
						pageObj.data = sessionStorage.savedFlightDetails;
					pageObj.isFrom=sessionStorage.previousPage;
					pageObj.flightDetails=sessionStorage.flightDetailsObject;
					pageObj.flightResults=localStorage.searchResults;
					pushPageOnToBackStack(pageObj);
					//$('.flightdetails_view').hide();
					showActivityIndicatorForAjax('false');
					var id = $(e.currentTarget).attr('id');
					var airportcode = $('#' + id + ' li:eq(0) p').html();
					var obj={};
					sessionStorage.airportName = getAirportFullName(airportcode);
					sessionStorage.airport_code= airportcode;
					obj.airportName = getAirportFullName(airportcode);
					obj.airportCode = airportcode;
					obj.countryName = (getCountryName(airportcode)).split('_')[0];
					obj.countryCode = (getCountryName(airportcode)).split('_')[1];
					var countryCodeOld;
					countryCodeOld =getCountryNameOld(airportcode);


					obj.airportDetails_URL = URLS.AIRPORT_DETAILS+"airportcode="+airportcode+"&city="+getAirportNameOld(airportcode)+"&countrycode="+countryCodeOld.split('_')[1];
					obj.cityName = getAirportName(airportcode);
					sessionStorage.currentPage = "airportDetails";
					sessionStorage.airportDetailsObj=JSON.stringify(obj);
					setTimeout(function(){window.location.href = "index.html#airportDetails"; },1000);
				}
				window.networkStatCallBack = function(){}
			}
			window.open(iSC.Network, "_self");

		}
		/* End of airport navigation */
	});

	return flightDetailsPageView;
});