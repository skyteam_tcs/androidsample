/* Airports Functionality */
var _lat;
var _lon;
var uiValue;
var closeInfowindow;
var markers = [];
define(['jquery', 'backbone', 'underscore', 'text!template/searchAirportTemplate.html', 'text!template/menuTemplate.html'], function($, Backbone, _, airportSearchTemplate, menuPageTemplate) {
	var list = {};
	var keyval = "";
	sessionStorage.keyval = "";
	var airportPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'airportsearch-page',
		initialize : function() {
            localStorage.googleMapsLoaded = "true";
			if (sessionStorage.previousPages != undefined) {
				if (JSON.parse(sessionStorage.previousPages).length == 0) {
					sessionStorage.removeItem('previousPages');
				}
			}
			$('#headingText img').css('display', 'none');
			$('#headingText h2').css('display', 'block');
			$('#headingText h2').html(localization.english.Airports_finder);

			if (sessionStorage.savedflights == "yes") {
				$('#brief img').css('display', 'block');
				$('#brief h2').css('display', 'none');
                sessionStorage.savedflights = "no";
			}
			if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
				$('.main_header ul li:eq(0) img').remove();
				$('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

			}
			if (sessionStorage.navToAirportSearch == "yes") {

			} else {
				$(".menuItems").pageslide();

				var template = _.template(menuPageTemplate,$.i18n.map);
				$('#pageslide').append(template);
			}
		},

		render : function() {
            sessionStorage.skyTipsAirportName = "";
			sessionStorage.From = "Menu";
			sessionStorage.skytipstheme = 1;
			sessionStorage.currentPage = "airportSearchPage";
			$.i18n.map.check=localization.english;
			var compiledTemplate = _.template(airportSearchTemplate,$.i18n.map);
			$(this.el).html(compiledTemplate);
			var locationPath = window.location;
			locationPath = locationPath.toString();
			var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
			if (spath == "airportSearchPage" && sessionStorage.navToAirportSearch == "yes") {
				$(".menuItems").trigger('click');
				sessionStorage.navToAirportSearch = "no";
			}
			var self = this;
            window.networkStatCallBack = function(){
                var netstat = JSON.parse(localStorage.networkStatus);
                if(netstat.network == "offline"){
                    hideActivityIndicator();
                    setTimeout(function(){
                    openAlertWithOk($.i18n.map.sky_notconnected);
                    },100);
                } else {
                    if (sessionStorage.searchAirport != undefined && sessionStorage.searchAirport != null && sessionStorage.searchAirport != "") {
                        $('#airport_text').val(sessionStorage.searchAirport);
                        $('.cross_image').css('visibility', 'visible');
                        $('.search_icon1').css('visibility', 'hidden');
                        self.loadspecificmap(event, uiValue);
                    } else {
                        self.loadmaps();
                    }
                }
                window.networkStatCallBack = function(){}
            }
            window.open(iSC.Network, "_self");
			$('.cross_image').css('visibility', 'hidden');
			$('.search_icon1').css('visibility', 'visible');
			$('.list_maps').css('display', 'none');

			/* Show Suggestive Text */
			if (localStorage.cityList == 'undefined' || localStorage.cityList == null) {
				window.getAirportsCallback = function() {
					var AirportNames = JSON.parse(localStorage.cityList)
					var names = _.uniq(_.map(AirportNames.AirPorts_test, function(AirportName) {
						return AirportName.cityName + " (" + AirportName.cityCode + ")";
					}));
					if (names != null) {
						$("#airport_text").autocomplete({
							source : names,
							minLength : 3,
                            appendTo: ".airports_main",
                            open : function(event, ui) {
                            $(".ui-autocomplete").scrollTop(0);
                            },
							select : function(event, ui) {
                            sessionStorage.searchAirport = ui.item.value;
								uiValue = ui;
								$(".ui-autocomplete").hide();
                                showActivityIndicator($.i18n.map.sky_please);
                                window.networkStatCallBack = function(){
                                    var netstat = JSON.parse(localStorage.networkStatus);
                                    if(netstat.network == "offline"){
                                        hideActivityIndicator();
                                        setTimeout(function(){
                                        openAlertWithOk($.i18n.map.sky_notconnected);
                                        },100);
                                    } else {
                                        self.loadspecificmap(event, ui);
                                    }
                                    window.networkStatCallBack = function(){}
                                }
                                window.open(iSC.Network, "_self");
							}
						});
					}
				}
                                               setTimeout(function(){
                                                          window.open(iSC.getAirports);
                                                          },1000);
			} else {
				var AirportNames = JSON.parse(localStorage.cityList)

				var names = _.uniq(_.map(AirportNames.AirPorts_test, function(AirportName) {
					return AirportName.cityName + " (" + AirportName.cityCode + ")";
				}));
				if (names != null) {
                
					$("#airport_text").autocomplete({
						source : names,
						minLength : 3,
                         appendTo: ".airports_main",
                        open : function(event, ui) {
                            $(".ui-autocomplete").scrollTop(0);
                        },
						select : function(event, ui) {
                        sessionStorage.searchAirport = ui.item.value;
							uiValue = ui;
							$(".ui-autocomplete").hide();
							$(this).blur();
							showActivityIndicator($.i18n.map.sky_please);

							_lat = "";
							_lon = "";
                            window.networkStatCallBack = function(){
                                var netstat = JSON.parse(localStorage.networkStatus);
                                if(netstat.network == "offline"){
                                    hideActivityIndicator();
                                    setTimeout(function(){
                                    openAlertWithOk($.i18n.map.sky_notconnected);
                                    },100);
                                } else {
                                    self.loadspecificmap(event, uiValue);
                                }
                                window.networkStatCallBack = function(){}
                            }
                            window.open(iSC.Network, "_self");
						}
					});
				}
			}
			/* End of Show Suggestive Text */
           if ($('#airport_text').val() == ''){
           $(".cross_image").css('visibility', 'hidden');
           $('.search_icon1').css('visibility', 'visible');
           }
           else{
           $(".cross_image").css('visibility', 'visible');
           $('.search_icon1').css('visibility', 'hidden');
           }
           
		},

		events : {
			'click #airport_maps' : 'displaymaps',
			'click #airport_list' : 'displaylist',
            'focus :input' : 'showCrossImage',
			'keydown :input' : 'airport_auto',
			'click .cross_image' : 'crossImage',
			'touchstart *' : function() {
				$.pageslide.close();
			}
		},
        showCrossImage : function(e) {
                                               if ($('#airport_text').val() == ''){
                                               $(".cross_image").css('visibility', 'hidden');
                                               $('.search_icon1').css('visibility', 'visible');
                                               }
                                               else{
                                               $(".cross_image").css('visibility', 'visible');
                                               $('.search_icon1').css('visibility', 'hidden');
                                               }
        },
		/* Function to Show Map */
		displaymaps : function(e) {
            this.listView = false;
            closeInfowindow = setTimeout(function() {
                closeInfos(markers);
            }, 15000);
			$("#airport_maps").addClass("onHover").removeClass("offHover");
			$("#airport_list").addClass("offHover").removeClass("onHover");
			$('.map_area_canvas').css({
				'z-index' : '10'
			});
			$('.list_maps').css({
				'z-index' : '-1'
			});
			$('.list_maps').css('display', 'none');
		},
		/* End of Function to Show Map */

		/* Function to Show List */
		displaylist : function(e) {
            this.listView = true;
            clearTimeout(closeInfowindow);
			$("#airport_list").addClass("onHover").removeClass("offHover");
			$("#airport_maps").addClass("offHover").removeClass("onHover");
			$('.map_area_canvas').css({
				'z-index' : '-1'
			});
			$('.list_maps').css({
				'z-index' : '10'
			});
			$('.list_maps').css('display', 'block');
		},
		/* End of Function to Show List */

		/* Show & Hide of Auto-complete cancel Image and Search Image */
		airport_auto : function(e) {
			$('#airport_text').keyup(function() {
				if ($('#airport_text').val() != '') {
					$('.cross_image').css('visibility', 'visible');
					$('.search_icon1').css('visibility', 'hidden');
				} else {
					$('.cross_image').css('visibility', 'hidden');
					$('.search_icon1').css('visibility', 'visible');
				}
			});
			keyval = $('#' + e.target.id).val();
			if (keyval != "") {
				sessionStorage.keyval = keyval;
			}
                                               
		},
		/* End of Show & Hide of Auto-complete cancel Image and Search Image */

		/* Display Specific Map */
		loadspecificmap : function(event, ui) {
			var self = this;
			var getCountryCode = {};
			var results = {};
			var testObj = JSON.parse(localStorage.cityList);
			$.each(testObj.AirPorts_test, function(key) {
				if (testObj.AirPorts_test[key].airportCode == ui.item.value.split('(')[1].split(')')[0]) {
					getCountryCode = testObj.AirPorts_test[key];
				}
			})
			xhrajax=$.ajax({
				//url : URLS.AIRPORT_SEARCH+'?source=Mobile&city=' + ui.item.value.split('(')[0] + '&countrycode=' + getCountryCode.countryCode + '&radius=300&version=2',
				url : URLS.AIRPORT_SEARCH+'?citycode=' + ui.item.value.split('(')[0] + '&countrycode=' + getCountryCode.countryCode + '&radius=300&version=3',
				datatype : 'json',
				async : true,
				timeout : 20000,
				success : function(result) {
					sessionStorage.city_Name = ui.item.value.split('(')[0]
					sessionStorage.airport_code = ui.item.value.split('(')[1].split(')')[0];
					var testResult = JSON.parse(result);
					if (testResult.ErrorCode) {
						
						openAlertWithOk(testResult.ErrorMessage);
                   hideActivityIndicator();
					} else {
						if (!testResult.AirportDetails.length) {
							var tempArray = [];
							if (!((testResult.AirportDetails.AirportName).match('- All airports'))) {
								tempArray.push(testResult.AirportDetails);
							}
							result = JSON.stringify(tempArray);
						} else {
							var tempArray = [];
							for (var i = 0; i < testResult.AirportDetails.length; i++) {
								if (!((testResult.AirportDetails[i].AirportName).match('- All airports'))) {
									tempArray.push(testResult.AirportDetails[i]);
								}
							}
							result = JSON.stringify(tempArray);
						}
						_lat = Number(testResult.LocationDetails.Latitude).toFixed(4);
						_lon = Number(testResult.LocationDetails.Longitude).toFixed(4);
						results = JSON.parse(JSON.parse(JSON.stringify(result)));
						if (results.length == undefined) {
							if (results.ErrorCode == undefined) {
								if (localStorage.googleMapsLoaded == "true") {
									showPosition(results);
									showList(results);
								} else {
									showList(results);
                  
                   if(!self.listView){
                   openAlertWithOk($.i18n.map.sky_loadmap);
                   }
                    hideActivityIndicator();
								}
							} else {
								
								openAlertWithOk(results.ErrorMessage);
                   hideActivityIndicator();
							}
						} else {
							if (results[0].ErrorCode == undefined) {
								if (localStorage.googleMapsLoaded == "true") {
									showPosition(results);
									showList(results);
								} else {
									showList(results);
                   
                   if(!self.listView){
                   openAlertWithOk($.i18n.map.sky_loadmap);
                   }
                   hideActivityIndicator();
								}
							} else {
								
								openAlertWithOk(results[0].ErrorMessage);
                   hideActivityIndicator();
							}
						}
						window.open(iSC.Network, "_self");
					}
				},
				error : function(xhr, status, error) {
					openAlertWithOk($.i18n.map.sky_requestfailed);
					hideActivityIndicator();
                   if(!self.listView)
					self.showEmptyMaps($.i18n.map.sky_fault);
				}
			});
			setTimeout(function() {
				if (results == null) {
					hideActivityIndicator();
				}
			}, 200);
		},
		/* End of Display Specific Map */

		/* Display Default Map */
		loadmaps : function(e) {
			var self = this;
			showActivityIndicator($.i18n.map.sky_please);
			if (sessionStorage.deviceInfo == "android") {
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(mapsposition,function(error){
                        if(error.code == 1){
                            openAlertWithOk($.i18n.map.sky_disable);
                        } else {
                            openAlertWithOk($.i18n.map.sky_loadmap);
                        }
                        hideActivityIndicator();
                        self.showEmptyMaps($.i18n.map.sky_fault);
                    },{
                    timeout: 20000,
                    maximumAge: 0
                    });
				}
				function mapsposition(position) {
					var results = {};
					_lat = Number(position.coords.latitude).toFixed(4);
					_lon = Number(position.coords.longitude).toFixed(4);
					xhrajax=$.ajax({
						//url : URLS.AIRPORT_SEARCH+'?source=Mobile&latitude=' + Number(position.coords.latitude).toFixed(4) + '&longitude=' + Number(position.coords.longitude).toFixed(4) + '&radius=300&version=2',
						url : URLS.AIRPORT_SEARCH+'?latitude=' + Number(position.coords.latitude).toFixed(4) + '&longitude=' + Number(position.coords.longitude).toFixed(4) + '&radius=300&version=3',
						datatype : 'json',
						async : true,
						timeout : 20000,
						success : function(result) {
							var testResult = JSON.parse(result);
							if (testResult.ErrorCode) {
								hideActivityIndicator();
								openAlertWithOk(testResult.ErrorMessage);
							} else {
								if (!testResult.AirportDetails.length) {
									var tempArray = [];
									if (!((testResult.AirportDetails.AirportName).match('- All airports'))) {
										tempArray.push(testResult.AirportDetails);
									}
									result = JSON.stringify(tempArray);
								} else {
									var tempArray = [];
									for (var i = 0; i < testResult.AirportDetails.length; i++) {
										if (!((testResult.AirportDetails[i].AirportName).match('- All airports'))) {
											tempArray.push(testResult.AirportDetails[i]);
										}
									}
									result = JSON.stringify(tempArray);
								}
								var results = JSON.parse(JSON.parse(JSON.stringify(result)));
								if (results[0].ErrorCode == undefined) {
									if (localStorage.googleMapsLoaded == "true") {
										showPosition(results);
										showList(results);
									} else {
										showList(results);
                           hideActivityIndicator();
                           openAlertWithOk($.i18n.map.sky_loadmap);
									}
								} else {
									hideActivityIndicator();
									openAlertWithOk(results[0].ErrorMessage);
								}
							}
						},
						error : function(xhr, status, error) {
							openAlertWithOk($.i18n.map.sky_requestfailed);
							hideActivityIndicator();
                           if(!self.listView)
							self.showEmptyMaps($.i18n.map.sky_fault);
						}
					});
				}

			} else if (sessionStorage.deviceInfo == "iPhone") {
                    delete localStorage.currentLocation;
					window.skyTipsLocationAjaxCallback = function() {
						if (localStorage.currentLocation != undefined) {
                            if(JSON.parse(localStorage.currentLocation).status === "ERROR" ){
                                hideActivityIndicator();
                                setTimeout(function(){
                                openAlertWithOk($.i18n.map.sky_loadplease);
                                },100);
                            } else {
							var results = {};
							_lat = Number(JSON.parse(localStorage.currentLocation).latitude).toFixed(4);
							_lon = Number(JSON.parse(localStorage.currentLocation).longitude).toFixed(4);
							xhrajax=$.ajax({
								//url : URLS.AIRPORT_SEARCH+'?source=Mobile&latitude=' + Number(JSON.parse(localStorage.currentLocation).latitude).toFixed(4) + '&longitude=' + Number(JSON.parse(localStorage.currentLocation).longitude).toFixed(4) + '&radius=300&version=2',
								url : URLS.AIRPORT_SEARCH+'?latitude=' + Number(JSON.parse(localStorage.currentLocation).latitude).toFixed(4) + '&longitude=' + Number(JSON.parse(localStorage.currentLocation).longitude).toFixed(4) + '&radius=300&version=3',
								datatype : 'json',
								async : true,
								timeout : 20000,
								success : function(result) {
                                if(result != undefined){
									var testResult = JSON.parse(result);
									if (testResult.ErrorCode) {
										hideActivityIndicator();
										openAlertWithOk(testResult.ErrorMessage);
									} else {
										if (!testResult.AirportDetails.length) {
											var tempArray = [];
											if (!((testResult.AirportDetails.AirportName).match('- All airports'))) {
												tempArray.push(testResult.AirportDetails);
											}
											result = JSON.stringify(tempArray);
										} else {
											var tempArray = [];
											for (var i = 0; i < testResult.AirportDetails.length; i++) {
												if (!((testResult.AirportDetails[i].AirportName).match('- All airports'))) {
													tempArray.push(testResult.AirportDetails[i]);
												}
											}
											result = JSON.stringify(tempArray);
										}
										results = JSON.parse(JSON.parse(JSON.stringify(result)));
										if (results[0].ErrorCode == undefined) {
											if (localStorage.googleMapsLoaded == "true") {
												showPosition(results);
												showList(results);
											} else {
												showList(results);
                                   hideActivityIndicator($.i18n.map.sky_loadmap);
                                   openAlertWithOk();
											}
										} else {
											hideActivityIndicator();
											openAlertWithOk(results[0].ErrorMessage);
										}
								}	}
								},
								error : function(xhr, status, error) {
									openAlertWithOk($.i18n.map.sky_requestfailed);
									hideActivityIndicator();
                                   if(!self.listView)
									self.showEmptyMaps($.i18n.map.sky_fault);
								}
							});
                            }
						}
						window.skyTipsLocationAjaxCallback = function() {
						};
					}
					window.open(iSC.skyTipsLocation, "_self");
                    var repeatSkyTipsLocationCall = setInterval(function(){
                        if(!localStorage.currentLocation) {
                            window.open(iSC.skyTipsLocation, "_self");
                        } else {
                            clearInterval(repeatSkyTipsLocationCall);
                        }
                    },3000);
                    setTimeout(function(){
                       if(!localStorage.currentLocation) {
                            openAlertWithOk($.i18n.map.sky_loadplease);
							hideActivityIndicator();
                            clearInterval(repeatSkyTipsLocationCall);
                        }
                    },20000);
				
			}
			function onError(error) {
				hideActivityIndicator();
			}

		},
		/* End of Display Default Map */

		crossImage : function(e) {
			$("#airport_text").val("");
			$(".cross_image").css("visibility", "hidden");
			$('.search_icon1').css('visibility', 'visible');
            $("#airport_text").focus();
		},
		showEmptyMaps : function(message) {
			$('#map_canvas').empty().removeAttr("style").append($('<div></div>').append($('<p></p>').text(message).addClass("map_message")).addClass("map_container"));
		}
	});
	return airportPageView;
});

/* Load Map */
function showPosition(results) {
	var airportslist = "";
	var lat = "";
	var lng = "";
	var distance;
	$.each(results, function(key) {

		if (results[key].AirportCode == sessionStorage.airport_code) {
			lat = results[key].Latitude;
			lng = results[key].Longitude;
		} else {
			lat = results[0].Latitude;
			lng = results[0].Longitude;
			sessionStorage.airport_code = results[0].AirportCode;
		}
	})
	var mapOptions = {
		zoom : 5,
		center : new google.maps.LatLng(lat, lng),
		zoomControl : true,
		streetViewControl : false,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		disableDefaultUI : true

	};
	var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
	$.each(results, function(key) {

		if (localStorage.distance == "miles") {
			distance = distanceConvertor(results[key].Distance) + " miles";
		} else {
			distance = results[key].Distance + " km";
		}
        var image = {
    url: './images/map_20x20.png',
    // This marker is 20 pixels wide by 32 pixels tall.
    size: new google.maps.Size(20, 32)
  };

		if (sessionStorage.deviceInfo == "android") {
			if (_lat != "" && _lon != "") {

				var mycenter = new google.maps.LatLng(_lat, _lon);
                				var marker = new google.maps.Marker({
					position : mycenter,
					title : 'Click to zoom',
					icon : image,
                    
				});
				marker.setMap(map);
			}
		}
		if (sessionStorage.deviceInfo == "iPhone") {
			if (_lat != "" && _lon != "") {
				var mycenter = new google.maps.LatLng(_lat, _lon);
				var marker = new google.maps.Marker({
					position : mycenter,
					title : 'Click to zoom',
					icon : image,
				});
				marker.setMap(map);
			}
		}
		if (results[key].AirportCode == sessionStorage.airport_code) {
           var contentString = '<div class="info_map" id="infoMarker_' + results[key].Latitude + '" style="background: url(images/purple_arrow.png) no-repeat;background-size: 10px 12px;background-position: 100% 25%;">' + results[key].AirportName + ' ' + distance + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';
			var infowindow = new google.maps.InfoWindow({
				content : contentString
			});
			var mycenter = new google.maps.LatLng(results[key].Latitude, results[key].Longitude);
			var marker = new google.maps.Marker({
				position : mycenter,
				title : 'Click to zoom',
				icon : './images/airportMarker_new.png',
			});
			marker.setMap(map);
			setTimeout(function() {
				closeInfos(markers);
				infowindow.open(map, marker);
                       google.maps.event.addListener(infowindow,'domready', function(){
                                          $('.info_map').parent().parent().css('width','92%').next().remove();
                                                     
                                               });
                      
				$("div[id^='infoMarker_']").off("click").on("click", function() {
					saveData(results[key]);
				});

                
                    var $transparent_image = $($("img[src='https://maps.gstatic.com/mapfiles/transparent.png']")[0]);
                            $transparent_image.unbind('click').bind('click', function(e) {
                            saveData(results[key]);
                        });
                
                clearTimeout(closeInfowindow);
                closeInfowindow = setTimeout(function() {
                                    closeInfos(markers);
                                  }, 15000);
			},1000);
			google.maps.event.addListener(marker, 'click', function() {
				closeInfos(markers);
				infowindow.open(map, marker);
                        setTimeout(function() {
                        $($("img[src='https://maps.gstatic.com/mapfiles/transparent.png']")[0]).unbind('click').bind('click', function(e) {
						saveData(results[key]);
					});
				}, 50);
				$("div[id^='infoMarker_']").off("click").on("click", function() {
					saveData(results[key]);
				});
                clearTimeout(closeInfowindow);
				closeInfowindow = setTimeout(function() {
                                    closeInfos(markers);
                                  }, 15000);
			});
			markers.push(infowindow);
		} else {
			var mycenter = new google.maps.LatLng(results[key].Latitude, results[key].Longitude);
			var marker = new google.maps.Marker({
				position : mycenter,
				title : 'Click to zoom',
				icon : './images/airportMarker_new.png',
			});
			marker.setMap(map);
			var contentString1 = '<div class="info_map" id="infoMarker_' + key + '" style="background: url(images/purple_arrow.png) no-repeat;background-size: 10px 12px;background-position: 100% 25%;">' + results[key].AirportName + ' ' + distance + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';

			var infowindow = new google.maps.InfoWindow({
				content : contentString1
			});
			markers.push(infowindow);
			google.maps.event.addListener(marker, 'click', function() {
				closeInfos(markers);
				infowindow.open(map, marker);
                                          google.maps.event.addListener(infowindow,'domready', function(){
                                                 $('.info_map').parent().parent().css('width','92%').next().remove();
                                                                        
                                                                        });

                                          
				setTimeout(function() {
                           
                        $($("img[src='https://maps.gstatic.com/mapfiles/transparent.png']")[0]).unbind('click').bind('click', function(e) {
						saveData(results[key]);
					});
				}, 50);
				$("div[id^='infoMarker_']").off("click").on("click", function() {
					saveData(results[key]);
				});
                clearTimeout(closeInfowindow);
				closeInfowindow = setTimeout(function() {
                                     closeInfos(markers);
                                  }, 15000000);
			});
		}

	});
	hideActivityIndicator();
}

/* End of Load Map */

/* Close Infowindow */
function closeInfos(markers) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].close();
    }
}
/* End of Close Infowindow */

/* Load List */
function showList(results) {
	var markers = [];
	var airportslist = "";
	var lat = "";
	var lng = "";
	var distance;
	$.each(results, function(key) {

		if (results[key].AirportCode == sessionStorage.airport_code) {
			lat = results[key].Latitude;
			lng = results[key].Longitude;
		} else {
			lat = results[0].Latitude;
			lng = results[0].Longitude;
			sessionStorage.airport_code = results[0].AirportCode;
		}
	})
	$.each(results, function(key) {
		if (localStorage.distance == "miles") {
			distance = distanceConvertor(results[key].Distance) + " miles";
		} else {
			distance = results[key].Distance + " km";
		}
		if (results[key].AirportCode == sessionStorage.airport_code) {

			airportslist += ' <div class="airport_list" name="' + results[key].AirportName + ',' + results[key].CountryName + ',' + results[key].CountryCode + ',' + results[key].AirportCode + ',' + results[key].AirportDetailsURL + ',' + results[key].CityName + '"><ul><li><p class="list_name">' + results[key].AirportName + '</p><p></p><p class="list_distance">' + distance + '</p></li></ul></div>';
			var contentString1 = '<div class="info_map" id="infoMarker_' + results[key].Latitude + '" style="background: url(images/purple_arrow.png) no-repeat;background-size: 10px 12px;background-position: 100% 25%;">' + results[key].AirportName + ' ' + distance + '</div>';
		} else {
			airportslist += ' <div class="airport_list" name="' + results[key].AirportName + ',' + results[key].CountryName + ',' + results[key].CountryCode + ',' + results[key].AirportCode + ',' + results[key].AirportDetailsURL + ',' + results[key].CityName + '"><ul><li><p class="list_name">' + results[key].AirportName + '</p><p></p><p class="list_distance">' + distance + '</p></li></ul></div>';
			var contentString = '<div id="infoMarker_' + key + '">' + results[key].AirportName + ' ' + distance + '</div>';
		}
           
		$('.list_maps').html(airportslist);
		$(".airport_list").click(function(e) {
			showActivityIndicator($.i18n.map.sky_please);
            
			var obj = {};
			sessionStorage.airport_code = $(e.currentTarget).attr('name').split(',')[3];
			sessionStorage.airportName = $(e.currentTarget).attr('name').split(',')[0];
			obj.airportName = $(e.currentTarget).attr('name').split(',')[0];
			obj.countryName = $(e.currentTarget).attr('name').split(',')[1];
			obj.countryCode = $(e.currentTarget).attr('name').split(',')[2];
			obj.airportCode = $(e.currentTarget).attr('name').split(',')[3];
			obj.airportDetails_URL = $(e.currentTarget).attr('name').split(',')[4];
			obj.cityName = $(e.currentTarget).attr('name').split(',')[5];
			sessionStorage.airportDetailsObj = JSON.stringify(obj);
			window.location.href = "index.html#airportDetails";
			var obj = {};
			obj.previousPage = "airportSearchPage";
			pushPageOnToBackStack(obj);
		});
	});
}

/* End of Load List */

/* Save Airport Data and Navigate to Airport Details Page */
function saveData(resp) {
	var obj = {};
	showActivityIndicator($.i18n.map.sky_please);
	sessionStorage.airportName = resp.AirportName;
	sessionStorage.airport_code = resp.AirportCode;
	obj.airportName = resp.AirportName;
	obj.countryName = resp.CountryName;
	obj.airportCode = resp.AirportCode;
	obj.countryCode = resp.CountryCode;
	obj.airportDetails_URL = resp.AirportDetailsURL;
	obj.cityName = resp.CityName;
	sessionStorage.airportDetailsObj = JSON.stringify(obj);
	window.location.href = "index.html#airportDetails";

	var obj = {};
	obj.previousPage = "airportSearchPage";
	pushPageOnToBackStack(obj);

}

/* End of Save Airport Data and Navigate to Airport Details Page */

/* End of Airports Functionality */