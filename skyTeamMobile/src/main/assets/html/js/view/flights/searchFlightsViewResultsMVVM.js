define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/searchFlightResultList.html','text!template/searchFlightResultsMVVM.html','models/flights/searchFlightResultsModel','view/flights/searchFlightView','appView'],
       function($, _, Backbone, utilities, constants, jqueryUi, searchFlightResultList,searchResultsTemplate, searchFlightResultsModel,searchFlightView,appView) {

       var self,searchResultsView = appView.extend({
            transitionRequired:true,
            initialize : function() {

            /*back button*/
            $('#backButtonList').show();

//            $('#headingText img').hide();
//            $('#headingText h2').show();
//            $('#headingText h2').html($.i18n.map.sky_search);

//Code changed for initiate booking new design -19-05-2017 --Justin
              $('#headingText img').hide();
              $('#headingText h2').show();

                 // $('#headingText h2').html($.i18n.map.sky_search);

//
// var hTxt='<div class="newheaderrow"><div class="newdepname">';
//              hTxt+='<div id="mbl-dep-code">'+sessionStorage.source+'</div></div><div class="headerflighticon">';
//              hTxt+='<div class="icon-flight head" id="mblfr"></div><div class="icon-flight head" id="mblbk">';
//              hTxt+='</div></div><div class="newarrname"><div id="mbl-ret-code">'+sessionStorage.destination+'</div></div></div>';
//             $('#headingText h2').html(hTxt);


            $('#brief h2').hide();

            if (sessionStorage.savedflights == "yes" || sessionStorage.isFromSaved == "yes") {
            $('#brief img').show();
            sessionStorage.savedflights = "no";
            sessionStorage.isFromSaved = "no";
            }
            var currentPage=JSON.parse(sessionStorage.previousPages);
            if(currentPage[0].previousPage == "savedFlightsOrAirports"){
            $('#brief img').hide();
            }
            self = this;

            var dataModel = new searchFlightResultsModel();
            var response = JSON.parse(localStorage.searchResults);

            if(response.journey === 'oneway'){
            sessionStorage.oneway_or_return ="oneway";
            delete response.journey;
            dataModel.set({'journeyType':'oneway'});
            dataModel.set(response);


 var hTxt='<div class="newheaderrow"><div class="newdepname">';
 var headerIconClass;

                                                    var aptCodeSrc;

                                                    var aptCodeDes;



                                                    if((sessionStorage.sourceLocationType!="CTY")&&(sessionStorage.destinationLocationType!="CTY")){

                                                    headerIconClass="headerflighticon-withoutAirport";

                                                    aptCodeSrc="mbl-dep-code-Without";

                                                    aptCodeDes="mbl-ret-code-Without";





                                                    }

                                                    else{

                                                    headerIconClass="headerflighticon";

                                                    aptCodeSrc="mbl-dep-code";

                                                    aptCodeDes="mbl-ret-code";

                                                    }

                                                    if(sessionStorage.sourceLocationType=="CTY"){



                                                    var airportNameSrclength=sessionStorage.sourceAirportName.split("-").length;



                                                    var airportNameSrc=sessionStorage.sourceAirportName.split("-")[airportNameSrclength-1];



                                                    if(airportNameSrclength==1){

                                                         airportNameSrclength=sessionStorage.sourceAirportName.split("–").length;

                                                         airportNameSrc=sessionStorage.sourceAirportName.split("–")[airportNameSrclength-1];



                                                    if(airportNameSrclength==1){

                                                    airportNameSrclength=sessionStorage.sourceAirportName.split("－").length;

                                                    airportNameSrc=sessionStorage.sourceAirportName.split("－")[airportNameSrclength-1];

                                                    }



                                                    }





                                                    hTxt+='<div id="'+aptCodeSrc+'">'+sessionStorage.source+'</div><div id="mbl-dep-name">'+airportNameSrc+'</div></div><div class="'+headerIconClass+'">';



                                                    }

                                                    else{

                                                    hTxt+='<div id="'+aptCodeSrc+'">'+sessionStorage.source+'</div></div><div class="'+headerIconClass+'">';

                                                    }



                                                    hTxt+='<div class="icon-flight head" id="mblfr" style="margin-top:8px"></div><div class="icon-flight head" id="mblbk" style="display:none">';



                                                    if(sessionStorage.destinationLocationType=="CTY"){



                                                    var airportNameDeslength=sessionStorage.destinationAirportName.split("-").length;

                                                    var airportNameDes=sessionStorage.destinationAirportName.split("-")[airportNameDeslength-1];



                                                    if(airportNameDeslength==1){

                                                    airportNameDeslength=sessionStorage.destinationAirportName.split("–").length;

                                                    airportNameDes=sessionStorage.destinationAirportName.split("–")[airportNameDeslength-1];



                                                    if(airportNameDeslength==1){

                                                    airportNameDeslength=sessionStorage.destinationAirportName.split("－").length;

                                                    airportNameDes=sessionStorage.destinationAirportName.split("－")[airportNameDeslength-1];

                                                    }



                                                    }



                                                    hTxt+='</div></div><div class="newarrname"><div id="'+aptCodeDes+'">'+sessionStorage.destination+'</div><div id="mbl-arr-name">'+airportNameDes+'</div></div></div>';

                                                    }



                                                    else{

                                                    hTxt+='</div></div><div class="newarrname"><div id="'+aptCodeDes+'">'+sessionStorage.destination+'</div></div></div>';

                                                    }




//              hTxt+='<div id="mbl-dep-code">'+sessionStorage.source+'</div><div id="mbl-dep-name">'+sessionStorage.sourceAirportName+'</div></div><div class="headerflighticon">';
//              hTxt+='<div class="icon-flight head" id="mblfr" style="margin-top:8px"></div><div class="icon-flight head" id="mblbk" style="display:none">';
//             hTxt+='</div></div><div class="newarrname"><div id="mbl-ret-code">'+sessionStorage.destination+'</div><div id="mbl-arr-name">'+sessionStorage.destinationAirportName+'</div></div></div>';
//


             $('#headingText h2').html(hTxt);
            }
            else{
            sessionStorage.oneway_or_return ="return";
            dataModel.set({'journeyType':'roundtrip'});
            dataModel.set(response);


 var hTxt='<div class="newheaderrow"><div class="newdepname">';


 var headerIconClass;

                                                    var aptCodeSrc;

                                                    var aptCodeDes;



                                                    if((sessionStorage.sourceLocationType!="CTY")&&(sessionStorage.destinationLocationType!="CTY")){

                                                    headerIconClass="headerflighticon-withoutAirport";

                                                    aptCodeSrc="mbl-dep-code-Without";

                                                    aptCodeDes="mbl-ret-code-Without";





                                                    }

                                                    else{

                                                    headerIconClass="headerflighticon";

                                                    aptCodeSrc="mbl-dep-code";

                                                    aptCodeDes="mbl-ret-code";

                                                    }



                                                    if(sessionStorage.sourceLocationType=="CTY"){



                                                    var airportNameSrclength=sessionStorage.sourceAirportName.split("-").length;

                                                    var airportNameSrc=sessionStorage.sourceAirportName.split("-")[airportNameSrclength-1];



                                                    if(airportNameSrclength==1){

                                                    airportNameSrclength=sessionStorage.sourceAirportName.split("–").length;

                                                    airportNameSrc=sessionStorage.sourceAirportName.split("–")[airportNameSrclength-1];



                                                    if(airportNameSrclength==1){

                                                    airportNameSrclength=sessionStorage.sourceAirportName.split("－").length;

                                                    airportNameSrc=sessionStorage.sourceAirportName.split("－")[airportNameSrclength-1];

                                                    }

                                                    }



                                                    hTxt+='<div id="'+aptCodeSrc+'">'+sessionStorage.source+'</div><div id="mbl-dep-name">'+airportNameSrc+'</div></div><div class="'+headerIconClass+'">';

                                                    }

                                                    else{

                                                    hTxt+='<div id="'+aptCodeSrc+'">'+sessionStorage.source+'</div></div><div class="'+headerIconClass+'">';

                                                    }



                                                    hTxt+='<div class="icon-flight head" id="mblfr"></div><div class="icon-flight head" id="mblbk">';

                                                    if(sessionStorage.destinationLocationType=="CTY"){



                                                    var airportNameDeslength=sessionStorage.destinationAirportName.split("-").length;

                                                    var airportNameDes=sessionStorage.destinationAirportName.split("-")[airportNameDeslength-1];



                                                    if(airportNameDeslength==1){

                                                    airportNameDeslength=sessionStorage.destinationAirportName.split("–").length;

                                                    airportNameDes=sessionStorage.destinationAirportName.split("–")[airportNameDeslength-1];



                                                    if(airportNameDeslength==1){

                                                    airportNameDeslength=sessionStorage.destinationAirportName.split("－").length;

                                                    airportNameDes=sessionStorage.destinationAirportName.split("－")[airportNameDeslength-1];

                                                    }

                                                    }



                                                    hTxt+='</div></div><div class="newarrname"><div id="'+aptCodeDes+'">'+sessionStorage.destination+'</div><div id="mbl-arr-name">'+airportNameDes+'</div></div></div>';

                                                    }

                                                    else{

                                                    hTxt+='</div></div><div class="newarrname"><div id="'+aptCodeDes+'">'+sessionStorage.destination+'</div></div></div>';

                                                    }



//              hTxt+='<div id="mbl-dep-code">'+sessionStorage.source+'</div><div id="mbl-dep-name">'+sessionStorage.sourceAirportName+'</div></div><div class="headerflighticon">';
//              hTxt+='<div class="icon-flight head" id="mblfr"></div><div class="icon-flight head" id="mblbk">';
//              hTxt+='</div></div><div class="newarrname"><div id="mbl-ret-code">'+sessionStorage.destination+'</div><div id="mbl-arr-name">'+sessionStorage.destinationAirportName+'</div></div></div>';
             $('#headingText h2').html(hTxt);
            }
            /* back from flight details to show previous shown flight list(departure/arrival)
            * sessionStorage.showList should have either one of the following values
            * "Onward" -- to show departure flight list
            * "Return" -- to show return flight list
            */
            dataModel.set({'currentTravel':sessionStorage.showList});
            /* back from flight details to show previous shown flight list(departure/arrival)*/
            dataModel.resetData();
             dataModel.set($.i18n.map);
            //setting the Duration as the default tab;
            dataModel.set({'currentTab':'Duration'});
            this.model =dataModel;

               if(dataModel.get("journeyType") == "roundtrip" && sessionStorage.showList === "Onward" && sessionStorage.isSixDaysReturnPopupShown === "yes"){
                   if(dataModel.get('currentList').length  !== dataModel.get('returnFlights').trips.length){
                   sessionStorage.show7DaysArrival = "yes";
                   }
               }

            },
            events:{
            'click .tab':'changeTab',
            'click  #oneWayFilter' : 'openFilterOptions',
            'click  #returnFlightsBtn' : 'showReturnFlights',
            'click  #outboundFlightsBtn' : 'showOutboundFlights',
            'click  .savedFlightsResults' : 'openFlightDetails',
            'click  .booking_class' : 'booking_function',
             'click  .altdate' : 'suggestedflightResults',
            'touchstart *' : function() {
            $.pageslide.close();
            }

            },
            render:function(){

            var compiledTemplate = _.template(searchResultsTemplate,this.model.toJSON());


            $(this.el).html(compiledTemplate);
             /* sorting the flight list based on the selected tab ,default duration*/
             var flightList = this.tabFilter(this.model.get('currentList'));
             var isFlightAvailable ;
             if("Onward" === self.model.get("currentTravel")){
                 isFlightAvailable = sessionStorage.sixDaysOnward;

//                 if("false" === sessionStorage.sixDaysReturn){
//                 self.$el.find('#returnFlightsBtn').html($.i18n.map.sky_returnflight+"(0)");
//                 }

                 sessionStorage.sixDaysOnward = "true";


             }
             else{
             isFlightAvailable = sessionStorage.sixDaysReturn;

//             if("false" === sessionStorage.sixDaysOnward){
//             self.$el.find('#outboundFlightsBtn').html($.i18n.map.sky_departflight+"(0)");
//             }

             sessionStorage.sixDaysReturn = "true";//resettting


             }

            if("true" === isFlightAvailable || self.model.get('journeyType')!== "roundtrip"){//one way trip
             /* binding the flight list*/


             this.bindFlightListTemplate(flightList);
             /* binding the flight list*/

            }
            else{///round trip
               if(self.model.get(self.model.get("currentTravel")).ErrorCode){//Any error codes(3203,3204,3289,3010)

                 this.bindFlightListTemplate([]);
               }
               else{

               //  this.openUserSuggestNoFlight(flightList);
                 this.bindFlightListTemplate(flightList);
               }

            }
            hideActivityIndicator();
            sessionStorage.currentPage="searchResults";
            /*Back button functionality */
            $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
            backbuttonlog=1;
              if (sessionStorage.currentPage == "searchResults") {
              sessionStorage.show7DaysArrival ="no";
              sessionStorage.show7DaysDeparture ="no";
              show7DaysDep = "no";
              show7DaysReturn = "no";
              filterRes = undefined;
              e.preventDefault();
              e.stopImmediatePropagation();
              sessionStorage.removeItem('filter');
              sessionStorage.removeItem('Res');
              var currentobj = JSON.parse(sessionStorage.previousPages);
              showActivityIndicator($.i18n.map.sky_please);
              sessionStorage.searchData=currentobj[0].flightSearch;
              window.location.href = "index.html#" + currentobj[0].previousPage;

              popPageOnToBackStack();
              }

              if (sessionStorage.currentPage == "searchFlights")
              sessionStorage.savedflights = "yes";
              sessionStorage.removeItem('previousPage');

              $(self.el).undelegate();
              });
            /* End of Back functionality */
            if(self.transitionRequired){
             /*
              *hiding the bottom departure flights/return  flights button
              *showing after the animation ends
              */
             if(self.model.get("journeyType") === "roundtrip"){
                 $('.search_btn').hide();
                 setTimeout(function(){
                    $('.search_btn').show();
                  },350);
                 }
             //slideThePage(sessionStorage.navDirection);
             self.transitionRequired=false;
            }
               return appView.prototype.render.apply(this, arguments);

            },
            changeTab:function(){
                   var currentFlightList = self.model.get("currentList");
                   var fliteredList ='', tabName ='';
                   if(event)
                   tabName = event.target.id;

                   self.$el.find('.tab.onHover').removeClass('onHover').addClass('offHover');
                   self.$el.find('#'+tabName).removeClass('offHover').addClass('onHover');
                     if("arrivalTab" === tabName){
                         self.model.set({'currentTab':"Arrival"});
                     }
                     else if("departureTab" === tabName){
                         self.model.set({'currentTab':"Departure"});
                     }
                     else if("durationTab" === tabName){
                         self.model.set({'currentTab':"Duration"});
                     }
                 fliteredList = self.tabFilter(currentFlightList);

                 self.bindFlightListTemplate(fliteredList);

            },
            tabFilter:function(list){
                var selectedTab = self.model.get('currentTab');
                 if (list.length === undefined)
                 {
                     list = $.makeArray(list);
                  }
                if("Arrival" === selectedTab){
                /* sorting based on Arrival Date  and Time */
                list = list.sort(function(a,b){

                                     if(new Date(a.flights[a.totalNoTransfers].ArrivalDateAndTime) >= new Date(b.flights[b.totalNoTransfers].ArrivalDateAndTime)){
                                     return 1;
                                     }
                                     else{
                                     return -1;
                                     }

                                     });
                }
                 else if("Departure" === selectedTab){
                 /* sorting based on Departure Date  and Time */
                 list = list.sort(function(a,b){

                                  if(new Date(a.flights[0].DepartureDateAndTime) >= new Date(b.flights[0].DepartureDateAndTime)){
                                  return 1;
                                  }
                                  else{
                                  return -1;
                                  }

                                  });
                 }
                 else{//Default Tab Duration(First tab)
                 /* sorting based on duration time */

                 list = list.sort(function(a,b){
                                  var dur1,dur2;
                                  dur1 =new Date(a.flights[0].DepartureDateAndTime) - new Date(a.flights[a.totalNoTransfers].ArrivalDateAndTime);
                                  dur2 =new Date(b.flights[0].DepartureDateAndTime) - new Date(b.flights[b.totalNoTransfers].ArrivalDateAndTime);
                                  if(dur1 <= dur2){
                                  return 1;
                                  }
                                  else{
                                  return -1;
                                  }

                                  });
                 }
                 return list;
            },

             bindFlightListTemplate:function(list){


             var _generatedHTML='';
             if(list.length >0){
             if("Return" === self.model.get('currentTravel')){


                sessionStorage.showDays = sessionStorage.show7DaysArrival;
             }
             else{

                sessionStorage.showDays = sessionStorage.show7DaysDeparture;
             }

/******* interline */
 if(sessionStorage.isinterline == "yes"){
for(var i=0,len=list.length;i<len;i++){
if(list[i].interDuplicate=="true"){
list.splice(i,1);
i=0;
len=list.length;
}}}
/******* interline */

                for(var i=0,len=list.length;i<len;i++){
                     list[i].sky_duration = $.i18n.map.sky_duration;
                     list[i].sky_fileson = $.i18n.map.sky_fileson;
                     list[i].sky_flights_searchResults = $.i18n.map.sky_flights_searchResults;
                     _generatedHTML = _generatedHTML + _.template(searchFlightResultList,list[i]);

                 }
             }
            else{

                   var errorCode = self.model.get(self.model.get("currentTravel")).ErrorCode;
                   if(errorCode && (errorCode == "3203" || errorCode == "3010") ){
                   _generatedHTML ="<p class='class_service_failed'>"+$.i18n.map.sky_flights_onward_3203+"</p>";
                   }
                   else if(errorCode && (errorCode == "3204" || errorCode == "3010") ){
                   _generatedHTML ="<p class='class_service_failed'>"+$.i18n.map.sky_flights_return_3204+"</p>";
                   }
                   else if((self.model._previousAttributes.schedules.onwardSchedules.Errors !=undefined)&&(self.model._previousAttributes.schedules.returnSchedules == undefined)){







                                                                      if (self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Code=="30401"){

                                                                      _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Description.replace(':','')+": <span class=altdate>"+self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Next_Availability.Date+"</span></p>";

                                                                      self.$el.find('.searchresultstabs').css('visibility', 'hidden');

                                                                      self.$el.find('#oneWayFilter').css('visibility', 'hidden');
                   }

                                                                      }

                   else if((self.model._previousAttributes.schedules.onwardSchedules.Errors !=undefined)&&(self.model._previousAttributes.schedules.returnSchedules.Errors !=undefined)){
                    if((self.model.get("currentTravel")) == "Return"){
                    if (self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Code=="30401"){//Change for Undefined


                      if(self.model._previousAttributes.schedules.returnSchedules.Errors[0].Next_Availability.Date==undefined){
                                             _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Title+"</p>";
                                              }
                                             else{
                    _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Description+" <span class=altdate>"+self.model._previousAttributes.schedules.returnSchedules.Errors[0].Next_Availability.Date+"</span></p>";
                    }
                    self.$el.find('.searchresultstabs').css('visibility', 'hidden');
                     self.$el.find('#oneWayFilter').css('visibility', 'hidden');

                      }
                    }else{
                    if (self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Code=="30401"){

                    _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Description+" <span class=altdate>"+self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Next_Availability.Date+"</span></p>";
                     self.$el.find('.searchresultstabs').css('visibility', 'hidden');
                     self.$el.find('#oneWayFilter').css('visibility', 'hidden');

                    }
                    }
                    }
                   else if(self.model._previousAttributes.schedules.onwardSchedules.Errors !=undefined){
                   if (self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Code=="30401"){
                    _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Description+": <span class=altdate>"+self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Next_Availability.Date+"</span></p>";
  self.$el.find('.searchresultstabs').css('visibility', 'hidden');
                                    self.$el.find('#oneWayFilter').css('visibility', 'hidden');

                    }
                     }
else if(self.model._previousAttributes.schedules.returnSchedules.Errors !=undefined){
                                        if (self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Code=="30401"){
                                        //Change for Undefined
                                         if(self.model._previousAttributes.schedules.returnSchedules.Errors[0].Next_Availability.Date==undefined){
                                         _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Title+"</p>";
                                          }
                                          else{

                                         _generatedHTML ="<p class='class_service_failed'>"+self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Description+": <span class=altdate>"+self.model._previousAttributes.schedules.returnSchedules.Errors[0].Next_Availability.Date+"</span></p>";
  self.$el.find('.searchresultstabs').css('visibility', 'hidden');
   }
                                    self.$el.find('#oneWayFilter').css('visibility', 'hidden');


                                          }
                                          }
                   else{
                    _generatedHTML ="<p class='class_service_failed'>"+$.i18n.map.sky_requestfailed+"</p>";
                   }
            }
             self.$el.find("#flightCount").html(list.length);
             self.$el.find(".searchresultslist").html('');
             self.$el.find(".searchresultslist").html(_generatedHTML);

             },
             openFilterOptions:function(e){
              if(self.model.get("currentList").length > 0){
				   sessionStorage.showDays = "no";//resetting the seven days schedule
                 self.$el.find("#blackshade").show();
                 self.$el.find('#blackshade').css({
                                      opacity : 0.6,
                                      'width' : $(document).width(),
                                      'height' : $(document).height()
                                      });
                 self.model.set({"flightList":self.model.get('currentList')});
                 require(['view/flights/subViews/moreOptionsView'], function(optionsView) {
                         var optionsViewObj = new optionsView();
                         optionsViewObj.render(self);});
                }
             },

             suggestedflightResults:function(e){


                    if(self.model._previousAttributes.schedules.onwardSchedules.Errors !=undefined){
                    if (self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Business_Error_Code=="30401"){
                     sessionStorage.depDate=self.model._previousAttributes.schedules.onwardSchedules.Errors[0].Next_Availability.Date;
                    }
                     }

if(self.model._previousAttributes.schedules.returnSchedules!=undefined){
                     if(self.model._previousAttributes.schedules.returnSchedules.Errors !=undefined){
                     if (self.model._previousAttributes.schedules.returnSchedules.Errors[0].Business_Error_Code=="30401"){
                     sessionStorage.returndate=self.model._previousAttributes.schedules.returnSchedules.Errors[0].Next_Availability.Date;
}
                     }
                     }
else{
                                                   sessionStorage.returndate="";
                                                   }

                                                   //debugger
                                                   //Departure Date greater than Return Date. //Change
                                                   if((sessionStorage.returndate!="")||(sessionStorage.returndate!=undefined)){

                                                   if((sessionStorage.isFrom=="roundtrip")&&(sessionStorage.returndate=="undefined")){

                                                   backbuttonlog=1;
                                                   sessionStorage.returndate=sessionStorage.depDate;
                                                   sessionStorage.show7DaysArrival ="no";
                                                   sessionStorage.show7DaysDeparture ="no";
                                                   show7DaysDep = "no";
                                                   show7DaysReturn = "no";
                                                   filterRes = undefined;
                                                   e.preventDefault();
                                                   e.stopImmediatePropagation();
                                                   sessionStorage.removeItem('filter');
                                                   sessionStorage.removeItem('Res');
                                                   var currentobj = JSON.parse(sessionStorage.previousPages);
                                                   showActivityIndicator($.i18n.map.sky_please);
                                                   sessionStorage.searchData=currentobj[0].flightSearch;
                                                   window.location.href = "index.html#" + currentobj[0].previousPage;

                                                   popPageOnToBackStack();


                                                   }

                                                   else if((sessionStorage.depDate>sessionStorage.returndate)&& (sessionStorage.returndate!="")){
                                                   //alert("Suggested-Scenario");
                                                   //debugger
                                                   backbuttonlog=1;
                                                   sessionStorage.returndate=sessionStorage.depDate;
                                                   sessionStorage.show7DaysArrival ="no";
                                                   sessionStorage.show7DaysDeparture ="no";
                                                   show7DaysDep = "no";
                                                   show7DaysReturn = "no";
                                                   filterRes = undefined;
                                                   e.preventDefault();
                                                   e.stopImmediatePropagation();
                                                   sessionStorage.removeItem('filter');
                                                   sessionStorage.removeItem('Res');
                                                   var currentobj = JSON.parse(sessionStorage.previousPages);
                                                   showActivityIndicator($.i18n.map.sky_please);
                                                   sessionStorage.searchData=currentobj[0].flightSearch;
                                                   window.location.href = "index.html#" + currentobj[0].previousPage;

                                                   popPageOnToBackStack();

                                                   }

                                                   }


//
//location.reload();


//            var filterRes;
//            if(sessionStorage.Res != undefined){
//                filterRes = JSON.parse(sessionStorage.Res);
//            }
//            var searchflightsresultsView = new searchResults();
//                    searchflightsresultsView.arguments = filterRes;
//            viewNavigator.goto(searchflightsresultsView);
 var dataModelsearchFlightView = new searchFlightView();
              dataModelsearchFlightView.searchResults(e);
              wait();
//setTimeout(function(){
//alert(sessionStorage.successindicator);
//
//                   var newFragment = Backbone.history.getFragment($(this).attr('href'));
//                       if (Backbone.history.fragment == newFragment) {
//                           // need to null out Backbone.history.fragement because
//                           // navigate method will ignore when it is the same as newFragment
//                           Backbone.history.fragment = null;
//                           Backbone.history.navigate(newFragment, true);
//                       };
//
//                  },5000);

function wait(){
  if (sessionStorage.successindicator==0){
    setTimeout(wait,100);
  } else {
    var newFragment = Backbone.history.getFragment($(this).attr('href'));
                           if (Backbone.history.fragment == newFragment) {
                               // need to null out Backbone.history.fragement because
                               // navigate method will ignore when it is the same as newFragment
                               Backbone.history.fragment = null;
                               Backbone.history.navigate(newFragment, true);
                           };

                           if(($('#outboundFlightsBtn').css('display') == 'block') && ($('#returnFlightsBtn').css('display') == 'block'))
                           {
                           self.showReturnFlights();


                           }

  }
}

  if(self.$el.find('div.searchresultslist p').hasClass('class_service_failed')){
                                    self.$el.find('.searchresultstabs').css('visibility', 'hidden');
                                    self.$el.find('#oneWayFilter').css('visibility', 'hidden');

                                                    }else{
                                                         self.$el.find('.searchresultstabs').css('visibility', 'visible');
                                                         self.$el.find('#oneWayFilter').css('visibility', 'visible');

                                                    }
             },


booking_function:function(e){

e.preventDefault();
e.stopPropagation();
if($(e.currentTarget).data('bookingurl')==''){
        return;
       }


var book_Ori=sessionStorage.source;
var book_Des=sessionStorage.destination;
var book_OriDate=sessionStorage.depDate;
var book_RetDate=sessionStorage.returndate;
var book_lan=localStorage.language;
var oneortwo_way=sessionStorage.oneway_or_return;
var marketing_Code=$(e.currentTarget).data('marketingcode');
var bookingURL = $(e.currentTarget).data('bookingurl');

var analyticsBtn="analytics://_trackEvent#"+"initiatebooking#"+"button_press#bookbtn-"+marketing_Code+"/"+localStorage.language+"#"+bookingURL;

                                                     setTimeout(function() {

                                                                window.open(analyticsBtn, "_self");

                                                                }, 1000)


if(marketing_Code=="KQ" || marketing_Code=="CI" || marketing_Code=="MF"){

if(marketing_Code=="KQ"){
var kenyaLanguage = ['FR', 'EN', 'SP', 'IT', 'CN'];

                              if (book_lan.toLocaleLowerCase() == "zh" || book_lan.toLocaleLowerCase() == "zh-tw") {
                              book_lan = "CN";
                              }
                              if (book_lan.toLocaleLowerCase() == "es") {
                               book_lan = "SP";
                                }

                             if(kenyaLanguage.indexOf(book_lan.toLocaleUpperCase())<0){
                             book_lan="EN";
                             }

book_lan=book_lan.toLocaleUpperCase()
window.MyJSClient.getStringFromJS(bookingURL,book_Ori,book_Des,book_OriDate,book_RetDate,book_lan,oneortwo_way,marketing_Code);
}else if(marketing_Code=="CI"){
window.MyJSClient.getStringFromJS(bookingURL,book_Ori,book_Des,book_OriDate,book_RetDate,book_lan,oneortwo_way,marketing_Code);
} else if(marketing_Code=="MF"){
var locale = "En-eu";//var locale =urlM;
                                             var response='';
                                             // var apiUrl = 'http://www.xiamenair.com/';
                                             var apiUrl = 'https://services.skyteam.com/v1/booking/xiamen?'; //( WITH XIAMEN TIBCO URL)
                                             var postUrlMF='';
                                             if (oneortwo_way=="roundtrip") {
                                                 $.ajax({
                                                                                url: apiUrl + 'uni-svc/Home/RedirectRoundWay?Nation='+ locale +'&AdultNum=1&ChildNum=0&InfantNum=0&CabinLevel=ECONOMY&OrgCity='+ book_Ori +'&DstCity=' + book_Des + '&DepartDate='+ book_OriDate +'&ReturnDate='+ book_RetDate +'&ExpireDate=000000&FZRA=00000003',
                                                                                type: 'GET',
                                                                                dataType: "json",
                                                                                 async:false,
                                                                                success: function (data) {

                                                                                    response=data;
                                                                                   
                                                                                   

                                                                                }
                                                                            });


                                                                 }
                                                             else {
                                                                     $.ajax({
                                                                                                    url: apiUrl + 'uni-svc/Home/RedirectOneWay?Nation=' + locale + '&AdultNum=1&ChildNum=0&InfantNum=0&CabinLevel=ECONOMY&OrgCity=' + book_Ori + '&DstCity=' + book_Des + '&DepartDate=' + book_OriDate + '&ExpireDate=000000&FZRA=00000003',
                                                                                                    type: 'GET',
                                                                                                    dataType: "json",
                                                                                                    async:false,
                                                                                                    success: function (data) {
                                                                            response=data;
                                                                                                    }
                                                                                                });

                                                                 }
                                             var EMBEDDED_TRANSACTION=response.url.split('&')[1].split('=')[1];
                                             var SITE=response.url.split('&')[2].split('=')[1];
                                             var ENCT=response.url.split('&')[3].split('=')[1];
                                             var LANGUAGE=response.url.split('&')[4].split('=')[1];
                                             var ENC=response.url.split('&')[5].split('=')[1];

                                             postUrlMF="EMBEDDED_TRANSACTION="+EMBEDDED_TRANSACTION+"&SITE="+SITE+"&ENCT="+ENCT+"&LANGUAGE="+LANGUAGE+"&ENC="+ENC;

window.MyJSClient.getStringFromJS("https://global-et.xiamenair.com/plnext/XiamenAirB2C/Override.action",EMBEDDED_TRANSACTION,SITE,ENCT,LANGUAGE,ENC,oneortwo_way,marketing_Code);

                             }
}else{
if(marketing_Code=="KE"){
sessionStorage.memberDetailsURL=bookingURL;
} else if(marketing_Code=="SV"){
bookingURL = bookingURL.replace(/(\r\n|\n|\r)/gm,"");
sessionStorage.memberDetailsURL=bookingURL;
}
else{
sessionStorage.memberDetailsURL=encodeURI(bookingURL);
}
//window.open(iSC.openSkyTeamWebSite, "_self");
window.MyJSClient.getStringFromJS(bookingURL,"","","","","","",marketing_Code);
}




},
             showReturnFlights :function(e){
sessionStorage.slider_return_stops="";
sessionStorage.slider_onward_stops="";
sessionStorage.slider_return_time0="";
sessionStorage.slider_return_time1="";
sessionStorage.slider_onward_time0="";
sessionStorage.slider_onward_time1="";
checkedAirlineStoring_onward = [];
checkedAirlineStoring_return = [];

                if(e)e.preventDefault();
                //sessionStorage.isSixDaysReturnPopupShown = "yes";
                self.model.set({'currentTab':'Duration','currentTravel':'Return'});
                   if(self.model.get('schedules').returnSchedules.totalNumberTripOptions == 0){//Any error codes(3204,3289,3010
                   self.model.set({"currentList":[]});
                   }
                   else{
                   if(sessionStorage.show7DaysArrival === "yes"){
                     self.model.set({"currentList":self.model.get('schedules').returnSchedules.trips});
                   }
                   else{
                    if(self.model.get('returnFlights').trips.length >0){
                     self.model.set({"currentList":self.model.get('returnFlights').trips});
                    }
                   else{
                    self.model.set({"currentList":self.model.get('schedules').returnSchedules.trips});
                   }
                   }
                  }
                //refereshing the page for new list
                 self.render();
                 if(self.$el.find('div.searchresultslist p').hasClass('class_service_failed')){
                                   self.$el.find('.searchresultstabs').css('visibility', 'hidden');
                                   self.$el.find('#oneWayFilter').css('visibility', 'hidden');

                                                   }else{
                                                        self.$el.find('.searchresultstabs').css('visibility', 'visible');
                                                        self.$el.find('#oneWayFilter').css('visibility', 'visible');

                                                   }

             },
             showOutboundFlights:function(e){

             sessionStorage.slider_return_stops="";
             sessionStorage.slider_onward_stops="";
             sessionStorage.slider_return_time0="";
             sessionStorage.slider_return_time1="";
             sessionStorage.slider_onward_time0="";
             sessionStorage.slider_onward_time1="";
             checkedAirlineStoring_onward = [];
             checkedAirlineStoring_return = [];


                 if(e)e.preventDefault();
                 self.model.set({'currentTab':'Duration','currentTravel':'Onward'});
                   if(self.model.get('schedules').onwardSchedules.totalNumberTripOptions == 0){//Any error codes(3203,3289,3010
                   self.model.set({"currentList":[]});
                   }
                   else{
                 if(sessionStorage.show7DaysDeparture === "yes"){
                    self.model.set({"currentList":self.model.get('schedules').onwardSchedules.trips});
                 }
                 else{
                    if(self.model.get('departureFlights').trips.length >0){
                      self.model.set({"currentList":self.model.get('departureFlights').trips});
                    }
                    else{
                     self.model.set({"currentList":self.model.get('schedules').onwardSchedules.trips});
                    }
                }
                }
                 //refereshing the page for new list
                 self.render();

                 if(self.$el.find('div.searchresultslist p').hasClass('class_service_failed')){
                  self.$el.find('.searchresultstabs').css('visibility', 'hidden');
                  self.$el.find('#oneWayFilter').css('visibility', 'hidden');

                                  }else{
                                       self.$el.find('.searchresultstabs').css('visibility', 'visible');
                                       self.$el.find('#oneWayFilter').css('visibility', 'visible');

                                  }


             },
             openUserSuggestNoFlight:function(flightList) {

             self.$el.find("#flightCount").html('0');
             openalertsokbuttonFlag=true;
             $('*').bind('touchmove', function(e) {
                         e.preventDefault();
                         });
             $('.popUpShade').show();
             $('.popUp').show();

             $('.popUpMessage1').html($.i18n.map.sky_notavailable);

             $('.popUpMessage1').css('text-align','center');
             $('.popUpShade').css({opacity : 0.5});
             var buttons='</button><button id="noButton"></button><button id="yesButton">';
             $('.popUp .popup_button').html(buttons);
             $('.popup_button #yesButton').text($.i18n.map.sky_yes);
             $('.popup_button #noButton').text($.i18n.map.sky_no);

             $('.popUp button').removeAttr('disabled');
             $('.popUp button').click(function(e) {

                                      e.preventDefault();
                                      e.stopPropagation();
                                      $('*').unbind('touchmove');
                                      $('.popUpShade').hide();
                                      $('.popUp').hide();
                                      $('.popUp button:eq(2)').remove();
                                      $('.popUp button:eq(1)').remove();
                                      if(this.id === "yesButton"){
                                      $('#noButton').attr('id','okButton');

                                      self.bindFlightListTemplate(flightList);

                                      }
                                      else if(this.id === "noButton"){
                                      $('#noButton').attr('id','okButton');
                                      sessionStorage.sixDays="no";
                                      sessionStorage.sixDaysOnward="false";
                                      sessionStorage.sixDaysReturn ="false";
                                      sessionStorage.navDirection = "Left";
                                      window.location.href = 'index.html#searchFlights';



                                      }
                                      });

             },
             openFlightDetails:function(e){
                    e.preventDefault();
                     sessionStorage.saved = "no";
                     sessionStorage.home = "no";
                     var curentObj = JSON.parse(sessionStorage.previousPages);
                     if(curentObj[0].previousPage != "searchResults"){
                     var pageObj = {};
                     pageObj.previousPage = "searchResults";
                        if(self.model.get('currentTravel') == "Return"){
                            pageObj.fromList = "Return"; /* back from flight details to show previous shown flight list(departure/arrival)*/
                        }
                        else{
                            pageObj.fromList = "Onward"; /* back from flight details to show previous shown flight list(departure/arrival)*/
                        }
                     pushPageOnToBackStack(pageObj);
                     }
                     var flightDetailsObj ={};
                     var segmentId = e.currentTarget.id;
                     var flightList = self.model.get('currentList');
                     for(var i=0,total=flightList.length;i<total;i++){
                         if(flightList[i].segmentIdentifier === segmentId){
                             flightDetailsObj.details = flightList[i];
                             flightDetailsObj.flightDetailsID =segmentId;
                                sessionStorage.flightDetailsObject =JSON.stringify(flightDetailsObj);
                                window.location.href = 'index.html#FlightDetailsPage';
                                showActivityIndicator($.i18n.map.sky_please);
                                break;
                         }
                     }


             }
       });
       return searchResultsView;
       });

