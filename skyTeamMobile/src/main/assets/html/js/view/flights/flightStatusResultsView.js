define(['jquery', 'underscore', 'backbone', 'text!template/flightStatusResults.html','appView'], function($, _, Backbone, flightstatusResTemplate,appView) {
	var res,that;
	var flightstatusresultsView = appView.extend({
		
		id : 'flightstatusResults-page',

		initialize : function() {
			 $('#backButtonList').show();
			$('#headingText img').css('display', 'none');
			$('#headingText h2').css('display', 'block');
			$('#headingText h2').html($.i18n.map.sky_flight_result);
                                                       
			
			
			if (sessionStorage.savedflights == "yes") {
				$('#brief img').css('display', 'block');
				$('#brief h2').css('display', 'none');

			}
             that = this;
		},

		render: function(){
           
			hideActivityIndicator();
			sessionStorage.currentPage = "flightstatusresults";
			sessionStorage.From = "";
			var locationPath = window.location;
			locationPath = locationPath.toString();
			var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);

			res = JSON.parse(localStorage.flightStatusResults);
                                         
			$.i18n.map.res=res;
			var compiledTemplate = _.template(flightstatusResTemplate,$.i18n.map);
                                                 
                                                 
           $(this.el).html(compiledTemplate);


			$('.main_header ul li:eq(1) img').on('click');
                                                 if(res.schedules != "Network Error"){
                                                 if(res.schedules.trips.length != undefined){
                                                 for (var k=0,maxLeng=res.schedules.trips.length;k<maxLeng;k++){
                                                 if(res.schedules.trips[k].flights != undefined){
                                                 if(res.schedules.trips[k].flights.length != undefined){
                                                 if(that.$el.find('#status_'+k+' ul:eq(1) li:eq(1) p').html() != " "){
                                                 that.$el.find('#status_'+k+' ul:eq(1) li:eq(1) p').css('display','block');
                                                 that.$el.find('#status_'+k+' ul:eq(1) li:eq(1) img').css('display','none');
                                                 }
                                                 else{
                                                 that.$el.find('#status_'+k+' ul:eq(1) li:eq(1) p').css('display','none');
                                                 that.$el.find('#status_'+k+' ul:eq(1) li:eq(1) img').css('display','block');
                                                 }
                                                 }}}}}
                                                 

           
           /*Back button functionality */
			$('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                var currentobj = JSON.parse(sessionStorage.previousPages);
				showActivityIndicator($.i18n.map.sky_please);
				e.preventDefault();
                e.stopImmediatePropagation();
				window.location.href = "index.html#" + currentobj[0].previousPage;
				popPageOnToBackStack();
				$(that.el).undelegate();
			});
			/* End of Back functionality */
           return appView.prototype.render.apply(this, arguments);
                                                
		},
		events : {
			'click .flightStatusResults' : 'openFlightstatusDetails',
                                                       'touchstart *' : function() {
                                                       $.pageslide.close();
                                                       }

		},
		/* To show the selected flight details */
		openFlightstatusDetails : function(e) {
			var id = $(e.currentTarget).attr('id').split('_')[1];
			var flightStatusObj = {};
			res = res.schedules.trips;
			if (res.length != undefined) {
				for (var i = 0,maxLeng= res.length; i < maxLeng; i++) {

					if (id == i) {

						flightStatusObj.flightDetailsID = $(e.currentTarget).attr('id');
						flightStatusObj.details = res[i];
						break;
					}
				}
			} else {
				flightStatusObj.flightDetailsID = $(e.currentTarget).attr('id');
				flightStatusObj.details = res;
			}
			sessionStorage.flightStatusObj = JSON.stringify(flightStatusObj);
                                                 
                                                 
			var pageObj = {};
			pageObj.previousPage = "flightstatusresults";

			pushPageOnToBackStack(pageObj);
			$('.main_header ul li:eq(1) img').off('click')
			window.location.href = "index.html#flightStatusDetails";
		},
		/*End of displaying flight details */

	});
	return flightstatusresultsView;
});
