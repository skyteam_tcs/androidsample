/* Airport Details Functionality */
var self;
define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'text!template/menuTemplate.html', 'text!template/airports.html'], 
		function($, _, Backbone, utilities, constants, menuPageTemplate, airport_details) {

	var list = {},
	netstat,
	check = 0;
	var weather_Image={
			"01d": "Weather_Sunny_b",
			"02d": "Weather_PartlyCloud_d_b",
			"03d":"Weather_PartlyCloud_d_b" ,
			"04d":"Weather_PartlyCloud_d_b",
			"09d":"Weather_Rainy_b",
			"10d":"Weather_Rainy_b",
			"11d":"thunder_storms_b",
			"13d":"Weather_Snow_b",
			"50d":"Weather_Fog_b",
			"01n": "Weather_Sunny_b",
			"02n": "Weather_PartlyCloud_d_b",
			"03n":"Weather_Cloudy_b" ,
			"04n":"Weather_Cloudy_b",
			"09n":"Weather_Rainy_b",
			"10n":"Weather_Rainy_b",
			"11n":"thunder_storms_b",
			"13n":"Weather_Snow_b",
			"50n":"Weather_Fog_b",
	}
	var airportDetailsPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'flight-page',
		initialize : function() {
			$('#backButtonList').show();
			window.networkStatCallBack = function(){};
			window.open(iSC.Network, "_self");
			$('#headingText img').hide();
			$('#headingText h2').show();
			$('#headingText h2').html($.i18n.map.sky_airportdetails);

			if ( "yes" === sessionStorage.savedflights ) {
				$('#brief img').show();
				$('#brief h2').hide();
				sessionStorage.savedflights = "no";
			}
			$('#brief img').removeClass('noclass');
			$('#brief img').show();
			var currentPage = JSON.parse(sessionStorage.previousPages);

			if ( "savedFlightsOrAirports" === currentPage[0].previousPage ) {
				sessionStorage.isFromSaved = "yes";
				$('#brief img').hide();
				$('#brief h2').hide();
			} else if ( "FlightDetailsPage" === currentPage[0].previousPage ) {
				if ( "savedFlightsOrAirports" === currentPage[1].previousPage) {
					sessionStorage.isFromSaved = "yes";
					$('#brief img').hide();
					$('#brief h2').hide();
				}
			}
			sessionStorage.currentPage ="airportDetails";
		},

		render : function() {
			self = this;
			if ( "searchResults" === sessionStorage.From ) {
				sessionStorage.From = "searchResults";

			} else {
				sessionStorage.From = "";
			}
			var compiledTemplate = _.template(airport_details,$.i18n.map);
			$('.main_header ul li:eq(1) img').on('click');
			$(this.el).html(compiledTemplate);
			/* Back button click event */
			$('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				 sessionStorage.navDirection = "Left";

				if( "block" === $('#pageslide').css('display') ){
					$.pageslide.close();
				}
				else{  var currentobj = JSON.parse(sessionStorage.previousPages);
				showActivityIndicator($.i18n.map.sky_please);
				if ( "FlightDetailsPage" === currentobj[0].previousPage  || "flightStatusDetails" === currentobj[0].previousPage ) {
					if( "homePage" === currentobj[0].isFrom ){
						sessionStorage.savedFlightDetails = currentobj[0].data;
					}
					sessionStorage.currentPage = currentobj[0].isFrom;
					sessionStorage.flightDetailsObject=currentobj[0].flightDetails;
					localStorage.searchResults=currentobj[0].flightResults;
					sessionStorage.removeItem('lastPage');
					e.preventDefault();
					window.location.href = "index.html#" + currentobj[0].previousPage;
					popPageOnToBackStack();
					$(self.el).undelegate();
				} else if ( "savedFlightsOrAirports" === currentobj[0].previousPage ) {
					localStorage.removeItem('savedFlightsAirports');
					if ( undefined == localStorage.savedFlightsAirports ||  null == localStorage.savedFlightsAirports) {
						popPageOnToBackStack();
						sessionStorage.removeItem('lastPage');
						e.preventDefault();
						e.stopImmediatePropagation();
						window.open(iSC.savedAirports, "_self");

					}
				}
				}
				hideActivityIndicator();
			});
			
			/* End of back functionality */
			//  sessionStorage.lastPage = "airportSearchPage";

			sessionStorage.airpor = "yes";
			var savedObj = JSON.parse(sessionStorage.airportDetailsObj);
			sessionStorage.isFromAirports = "yes";
			$('.airport_name h6').html(getAirportCountryName(savedObj.airport_code));
			$('.airport_name h1').html(getAirportFullName(savedObj.airport_code));
			sessionStorage.skyTipsAirportName = savedObj.airport_Name;
			$('.airports_temp ul li:eq(0) p').html(savedObj.airport_code);
			$('.main_header ul li:eq(1) img').on('click');
			if ( undefined != localStorage.savedFlightsAirports ) {
				var savedairports = JSON.parse(localStorage.savedFlightsAirports)
				var results = {};
				if ( "yes" == sessionStorage.savedAirportDetails ) {
					$.each(savedairports.SavedAirports_test, function(key) {
						if (savedairports.SavedAirports_test[key].Location_Code == savedObj.airport_code) {
							$(".save_buttons").hide()
						}
					});
					//hideActivityIndicator();
					sessionStorage.savedAirportDetails = "no";
				}

			}
			/* checking for the network status */
			if ( undefined != localStorage.networkStatus ) {
				hideActivityIndicator();
				netstat = JSON.parse(localStorage.networkStatus);

				if ( "offline" == netstat.network && savedairports) {
					var saved_airports = {};
					$.each(savedairports.SavedAirports_test, function(key) {
						if (savedairports.SavedAirports_test[key].Location_Code == savedObj.airport_code) {
							display_AirportDetails(savedairports.SavedAirports_test[key], weather_Image);

						}

					});
					
				} else {
					/* ajax call for retrieving airport details in online mode*/
					// showActivityIndicator();
					if(sessionStorage.AirportDetails_Response !=undefined){
						var valueOfAirportDetails =JSON.parse(JSON.stringify(sessionStorage.AirportDetails_Response));
						if(valueOfAirportDetails.AirportCode == sessionStorage.airport_code){
							display_AirportDetails(JSON.parse(valueOfAirportDetails.AirportDetails), weather_Image);
						}
						else{
					xhrajax=$.ajax({
						url : savedObj.airportDetails_URL,
						headers:{'api_key':API_KEY, 'source': 'SkyApp'},
						datatype : 'json',
						async : true,
						timeout : 20000,
						success : function(result) {
							
							results = JSON.parse(JSON.parse(JSON.stringify(result)));
							var objDetails = {"AirportCode":sessionStorage.airport_code,"AirportDetails":results};
							sessionStorage.AirportDetails_Response=JSON.stringify(objDetails);
							
							sessionStorage.LocalTime = results.LocalTime;
							sessionStorage.Temperature = results.Temperature;
							sessionStorage.WeatherCode = results.WeatherCode;
							sessionStorage.LoungesCount = results.LoungesCount;
							sessionStorage.SkyTipsCount = results.SkyTipsCount;
							sessionStorage.AirlinesCount = results.AirlinesCount;
							sessionStorage.airlines = JSON.stringify(results.AirlineDetails);
							sessionStorage.detailsTransportation= JSON.stringify(results.Details);
							display_AirportDetails(results, weather_Image);

						},
						error : function(xhr, status, error) {
							if( 'abort' != status ){
								openAlertWithOk($.i18n.map.sky_requestfailed,'airportDetails');
							}
							self.undelegateEvents();
							hideActivityIndicator();
						},
						failure : function() {
							hideActivityIndicator();
						}
					});
					/* end of ajax call */
				}
			} else {
				/* ajax call for retrieving airport details */
				xhrajax=$.ajax({
					url : savedObj.airportDetails_URL,
					headers:{'api_key':API_KEY, 'source': 'SkyApp'},
					datatype : 'json',
					async : true,
					timeout : 20000,
					success : function(result) {
						
						results = JSON.parse(JSON.parse(JSON.stringify(result)));
						;
						var objDetails = {"AirportCode":sessionStorage.airport_code,"AirportDetails":results};
						sessionStorage.AirportDetails_Response=JSON.stringify(objDetails);
						
						sessionStorage.LocalTime = results.LocalTime;
						sessionStorage.Temperature = results.Temperature;
						sessionStorage.WeatherCode = results.WeatherCode;
						sessionStorage.LoungesCount = results.LoungesCount;
						sessionStorage.SkyTipsCount = results.SkyTipsCount;
						sessionStorage.AirlinesCount = results.AirlinesCount;
						sessionStorage.airlines = JSON.stringify(results.AirlineDetails);
						sessionStorage.detailsTransportation= JSON.stringify(results.Details);
						display_AirportDetails(results, weather_Image);

					},
					error : function(xhr, status, error) {
						if(status != 'abort'){
							openAlertWithOk($.i18n.map.sky_requestfailed,'airportDetails');
						}
						self.undelegateEvents();
						hideActivityIndicator();
					},
					failure : function() {
						hideActivityIndicator();
					}
				});
				/* end of ajax call */
			}
		}
			}
			else{
				xhrajax=$.ajax({
					url : savedObj.airportDetails_URL,
					headers:{'api_key':API_KEY, 'source': 'SkyApp'},
					datatype : 'json',
					async : true,
					timeout : 20000,
					success : function(result) {
						
						results = JSON.parse(JSON.parse(JSON.stringify(result)));
						var objDetails = {"AirportCode":sessionStorage.airport_code,"AirportDetails":results};
						sessionStorage.AirportDetails_Response=JSON.stringify(objDetails);
						
						sessionStorage.LocalTime = results.LocalTime;
						sessionStorage.Temperature = results.Temperature;
						sessionStorage.WeatherCode = results.WeatherCode;
						sessionStorage.LoungesCount = results.LoungesCount;
						sessionStorage.SkyTipsCount = results.SkyTipsCount;
						sessionStorage.AirlinesCount = results.AirlinesCount;
						sessionStorage.airlines = JSON.stringify(results.AirlineDetails);
						sessionStorage.detailsTransportation= JSON.stringify(results.Details);
						display_AirportDetails(results, weather_Image);

					},
					error : function(xhr, status, error) {
						if( 'abort' != status ){
							openAlertWithOk($.i18n.map.sky_requestfailed,'airportDetails');
						}
						self.undelegateEvents();
						hideActivityIndicator();
					},
					failure : function() {
						hideActivityIndicator();
					}
				});
			}
			var fileurl="images/hub_airports/Hub_"+savedObj.airport_code+".jpg";
			$.ajax({
				url:fileurl,
				type:"GET",

				success: function()
				{
					$('.airports_image').css('background','url('+fileurl+') no-repeat');
				}, error: function(statusCode,error)
				{
					// $('.airports_image').css('background','url("images/new_images/1.jpg") no-repeat');
				}
			});slideThePage(sessionStorage.navDirection);
		},

		events : {
			'click #lounges' : 'openLoungeFinder',
			'click #skyTips' : 'openSkyTips',
			'click #fly_here' : 'openfromsearchFlights',
			'click #fly_to' : 'opentosearchFlights',
			'click #airport_save' : 'saveairport',
			'click #weatherReport' : 'displayWeatherInfo',
			'click #memberAirlines' : 'openMemberAirlines',
			'click #skyPriority' : 'openSkyPriority',
			'click #transportation' : 'openTransportation',
			'click #airportmaps' : 'openAirportMaps',
			'touchstart *' : function() {
				$.pageslide.close();
			}

		},
		openTransportation : function(e){

			var obj1 = {};
			obj1.previousPage = "airportDetails";
			pushPageOnToBackStack(obj1);
			window.location.href="index.html#transportationPage";
		},
		openAirportMaps : function(e) {

                                                                    
                        e.preventDefault();
                                                                      
                       e.stopImmediatePropagation();
                                                                     
                       var obj1 = {};
                                                                      
                       obj1.previousPage = "airportDetails";
                                                                      
                      pushPageOnToBackStack(obj1);
                                                                      
                      var airportMapNative = "app:nativeAirportMap-"+sessionStorage.airport_code;
                                                                     
                      //alert(airportMapNative);
                       window.open(airportMapNative,"_self");
                      },
		/* function to skyPriority page details */
		openSkyPriority : function(e) {

			//sessionStorage.previousPage="index.html#flightStatusDetails";
			sessionStorage.airportFlag=1;
			var obj1 = {};
			//obj1.airportObj=sessionStorage.airportDetailsObj;
			obj1.previousPage = "airportDetails";
			pushPageOnToBackStack(obj1);
			sessionStorage.airpor = "no";
			sessionStorage.lastPage = "";
 comingfromairportskypriority=1;
			window.location.href="index.html#skyPriorityFinder";
		},
		/* end of function to skyPriority page details*/
		/* To get into lounges page */
		openLoungeFinder : function(e) {

			var obj = {};
			obj.previousPage = "airportDetails";
			obj.airportName = sessionStorage.airportName;
			sessionStorage.airport_newName=obj.airportName;
			pushPageOnToBackStack(obj);
			 sessionStorage.airportCode = sessionStorage.airport_code;
			sessionStorage.lastPage = "";
			sessionStorage.lounge = "airportDetails";
			sessionStorage.loungeFlag = 0;
			if ( "no" == sessionStorage.savedAirportDetails) {
				sessionStorage.savedAirportDetails = "yes";
			} else {
				sessionStorage.savedAirportDetails = "";
			}
			sessionStorage.airpor = "no";
			var airportCode = $('.airports_temp ul li:eq(0) p').html();


			var obj = {};
			obj.airportCode = airportCode;
			obj.url = URLS.LOUNGE_SEARCH+'airportcode=' + obj.airportCode;
			sessionStorage.loungesData = JSON.stringify(obj);
			showActivityIndicatorForAjax('true');
			window.open(iSC.loungeFinder, "_self");

		},
		/** End of lounges **/

		/** navigate to skytips page **/
		openSkyTips : function(e) {

			sessionStorage.airportFlag=1;
			sessionStorage.theme = "no";
			var pageObj = {};
			pageObj.airportObj=sessionStorage.airportDetailsObj;
			pageObj.previousPage = "airportDetails";
			pushPageOnToBackStack(pageObj);
			sessionStorage.airpor = "no";
			sessionStorage.lastPage = "";
			if (sessionStorage.history != "skytips") {
				localStorage.skyTipsDetails = "";
			}
			if (sessionStorage.savedAirportDetails == "no") {
				sessionStorage.savedAirportDetails = "yes";
			} else {
				sessionStorage.savedAirportDetails = "";
			}
			var airportCode = $('.airports_temp ul li:eq(0) p').html();
			var obj = {};
			obj.airportCode = airportCode;
//			obj.url = URLS.SKYTIP_SEARCH+'?airportcode=' + obj.airportCode+"&version=3";

			obj.url = URLS.SKYTIP_SEARCH+'airportcode=' + obj.airportCode;
			sessionStorage.skytipsReqData = JSON.stringify(obj);
			showActivityIndicatorForAjax('true');
			if(hasValue(sessionStorage.themeSelected))
				sessionStorage.removeItem("themeSelected");
			window.open(iSC.skytips,"_self");
		},
		/* end of skytips */

		/* to open search flights page with source airport */
		openfromsearchFlights : function(e) {

			//  sessionStorage.removeItem("previousPages");
			sessionStorage.airportFlag=1;
			var pageObj = {};
			pageObj.airportObj=sessionStorage.airportDetailsObj;
			pageObj.previousPage = "airportDetails";
			pushPageOnToBackStack(pageObj);
			sessionStorage.airpor = "no";
			sessionStorage.lastPage = "";
			if (sessionStorage.savedAirportDetails == "no") {
				sessionStorage.savedAirportDetails = "yes";
			} else {
				sessionStorage.savedAirportDetails = "";
			}
			sessionStorage.flyfromhere = "yes";
			sessionStorage.flytohere = "no";
			if(sessionStorage.searchData != undefined && sessionStorage.searchData != null){
				sessionStorage.removeItem('searchData');

			}
			 comingfromflyhere=1;
			sessionStorage.removeItem('Res');
			window.location.href = 'index.html#searchFlights';

		},
		/* End of serach flights */

		/* To open search flights with destination */
		opentosearchFlights : function(e) {

			//   sessionStorage.removeItem("previousPages");
			sessionStorage.airportFlag=1;
			var pageObj = {};
			pageObj.airportObj=sessionStorage.airportDetailsObj;
			pageObj.previousPage = "airportDetails";
			pushPageOnToBackStack(pageObj);
			sessionStorage.airpor = "no";
			sessionStorage.lastPage = "";
			sessionStorage.flytohere = "yes";
			sessionStorage.flyfromhere = "no";
			if ( "no" === sessionStorage.savedAirportDetails ) {
				sessionStorage.savedAirportDetails = "yes";
			} else {
				sessionStorage.savedAirportDetails = "";
			}
			if( undefined != sessionStorage.searchData && null != sessionStorage.searchData ){
				sessionStorage.removeItem('searchData');

			}
			comingfromflyto=1;
			sessionStorage.removeItem('Res');
			window.location.href = 'index.html#searchFlights';

		},
		/* end of search flights */

		/* to save an airport */
		saveairport : function(e) {

			var save_airport = {};
			var obj = JSON.parse(sessionStorage.airportDetailsObj);
			save_airport.Airport_Name = obj.airportName;
			save_airport.Location_Code = obj.airportCode;
			save_airport.Country_Code = obj.countryCode;
			save_airport.City_Name = obj.cityName;
			save_airport.AirportDetails_URL = obj.airportDetails_URL;
			save_airport.Country_Name = obj.countryName;
			save_airport.LocalTime = sessionStorage.LocalTime;
			save_airport.Temperature = sessionStorage.Temperature;
			save_airport.WeatherCode = sessionStorage.WeatherCode;
			save_airport.LoungesCount = sessionStorage.LoungesCount;
			save_airport.SkyTipsCount = sessionStorage.SkyTipsCount;
			save_airport.AirlinesCount = sessionStorage.AirlinesCount;

			sessionStorage.saveAirportReqData = JSON.stringify(save_airport);
			window.open(iSC.insertSavedAirport, "_self")
		},
		/* End of airport saving */

		/* To display weather information */
		displayWeatherInfo : function(e) {

			var obj = {};
			obj.previousPage = "airportDetails";
			pushPageOnToBackStack(obj);

			//sessionStorage.weatherfrom = "AirportDetails";
			sessionStorage.lastPage = "";
			sessionStorage.airpor = "yes";
			if ( "no" === sessionStorage.savedAirportDetails ) {
				sessionStorage.savedAirportDetails = "yes";
			} else {
				sessionStorage.savedAirportDetails = "";
			}
		     window.networkStatCallBack = function(){
			netstat = JSON.parse(localStorage.networkStatus);

			if ( "offline" === netstat.network ) {
				openAlertWithOk($.i18n.map.sky_retrievefailed);
				popPageOnToBackStack();
			} else {
				showActivityIndicatorForAjax('false');
				setTimeout(function(){
					openWeatherInfoPage();
				},1);

//					$('.main_header ul li:eq(1) img').off('click');
			}
			};
			window.open(iSC.Network, "_self");
		},
		/* End of display weather info */

		/* Display skyteam member airlines */
		openMemberAirlines : function(e) {

			var obj = {};
			obj.previousPage = "airportDetails";
			pushPageOnToBackStack(obj);

			//sessionStorage.savedflights = "yes";
			sessionStorage.lastPage = "";
			if ( "no" === sessionStorage.savedAirportDetails ) {
				sessionStorage.savedAirportDetails = "yes";
			} else {
				sessionStorage.savedAirportDetails = "";
			}
			sessionStorage.members = "AirportDetails";
			if ( 0 != sessionStorage.AirlinesCount ) {
				netstat = JSON.parse(localStorage.networkStatus);

				if ( "offline" === netstat.network ) {
					openAlertWithOk($.i18n.map.sky_retrievefailed);
					popPageOnToBackStack();
				} else {
					$('.main_header ul li:eq(1) img').off('click');
					window.location.href = 'index.html#aboutSkyTeam';
				}
			} else {
				$('.main_header ul li:eq(1) img').on('click');
				openAlertWithOk($.i18n.map.sky_sorry);
				popPageOnToBackStack();
			}

		}
		/* end of member airlines */
	});
	return airportDetailsPageView;

});

/* to display selected airport details */
function display_AirportDetails(results, weather_Image) {

	if ( undefined == results.ErrorCode ) {
		if ( undefined != sessionStorage.airlines &&  "undefined" != sessionStorage.airlines) {
			var myString = sessionStorage.airlines;
			var myArray = listToAray(myString, ',');
			if ( (undefined !== results.AirlineDetails.Operating.Count) && (0 !== results.AirlineDetails.Operating.Count.length) &&  ("0" !== results.AirlineDetails.Operating.Count) ) {
				$("#airlines_count").html(' (' + results.AirlineDetails.Operating.Count + ')');
			}
			else if ( (undefined !== results.AirlineDetails.Marketing.Count) && (0 !== results.AirlineDetails.Marketing.Count.length) &&  ("0" !== results.AirlineDetails.Marketing.Count) ) {
				$("#airlines_count").html('');
			}
			else
			{
				$("#memberAirlines").addClass('noclass');
			}	

		}
		else
		{
			$("#memberAirlines").addClass('noclass');
		}
		if(  "0" != results.LoungesCount )
		{
			$("#lounges_count").html(' (' + results.LoungesCount + ')');
		}
		else
		{
			$("#lounges").hide();
		}
		if ( ("" != results.SkyTipsCount ) && ("0" != results.SkyTipsCount)) {
			$("#skyTips").css('display', 'block');
			$("#skytips_count").html(' (' + results.SkyTipsCount + ')');
		} else {
			$("#skyTips").addClass('noclass');
		}

		if("Y" === results.SkyPriorityFeatures ){
			$("#skyPriority").css('display', 'block');
			sessionStorage.skyPriorityAirlines=results.SkyPriorityAirlines;
		}
		else{
			$("#skyPriority").addClass('noclass');
		}
		if( undefined == results.Details ||  0 === results.Details.length || "[]" == sessionStorage.detailsTransportation ){
			$("#transportation").addClass('noclass');

		}
		else{
			$("#transportation").css('display', 'block');
		}
		var image_weather;
		var Airport_Imagepath = results.AirportImage;
		var weathercondition = results.WeatherType;
		var currenttemp;
		var time;
		var tempType;
		if ( "fahrenheit" === localStorage.temparature ) {
			currenttemp = Math.round(temparatureConvertor(results.Temperature));
			tempType = "F";
		} else {
			currenttemp = Math.round(results.Temperature);
			tempType = "C";
		}
		if( (undefined == results.LocalTime) || (null == results.LocalTime) || (0 === results.LocalTime.length)){
			$("#local_time").css('display','none');}
		else{


			if ( "12h" === localStorage.time ) {
				time = timeConvertor(results.LocalTime.split(" ")[0]);
			} else {
				time = results.LocalTime.split(" ")[0];
			}
			$("#local_time").html(time);
			$(".gmt").html(results.LocalTime.split(" ")[1]);}
		if ( results.WeatherCode !== undefined && results.WeatherCode !== "undefined" && 0 < results.WeatherCode.length ) {
			$("#weather_icon").html('<img src=./images/' + weather_Image[results.WeatherCode] + '.png></img>')
		}
		$("#currenttemp").html(currenttemp);
		$(".airports_temp ul li:eq(2) sup").html('o');
		$(".airports_temp ul li:eq(2) p:eq(1)").html(tempType);
		hideActivityIndicator();
		//adjusting the border of airport facility
		var hiddenFacilities=$('.airport_facility li:not(.noclass)').length;

		if( 0 === hiddenFacilities%2 ){
			$($('.airport_facility li:not(.noclass)')[hiddenFacilities-2]).css('border-bottom','0px');
			$('.airport_facility li:not(.noclass):last()').css('border-bottom','0px');
		}
		else{
			$('.airport_facility li:not(.noclass):last()').css('border-bottom','0px');
		}
	} else {
		hideActivityIndicator();
		self.undelegateEvents();
		openAlertWithOk(results.ErrorMessage);
	}
}

/* End of Airport Details Functionality */
