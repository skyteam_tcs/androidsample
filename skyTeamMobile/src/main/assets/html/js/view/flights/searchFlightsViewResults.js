define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/searchFlightList.html', 'text!template/searchResults.html'], 
		function($, _, Backbone, utilities, constants, jqueryUi, searchFlightList, searchResults) {

	var list = {};
	var res, showDays, url, returnListChanged, currentList, onwardListChanged, connectionType = "", onWardList,
	returnList, show7DaysDep, show7DaysReturn, filterRes,selectFlag, deselectFlag, isFromOnward, isFromReturn,
	returnRes, onwardRes, filtered, searchResultsPageObj, isBackFromFlightDetails;

	var flightPageView = Backbone.View.extend({
		el : '#skyteam',
		id : 'search-flight',
		tabViews : {},
		initialize : function() {

			$('#headingText img').hide();
			$('#headingText h2').show();
			$('#headingText h2').html($.i18n.map.sky_search);
			$('#headingText h2').css('margin-right','37px');


			if ("yes" === sessionStorage.savedflights) {
				$('#brief img').show();
				$('#brief h2').hide();
			}
			searchResultsPageObj =this;
		},

		render : function(filterRes1) {
			$('#backButtonList').show();
			var that = this;
			hideActivityIndicator();
			sessionStorage.From = "";
			var locationPath = window.location;
			locationPath = locationPath.toString();
			if("yes" === sessionStorage.filter || "yes" === sessionStorage.sixDays){
				filterRes = filterRes1;
			}
			else{
				filterRes = undefined;
			}
			var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
			if ("searchresults" === spath && "yes" === sessionStorage.navToSearch) {
				$(".menuItems").trigger('click');
				sessionStorage.navToSearch = "no";
			}
			if("FlightDetailsPage" === sessionStorage.currentPage ){
				isBackFromFlightDetails =true;
			}
			else{
				isBackFromFlightDetails = false;
			}
			sessionStorage.currentPage = "searchResults";
			var compiledTemplate = _.template(searchResults,$.i18n.map);

			/*Back button functionality */
			$('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();
		 sessionStorage.navDirection = "Left";

				if("block" === $('#pageslide').css('display')){

					$.pageslide.close();
				}
				else{
					if ("searchResults" === sessionStorage.currentPage) {
						sessionStorage.showDays = "no";
						show7DaysDep = "no";
						show7DaysReturn = "no";
						filterRes = undefined;
						e.preventDefault();
						e.stopImmediatePropagation();
						sessionStorage.removeItem('filter');
						sessionStorage.removeItem('Res');
						var currentobj = JSON.parse(sessionStorage.previousPages);
						showActivityIndicator($.i18n.map.sky_please);
						sessionStorage.searchData= currentobj[0].flightSearch;
						window.location.href = "index.html#" + currentobj[0].previousPage;

						popPageOnToBackStack();
					}

					if ("searchFlights" === sessionStorage.currentPage){
						sessionStorage.savedflights = "yes";
						$('.main_header ul li:eq(0) img').remove();
						$('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

						sessionStorage.removeItem("previousPage");

					}
					$(that.el).undelegate();
				}
			});
			/* End of Back functionality */


			$(this.el).html(compiledTemplate);
			$('.main_header ul li:eq(0) img').on('click');
			// first time loading results

			if (undefined == filterRes) {
				res = JSON.parse(localStorage.searchResults);
				if("no" === sessionStorage.showDay)
					sessionStorage.showDays="no";
				if ("oneway" === res.journey) {
					connectionType = "oneway";
					var onwardResArray;
					if(undefined != res.schedules.onwardSchedules.trips.length){
						onwardResArray = res.schedules.onwardSchedules.trips;
					}
					else{
						onwardResArray = $.makeArray(res.schedules.onwardSchedules.trips);
					}
					if("yes" === sessionStorage.sixDays){//new six days filter
						onwardRes = onwardResArray;
					}
					else{
						onwardRes = $.grep(onwardResArray, function(r, i) {
							if ( typeof r.RunOnDays == "object") {
								for (var i = 0, runLength=r.RunOnDays.length;i < runLength; i++) {
									if (r.RunOnDays[i] == sessionStorage.selectedDepDay){
										return true;
									}
								}
							} else {
								if (r.RunOnDays == sessionStorage.selectedDepDay){
									return true;
								}
							}
						});

					}
					$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + onwardRes.length + ")");
					$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.source));
					$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.destination));
					$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate+ ": " + getDateInLocaleFormat(sessionStorage.depDate,true));
					if (onwardRes.length > 0) {
						var filler = this.Paintpage(onwardRes);

						$('.searchresultslist').html(filler);

					} else {
						sessionStorage.showDays="no";
						if("yes" != sessionStorage.savedflights)
							openAlertWithOk($.i18n.map.sky_notavailable);

						this.openUserSuggestNoFlight($.i18n.map.sky_notavailable,this.Paintpage,resArray);


					}

				} else if ("roundtrip" === res.journey) {
					if (sessionStorage.showList != undefined && sessionStorage.showList != "")
						connectionType = sessionStorage.showList;
					else
						connectionType = "onward";

					if ("onward" === connectionType) {

						if("yes" === sessionStorage.showDays)
							onwardListChanged="yes";
						$('#returnFlightsBtn').css('display', 'block');
						if(undefined != res.schedules.returnSchedules.trips){
							if("true" === sessionStorage.sixDaysReturn){
								var returnResArray; 
								if(undefined != res.schedules.returnSchedules.trips.length){
									returnResArray = res.schedules.returnSchedules.trips;
								}
								else{
									returnResArray=$.makeArray(res.schedules.returnSchedules.trips);
								}
								returnRes = $.grep(returnResArray, function(r, i) {
									if ( typeof r.RunOnDays == "object") {
										for (var i = 0, runDaysLength=r.RunOnDays.length;i < runDaysLength; i++) {
											if (r.RunOnDays[i] == sessionStorage.selectedRetDay){
												return true;
											}
										}
									} else {
										if (r.RunOnDays == sessionStorage.selectedRetDay){
											return true;
										}
									}
								});

								$('#returnFlightsBtn').html($.i18n.map.sky_returnflight + " ("+ returnRes.length + ")");

							}
							else{//six days filter
								if(undefined != res.schedules.returnSchedules.trips.length){
									returnRes = res.schedules.returnSchedules.trips;
								}
								else{
									returnRes=$.makeArray(res.schedules.returnSchedules.trips);
								}
								$('#returnFlightsBtn').html($.i18n.map.sky_returnflight + " ("+ 0 + ")"); 
							}
						}
						else{
							returnRes=[];
						}


						//$('#returnFlightsBtn').html($.i18n.map.sky_returnflight + " ("+ returnRes.length + ")");
						var onwardResArray;
						if(undefined != res.schedules.onwardSchedules.trips){
							if("true" === sessionStorage.sixDaysOnward){
								var onwardResArray
								if(undefined != res.schedules.onwardSchedules.trips.length){
									onwardResArray = res.schedules.onwardSchedules.trips;
								}
								else{
									onwardResArray = $.makeArray(res.schedules.onwardSchedules.trips);
								}
								onwardRes = $.grep(onwardResArray, function(r, i) {
									if ( typeof r.RunOnDays == "object") {
										for (var i = 0, rundaysLength=r.RunOnDays.length;i < rundaysLength; i++) {
											if (r.RunOnDays[i] == sessionStorage.selectedDepDay){
												return true;
											}
										}
									} else {
										if (r.RunOnDays == sessionStorage.selectedDepDay){
											return true;
										}
									}
								});
								$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + onwardRes.length + ")");
							}
							else{//six days filter starts
								if(undefined != res.schedules.onwardSchedules.trips.length){
									onwardRes = res.schedules.onwardSchedules.trips;
								}
								else{
									onwardRes = $.makeArray(res.schedules.onwardSchedules.trips);
								}
								$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + 0 + ")");
							}

						}
						else{
							onwardRes = [];
						}
						if (onwardRes.length > 0) {
							$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + onwardRes.length + ")");
							$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.source));
							$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.destination));
							$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate + ": "+ getDateInLocaleFormat(sessionStorage.depDate,true));

							if("true" === sessionStorage.sixDaysOnward){
								var filler = this.Paintpage(onwardRes);
								$('.searchresultslist').html(filler);

							}
							else{
								if(isBackFromFlightDetails){
									var filler = this.Paintpage(onwardRes);
									$('.searchresultslist').html(filler);

								}
								else{
									this.openUserSuggestNoFlight(onwardRes);
								}
							}

						} else {
							sessionStorage.showDays="no";
							if("yes" !== sessionStorage.savedflights)
								openAlertWithOk( $.i18n.map.sky_nodateselect);
							window.location.href = "index.html#searchFlights";

						}

					} else if ("return" == connectionType) {

						if("yes" === sessionStorage.showDays)
							returnListChanged="yes";

						$('#outboundFlightsBtn').css('display','block');
						if(undefined != res.schedules.returnSchedules.trips){
							var returnResArray;
							if(undefined != res.schedules.returnSchedules.trips.length){
								returnResArray = res.schedules.returnSchedules.trips;
							}
							else{
								var returnResArray=$.makeArray(res.schedules.returnSchedules.trips);
							}
							if("true" === sessionStorage.sixDaysReturn){
								returnRes = $.grep(returnResArray, function(r, i) {
									if ( typeof r.RunOnDays == "object") {
										for (var i = 0,runDaysLength=r.RunOnDays.length; i < runDaysLength; i++) {
											if (r.RunOnDays[i] == sessionStorage.selectedRetDay){
												return true;
											}
										}
									} else {
										if (r.RunOnDays == sessionStorage.selectedRetDay){
											return true;
										}
									}
								});
							}
							else{
								returnRes = returnResArray;
							}


						}
						else{
							returnRes=[];
						}
						if(undefined != res.schedules.onwardSchedules.trips){
							var onwardResArray;
							if(undefined != res.schedules.onwardSchedules.trips.length){
								var onwardResArray = res.schedules.onwardSchedules.trips;
							}
							else{
								onwardResArray = $.makeArray(res.schedules.onwardSchedules.trips);
							}
							onwardRes = $.grep(onwardResArray, function(r, i) {
								if ( typeof r.RunOnDays == "object") {
									for (var i = 0,runLength=r.RunOnDays.length; i < runLength; i++) {
										if (r.RunOnDays[i] == sessionStorage.selectedDepDay){
											return true;
										}
									}
								} else {
									if (r.RunOnDays == sessionStorage.selectedDepDay){
										return true;
									}
								}
							});
						}
						else{
							onwardRes=[];
						}

						$('#outboundFlightsBtn').html($.i18n.map.sky_diff_departflight+ " (" + onwardRes.length + ")");

						if (returnRes.length > 0) {
							$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_returnflight +"(" + returnRes.length + ")");
							$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.destination));
							$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.source));
							$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate + ": " + getDateInLocaleFormat(sessionStorage.returndate,true));


							var filler = this.Paintpage(returnRes);

							$('.searchresultslist').html(filler);

						} else {
							sessionStorage.showDays="no";
							if(sessionStorage.savedflights != "yes")
								openAlertWithOk($.i18n.map.sky_nodateselect);
							window.location.href = "index.html#searchFlights";

						}

					}


				}
			}

			// after filter
			else {//currentList = res.journey;

				if ("oneway" === currentList) {

					$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.source));
					$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.destination));

					if (filterRes.length > 0) {

					} else {
						sessionStorage.showDays="no";
						if("yes" !== sessionStorage.savedflights)
							openAlertWithOk($.i18n.map.sky_selectedcriteria);
						var filterResArray;
						if(undefined != res.schedules.onwardSchedules.trips.length){
							filterResArray =res.schedules.onwardSchedules.trips;
						}
						else{
							filterResArray = $.makeArray( res.schedules.onwardSchedules.trips );
						}
						if("yes" === sessionStorage.sixDays){
							filterRes = $.grep(filterResArray, function(r, i) {
								if ( typeof r.RunOnDays == "object") {
									for (var i = 0,runLength=r.RunOnDays.length; i < runLength; i++) {
										if (r.RunOnDays[i] == sessionStorage.selectedDepDay){
											return true;
										}
									}
								} else {
									if (r.RunOnDays == sessionStorage.selectedDepDay){
										return true;
									}
								}
							});
						}
						else{//six days filter
							filterRes = filterResArray;
						}
						onwardRes = filterRes;
					}

					$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate +": "+ getDateInLocaleFormat(sessionStorage.depDate,true));
					$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + filterRes.length + ")");

				} else if ("onward" === currentList) {

					onwardListChanged = "yes";
					$('#returnFlightsBtn').css('display', 'block');

					$('#returnFlightsBtn').html($.i18n.map.sky_returnflight +" (" + ((returnRes.length != undefined) ? returnRes.length : "1") + ")");

					$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.source));
					$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.destination));
					if (1 > filterRes.length) {

						//} else {
						sessionStorage.showDays="no";
						if("yes" !== sessionStorage.savedflights)
							openAlertWithOk($.i18n.map.sky_selectedcriteria);
						var filterResArray; 
						if(undefined != res.schedules.onwardSchedules.trips.length){
							filterResArray = schedules.onwardSchedules.trips;
						}
						else{
							filterResArray = $.makeArray(res.schedules.onwardSchedules.trips);
						}
						if("true" === sessionStorage.sixDaysOnward){
							filterRes = $.grep(filterResArray, function(r, i) {
								if ( typeof r.RunOnDays == "object") {
									for (var i = 0,runDaysLength=r.RunOnDays.length; i <runDaysLength; i++) {
										if (r.RunOnDays[i] == sessionStorage.selectedDepDay){
											return true;
										}
									}
								} else {
									if (r.RunOnDays == sessionStorage.selectedDepDay){
										return true;
									}
								}
							});
						}
						else{
							filterRes = filterResArray;
						}
						onwardRes = filterRes;
					}
					$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate + ": " + getDateInLocaleFormat(sessionStorage.depDate,true));
					$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + filterRes.length + ")");

				} else if ("return" === currentList) {
					returnListChanged = "yes";
					$('#outboundFlightsBtn').css('display', 'block');
					$('#returnFlightsBtn').css('display', 'none');

					$('#outboundFlightsBtn').html($.i18n.map.sky_diff_departflight+ " (" + onwardRes.length + ")");

					$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.source));
					$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.destination));
					if (1 > filterRes.length) {

						//} else {
						sessionStorage.showDays="no";
						if("yes" !== sessionStorage.savedflights)
							openAlertWithOk($.i18n.map.sky_selectedcriteria);
						if(undefined != res.schedules.returnSchedules.trips.length){
							filterRes = $.grep(res.schedules.returnSchedules.trips, function(r, i) {
								if ( typeof r.RunOnDays == "object") {
									for (var i = 0,rundayLength=r.RunOnDays.length; i < rundayLength; i++) {
										if (r.RunOnDays[i] == sessionStorage.selectedRetDay){
											return true;
										}
									}
								} else {
									if (r.RunOnDays == sessionStorage.selectedRetDay){
										return true;
									}
								}
							});}
						else{
							var filterResArray = $.makeArray(res.schedules.returnSchedules.trips);
							filterRes = $.grep(filterResArray, function(r, i) {
								if ( typeof r.RunOnDays == "object") {
									for (var i = 0,runLength=r.RunOnDays.length; i <runLength; i++) {
										if (r.RunOnDays[i] == sessionStorage.selectedRetDay){
											return true;
										}
									}
								} else {
									if (r.RunOnDays == sessionStorage.selectedRetDay){
										return true;
									}
								}
							});
						}
						ret = filterRes;
					}
					$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_returndate + ": " + getDateInLocaleFormat(sessionStorage.returndate,true));
					$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_returnflight +"(" + filterRes.length + ")");
				}

				var filler = this.Paintpage(filterRes);

				$('.searchresultslist').html(filler);

			}
			if("roundtrip" === sessionStorage.tripType){
				$(".searchresultslist li.searchrersultssublist:last-child").css("padding-bottom","70px");
			}
			else if("oneway" === sessionStorage.tripType){
				$(".searchresultslist li.searchrersultssublist:last-child").css("padding-bottom","5px");
			}
			slideThePage(sessionStorage.navDirection);
			 setTimeout(function() {
                           $('.search_btn').attr('class','search_btn positionFixed');
                    }, 348);

		},
		events : {
			'click  .tab' : 'openTab',
			'click  .savedFlightsResults' : 'openFlightDetails',
			'click  #oneWayFilter' : 'openFilterOptions',
			'click  #returnFlightsBtn' : 'showReturnFlights',
			'click  #outboundFlightsBtn' : 'showOutboundFlights',

			'click  #dropdown' : 'openDropdown',
			'click  #operator-ok' : 'operatorsClosePopUp',
			'click  #select' : 'selectAllCheckboxes',

			'click  .groupCheckbox' : 'deselectcheck',
			'click  #moreOptions-ok ' : 'moreOptionsClose',
			'click .airlineName' : 'checkSelectedAirline',
			'click .scheduleChkBox' : 'checkScheduleCheckBox',
     		'click .interlineChkBox' : 'checkInterlineCheckBox',
			'touchstart *' : function() {
				$.pageslide.close();
			}

		},
		/*To close filter options */
		moreOptionsClose : function(e) {

			sessionStorage.showDays = "no";
			$('.moreOptionsDetails').css({
				'display' : 'none'
			});
			$('#moreOptions-ok').css({
				'display' : 'none'
			});
			$("#blackshade").css('display', 'none');
			var a = $('.moreOptionsDetails :checked').val();
			if (undefined == a) {
				if ("block" === $('#returnFlightsBtn').css('display')) {

					show7DaysDep = "no";
				} else if ("block" === $('#outboundFlightsBtn').css('display')) {
					show7DaysReturn = "no";
				}

			} else {

				sessionStorage.showDays = "yes";
				sessionStorage.showDay = "yes";
				if ("block" === $('#returnFlightsBtn').css('display')) {

					show7DaysDep = "yes";
				} else if ("block" === $('#outboundFlightsBtn').css('display')) {
					show7DaysReturn = "yes";
				}

			}
			var noOfStops = "";

			var maxTime = $('#time').text().split(':')[0];
			var minTime = $('#time1').text().split(':')[0];

			if (0.5 == $("#slider").slider("value")) {
				noOfStops = "0";
			} else if (0.9 == $("#slider").slider("value")) {
				noOfStops = "1";
			} else if (1.3 == $("#slider").slider("value")) {
				noOfStops = "2";
			} else if (1.7 == $("#slider").slider("value")) {
				noOfStops = " ";
			}
			res = JSON.parse(localStorage.searchResults);

			if (noOfStops != " ") {
				 
				 if (res.journey == "oneway") {
				 var NoOfStopsFromRes = res.schedules.onwardSchedules.totalNumberTripOptions;
				 filterRes = $.grep(res.schedules.onwardSchedules.trips, function(r, i) {
									if (r.flights.length != undefined) {
									
									return ((NoOfStopsFromRes <= noOfStops) && parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									
									} else {
									return ((NoOfStopsFromRes <= noOfStops) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									}
									});
				 currentList = "oneway";
				 
				 } else if (res.journey == "roundtrip") {
				 
				 if (isFromOnward == "yes") {
				 
				 isFromOnward = "no";
				 var onwardNoOfStopsFromRes = res.schedules.onwardSchedules.totalNumberTripOptions;
				 filterRes = $.grep(res.schedules.onwardSchedules.trips, function(r, i) {// just use arr
									
									if (r.flights.length != undefined) {
									
									return ((onwardNoOfStopsFromRes <= noOfStops) && parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									} else {
									return ((onwardNoOfStopsFromRes <= noOfStops) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									}
									});
				 currentList = "onward";
				 
				 } else if (isFromReturn == "yes") {
				 isFromReturn = "no";
				 var returndNoOfStopsFromRes = res.schedules.returnSchedules.totalNumberTripOptions;
				 filterRes = $.grep(res.schedules.returnSchedules.trips, function(r, i) {// just use arr
									
									if (r.flights.length != undefined) {
									
									return ((returndNoOfStopsFromRes <= noOfStops) && parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									} else {
									return ((returndNoOfStopsFromRes <= noOfStops) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									}
									
									});
				 currentList = "return";
				 
				 }
				 }
				 
				 } else {
				 
				 if (res.journey == "oneway") {
					var filterResArrays
				 if(res.schedules.onwardSchedules.trips != undefined){
					filterResArrays = res.schedules.onwardSchedules.trips;
				 }
				 else{
					filterResArrays = $.makeArray( res.schedules.onwardSchedules.trips );
				 
				 }
				 
				 filterRes = $.grep(filterResArrays, function(r, i) {// just use arr
									if (r.flights.length != undefined) {
									
									return (parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									} else {
									return (parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									}
									});
									
				 currentList = "oneway";
				 
				 } else if (res.journey == "roundtrip") {
				 
				 if (isFromOnward == "yes") {
				 isFromOnward = "no";
				 var filterResArrays;
				 if(res.schedules.onwardSchedules.trips.length != undefined){
					filterResArrays = res.schedules.onwardSchedules.trips
				 }
				 else{
					filterResArrays = $.makeArray(res.schedules.onwardSchedules.trips );
				 
				 
				 }
				 filterRes = $.grep(filterResArrays, function(r, i) {// just use arr
									if (r.flights.length != undefined) {
									
									return (parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									} else {
									return (parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									}
									});
				 currentList = "onward";
				 } else if (isFromReturn == "yes") {
				 isFromReturn = "no";
				  var filterResArrays;
				 if(res.schedules.returnSchedules.trips != undefined){
					   filterResArrays = res.schedules.returnSchedules.trips;
				 }
				 else{
				  filterResArrays = $.makeArray(res.schedules.returnSchedules.trips );
				 
				 }
				 filterRes = $.grep(filterResArrays, function(r, i) {// just use arr
									if (r.flights.length != undefined) {
									return (parseFloat(((r.flights[0].flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights[0]..flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									} else {
									return (parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) >= parseFloat(minTime) && parseFloat(((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[0]) + "." + ((r.flights.flightLegs[0].departureDetails.scheduledTime).split(':')[1])) <= parseFloat(maxTime));
									}
									});
				 currentList = "return";
				 }
				 }
				 }

			var filterResOne = [];
			if (undefined != sessionStorage.selectedAirlines) {
				var airlines = JSON.parse(sessionStorage.selectedAirlines);
				if (airlines.checkedAirline.length > 0) {
					for (var i = 0,airlineLength=airlines.checkedAirline.length; i < airlineLength; i++) {
						for (var j = 0; j < filterRes.length; j++) {

							if (undefined == filterRes[j].Segments.length) {
								if (((filterRes[j].Segments.FlightNumber).substring(0, 2)) == airlines.checkedAirline[i]) {

									filterResOne.push(filterRes[j]);
								}
							} else {

								for (var k = 0,filterLength=filterRes[j].Segments.length; k <filterLength; k++) {
									if (((filterRes[j].Segments[k].FlightNumber).substring(0, 2)) == airlines.checkedAirline[i]) {
										filterResOne.push(filterRes[j]);
										break;
									}
								}
							}
						}
					}
				}
				sessionStorage.selectedAirlines = undefined;
				sessionStorage.removeItem('selectedAirlines');
				filterRes = filterResOne;

			}
			if ("no" === sessionStorage.showDays) {
				if("yes" === sessionStorage.sixDays ){
					if ("onward" === currentList || "oneway" === currentList) {
						filterRes = $.grep(filterRes, function(r, i) {
							if ( typeof r.RunOnDays == "object") {
								for (var a = 0,runLength=r.RunOnDays.length; a < runLength; a++) {
									if (r.RunOnDays[a] == sessionStorage.selectedDepDay){
										return true;
									}
								}
							} else {
								if (r.RunOnDays == sessionStorage.selectedDepDay){
									return true;
								}
							}
						});
					} else if ("return" === currentList ) {
						filterRes = $.grep(filterRes, function(r, i) {
							if ( typeof r.RunOnDays == "object") {
								for (var a = 0,rundaysLength=r.RunOnDays.length; a < rundaysLength; a++) {
									if (r.RunOnDays[a] == sessionStorage.selectedRetDay){
										return true;
									}
								}
							} else {
								if (r.RunOnDays == sessionStorage.selectedRetDay){
									return true;
								}
							}
						});
					}
				}
			}

			$('.moreOptDiv').remove();
			sessionStorage.filter = "yes";
			sessionStorage.Res=JSON.stringify(filterRes);
			var newFragment = Backbone.history.getFragment($(this).attr('href'));
			if (Backbone.history.fragment == newFragment) {
				// need to null out Backbone.history.fragement because
				// navigate method will ignore when it is the same as newFragment
				sessionStorage.transRequired = "No";
				Backbone.history.fragment = null;
				Backbone.history.navigate(newFragment, true);
			}


			if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_diff_departflight) {
				onwardRes = filterRes;
			} else if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_returnflight) {
				returnRes = filterRes;
			}


		},
		/*End of filter close function*/
		/* To open preferred airlines in filter */
		openDropdown : function(e) {

			$('.moreOptionsDetails').css({
				'display' : 'none'
			});
			$('#moreOptions-ok').css({
				'display' : 'none'
			});
			$('.proferedline-popup').css({
				'display' : 'block'
			});
			$('#operator-ok').css({
				'display' : 'block'
			});
			$('#select').prop("checked", true);
			$('.groupCheckbox').prop("checked", true);


		},
		/*End of Open preferred airlines */
		/*Close of prefered airlines dropdown */
		operatorsClosePopUp : function(e) {

			var allVals = {};
			allVals.checkedAirline = [];
			$('.scrollforoperators :checked').each(function() {
				allVals.checkedAirline.push($(this).val());
			});
			sessionStorage.selectedAirlines = (JSON.stringify(allVals));

			$('.proferedline-popup').css({
				'display' : 'none'
			});
			$('#operator-ok').css({
				'display' : 'none'
			});
			$('.moreOptionsDetails').css({
				'display' : 'block'
			});
			$('#moreOptions-ok').css({
				'display' : 'block'
			});

		},
		/*End of prefered airlines close */
		/*For selecting all airlines in prefered airline popup */
		selectAllCheckboxes : function(e) {

			selectFlag = 1;
			if (true == $('#select').prop("checked")) {
				$('.groupCheckbox').prop("checked", true);
			} else {

				$('.groupCheckbox').prop("checked", false);
			}

		},
		/*End  prefered airline popup */
		//Code for deselecting all airlines
		deselectcheck : function() {
			$('#select').prop("checked", false);
		},
		//End of deselecting
		//To show return flights
		showReturnFlights : function(e) {

			sessionStorage.showList = "return";
			$(".searchresults_wrapper").scrollTop(0);
			$(".tab").addClass("offHover").removeClass("onHover");
			$("#durationTab").addClass("onHover").removeClass("offHover");
			this.durationTab();
			connectionType = "return";
			var tempOBList;
			var tempReturnList;
			sessionStorage.showDays = "no";
			$('#outboundFlightsBtn').css('display', 'block');
			$('#returnFlightsBtn').css('display', 'none');
			$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.destination));
			$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.source));
			if ("yes" === onwardListChanged && "yes" === returnListChanged && "yes" === show7DaysDep && "yes" === show7DaysReturn) {
				sessionStorage.showDays = "yes";

			} else if ("yes" === onwardListChanged) {
				if ("yes" === show7DaysDep && "yes" === show7DaysReturn) {
					sessionStorage.showDays = "yes";
				}

			} else if ("yes" === returnListChanged) {
				if ("yes" === show7DaysReturn) {
					sessionStorage.showDays = "yes";
				}

			}

			tempOBList = onwardRes;
			tempReturnList = returnRes;

			$('#outboundFlightsBtn').html($.i18n.map.sky_diff_departflight+ " (" + ((tempOBList.length != undefined) ? tempOBList.length : "1") + ")");

			$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_returnflight +"(" + ((tempReturnList.length != undefined) ? tempReturnList.length : "1") + ")");

			if (undefined != tempReturnList.length && 0 < tempReturnList.length) {
				if("true" === sessionStorage.sixDaysReturn){
					$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_returndate + ": " + getDateInLocaleFormat(sessionStorage.returndate,true));
					var filler = this.Paintpage(tempReturnList);
					$('.searchresultslist').html(filler);

				}
				else{
					if(-1 !== e.target.textContent.search('0')){
						this.openUserSuggestNoFlight(tempReturnList);

					}
					else{
						$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_returndate + ": " + getDateInLocaleFormat(sessionStorage.returndate,true));
						var filler = this.Paintpage(tempReturnList);
						$('.searchresultslist').html(filler);

						//this.showOutboundFlights();
					}
				}
			} else if (0 === tempReturnList.length == 0) {
				openAlertWithOk($.i18n.map.sky_noreturn);
				//this.showOutboundFlights();
			} 

		},
		/* End of Return flights */
		/* Show Outbound flights */
		showOutboundFlights : function(e) {

			sessionStorage.showList = "onward";
			$(".searchresults_wrapper").scrollTop(0);
			$(".tab").addClass("offHover").removeClass("onHover");
			$("#durationTab").addClass("onHover").removeClass("offHover");
			this.durationTab();
			connectionType = "onward";
			sessionStorage.showDays = "no";

			var tempOBList, tempReturnList;
			$('#returnFlightsBtn').css('display', 'block');
			$('#outboundFlightsBtn').css('display', 'none');
			$('.searchresults_text ul li:eq(1) h2').html(getAirportName(sessionStorage.source));
			$('.searchresults_text ul li:eq(3) h2').html(getAirportName(sessionStorage.destination));
			if ("no" === returnListChanged) {

				$('#returnFlightsBtn').html($.i18n.map.sky_returnflight +" (" + ((returnList.length != undefined) ? "returnList.length" : "1") + ")");
			}

			if ("yes" === onwardListChanged && "yes" === returnListChanged && "yes" === show7DaysReturn && "yes" === show7DaysDep) {
				sessionStorage.showDays = "yes";
			} else if ("yes" === onwardListChanged) {
				if ("yes" === show7DaysDep) {
					sessionStorage.showDays = "yes";
				}

			} else if ("yes" === returnListChanged) {
				if ("yes" === show7DaysReturn && "yes" === show7DaysDep) {
					sessionStorage.showDays = "yes";
				}
			}

			tempOBList = onwardRes;
			tempReturnList = returnRes;

			$('#returnFlightsBtn').html($.i18n.map.sky_returnflight +" ("+ ((tempReturnList.length != undefined) ? tempReturnList.length : "1") + ")");
			$('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + ((tempOBList.length != undefined) ? tempOBList.length : "1") + ")");

			if (tempOBList.length != undefined && tempOBList.length > 0) {
				$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate +": "  + getDateInLocaleFormat(sessionStorage.depDate));

				var filler = this.Paintpage(tempOBList);

				$('.searchresultslist').html(filler);

			} else if (tempOBList.length == 0) {
				openAlertWithOk($.i18n.map.sky_outbound);
				this.showReturnFlights();
			} else {
				$('.searchresults_text ul li:eq(4) h2').html($.i18n.map.sky_departdate + ": "  + getDateInLocaleFormat(sessionStorage.depDate));	 

				var filler = this.Paintpage(tempOBList);

				$('.searchresultslist').html(filler);

			}

		},
		/*End of outbound flights */
		/*Code for opening tabs */
		openTab : function(e) {
			showActivityIndicator($.i18n.map.sky_please);
			var el = $(e.currentTarget);
			$(".searchresultslist").children().detach();


			if (!this.tabViews[el.attr('id')]) {

				this.tabViews[el.attr('id')] = this.createTabViewForEl(el);
			}
			var array = ["durationTab", "stopsTab", "departureTab", "arrivalTab"]
			for (var i = 0,arrL = array.length; i < arrL; i++) {
				if (array[i] == el.attr('id')) {
					$("#" + el.attr('id')).addClass("onHover").removeClass("offHover");
				} else {
					$("#" + array[i]).addClass("offHover").removeClass("onHover");
				}

			}
		},
		createTabViewForEl : function(el) {
			var tab;
			setTimeout(function() {
				hideActivityIndicator();
			}, 500);
			switch(el.attr('id')) {
			case "durationTab":
				this.durationTab();

				break;
			case "stopsTab":
				this.stopsTab();

				break;

			case "departureTab":
				this.departureTab();

				break;

			case "arrivalTab":
				this.arrivalTab();

				break;

			}
			return tab;
		},
		/*End of tabs code */
		/*code for opening Filter options */
		openFilterOptions : function() {

			//sessionStorage.showDays = "no";

			if (($('#outboundFlightsBtn').css('display')) == 'none' && ($('#returnFlightsBtn').css('display')) == 'block') {

				if (isFromOnward == "no") {
					isFromOnward = "yes";
				} else {
					isFromOnward = "yes";

					returnList = returnRes;

				}

			} else if (($('#outboundFlightsBtn').css('display')) == 'block') {

				if (isFromReturn == "no") {
					isFromReturn = "yes";
				} else {
					isFromReturn = "yes";
					onWardList = onwardRes;

				}

			}

			$("#blackshade").show();
			$('#blackshade').css({
				opacity : 0.6,
				'width' : $(document).width(),
				'height' : $(document).height()
			});
			require(['view/flights/subViews/moreOptionsView'], function(optionsView) {
				var optionsViewObj = new optionsView();
				optionsViewObj.render();
			});

		},
		/*End of more options */
		/*To get the formatted results */
		getResponse : function(res) {
			var tempOBList, tempReturnList;

			tempOBList = onwardRes;
			tempReturnList = returnRes;
			if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_diff_departflight){
				return this.Paintpage(tempOBList);
			} else if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_returnflight) {
				return this.Paintpage(tempReturnList);
			}
		},
		/* End of getting formatted results */
		/*Code for tabs */
		durationTab : function() {

			res = JSON.parse(localStorage.searchResults);
			var filler = " ";

			if ("oneway" === res.journey) {
				filler = this.Paintpage(onwardRes);
			} else if ("roundtrip" === res.journey) {
				filler = this.getResponse(res);
			}

			$('.searchresultslist').html(filler);


		},

		departureTab : function() {

			 res = JSON.parse(localStorage.searchResults);
			 var sortRes;
			 if (res.journey == "oneway") {
			 sortRes = _.sortBy(onwardRes, function(kk) {
							if (kk.flights.length == undefined) {
							return [kk.flights.flightLegs[0].departureDetails.scheduledDate, kk.flights.flightLegs[0].departureDetails.scheduledTime].join("_");
							
							} else {
							return [kk.flights[0].flightLegs[0].departureDetails.scheduledDate, kk.flights[0].flightLegs[0].departureDetails.scheduledTime].join("_");
							
							}
							});
			 } else if (res.journey == "roundtrip") {
			 var tempOBList;
			 var tempReturnList;
			 tempOBList = onwardRes;
			 tempReturnList = returnRes;
			 
			 
			 if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_diff_departflight) {
			 if (tempOBList.length != undefined) {
			 sortRes = _.sortBy(tempOBList, function(kk) {
							if (kk.flights.length == undefined) {
							return [kk.flights.flightLegs[0].departureDetails.scheduledDate,kk.flights.flightLegs[0].departureDetails.scheduledTime]
							} else {
							return [kk.flights[0].flightLegs[0].departureDetails.scheduledDate,kk.flights[0].flightLegs[0].departureDetails.scheduledTime]
							}
							});
			 } else {
			 sortRes = tempOBList;
			 }
			 } else if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_returnflight) {
			 if (tempReturnList.length != undefined) {
			 sortRes = _.sortBy(tempReturnList, function(kk) {
							if (kk.flights.length == undefined) {
							return [kk.flights.flightLegs[0].departureDetails.scheduledDate,kk.flights.flightLegs[0].departureDetails.scheduledTime]
							} else {
							return [kk.flights[0].flightLegs[0].departureDetails.scheduledDate,kk.flights[0].flightLegs[0].departureDetails.scheduledTime]
							}
							});
			 } else
			 sortRes = tempReturnList;
			 
			 }
			 }
			 
			 var filler = this.Paintpage(sortRes);
			 $('.searchresultslist').html(filler);


			 },
		arrivalTab : function() {

				 res = JSON.parse(localStorage.searchResults);
				 var sortRes;
				 if (res.journey == "oneway") {
				 sortRes = _.sortBy(onwardRes, function(kk) {
								return [kk.flights[kk.flights.length - 1].flightLegs[kk.flights[kk.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledDate, kk.flights[kk.flights.length - 1].flightLegs[kk.flights[kk.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledTime].join("_");
								});
				 } else if (res.journey == "roundtrip") {
				 var tempOBList;
				 var tempReturnList;
				 
				 tempOBList = onwardRes;
				 tempReturnList = returnRes;
				 if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_diff_departflight){
				 if (tempOBList.length != undefined) {
				 sortRes = _.sortBy(tempOBList, function(kk) {
								return [kk.flights[kk.flights.length - 1].flightLegs[kk.flights[kk.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledDate, kk.flights[kk.flights.length - 1].flightLegs[kk.flights[kk.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledTime].join("_");
								});
				 } else
				 sortRes = tempOBList;
				 } else if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_returnflight) {
				 if (tempReturnList.length != undefined) {
				 sortRes = _.sortBy(tempReturnList, function(kk) {
								return [kk.flights[kk.flights.length - 1].flightLegs[kk.flights[kk.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledDate, kk.flights[kk.flights.length - 1].flightLegs[kk.flights[kk.flights.length - 1].flightLegs.length-1].arrivalDetails.scheduledTime].join("_");
								});
				 } else
				 sortRes = tempReturnList;
				 }
				 
				 }
				 
				 var filler = this.Paintpage(sortRes);
				 $('.searchresultslist').html(filler);

				 
				 },
		/*End of tabs*/
		// To open selected flight details
		openFlightDetails : function(e) {
			sessionStorage.saved = "no";
			sessionStorage.home = "no";
			var curentObj = JSON.parse(sessionStorage.previousPages);
			if("searchResults" !== curentObj[0].previousPage){
				var pageObj = {};
				pageObj.previousPage = "searchResults";
				if ('block' === $('#outboundFlightsBtn').css('display'))
					pageObj.fromList = "return";
				else
					pageObj.fromList = "onward";
				pushPageOnToBackStack(pageObj);
			}
			showActivityIndicator($.i18n.map.sky_please);
			var id = $(e.currentTarget).attr('id').split('_')[1];
			var connType = $(e.currentTarget).attr('id').split('_')[0];
			var flightDetailsObj = {};

			if (undefined == res.length) {
				if ("oneway" === connType) {

					for (var i = 0,onwardLength=onwardRes.length; i < onwardLength; i++) {

						if (id == i) {

							flightDetailsObj.flightDetailsID = $(e.currentTarget).attr('id');
							flightDetailsObj.details = onwardRes[i];
							break;
						}
					}
					sessionStorage.Res = JSON.stringify(onwardRes);
				} else if ("onward" === connType) {

					for (var i = 0,onwardLength=onwardRes.length; i < onwardLength; i++) {

						if (id == i) {

							flightDetailsObj.flightDetailsID = $(e.currentTarget).attr('id');
							flightDetailsObj.details = onwardRes[i];
							break;
						}
					}
					sessionStorage.Res =JSON.stringify(onwardRes);
				} else if ("return" === connType) {
					for (var i = 0,returnLength=returnRes.length; i < returnLength; i++) {

						if (id == i) {

							flightDetailsObj.flightDetailsID = $(e.currentTarget).attr('id');
							flightDetailsObj.details = returnRes[i];
							break;
						}
					}
					sessionStorage.Res =JSON.stringify(returnRes);
				}
			} else {
				for (var i = 0,resultLength=res.length; i < resultLength; i++) {

					if (id == i) {
						flightDetailsObj.flightDetailsID = $(e.currentTarget).attr('id');
						flightDetailsObj.details = res[i];
						break;
					}
				}
			}
			sessionStorage.flightDetailsObject = JSON.stringify(flightDetailsObj);
			window.location.href = 'index.html#FlightDetailsPage';
		},
		/*End of flight details */
		//To diaplay the search results
		Paintpage : function(res) {

			var flightNo = " ";
			// code for multiple flight results
			if (undefined != res.length && typeof res == 'object') {
				var y = "";
				var x;
                if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_diff_departflight) {
                      $('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_diff_departflight +"(" + res.length + ")");
                } else if (($('.searchresults_text ul li:eq(0) h1').html().split('(')[0]) == $.i18n.map.sky_returnflight) {
                      $('.searchresults_text ul li:eq(0) h1').html($.i18n.map.sky_returnflight +"(" + res.length + ")");
                }

				for (var i = 0,resultsLength=res.length; i < resultsLength; i++) {

					if ("yes" === sessionStorage.showDays) {
						var runondays = [], q = "", w;
						var days = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"];
						if ( typeof res[i].RunOnDays == 'object') {

							var dayAdded;

							for (var l = 0,daysLength=days.length; l < daysLength; l++) {
								dayAdded = "no";
								for (var r = 0,maxRunDays=res[i].RunOnDays.length; r < maxRunDays; r++) {

									runondays[r] = (res[i].RunOnDays[r]).split('-')[0];
									if (runondays[r] === days[l]) {
										w = '<label style="color:#83458B";>' + $.i18n.map["sky_"+days[l]] + '</label>';
										dayAdded = "yes";
										break;
									}

								}
								if ("no" === dayAdded) {
									w = '<label style="color:#000; display:none;">' + $.i18n.map["sky_"+days[l]] + '</label>';
								}
								q = q + w;

							}

						} else {

							for (var l = 0,maxDaysLen=days.length; l < maxDaysLen; l++) {
								runondays[0] = (res[i].RunOnDays).split('-')[0];
								if (runondays[0] == days[l]) {
									w = '<label style="color:#83458B";>' + $.i18n.map["sky_"+days[l]] + '</label>';

								} else {

									w = '<label style="color:#000; display: none;">' + $.i18n.map["sky_"+days[l]] + '</label>';

								}
								q = q + w;

							}
						}

					}
					// code for single segment/no stops

					if (undefined == res[i].flights.length) {
						var diff = daydiff(parseDate(res[i].Segments.DepartureDate), parseDate(res[i].Segments.ArrivalDate));
						var dep_date, dep_time, arr_date, arr_time;
						dep_date = getDateInLocaleFormat(res[i].Segments.DepartureDate,true);
						arr_date = getDateInLocaleFormat(res[i].Segments.ArrivalDate,true);

						if ("12h" === localStorage.time) {
							dep_time = timeConvertor((res[i].Segments.DepartureTime.split('+')[0]).split(':')[0] + ":" + (res[i].Segments.DepartureTime.split('+')[0]).split(':')[1]);
							arr_time = timeConvertor((res[i].Segments.ArrivalTime.split('+')[0]).split(':')[0] + ":" + (res[i].Segments.ArrivalTime.split('+')[0]).split(':')[1]);
						} else {
							dep_time = (res[i].Segments.DepartureTime.split('+')[0]).split(':')[0] + ":" + (res[i].Segments.DepartureTime.split('+')[0]).split(':')[1];
							arr_time = (res[i].Segments.ArrivalTime.split('+')[0]).split(':')[0] + ":" + (res[i].Segments.ArrivalTime.split('+')[0]).split(':')[1];
						}
						x = '<li class="searchrersultssublist"> <div class="savedFlightsResults" id="' + connectionType + '_' + i + '"> <ul> <li> <p class="header2">' + res[i].Segments.Origin + '</p> </li><li><img src="./images/stops-0-icon.png"  /></li><li><p class="header2">' + res[i].Segments.Destination + '</p></li></ul><ul><li><p class="header5" style="font-size:12px;">' + dep_date + '</p></li><li><p class="header5">' + dep_time + '</p></li><li><p class="header5">' + arr_time + '</p></li><li><p class="header5" style="font-size:12px;"> ' + arr_date + '</p></li></ul><ul><li><p class="header5">'+ $.i18n.map.sky_duration + ': '  + getTravelTime(((res[i].Segments.TravelTime).toLowerCase())) + '</p></li><li><p class="header3">' + getAirlineForFlightNum(res[i].Segments.FlightNumber) + '</p></li><li id="show7Days" class=' + ((sessionStorage.showDays == "yes") ? "showDays" : "hideDays") + '><p class="header3">'+$.i18n.map.sky_fileson+':' + q + '</p></li></ul></div></li>';

					}
					// codefor multiple stops/multi segments
					else {

							 var k = 0;
							 var j = res[i].flights.length - 1;
							 flightNo = res[i].flights[0].marketingFlightNumber;
							 for (var p = 1; p <= j; p++) {
							 flightNo = flightNo + "/" + res[i].flights[p].marketingFlightNumber;
							 }
							 var dep_date_unformated = res[i].flights[k].flightLegs[0].departureDetails.scheduledDate;
							 var arr_date_unformated = res[i].flights[j].flightLegs[res[i].flights[j].flightLegs.length-1].arrivalDetails.scheduledDate;
							 var dep_time_unformated = res[i].flights[k].flightLegs[0].departureDetails.scheduledTime;
							 var arr_time_unformated = res[i].flights[j].flightLegs[res[i].flights[j].flightLegs.length-1].arrivalDetails.scheduledTime;
							 
							 var diff = daydiff(parseDate(dep_date_unformated), parseDate(arr_date_unformated));
							 var dep_date;
							 var dep_time;
							 var arr_date;
							 var arr_time;
							 dep_date = getDateInLocaleFormat(dep_date_unformated,true);
							 arr_date = getDateInLocaleFormat(arr_date_unformated,true);
							 if (localStorage.time == "12h") {
							 dep_time = timeConvertor((dep_time_unformated.split('+')[0]).split(':')[0] + ":" + (dep_time_unformated.split('+')[0]).split(':')[1]);
							 arr_time = timeConvertor((arr_time_unformated.split('+')[0]).split(':')[0] + ":" + (arr_time_unformated.split('+')[0]).split(':')[1]);
							 } else {
							 dep_time = (dep_time_unformated.split('+')[0]).split(':')[0] + ":" + (dep_time_unformated.split('+')[0]).split(':')[1];
							 arr_time = (arr_time_unformated.split('+')[0]).split(':')[0] + ":" + (arr_time_unformated.split('+')[0]).split(':')[1];
							 }
							 x = '<li class="searchrersultssublist"> <div class="savedFlightsResults" id="' + connectionType + '_' + i + '"><ul> <li> <p class="header2">' + res[i].flights[k].origin.airportCode + '</p> </li><li><img src="./images/stops-' + res[i].totalNoTransfers + '-icon.png"  /></li><li><p class="header2">' + res[i].flights[j].destination.airportCode + '</p></li></ul><ul><li><p class="header5" style="font-size:12px;">' + dep_date + '</p></li><li><p class="header5">' + dep_time + '</p></li><li><p class="header5">' + arr_time + '</p></li><li><p class="header5" style="font-size:12px;"> ' + arr_date + '</p></li></ul><ul><li><p class="header5">'+ $.i18n.map.sky_duration + ": "  + getTravelTime(( (res[i].totalDuration).toLowerCase() )) + '</p></li><li><p class="header3">' +getAirlineNameForFlightNumber(flightNo)+ '</p></li><li id="show7Days" class=' + ((sessionStorage.showDays == "yes") ? "showDays" : "hideDays") + '><p class="header3">'+ $.i18n.map.sky_fileson + ':' + q + ' </p></li></ul></div></li>';
							 
							 }

					y = y + x;

				}

			} else {
				var y = "";
				var x;
				var i = 0;
				if (res.Segments.length == undefined) {

					var diff = daydiff(parseDate(res.Segments.DepartureDate), parseDate(res.Segments.ArrivalDate));
					var dep_date, dep_time, arr_date, arr_time;
					dep_date = getDateInLocaleFormat(res.Segments.DepartureDate,true);
					arr_date = getDateInLocaleFormat(res.Segments.ArrivalDate,true);

					if ("12h" === localStorage.time) {
						dep_time = timeConvertor((res.Segments.DepartureTime.split('+')[0]).split(':')[0] + ":" + (res.Segments.DepartureTime.split('+')[0]).split(':')[1]);
						arr_time = timeConvertor((res.Segments.ArrivalTime.split('+')[0]).split(':')[0] + ":" + (res.Segments.ArrivalTime.split('+')[0]).split(':')[1]);
					} else {
						dep_time = (res.Segments.DepartureTime.split('+')[0]).split(':')[0] + ":" + (res.Segments.DepartureTime.split('+')[0]).split(':')[1];
						arr_time = (res.Segments.ArrivalTime.split('+')[0]).split(':')[0] + ":" + (res.Segments.ArrivalTime.split('+')[0]).split(':')[1];
					}
					x = '<li class="searchrersultssublist"> <div class="savedFlightsResults" id="' + connectionType + '_' + i + '"> <ul> <li> <p class="header2">' + res.Segments.Origin + '</p> </li><li><img src="./images/stops-0-icon.png"  /></li><li><p class="header2">' + res.Segments.Destination + '</p></li></ul><ul><li><p class="header5" style="font-size:12px;">' + dep_date + '</p></li><li><p class="header5">' + dep_time + '                                                                                                                                                                                                                                                                                                                          </p></li><li><p class="header5">' + arr_time + '</p></li><li><p class="header5" style="font-size:12px;"> ' + arr_date + '</p></li></ul><ul><li><p class="header5">'+ $.i18n.map.sky_duration + ': '  + getTravelTime(((res.Segments.TravelTime).toLowerCase())) + '</p></li><li><p class="header3">' + getAirlineForFlightNum(res.Segments.FlightNumber) + '</p></li><li id="show7Days" class=' + ((sessionStorage.showDays == "yes") ? "showDays" : "hideDays") + '><p class="header3">'+$.i18n.map.sky_fileson+': <label>MON</label><label>TUE</label><label>WED</label><label>THU</label><label>FRI</label><label>SAT</label><label>SUN</label> </p></li></ul></div></li>';

				} else {
					var k = 0;
					var diff = daydiff(parseDate(res.Segments[k].DepartureDate), parseDate(res.Segments[k].ArrivalDate));
					var j = res.Segments.length - 1;
					flightNo = res.Segments[0].FlightNumber;
					for (var p = 1; p <= j; p++) {
						flightNo = flightNo + "/" + res.Segments[p].FlightNumber;
					}
					var dep_date, dep_time, arr_date, arr_time;
					dep_date = getDateInLocaleFormat(res.Segments[k].DepartureDate,true);
					arr_date = getDateInLocaleFormat(res.Segments[j].ArrivalDate,true);

					if ("12h" === localStorage.time) {
						dep_time = timeConvertor((res.Segments[k].DepartureTime.split('+')[0]).split(':')[0] + ":" + (res.Segments[k].DepartureTime.split('+')[0]).split(':')[1]);
						arr_time = timeConvertor((res.Segments[j].ArrivalTime.split('+')[0]).split(':')[0] + ":" + (res.Segments[j].ArrivalTime.split('+')[0]).split(':')[1]);
					} else {
						dep_time = (res.Segments[k].DepartureTime.split('+')[0]).split(':')[0] + ":" + (res.Segments[k].DepartureTime.split('+')[0]).split(':')[1];
						arr_time = (res.Segments[j].ArrivalTime.split('+')[0]).split(':')[0] + ":" + (res.Segments[j].ArrivalTime.split('+')[0]).split(':')[1];
					}
					x = '<li class="searchrersultssublist"> <div class="savedFlightsResults" id="' + connectionType + '_' + i + '"><ul> <li> <p class="header2">' + res.Segments[k].Origin + '</p> </li><li><img src="./images/stops-' + res.NoOfStops + '-icon.png"  /></li><li><p class="header2">' + res.Segments[j].Destination + '</p></li></ul><ul><li><p class="header5" style="font-size:12px;">' + dep_date + '</p></li><li><p class="header5">' + dep_time + '</p></li><li><p class="header5">' + arr_time + '</p></li><li><p class="header5" style="font-size:12px;"> ' + arr_date + '</p></li></ul><ul><li><p class="header5">'+ $.i18n.map.sky_duration + ': '  + getTravelTime(( (res.TotalDuration).toLowerCase() )) + '</p></li><li><p class="header3">' + getAirlineForFlightNum(flightNo) + '</p></li><li id="show7Days" class=' + ((sessionStorage.showDays == "yes") ? "showDays" : "hideDays") + '><p class="header3">'+$.i18n.map.sky_fileson+': <label >MON</label><label >TUE</label><label >WED</label><label>THU</label><label>FRI</label><label>SAT</label><label>SUN</label> </p></li></ul></div></li>';

				}

				y = y + x;
				var runondays = [];
				for (var l = 0; l < 7; l++) {
					var m = $('#show7Days p label:eq(' + l + ')').html();
					if ( typeof res.RunOnDays == 'object') {
					} else {
					}
					for (var r = 0,resultsDaysLength=res.RunOnDays.length; r < resultsDaysLength; r++) {
						runondays[r] = (res.RunOnDays[r]).split('-')[0];
						if (runondays[r] == m) {
							$('#show7Days p label:eq(' + l + ')').css('color', '#83458B');
						}
						else{
							$('#show7Days p label:eq(' + l + ')').hide();
						}
					}
				}

			}

			return y;

		}
		/*End of results */,
		openUserSuggestNoFlight:function(data) {
			openalertsokbuttonFlag=true;
			$('*').bind('touchmove', function(e) {
				e.preventDefault();
			});
			$('.popUpShade').show();
			$('.popUp').show();

			$('.popUpMessage1').html($.i18n.map.sky_notavailable);

			$('.popUpMessage1').css('text-align','center');
			$('.popUpShade').css({
				opacity : 0.5,

			});
			var buttons='</button><button id="noButton"></button><button id="yesButton">';
			$('.popUp .popup_button').html(buttons);
			$('.popup_button #yesButton').text($.i18n.map.sky_yes);
			$('.popup_button #noButton').text($.i18n.map.sky_no);

			$('.popUp button').removeAttr('disabled');
			$('.popUp button').click(function(e) {

				e.preventDefault();
				e.stopPropagation();
				$('*').unbind('touchmove');
				$('.popUpShade').hide();
				$('.popUp').hide();
				$('.popUp button:eq(2)').remove();
				$('.popUp button:eq(1)').remove();
				if("yesButton" === this.id){
					$('#noButton').attr('id','okButton');
					var filler = searchResultsPageObj.Paintpage(data);
					$('.searchresultslist').html(filler);


				}
				else if("noButton" === this.id){
					$('#noButton').attr('id','okButton');
					sessionStorage.sixDays="no";
					sessionStorage.sixDaysOnward="false";
					sessionStorage.sixDaysReturn ="false";
					sessionStorage.navDirection = "Left";
					window.location.href = 'index.html#searchFlights';



				}
			});

		},
		/* function to check the checkbox(theme) when corresponding label(theme name) is tapped */
		checkSelectedAirline: function(e) {
			var forid ='#' + $(e.target).attr('for');
			if ( $(forid).prop('checked') ) { 
				$(forid).prop("checked", false);
				$('#select').prop('checked', false);
			} else {
				$(forid).prop("checked", true);
			}
			if ( forid === '#select' ) {
					this.selectAllCheckboxes();
			}
			e.preventDefault();
		},

		checkScheduleCheckBox: function(e) {

			var forid ='#' + $(e.target).attr('for');
			if ( $(forid).prop('checked') ) { 
				$(forid).prop("checked", false);
			} else {
				$(forid).prop("checked", true);
			}
			
			e.preventDefault();
		},
		/******* interline */
		checkInterlineCheckBox: function(e) {

        			var forid ='#' + $(e.target).attr('for');
        			if ( $(forid).prop('checked') ) {
        				$(forid).prop("checked", false);
        			} else {
        				$(forid).prop("checked", true);
        			}
        			e.preventDefault();
        		},
     /******* interline */
	});
	return flightPageView;
});

