/*  Functionality for calendar display  */
define(['jquery',
        'underscore',
		'backbone',
		'text!template/datePopupTemplate.html'
		], function($,_,Backbone,dateTemplate) {
       var formatDate,texid="";
       var dateToday="";
       var that;
       var months =[$.i18n.map.sky_jan,$.i18n.map.sky_feb,$.i18n.map.sky_mar,$.i18n.map.sky_apr,$.i18n.map.sky_may,$.i18n.map.sky_jun,$.i18n.map.sky_jul,$.i18n.map.sky_aug,$.i18n.map.sky_sep,$.i18n.map.sky_oct,$.i18n.map.sky_nov,$.i18n.map.sky_dec];
	   var days= [$.i18n.map.sky_sunday,$.i18n.map.sky_monday,$.i18n.map.sky_tuesday,$.i18n.map.sky_wednesday,$.i18n.map.sky_thursday,$.i18n.map.sky_friday,$.i18n.map.sky_saturday];

       var datePageView = Backbone.View.extend({
               el : '#skyteam',
               initialize : function()
               {
               that = this;
               },
               render : function(id,depDate)
               {
               $.datepicker.setDefaults($.extend($.datepicker.regional[localStorage.language]));
               texid=id;
               var compileTemplate = _.template(dateTemplate,$.i18n.map);
               $(this.el).append(compileTemplate);
               $('.ui-state-active').css('border', 'none');
               if( texid == "arrival" )
                {
             $('.date-popup h2').html($.i18n.map.sky_select_return);
               }
               else{
               $('.date-popup h2').html($.i18n.map.sky_select_departure);
               }
			   $('#blackshade').css('display', 'block');

               $('.date-popup').css({
                'display' : 'block'
                });
               $('#ok').css({
                'display' : 'block'
                });
				var dateToday,selectedDate;

				//settting the date format date
				if(localStorage.date == "DD"){
					formatDate ='D dd M y';
				}
				else if(localStorage.date == "MMM"){
					formatDate ='D M dd y';
				}
				else if(localStorage.date ="YYYY"){
					formatDate ='D yy-mm-dd';
				}
				else{

				}
				if(id == "departure"){
				dateToday = getDateInDatePicker(new Date());
				selectedDate = $('#departure').html();
				}
				else{
				dateToday = $('#departure').html();
				selectedDate = $('#arrival').html();
				}

               $('#dates').datepicker({
                  firstDay : 1,
                  dateFormat : formatDate,
                  minDate : dateToday,
				  onSelect : function(dateText, inst) {

					event.preventDefault();
					event.stopPropagation();

					var day = dateText.split(' ')[0];
                    var m,d;
					m=inst.selectedMonth+1;
					d=inst.selectedDay;
					m=(m>9)?m:"0"+m;
					d=(d>9)?d:"0"+d;
					// extracting the date & day -start
					if(id == "departure"){
						//getting departure date;
						sessionStorage.depDate = inst.selectedYear+"-"+m+"-"+d;
						if(localStorage.language != 'En'){
							sessionStorage.selectedDepDay=getDaysInEnglish(day);
						}
						else{
							sessionStorage.selectedDepDay=dateText.split(' ')[0];
						}
					}
					else{
						//getting arrival date;
						sessionStorage.returndate = inst.selectedYear+"-"+m+"-"+d;
						if(localStorage.language != 'En'){
							sessionStorage.selectedRetDay=getDaysInEnglish(day);
						}
						else{
							sessionStorage.selectedRetDay=dateText.split(' ')[0];
						}
					}
					// extracting the day -end;

						$("#"+id).html(dateText);
						   if(id == "departure"){
							if($.datepicker.parseDate(formatDate, $('#arrival').html()).getTime() < $.datepicker.parseDate(formatDate, $('#departure').html()).getTime()){
							$("#arrival").html(dateText);
							sessionStorage.returndate = sessionStorage.depDate;
							sessionStorage.selectedRetDay=sessionStorage.selectedDepDay;
							}
						}

					}
              });
             $('#dates').datepicker("setDate",selectedDate);
           },
		   events : {
               'click #ok' : 'closePopUp'
		   },
		   closePopUp : function(e) {
			 if($('#roundtrip').hasClass('onHover') && texid == "departure"){
				if($.datepicker.parseDate(formatDate, $('#arrival').html()).getTime() < $.datepicker.parseDate(formatDate, $('#departure').html()).getTime()){
				$("#arrival").html($('#departure').html());
				sessionStorage.returndate = sessionStorage.depDate;
				sessionStorage.selectedRetDay=sessionStorage.selectedDepDay;
				}
				}
			 $('.date-popup').empty();
			 $("#blackshade").css('display', 'none');
			 $('.tripDetails').css('overflow-y', 'auto');
			 $('.date-popup').css({
								  'display' : 'none'
								  });
			 $('#ok').css({
						  'display' : 'none'
						  });

		   }
            });
        return datePageView;
     });
