/*  Functionality for filter options  */
define(['jquery',
		'backbone',
		'underscore',
        'touchpunch',
		'text!template/skyPriorityAirlineTemplate.html'
		], function($, Backbone, _,touchPunch,skyPriorityAirlineTemplate) {
       
       var skyPriorityAirlinesView = Backbone.View.extend({
         el : '#skyteam',
         initialize : function() {
         that=this;
         },
         events:{
            'click  #sp_airline-ok' : 'closeOperators',
         },
     render : function() {
        var airlinesTemplate = _.template(skyPriorityAirlineTemplate,this.model.toJSON());
        that.$el.find('.proferedline-popup').remove();
        $(that.el).append(airlinesTemplate);
          //adjusting airline height & align the popup vertically center
          var airlinesListHeight=(this.model.get('PriorityAirlines').length+2)*39;
          var popupWidgetHeight = that.$el.height()*.95;
          if(airlinesListHeight < popupWidgetHeight){
          //adjusting the height of the popup
          that.$el.find('#members-popup').height('auto');
          
          //vertically aligning the popup to middle
          that.$el.find('#members-popup').css('top',(popupWidgetHeight-airlinesListHeight)/2);
          }
          else{
          that.$el.find('#members-popup').height('95%');
          that.$el.find('#members-popup').css('top','10px');
          }
          
                                                          
$('#blackshade').show();
        },
        closeOperators:function(e){
          e.preventDefault();
          e.stopImmediatePropagation();

          that.model.set({'SelectedAirLineCode':that.$el.find('.scrollforflightsname :checked').val()});
          that.$el.find('.proferedline-popup').remove();
          //that.remove();
          
          $("#blackshade").hide();
                                                          
        }
     });
       return skyPriorityAirlinesView;
       });
