define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html', 'text!template/SkyPriorityDetailsTemplate.html'],
    function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate, skyPriorityDetailsTemplate) {

        var memberSelected, featureResponse, skyPriorityDetailsView, keyval = "";
        skyPriorityDetailsView = Backbone.View.extend({

            el: '#skyteam',
            id: 'skyPriorityDetails-page',

            initialize: function() {
                $('#backButtonList').show();
                $('#headingText img').css('display', 'none');
                $('#headingText h2').css('display', 'block');
                $('#headingText h2').text($.i18n.map.sky_priority_details);
                if (("yes" === sessionStorage.isFromSaved) || ("yes" === sessionStorage.savedflights)) {
                    $('#brief img').css('display', 'block');
                    $('#brief h2').css('display', 'none');
                    sessionStorage.savedflights = "no";
                    sessionStorage.isFromSaved = "no";
                }
                if ('menuItems' !== $('.main_header ul li:eq(0) a').attr('class')) {
                    $('.main_header ul li:eq(0) img').remove();
                    $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

                }

                $(".menuItems").pageslide();
            },

            render: function() {
            	var locationPath, spath, compiledTemplate, radioButtonGroup;
                locationPath = window.location.toString();
                if ("" != localStorage.SPDeatils) {
                    featureResponse = JSON.parse(localStorage.SPDeatils);
                }
                spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                var compiledTemplate = _.template(skyPriorityDetailsTemplate, $.i18n.map);
                $(this.el).html(compiledTemplate);
                $(".skypriority_details ul li:nth-child(3)").css("margin-top", "4.5em");
                $('#sp_airportname h2').html(getAirportFullName(sessionStorage.airport_code));
                $('#sp_airline').html($.i18n.map['sky_' + sessionStorage.skyPriorityCarrier]);
                memberSelected = sessionStorage.skyPriorityCarrier;
                setTimeout(function(){$('#popupWrapper').html(sessionStorage.SPAirlines);},352);
                
                radioButtonGroup = $('input[type=radio]');

                for (var i = 0,radioLength=radioButtonGroup.length; i < radioLength; i++) {
                    if (sessionStorage.skyPriorityCarrier === $(radioButtonGroup[i]).val()) {
                        $(radioButtonGroup[i]).trigger('click');
                        break;
                    }
                }
                //**end** code to check the selected airline in the popup



                paintSPDetails(featureResponse);
                hideActivityIndicator();

                /*Back button functionality */
                $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {

                    e.preventDefault();
                    e.stopImmediatePropagation();
                   sessionStorage.navDirection = "Left";

                    if("block" === $('#pageslide').css('display')){
               		 $.pageslide.close();
                    }
                    else{
                    sessionStorage.isFromSPDetails = true;
                    window.location.href = 'index.html#skyPriorityFinder';
                    $('.main_header ul li:eq(1) img').off('click');
                    }
                });
                /* End of Back functionality */


                sessionStorage.currentPage = "skyPriorityDetails";
slideThePage(sessionStorage.navDirection);
            },
            events: {

                'click .info': 'infoDetails',
                'click .Sp_select_airline': 'openMembersPopup',
                'click .members-ok': 'closeMembersPopup',
                'click .disclaimerShown': 'showDisclaimerMsg',
                'click .airlineName' : 'checkSelectedAirline',
                'touchstart *': function() {
                    $.pageslide.close();
                }


            },
            closeMembersPopup: function(e) {
                $('#members-popup').css('display', 'none');
                $('.members-ok').css('display', 'none');
                $("#blackshade").css('display', 'none');

                if ( (undefined !== $('.scrollforflightsname :checked').val()) && (memberSelected != $('.scrollforflightsname :checked').val()) ) {
                    showActivityIndicatorForAjax('false');


                    memberSelected = $('.scrollforflightsname :checked').val();

                    sessionStorage.skyPriorityCarrier = memberSelected;
                    $('#sp_airline').html($.i18n.map['sky_' + memberSelected]);
                    $('.Sp_select_airline').css('border', 'none');
                    $('.Sp_results').html('');

                    // hidding disclaimer text if its shown
                    if (!$('.disclaimer_text').hasClass('noclass'))
                        $('.disclaimer_text').addClass('noclass');
                    
                    //*** start of  the ajax SPFinder basic call
                    setTimeout(function() {
                        var isResponseValid;
                        localStorage.SPDeatils = '';
                        isResponseValid = false;
                        var acceptlanguage= (localStorage.language == "zh")?'zh-Hans':localStorage.language;
                        var spFinderBasicURL = URLS.SKYPRIORITY_BASIC+'triporigin=' + sessionStorage.airport_code + '&operatingcarrier=' + sessionStorage.skyPriorityCarrier;
                        xhrajax = $.ajax({
                            url: spFinderBasicURL,
                            type: 'GET',
                            datatype: JSON,
                            timeout: 30000,
                            async: true,
                            beforeSend: function (request)
							{
                            	request.setRequestHeader("Accept-Language", acceptlanguage);
								request.setRequestHeader("api_key", API_KEY);
								request.setRequestHeader("source", "SkyApp");
							},
                            success: function(response) {
                                hideActivityIndicator();

                                if ("Y" === response.SkyPriorityEligibility) {

                                    paintSPDetails((response.TripOriginFeatures));
                                } else {
                                    openAlertWithOk($.i18n.map.sky_priority_basic_no_benefits);

                                }
                            },
                            error: function(xhr, exception) {
                                hideActivityIndicator();
                                if ("timeout" === exception) {
                                    openAlertWithOk($.i18n.map.sky_unload);
                                } else if ("error" === exception) {
                                    openAlertWithOk($.i18n.map.sky_notconnected);
                                } else {

                                }
                            },
                            failure: function() {
                                hideActivityIndicator();
                            }

                        });
                    }, 1000);
                    //*** end of  the ajax SPFinder basic call



                }
            },
            openMembersPopup: function(e) {

                $('#members-popup').css('display', 'block');
                $('.members-ok').css('display', 'block');
                $("#blackshade").css('display', 'block');

            },
            infoDetails: function(e) {
                e.preventDefault();
                e.stopPropagation();
                /*
                 * Don't call  e.stopImmediatePropagation();
                 * 'infoDetails' & 'showDisclaimerMsg' function are called from same click
                 * so the event will call showDisclaimerMsg function to show/hide disclaimer msg
                 */
                var curDiv;
                if ((e.target.textContent === $.i18n.map.sky_moreinfo)) {
                    $(e.target.parentElement.parentElement).find('.sp_dep_content').addClass('noclass');
                    $(e.target.parentElement.parentElement).find('.sp_container_open').removeClass('noclass');
                    e.target.textContent = $.i18n.map.sky_lessinfo;
                } else if ((e.target.textContent === $.i18n.map.sky_lessinfo)) {
                    e.target.textContent = $.i18n.map.sky_moreinfo;
                    $(e.target.parentElement.parentElement).find('.sp_container_open').addClass('noclass');
                    $(e.target.parentElement.parentElement).find('.sp_dep_content').removeClass('noclass');
                } else {}
            },
            showDisclaimerMsg: function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('.disclaimer_text').toggleClass('noclass');
            },
            
            /* function to check the radiobutton(airline) when corresponding label is tapped */
            checkSelectedAirline: function(e) {
    			var forid ='#' + $(e.target).attr('for');
    			$(forid).prop("checked", true);
    			e.preventDefault();
    		}


        });
        return skyPriorityDetailsView;

    });

function paintSPDetails(featureResponse) {
    var topResHTML, noResHTML, arrSmallInfo, arrBigInfo;
    var depSmallInfo, depBigInfo, isDepatureAvail, disclaimermsg;
    
    $('.scrollforflightsname').html(localStorage.airlinesHTML);
    isDepatureAvail = false;


    topResHTML = $('#spResultTop').html();
    noResHTML = $('#sp_nofeature').html();
    depSmallInfo = '';
    depBigInfo = '';
    if ("Y" === featureResponse.PriorityCheckIn) {
        //spImages_large
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityCheckIn"] + '"/></div>';
        depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityCheckIn"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_checkin_title + '</h1><p>' + $.i18n.map.sky_priority_checkin_msg + '</p></div></div>';
        isDepatureAvail = true;
    }
    if ("Y" === featureResponse.PriorityBaggageDropOff) {
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityBaggageDropOff"] + '"/></div>';
        depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityBaggageDropOff"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_baggage_title + '</h1><p>' + $.i18n.map.sky_priority_baggage_msg + '</p></div></div>';
        isDepatureAvail = true;
    }
    if ("Y" === featureResponse.PriorityImmigration) {
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityImmigration"] + '"/></div>';
        if ("0" === featureResponse.ImmigrationFasttrackAccessibility) {
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityImmigration"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_immigration_title + '</h1><img src="images/spfinder/cornerStar.png" class="disclaimer_img"/><p>' + $.i18n.map.sky_priority_immigration_msg + '</p></div></div>';
        } else {
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityImmigration"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_immigration_title + '</h1><p>' + $.i18n.map.sky_priority_immigration_msg + '</p></div></div>';
        }
        isDepatureAvail = true;
    }
    if ("Y" === featureResponse.PrioritySecurityLines) {
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PrioritySecurityLines"] + '"/></div>';
        if ("0" === featureResponse.SecurityFasttrackAccessibility) {
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PrioritySecurityLines"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_security_title + '</h1><img src="images/spfinder/cornerStar.png" class="disclaimer_img"/><p>' + $.i18n.map.sky_priority_security_msg + '</p></div></div>';
        } else {
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PrioritySecurityLines"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_security_title + '</h1><p>' + $.i18n.map.sky_priority_security_msg + '</p></div></div>';
        }
        isDepatureAvail = true;
    }
    if ("Y" === featureResponse.PriorityTicketDesk) {
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityTicketDesk"] + '"/></div>';
        depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityTicketDesk"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_ticket_title + '</h1><p>' + $.i18n.map.sky_priority_ticket_msg + '</p></div></div>';
        isDepatureAvail = true;
    }

    if ("Y" === featureResponse.PriorityTransferDesk) {
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityTransferDesk"] + '"/></div>';
        depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityTransferDesk"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_transfer_title + '</h1><p>' + $.i18n.map.sky_priority_transfer_msg + '</p></div></div>';
        isDepatureAvail = true;
    }
    if ("Y" === featureResponse.PriorityBoarding) {
        depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityBoarding"] + '"/></div>';
        depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityBoarding"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_boarding_title + '</h1><p>' + $.i18n.map.sky_priority_boarding_msg + '</p></div></div>';
        isDepatureAvail = true;
    }

    $('.Sp_results').html('');
    if (isDepatureAvail) {
        // adding the class "disclaimerShown" with "info" class to show and hide "disclaimer" if disclaimer feature available
        if ( ("0" === featureResponse.ImmigrationFasttrackAccessibility) || ("0" === featureResponse.SecurityFasttrackAccessibility) ) {
            $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('="info"', '="info disclaimerShown"').replace('{#toheading}', $.i18n.map.sky_departure) + '<div class="sp_dep_content">' + depSmallInfo + '</div><div class="sp_container_open noclass">' + depBigInfo + '</div></div>');
        } else {
            $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('{#toheading}', $.i18n.map.sky_departure) + '<div class="sp_dep_content">' + depSmallInfo + '</div><div class="sp_container_open noclass">' + depBigInfo + '</div></div>');
        }
    } else { //for non skyteam airline or unavailable of feature
        $('.Sp_results').append('<div class="Sp_results_container">' + noResHTML.replace('{#toheading}', $.i18n.map.sky_departure) + '</div>');
    }
    //arrival Info
    arrSmallInfo = '';
    arrBigInfo = '';
    if ("Y" === featureResponse.BaggageFirst) {
        arrSmallInfo = '<div class="sp_dep_content"><div><img src="' + spImages_small["BaggageFirst"] + '"/></div></div>';
        arrBigInfo = '<div class="sp_container_open noclass"><div class="box"><div class="leftcontent"><img src="' + spImages_large["BaggageFirst"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_baggage_handle_title + '</h1><p>' + $.i18n.map.sky_priority_baggage_handle_msg + '</p></div></div></div>';

        $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('{#toheading}', $.i18n.map.sky_arrival) + arrSmallInfo + arrBigInfo + '</div>');
    } else { //for non skyteam airline or unavailable of feature
        $('.Sp_results').append('<div class="Sp_results_container">' + noResHTML.replace('{#toheading}', $.i18n.map.sky_arrival) + '</div>');
    }
}