define(['jquery','underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html','text!template/skyPriorityAdvancedDetailsTemplateMVVM.html','models/skypriority/skyPriorityAdvanceModel','appView'], function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate,skyPriorityAdvancedDetailsTemplate,skyPriorityAdvanceModel,appView){

       var disclaimerTextShownFlag;
       var skyPriorityDetailsView = appView.extend({



                                                         initialize : function() {
                                                         $('#backButtonList').show();
                                                         $('#headingText h2').css('margin-right','');
                                                         $('#headingText img').hide();
                                                         $('#headingText h2').show();
                                                         $('#headingText h2').text($.i18n.map.sky_priority_details);
                                                         if (sessionStorage.isFromSaved == "yes" || sessionStorage.savedflights == "yes") {
                                                         $('#brief img').show();
                                                         $('#brief h2').hide();
                                                         sessionStorage.savedflights = "no";

                                                         sessionStorage.isFromSaved = "no";
                                                         }
                                                         if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
                                                         $('.main_header ul li:eq(0) img').remove();
                                                         $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

                                                         }

                                                         $(".menuItems").pageslide();
                                                         },

                                                         render : function() {

                                                         var response=JSON.parse(sessionStorage.SPAdvance_response);
                                                         var dataModel = new skyPriorityAdvanceModel(response);
                                                         dataModel.setDestinationCarrier();
                                                         dataModel.set($.i18n.map);
                                                         this.model = dataModel;
                                                         var departureData = this.model.get("TripOriginFeatures");
                                                         if(departureData.isAvail && (departureData.ImmigrationFasttrackAccessibility === "0" || departureData.SecurityFasttrackAccessibility === "0")){
                                                         disclaimerTextShownFlag = 1;
                                                         }
                                                         else{
                                                         disclaimerTextShownFlag = 0;
                                                         }


                                                         var compiledTemplate = _.template(skyPriorityAdvancedDetailsTemplate,this.model.toJSON());
                                                         this.$el.html(compiledTemplate);
                                                         /*Back button functionality */
                                                         $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {

                                                                                                           e.preventDefault();
                                                                                                           e.stopImmediatePropagation();
                                                                                                           var currentPage = JSON.parse(sessionStorage.previousPages);
                                                                                                           if (currentPage[0].flightData !== undefined && currentPage[0].previousPage === "FlightDetailsPage")
                                                                                                           {
                                                                                                           sessionStorage.flightDetailsObject = currentPage[0].flightData;
                                                                                                           }
                                                                                                           window.location.href = 'index.html#'+JSON.parse(sessionStorage.previousPages)[0].previousPage;
                                                                                                           popPageOnToBackStack();
                                                                                                           $('.main_header ul li:eq(1) img').off('click');
                                                                                                           });
                                                         /* End of Back functionality */
                                                         sessionStorage.currentPage="skyPriorityAdvancedDetails";
                                                         //paintAdvanceDetails();
                                                          _.bindAll(this,"infoDetails");

                                                         return appView.prototype.render.apply(this, arguments);
                                                         },
                                                         events : {

                                                         'click .info':'infoDetails',
                                                         'touchstart *' : function() {
                                                             $.pageslide.close();
                                                             }

                                                         },
                                                         infoDetails:function(e) {
                                                    	 e.preventDefault();
                                                         e.stopImmediatePropagation();
                                                         if((e.target.textContent === $.i18n.map.sky_moreinfo)){
                                                         $(e.target.parentElement.parentElement).find('.sp_dep_content').addClass('noclass');
                                                         $(e.target.parentElement.parentElement).find('.sp_container_open').removeClass('noclass');
                                                         e.target.textContent = $.i18n.map.sky_lessinfo;
                                                         if($(e.target).hasClass('disclaimerShown')){
                                                        	 disclaimerTextShownFlag++;
                                                        	 if(this.$el.find('.disclaimer_text').hasClass('noclass')){
                                                                 this.$el.find('.disclaimer_text').removeClass('noclass');
                                                              }
                                                         }


                                                         }
                                                         else if((e.target.textContent === $.i18n.map.sky_lessinfo)){
                                                         e.target.textContent = $.i18n.map.sky_moreinfo;
                                                         $(e.target.parentElement.parentElement).find('.sp_container_open').addClass('noclass');
                                                         $(e.target.parentElement.parentElement).find('.sp_dep_content').removeClass('noclass');
                                                         if($(e.target).hasClass('disclaimerShown')){
                                                             //if(disclaimerTextShownFlag>0)
                                                        	 disclaimerTextShownFlag--;
                                                        	 if((!$('.disclaimer_text').hasClass('noclass')) && (disclaimerTextShownFlag<2)){
                                                        		 this.$el.find('.disclaimer_text').addClass('noclass');
                                                        	 }
                                                         }
                                                         }
                                                         else{
                                                         }
                                                         }

                                                         });
       return skyPriorityDetailsView;

       });
