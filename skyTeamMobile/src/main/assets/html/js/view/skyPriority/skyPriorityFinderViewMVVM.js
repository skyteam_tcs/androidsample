define(['jquery','underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html','text!template/skyPriorityFinderTemplateMVVM.html','models/skypriority/skyPriorityFinderModel','appView'], function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate,skyPriorityFinderTemplate,skyPriorityFinderModel,appView){


       var keyval = "";
       var currentView;
       sessionStorage.skyPriorityCarrier='';
             var skyPriorityFinderView = appView.extend({

                                                        id : 'skyPriorityFinder-page',

                                                        initialize : function() {
                                                        $('#backButtonList').hide();
                                                        $('#headingText img').hide();
                                                        $('#headingText h2').show();
                                                        $('#headingText h2').text($.i18n.map.sky_priority_finder);
                                                        $('#brief h2').hide();
                                                         hideActivityIndicator();
                                                        if (sessionStorage.isFromSaved == "yes") {
                                                            $('#brief img').hide();
                                                        }
                                                        else{
                                                            $('#brief img').show();
                                                        }
                                                        if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
                                                        $('.main_header ul li:eq(0) img').remove();
                                                        $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));
                                                        $(".menuItems").pageslide();
                                                        }
                                                        if($('#pageslide').children().length === 0)
                                                        {
                                                        $(".menuItems").pageslide();
                                                        var template = _.template(menuPageTemplate,$.i18n.map);
                                                        $('#pageslide').html(template);
                                                        sessionStorage.isMenuAdded = "YES";
                                                        }

                                                        currentView = this;
                                                        //$(".menuItems").pageslide();
                                                         sessionStorage.currentPage="skyPriorityFinder";
                                                        },

                                                        render : function() {
                                                            /* Back button click event */
                                                            $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {


                                                                                                              var currentobj = JSON.parse(sessionStorage.previousPages);
                                                                                                              showActivityIndicator($.i18n.map.sky_please);

                                                                                                              if (currentobj[0].previousPage == "airportDetails" || currentobj[0].previousPage == "airportDetailsViewViaMap") {
                                                                                                              if(currentobj[0].isFrom == "homePage"){
                                                                                                              sessionStorage.savedFlightDetails = currentobj[0].data;
                                                                                                              }
                                                                                                              sessionStorage.currentPage = currentobj[0].isFrom;

                                                                                                              }

                                                                                                              sessionStorage.removeItem('lastPage');
                                                                                                              e.preventDefault();
                                                                                                                         window.location.href = "index.html#" + currentobj[0].previousPage;
                                                                                                              popPageOnToBackStack();
                                                                                                              $(self.el).undelegate();
                                                                                                              });
                                                            /* End of back functionality */
                                                        var dataModel = new skyPriorityFinderModel();

                                                        if(hasValue(sessionStorage.airportName) && hasValue(sessionStorage.airport_code)){
                                                        dataModel.set({"AirportCode":sessionStorage.airport_code,"AirportName":sessionStorage.airportName});
                                                        }
                                                        if(hasValue(sessionStorage.skyPriorityCarrier)){
                                                        dataModel.set({"SelectedAirLineCode":sessionStorage.skyPriorityCarrier});
                                                        }
                                                        if(hasValue(sessionStorage.skyPriorityAirlines)){
                                                        dataModel.set({"PriorityAirlines":sessionStorage.skyPriorityAirlines.split(',')});
                                                        }

                                                        dataModel.set($.i18n.map);
                                                        this.model = dataModel;


                                                        var locationPath = window.location;
                                                        locationPath = locationPath.toString();
                                                        var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);


                                                        var compiledTemplate = _.template(skyPriorityFinderTemplate,this.model.toJSON());
                                                        $(this.el).html(compiledTemplate);
                                                        $.pageslide.close();

                                                        if(sessionStorage.previousPages !== undefined && sessionStorage.previousPages !== "[]"){
                                                        var previousPagesJSON=JSON.parse(sessionStorage.previousPages);
                                                        if(previousPagesJSON[0].previousPage === "airportDetails" || previousPagesJSON[0].previousPage ==="airportDetailsViewViaMap"){
                                                        	 $('#backButtonList').show();
                                                        var countryCodeOld;
                                                        countryCodeOld =getCountryName(sessionStorage.airport_code);

                                                        this.$el.find('#airport_text').val(sessionStorage.airportName+" ("+sessionStorage.airport_code+"), "+countryCodeOld.split('_')[0]);
                                                        this.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                        this.$el.find('.search_skytips').css('visibility', 'hidden');
                                                        var list = this.model.get("PriorityAirlines");

                                                        if(list.length === 1){
                                                        this.$el.find('#airlinesList p').html($.i18n.map['sky_'+list[0]]);
                                                        this.model.set({"SelectedAirLineCode":list[0]});
                                                        this.model.set({"SelectedAirLineName":$.i18n.map['sky_'+list[0]]});
                                                        }
                                                        else{//no need to show the previousky selecteda irline if more than one airline
                                                        this.model.set({"SelectedAirLineCode":""});
                                                        this.openMembersPopup();
                                                        }






                                                        }
                                                        else if(previousPagesJSON[0].previousPage === "skyPriorityFinder"){
                                                        popPageOnToBackStack();
                                                        if(previousPagesJSON.length >1){//If visited airport details
														$('#backButtonList').show();
                                                        }
														var countryCodeOld;
														if(sessionStorage.airport_code != undefined && sessionStorage.airport_code.length === 3){
                                                        countryCodeOld =getCountryName(sessionStorage.airport_code);

                                                        this.$el.find('#airport_text').val(sessionStorage.airportName+" ("+sessionStorage.airport_code+"), "+countryCodeOld.split('_')[0]);
                                                      }
                                                        this.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                        this.$el.find('.search_skytips').css('visibility', 'hidden');
                                                        var list = this.model.get("PriorityAirlines");

                                                        if(list.length === 1){
                                                            this.$el.find('#airlinesList p').html($.i18n.map['sky_'+list[0]]);
                                                        }
                                                        else{//no need to show the previousky selecteda irline if more than one airline
                                                        this.model.set({"SelectedAirLineCode":""});
                                                        }
                                                       }
                                                        }
                                                        if(sessionStorage.previousPage == "savedFlightsOrAirports"){
                                                            if(isValid(sessionStorage.airport_code) && (sessionStorage.airport_code.length === 3)){
                                                            var countryCodeOld;
                                                            countryCodeOld =getCountryName(sessionStorage.airport_code);

                                                            this.$el.find('#airport_text').val(sessionStorage.airportName+" ("+sessionStorage.airport_code+"), "+countryCodeOld.split('_')[0]);
                                                            this.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                            this.$el.find('.search_skytips').css('visibility', 'hidden');
                                                            }
                                                        sessionStorage.removeItem('previousPage');
                                                        }
                                                        _.bindAll(this,"showAirportNames");
                                                        _.bindAll(this,"openMembersPopup");
                                                        _.bindAll(this,"searchPriority");
                                                        _.bindAll(this,"dataChange");
                                                        this.listenTo( this.model,'change', this.dataChange);

                                                         //********************************************************************************
                                                                                                                if(comingfromairportskypriority==0)
                                                                                                                                                                        {
                                                                                                                if(latLonAirportName.length){
 autocomplete_autofill_inlanguagechange();
                                                                                                                this.$el.find('#airport_text').val(conte);

                                                                                                                this.model.set({"AirportCode":latLonAirportCode});
                                                                                                                this.model.set({"AirportName":latLonAirportName});
                                                                                                                $(this).blur();
                                                                                                                $(this).css("border-color", "");
                                                                                                                var selectedText=conte;
                                                                                                                var resultObj = validateAirportTextField(conte,latLonAirportCode );
                                                                                                                if(resultObj.isValid){

                                                                                                                if(sessionStorage.airport_code !== resultObj.Code){
                                                                                                                sessionStorage.skyPriorityAirlines ='';
                                                                                                                currentView.model.set({"PriorityAirlines":"","SelectedAirLineCode":""});

                                                                                                                currentView.$el.find('.scrollforflightsname').html('');
                                                                                                                currentView.$el.find('#airlinesList p').html($.i18n.map.sky_operating_airline);
                                                                                                                if(sessionStorage.previousPages !== undefined && sessionStorage.previousPages !== "[]" && (JSON.parse(sessionStorage.previousPages).length > 1) ){
                                                                                                                sessionStorage.removeItem('previousPages');
                                                                                                                $('#backButtonList').hide();
                                                                                                                }


                                                                                                                }
                                                                                                                sessionStorage.airport_code = latLonAirportCode;
                                                                                                                sessionStorage.airportName = latLonAirportName;
                                                                                                                this.$el.find('.search_skytips').css('visibility','hidden');
                                                                                                                   this.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                                                                                   this.$el.find('#airport_text').removeClass("has-error");

                                                                                                                   }else{
                                                                                                                   this.$el.find('.search_skytips').css('visibility','visible');
                                                                                                                   this.$el.find('.cancel_Skytips').css('visibility', 'hidden');
                                                                                                                   	this.$el.find('#airport_text').removeClass("has-error");

                                                                                                                   }
                                                                                                                if (sessionStorage.skytipsReqData != null) {
                                                                                                                sessionStorage.skytipsReqData = null;
                                                                                                                }
                                                                                                                }
                                                                                                                }
                                                                                                                comingfromairportskypriority=0;
                                                                                                                //  ***********************************************************************************

                                                        return appView.prototype.render.apply(this, arguments);
                                                        },
                                                        events : {
                                                        /*'focusin :input' : 'changeHeight',*/
                                                        'keydown :input' : 'showAirportNames',
                                                        'focusout input' : 'retainHeight',
                                                        'click .cancel_Skytips' : 'crossImage_Skypriority',
                                                        'click .search_skytips' : 'createFocus_Skypriority',
                                                        'click #airlinesList' : 'openMembersPopup',
                                                        'click .members-ok' : 'closeMembersPopup',
                                                        'click #searchPriority':'searchPriority',
                                                        'click .airlineName' : 'checkSelectedAirline',
                                                        'touchstart *' : function() {
                                                        $.pageslide.close();
                                                        }


                                                        },
                                                        dataChange:function(model,options){
                                                        if(model.hasChanged('AirportCode')){
														var inputedValue = model.get("AirportCode");
														   if(hasValue(inputedValue) && inputedValue.length === 3){
														   this.$el.find('#airport_text').removeClass("has-error");
														   }
														   else{
														   this.$el.find('#airport_text').addClass("has-error");
														   }
														}


														if(model.hasChanged('AirportName')){

														   var inputedValue = model.get("AirportName");
															if(hasValue(inputedValue)){
																this.$el.find('.search_skytips').css('visibility','hidden');
																this.$el.find('.cancel_Skytips').css('visibility', 'visible');
																var validateCheck = validateAirportTextField(inputedValue,model.get("AirportCode"));
																if(validateCheck.isValid){
																this.$el.find('#airport_text').removeClass("has-error");
																}
																else{
																this.$el.find('#airport_text').addClass("has-error");
																}

															}
															else{
																this.$el.find('.search_skytips').css('visibility','visible');
																this.$el.find('.cancel_Skytips').css('visibility', 'hidden');
																this.$el.find('#airport_text').addClass("has-error");
															}
														 }

                                                        if(model.hasChanged('SelectedAirLineCode')){
                                                        var selectedCode = this.model.get("SelectedAirLineCode");
                                                        var airlineName = $.i18n.map['sky_'+selectedCode];
                                                        this.model.set({"SelectedAirLineName":airlineName});
                                                        sessionStorage.skyPriorityCarrier=selectedCode;

                                                        this.$el.find('#airlinesList p').html(airlineName);
                                                        this.$el.find('#airlinesList').removeClass("has-error");
                                                        }



                                                        },
                                                        searchPriority:function(e){
                                                        e.preventDefault();
                                                        e.stopImmediatePropagation();

                                                        var airportText = this.$el.find('#airport_text').val();
                                                        if (hasValue(airportText)){
                                                            var resultObj = validateAirportTextField(airportText,this.model.get("AirportCode"));

                                                            if(resultObj.isValid){
                                                            var list = this.model.get("PriorityAirlines");

                                                            if(list.length === 1){
                                                                sessionStorage.skyPriorityCarrier = list[0];
                                                                this.model.set({"SelectedAirLineCode":list[0]});
                                                                this.model.set({"SelectedAirLineName":$.i18n.map["sky_"+list[0]]});
                                                            }


                                                            var selectedAirlineName = this.model.get('SelectedAirLineName');
                                                            var airLineName = this.$el.find('#airlinesList p').html();

                                                                if(selectedAirlineName == airLineName){




                                                        var spFinderBasicURL=URLS.SKYPRIORITY_BASIC+'triporigin='+sessionStorage.airport_code+'&operatingcarrier='+sessionStorage.skyPriorityCarrier;

                                                        var requestData = requestBuilder(spFinderBasicURL);
                                                        showActivityIndicatorForAjax('false');
                                                        //making service call
                                                        callService(requestData,this.detailsServiceCallBack);







                                                                }
                                                                else{//airline value is not given
                                                                  this.$el.find('#airlinesList').addClass("has-error");
                                                                }
                                                            }
                                                            else{// proper airport value is not given
                                                                this.$el.find('#airport_text').addClass("has-error");
                                                            }

                                                            }//airport value is not given
                                                        else{
                                                        this.$el.find('#airport_text').addClass("has-error");
                                                        }
                                                        },
                                                        retainHeight : function(e) {
														e.preventDefault();
                                                        var inputField = this.$el.find('#airport_text');

                                                        if( !$(inputField).hasClass('has-error')){
                                                           var airportText = inputField.val().toLowerCase();
                                                           var modelAirportValue = this.model.get("AirportName").toLowerCase();
                                                           if((!hasValue(airportText)) || modelAirportValue.indexOf(airportText) < 0){
                                                            $(inputField).addClass('has-error')
                                                           }
                                                        }
                                                        /*currentView.$el.find('.priority_img').css("margin-top","0px");*/

                                                        },
                                                        /*changeHeight : function(e){
														e.preventDefault();
													    if(e.target.id !== "operator-ok" && e.target.id !=="searchPriority"){
														currentView.$el.find('.priority_img').css("margin-top","-10%");
														}
                                                        },*/
                                                        // to create focus...
                                                        createFocus_Skypriority : function(e){
                                                        isReadMoreDisabled = false;
                                                        currentView.$el.find("#airport_text").val("");

                                                        currentView.$el.find(".cancel_Skytips").css("visibility","hidden");
                                                        currentView.$el.find('.search_skytips').css('visibility', 'visible');
                                                        currentView.$el.find("#airport_text").focus();
                                                        },
                                                        closeMembersPopup : function(e){
														 e.preventDefault();
														 e.stopImmediatePropagation();
                                                        //currentView.model.set({"PriorityAirlines":"","SelectedAirLineCode":"");
                                                        var selectedCode = currentView.$el.find('.scrollforflightsname :checked').val();
                                                        if(hasValue(selectedCode)){

                                                        currentView.model.set({"SelectedAirLineCode":selectedCode});
                                                        currentView.model.set({"SelectedAirLineName":$.i18n.map['sky_'+selectedCode]});
                                                        sessionStorage.skyPriorityCarrier=selectedCode;

                                                        currentView.$el.find('#airlinesList p').html($.i18n.map['sky_'+selectedCode]);
                                                        currentView.$el.find('#airlinesList').removeClass("has-error");

                                                        //currentView.$el.find('#airport_flightNumber').val('');
                                                        //currentView.$el.find(".cancel_flightNumber").css("visibility", "hidden");
                                                        }


                                                        currentView.$el.find('#members-popup').hide();
                                                        currentView.$el.find('.members-ok').hide();
                                                        currentView.$el.find("#blackshade").hide();
                                                        },
                                                        openMembersPopup : function(e){
                                                        if(e){
                                                        e.preventDefault();
                                                        e.stopImmediatePropagation();
                                                        }

                                                        var input=this.$el.find('#airport_text').val();

                                                        var airportCode = this.model.get("AirportCode");
                                                        var checkResponse =validateAirportTextField(input,airportCode);
                                                        if( checkResponse.isValid ){

                                                             var priorityAirlineList = this.model.get("PriorityAirlines");

                                                            if(hasValue(priorityAirlineList) && priorityAirlineList.length >0){
                                                                this.populateAirlineList();
															}
															else{
															this.getSkyPriorityAirlines(airportCode);

															}
                                                        }
                                                        else{
                                                        this.$el.find('#airport_text').addClass("has-error");
                                                        }
                                                        },
                                                        /*  Functionality on clicking cancel image in input field  */
                                                        crossImage_Skypriority: function(e){
                                                        isReadMoreDisabled = false;
                                                        currentView.$el.find("#airport_text").val("");
                                                        currentView.model.set({'AirportCode':''});
                                                        sessionStorage.airport_code = '';
                                                        currentView.model.set({'AirportName':''});
                                                        sessionStorage.airportName ='';
                                                        currentView.$el.find(".cancel_Skytips").css("visibility","hidden");
                                                        currentView.$el.find('.search_skytips').css('visibility', 'visible');

                                                        currentView.$el.find("#airport_text").focus();
                                                        },
                                                        /* End of function */


                                                        showAirportNames : function(e)   {

                                                        $('#airport_text').keyup(function() {
                                                                                 if ($('#airport_text').val() != ''){
                                                                                 $('.cancel_Skytips').css('visibility', 'visible');
                                                                                 $('.search_skytips').css('visibility', 'hidden');
                                                                                 }
                                                                                 else{
                                                                                 $('.cancel_Skytips').css('visibility', 'hidden');
                                                                                 $('.search_skytips').css('visibility', 'visible');
                                                                                 }
                                                                                 });
                                                if (e.keyCode == 8) {
                                                        keyval = $('#' + e.target.id).val();
                                                }
                                                else {
                                                        keyval = keyval + String.fromCharCode(e.keyCode);
                                                }
                                                        if (keyval != "" && e.keyCode != 8) {
                                                        sessionStorage.keyval = keyval;
                                                        var minLenVal=3;

                                                        var   names=[];
                                                        names=JSON.parse(localStorage.cityList);
                                                        if(checkCJKCompliedLanguage())
                                                        {

                                                        minLenVal=1;

                                                        }


                                                        if (names != null) {
                                                        toHighlightAutoComplete(true);
                                                        $("#airport_text").autocomplete({
                                                                                        source : names,
                                                                                        minLength : minLenVal,
                                                                                        open : function(event, ui) {
                                                                                        $(".ui-autocomplete").scrollTop(0);
                                                                                        },
                                                                                        select : airportselected
                                                                                        });
                                                        function airportselected(event, ui) {
                                                        if(hasValue(ui.item.airportCode)){
                                                              currentView.model.set({"AirportCode":ui.item.airportCode});
                                                        }
                                                        currentView.model.set({"AirportName":ui.item.value});

														$(this).blur();
                                                        $(this).css("border-color", "");

                                                        var selectedText=ui.item.value;

                                                        var resultObj = validateAirportTextField( ui.item.value,ui.item.airportCode );
                                                        if(resultObj.isValid){
                                                        /* invalidating previously fetched airlines if airport value has changed */
                                                        if(sessionStorage.airport_code !== resultObj.Code){
                                                        sessionStorage.skyPriorityAirlines ='';

                                                        currentView.model.set({"PriorityAirlines":"","SelectedAirLineCode":""});

                                                        currentView.$el.find('.scrollforflightsname').html('');
                                                        //change previously selected airline text
														currentView.$el.find('#airlinesList p').html($.i18n.map.sky_operating_airline);

                                                        //navigated from airport details,removing the previous pages & hiding the back button
														if(sessionStorage.previousPages !== undefined && sessionStorage.previousPages !== "[]" && (JSON.parse(sessionStorage.previousPages).length > 1) ){
														sessionStorage.removeItem('previousPages');
														$('#backButtonList').hide();
														}


                                                        }
                                                        sessionStorage.airport_code = resultObj.Code;
                                                        sessionStorage.airportName = resultObj.Name;
                                                        }
                                                        if (sessionStorage.skytipsReqData != null) {
                                                        sessionStorage.skytipsReqData = null;
                                                        }

                                                        }
                                                        }
                                                        }
                                                        if (sessionStorage.deviceInfo == "android") {
                                                            if(e.keyCode == 13){
                                                            $(this).blur();
                                                            }
                                                        }

                                                        },
                                                        getSkyPriorityAirlines:function(airportcode){
                                                        showActivityIndicatorForAjax('false');
                                                        var airportDetails_URL = getAirportFinderURL(airportcode);
                                                        var requestData = requestBuilder(airportDetails_URL);

                                                        var callBack =function(response){
                                                            if(typeof response === "string"){
                                                            response=JSON.parse(response);
                                                            }
                                                            hideActivityIndicator();
                                                            if(response.SkyPriorityFeatures === "Y" && response.SkyPriorityAirlines.length >1){
                                                                currentView.model.set({"PriorityAirlines":response.SkyPriorityAirlines.split(",")});
                                                                sessionStorage.skyPriorityAirlines=response.SkyPriorityAirlines;

                                                            var list = currentView.model.get("PriorityAirlines");

                                                            if(list.length === 1){
                                                            currentView.$el.find('#airlinesList p').html($.i18n.map['sky_'+list[0]]);
                                                            currentView.model.set({"SelectedAirLineCode":list[0]});
                                                            }
                                                            else{//no need to show the previousky selecteda irline if more than one airline
                                                             currentView.populateAirlineList();

                                                            }

                                                            }
                                                            else{
                                                            hideActivityIndicator();
                                                            openAlertWithOk($.i18n.map.sky_priority_no_benefits);
                                                            localStorage.SPDeatils ='';
                                                            }
                                                        };

                                                        callService(requestData,callBack);


                                                        },

                                                        checkSelectedAirline: function(e)
                                                        {
                                                        var forid = '#' + $(e.target).attr('for');
                                                        currentView.$el.find(forid).prop("checked", true);
                                                        e.preventDefault();

                                                        },
                                                        detailsServiceCallBack:function(data){

                                                        if(data.SkyPriorityEligibility === "Y" ){

                                                        localStorage.SPDeatils= JSON.stringify(data.TripOriginFeatures);
                                                        var obj1 = {};
                                                        obj1.previousPage = sessionStorage.currentPage;
                                                        pushPageOnToBackStack(obj1);

                                                        /* remove Sky priority banner image from DOM*/
                                                        removeBackgroundImage($('div.sp_banner-img'));
                                                        sessionStorage.skyPriorityCarrier = currentView.model.get('SelectedAirLineCode');
                                                        sessionStorage.skyPriorityAirlines = currentView.model.get('PriorityAirlines');
                                                        window.location.href="index.html#skyPriorityDetails";

                                                        }
                                                        else{
                                                        hideActivityIndicator();
                                                        openAlertWithOk($.i18n.map.sky_priority_basic_no_benefits);
                                                        }
                                                        },
                                                        populateAirlineList:function(e){
                                                        if(e){
                                                        e.preventDefault();
                                                        e.stopImmediatePropagation();
                                                        }
                                                        $("#blackshade").show();
                                                        $('#blackshade').css({
                                                                             opacity : 0.6,
                                                                             'width' : $(document).width(),
                                                                             'height' : $(document).height()
                                                                             });
                                                        require(['view/skyPriority/subViews/skyPriorityAirlines'], function(skyPriorityAirlinesView) {
                                                                var skyPriorityAirlinesView = new skyPriorityAirlinesView({'model':currentView.model});
                                                                skyPriorityAirlinesView.render();});
                                                        }



                                                        });
       return skyPriorityFinderView;

       });
//**** start-Function to generate airlines for sky priority ** //
