define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html', 'text!template/skyPriorityFinderTemplate.html'], 
		function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate, skyPriorityFinderTemplate) {

	var memberSelected,
		keyval = "";
		isCloseBtnClicked = false,
		strKeyboardLang="";
		sessionStorage.skyPriorityCarrier = '';
	var skyPriorityFinderView = Backbone.View.extend({
		
		
		
		el: '#skyteam',
		id: 'skyPriorityFinder-page',

		initialize: function() {
			$('#backButtonList').hide();
			$('#headingText img').css('display', 'none');
			$('#headingText h2').css('display', 'block');
			$('#headingText h2').text($.i18n.map.sky_priority_finder);
			hideActivityIndicator();
			if ("yes" === sessionStorage.isFromSaved || "yes" === sessionStorage.savedflights) {
				$('#brief img').css('display', 'block');
				$('#brief h2').css('display', 'none');

				sessionStorage.savedflights = "no";
				memberSelected = 0;
				sessionStorage.isFromSaved = "no";
			}
			if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
				$('.main_header ul li:eq(0) img').remove();
				$('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));
				$(".menuItems").pageslide();
			}

			$(".menuItems").pageslide();
			sessionStorage.currentPage = "skyPriorityFinder";

			 if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
                              hideActivityIndicator();
                         }
                         else
                         {
                              window.getAirportsCallback = function() {
                                    hideActivityIndicator();
                               }
                              showActivityIndicator($.i18n.map.sky_please);
                              window.open(iSC.getAirports);
                         }
		},

		render: function() {
			/* Back button click event */
			$('.main_header ul li:eq(1) img').off('click').on('click', function(e) {

				sessionStorage.navDirection = "Left";

				if("block" === $('#pageslide').css('display')){
					e.preventDefault();
					e.stopImmediatePropagation();
					$.pageslide.close();
				}
				else
				{
					var currentobj = JSON.parse(sessionStorage.previousPages);
					showActivityIndicator($.i18n.map.sky_please);


					if ("airportDetails" === currentobj[0].previousPage || "airportDetailsViewViaMap" === currentobj[0].previousPage ) {

						if ("homePage" === currentobj[0].isFrom) {
							sessionStorage.savedFlightDetails = currentobj[0].data;
						}
						sessionStorage.currentPage = currentobj[0].isFrom;

					}

					sessionStorage.removeItem('lastPage');
					e.preventDefault();
					window.location.href = "index.html#" + currentobj[0].previousPage;
					popPageOnToBackStack();
					$(self.el).undelegate();
				}
			});
			/* End of back functionality */
			memberSelected = 0;
			var locationPath, spath;
			locationPath = window.location.toString();
			spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
			
			var compiledTemplate = _.template(skyPriorityFinderTemplate, $.i18n.map);
			$(this.el).html(compiledTemplate);
			$.pageslide.close(); {

				if ( undefined != sessionStorage.previousPages && "[]" !== sessionStorage.previousPages) {
					var previousPagesJSON = JSON.parse(sessionStorage.previousPages);

					if ( "airportDetails" === previousPagesJSON[0].previousPage || "airportDetailsViewViaMap" === previousPagesJSON[0].previousPage) {

						$('#backButtonList').show();
						var countryCodeOld;
						countryCodeOld = getCountryName(sessionStorage.airport_code);
						
						$('#airport_text').val(sessionStorage.airportName + " (" + sessionStorage.airport_code + "), " + countryCodeOld.split('_')[0]);
						$('.cancel_Skytips').css('visibility', 'visible');
						$('.search_skytips').css('visibility', 'hidden');

						$('.scrollforflightsname').html(getAirlines(sessionStorage.skyPriorityAirlines));
						
						/* adjusting the height & vertically align the popup vertically middle*/
                        var airlinesListHeight=(sessionStorage.skyPriorityAirlines.split(',').length+1)*39;
                        var popupWidgetHeight = $('#skyteam').height()*.95;
                        if(airlinesListHeight < popupWidgetHeight){
                        //adjusting the height of the popup
                        $('#members-popup').height('auto');
                        
                        //vertically aligning the popup to middle
                        $('#members-popup').css('top',(popupWidgetHeight-airlinesListHeight)/2);
                        }
                        else{
                        $('#members-popup').height('95%');
                        $('#members-popup').css('top','10px');
                        }
                        setTimeout(function(){
                        $('#airlinesList').trigger('click');
                        },352);
						
					} else if ("skyPriorityFinder" === previousPagesJSON[0].previousPage) {
						popPageOnToBackStack();
						if (1 < previousPagesJSON.length) { //If visited airport details
							$('#backButtonList').show();
						}
						var countryCodeOld;
						countryCodeOld = getCountryName(sessionStorage.airport_code);

						$('#airport_text').val(sessionStorage.airportName + " (" + sessionStorage.airport_code + "), " + countryCodeOld.split('_')[0]);
						$('.cancel_Skytips').css('visibility', 'visible');
						$('.search_skytips').css('visibility', 'hidden');
						setTimeout(function(){
						$('#popupWrapper').html(sessionStorage.SPAirlines);
						},315);
					}
				}


			}
slideThePage(sessionStorage.navDirection);
		},
		events: {
			'focusin :input': 'changeHeight',
			'keydown :input': 'showAirportNames',
			'focusout input': 'retainHeight',
			'click .cancel_Skytips': 'crossImage_Skypriority',
			'click .search_skytips': 'createFocus_Skypriority',
			'click #airlinesList': 'openMembersPopup',
			'click .members-ok': 'closeMembersPopup',
			'click #searchPriority': 'searchPriority',
			'click .airlineName' : 'checkSelectedAirline',
			'touchstart *': function() {
				$.pageslide.close();
			}


		},
		searchPriority: function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			if ( 0 === memberSelected  || undefined == memberSelected ) {
				$('#airlinesList').css("border-color", "red");
			}
			var isAirportTextValid = false;
			var airportValue = $('#airport_text').val();
			if ('' != airportValue) {
				isAirportTextValid = validateAirportTextField(airportValue, sessionStorage.airport_code).isValid;
			}
			if (!isAirportTextValid) {
				$('#airport_text').css("border-color", "red");
			}


			if (memberSelected != 0 && memberSelected != undefined && isAirportTextValid) {
				showActivityIndicatorForAjax('false');
				sessionStorage.SPAirlines = $('#popupWrapper').html();
				setTimeout(function() {
					//*** start of  the ajax SPFinder basic call
					var isResponseValid;
					localStorage.SPDeatils = '';
					isResponseValid = false;
                    var acceptlanguage=  (localStorage.language == "zh")?'zh-Hans':localStorage.language;
					var spFinderBasicURL = URLS.SKYPRIORITY_BASIC+'triporigin=' + sessionStorage.airport_code + '&operatingcarrier=' + sessionStorage.skyPriorityCarrier;
					xhrajax = $.ajax({
						url: spFinderBasicURL,
						type: 'GET',
						datatype: JSON,
						timeout: 30000,
						async: true,
						 beforeSend: function (request)
							{
								request.setRequestHeader("Accept-Language", acceptlanguage);
								request.setRequestHeader("api_key", API_KEY);
								request.setRequestHeader("source", "SkyApp");
							},
						success: function(response) {
							hideActivityIndicator();

							if (response.SkyPriorityEligibility === "Y") {
								localStorage.SPDeatils = JSON.stringify(response.TripOriginFeatures);
								var obj1 = {};
								obj1.previousPage = sessionStorage.currentPage;
								pushPageOnToBackStack(obj1);
								window.location.href = "index.html#skyPriorityDetails";

							} else {
								hideActivityIndicator();
								openAlertWithOk($.i18n.map.sky_priority_basic_no_benefits);
								localStorage.SPDeatils = '';
							}
						},
						error: function(xhr, exception) {
							hideActivityIndicator();
							if (exception == "timeout") {
								openAlertWithOk($.i18n.map.sky_unload);
							} else if (exception == "error") {
								openAlertWithOk($.i18n.map.sky_notconnected);
							} else {

							}
						},
						failure: function() {
							hideActivityIndicator();
						}

					});
					//*** end of  the ajax SPFinder basic call
					
				}, 5);

			}
		},
		retainHeight: function(e) {
			e.preventDefault();
			//	e.stopimmediatepropagation();
			$('.priority_img').css("margin-top", "0px");

		},
		changeHeight: function(e) {
			e.preventDefault();
			//	e.stopimmediatepropagation();
			if (e.target.id !== "operator-ok" && e.target.id !== "searchPriority") {
				$('.priority_img').css("margin-top", "-10%");
			}
		},
		// to create focus...
		createFocus_Skypriority: function(e) {
			isReadMoreDisabled = false;
			$("#airport_text").val("");

			$(".cancel_Skytips").css("visibility", "hidden");
			$('.search_skytips').css('visibility', 'visible');
			$("#airport_text").focus();
		},
		closeMembersPopup: function(e) {
			e.preventDefault();
			if ($('.scrollforflightsname :checked').val() != memberSelected) {
				$('#airport_flightNumber').val('');
				$(".cancel_flightNumber").css("visibility", "hidden");
			}
			memberSelected = $('.scrollforflightsname :checked').val();
			sessionStorage.skyPriorityCarrier = memberSelected;
			$('#airlinesList p').html($.i18n.map['sky_' + memberSelected]);
			$('#airlinesList').css('border', 'none');
			$('#members-popup').css('display', 'none');
			$('.members-ok').css('display', 'none');
			$("#blackshade").css('display', 'none');
		},
		openMembersPopup: function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			var inputText = $("#airport_text").val();
			var validation = false;
			if(sessionStorage.airport_code != null )
				validation = validateAirportTextField(inputText,sessionStorage.airport_code).isValid;
			
			if(validation){

				if(isValid(sessionStorage.skyPriorityAirlines) && sessionStorage.skyPriorityAirlines.length >1){
					$('#members-popup').css('display','block');
					$('.members-ok').css('display','block');
					$("#blackshade").css('display', 'block');
				}
				else{
					this.getSkyPriorityAirlines(sessionStorage.airport_code);

				}
			}
			else{
				$('#airport_text').css("border-color", "red");
			}

		},
		/*  Functionality on clicking cancel image in input field  */
		crossImage_Skypriority: function(e) {
			isReadMoreDisabled = false;
			$("#airport_text").val("");

			$(".cancel_Skytips").css("visibility", "hidden");
			$('.search_skytips').css('visibility', 'visible');
			isCloseBtnClicked = true;
			$("#airport_text").focus();
		},
		/* End of function */


		showAirportNames: function(e) {

			$('#airport_text').keyup(function() {
				if ($('#airport_text').val() != '') {
					$('.cancel_Skytips').css('visibility', 'visible');
					$('.search_skytips').css('visibility', 'hidden');
				} else {
					$('.cancel_Skytips').css('visibility', 'hidden');
					$('.search_skytips').css('visibility', 'visible');
				}
			});
			if (8 === e.keyCode) {
				keyval = $('#' + e.target.id).val();
				
			} else {
				keyval = keyval + String.fromCharCode(e.keyCode);
			}
			if ( ("" != keyval) &&  (8 != e.keyCode) ) {
				sessionStorage.keyval = keyval;
				


				   var minLenVal=3,
                   	   names=[];
				   
                   names=JSON.parse(localStorage.cityList);
                   if(localStorage.language==="zh" || "Ja" === localStorage.language){
                   minLenVal=1;
                   }


				if (null != names) {
					toHighlightAutoComplete(true);
					$("#airport_text").autocomplete({
						source: names,
						minLength: minLenVal,
						open: function(event, ui) {
							$(".ui-autocomplete").scrollTop(0);
						},
						select: airportselected
					});

					function airportselected(event, ui) {
						$(this).blur();
						$(this).css("border-color", "");
						var selectedAiportCode = ui.item.airportCode;
						
						/* invalidating previously fetched airlines if airport value has changed */ 
						if(sessionStorage.airport_code !== selectedAiportCode){
							sessionStorage.skyPriorityAirlines ='';
							$('.scrollforflightsname').html('');
							//change previously selected airline text
							$('#airlinesList p').html($.i18n.map.sky_operating_airline);

							//navigated from airport details,removing the previous pages & hiding the back button
							if(sessionStorage.previousPages !== undefined && sessionStorage.previousPages !== "[]" && (JSON.parse(sessionStorage.previousPages).length > 1) ){
								sessionStorage.removeItem('previousPages');
								$('#backButtonList').hide();
							}


						}
						sessionStorage.airport_code = selectedAiportCode;
						sessionStorage.airportName = getAirportFullName(selectedAiportCode);

						if (null != sessionStorage.skytipsReqData) {
							sessionStorage.skytipsReqData = null;
						}

					}
				}
			}
			if ("android" === sessionStorage.deviceInfo) {
				if (13 === e.keyCode) {
					$(this).blur();
					this.openSkyTips();
				}
			}
		},
		getSkyPriorityAirlines:function(airportcode){
			showActivityIndicatorForAjax('false',$.i18n.map.sky_please_data);
			var airportDetails_URL = getAirportFinderURL(airportcode);
			xhrajax=$.ajax({
				url : airportDetails_URL,
				headers : {"api_key": API_KEY, "source": "SkyApp"},
				type: 'GET',
				success: function(response)
				{
					if(typeof response === "string"){
						response=JSON.parse(response);
					}
					hideActivityIndicator();
					if("Y" === response.SkyPriorityFeatures && 1 < response.SkyPriorityAirlines.length){
						sessionStorage.skyPriorityAirlines=response.SkyPriorityAirlines;
						$('.scrollforflightsname').html(getAirlines(sessionStorage.skyPriorityAirlines));
						$('#members-popup').css('display','block');
						$('.members-ok').css('display','block');
						$("#blackshade").css('display', 'block');
						
						//adjusting airline height & align the popup vertically center
                        
                        var airlinesListHeight=(sessionStorage.skyPriorityAirlines.split(',').length+1)*39;
                        var popupWidgetHeight = $('#skyteam').height()*.95;
	                           if(airlinesListHeight < popupWidgetHeight){
		                           //adjusting the height of the popup
		                           $('#members-popup').height('auto');
		                           
		                           //vertically aligning the popup to middle
		                           $('#members-popup').css('top',(popupWidgetHeight-airlinesListHeight)/2);
	                           }
	                           else{
		                           $('#members-popup').height('95%');
		                           $('#members-popup').css('top','10px');
	                           }
					}
					else{
						hideActivityIndicator();
						openAlertWithOk($.i18n.map.sky_priority_no_benefits);
						localStorage.SPDeatils ='';
					}
				},
				error: function(xhr,exception,errorThrown){
					hideActivityIndicator();
					if("timeout" === exception)
					{
						openAlertWithOk($.i18n.map.sky_unload);
					}

					else if("error" === exception){
						openAlertWithOk($.i18n.map.sky_notconnected);
					}
					else{

					}
				},
				failure: function(){
					hideActivityIndicator();
				}

			});
		},
		
		 /* function to check the radiobutton(airline) when corresponding label is tapped */
		checkSelectedAirline: function(e) {
			var forid ='#' + $(e.target).attr('for');
			$(forid).prop("checked", true);
			e.preventDefault();
		}


	});
	return skyPriorityFinderView;

});
//**** start-Function to generate airlines for sky priority **

function getskyPriorityDetailsAjax(stayOnThePage) {
	var isResponseValid;
	localStorage.SPDeatils = '';
	isResponseValid = false;
    var acceptlanguage= (localStorage.language == "zh")?'zh-Hans':localStorage.language;
	var spFinderBasicURL = URLS.SKYPRIORITY_BASIC+'triporigin=' + sessionStorage.airport_code + '&operatingcarrier=' + sessionStorage.skyPriorityCarrier;
	xhrajax = $.ajax({
		url: spFinderBasicURL,
		type: 'GET',
		datatype: JSON,
		timeout: 500,
		async: false,
		 beforeSend: function (request)
			{
				request.setRequestHeader("Accept-Language", acceptlanguage);
				request.setRequestHeader("api_key", API_KEY);
				request.setRequestHeader("source", "SkyApp");
			},
		success: function(response) {
			
			if ("Y" === response.SkyPriorityEligibility) {
				isResponseValid = true;
				localStorage.SPDeatils = JSON.stringify(response.TripOriginFeatures);
				if (!stayOnThePage) {
					window.location.href = "index.html#skyPriorityDetails";
				}
			} else {
				hideActivityIndicator();
				openAlertWithOk($.i18n.map.sky_priority_no_benefits);
				localStorage.SPDeatils = '';
			}
		},
		error: function(xhr, exception) {
			hideActivityIndicator();
			if ("timeout" === exception) {
				openAlertWithOk($.i18n.map.sky_unload);
			} else if ("error" === exception) {
				openAlertWithOk($.i18n.map.sky_notconnected);
			} else {

			}
		},
		failure: function() {
			hideActivityIndicator();
		}

	});
	return isResponseValid;

}