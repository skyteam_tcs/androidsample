define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html', 'text!template/SkyPriorityAdvancedDetailsTemplate.html'], 
		function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate, skyPriorityAdvancedDetailsTemplate) {

    var memberSelected, 
    	keyval = "", 
    	disclaimerTextShownFlag = 0; 
    	
    
    var skyPriorityDetailsView = Backbone.View.extend({

        el: '#skyteam',
        id: 'skyPriorityAdvancedDetails-page',

        initialize: function () {
            $('#backButtonList').show();
            $('#headingText h2').css('margin-right', '');
            $('#headingText img').css('display', 'none');
            $('#headingText h2').css('display', 'block');
            $('#headingText h2').text($.i18n.map.sky_priority_details);
            $('#headingText h2').css("margin-right", "37px");
             
            if (("yes" === sessionStorage.isFromSaved) || ("yes" === sessionStorage.savedflights)) {
                $('#brief img').css('display', 'block');
                $('#brief h2').css('display', 'none');
                sessionStorage.savedflights = "no";
                sessionStorage.isFromSaved = "no";
            }
            if ('menuItems' !== $('.main_header ul li:eq(0) a').attr('class')) {
                $('.main_header ul li:eq(0) img').remove();
                $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

            }

            $(".menuItems").pageslide();
        },

        render: function () {
            disclaimerTextShownFlag = 0;
            var locationPath, spath, compiledTemplate;
            locationPath = window.location.toString();
            spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
            compiledTemplate = _.template(skyPriorityAdvancedDetailsTemplate, $.i18n.map);
            $(this.el).html(compiledTemplate);
            $('.skypriority_details ul li:nth-child(4)').css('margin-top', '0em');
            sessionStorage.isDisclaimerTextShown = false;
            /*Back button functionality */
            $('.main_header ul li:eq(1) img').off('click').on('click', function (e) {

                e.preventDefault();
                e.stopImmediatePropagation();
              sessionStorage.navDirection = "Left";

                if("block" === $('#pageslide').css('display')){
           		 $.pageslide.close();
                }
                else{
                window.location.href = 'index.html#' + JSON.parse(sessionStorage.previousPages)[0].previousPage;
                popPageOnToBackStack();
                $('.main_header ul li:eq(1) img').off('click');
                }
            });
            /* End of Back functionality */
            
            sessionStorage.currentPage = "skyPriorityAdvancedDetails";
            paintAdvanceDetails();

slideThePage(sessionStorage.navDirection);
        },
        events: {

            'click .info': 'infoDetails',
            'touchstart *': function () {
                $.pageslide.close();
            }

        },
        infoDetails: function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            if ((e.target.textContent === $.i18n.map.sky_moreinfo)) {
                $(e.target.parentElement.parentElement).find('.sp_dep_content').addClass('noclass');
                $(e.target.parentElement.parentElement).find('.sp_container_open').removeClass('noclass');
                e.target.textContent = $.i18n.map.sky_lessinfo;
                if ($(e.target).hasClass('disclaimerShown')) {
                    disclaimerTextShownFlag++;
                }
                if (0 < disclaimerTextShownFlag && $('.disclaimer_text').hasClass('noclass')) {
                    $('.disclaimer_text').removeClass('noclass');
                }

            } else if ((e.target.textContent === $.i18n.map.sky_lessinfo)) {
                e.target.textContent = $.i18n.map.sky_moreinfo;
                $(e.target.parentElement.parentElement).find('.sp_container_open').addClass('noclass');
                $(e.target.parentElement.parentElement).find('.sp_dep_content').removeClass('noclass');
                if ($(e.target).hasClass('disclaimerShown')) {
                    disclaimerTextShownFlag--;
                    if (disclaimerTextShownFlag < 1 && (!$('.disclaimer_text').hasClass('noclass'))) {
                        $('.disclaimer_text').addClass('noclass');
                    }
                }
            } else {}
        }


    });
    return skyPriorityDetailsView;

});

function paintAdvanceDetails() {
    var topResHTML, noResHTML, response, conSmallInfo, conBigInfo, flightDetails,
	    isDepatureAvail = false,
	    isArrivalAvail = false,
	    isConnectionAvail = false,
	    connectionAirportShown = false,
	    isDisclaimerTextShown = false,
	    arrSmallInfo = '',
	    arrBigInfo = '',
	    depSmallInfo = '',
	    depBigInfo = '';
    
    
    var operatingAirlineNames=[];
    $('.Sp_results').html('');
    
    /** logic to get operating Airline name from flight details/flight status **/
    var previousPages= JSON.parse(sessionStorage.previousPages);
    
    if("FlightDetailsPage" === previousPages[0].previousPage){
    	flightDetails = JSON.parse(sessionStorage.flightDetailsObject);
    }
    else if("flightStatusDetails" === previousPages[0].previousPage){
    	flightDetails = JSON.parse(sessionStorage.flightStatusObj);
    }
    
    if("array" === $.type(flightDetails.details.Segments)){
    	$.each(flightDetails.details.Segments,function(index,flight){
    		operatingAirlineNames.push(flight.OperatedBy)
    	});
	}
    else{
    	operatingAirlineNames.push(flightDetails.details.Segments.OperatedBy);
    }
    /** end - logic to get operating Airline name from flight details/flight status **/
    
    response = JSON.parse(sessionStorage.SPAdvance_response);
    topResHTML = $('#spResultTop').html();
    noResHTML = $('#sp_nofeature').html();
    
    $('.Sp_airport h2').html(getAirportCityCountryName(response.TripOriginFeatures.TripOrigin).split('_')[0] + " " + $.i18n.map.sky_to + " " +
        getAirportCityCountryName(response.TripDestinationFeatures.TripDestination).split('_')[0]);


    if ("101" != response.TripOriginFeatures.ErrorCode) { //checking for skyteamairline 

        if ("Y" === response.TripOriginFeatures.PriorityCheckIn) {
            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityCheckIn"] + '"/></div>';
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityCheckIn"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_checkin_title + '</h1><p>' + $.i18n.map.sky_priority_checkin_msg + '</p></div></div>';

            isDepatureAvail = true;

        }
        if ("Y" === response.TripOriginFeatures.PriorityBaggageDropOff) {
            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityBaggageDropOff"] + '"/></div>';
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityBaggageDropOff"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_baggage_title + '</h1><p>' + $.i18n.map.sky_priority_baggage_msg + '</p></div></div>';

            isDepatureAvail = true;
        }
        if ("Y" === response.TripOriginFeatures.PriorityImmigration) {

            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityImmigration"] + '"/></div>';
            if ("0" === response.TripOriginFeatures.ImmigrationFasttrackAccessibility) {
                depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityImmigration"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_immigration_title + '</h1><img src="images/spfinder/cornerStar.png" class="disclaimer_img"/><p>' + $.i18n.map.sky_priority_immigration_msg + '</p></div></div>';
                isDisclaimerTextShown = true;
            } else {
                depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityImmigration"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_immigration_title + '</h1><p>' + $.i18n.map.sky_priority_immigration_msg + '</p></div></div>';
            }
            isDepatureAvail = true;
        }

        if ("Y" === response.TripOriginFeatures.PrioritySecurityLines) {
            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PrioritySecurityLines"] + '"/></div>';
            if ("0" === response.TripOriginFeatures.SecurityFasttrackAccessibility) {
                depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PrioritySecurityLines"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_security_title + '</h1><img src="images/spfinder/cornerStar.png" class="disclaimer_img"/><p>' + $.i18n.map.sky_priority_security_msg + '</p></div></div>';
                isDisclaimerTextShown = true;
            } else {
                depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PrioritySecurityLines"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_security_title + '</h1><p>' + $.i18n.map.sky_priority_security_msg + '</p></div></div>';
            }
            isDepatureAvail = true;
        }

        if ("Y" === response.TripOriginFeatures.PriorityTicketDesk) {
            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityTicketDesk"] + '"/></div>';
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityTicketDesk"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_ticket_title + '</h1><p>' + $.i18n.map.sky_priority_ticket_msg + '</p></div></div>';

            isDepatureAvail = true;
        }
        if ("Y" === response.TripOriginFeatures.PriorityTransferDesk) {
            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityTransferDesk"] + '"/></div>';
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityTransferDesk"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_transfer_title + '</h1><p>' + $.i18n.map.sky_priority_transfer_msg + '</p></div></div>';

            isDepatureAvail = true;
        }
        if ("Y" ===  response.TripOriginFeatures.PriorityBoarding) {
            depSmallInfo = depSmallInfo + '<div><img src="' + spImages_small["PriorityBoarding"] + '"/></div>';
            depBigInfo = depBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityBoarding"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_boarding_title + '</h1><p>' + $.i18n.map.sky_priority_boarding_msg + '</p></div></div>';

            isDepatureAvail = true;
        }

    }


    if (isDepatureAvail) {
        if (isDisclaimerTextShown) {
            $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('="info"', '="info disclaimerShown"').replace('{#toheading}', $.i18n.map.sky_departure).replace('{#airportname}', getAirportFullName(response.TripOriginFeatures.TripOrigin)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[0]) + '<div class="sp_dep_content">' + depSmallInfo + '</div><div class="sp_container_open noclass">' + depBigInfo + '</div></div>');
            //resetting for use the same flag for connection airport details 
            isDisclaimerTextShown = false;
        } else {
            $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('{#toheading}', $.i18n.map.sky_departure).replace('{#airportname}', getAirportFullName(response.TripOriginFeatures.TripOrigin)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[0]) + '<div class="sp_dep_content">' + depSmallInfo + '</div><div class="sp_container_open noclass">' + depBigInfo + '</div></div>');
        }
    } else { //for non skyteam airline or unavailable of feature
        $('.Sp_results').append('<div class="Sp_results_container">' + noResHTML.replace('{#toheading}', $.i18n.map.sky_departure).replace('{#airportname}', getAirportFullName(response.TripOriginFeatures.TripOrigin)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[0]) + '</div>');
    }

    //arrival feature
    if (("101" != response.TripDestinationFeatures.ErrorCode) && ("Y" === response.TripDestinationFeatures.BaggageFirst)) {
        arrSmallInfo = '<div class="sp_dep_content"><img class="sp_arrival_img" src="' + spImages_small["BaggageFirst"] + '"/></div>';
        arrBigInfo = '<div class="sp_container_open noclass"><div class="box"><div class="leftcontent"><img src="' + spImages_large["BaggageFirst"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_baggage_handle_title + '</h1><p>' + $.i18n.map.sky_priority_baggage_handle_msg + '</p></div></div></div>';
        isArrivalAvail = true;

    }


    if (undefined === response.TransferAirportFeatures) {
        connectionAirportShown = false;
    } else {
        connectionAirportShown = true;
        var leng = response.TransferAirportFeatures.length;
        for (i = 0; i < leng / 2; i++) {
            //resetting isDisclaimerTextShown flag
            isDisclaimerTextShown = false;
            var departure, arrival, j;
            var arrivalHTML, departureHTML;
            arrivalHTML = '';
            departureHTML = '';
            j = 2 * i;
            conSmallInfo = '';
            conBigInfo = '';
            departure = response.TransferAirportFeatures[j];
            arrival = response.TransferAirportFeatures[j + 1];
            var connectionTitle;
            if (2 < leng) { //Getting connection no 1,2..if more than 1 connection airport
                connectionTitle = $.i18n.map.sky_priority_connection_airport + " " + (i + 1);
            } else {
                connectionTitle = $.i18n.map.sky_priority_connection_airport;
            }
            //connectioj airport arrival features
            if ("101" != arrival.ErrorCode) { //non skyteam airline check
                if ("Y" === arrival.PriorityCheckIn) {
                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PriorityCheckIn"] + '"/></div>';
                    conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityCheckIn"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_checkin_title + '</h1><p>' + $.i18n.map.sky_priority_checkin_msg + '</p></div></div>';

                    isConnectionAvail = true;
                }
                if ("Y" === arrival.PriorityBaggageDropOff) {
                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PriorityBaggageDropOff"] + '"/></div>';
                    conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityBaggageDropOff"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_baggage_title + '</h1><p>' + $.i18n.map.sky_priority_baggage_msg + '</p></div></div>';

                    isConnectionAvail = true;
                }
                if ("Y" === arrival.PriorityImmigration) {

                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PriorityImmigration"] + '"/></div>';
                    if ("0" === arrival.ImmigrationFasttrackAccessibility) {
                        conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityImmigration"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_immigration_title + '</h1><img src="images/spfinder/cornerStar.png" class="disclaimer_img"/><p>' + $.i18n.map.sky_priority_immigration_msg + '</p></div></div>';
                        isDisclaimerTextShown = true;
                    } else {
                        conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityImmigration"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_immigration_title + '</h1><p>' + $.i18n.map.sky_priority_immigration_msg + '</p></div></div>';
                    }
                    isConnectionAvail = true;
                }
                if ("Y" === arrival.PrioritySecurityLines) {
                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PrioritySecurityLines"] + '"/></div>';
                    if ("0" === arrival.SecurityFasttrackAccessibility) {
                        conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PrioritySecurityLines"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_security_title + '</h1><img src="images/spfinder/cornerStar.png" class="disclaimer_img"/><p>' + $.i18n.map.sky_priority_security_msg + '</p></div></div>';
                        isDisclaimerTextShown = true;
                    } else {
                        conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PrioritySecurityLines"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_security_title + '</h1><p>' + $.i18n.map.sky_priority_security_msg + '</p></div></div>';
                    }
                    isConnectionAvail = true;
                }

                if ("Y" === arrival.PriorityTicketDesk) {
                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PriorityTicketDesk"] + '"/></div>';
                    conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityTicketDesk"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_ticket_title + '</h1><p>' + $.i18n.map.sky_priority_ticket_msg + '</p></div></div>';

                    isConnectionAvail = true;
                }
                if ("Y" === arrival.PriorityTransferDesk) {
                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PriorityTransferDesk"] + '"/></div>';
                    conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityTransferDesk"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_transfer_title + '</h1><p>' + $.i18n.map.sky_priority_transfer_msg + '</p></div></div>';

                    isConnectionAvail = true;
                }
                if ("Y" === arrival.PriorityBoarding) {
                    conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["PriorityBoarding"] + '"/></div>';
                    conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["PriorityBoarding"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_boarding_title + '</h1><p>' + $.i18n.map.sky_priority_boarding_msg + '</p></div></div>';

                    isConnectionAvail = true;
                }
            }
            //connection airport arrival
            if ("101" != departure.ErrorCode && "Y" === departure.BaggageFirst) {

                conSmallInfo = conSmallInfo + '<div><img src="' + spImages_small["BaggageFirst"] + '"/></div>';
                conBigInfo = conBigInfo + '<div class="box"><div class="leftcontent"><img src="' + spImages_large["BaggageFirst"] + '"/></div><div class="rightcontent"><h1>' + $.i18n.map.sky_priority_baggage_handle_title + '</h1><p>' + $.i18n.map.sky_priority_baggage_handle_msg + '</p></div></div>';
                isConnectionAvail = true;
            }
            if (isConnectionAvail) {
                if (isDisclaimerTextShown) {
                    $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('="info"', '="info disclaimerShown"').replace('{#toheading}', connectionTitle).replace('{#airportname}', getAirportFullName(departure.TransferAirport)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[i+1]) + '<div class="sp_dep_content">' + conSmallInfo + '</div><div class="sp_container_open noclass">' + conBigInfo + '</div></div>');
                } else {
                    $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('{#toheading}', connectionTitle).replace('{#airportname}', getAirportFullName(departure.TransferAirport)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[i+1]) + '<div class="sp_dep_content">' + conSmallInfo + '</div><div class="sp_container_open noclass">' + conBigInfo + '</div></div>');
                }
            } else {
                $('.Sp_results').append('<div class="Sp_results_container">' + noResHTML.replace('{#toheading}', connectionTitle).replace('{#airportname}', getAirportFullName(departure.TransferAirport)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[i+1]) + '</div>');
            }

        } //for loop end
    }

   if (isArrivalAvail) {
        var destinationOperatingCarrier = '';
        if (connectionAirportShown) {
            destinationOperatingCarrier = response.TransferAirportFeatures[response.TransferAirportFeatures.length - 1].OperatingCarrier;
        } else {
            destinationOperatingCarrier = response.TripOriginFeatures.OperatingCarrier;
        }

        $('.Sp_results').append('<div class="Sp_results_container">' + topResHTML.replace('{#toheading}', $.i18n.map.sky_arrival).replace('{#airportname}', getAirportFullName(response.TripDestinationFeatures.TripDestination)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[operatingAirlineNames.length-1]) + arrSmallInfo + arrBigInfo + '</div>');
    } else {
        $('.Sp_results').append('<div class="Sp_results_container">' + noResHTML.replace('{#toheading}', $.i18n.map.sky_arrival).replace('{#airportname}', getAirportFullName(response.TripDestinationFeatures.TripDestination)).replace('{#airlinename}', $.i18n.map.sky_operatedby + ": " + operatingAirlineNames[operatingAirlineNames.length-1]) + '</div>');

    }

}