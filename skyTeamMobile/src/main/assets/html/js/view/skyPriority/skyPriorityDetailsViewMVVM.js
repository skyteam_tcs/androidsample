define(['jquery','underscore', 'backbone', 'utilities', 'constants', 'jqueryui', 'text!template/menuTemplate.html','models/skypriority/skyPriorityDetailsModel', 'text!template/skyPriorityDetailsTemplateMVVM.html','serviceInteraction','appView','models/skypriority/skyPriorityFinderModel'],
       function($, _, Backbone, utilities, constants, jqueryUi, menuPageTemplate, skyPriorityDetailsModel, skyPriorityDetailsTemplateMVVM,serviceInteraction,appView,skyPriorityFinderModel){

       var disclaimerTextShownFlag,self;
       var airlineModel;

       var skyPriorityDetailsView = appView.extend({

                                                         id : 'skyPriorityDetails-page',
                                                         transition:true,
                                                         initialize : function() {
                                                         $('#backButtonList').show();
                                                         $('#headingText h2').css('margin-right','');
                                                         $('#headingText img').hide();
                                                         $('#headingText h2').show();
                                                         $('#headingText h2').text($.i18n.map.sky_priority_details);
                                                         if (sessionStorage.isFromSaved == "yes") {
                                                            $('#brief img').hide();
                                                         }
                                                         else{
                                                            $('#brief img').show();
                                                         }
                                                         if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
                                                         $('.main_header ul li:eq(0) img').remove();
                                                         $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));

                                                         }
                                                         self = this;

                                                         },
                                                         events : {

                                                         'click .info':'infoDetails',
                                                         'click .disclaimerShown': 'showDisclaimerMsg',
                                                         'click .Sp_select_airline': 'openMembersPopup',
                                                         'click .members-ok': 'closeMembersPopup',
                                                         'click .airlineName' : 'checkSelectedAirline',
                                                         'touchstart *' : function() {
                                                         $.pageslide.close();
                                                         }
                                                         },
                                                         render : function() {
                                                           airlineModel = new skyPriorityFinderModel($.i18n.map);
                                                           airlineModel.set({'SelectedAirLineCode':sessionStorage.skyPriorityCarrier});
                                                           airlineModel.set({'PriorityAirlines':sessionStorage.skyPriorityAirlines.split(',')});

                                                         var response=JSON.parse(localStorage.SPDeatils);
                                                         var dataModel = new skyPriorityDetailsModel(response);
                                                         dataModel.resetData();
                                                         dataModel.set($.i18n.map);
                                                         this.model = dataModel;
                                                         var departureData = this.model.get("TripOriginFeatures");
                                                         if(this.model.get("isDisclaimerShown")){
                                                         disclaimerTextShownFlag = 1;
                                                         }
                                                         else{
                                                         disclaimerTextShownFlag = 0;
                                                         }

                                                         var compiledTemplate = _.template(skyPriorityDetailsTemplateMVVM,this.model.toJSON());





                                                         $(this.el).html(compiledTemplate);





                                                         hideActivityIndicator();

                                                         /*Back button functionality */
                                                         $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                                                         comingfromairportskypriority=1;

                                                                                                           e.preventDefault();
                                                                                                           e.stopImmediatePropagation();
                                                                                                           sessionStorage.isFromSPDetails =true;
                                                                                                           window.location.href = 'index.html#skyPriorityFinder';

                                                                                                           $('.main_header ul li:eq(1) img').off('click');
                                                                                                           });
                                                         /* End of Back functionality */


                                                         sessionStorage.currentPage="skyPriorityDetails";


                                                         this.$el.find('#'+sessionStorage.skyPriorityCarrier).prop('checked',true);
                                                         _.bindAll(this,"closeMembersPopup");

                                                         this.listenTo( airlineModel,'change', this.closeMembersPopup);
                                                         return appView.prototype.render.apply(this, arguments);

                                                         },
                                                         infoDetails: function(e) {
                                                         e.preventDefault();
                                                         e.stopPropagation();
                                                         /*
                                                          * Don't call  e.stopImmediatePropagation();
                                                          * 'infoDetails' & 'showDisclaimerMsg' function are called from same click
                                                          * so the event will call showDisclaimerMsg function to show/hide disclaimer msg
                                                          */
                                                         var curDiv;
                                                         if ((e.target.textContent === $.i18n.map.sky_moreinfo)) {
                                                         $(e.target.parentElement.parentElement).find('.sp_dep_content').addClass('noclass');
                                                         $(e.target.parentElement.parentElement).find('.sp_container_open').removeClass('noclass');
                                                         e.target.textContent = $.i18n.map.sky_lessinfo;
                                                         } else if ((e.target.textContent === $.i18n.map.sky_lessinfo)) {
                                                         e.target.textContent = $.i18n.map.sky_moreinfo;
                                                         $(e.target.parentElement.parentElement).find('.sp_container_open').addClass('noclass');
                                                         $(e.target.parentElement.parentElement).find('.sp_dep_content').removeClass('noclass');
                                                         } else {}
                                                         },
                                                         showDisclaimerMsg: function(e) {
                                                         e.preventDefault();
                                                         e.stopPropagation();
                                                         self.$el.find('.disclaimer_text').toggleClass('noclass');
                                                         },
                                                         openMembersPopup: function(e) {
                                                           $("#blackshade").show();
                                                           $('#blackshade').css({
                                                                        opacity : 0.6,
                                                                        'width' : $(document).width(),
                                                                        'height' : $(document).height()
                                                                        });
                                                   require(['view/skyPriority/subViews/skyPriorityAirlines'], function(skyPriorityAirlinesView) {

                                                           var skyPriorityAirlinesView = new skyPriorityAirlinesView({'model':airlineModel});
                                                           skyPriorityAirlinesView.render();});


                                                         },
                                                         closeMembersPopup: function(e) {
                                                         self.$el.find('#members-popup').hide();
                                                         self.$el.find('.members-ok').hide();
                                                         self.$el.find("#blackshade").hide();
                                                         var selectedCarrier = airlineModel.get('SelectedAirLineCode');


                                                         var spFinderBasicURL=URLS.SKYPRIORITY_BASIC+'triporigin='+sessionStorage.airport_code+'&operatingcarrier='+selectedCarrier;

                                                         var requestData = requestBuilder(spFinderBasicURL);
                                                         showActivityIndicatorForAjax('false');
                                                         callService(requestData,this.serviceCallBack);
                                                         },
                                                         serviceCallBack:function(data){
                                                         if(data.SkyPriorityEligibility === "Y" ){
                                                         localStorage.SPDeatils = JSON.stringify(data.TripOriginFeatures);
                                                         sessionStorage.skyPriorityCarrier = data.TripOriginFeatures.OperatingCarrier;
                                                         self.transition = false;
                                                         self.render();

                                                         }
                                                         else{
                                                         openAlertWithOk($.i18n.map.sky_priority_basic_no_benefits);
                                                         }

                                                         },
                                                         checkSelectedAirline: function(e)
                                                         {
                                                         var forid = '#' + $(e.target).attr('for');
                                                         self.$el.find(forid).prop("checked", true);
                                                         e.preventDefault();

                                                         }


                                                         });
       return skyPriorityDetailsView;

       });