/*  Weather Information  */
define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'text!template/menuTemplate.html', 'text!template/airports.html', 'text!template/weatherTemplateMVVM.html','models/weatherforecast/weatherForeCastModel','appView'], function($, _, Backbone, utilities, constants, menuPageTemplate, airport_details, weatherInfo, weatherForeCastModel,appView) {
       var self;
       var flightPageView = appView.extend({
               id : 'flight-page',
               initialize : function() {
                   $('#headingText img').css('display', 'none');
                   $('#headingText h2').css('display', 'block');
                   $('#headingText h2').html($.i18n.map.sky_weatherreport);
                   $('#brief h2').css('display', 'none');

                   if (sessionStorage.savedflights == "yes") {
                   $('#brief img').css('display', 'block');
                   $('#brief h2').css('display', 'none');
                   $(".menuItems").pageslide();
                   var template = _.template(menuPageTemplate,$.i18n.map);
                   $('#pageslide').append(template);
                   sessionStorage.savedflights = "no";
                   }
                   /*if(sessionStorage.weatherfrom == "AirportDetails"){
                   sessionStorage.savedflights = "yes";
                   }*/
                   if (sessionStorage.isFromSaved == "yes") {
                   $('#brief img').css('display', 'none');
                   $('#brief h2').css('display', 'none');
                   }
                   else{
                   sessionStorage.currentPage="weatherDetails";
                   }
                   self = this;
               },
               render : function() {
               sessionStorage.currentPage="weatherDetails";
               $('.main_header ul li:eq(1) img').on('click');
               $('.main_header ul li:eq(1) img').off('click').click(function(e) {
                                                                    var currentobj=JSON.parse(sessionStorage.previousPages);
                                                                    showActivityIndicatorForAjax('false');
                                                                    e.preventDefault();
                                                                    e.stopImmediatePropagation();
                                                                    window.location.href = "index.html#"+currentobj[0].previousPage;
                                                                    popPageOnToBackStack();

                                                                    sessionStorage.currentPage=currentobj[0].previousPage ;
                                                                    sessionStorage.airpor="yes";
                                                                    $('.main_header ul li:eq(1) img').off('click');
                                                                    });

               var obj=JSON.parse(sessionStorage.airportDetailsObj);
               var results = JSON.parse(sessionStorage.weatherDetailsResult);
               var dataModel = new weatherForeCastModel();
               dataModel.set({'AirportName':obj.airportName});

               if(!(results.hasOwnProperty('ErrorCode') || results.hasOwnProperty('ErrorMessage'))){
               dataModel.set({'WeatherDetails':results});
               dataModel.resetData();
               }
               else{
               dataModel.set(results);
               }
               this.model = dataModel;
               var compiledTemplate = _.template(weatherInfo,this.model.toJSON());
               $(this.el).html(compiledTemplate);
               hideActivityIndicator();
               return appView.prototype.render.apply(this, arguments);

               },
               events:{
               'touchstart *':function(){
               $.pageslide.close();
               }
               }
               });
       return flightPageView;
       });