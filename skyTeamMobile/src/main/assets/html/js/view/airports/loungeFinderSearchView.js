define(
		['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'tooltip', 'text!template/loungeFinderSearchTemplate.html', 'text!template/menuTemplate.html'],
		function($, _, Backbone, utilities, constants, tooltip, loungeFinderTemplate, menuPageTemplate) {
			var strKeyboardLang = "",
				loungeAirportCode;
			var loungeSearchView = Backbone.View.extend({
				el : '#skyteam',
				id : 'loungeFinderSearch-page',
				initialize : function() {
					$('#backButtonList').hide();
					$('#headingText h2').css('margin-right', '');
					if ( sessionStorage.previousPages != undefined ) {
						if( JSON.parse( sessionStorage.previousPages ).length === 0 ) {
							sessionStorage.removeItem('previousPages');
						}
					}
					hideActivityIndicator();
					sessionStorage.airportname = "";
					$('#headingText img').css('display', 'none');
					$('#headingText h2').css('display', 'block');
					$('#headingText h2').html( $.i18n.map.sky_lounge_finder );
					
					if ( sessionStorage.savedflights === "yes" ) {
						$('#brief img').css('display', 'block');
						$('#brief h2').css('display', 'none');
						$(".menuItems").pageslide();
						var template = _.template( menuPageTemplate, $.i18n.map );
						$('#pageslide').append( template );
						sessionStorage.savedflights = "no";
					}
					sessionStorage.currentPage = "loungesSearch";
					 if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
                          hideActivityIndicator();
                     }
                     else
                     {
                          window.getAirportsCallback = function() {
                                hideActivityIndicator();
                           }
                          showActivityIndicator($.i18n.map.sky_please);
                          window.open(iSC.getAirports);
                     }
				},
				
				render: function() {
					sessionStorage.skyTipsAirportName = "";
					sessionStorage.currentPage = "loungesSearch";
					var locationPath = window.location;
					locationPath = locationPath.toString();
					var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
					if ( spath === "loungesSearch" && sessionStorage.navToloungesFinderMenuView === "yes" ) {
						$(".menuItems").trigger('click');
						sessionStorage.navToloungesFinderMenuView = "no";
					}
					var compiledTemplate = _.template( loungeFinderTemplate, $.i18n.map );
					$(this.el).html( compiledTemplate );
					if ( sessionStorage.fromLoungesResult === "yes" ) {
						$('#airport_text').val( sessionStorage.airportName );
						$('.cancel_Skytips').css('visibility', 'visible');
						$('.search_skytips').css('visibility', 'hidden'); 
					}
					sessionStorage.fromLoungesResult = "no";
					if ( sessionStorage.isFromSaved === "yes" ) {
						if ( (sessionStorage.airportname == "undefined") ||  (sessionStorage.airportname == null) ) {
							$('#airport_text').val('');
						} else {
							$('#airport_text').val( sessionStorage.airportname );
						}
						sessionStorage.isFromSaved = "no";
					}
					if ( $('#airport_text').val() != "" ) {
						$(".cancel_Skytips").css("visibility", "visible");
						$('.search_skytips').css('visibility', 'hidden');
					} else {
						$('.cancel_Skytips').css('visibility', 'hidden');
						$('.search_skytips').css('visibility', 'visible');
					}slideThePage(sessionStorage.navDirection);
				},
				
				events:{
					'focusin :input' : 'changeHeight',
					'focusout :input' : 'retainHeight',
					'keydown #airport_text' : 'logkey',
					'click #searchLounges':'openLoungesSearchResults',
					'click .notes p span':'openReadMore',
					'focus :input' : 'showCancelImage',
					'click .cancel_Skytips' : 'crossImage_Lounges',
					'click .search_skytips' : 'createFocus_Lounges',
					'touchend input': 'hideLoungeBtn',
					'blur input': 'showLoungeBtn',
					'touchstart *': function(){
						$.pageslide.close();
					},
					'touchend *': function(){
						$(".loungeSearch").show();
					}
				},
			
				/* function to move up the view when keyboard appears */
				changeHeight : function(e) {
					$('.priority_img').css("margin-top", "-10%");
				},
				
				/* function to move down the view when keyboard dismisses */
				retainHeight : function(e) {
					$('.priority_img').css("margin-top", "0px");
				},
				
				/* function to hide the Find lounges button */
				hideLoungeBtn : function(){
					$(".loungeSearch").hide();
				},
				
				/* function to show the Find lounges button */
				showLoungeBtn: function(){
					$(".loungeSearch").show();
				},
				
				/* function to display auto complete view when user inputs characters */
				logkey : function(e) {
					$('#airport_text').keyup(function() {
						if ( $('#airport_text').val() != '' ) {
							$('.cancel_Skytips').css('visibility', 'visible');
							$('.search_skytips').css('visibility', 'hidden');
						} else {
							$('.cancel_Skytips').css('visibility', 'hidden');
							$('.search_skytips').css('visibility', 'visible');
						}
					});
					
					if ( e.keyCode === 8 ) {
						var keyval = $('#' + e.target.id).val();
					} else {
						keyval = keyval + String.fromCharCode(e.keyCode);
					}
					if ( keyval != "" && e.keyCode != 8 ) {
						sessionStorage.keyval = keyval;
						var minLenVal = 3,
							names = [];
						names = JSON.parse( localStorage.cityList );
						if ( localStorage.language === "zh" || localStorage.language === "Ja") {
							minLenVal = 1;
						}

						if ( names != null ) {
							toHighlightAutoComplete( true );
							$("#airport_text").autocomplete({
								source : names,
								minLength : minLenVal,
								open : function( event, ui ) {
									$(".ui-autocomplete").scrollTop(0);
									$('.loungeDetails').css('display', 'none');
									$('.searchResultNumber').css('display', 'none');
									$('.airportss').css('display', 'none');
								},
								select : function(event, ui) {
									loungeAirportCode = ui.item.airportCode;
									sessionStorage.airport_code = loungeAirportCode;
									$(this).blur();
								},
							});
						}
					} 
					if ( sessionStorage.deviceInfo === "android" ) {
						if ( e.keyCode === 13 ) {
							$(this).blur();
							this.openLoungesSearchResult();
						}
					}
				},
				
				/* function to open lounges results page */
				openLoungesSearchResults: function(e) {
					var loungeFlag = 0;
					var airportNam = $('#airport_text').val();
					sessionStorage.airportname = airportNam;
					if ( airportNam === null || airportNam === "" ) {
						openAlertWithOk( $.i18n.map.sky_enter );
					} else {
						var objSrc = validateAirportTextField(airportNam, loungeAirportCode);
						if(objSrc.isValid){
							loungeFlag = 1;
							loungeAirportCode = objSrc.Code;
						}

						if ( loungeFlag === 0 ) {
							$('#airport_text').val('');
							openAlertWithOk( $.i18n.map.sky_notexist );
							$(".cancel_Skytips").css("visibility", "hidden");
							$('.search_skytips').css('visibility', 'visible'); 
						} else if ( loungeFlag === 1) {

							var obj = {};
							obj.airportCode = loungeAirportCode;
							obj.airportName = sessionStorage.airportName;
							sessionStorage.airport_newName = getAirportFullName( obj.airportCode );

							obj.url = URLS.LOUNGE_SEARCH + 'airportcode=' + obj.airportCode;
							sessionStorage.airport_code = obj.airportCode;
							sessionStorage.loungesData = JSON.stringify( obj );
							obj.previousPage = "loungesSearch";
							pushPageOnToBackStack( obj );
							showActivityIndicator( $.i18n.map.sky_please );
							showActivityIndicatorForAjax('true');
							window.open( iSC.loungeFinder, "_self" );
						} else {}
					}
				},
				
				/* function to display the read more screen */
				openReadMore: function(e) {
					sessionStorage.airportname = $('#airport_text').val();
					var obj = {};
					obj.previousPage = "loungesSearch";
					obj.name = $('#airport_text').val();
					pushPageOnToBackStack( obj );
					window.location.href = "index.html#accessPolicy";
				},
				
				/* function to display clear button for the text field */
				showCancelImage : function(e) {

					if ( $('#airport_text').val() != '' ) {
						$('.cancel_Skytips').css('visibility', 'visible');
						$('.search_skytips').css('visibility', 'hidden');                                            
					} else {
						$('.cancel_Skytips').css('visibility', 'hidden');
						$('.search_skytips').css('visibility', 'visible');
					}
				},
				
				/* function to to clear the text field on click of clear button */
				crossImage_Lounges: function(e) {
					$("#airport_text").val("");
					$(".loungeSearch").hide();
					$(".cancel_Skytips").css("visibility", "hidden");
					$('.search_skytips').css('visibility', 'visible');
					$("#airport_text").focus();
				},
				
				/* function to focus the text field */
				createFocus_Lounges : function(e) {
					$("#airport_text").val("");
					$(".loungeSearch").hide();
					$(".cancel_Skytips").css("visibility", "hidden");
					$('.search_skytips').css('visibility', 'visible');
					$("#airport_text").focus();
				}
			});
			
			return loungeSearchView;

		});