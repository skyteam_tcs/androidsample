define(
		['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'tooltip', 'text!template/loungeFinderResultTemplate.html', 'text!template/menuTemplate.html'],
		function($, _, Backbone, utilities, constants, tooltip, loungeFinderResultTemplate, menuPageTemplate) {
			var loungeFinderResultView = Backbone.View.extend({
			 
				el : '#skyteam',
				id : 'loungeFinderResult-page',
			
				initialize : function() {

					$('#backButtonList').show();
					$('#headingText img').hide();
					$('#headingText h2').show();
					$('#headingText h2').html( $.i18n.map.sky_lounge );
					$('#headingText h2').css('margin-right','37px');
				
					if ( sessionStorage.lounges === "yes" ) {
						$('#brief img').show();
						$('#brief h2').hide();
						$(".menuItems").pageslide();
						var template = _.template( menuPageTemplate, $.i18n.map );
						$('#pageslide').append( template );
						sessionStorage.lounges = "no";
					}

					if( sessionStorage.savedflights === "yes" ) {
						hideActivityIndicator();
						$('#brief img').show();
						$('#brief h2').hide();
						$(".menuItems").pageslide();
						var template = _.template( menuPageTemplate, $.i18n.map );
						$('#pageslide').append( template );
						sessionStorage.savedflights = "no";
					}
				},
				
				render: function() {
					
					hideActivityIndicator();
					$('.main_header ul li:eq(1) img')
						.off('click')
						.on('click', function(e) {
							e.preventDefault();
							sessionStorage.fromLoungesResult = "yes";
		               sessionStorage.navDirection = "Left";

							var currentobj;
							currentobj = JSON.parse( sessionStorage.previousPages );
							if ( $('#pageslide').css('display') === "block" ) {
								$.pageslide.close();
							
							} else if ( currentobj[0].previousPage === "FlightDetailsPage" || currentobj[0].previousPage === "flightStatusDetails" ) {
								showActivityIndicatorForAjax('false');
								currentobj[0].previousPage = sessionStorage.lounge;
								window.location.href = "index.html#" + currentobj[0].previousPage;
							
							} else if ( sessionStorage.lounge === "airportDetails" ) {
								showActivityIndicatorForAjax('false');
								window.location.href = "index.html#" + currentobj[0].previousPage;
								popPageOnToBackStack();
							
							} else {
								showActivityIndicator( $.i18n.map.sky_please );
								currentobj = JSON.parse( sessionStorage.previousPages );
								window.location.href = "index.html#" + currentobj[0].previousPage;
								popPageOnToBackStack();
							}
							sessionStorage.savedflights = "no";
							if ( sessionStorage.currentPage === "loungeFinderResult" ) {
								sessionStorage.savedflights = "yes";
							}
						});

					sessionStorage.currentPage="loungeFinderResult";
					if ( localStorage.networkStatus != undefined ) {
						var netstat = JSON.parse( localStorage.networkStatus );
					}
					compiledTemplate = _.template( loungeFinderResultTemplate, $.i18n.map );
					$(this.el).html( compiledTemplate );
					sessionStorage.currentPage = "loungeFinderResult";
					
					if (localStorage.loungeFinderMenuDisplay != undefined) {
						if((localStorage.loungeFinderMenuDisplay != "undefined") && (localStorage.loungeFinderMenuDisplay != "")){
						
							$('#backButtonList').show();
							$('#headingText h2').html( $.i18n.map.sky_lounge );
							var loungesresponse = JSON.parse( localStorage.loungeFinderMenuDisplay );
							var arrLoungeData =[];
							if(loungesresponse.LoungeData.length != undefined) {
								arrLoungeData = loungesresponse.LoungeData.slice(0);
							}
							else {
								arrLoungeData = $.makeArray(loungesresponse.LoungeData);
							}
							var $loungeDetails = $('<div\>');
							$('.searchResultNumber').html( arrLoungeData.length + " " + $.i18n.map.sky_lounge );
							$('.airportss').html( getAirportName( sessionStorage.airport_code ) + " - " + sessionStorage.airport_newName + ", " + getCountryName( sessionStorage.airport_code ).split("_")[0] );

								for ( var i = 0, loungesLen = arrLoungeData.length; i < loungesLen; i++ ) {
									var open;
									if( netstat.network === "offline" ) {
										if( ( arrLoungeData[i].HoursOfOperation === " " ) || 
												( arrLoungeData[i].HoursOfOperation === "" ) ) {
											open = "";
										} else {
											open = "Open : " + arrLoungeData[i].HoursOfOperation;
										}
										
										if( ( arrLoungeData[i].AdditionalInformation === " " ) || 
												( arrLoungeData[i].AdditionalInformation === "" ) ) {
											additionalInfo = "";
										} else {
											additionalInfo = "Extra info:  " + arrLoungeData[i].AdditionalInformation;
										}
									} else {
										if ( arrLoungeData[i].hasOwnProperty('HoursOfOperation') && 
												( arrLoungeData[i].HoursOfOperation != "" ) && 
												( arrLoungeData[i].HoursOfOperation != null ) ) {
											open = "Open : " + arrLoungeData[i].HoursOfOperation;
										} else {
											open = "";
										}
										if ( arrLoungeData[i].hasOwnProperty('AdditionalInformation') &&
												( arrLoungeData[i].AdditionalInformation != "" ) &&
												( arrLoungeData[i].AdditionalInformation != null ) ) {
											additionalInfo = "Extra info:  " + arrLoungeData[i].AdditionalInformation;
										} else {
											additionalInfo = "";
										}
									}
									
									if ( arrLoungeData[i].FreeWiFi === "TRUE" &&
											arrLoungeData[i].PaidWiFi === "TRUE" ) {
										wifi = "TRUE";
										toolmsg = "Free, Paid WiFi";
									} else if (arrLoungeData[i].FreeWiFi === "TRUE" &&
											arrLoungeData[i].PaidWiFi == "FALSE" ) {
										wifi = "TRUE";
										toolmsg = "Free WiFi";
									} else if (arrLoungeData[i].FreeWiFi === "FALSE" &&
											arrLoungeData[i].PaidWiFi === "TRUE" ) {
										wifi = "TRUE";
										toolmsg = "Paid WiFi";
									} else {
										wifi = "FALSE";
										toolmsg = "";

									}

									$('<div\>', { class: 'loungeDetail' })
									.append($('<p\>', { class: 'loungeName'})
											.html( arrLoungeData[i].LoungeName ))
									.append($('<p\>', { class: 'loungeLocation'})
											.html( arrLoungeData[i].LoungeLocation ))
									.append($('<ul\>')
											.append($('<li\>', {
															class: 'tooltip',
															id: 'shower-' + (arrLoungeData[i].Shower),
															title: 'Shower' }))
											.append($('<li\>', {
															class: 'tooltip',
															id: 'accessDisabled-' + (arrLoungeData[i].AccessTodisabled),
															title: 'Access to disabled' }))
											.append($('<li\>', {
															class: 'tooltip',
															id: 'wifiFree-' + wifi,
															title: toolmsg }))
											.append($('<li\>', {
															class: 'tooltip',
															id: 'FixedInternetAcces-' + (arrLoungeData[i].FixedInternetAcces),
															title: 'Fixed Internet Acces' })))
									.append($('<p\>', { class: 'timeDetails'})
											.html( open ))
									.append($('<p\>', { class: 'details'})
											.html( additionalInfo ))
									.appendTo($loungeDetails);
								}
								$('.loungeDetails').append($loungeDetails);
						}
					}slideThePage(sessionStorage.navDirection);
				},
				
				events:{
					'click .notes p span':'openReadmore',
					'click .tooltip' : 'openTooltip',
					'touchstart *' : function() {
						$.pageslide.close();
					}
				},

				/* function to display lounge access policy screen */
				openReadmore: function(e) {
					var obj = {};
					obj.previousPage = "loungeFinderResult";
					pushPageOnToBackStack( obj );
					window.location.href = "index.html#accessPolicy";
				},
				
				/* function to show the tooltip  description */
				openTooltip : function(e) {
					$('.tooltip').tooltipster({
						trigger : 'click',
						timer : 350
					});
				},
			});
			
			return loungeFinderResultView;

		});
