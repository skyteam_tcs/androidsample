/*   Lounge Access Policy    */
define(['jquery', 'backbone', 'underscore', 'text!template/loungeAccessPolicy.html', 'text!template/menuTemplate.html','appView'],
       function($, Backbone, _,loungeAccessTemplate, menuPageTemplate,appView) {
       var loungesAccessView = appView.extend({
                                                    id : 'Access-page',
                                                    initialize : function() {
                                                    $('#backButtonList').show();
                                                    $('#brief h2').hide();
                                                    $('#headingText img').hide();
                                                    $('#headingText h2').show();
                                                    $('#brief img').hide();
                                                    $("#main_header_ul").hide(1,function(){
                                                                              $('#headingText h2').html($.i18n.map.sky_loungepolicy);
                                                                              $("#main_header_ul").show();
                                                                              
                                                                              });
                                                    
                                                    
                                                  
                                                    
                                                    },
                                                    
                                                    render : function() {
                                                    var previousPage=sessionStorage.currentPage;
                                                    
                                                   $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                                                                                            e.preventDefault();
                                                                                          e.stopImmediatePropagation();
                                                                                            var currentobj=JSON.parse(sessionStorage.previousPages);
                                                                                            if(sessionStorage.lounge=="airportDetails"){
                                                                                            
                                                                                            showActivityIndicator($.i18n.map.sky_please);
                                                                                            
                                                                                            window.location.href = "index.html#"+currentobj[0].previousPage;
                                                                                            popPageOnToBackStack();}
                                                                                            else if(currentobj[0].previousPage == "loungesSearch" || currentobj[0].previousPage == "airportSearchPage"){
                                                                                            if(currentobj[0].previousPage == "airportSearchPage"){
                                                                                            currentobj[0].previousPage = "loungesSearch"
                                                                                            sessionStorage.fromLoungesResult="no";
                                                                                                     
                                                                                            }
                                                                                            else{
                                                                                            sessionStorage.fromLoungesResult="yes";}
sessionStorage.navToloungesFinderMenuView = "yes";
                                                                                            window.location.href="index.html#loungesSearch";
                                                                                           sessionStorage.removeItem('previousPages');
                                                                                            }
                                                                                            
                                                            
                                                                                            else{
                                                                                            e.preventDefault();
                                                                                            e.stopImmediatePropagation();
                                                                                            
                                                                                            showActivityIndicator($.i18n.map.sky_please);
                                                                                            window.location.href = "index.html#"+currentobj[0].previousPage;
                                                                                            popPageOnToBackStack();
                                                                                            }
                                                                                            if(sessionStorage.lounge=="airportDetails"){
                                                                                            $('.main_header ul li:eq(1) img').remove();
                                                                                            $('.main_header ul li:eq(1)').append($('<img></img>').attr('src', './images/back.png'));
                                                                                            }
                                                                                            else{
                                                                                            }
                                                                                            if (sessionStorage.isFromSaved == "yes") {
                                                                                            $('#brief img').css('display', 'none');
                                                                                            }
                                                                                            else{
                                                                                            $('#brief img').css('display', 'block');
                                                                                            }
                                                                                            $('.main_header ul li:eq(1) img').off('click');
                                                                                    });
                                                    
                                                    
                                                    if(sessionStorage.From == "searchResults"){
                                                    sessionStorage.From = "searchResults";
                                                    }
                                                    else{
                                                    sessionStorage.From = "";
                                                    }
                                                    
                                                    var compiledTemplate = _.template(loungeAccessTemplate,$.i18n.map);
                                                    $(this.el).html(compiledTemplate);
                                                    sessionStorage.currentPage="loungeAccessPolicy";
                                                    return appView.prototype.render.apply(this, arguments);

                                                    },
                                                    events:{
                                                    
                                                    'touchstart *' : function() {
                                                    $.pageslide.close();
                                                    }
                                                    },
                                                    });
       return loungesAccessView;
       });