define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'tooltip','models/lounges/loungeSearchModel', 'text!template/loungeFinderSearchTemplateMVVM.html', 'text!template/menuTemplate.html','appView'], function($, _, Backbone, utilities, constants, tooltip, loungeSearchModel,loungeFinderTemplate, menuPageTemplate,appView) {
       var AirportNames,self;
       var strKeyboardLang="";
       var loungeSearchView = appView.extend({                                                   
                                                   id : 'loungeFinderSearch-page',
                                                   initialize : function()
                                                   {
                                                   $('#backButtonList').hide();
                                                   $('#headingText h2').css('margin-right','');
                                                   $('#brief').show();
                                                   $('#brief img').show();//myskyteam icon
                                                   $('#brief h2').hide();
                                                   if(sessionStorage.previousPages != undefined){
                                                   if(JSON.parse(sessionStorage.previousPages).length != 0){
                                                   sessionStorage.removeItem('previousPages');
                                                   }
                                                   }
                                                   hideActivityIndicator();
                                                   $('#headingText img').hide();
                                                   $('#headingText h2').show();
                                                   $('#headingText h2').html($.i18n.map.sky_lounge_finder);
                                                   self = this;
                                                   /* checking city list availability */
                                                   if (localStorage.cityList != 'undefined' && localStorage.cityList != null) {
                                                       hideActivityIndicator();
                                                   }
                                                   else
                                                   {
                                                       window.getAirportsCallback = function() {
                                                           hideActivityIndicator();
                                                       };
                                                       window.open(iSC.getAirports);
                                                   }
                                                    /* checking city list availability */
                                                   
                                                   this.model = new loungeSearchModel();
                                                   this.model.set($.i18n.map);
                                                   //while back from lounge access policy || lounge results || my skyteam
                                                   if("loungeAccessPolicy" === sessionStorage.currentPage || "loungeFinderResult" === sessionStorage.currentPage || sessionStorage.previousPage == "savedFlightsOrAirports"){
                                                    this.model.set({'AirportName':sessionStorage.airportName});
                                                    this.model.set({'AirportCode':sessionStorage.airport_code});
                                                    sessionStorage.previousPage = '';
                                                   }
                                                   
                                                   /* checking and adding menupage */
                                                   if($('#pageslide').children().length === 0)
                                                   {
                                                   $(".menuItems").pageslide();
                                                   var template = _.template(menuPageTemplate,$.i18n.map);
                                                   $('#pageslide').html(template);
                                                   sessionStorage.isMenuAdded = "YES";
                                                   }
                                                    /* checking and adding menupage */
                                                   },
                                                   render:function(){

                                                   //********************************************************************************


                                                                                                if(latLonAirportName.length){

                                                                                                autocomplete_autofill_inlanguagechange();
                                                                                                self.$el.find('#airport_text').val(conte);

                                                                                                self.model.set({'AirportCode':latLonAirportCode});
                                                                                                sessionStorage.airport_code = latLonAirportCode;
                                                                                                self.model.set({'AirportName':latLonAirportName});
                                                                                                sessionStorage.airportName =getAirportFullName(latLonAirportCode);
                                                                                                }

                                                    //***********************************************************************************


                                                   var locationPath = window.location;
                                                   locationPath = locationPath.toString();
                                                   
                                                   var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
                                                   if (spath == "loungesSearch" && sessionStorage.navToloungesFinderMenuView == "yes") {
                                                   $.pageslide.close();
                                                   sessionStorage.navToloungesFinderMenuView = "no";
                                                   }
                                                   
                                                   var compiledTemplate = _.template(loungeFinderTemplate,this.model.toJSON());
                                                   $(this.el).html(compiledTemplate);
                                                   
                                                   
                                                   if(this.model.has('AirportCode') && this.model.get('AirportCode').length === 3){
                                                   var airportCode = this.model.get('AirportCode');
                                                   var airportText =getAirportFullName(airportCode)+" ("+airportCode+"), "+getAirportCountryName(airportCode);
                                                   this.$el.find('#airport_text').val(airportText);
                                                   this.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                   this.$el.find('.search_skytips').css('visibility', 'hidden');
                                                   }
                                                   
                                                   this.listenTo( this.model,'change', this.dataChange);
                                                   sessionStorage.currentPage = "loungesSearch";
                                                   return appView.prototype.render.apply(this, arguments);
                                                   
                                                   },
                                                   events:{
                                                   
                                                   'keydown :input' : 'logkey',
                                                   'click #searchLounges':'openLoungesSearchResults',
                                                   'click .notes p span':'openReadMore',
                                                   'focus :input' : 'showCancelImage',
                                                   'click .cancel_Skytips' : 'crossImage_Lounges',
                                                   'touchend input': 'hideLoungeBtn',
                                                   'blur input': 'showLoungeBtn',
                                                   'click .search_skytips' : 'createFocus_Lounges',
                                                   'touchstart *':function(){
                                                   $.pageslide.close();
                                                   },
                                                   'touchend *':function(){
                                                   self.$el.find(".loungeSearch").show();
                                                   }
                                                   },
                                                   hideLoungeBtn :function(){
                                                   self.$el.find(".loungeSearch").hide();
                                                   },
                                                   showLoungeBtn: function(){
                                                   self.$el.find(".loungeSearch").show();
                                                   var inputField = self.$el.find('#airport_text');
                                                   
                                                   if( !$(inputField).hasClass('has-error')){
                                                   var airportText = inputField.val().toLowerCase();
                                                   var modelAirportValue = self.model.get("AirportName").toLowerCase();
                                                   if((!hasValue(airportText)) || modelAirportValue.indexOf(airportText) < 0){
                                                   $(inputField).addClass('has-error');
                                                   }
                                                   }
                                                   //when selecting no results found
                                                   if(!(hasValue(inputField.val()) && hasValue(self.model.get('AirportCode'))) ){
                                                   self.$el.find('.cancel_Skytips').css('visibility', 'hidden');
                                                   self.$el.find('.search_skytips').css('visibility', 'visible');
                                                   }
                                                   
                                                   
                                                   
                                                   },
                                                   logkey : function(e) {
                                                   self.$el.find('#airport_text').keyup(function() {
                                                                            if (self.$el.find('#airport_text').val() != ''){
                                                                            self.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                                            self.$el.find('.search_skytips').css('visibility', 'hidden');
                                                                            }
                                                                            else{
                                                                            self.$el.find('.cancel_Skytips').css('visibility', 'hidden');
                                                                            self.$el.find('.search_skytips').css('visibility', 'visible');
                                                                            }
                                                                            });
                                            if (e.keyCode == 8) {
                                                   var keyval = $('#' + e.target.id).val();
                                                   if( $('#' + e.target.id).val().length == 2)
                                                   {
                                                   var regex = /^([a-zA-Z])*$/;
                                                   if(regex.test($('#' + e.target.id).val())){
                                                   strKeyboardLang="En";
                                                   sessionStorage.keyboardLang="En";
                                                   }
                                                   else
                                                   {
                                                   strKeyboardLang=localStorage.language;
                                                   sessionStorage.keyboardLang=localStorage.language;
                                                   }
                                                   
                                                   }
                                            } else {
                                                   keyval = keyval + String.fromCharCode(e.keyCode);
                                            }
                                                   if (keyval != "" && e.keyCode != 8) {
                                                   //sessionStorage.keyval = keyval;
                                                   
                                                   var minLenVal=3;
                                                   
                                                   var   names=[];
                                                   names=JSON.parse(localStorage.cityList);
                                                   if(checkCJKCompliedLanguage())
                                                   {
                                                   
                                                   minLenVal=1;
                                                   
                                                   }
                                                   


                                                   
                                                   if (names != null) {
                                                   toHighlightAutoComplete(true);
                                                   self.$el.find("#airport_text").autocomplete({
                                                                                   source : names,
                                                                                   minLength : minLenVal,
                                                                                   open : function(event, ui) {
                                                                                   self.$el.find(".ui-autocomplete").scrollTop(0);
                                                                                               self.$el.find('.loungeDetails').hide();
                                                                                  
                                                                                   },
                                                                                   select : function(event, ui) {
                                                                                               self.model.set({'AirportCode':ui.item.airportCode});
                                                                                               sessionStorage.airport_code = ui.item.airportCode;
                                                                                               self.model.set({'AirportName':ui.item.value});
                                                                                               sessionStorage.airportName =getAirportFullName(ui.item.airportCode);
                                                                                               
                                                                                   $(this).blur();
                                                                                   },
                                                                                   });
                                                   
                                                   }
                                                   }
                                                   
                                                   if (sessionStorage.deviceInfo == "android") {
                                                   if(e.keyCode == 13){
                                                   $(this).blur();
                                                   this.openLoungesSearchResult();
                                                   }
                                                   }
                                                   },
                                                   dataChange:function(model,options){
                                                   if(model.hasChanged('AirportCode')){
                                                   var inputedValue = model.get("AirportCode");
                                                   if(hasValue(inputedValue) && inputedValue.length === 3){
                                                   self.$el.find('#airport_text').removeClass("has-error");
                                                    sessionStorage.airport_code = inputedValue;
                                                   }
                                                   else{
                                                   self.$el.find('#airport_text').addClass("has-error");
                                                   }
                                                   }
                                                   
                                                   
                                                   if(model.hasChanged('AirportName')){
                                                   
                                                   var inputedValue = model.get("AirportName");
                                                   if(hasValue(inputedValue)){
                                                   self.$el.find('.search_skytips').css('visibility','hidden');
                                                   self.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                   var validateCheck = validateAirportTextField(inputedValue,model.get("AirportCode"));
                                                   if(validateCheck.isValid){
                                                   self.$el.find('#airport_text').removeClass("has-error");
                                                   }
                                                   else{
                                                   self.$el.find('#airport_text').addClass("has-error");
                                                   }
                                                   
                                                   }
                                                   else{
                                                   self.$el.find('.search_skytips').css('visibility','visible');
                                                   self.$el.find('.cancel_Skytips').css('visibility', 'hidden');
                                                   self.$el.find('#airport_text').addClass("has-error");
                                                   }
                                                   }
                                                   },
                                                   openLoungesSearchResults:function(e){
                                                   if(hasValue(self.$el.find('#airport_text').val())){
                                                       if(hasValue(self.model.get("AirportName")) && hasValue(self.model.get("AirportCode"))){

                                                           var obj = {};

                                                           obj.url = URLS.LOUNGE_SEARCH+'airportcode=' + self.model.get("AirportCode");
                                                           obj.airportCode = self.model.get("AirportCode");
                                                           sessionStorage.loungesData = JSON.stringify(obj);
                                                           obj.previousPage="loungesSearch";
                                                           pushPageOnToBackStack(obj);
                                                           sessionStorage.airportCode = self.model.get("AirportCode");
                                                           showActivityIndicatorForAjax('true');
                                                           window.open(iSC.loungeFinder, "_self");
                                                       }
                                                       else{
                                                       openAlertWithOk($.i18n.map.sky_notexist);
                                                       self.$el.find('#airport_text').val('');
                                                       self.model.set({"AirportCode":''});
                                                       self.model.set({"AirportName":''});
                                                       }
                                                   }
                                                   else{
                                                   openAlertWithOk($.i18n.map.sky_enter);
                                                   self.$el.find('#airport_text').addClass('has-error');
                                                   }
                                                   },
                                                   
                                                   //lounge access policy
                                                   openReadMore:function(e){
                                                   var obj={};
                                                   obj.previousPage="loungesSearch";
                                                   
                                                   pushPageOnToBackStack(obj);
                                                   window.location.href="index.html#accessPolicy";
                                                   },
                                                   
                                                   //when input has some value
                                                   showCancelImage : function(e){
                                                   
                                                   if (self.$el.find('#airport_text').val() != ''){
                                                   self.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                   self.$el.find('.search_skytips').css('visibility', 'hidden');
                                                   }
                                                   else{
                                                   self.$el.find('.cancel_Skytips').css('visibility', 'hidden');
                                                   self.$el.find('.search_skytips').css('visibility', 'visible');
                                                   }
                                                   },
                                                   
                                                   //when clicking the cross image
                                                   crossImage_Lounges: function(e){
                                                   self.$el.find("#airport_text").val("");
                                                   self.$el.find(".loungeSearch").hide();
                                                   self.$el.find(".cancel_Skytips").css("visibility","hidden");
                                                   self.$el.find('.search_skytips').css('visibility', 'visible');
                                                   self.model.set({'AirportCode':''});
                                                   sessionStorage.airport_code = '';
                                                   self.model.set({'AirportName':''});
                                                   sessionStorage.airportName ='';
                                                   self.$el.find("#airport_text").focus();
                                                   },
                                                   
                                                   // when clicking the search icon
                                                   createFocus_Lounges : function(e){
                                                   self.$el.find("#airport_text").val("");
                                                   self.$el.find(".loungeSearch").hide();
                                                   self.$el.find(".cancel_Skytips").css("visibility","hidden");
                                                   self.$el.find('.search_skytips').css('visibility', 'visible');
                                                   self.$el.find("#airport_text").focus();
                                                   }
                                                   });
       return loungeSearchView;
       });