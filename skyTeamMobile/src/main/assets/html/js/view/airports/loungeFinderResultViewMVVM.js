define(['jquery', 'underscore', 'backbone', 'utilities', 'constants', 'tooltip','models/lounges/loungeResultModel', 'text!template/loungeFinderResultTemplateMVVM.html', 'text!template/menuTemplate.html','appView'], function($, _, Backbone, utilities, constants, tooltip, loungeResultModel, loungeFinderResultTemplateMVVM, menuPageTemplate,appView) {
       var loungeFinderResultView = appView.extend({
                                                         id : 'loungeFinderResult-page',
                                                         initialize : function()
                                                         {
                                                         $('#backButtonList').show();
                                                         $('#headingText img').hide();
                                                         $('#headingText h2').show();
                                                         $('#headingText h2').html($.i18n.map.sky_lounge);
                                                         if (sessionStorage.isFromSaved === "yes") {
                                                         $('#brief img').hide();
                                                         $('#brief h2').hide();
                                                         }
                                                         else
                                                         {
                                                         $('#brief img').show();
                                                         $('#brief h2').hide();
                                                         }
                                                         
                                                         //$(".menuItems").pageslide();
                                                         var template = _.template(menuPageTemplate,$.i18n.map);
                                                         $('#pageslide').append(template);
                                                         
                                                         
                                                         },
                                                         render:function(){
                                                         hideActivityIndicator();
                                          $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
                                                                                            e.preventDefault();
                                                                                            sessionStorage.fromLoungesResult="yes";
                                                                                                var currentobj;
                                                                                                 currentobj =JSON.parse(sessionStorage.previousPages);
                                                                                            if(currentobj[0].previousPage == "airportDetails" ||  currentobj[0].previousPage == "airportDetailsViewViaMap"  ){
                                                                                            showActivityIndicatorForAjax('false');
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                               showActivityIndicator($.i18n.map.sky_please);
                                                                                            }
                                                                                            
                                                                                             if(currentobj[0].previousPage == "airportDetails" ){
                                                                                              currentobj[0].previousPage =sessionStorage.lounge;
                                                                                              window.location.href = "index.html#"+currentobj[0].previousPage;
                                                                                              popPageOnToBackStack();
                                                                                             }else{
                                                                                              window.location.href = "index.html#"+currentobj[0].previousPage;
                                                                                             }
                                                                                          
                                                                                             sessionStorage.savedflights = "no";
                                                                                            if (sessionStorage.currentPage == "loungeFinderResult")
                                                                                             sessionStorage.savedflights = "yes";
                                                                                             });
                                                         
                                                         
                                                         this.model = new loungeResultModel($.i18n.map);
                                                                                     var response = JSON.parse(localStorage.loungeFinderMenuDisplay);
                                                         
                                                         //if louge data(one lounge) is not in  array converting it to array
                                                         if(!_.isArray(response.LoungeData)){
                                                         response.LoungeData = $.makeArray(response.LoungeData);
                                                         }
                                                         
                                                         this.model.set(response);
                                                         var compiledTemplate = _.template(loungeFinderResultTemplateMVVM,this.model.toJSON());
                                                         $(this.el).html(compiledTemplate);
                                                         
                                                         
                                                         
                                                         
                                                         
                                                         sessionStorage.currentPage="loungeFinderResult";
                                                         
                                                         return appView.prototype.render.apply(this, arguments);

                                                         },
                                                         events:{
                                                         'click .notes p span':'openReadmore',
                                                         'click .tooltip' : 'openTooltip',
                                                         'touchstart *' : function() {
                                                         $.pageslide.close();
                                                         }
                                                         },
                                                         openReadmore:function(e){
                                                         var obj={};
                                                         obj.previousPage="loungeFinderResult";
                                                         pushPageOnToBackStack(obj);
                                                         window.location.href="index.html#accessPolicy";
                                                         },
                                                         openTooltip : function(e) {
                                                         $('.tooltip').tooltipster({
                                                                                   trigger : 'click',
                                                                                   timer : 350
                                                                                   });
                                                         },
                                                         });
       return loungeFinderResultView;
       
       });