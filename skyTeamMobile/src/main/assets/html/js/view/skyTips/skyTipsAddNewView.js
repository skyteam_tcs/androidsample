define(
		['jquery', 'backbone', 'underscore','text!template/skyTipsAddNewTemplate.html', 'text!template/skyTipsTemplate.html'],
		function($, Backbone, _, skytipsAddNewTemplate, skytipsTemplate) {
			var that, skyTipsResponse, AirportNames,currentView,
			filterTipsResponse = [], 
			tipsFlag = 0,
			keyval = "",
			uiValue = "",
			isCloseBtnClicked = false,
			strKeyboardLang = "",
			tipAirportCode,
			tipAirportName;

			var skytipsAddNewView = Backbone.View.extend({

				el : '#skyteam',
				id : 'addSkyTipsPage-page',

				initialize : function() {
					$('#backButtonList').hide();  //Hide back button
					$('#headingText h2').css('margin-right','');
					that = this;
					if ( sessionStorage.previousPages != undefined ) {
						if ( JSON.parse( sessionStorage.previousPages ).length == 0 ){
							sessionStorage.removeItem('previousPages');
						}
					}
					$('#headingText img').hide();
					$('#headingText h2').show();
					$('#headingText h2').html( $.i18n.map.sky_tips );

					if ( sessionStorage.isFromSaved === "yes" ) {
						$('#brief img').hide();
						$('#brief h2').hide();
					} else {
						$('#brief img').show();
						$('#brief h2').hide();
					}
					currentView = this;
				},

				render : function() {
					var locationPath = window.location;
					locationPath = locationPath.toString();
					var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
					if  (spath === "addSkyTipsPage" && sessionStorage.navToaddSkyTipsPage === "yes") {
						sessionStorage.navToaddSkyTipsPage = "no";
					}
					var compiledTemplate = _.template( skytipsAddNewTemplate, $.i18n.map );
					$(this.el).html( compiledTemplate );

					if (sessionStorage.currentPage === "skytipsTerms"){
						/* 583152 -filling data when back from terms & condition */
						// auto filling previous filled details
						this.loadFilledInputs();
						/* 583152 -filling data when back from terms & condition */
					}

					if ( $('#airport_text').val() != "" ){
						$(".cancel_skytips").css("visibility", "visible");
						$('.skytips_searchImg').css('visibility', 'hidden');
					} else {
						$('.cancel_skytips').css('visibility', 'hidden');
						$('.skytips_searchImg').css('visibility', 'visible');
					}

					$('.cancel_skytips_btn').css('visibility', 'hidden');
					$('#tipThemesSelect').css('display', 'none');
					$('.themes-ok').css('display', 'none');
					$("#blackshade").css('display', 'none');
					$(".preview-popUp").css('display', 'none');
					sessionStorage.fromSkytipsResults = "no";
					sessionStorage.currentPage = "addSkyTipsPage";
					slideThePage(sessionStorage.navDirection);

				},

				events : {
					'focusin :input#airport_text' : 'changeHeight',
					'focus :input' : 'showCancelImage',
					'focusout :input#airport_text' : 'retainHeight',
					'keydown :input' : 'showCancelImage',
					'keydown #sky_desc' : 'removeBorder',
					'keydown :input#airport_text' : 'showAirportNames',
					'click #previewSkytip' : 'openPreview',
					'click .cancel_skytips_btn' : 'crossImage_Skytips',
					'mousedown .cancel_skytips' : 'crossImage_Skytips',
					'click .skytips_searchImg' : 'createFocus_Skytips',
					'click .btn_edit':'closePreview',
					'click .btn_post':'postSkyTips',
					'click #findSkytips' : 'openFindSkytips',
					'click #ThemeSelect':'openThemes',
					'click #terms':'openTermsAndCondition',
					'click .themes-ok' : 'closeThemesPopup',
					'click .themeName' : 'checkSelectedTheme',
					'click .spanCheckBox' : 'checkOrUncheckCheckBox',
					'touchstart *':function(){
						$.pageslide.close();
					},
					'touchend *':function(){
						$(".searchDiv").show();
					}
				},

				/* function to remove the border of description textview */
				removeBorder: function(){
					$('#skytip_desc').css("border-color", "#d7d7d7");
				},
				
				/* function to check the check box (theme) when the corresponding label is tapped */
				checkSelectedTheme: function(e){
					var forid = $(e.target).attr('for');
					$('#' + forid).prop("checked", true);
					e.preventDefault();
				},

				/* function to move the view down on keyboard dismissal */
				retainHeight : function(e) {
					$(".skytipContent").scrollTop(0);
					$('.skytipContent').css('margin-top', '0px');
					if ( sessionStorage.deviceInfo == "android" ){
						if ( ( screen.width === 480 ) && ( screen.height === 800 ) ) {
							$(".skytipContent").css('margin-top', '0px');
						}
					}
					if ( isCloseBtnClicked )
					{
						isCloseBtnClicked = false;
						$('#'+e.target.id).focus();
					}
				},

				/* function to move the view up on keyboard dismissal */
				changeHeight : function(e){
					var osVersion;
					if ( sessionStorage.deviceInfo === "iPhone" ){
						if ( ( screen.width === 320 ) && ( screen.height === 480 ) ) {
							osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
							osVersion = osVersion[0];
							osVersion = osVersion.replace(/_/g, '.');
							osVersion = osVersion.replace('OS ', '');
							if((osVersion.split('.')[0]) == "7"){
								$('.skytipContent').css('margin-top', sessionStorage.scrollHeight);
							}
						}
					} else if ( sessionStorage.deviceInfo === "android" ){
						if ( ( screen.width === 480 ) && ( screen.height === 800 ) ) {
							$(".skytipContent").css('margin-top', '-70px');
						}
					} else {}
				},                                          

				/* function to display theme selection pop up */
				openThemes:function(e){
					if ( sessionStorage.tipsFlag === 0 ) {
						for ( var i = 0, maxlen = skyTipsResponse.tipTheme.length; i < maxlen; i++ ) {
									if ( skyTipsResponse.tipTheme[i] != "") {
										$('.scrollforThemes')
												.append( $('<label></label>')
														.attr('class', 'radiobutton')
														.append( $('<input type="radio">')
																.attr('class', 'groupCheckbox')
																.attr('value', skyTipsResponse.tipTheme[i])
																.attr('name', 'operatorsGroup'))
														.append( $('<span></span>')))
												.append( $('<h6></h6>')
														.html( skyTipsResponse.tipTheme[i] ));
									}
						}
						sessionStorage.tipsFlag = 1;
					}
					$('.themes-ok').css('display', 'block');
					$('#tipThemesSelect').css('display', 'block');
					$("#blackshade").css('display', 'block');
					$("#addskytipsPage").css("display", "none");
				},

				/* function to display preview pop up */
				openPreview:function(e){
					if( this.validateInput() ){
						var curDate = new Date(),
							months = [$.i18n.map.sky_jan, $.i18n.map.sky_feb, $.i18n.map.sky_mar, $.i18n.map.sky_apr, $.i18n.map.sky_may, $.i18n.map.sky_jun, $.i18n.map.sky_jul, $.i18n.map.sky_aug, $.i18n.map.sky_sep, $.i18n.map.sky_oct, $.i18n.map.sky_nov, $.i18n.map.sky_dec],
							postDate;

						if ( localStorage.date === "YYYY" ){
							postDate = curDate.yyyymmdd();
						} else if( localStorage.date === "DD" ){
							postDate = curDate.getDate() + " " + months[curDate.getMonth()] + " " + curDate.getFullYear();
						} else {
							postDate = months[curDate.getMonth()] + curDate.getDate() + " " + curDate.getFullYear();
						}
						var imgpath = getImagePathForTheme( $(".themeSelectVal").html().trim() );

						$('.themeImgPreview').attr('src', imgpath);
						$(".previewMessage").html( $('#skytip_desc').val() );
						$(".username").html( $('#skytip_fname').val() + ' ' + $('#skytip_lname').val() );
						$(".sub_heading").html( $('#skytip_tite').val() );
						$(".userlocation").html( $('#skytip_country').val() );
						$(".postDate").html( postDate );
						$("#blackshade").css('display', 'block');
						$(".preview-popUp").css('display', 'block');
						$(".popUpMessage").css('display', 'block');
						$('#addskytipsPage').css('display', 'none'); 
					} 
				},

				/* function to dismiss preview pop up */
				closePreview:function(e){
					$("#blackshade").css('display', 'none');
					$(".preview-popUp").css('display', 'none');
					$('#addskytipsPage').css('display', 'block');
				},

				/* function to post the skytips */
				postSkyTips:function(e){
					$(".preview-popUp").css('display', 'none');
					$("#blackshade").css('display', 'none'); 
					showActivityIndicatorForAjax('false');
					var filterIndex, airName, airCode;
					filterIndex=$('#airport_text').val().indexOf("(");
					airName = tipAirportName;
					airCode = tipAirportCode;

					/* skyTips TipTheme and ID mapping */
					var tipTheme=$(".themeSelectVal").html();
					var tipThemeID;

					switch (tipTheme) {
							case $.i18n.map.sky_fast_track:
								tipThemeID = 1;// 1
								break;
							case $.i18n.map.sky_food_drink:
								tipThemeID = 3;// 3
								break;
							case $.i18n.map.sky_relaxation:
								tipThemeID = 4;// 4
								break;
							case $.i18n.map.sky_transportation:
								tipThemeID = 5;// 5
								break;
							case $.i18n.map.sky_leisure:
								tipThemeID = 6;// 6
								break;
							case $.i18n.map.sky_e_connectivity:
								tipThemeID = 7;// 7
								break;
							case $.i18n.map.sky_shopping:
								tipThemeID = 8;// 8
								break;
							case $.i18n.map.sky_other:
								tipThemeID = 9;// 9
								break;
							case $.i18n.map.sky_lounge:
								tipThemeID = 18;//18
								break;
							default:
								console.error("Unknown SkyTips Theme Name:" + tipTheme + " in AddSkyTips");
					}

					var now = new Date(),
						nowHour = now.getHours(),
						nowMin = now.getMinutes();

					//converting hour into 2 digit,  if its in 1 digit
					nowHour = nowHour < 10 ? ("0" + nowHour) : ( nowHour );

					//converting Minutes into 2 digit,  if its in 1 digit
					nowMin = nowMin > 9 ? ( nowMin ) : ("0"+nowMin);

					var created_at = $.datepicker.formatDate('dd/mm/yy', new Date());
					var formData = {"TipAirportName":airName,"TipAirportCode":airCode,"TipTheme": $(".themeSelectVal").html(),"TipTitle": $("#skytip_tite").val(),"TipDescription": $("#skytip_desc").val(),"FirstName":$("#skytip_fname").val(),"LastName":$("#skytip_lname").val(),"Email":$("#skytip_email").val(),"Country":$("#skytip_country").val(),"TipThemeID":tipThemeID,"TipCreatedAt":created_at+" "+nowHour+":"+nowMin};
					var acceptLanguage = ( localStorage.language != "zh" ) ? localStorage.language : "zh-Hans";
					var postUrl = URLS.SKYTIPS_ADD;

					xhrajax=$.ajax({
						url : postUrl,
						type: 'POST',
						headers:{'api_key': API_KEY, 'Accept-Language': acceptLanguage, 'source': 'SkyApp'},
						data : JSON.stringify( formData ),
						contentType: "application/json; charset=utf-8",
						success: function( data, textStatus, jqXHR )
						{
							//data - response from server     
							hideActivityIndicator();
							openAlertWithOk( $.i18n.map.sky_tip_confm_msg, 'skytips' ); 
						},
						error: function( jqXHR, textStatus, errorThrown )
						{
							hideActivityIndicator();
							if( textStatus != 'abort' ){
								openAlertWithOk( $.i18n.map.sky_tips_post_error );
							}
						}
					});
				},

				/* function to dismiss theme selection pop up */
				closeThemesPopup: function(e) {
					$('#tipThemesSelect').css('display', 'none');
					$('.themes-ok').css('display', 'none');
					$("#blackshade").css('display', 'none');
					if( $('.scrollforThemes :checked').val() != "" && $('.scrollforThemes :checked').val() != undefined ){
						$('#ThemeSelect p').html('');
						$('#ThemeSelect p').html( $('.scrollforThemes :checked').val() );
						$('#ThemeSelect').css("border-color", "#d7d7d7");
						$('.tips_msgs').empty();

						var themeName = $('.scrollforThemes :checked').val();
						sessionStorage.theme = "yes";
						filterTipsResponse = [];
						sessionStorage.themeSelected = themeName;
					}
				},

				/* function to switch to find skytips page */
				openFindSkytips : function(e) {
		            sessionStorage.transRequired = "No";
					window.location.href = 'index.html#skyTipsFinder';
				},

				/* function to focus the airport text field */
				createFocus_Skytips : function(e) {
					$("#airport_text").val("");
					$("#airport_text .cancel_skytips").css("visibility", "hidden");
					$('.skytips_searchImg').css('visibility', 'visible');                                        
					$("#airport_text").focus();
				},

				/* function to clear corresponding text fields on the click of clear button */
				crossImage_Skytips: function(e) {
					var targetId = e.target.id;

					if( targetId === 'btn_airport_text' ){
						$("#airport_text").val("");
						$(".searchDiv").hide();
						$("#airport_text .cancel_skytips").css("visibility", "hidden");
						$('.skytips_searchImg').css('visibility', 'visible');                                        
						isCloseBtnClicked=true;
						$("#airport_text").focus();
					} else {
						$('#'+targetId.replace('btn_', '')).val('');
						$('#cancel_'+targetId.replace('btn_', '')).css("visibility", "hidden");
						$('#'+targetId.replace('btn_', '')).focus();
					}
				},

				/* function to display clear button in the text fields */
				showCancelImage : function(e) {
					var targetId=e.target.id;

					$('#'+targetId).keyup(function() {
						if( $('#'+targetId).val() != '' ){
							$('#cancel_'+targetId).css("visibility", "visible");
							$('#btn_'+targetId).css("visibility", "visible");
							$('#'+targetId).css("border-color", "#d7d7d7");
						} else {
							$('#cancel_'+targetId).css("visibility", "hidden");
						}
					});
					if( targetId === 'airport_text' ){
						if ( $('#airport_text').val() != ''){
							$('.cancel_skytips').css('visibility', 'visible');
							$('.skytips_searchImg').css('visibility', 'hidden');
						} else {
							$('.cancel_skytips').css('visibility', 'hidden');
							$('.skytips_searchImg').css('visibility', 'visible');
						}
					}
				},

				/* function to validate the input */
				validateInput: function(e) {
					var isValidInput = true,
					 	isValidEmail = false,
					 	isValidAirport = false;

					if ( $("#airport_text").val() === '' ) {
						$('#airport_text').css("border-color", "red");
						isValidInput = false;
					} else {
						var objTip = validateAirportTextFieldForSkyTips($('#airport_text').val(), tipAirportCode);
						if(objTip.isValid){
							isValidAirport = true;
							tipAirportCode = objTip.Code;
							tipAirportName = objTip.Name;
						}
					}

					if ( $("#skytip_tite").val() === '' ) {
						$('#skytip_tite').css("border-color", "red");
						isValidInput = false;
					}
					if ( $("#skytip_desc").val() === '' ) {
						$('#skytip_desc').css("border-color", "red");
						isValidInput = false;
					}
					if ( $("#skytip_fname").val() === '' ) {
						$('#skytip_fname').css("border-color", "red");
						isValidInput = false;
					}  
					if ( $("#skytip_lname").val() === '' ) {
						$('#skytip_lname').css("border-color", "red");
						isValidInput = false;
					}  
					if ( $("#skytip_country").val() === '' ) {
						$('#skytip_country').css("border-color", "red");
						isValidInput = false;
					} 
					if ( $(".themeSelectVal").html() === $.i18n.map.sky_select_theme )  {
						$('#ThemeSelect').css("border-color", "red");
						isValidInput = false;
					} 

					if ( $("#skytip_email").val() === '' ) {
						$('#skytip_email').css("border-color", "red");
						isValidInput = false;
					} else {
						var strEmail = $("#skytip_email").val();
						var atStr=[];
						atStr=strEmail.split('@');

						if ( atStr.length === 2 && atStr[0].length >= 1 ) {
							var dotStr = atStr[1].split('.');
							if ( dotStr.length >= 2 && dotStr[0].length >= 1 && ( dotStr[dotStr.length-1].length >=2 && dotStr[dotStr.length-1].length <= 4 ) ) {
								isValidEmail = true;
							}
						} 
					}

					if (!isValidInput) {
						openAlertWithOk( $.i18n.map.sky_validation_all_fields );
					} else if (!isValidAirport) {

						isValidInput = false;
						$('#airport_text').css("border-color", "red");
						openAlertWithOk( $.i18n.map.sky_validation_airport_name );

					} else if (!isValidEmail) {

						isValidInput = false;
						$('#skytip_email').css("border-color", "red");
						openAlertWithOk( $.i18n.map.sky_validation_email_addr );

					} else if ( $('#checkone').prop('checked') != true ) {

						openAlertWithOk( $.i18n.map.sky_validation_check_one );
						isValidInput = false;

					} else if ( $('#checktwo').prop('checked') != true ) {

						openAlertWithOk( $.i18n.map.sky_validation_terms );
						isValidInput = false;

					}  else {}

					return isValidInput;
				},

				/* reset the add sky tips page when skytips got posted */
				resetSkyTips: function(e) {
					$("#airport_text").val('');
					$('#cancel_skytips').css("visibility", "hidden");
					$("#skytip_desc").val('');
					$("#skytip_tite").val('');
					$('#cancel_skytip_tite').css("visibility", "hidden");
					$("#skytip_fname").val('');
					$('#cancel_skytip_fname').css("visibility", "hidden");
					$("#skytip_lname").val('');
					$('#cancel_skytip_lname').css("visibility", "hidden");
					$("#skytip_email").val('');
					$('#cancel_skytip_email').css("visibility", "hidden");
					$("#skytip_country").val('');
					$('#cancel_skytip_country').css("visibility", "hidden");

					var theme = $.i18n.map.sky_select_theme;
					$(".themeSelectVal").html( theme );
					$('#ThemeSelect p').html( theme );
					$('.scrollforThemes :checked').attr('checked', false);
					$('#checkone').attr('checked', false);
					$('#checktwo').attr('checked', false);

				},         

				/* Display suggestive text when user inputs characters */
				showAirportNames : function(e) {
					$('#airport_text').keyup(function() {
						if ( $('#airport_text').val() != '' ) {
							$('.cancel_skytips').css('visibility', 'visible');
							$('.skytips_searchImg').css('visibility', 'hidden');
						} else {
							$('.cancel_skytips').css('visibility', 'hidden');
							$('.skytips_searchImg').css('visibility', 'visible');
						}
					});
					if ( e.keyCode === 8 ) {
						keyval = $('#' + e.target.id).val();

					} else {
						keyval = keyval + String.fromCharCode(e.keyCode);
					}
					
					if ( keyval != "" && e.keyCode != 8 ) {
						sessionStorage.keyval = keyval;
						var minLenVal = 3;
						
						if ( SkyAirportNames != undefined && SkyAirportNames !="undefined" ) {
							SkyAirportNames.sort( function( a, b ) {
								return (a["Airport_Name_" + localStorage.language] > b["Airport_Name_" + localStorage.language]) ? 1 : -1;
							});
						}
						if ( localStorage.language === "zh" || "Ja" === localStorage.language) {
							minLenVal = 1;
						}
						if ( SkyAirportNames != null ) {
							toHighlightAutoComplete( false, true );
							$("#airport_text").autocomplete({
								source : SkyAirportNames,
								minLength : minLenVal,
								open : function( event, ui ) {
									$(".ui-autocomplete").scrollTop(0);
								},
								select : airportselected
							});
							function airportselected( event, ui ) {
								$(this).blur();
								uiValue = ui;
								tipAirportCode = ui.item.airportCode;
								if ( sessionStorage.skytipsReqData != null ) {
									sessionStorage.skytipsReqData = null;
								}
							}
						}
					}
					if ( sessionStorage.deviceInfo === "android" ) {
						if ( e.keyCode === 13 ) {
							$(this).blur();
							this.openSkyTips();
						}
					}
				},

				/* function to display terms and conditions screen */
				openTermsAndCondition: function(e) {
					e.preventDefault();
					e.stopImmediatePropagation();
					/* 583152 -filling data when back from terms & condition */
					/* storing filled form inputs in session */
					var inputsData = {
							'aiportText': $('#airport_text').val(),
							'theme': $('#ThemeSelect p').text(),
							'title': $('#skytip_tite').val(),
							'description': $('#skytip_desc').val(),
							'firstName': $('#skytip_fname').val(),
							'lastName': $('#skytip_lname').val(),
							'email': $('#skytip_email').val(),
							'country': $('#skytip_country').val(),
							'checkOne': $('#checkone').prop('checked'),
							'checkTwo': $('#checktwo').prop('checked')
					};
					sessionStorage.skyTipsFormdata = JSON.stringify( inputsData );
					/* storing filled form inputs in session*/
					/* 583152 -filling data when back from terms & condition */
					showActivityIndicatorForAjax('false');
					var skyTipsTermsURL = URLS.SKYTIPS_ADD_TERMS;
					xhrajax=$.ajax({
						url : skyTipsTermsURL,
						type: 'GET',
						headers: {'api_key':'952e2nydf9ztgvn8n8m24bcm', 'source':'SkyApp'},
						timeout: 30000,
						dataType: 'html',
						success: function( data )
						{
							hideActivityIndicator();
							localStorage.skyTipsTerms =data;
							var obj1 = {};
							obj1.previousPage = sessionStorage.currentPage;
							pushPageOnToBackStack(obj1);
							window.location.href = "index.html#skytipsTerms";
						},
						error: function( xhr, exception ) {
							hideActivityIndicator();
							if (exception === "timeout") {
								openAlertWithOk($.i18n.map.sky_unload);
							} else if (exception === "error"){
								openAlertWithOk($.i18n.map.sky_notconnected);
							} else {}
						},
						failure: function() {
							hideActivityIndicator();
						}
					})
				},

				/* 583152 -filling data when back from terms & condition */
				loadFilledInputs: function () {
					var inputs = JSON.parse( sessionStorage.skyTipsFormdata );
					$('#airport_text').val( inputs.aiportText );
					$('#skytip_tite').val( inputs.title );
					$('#skytip_desc').val( inputs.description );
					$('#skytip_fname').val( inputs.firstName );
					$('#skytip_lname').val( inputs.lastName );
					$('#skytip_email').val( inputs.email );
					$('#skytip_country').val( inputs.country );
					$('#checkone').prop('checked', inputs.checkOne);
					$('#checktwo').prop('checked', inputs.checkTwo);

					var themeCollection = $('#operators input[type="radio"]');
					for ( var i = 0, maxlen = themeCollection.length; i < maxlen; i++ ) {
						if ( $(themeCollection[i]).val() === inputs.theme ) {
							$(themeCollection[i]).prop('checked', true);
							$('#ThemeSelect p').text( inputs.theme );
							break;
						}
					}
				},
				
				/* function to check the checkbox(theme) when corresponding label(theme name) is tapped */
				checkOrUncheckCheckBox: function(e) {
					var forid ='#' + $(e.target).attr('for');
					if ( $(forid).prop('checked') ) { 
						$(forid).prop("checked", false);
					} else {
						$(forid).prop("checked", true);
					}
				}
			});

			return skytipsAddNewView;
});
