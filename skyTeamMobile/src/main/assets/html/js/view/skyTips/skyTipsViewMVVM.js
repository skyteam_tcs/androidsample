/* SkyTips Functionality */
define(['jquery',  'underscore', 'backbone', 'models/skytips/skyTipViewModel','text!template/skyTipsTemplate.html', 'text!template/menuTemplate.html','appView'], function($, _, Backbone,skyTipViewModel,  skyTipsFinderTemplate, menuPageTemplate, appView) {
       var keyval = "";
       var that;
       var uiValue = "";
       var strKeyboardLang="";
       var skyTipsPageView = appView.extend({

          initialize : function() {
        	  $('#backButtonList').hide();
              $('#headingText h2').css('margin-right','');
              $('#headingText img').hide();
              $('#headingText h2').show();
              $('#headingText h2').html($.i18n.map.sky_tips);
              $('#headingText h2').css('text-align','center');
            if(hasValue(sessionStorage.previousPages))
            {
                if(JSON.parse(sessionStorage.previousPages).length != 0){
                sessionStorage.removeItem('previousPages');
                }
            }
            hideActivityIndicator();

              if (sessionStorage.isFromSaved === "yes") {
              $('#brief img').hide();
              $('#brief h2').hide();
              }
              else
              {
              $('#brief img').show();
              $('#brief h2').hide();
              }

              if($('#pageslide').children().length === 0)
              {
              $(".menuItems").pageslide();
              var template = _.template(menuPageTemplate,$.i18n.map);
              $('#pageslide').html(template);
              sessionStorage.isMenuAdded = "YES";
              }

           if ($('.main_header ul li:eq(0) a').attr('class') != 'menuItems') {
           $('.main_header ul li:eq(0) img').remove();
           $('.main_header ul li:eq(0)').append($('<a></a>').attr('class', 'menuItems').attr('href', '#nav').append($('<img></img>').attr('src', './images/menu.png')));
           $(".menuItems").pageslide();
           }
           //$(".menuItems").pageslide();


                    var arrTemp=JSON.parse(localStorage.cityList_Skytips);
                     SkyAirportNames=[];
                	 SkyAirportNames= arrTemp.AirPorts_test;

                      this.model = new skyTipViewModel();
                      this.model.set($.i18n.map);
                                                  if(hasValue(sessionStorage.skyTipsFullAirportName) && hasValue(sessionStorage.skytip_airportCode)){
                                                  this.model.set({'AirportName':sessionStorage.skyTipsFullAirportName});
                                                  this.model.set({'AirportCode':sessionStorage.skytip_airportCode});
                                                  }
                      _.bindAll(this,"render");
                                                  this.model.on("change", this.validation,this);
                                                  that=this;
          },
          render : function() {
            sessionStorage.currentPage = "skyTipsFinder";
          var locationPath = window.location;
          locationPath = locationPath.toString();
          var spath = locationPath.substring(locationPath.lastIndexOf('#') + 1);
          if (spath == "skyTipsFinder" && sessionStorage.navToskyTipsFinder == "yes") {
          $(".menuItems").trigger('click');
          sessionStorage.navToskyTipsFinder = "no";
          }

          var compiledTemplate = _.template(skyTipsFinderTemplate,this.model.toJSON());
          $(this.el).html(compiledTemplate);
          this.$el.find('#skytipsPage').height($('#main_content').height()*.8);/*setting height to 80% of the main container*/
          //this.$el.find('.searchDiv').show();
          /*if(sessionStorage.fromSkytipsResults == "yes"){

            if(sessionStorage.skyTipsFullAirportName != undefined && sessionStorage.skyTipsFullAirportName != ''){
                this.$el.find('#airport_text').val(sessionStorage.skyTipsFullAirportName);
            }
          }*/

          if(this.$el.find('#airport_text').val() != ""){
          this.$el.find(".cancel_Skytips").removeClass("in-Visibility");
          this.$el.find(".cancel_Skytips").addClass("Visibility");
          this.$el.find(".search_skytips").removeClass("Visibility");
          this.$el.find(".search_skytips").addClass("in-Visibility");
          }
          else{
           this.$el.find(".cancel_Skytips").removeClass("Visibility");
           this.$el.find(".cancel_Skytips").addClass("in-Visibility");
           this.$el.find(".search_skytips").removeClass("in-Visibility");
           this.$el.find(".search_skytips").addClass("Visibility");


          }
          sessionStorage.fromSkytipsResults == "no";

           setTimeout(function(){
                                                                      var fHeightAirports = parseInt(that.$el.find("#airports").css("height"));
                                                                      var fHeightFlights = parseInt(that.$el.find("#flights").css("height"));
                                                                      if(fHeightAirports > fHeightFlights)
                                                                      {
                                                                          that.$el.find("#flights").css("height",""+fHeightAirports);
                                                                      }
                                                                      else if(fHeightFlights > fHeightAirports)
                                                                      {
                                                                          that.$el.find("#airports").css("height",""+fHeightFlights);
                                                                      }
                                                                  },1);

           _.bindAll(this,"showAirportNames");
           _.bindAll(this,"showSkyTipsBtn");
           _.bindAll(this,"hideSkyTipsBtn");
           _.bindAll(this,"crossImage_Skytips");
           _.bindAll(this,"createFocus_Skytips");




                                            //********************************************************************************





                                            if(latLonAirportName.length){
                                            autocomplete_autofill_inlanguagechange();
                                            var skyTipscheck= skytipsvalidateAirportTextField(conte);
                                            if(skyTipscheck.Name.length>0)
                                            {

                                            this.$el.find('#airport_text').val(skyTipscheck.Name);


                                            sessionStorage.skyTipsFullAirportName=skyTipscheck.Name
                                            sessionStorage.skytip_airportCode = latLonAirportCode;

                                            if (sessionStorage.skytipsReqData != null) {
                                            sessionStorage.skytipsReqData = null;
                                            }

                                            if (this.$el.find('#airport_text').val() == ''){
                                            $(".arrow_down").css('display','none');
                                            }

                                            $(".cancel_Skytips").removeClass("in-Visibility");

                                            this.$el.find('.cancel_Skytips').css('visibility', 'visible');

                                            $(this).blur();
                                            this.model.set({'AirportCode':latLonAirportCode,'AirportName':skyTipscheck.Name});

                                            this.$el.find(".searchDiv").show();
this.$el.find("#airport_text").removeClass("has-error");
                                            }
                                            }





                                            //***********************************************************************************



         return appView.prototype.render.apply(this, arguments);


          },

          events : {
          'focus :input' : 'showCancelImage',
          'keydown :input' : 'showAirportNames',
          'click #searchSkyTips' : 'openSkyTips',
          'click .cancel_Skytips' : 'crossImage_Skytips',
          'click .search_skytips' : 'createFocus_Skytips',
          /*'touchend input':'hideSkyTipsBtn',*/
          'click #airports' : 'openAddSkytips',
          /*'blur input': 'showSkyTipsBtn',*/
          'touchstart *':function(){
          $.pageslide.close();
          },
          /*'touchend *':function(){
          that.$el.find(".searchDiv").show();
          }*/
          },
          validation:function(){


                                                  var validateResponse = validateSkyTipsAirportTextField(this.model.get("AirportName"));
                                                  if(validateResponse.isValid){
                                                  if(this.$el.find("#airport_text").hasClass("has-error"))
                                                    this.$el.find("#airport_text").removeClass("has-error");
                                                  return true;
                                                  }
                                                  else{
                                                  if(!this.$el.find("#airport_text").hasClass("has-error"))
                                                    this.$el.find("#airport_text").addClass("has-error");
                                                  return false;
                                                  }
          },
          showSkyTipsBtn: function(e){

                                                  if(!(uiValue.item&& hasValue(uiValue.item.value))){
                                                  this.$el.find(".cancel_Skytips").removeClass("Visibility");
                                                  this.$el.find(".cancel_Skytips").addClass("in-Visibility");
                                                  this.$el.find(".search_skytips").removeClass("in-Visibility");
                                                  this.$el.find(".search_skytips").addClass("Visibility");
                                                  }
                                                  else{
                                                  this.model.set({'AirportCode':uiValue.item.airportCode,'AirportName':uiValue.item.value});
                                                  }


            //this.$el.find(".searchDiv").show();
          },
          hideSkyTipsBtn: function(e){
          //e.stopImmediatePropagation();
          //this.$el.find(".searchDiv").hide();
          },
          /* Display Auto-complete cancel image */
          crossImage_Skytips: function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          sessionStorage.skyTipsFullAirportName='';
          sessionStorage.skytip_airport_code = '';
          this.$el.find("#airport_text").val("");
          //this.$el.find(".searchDiv").hide();
          this.$el.find(".cancel_Skytips").removeClass("Visibility");
          this.$el.find(".cancel_Skytips").addClass("in-Visibility");
          this.$el.find(".search_skytips").removeClass("in-Visibility");
          this.$el.find(".search_skytips").addClass("Visibility");
          this.$el.find("#airport_text").focus();
          },
                                                  // to create focus...
                                                  createFocus_Skytips : function(e){
                                                  this.$el.find("#airport_text").val("");
                                                  //this.$el.find(".searchDiv").hide();
                                                  this.$el.find(".cancel_Skytips").removeClass("Visibility");
                                                  this.$el.find(".cancel_Skytips").addClass("in-Visibility");
                                                  this.$el.find(".search_skytips").removeClass("in-Visibility");
                                                  this.$el.find(".search_skytips").addClass("Visibility");
                                                  this.$el.find("#airport_text").focus();
                                                  },
          /* End of Display Auto-complete cancel image */

          /* Display add sky tips page */
          openAddSkytips : function(e) {
            sessionStorage.transRequired = "No";
              sessionStorage.skyTipsFormdata = '';
              //invalidating the skytips finder airport value before moving
              sessionStorage.skyTipsFullAirportName ='';
        	  window.location.href = 'index.html#addSkyTipsPage';
        	  sessionStorage.navToaddSkyTipsPage = "yes";
          },
          /* End of Display add sky tips page */

          /* Retrieve SkyTips when user click Find SkyTips button */
          openSkyTips : function(e) {
                sessionStorage.airportFlag=0;
                sessionStorage.theme = "no";
                var skyTipsFlag = 0;
                if(this.$el.find('#airport_text').val()==''){
                    this.$el.find('#airport_text').addClass("has-error");
                    openAlertWithOk($.i18n.map.sky_selected_tips);
                }
                else{

                    if(!this.validation()){
                        this.$el.find('#airport_text').val('');
                        openAlertWithOk($.i18n.map.sky_tips_unavailable);

                                                  $(".cancel_Skytips").removeClass("Visibility");
                                                  $(".cancel_Skytips").addClass("in-Visibility");
                                                  $(".search_skytips").removeClass("in-Visibility");
                                                  $(".search_skytips").addClass("Visibility");
                    }
                    else{
                    	 var obj = {};
                         sessionStorage.skyTipsAirportName = this.model.get("AirportName");

                         obj.airportCode =this.model.get("AirportCode");
                       sessionStorage.skyTipsAirportName =getAirportFullName(obj.airportCode)+' ('+obj.airportCode+')';

                        obj.url = URLS.SKYTIP_SEARCH+'airportcode=' + obj.airportCode;

                        obj.airportString = sessionStorage.skyTipsAirportName;
                         sessionStorage.skytipsReqData = JSON.stringify(obj);
                         var pageObj = {};
			                 pageObj.previousPage = "skyTipsFinder";
						     pushPageOnToBackStack(pageObj);

                        sessionStorage.removeItem("themeSelected");
                        showActivityIndicatorForAjax('true');
                         window.open(iSC.skytips);
                    }
                }
        },
        /* End of Retrieve SkyTips when user click Find SkyTips button */

        /* Display auto-complete Cancel Image */
        showCancelImage : function(e){
            if (this.$el.find('#airport_text').val() == ''){
                $(".arrow_down").css('display','none');
            }
            if (this.$el.find('#airport_text').val() != ''){

                                                  $(".cancel_Skytips").removeClass("in-Visibility");
                                                  $(".cancel_Skytips").addClass("Visibility");

                                                  $(".search_skytips").removeClass("Visibility");
                                                  $(".search_skytips").addClass("in-Visibility");

            }
            else{

                                                  $(".cancel_Skytips").removeClass("Visibility");
                                                  $(".cancel_Skytips").addClass("in-Visibility");
                                                  $(".search_skytips").removeClass("in-Visibility");
                                                  $(".search_skytips").addClass("Visibility");
            }
        },
                                              /* End of Display auto-complete Cancel Image */

        /* Display suggestive text when user enter 3 characters */
        showAirportNames : function(e) {

            this.$el.find('#airport_text').keyup(function() {
                if ($('#airport_text').val() != ''){

                                     $(".cancel_Skytips").removeClass("in-Visibility");
                                     $(".cancel_Skytips").addClass("Visibility");
                                     $(".search_skytips").removeClass("Visibility");
                                     $(".search_skytips").addClass("in-Visibility");

                }
                else{
                                     $(".cancel_Skytips").removeClass("Visibility");
                                     $(".cancel_Skytips").addClass("in-Visibility");
                                     $(".search_skytips").removeClass("in-Visibility");
                                     $(".search_skytips").addClass("Visibility");
                }

            });
            if (e.keyCode == 8) {
                keyval = $('#' + e.target.id).val();
            }
            else {
                keyval = keyval + String.fromCharCode(e.keyCode);
            }
            if (keyval != "" && e.keyCode != 8) {
                sessionStorage.keyval = keyval;

                                                  var minLenVal=3;


                                                  if(SkyAirportNames != undefined && SkyAirportNames !="undefined"){
                                                  SkyAirportNames.sort(function(a, b) {
                                                             return (a["airportName_"+localStorage.language ]> b["airportName_"+localStorage.language]) ? 1 : -1;
                                                             });
                                                  }

                                                  if(checkCJKCompliedLanguage())
                                                  {

                                                  minLenVal=1;

                                                  }



                if (SkyAirportNames != null) {
                	toHighlightAutoComplete(false,true);
                    $("#airport_text").autocomplete({
                        source : SkyAirportNames,
                        minLength : minLenVal,
                        open : function(event, ui) {
                        $(".ui-autocomplete").scrollTop(0);
                        },
                        select : airportselected
                        });
                    function airportselected(event, ui) {
                        uiValue = ui;
                        $(this).blur();
                        sessionStorage.skyTipsFullAirportName=ui.item.value;
                        sessionStorage.skytip_airportCode = ui.item.airportCode;
                        that.model.set({'AirportCode':uiValue.item.airportCode,'AirportName':uiValue.item.value});
                        if (sessionStorage.skytipsReqData != null) {
                            sessionStorage.skytipsReqData = null;
                        }
                    }
                }
            }
                                              /*if (sessionStorage.deviceInfo == "android") {
                                              if(e.keyCode == 13){
                                              $(this).blur();
                                              this.openSkyTips();
                                              }
                                              }*/
        },
    /* Display suggestive text when user enter 3 characters */
    });
    return skyTipsPageView;
});
/* End of SkyTips Functionality */
