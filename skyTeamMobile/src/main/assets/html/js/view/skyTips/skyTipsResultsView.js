define(
		['jquery', 'backbone', 'underscore','text!template/skyTipsResultsTemplate.html', 'text!template/skyTipsTemplate.html'],
		function($, Backbone, _, skytipsResultsTemplate, skytipsTemplate) {
			var filterTipsResponse = [],
				skyTipsResponse;
				tipsFlag = 0;

			var skytipsResultsView = Backbone.View.extend({

				el : '#skyteam',
				id : 'skyTipsResults-page',

				initialize : function() {
					$('#backButtonList').show();
					that = this;
					if ( sessionStorage.previousPages != undefined ) {
						if ( JSON.parse( sessionStorage.previousPages ).length === 0 ) {
							sessionStorage.removeItem('previousPages');
						}
					}
					$('#headingText img').hide();
					$('#headingText h2').show();
					$('#headingText h2').html( $.i18n.map.sky_tips );
					$('#headingText h2').css('margin-right','37px');
					if ( sessionStorage.isFromSaved === "yes" ) {
						$('#brief img').hide();
						$('#brief h2').hide();
					} else {
						$('#brief img').show();
						$('#brief h2').hide();
						sessionStorage.currentPage = "skytipsResults";
					}
				},

				render : function() {
					var tipThemeFor,
						locationPath = window.location;
					locationPath = locationPath.toString();
					var spath = locationPath.substring( locationPath.lastIndexOf('#') + 1 );
					if ( spath === "skyTipsResults" && sessionStorage.navToskyTipsResults === "yes" ) {
						$(".menuItems").trigger('click');
						sessionStorage.navToskyTipsResults = "no";
					}
					var compiledTemplate = _.template( skytipsResultsTemplate, $.i18n.map );
					$(this.el).html( compiledTemplate );
					hideActivityIndicator();
					sessionStorage.currentPage = "skyTipsResults";

					if ( sessionStorage.From === "searchResults") {
						sessionStorage.From = "searchResults";
					} else {
						sessionStorage.From = "Menu";
					}

					$('.main_header ul li:eq(1) img')
						.off('click')
						.on('click', function(e) {
							if ( $('#pageslide').css('display') === "block" ) {
								e.preventDefault();
								e.stopImmediatePropagation();
								$.pageslide.close();
							} else {
								if ( sessionStorage.airportFlag === 0 ) {
									sessionStorage.fromSkytipsResults = "yes";
								}
								var currentobj = JSON.parse( sessionStorage.previousPages );
								if( currentobj[0].previousPage === "airportDetails" || currentobj[0].previousPage === "airportDetailsViewViaMap" ) {
									showActivityIndicatorForAjax('false');
									sessionStorage.airportDetailsObj = currentobj[0].airportObj;
								} else {
									showActivityIndicator( $.i18n.map.sky_please );
								}
								e.preventDefault();
								e.stopImmediatePropagation();
								window.location.href = "index.html#" + currentobj[0].previousPage;
								popPageOnToBackStack();
								sessionStorage.savedflights = "no";
								$('.main_header ul li:eq(1) img').off('click');
							}
					});

					if ( sessionStorage.theme === "no") { // from skytips finder
						if ( localStorage.skyTipsDetails != undefined && localStorage.skyTipsDetails != null && localStorage.skyTipsDetails != "" ) {
							$('#ThemeSelect').show()
							$('.facebook').hide();
							$('#noSkyTips').hide();
							skyTipsResponse = JSON.parse( localStorage.skyTipsDetails );
							if ( skyTipsResponse.SkyTips.length != undefined ) {
								if ( skyTipsResponse.Count > 0 ) {
									$('.noOfSkytips').html( skyTipsResponse.Count + ' ' + $.i18n.map.sky_tips + "<span class='airports_tipsFor'></span>" );
								} else {
									$('.noOfSkytips').html( $.i18n.map.sky_noskytips + " <span class='airports_tipsFor'></span>" );
									var checkedValues = $('input:checkbox[name=operatorsGroup]:checked');
									checkedValues.each( function( id, obj ) {
										if ( id === 0 ) {
											$('.airports_tipsFor').html(" " + $.i18n.map.sky_for + " " + obj.value);
										} else {
											$('.airports_tipsFor').append(', ' + obj.value);
										}
									});
								}
								$('.airports_tips').html( sessionStorage.skyTipsAirportName );
								for ( var i = 0, maxlen = skyTipsResponse.tipTheme.length ; i < maxlen; i++ ) {
									if ( skyTipsResponse.tipTheme[i] === $.i18n.map.sky_other ) {
										var otherTheme = skyTipsResponse.tipTheme[i];
										skyTipsResponse.tipTheme.splice(i, 1);
										skyTipsResponse.tipTheme.splice(skyTipsResponse.tipTheme.length, 1, otherTheme );
										break;
									}
								}
								var $airports_tipsFor = $('.airports_tipsFor');
								for ( var i = 0, maxlen = skyTipsResponse.tipTheme.length; i < maxlen; i++ ) {
									if ( skyTipsResponse.tipTheme[i] != "") {
										if ( i === 0 ) {
											this.addSelectAllToThemePopUp();
											$airports_tipsFor.html("  " + $.i18n.map.sky_for + " " + skyTipsResponse.tipTheme[i]);
										} else {
											$airports_tipsFor.append(', ' + skyTipsResponse.tipTheme[i]);
										}
										this.addThemeToPopUp( skyTipsResponse.tipTheme[i] );
									}
								}

								$(".arrow_down").show();
								var time = '',
									tipDate = '';

								if ( skyTipsResponse.SkyTips instanceof Array ) { // Start - Array of skytips(count more than 1)
									for ( var i = 0, maxLen = skyTipsResponse.SkyTips.length; i < maxLen; i++ ) {
										if ( skyTipsResponse.SkyTips[i].Date != "undefinfed" && skyTipsResponse.SkyTips[i].Date.length > 0 ) {
											if ( localStorage.time === "12h" ){
												time = timeConvertor((skyTipsResponse.SkyTips[i].Date).split(' ')[1]);
											} else {
												time = (skyTipsResponse.SkyTips[i].Date).split(' ')[1];
											}
											tipDate = getDateInLocaleFormat( (skyTipsResponse.SkyTips[i].Date).split(' ')[0], true );
										}
										var imgpath = getImagePathForTheme( skyTipsResponse.SkyTips[i].TipTheme.trim() );
										var autoTransText, generatedContent;
										if ( skyTipsResponse.SkyTips[i].TipTranslated === 'false' ) {
											autoTransText = '';
										} else {
											autoTransText = this.autoTranslatedText(skyTipsResponse.SkyTips[i].TipOriginalLanguage);
										}
										$('<ul\>')
											.append($('<li\>')
												.html('<img style="width:8%;padding-right:3%;" src=' + imgpath + ' alt="menu_icon"/>' + skyTipsResponse.SkyTips[i].TipTitle))
											.append($('<li\>')
												.html(	skyTipsResponse.SkyTips[i].TipDesc ))
											.append($('<li\>', {style:"width:100%;"})
													.append($('<div\>',{class: 'logo'}))
													.append($('<div\>', {class: 'name'})
															.html('<p>'	+ skyTipsResponse.SkyTips[i].Name + '</p><span>' + skyTipsResponse.SkyTips[i].Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>' + autoTransText)))
											.appendTo($('.tips_msgs'));
									/*	generatedContent = $('<ul></ul>')
																.append( $('<li></li>')
																		.html('<img style="width:8%;padding-right:3%;" src=' + imgpath + ' alt="menu_icon"/>' + skyTipsResponse.SkyTips[i].TipTitle))
																.append( $('<li></li>')
																		.html( skyTipsResponse.SkyTips[i].TipDesc ))
																.append( $('<li style="width:100%;"></li>')
																		.append( $('<div></div>')
																				.attr('class', 'logo'))
																		.append( $('<div></div>')
																				.attr('class', 'name')
																				.html('<p>'	+ skyTipsResponse.SkyTips[i].Name + '</p><span>' + skyTipsResponse.SkyTips[i].Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>' + autoTransText)));
										$('.tips_msgs').append(generatedContent);*/
									}
								} // End - Array of skytips(count more than 1)
								else {
									if ( skyTipsResponse.SkyTips.TipTranslated === 'true' ) {
										$('.tips_msgs')
											.append( $('<ul></ul>')
													.append( $('<li></li>')
															.html('<img style="width:8%;padding-right:3%;" src="images/skytips/lounges.png" alt="menu_icon"/>' + skyTipsResponse.SkyTips.TipTitle))
													.append( $('<li></li>')
															.html( skyTipsResponse.SkyTips.TipDesc ))
													.append( $('<li style="width:100%;"></li>')
															.append( $('<div></div>')
																	.attr('class', 'logo'))
															.append( $('<div></div>')
																	.attr('class', 'name')
																	.html('<p>' + skyTipsResponse.SkyTips.Name + '</p><span>' + skyTipsResponse.SkyTips.Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>' + this.autoTranslatedText(skyTipsResponse.SkyTips.TipOriginalLanguage)))));
									}
									else
									{
										$('.tips_msgs')
											.append( $('<ul></ul>')
													.append( $('<li></li>')
															.html('<img style="width:8%;padding-right:3%;" src="images/skytips/lounges.png" alt="menu_icon"/>' + skyTipsResponse.SkyTips.TipTitle))
													.append( $('<li></li>')
															.html( skyTipsResponse.SkyTips.TipDesc ))
													.append( $('<li style="width:100%;"></li>')
															.append( $('<div></div>')
																	.attr('class', 'logo'))
															.append( $('<div></div>')
																	.attr('class', 'name')
																	.html('<p>' + skyTipsResponse.SkyTips.Name + '</p><span>' + skyTipsResponse.SkyTips.Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>'))
															.append($('<div></div>')
																	.attr('class', 'likes')
																	.html())));
									}
								}
							} else {
								if ( skyTipsResponse.Count > 0 ) {
									$('.noOfSkytips').html( skyTipsResponse.Count + ' SkyTip ' + "<span class='airports_tipsFor'></span>" );
								} else {
									$('.noOfSkytips').html( $.i18n.map.sky_noskytips + " <span class='airports_tipsFor'></span>" );
								}

								for ( var i = 0, maxLen = skyTipsResponse.tipTheme.length; i < maxLen; i++ ) {
									if ( skyTipsResponse.tipTheme[i] === $.i18n.map.sky_other ) {
										var otherTheme = skyTipsResponse.tipTheme[i];
										skyTipsResponse.tipTheme.splice(i, 1);
										skyTipsResponse.tipTheme.splice(skyTipsResponse.tipTheme.length , 1, otherTheme );
										break;
									}
								}
								for (var i = 0, maxLen = skyTipsResponse.tipTheme.length; i < maxLen; i++ ) {
									if ( skyTipsResponse.tipTheme[i] != "" ) {
										if ( i === 0 ) {
											this.addSelectAllToThemePopUp();
											$('.airports_tipsFor').html(" " + $.i18n.map.sky_for + " " + skyTipsResponse.tipTheme[i]);
										} 
										else{
											$('.airports_tipsFor').append(', ' + skyTipsResponse.tipTheme[i] + '</small>');
										}
										this.addThemeToPopUp( skyTipsResponse.tipTheme[i] );
									}
								}

								$('.airports_tips').html( sessionStorage.skyTipsAirportName );
								var time = '',
									tipDate = '';
								if ( skyTipsResponse.SkyTips.Date != "undefinfed" && skyTipsResponse.SkyTips.Date.length > 0 ) {
									if(localStorage.time === "12h"){
										time = timeConvertor((skyTipsResponse.SkyTips.Date).split(' ')[1]);
									}
									else{
										time = (skyTipsResponse.SkyTips.Date).split(' ')[1];
									}
									tipDate = getDateInLocaleFormat(((skyTipsResponse.SkyTips.Date).split(' ')[0]), true);
									
									$(".arrow_down").show();
									if ( skyTipsResponse.SkyTips instanceof Array && skyTipsResponse.SkyTips.length > 1 ) {
										$('.tips_msgs')
											.append($('<ul></ul>')
													.append( $('<li></li>')
															.html('<img style="width:8%;padding-right:3%;" src="images/skytips/lounges.png" alt="menu_icon"/>' + skyTipsResponse.SkyTips[i].TipTitle))
													.append( $('<li></li>')
															.html( skyTipsResponse.SkyTips[i].TipDesc ))
													.append( $('<li style="width:100%;"></li>')
															.append( $('<div></div>')
																	.attr('class', 'logo'))
															.append( $('<div></div>')
																	.attr('class', 'name')
																	.html('<p>' + skyTipsResponse.SkyTips[i].Name + '</p><span>' + skyTipsResponse.SkyTips[i].Country + '</span><span style="float:right"> ' + tipDate + " " + time	+ '</span>'))
															.append( $('<div></div>')
																	.attr('class', 'likes'))
															.append( $('<div></div>')
																	.attr('class', 'disclaimer')
																	.html('<p>' + skyTipsResponse[i].Name + '</p><span style="float:right"> ' + '</span>'))));
									} else {
										if ( skyTipsResponse.SkyTips.TipTranslated === 'true' ) {
									
											$('.tips_msgs')
												.append( $('<ul></ul>')
														.append( $('<li></li>')
																.html('<img style="width:8%;padding-right:3%;" src="images/skytips/lounges.png" alt="menu_icon"/>' + skyTipsResponse.SkyTips.TipTitle))
														.append( $('<li></li>')
																.html( skyTipsResponse.SkyTips.TipDesc ))
														.append( $('<li style="width:100%;"></li>')
																.append( $('<div></div>')
																		.attr('class', 'logo'))
																.append( $('<div></div>')
																		.attr('class', 'name')
																		.html('<p>' + skyTipsResponse.SkyTips.Name + '</p><span>' + skyTipsResponse.SkyTips.Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>' + this.autoTranslatedText( skyTipsResponse.SkyTips.TipOriginalLanguage ) ))));
									
										} else {
											$('.tips_msgs')
												.append( $('<ul></ul>')
														.append( $('<li></li>')
																.html('<img style="width:8%;padding-right:3%;" src="images/skytips/lounges.png" alt="menu_icon"/>' + skyTipsResponse.SkyTips.TipTitle))
														.append( $('<li></li>')
																.html( skyTipsResponse.SkyTips.TipDesc ))
														.append( $('<li style="width:100%;"></li>')
																.append( $('<div></div>')
																		.attr('class', 'logo'))
																.append( $('<div></div>')
																		.attr('class', 'name')
																		.html('<p>' + skyTipsResponse.SkyTips.Name + '</p><span>' + skyTipsResponse.SkyTips.Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>')
																		.append( $('<div></div>')
																				.attr('class', 'likes')
																				.html()))));
										}
									}
								}
							}
						} else {
							$('#ThemeSelect').hide()
							$('#noSkyTips')
									.append( $('<p></p>')
											.html( $.i18n.map.sky_telltips ))
									.append( $('<p></p>')
											.html( $.i18n.map.sky_know_way ))
									.append( $('<p></p>')
											.html( $.i18n.map.sky_sharetip_airport ));
							$('.facebook').show();
						}
						// checking all the themes while rendering
						$('#selectall').trigger('click');
					} else if ( sessionStorage.theme === "yes" ) {
						
						$('#ThemeSelect').show()
						skyTipsResponse = JSON.parse( localStorage.skyTipsDetails );
						$('#ThemeSelect p').html( $.i18n.map.sky_select_theme );
						tipThemeFor = " " + $.i18n.map.sky_for + " ";
					
						for ( var i = 0, maxLen = skyTipsResponse.tipTheme.length; i < maxLen; i++ ) {
							if ( skyTipsResponse.tipTheme[i] != "" ) {
								if ( i === 0 ) {
									this.addSelectAllToThemePopUp();
									tipThemeFor = tipThemeFor + skyTipsResponse.tipTheme[i];
								} else {
									tipThemeFor = tipThemeFor + ", " + skyTipsResponse.tipTheme[i];
								}
								this.addThemeToPopUp( skyTipsResponse.tipTheme[i] );
							}
						}
					
						$('.airports_tips').html( sessionStorage.skyTipsAirportName );
						filterTipsResponse = [];
					
						var filterTips = JSON.parse( localStorage.skyTipsDetails );
						
						if ( filterTips.SkyTips.length != undefined ) {
							if ( sessionStorage.themeSelected === "Select theme" ) {
								for ( var i = 0, maxLen = filterTips.SkyTips.length; i < maxLen; i++ ) {
									filterTipsResponse.push( filterTips.SkyTips[i] );
								}
							} else {
								var selectedThemes = [];
								selectedThemes = JSON.parse( sessionStorage.themeSelected );
								for (var i = 0, maxLen = filterTips.SkyTips.length; i < maxLen; i++ ) {
									for ( var j = 0, numThemes = selectedThemes.length; j < numThemes; j++ ) {
										if( filterTips.SkyTips[i].TipTheme === selectedThemes[j] ) {
											filterTipsResponse.push( filterTips.SkyTips[i] );
											break;
										}
									}
								}
								var i = 0;
								if ( selectedThemes.length === filterTips.tipTheme.length ) {
									$('#selectall').prop('checked', true);
									$('#selectall').trigger('click');
								} else {
									$('.groupCheckbox').each( function() {
										// loop through each checkbox
										if ( this.value === selectedThemes[i] ) {
											this.checked = true;
											$(this).trigger('click');  // select all checkboxes with class "groupCheckbox"
											$(this).prop('checked', true);
											i++;
										}
									});
								}
							}
							if ( filterTipsResponse != undefined && filterTipsResponse.length != 0 ) {
								showActivityIndicator( $.i18n.map.sky_please );
								setTimeout( function() {
									hideActivityIndicator();
								}, 800);
								this.displaySuccess( filterTipsResponse );
							} else {
								this.displayFailure( filterTipsResponse );
							}
						} else {

							if ( sessionStorage.themeSelected === "Select theme" ) {
								filterTipsResponse.push( filterTips.SkyTips );
							} else {
								var selectedThemes = [];
								selectedThemes = JSON.parse( sessionStorage.themeSelected );
								if ( filterTips.SkyTips.TipTheme === selectedThemes[0] ) {
									filterTipsResponse.push( filterTips.SkyTips );
								}
								var i = 0;
								if ( selectedThemes.length === filterTips.tipTheme.length ) {
									$('#selectall').prop('checked', true);
									$('#selectall').trigger('click');
								} else {
									$('.groupCheckbox').each( function() {
										// loop through each checkbox
										if(this.value === selectedThemes[i]){
											this.checked = true;
											$(this).trigger('click');   // select all checkboxes with class "groupCheckbox"
											$(this).prop('checked', true);
											i++;
										}
									});
								}
							}
							if ( filterTipsResponse != undefined && filterTipsResponse.length != 0 ) {
								showActivityIndicator($.i18n.map.sky_please);
								setTimeout( function() {
									hideActivityIndicator();
								}, 800);
								this.displaySuccess( filterTipsResponse );
							} else {
								this.displayFailure( filterTipsResponse );
							}
						}
					}
					$('.menuItems').click( function(){
						localStorage.removeItem('skyTipsDetails');
					});

slideThePage(sessionStorage.navDirection);
				},
				
				events : {
					'click #ThemeSelect':'openThemes',
					'click .themes-ok' : 'closeThemesPopup',
					'click #selectall' : 'checkingThemes',
					'click .themeName' : 'checkThemeSelected',
					'click .groupCheckbox' : 'uncheckSelectAllCheckBox',
					'touchstart *' : function() {
						$.pageslide.close();
					}
				},
				
				/* function to select/deselect all the themes */
				checkingThemes: function(e) {

					if ( $('#selectall').prop('checked') ) { // check select status
						$('.groupCheckbox').each( function() {  // loop through each checkbox
							this.checked = true;   // select all checkboxes with class "groupCheckbox"
						});
					} else {
						$('.groupCheckbox').each( function() {  // loop through each checkbox
							this.checked = false;  // deselect all checkboxes with class "groupCheckbox"                       
						});         
					}
				},

				/* function to show themes pop up */
				openThemes: function(e) {
					if ( sessionStorage.tipsFlag === 0 ) {
						tipThemeFor = " " + $.i18n.map.sky_for + " ";
						for ( var i = 0, maxLen = skyTipsResponse.tipTheme.length; i < maxLen; i++ ) {
							if ( skyTipsResponse.tipTheme[i] === $.i18n.map.sky_other ) {
								var otherTheme = skyTipsResponse.tipTheme[i];
								skyTipsResponse.tipTheme.splice(i, 1);
								skyTipsResponse.tipTheme.splice( skyTipsResponse.tipTheme.length, 1, otherTheme );
								break;
							}
						}
						for ( var i = 0, maxLen = skyTipsResponse.tipTheme.length; i < maxLen; i++) {
							if( skyTipsResponse.tipTheme[i] != "" ) {
								if ( i === 0 ) {
									this.addSelectAllToThemePopUp();
									tipThemeFor = tipThemeFor + skyTipsResponse.tipTheme[i];
								} else {
									tipThemeFor = tipThemeFor + ", " + skyTipsResponse.tipTheme[i];
								}
								
								this.addThemeToPopUp( skyTipsResponse.tipTheme[i] );
							}
						}
						sessionStorage.tipsFlag = 1;
					}
					$('.themes-ok').css('display','block');
					$('#tipThemesSelect').css('display','block');
					$("#blackshade").show();

					$("#skytipsPage").hide();
				},
				
				/* function to dismiss theme selection pop up */
				closeThemesPopup: function(e) {
					var themeResults = [],
						checkedValues = [];
					checkedValues = $('input:checkbox[name=operatorsGroup]:checked');
					if ( checkedValues.length > 0 ) {
						$('#tipThemesSelect').hide();
						$('.themes-ok').hide();
						$("#blackshade").hide();
						if ( $('.scrollforThemes :checked').val() != "" && $('.scrollforThemes :checked').val() != undefined ) {
							if ( $('input:checkbox[name=operatorsGroup]:checked') )
							{
								for ( var i = 0, maxlen = checkedValues.length; i < maxlen; i++ ) {
									if ( $(checkedValues[i]).val() != "selectall" ) {
										themeResults.push( $(checkedValues[i]).val() );
									}
								}

							}
							sessionStorage.theme = "yes";
							filterTipsResponse = [];
							sessionStorage.themeSelected = JSON.stringify( themeResults );

							var filterTips = JSON.parse( localStorage.skyTipsDetails );
							if ( filterTips.SkyTips.length != undefined ) {
								if ( !$('input:checkbox[name=operatorsGroup]:checked') ) {
									for ( var i = 0, maxLen = filterTips.SkyTips.length; i < maxLen; i++ ) {
										filterTipsResponse.push( filterTips.SkyTips[i] );
									}
								} else {
									for ( var i = 0, maxLen = themeResults.length; i < maxLen; i++) {
										for (var j = 0, numSkytips = filterTips.SkyTips.length; j < numSkytips; j++ ) {
											if ( filterTips.SkyTips[j].TipTheme === themeResults[i] ) {
												filterTipsResponse.push( filterTips.SkyTips[j] );
											}
										}
									}
								}
								if ( filterTipsResponse != undefined && filterTipsResponse.length != 0 ) {
									$('.tips_msgs').empty();
									$('.airports_tips').empty();
									$('.noOfSkytips').empty();
									showActivityIndicator( $.i18n.map.sky_please );
									setTimeout( function() {
										hideActivityIndicator();
									}, 800);
									this.displaySuccess( filterTipsResponse );
								}
								else{
									openAlertWithOk($.i18n.map.sky_sorry_skytips);
								}
							} else {			// single skyTips
								if( !$('input:checkbox[name=operatorsGroup]:checked') ) {
									filterTipsResponse.push( filterTips.SkyTips );
								} else {
									for ( var i = 0, maxLen = themeResults.length; i < maxLen; i++ ) {
										if ( filterTips.SkyTips.TipTheme === themeResults[i] ) {
											filterTipsResponse.push( filterTips.SkyTips );
										}
									}
								}
								if ( filterTipsResponse != undefined && filterTipsResponse.length != 0 ) {
									showActivityIndicator( $.i18n.map.sky_please );
									setTimeout( function() {
										hideActivityIndicator();
									}, 800);
									this.displaySuccess( filterTipsResponse );
								} else {
									openAlertWithOk( $.i18n.map.sky_sorry_skytips_theme );
								}
							}
						}
						$("#skytipsPage").css("display", "block");
						$("#skytipsPage").css("display", "block");
					} else {
						$("#tipThemesSelect").addClass("degradeZindex");
						openAlertWithOk( $.i18n.map.sky_theme_error_msg );
					}
				},
				
				/* Success Function - When there are SkyTips */
				displaySuccess : function( filterTipsResponse ) {
					sessionStorage.filterTipsResponse = JSON.stringify( filterTipsResponse );
					$('.tips_msgs').show();
					$('.airports_tips').show();
					$('.noOfSkytips').show();
					$('#noSkyTips').hide();
					$('.facebook').hide();
					if ( filterTipsResponse.length > 0 ) {
						$('.noOfSkytips').html( filterTipsResponse.length + ' ' + $.i18n.map.sky_tips + " <span class='airports_tipsFor'></span>" );
						var checkedValues = $('input:checkbox[name=operatorsGroup]:checked');
						checkedValues.each( function( id, obj ) {
							if ( id === 0 && obj.value != "selectall" ) {
								$('.airports_tipsFor').html(" " + $.i18n.map.sky_for + " " + obj.value + ', ');
							} else if ( id === 0 ) {
								$('.airports_tipsFor').html(" "+$.i18n.map.sky_for+" ");
							} else if ( id === 1 ) {
								$('.airports_tipsFor').append('' + obj.value );
							} else {
								$('.airports_tipsFor').append(', ' + obj.value );
							}
						});
					}
					if( filterTipsResponse.length === 1 ) {
						$('.noOfSkytips').html( filterTipsResponse.length + ' SkyTip' + " <span class='airports_tipsFor'></span>" );
						var checkedValues = $('input:checkbox[name=operatorsGroup]:checked');
						checkedValues.each( function( id, obj ) {
							if ( id === 0 && obj.value != "selectall" ) {
								$('.airports_tipsFor').html(" "+$.i18n.map.sky_for+" "+obj.value+', ');
							} else if ( id === 0 ) {
								$('.airports_tipsFor').html(" "+$.i18n.map.sky_for+" ");
							} else if ( id === 1 ) {
								$('.airports_tipsFor').html( obj.value );
							} else {
								$('.airports_tipsFor').append(', '+ obj.value);
							}
						});
					}
					var time = '',
						tipDate = '';
					$(".arrow_down").show();
					$('.tips_msgs').empty();
					for ( var i = 0, maxLen = filterTipsResponse.length; i < maxLen; i++ ) {
						if ( filterTipsResponse[i].Date != "undefinfed" && filterTipsResponse[i].Date.length > 0 ) {
							if ( localStorage.time === "12h" ) {
								time = timeConvertor( (filterTipsResponse[i].Date).split(' ')[1] );
							} else {
								time = (filterTipsResponse[i].Date).split(' ')[1];
							}
							tipDate = getDateInLocaleFormat( (filterTipsResponse[i].Date).split(' ')[0], true );
						}
						var imgpath = getImagePathForTheme( filterTipsResponse[i].TipTheme.trim() );
						var autoTransText;
						if ( filterTipsResponse[i].TipTranslated === "false" ) {
							autoTransText = "";
						} else {
							autoTransText = this.autoTranslatedText(filterTipsResponse[i].TipOriginalLanguage);
						}
						$('.tips_msgs')
							.append( $('<ul></ul>')
									.append( $('<li></li>')
											.html('<img style="width:8%;padding-right:3%;" src=' + imgpath + ' alt="menu_icon"/>' + filterTipsResponse[i].TipTitle))
									.append( $('<li></li>')
											.html( filterTipsResponse[i].TipDesc ))
									.append( $('<li style="width:100%;"></li>')
											.append( $('<div></div>')
													.attr('class', 'logo'))
											.append( $('<div></div>')
													.attr('class', 'name')
													.html('<p>' + filterTipsResponse[i].Name + '</p><span>' + filterTipsResponse[i].Country + '</span><span style="float:right"> ' + tipDate + " " + time + '</span>' + autoTransText ))));
					}
					$('.airports_tips').html( sessionStorage.skyTipsAirportName );
				},

				/* Failure Function - When there are no SkyTips */
				displayFailure : function(filterTipsResponse) {
					$('#noSkyTips').empty();
					$('.searchDiv').hide();
					$(".arrow_down").show();
					$('.tips_msgs').hide();
					$('.airports_tips').hide();
					$('.noOfSkytips').hide();
					$('#noSkyTips').show();
					$('#noSkyTips')
						.append( $('<p></p>')
								.html( $.i18n.map.sky_telltips ))
						.append( $('<p></p>')
								.html( $.i18n.map.sky_know_way ))
						.append( $('<p></p>')
								.html( $.i18n.map.sky_sharetip_airport ));
					$('.facebook').show();
				},
				
				/* function  to get auto translated text */
				autoTranslatedText: function( originalValue ) {
					var language;
					if ( originalValue === "Es"){  // For Spanish
						language = $.i18n.map.sky_translation_english;
					} else if ( originalValue === "zh" ) {   // For Chinese
						language = $.i18n.map.sky_translation_chinese;
					} else {   // For English
						language = $.i18n.map.sky_translation_english;
					}
					
					return '<p class="translated">' + $.i18n.map.sky_tip_auto_translated_text + ' ' + language + '</p>';
				},
				
				/* function to add the theme checkboxes to theme selection pop up */
				addThemeToPopUp: function( themeName ) {
					$('<label/>', { class: 'myCheckbox'})
						.append($('<input/>', { 
								type: "checkbox",
								class: "groupCheckbox",
								id: themeName.trim().replace(/\s/g, ''),
								value: themeName,
								name: 'operatorsGroup'}))
						.append($("<span/>"))
						.appendTo($('.scrollforThemes'));
					
					$('<h6/>')
						.append($('<span/>', {
									class: "themeName",
									for: themeName.trim().replace(/\s/g, '') })
								.html( themeName ))
						.appendTo($('.scrollforThemes'));
				},
				
				/* function to add the select all checkbox to theme selection pop up */
				addSelectAllToThemePopUp: function() {
					
					$('<label/>', { class: 'myCheckbox'})
						.append($('<input/>', {
										type: 'checkbox',
										id: 'selectall',
										value: 'selectall',
										name: 'operatorsGroup'} ))
						.append($('<span/>'))
						.appendTo($('.scrollforThemes'));
					
					$('<h6/>')
						.append($('<span/>', {
										class: 'themeName',
										for: 'selectall' } )
							.html( $.i18n.map.sky_all ))
						.appendTo($('.scrollforThemes'));
					
					$('.scrollforThemes').append('<hr/>');
				/*	$('.scrollforThemes')
						.append( $('<label></label>')
							.attr('class', 'myCheckbox')
							.append( $('<input type="checkbox">')
									.attr('id', 'selectall')
									.attr('value', 'selectall')
									.attr('name', 'operatorsGroup'))
							.append( $('<span></span>')))
						.append( $('<h6></h6>')
							.append($('<span></span>')
								.attr('class', 'themeName')
								.attr('for', 'selectall' )
								.html( $.i18n.map.sky_all )))
						.append('<hr/>');*/
				},
				
				/* function to check the checkbox(theme) when corresponding label(theme name) is tapped */
				checkThemeSelected: function(e) {
					var forid ='#' + $(e.target).attr('for');
					if ( $(forid).prop('checked') ) { 
						$(forid).prop("checked", false);
						$('#selectall').prop('checked', false);
					} else {
						$(forid).prop("checked", true);
					}
					if ( forid === '#selectall' ) {
							this.checkingThemes();
					}
					e.preventDefault();
				},
				
				/* function to uncheck the select all check box when any of the checkbox is unchecked */
				uncheckSelectAllCheckBox: function(e) {
					if ( $(e.target).prop('checked') === false ) {   
						$('#selectall').prop('checked', false); 
					} 
				}
			});
			
			return skytipsResultsView;
		});
