/*   Lounge Access Policy    */
define(['jquery', 'backbone', 'underscore', 'text!template/menuTemplate.html','appView'],
       function($, Backbone, _, menuPageTemplate,appView) {
       var skyTipsTerms = appView.extend({

         id : 'terms-page',
         initialize : function() {
        	 $('#backButtonList').show();
         $('#brief h2').css('display', 'none');
         $('#headingText img').css('display', 'none');
         $('#headingText h2').css('display', 'block');
         $('#headingText h2').html($.i18n.map.sky_head_terms_condition);


         $('#brief img').css('display', 'none');

        },

         render : function() {
         var previousPage=sessionStorage.currentPage;

         $('.main_header ul li:eq(1) img').off('click').on('click', function(e) {
		 	 e.preventDefault();
		     e.stopImmediatePropagation();
		     var currentobj=JSON.parse(sessionStorage.previousPages);
             window.location.href="index.html#"+currentobj[0].previousPage;
             sessionStorage.removeItem('previousPages');
             $('.main_header ul li:eq(1) img').off('click');
          });

             sessionStorage.currentPage="skytipsTerms";

             var compiledTemplate = _.template(localStorage.skyTipsTerms);

             $(this.el).html(compiledTemplate);
             return appView.prototype.render.apply(this, arguments);

         },
         events:{
             'touchstart *' : function() {
				$.pageslide.close();
				}
             },
         });
       return skyTipsTerms;
       });

