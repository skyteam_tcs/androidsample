/* SkyTips Functionality */
define(
		['jquery',  'underscore', 'backbone', 'text!template/skyTipsTemplate.html', 'text!template/menuTemplate.html'],
		function($, _, Backbone,  skyTipsFinderTemplate, menuPageTemplate) {
			var that, AirportNames,
				keyval = "",
				uiValue = "",
				strKeyboardLang = "",
				tipAirportCode;
			
			var skyTipsPageView = Backbone.View.extend({
				el : '#skyteam',
				id : 'skyTipsFinder-page',
				
				initialize : function() {
					
					$('#backButtonList').hide();
					$('#headingText h2').css('margin-right','');
					if( sessionStorage.previousPages != undefined )
					{
						if( JSON.parse(sessionStorage.previousPages).length != 0 ){
							sessionStorage.removeItem('previousPages');
						}
					}
					
					hideActivityIndicator();
					
					$('#headingText img').hide();
					$('#headingText h2').show();
					$('#headingText h2').html( $.i18n.map.sky_tips );
					$('#headingText h2').css('text-align', 'center');
					
					if(sessionStorage.savedflights === "yes")
					{
						$('#brief img').show();
						$('#brief h2').hide();
						$(".menuItems").pageslide();
						
						var template = _.template( menuPageTemplate, $.i18n.map );
						$('#pageslide').append( template );
						sessionStorage.savedflights = "no";
					}
					if ( $('.main_header ul li:eq(0) a').attr('class') != 'menuItems' ) {
					
						$('.main_header ul li:eq(0) img').remove();
						
						$('.main_header ul li:eq(0)')
							.append( $('<a></a>')
									.attr('class', 'menuItems')
									.attr('href', '#nav')
									.append( $('<img></img>')
											.attr('src', './images/menu.png')));
						
						$(".menuItems").pageslide();
					
					}
					
					$(".menuItems").pageslide();
					sessionStorage.currentPage = "skyTipsFinder";
					
					var arrTemp=JSON.parse( localStorage.cityList_Skytips );
					SkyAirportNames = [];
					SkyAirportNames = arrTemp.AirPorts_test;
	
				},
				
				render : function() {
					
					var locationPath = window.location;
					locationPath = locationPath.toString();
					var spath = locationPath.substring( locationPath.lastIndexOf('#') + 1 );
				
					if ( spath == "skyTipsFinder" && sessionStorage.navToskyTipsFinder == "yes" ) {
						$(".menuItems").trigger('click');
						sessionStorage.navToskyTipsFinder = "no";
					}
					
					var compiledTemplate = _.template( skyTipsFinderTemplate, $.i18n.map );
					$( this.el ).html( compiledTemplate );
					$('.searchDiv').show();
					
					if( sessionStorage.fromSkytipsResults === "yes" ) {
						if (sessionStorage.searchData != undefined && sessionStorage.searchData != null) {
							var dataobj = JSON.parse( sessionStorage.searchData );
							$('#airport_text').val( dataobj.Src );
						}
					}
					
					if( $('#airport_text').val() != "" ) {
						$(".cancel_Skytips").css("visibility", "visible");
						$('.search_skytips').css('visibility', 'hidden');
					} else {
						$('.cancel_Skytips').css('visibility', 'hidden');
						$('.search_skytips').css('visibility', 'visible');

					}
					sessionStorage.fromSkytipsResults = "no";
slideThePage(sessionStorage.navDirection);
				},

				events : {
					'focus :input' : 'showCancelImage',
					'keydown :input' : 'showAirportNames',
					'click #searchSkyTips' : 'openSkyTips',
					'click .cancel_Skytips' : 'crossImage_Skytips',
					'touchend input':'hideSkyTipsBtn',
					'click #addSkytips' : 'openAddSkytips',
					'blur input': 'showSkyTipsBtn',
					'click .search_skytips' : 'crossImage_Skytips',
					'touchstart *':function(){
						$.pageslide.close();
					},
					'touchend *':function(){
						$(".searchDiv").show();
					}
				},
				
				/* Function to show the Find Skytips button */
				showSkyTipsBtn: function(e){
					$(".searchDiv").show();
				},
				
				/* Function to hide the Find Skytips button */
				hideSkyTipsBtn: function(){
					$(".searchDiv").hide();
				},
				
				/*  function to clear the text in auto-complete textfield */
				crossImage_Skytips: function(e){
					$("#airport_text").val("");
					$(".searchDiv").hide();
					$(".cancel_Skytips").css("visibility", "hidden");
					$('.search_skytips').css('visibility', 'visible');                                        
					$("#airport_text").focus();
				},
				
				/* function to display add sky tips page */
				openAddSkytips : function(e) {
		            sessionStorage.transRequired = "No";
					window.location.href = 'index.html#addSkyTipsPage';
					sessionStorage.navToaddSkyTipsPage = "yes";
				},
				
				/* function to Retrieve SkyTips when user click Find SkyTips button */
				openSkyTips : function(e) {
					sessionStorage.airportFlag=0;
					sessionStorage.theme = "no";
					var skyTipsFlag = 0;
					if($('#airport_text').val() == ''){
						$('#airport_text').css('border-color','red');
						openAlertWithOk( $.i18n.map.sky_selected_tips );
					} else {
						var objTip = validateAirportTextFieldForSkyTips($('#airport_text').val(), tipAirportCode);
						if(objTip.isValid){
							skyTipsFlag = 1;
							tipAirportCode = objTip.Code;
						}
						
						if(skyTipsFlag === 0){
							
							$('#airport_text').val('');
							$(".arrow_down").hide();
							openAlertWithOk( $.i18n.map.sky_tips_unavailable );
							$(".cancel_Skytips").css("visibility", "hidden");
							$('.search_skytips').css('visibility', 'visible');
							
						} else if ( skyTipsFlag === 1 ){
							
							var objAirport = {};
							objAirport.Src = $('#airport_text').val();
							sessionStorage.searchData = JSON.stringify( objAirport );
							
							var obj = {},
								x = uiValue.item.value;
							sessionStorage.skyTipsAirportName = uiValue.item.value;
							
							var dataArray = x.split("(");
							obj.airportCode = tipAirportCode;
							
							sessionStorage.skyTipsAirportName = getAirportFullName( tipAirportCode ) +' ('+tipAirportCode+')';

							obj.url = URLS.SKYTIP_SEARCH + 'airportcode=' + obj.airportCode;
							obj.airportString = x;
							sessionStorage.skytipsReqData = JSON.stringify( obj );
						
							var pageObj = {};
							pageObj.previousPage = "skyTipsFinder";
							pushPageOnToBackStack( pageObj );
							showActivityIndicatorForAjax('true');
							sessionStorage.themeSelected = '';
							window.open( iSC.skytips );
						
						} else {}
					}
				},

				/* function to display clear button in auto-complete text field */
				showCancelImage : function(e){
					if ( $('#airport_text').val() == '' ){
						$(".arrow_down").hide();
					}
					
					if ( $('#airport_text').val() != '' ){
						$('.cancel_Skytips').css('visibility', 'visible');
						$('.search_skytips').css('visibility', 'hidden');
					} else {
						$('.cancel_Skytips').css('visibility', 'hidden');
						$('.search_skytips').css('visibility', 'visible');
					}
				},

				/* to display suggestive texts i.e. autocomplete view when user inputs characters */
				showAirportNames : function(e) {

					$('#airport_text').keyup(function() {
						if ( $('#airport_text').val() != '' ){
							$('.cancel_Skytips').css('visibility', 'visible');
							$('.search_skytips').css('visibility', 'hidden');
						} else {
							$('.cancel_Skytips').css('visibility', 'hidden');
							$('.search_skytips').css('visibility', 'visible');
						}
						$('#airport_text').css('border-color','#d7d7d7');
					});
					
					if ( e.keyCode === 8 ) {
						keyval = $('#' + e.target.id).val();

					} else {
						keyval = keyval + String.fromCharCode( e.keyCode );
					}
					if ( keyval != "" && e.keyCode != 8 ) {
						sessionStorage.keyval = keyval;
				
						var minLenVal=3;
						
						if( SkyAirportNames != undefined && SkyAirportNames != "undefined" ){
							SkyAirportNames.sort(function( a, b ) {
								return ( a["Airport_Name_"+localStorage.language] > b["Airport_Name_"+localStorage.language] ) ? 1 : -1;
							});
						}

						if( localStorage.language === "zh" || "Ja" === localStorage.language)
						{
							minLenVal=1;
						}                                

						if ( SkyAirportNames != null ) {

							toHighlightAutoComplete( false, true );

							$("#airport_text").autocomplete({
								source : SkyAirportNames,
								minLength : minLenVal,
								open : function(event, ui) {
									$(".ui-autocomplete").scrollTop(0);
								},
								select : airportselected
							});
							
							function airportselected( event, ui ) {
								$(this).blur();
								uiValue = ui;
								tipAirportCode = ui.item.airportCode;
								if ( sessionStorage.skytipsReqData != null ) {
									sessionStorage.skytipsReqData = null;
								}
							}
						}
					}
					if (sessionStorage.deviceInfo == "android") {
						if( e.keyCode == 13 ){
							$(this).blur();
							this.openSkyTips();
						}
					}
				},
			});
			
			return skyTipsPageView;
});
