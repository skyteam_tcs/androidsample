define(['jquery', 'backbone', 'underscore','models/skytips/skyTipAddViewModel','text!template/skyTipsAddNewTemplateMVVM.html','text!template/skyTipsPostPreview.html','appView'], function($, Backbone, _,skyTipAddViewModel,skytipsAddNewTemplate,previewTemplate,appView) {
       var filterTipsResponse = [];
       var skyTipsResponse;
       var tipsFlag=0;
       var keyval = "";
       var popUpView;
       var uiValue = "";
       var AirportNames;
       var isCloseBtnClicked=false;
       var strKeyboardLang="";
       var airName,airCode,self;
       var previewPopup = Backbone.View.extend({
                el : '#skyteam',
                initialize : function() {

                },
                events:{
                   'click .btn_edit':'closePreview',
                   'click .btn_post':'postSkyTips',
                   'click #operator-ok' : 'closeThemesPopup',
                   'click .themeName' : 'checkSelectedTheme',

                },
                render:function(){
                   $('#blackshade').show();
                   var compiledPreviewTemplate = _.template(previewTemplate,self.model.toJSON());
                   this.$el.append(compiledPreviewTemplate)
                   this.$el.find('#tipThemesSelect').remove();
                },
               openThemesPopup:function(){
                   var compiledPreviewTemplate = _.template(previewTemplate,self.model.toJSON());
                   this.$el.append(compiledPreviewTemplate);
                   popUpView.$el.find('.preview-popUp').hide();//hidding the preview content
                   popUpView.$el.find('.themes-ok').show();//showing the theme content
                   popUpView.$el.find('#tipThemesSelect').show();
                   var previousThemeId =self.model.get("TipThemeID");
                   if( (self.$el.find('#ThemeSelect p').html().trim() !== $.i18n.map.sky_select_theme) && hasValue(previousThemeId)){
                    themeName=popUpView.$el.find('#'+previousThemeId).prop('checked',true);
                   }
                   popUpView.$el.find('#operator-ok').off('click').on('click',popUpView.closeThemesPopup);
                   $('#blackshade').show();
               },
               closeThemesPopup:function(e){
               e.preventDefault();
               e.stopImmediatePropagation();
               popUpView.$el.find('#operator-ok').off('click');
               var themeName ='',themeId='';
               var themeId= "9";
               if(popUpView.$el.find('.scrollforThemes :checked').val() != "" && popUpView.$el.find('.scrollforThemes :checked').val() != undefined){
               self.$el.find('#ThemeSelect p').html('');
               self.$el.find('#ThemeSelect p').html(popUpView.$el.find('.scrollforThemes :checked').val());
               themeName=popUpView.$el.find('.scrollforThemes :checked').val();
               themeId = popUpView.$el.find('.scrollforThemes :checked').attr('id');
               self.$el.find('#ThemeSelect').css("border-color", "#d7d7d7");


               sessionStorage.theme = "yes";
               filterTipsResponse = [];
               sessionStorage.themeSelected = themeName;
               //var filterTips = JSON.parse(localStorage.skyTipsDetails);

               self.model.set({"TipTheme":themeName});
               self.model.set({"TipThemeID":themeId});
               }
               popUpView.$el.find('.preview-popUp').remove();
               popUpView.$el.find('#tipThemesSelect').remove();
               $('#blackshade').hide();

               },
               checkSelectedTheme: function(e) {
               e.preventDefault();
               e.stopImmediatePropagation();
               var forid = '#' + $(e.target).attr('for');
               popUpView.$el.find(forid).prop("checked", true);

               },


               closePreview:function(e){
               e.preventDefault();
               e.stopImmediatePropagation();
               popUpView.$el.find('.preview-popUp').remove();
               popUpView.$el.find('#tipThemesSelect').remove();
               $('#blackshade').hide();
               },
               postSkyTips:function(e){
               e.preventDefault();
               e.stopImmediatePropagation();
               var formInput = self.model.toJSON();
               popUpView.$el.find('.preview-popUp').remove();
               popUpView.$el.find('#tipThemesSelect').remove();
               $('#blackshade').hide();
               showActivityIndicatorForAjax('false');

               var tipThemeID =0;
               switch (formInput.TipTheme) {
               case $.i18n.map.sky_fast_track:
               tipThemeID = 1;// 1
               break;
               case $.i18n.map.sky_food_drink:
               tipThemeID = 3;// 3
               break;
               case $.i18n.map.sky_relaxation:
               tipThemeID = 4;// 4
               break;
               case $.i18n.map.sky_transportation:
               tipThemeID = 5;// 5
               break;
               case $.i18n.map.sky_leisure:
               tipThemeID = 6;// 6
               break;
               case $.i18n.map.sky_e_connectivity:
               tipThemeID = 7;// 7
               break;
               case $.i18n.map.sky_shopping:
               tipThemeID = 8;// 8
               break;
               case $.i18n.map.sky_other:
               tipThemeID = 17;// 17
               break;
               case $.i18n.map.sky_lounge:
               tipThemeID = 18;//18
               break;
               default:

               }

               var now = new Date(),
               nowHour = now.getHours(),
               nowMin = now.getMinutes();

               //converting hour into 2 digit,  if its in 1 digit
               nowHour = nowHour < 10 ? ("0" + nowHour) : ( nowHour );

               //converting Minutes into 2 digit,  if its in 1 digit
               nowMin = nowMin > 9 ? ( nowMin ) : ("0"+nowMin);

               var created_at = $.datepicker.formatDate('dd/mm/yy', new Date());


               var formData={
               "TipAirportName":formInput.TipAirportName,
               "TipAirportCode":formInput.TipAirportCode,
               "TipTheme": formInput.TipTheme,
               "TipTitle": formInput.TipTitle,
               "TipDescription":formInput.TipDescription ,
               "FirstName":formInput.FirstName,
               "LastName":formInput.LastName,
               "Email":formInput.Email,
               "Country":formInput.Country,
               "TipThemeID":tipThemeID,
               "TipCreatedAt":created_at+" "+nowHour+":"+nowMin};



               var acceptLanguage=(localStorage.language !="zh")?localStorage.language : "zh-Hans";
               var postUrl=URLS.SKYTIPS_ADD;
               xhrajax=$.ajax({
                              url : postUrl,
                              type: 'POST',
                              headers:{'api_key':API_KEY,'Accept-Language':acceptLanguage, 'source': 'SkyApp'},
                              data : JSON.stringify(formData),
                              contentType:"application/json; charset=utf-8",
                              success: function(data, textStatus, jqXHR)
                              {
                              //data - response from server

                              hideActivityIndicator();
                              openAlertWithOk($.i18n.map.sky_tip_confm_msg,'skytips');


                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {

                              hideActivityIndicator();
                              if(textStatus != 'abort'){
                              openAlertWithOk($.i18n.map.sky_tips_post_error);
                              }
                              }
                              });

               },

        });
       popUpView = new previewPopup();
       var skytipsAddNewView = appView.extend({
                                                    //el : '#skyteam',
                                                    id : 'addSkyTipsPage-page',

                                                    initialize : function() {
                                                    $('#backButtonList').hide();
                                                    $('#headingText h2').css('margin-right','');

                                                    $('#headingText img').css('display', 'none');
                                                    $('#headingText h2').css('display', 'block');
                                                    $('#headingText h2').html($.i18n.map.sky_tips);
                                                    $('#brief img').show();
                                                    $('#brief h2').hide();
                                                    self = this;
                                                    _.bindAll(this,"render");
                                                    },
                                                    render : function() {
                                                    //_.bindAll(this,this.render);
                                                    var myModel = new skyTipAddViewModel();
                                                    myModel.set($.i18n.map);

                                                    if(hasValue(sessionStorage.skyTipsFormdata)){
                                                    myModel.set(JSON.parse(sessionStorage.skyTipsFormdata));
                                                    }


                                                    this.model = myModel;
                                                    this.listenTo( this.model,'change', this.dataChange);
                                                    var compiledTemplate = _.template(skytipsAddNewTemplate,this.model.toJSON());
                                                    /*if{
                                                    this.loadFilledInputs();
                                                    }*/

                                                    $(this.el).html(compiledTemplate);
                                                    this.$el.find('#previewSkytip').off('click').on('click',self.openPreview);
                                                    //input controls cancel button hidden
                                                    this.$el.find('.cancel_skytips_btn').hide();
                                                    if((sessionStorage.currentPage === "skytipsTerms")&&(hasValue(sessionStorage.skyTipsFormdata))){
                                                    this.populatePreviousValue();
                                                    }
                                              /*self.$el.find('.themes-ok').off('click').on('click',function(e){

                                                                                          self.closeThemesPopup();
                                                                                          });*/
                                                    sessionStorage.currentPage = "addSkyTipsPage";




                                                     //********************************************************************************

                                                                                                  if(latLonAirportName.length){

                                                                                                  autocomplete_autofill_inlanguagechange();
                                                                                                  var skyTipscheck= skytipsvalidateAirportTextField(conte);
                                                                                                  this.$el.find('#airport_text').val(skyTipscheck.Name);

                                                                                                  if (sessionStorage.skytipsReqData != null) {
                                                                                                  sessionStorage.skytipsReqData = null;
                                                                                                  }



                                                                                                  $(".cancel_Skytips").removeClass("in-Visibility");

                                                                                                  this.$el.find('.cancel_Skytips').css('visibility', 'visible');
                                                                                                  this.$el.find('.skytips_searchImg').css('visibility', 'hidden');

                                                                                                  }

                                                                                                  //***********************************************************************************


                                                    return appView.prototype.render.apply(this, arguments);

                                                    //slideThePage(sessionStorage.navDirection);




                                                    },
                                                    events : {
                                                    'focusin :input#airport_text' : 'changeHeight',
                                                    'focus :input' : 'showCancelImage',
                                                    'focusout #airport_text' : 'retainHeight',
                                                    'keydown :input' : 'showCancelImage',
                                                    'keydown #sky_desc' : 'removeBorder',
                                                    'keydown :input#airport_text' : 'showAirportNames',
                                                    'click #previewSkytip' : 'openPreview',
                                                    'click .cancel_skytips_btn' : 'crossImage_Skytips',
                                                    'mousedown .cancel_skytips' : 'crossImage_Skytips',
                                                    'click .skytips_searchImg' : 'createFocus_Skytips',
                                                    'touchend input':'hideSkyTipsBtn',
                                                    'blur input': 'showSkyTipsBtn',
                                                    'blur #skytip_desc': 'addDescription',
                                                    'click .spanCheckBox' : 'checkOrUncheckCheckBox',
                                                    'click #flights' : 'openFindSkytips',
                                                    'click #ThemeSelect':'openThemes',
                                                    'click #terms':'openTermsAndCondition',

                                                    'click :input[type="checkbox"]':'validateCheckBox',

                                                    'touchstart *':function(){
                                                    $.pageslide.close();
                                                    },
                                                    'touchend *':function(){
                                                    self.$el.find(".searchDiv").show();
                                                    }
                                                    },
                                                    //showClearImgForPrevious session value
                                                    populatePreviousValue:function(){
                                                        if(hasValue(sessionStorage.skyTipsFormdata)){
                                                        var inputs = this.$el.find("input[type='text']");
                                                        $.each(inputs,function(index,inputControl){
                                                               var inputedText = $(inputControl).val();
                                                                   if(hasValue(inputedText) && inputedText.length>1){
                                                                   var targetId = $(inputControl).attr("id");
                                                                       if(targetId !== "airport_text"){
                                                                       self.$el.find('#cancel_' + targetId).show();
                                                                       self.$el.find('#btn_' + targetId).show();
                                                                       }
                                                                       else{
                                                                       //this.$el.find('.skytips_searchImg').hide();
                                                                      self.$el.find('.skytips_searchImg').css('visibility', 'hidden');
                                                                      self.$el.find('#cancel_skytips').css("visibility","visible");
                                                                       //$('#cancel_skytips').show();
                                                                       self.$el.find('#btn_airport_text').css("visibility","visible");

                                                                       }
                                                                   }
                                                               });
                                                        }
                                                    },
                                                    addDescription:function(e){
                                                    var inputedText = $(e.target).val();

                                                    var modelKey =$(e.target).data("key");
                                                    var valueObj = {};
                                                    valueObj[modelKey]=inputedText;

                                                    if(modelKey === "TipAirportCode"){

                                                    }
                                                    else{
                                                    self.model.set(valueObj);
                                                    }
                                                    },
                                                    showSkyTipsBtn: function(e){
                                                    var inputedText = $(e.target).val();
                                                    var modelKey =$(e.target).data("key");
                                                    var valueObj = {};

                                                    if("TipAirportCode" == modelKey){
                                                    if(!hasValue(uiValue.item.value)){
                                                    self.$el.find('.skytips_searchImg').css('visibility', 'visible');
                                                    self.$el.find('#cancel_skytips').css("visibility","hidden");
                                                    //self.$el.find('#cancel_skytips').show();
                                                    self.$el.find('#btn_airport_text').css("visibility","hidden");
                                                    }
                                                    else{
                                                        inputedText = uiValue.item.airportCode;
                                                    }
                                                    }
                                                     valueObj[modelKey]=inputedText;
                                                    self.model.set(valueObj);

                                                    self.$el.find(".searchDiv").show();
                                                    },
                                                    hideSkyTipsBtn: function(){
                                                    self.$el.find(".searchDiv").hide();
                                                    },
                                                    removeBorder: function(){
                                                    self.$el.find('#skytip_desc').css("border-color", "#d7d7d7");
                                                    },
                                                    retainHeight : function(e) {

                                                    self.$el.find('.skytipContent').css('margin-top','0px');
                                                    if(sessionStorage.deviceInfo == "android"){
                                                    if ((screen.width == 480) && (screen.height == 800)) {
                                                    self.$el.find(".skytipContent").css('margin-top', '0px');

                                                    }
                                                    }
                                                    if(isCloseBtnClicked)
                                                    {
                                                    isCloseBtnClicked=false;
                                                    $('#'+e.target.id).focus();
                                                    }
                                                    },
                                                    changeHeight : function(e){
                                                    var osVersion;
                                                    if(sessionStorage.deviceInfo == "iPhone"){
                                                    if ((screen.width == 320) && (screen.height == 480)) {
                                                    osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i);
                                                    osVersion = osVersion[0];
                                                    osVersion = osVersion.replace(/_/g, '.');
                                                    osVersion = osVersion.replace('OS ', '');
                                                    if((osVersion.split('.')[0]) == "7"){
                                                    self.$el.find('.skytipContent').css('margin-top',sessionStorage.scrollHeight);
                                                    }
                                                    }
                                                    }
                                                    else if(sessionStorage.deviceInfo == "android"){
                                                    if ((screen.width == 480) && (screen.height == 800)) {
                                                    self.$el.find(".skytipContent").css('margin-top', '-70px');

                                                    }
                                                    }
                                                    },

                                                    openThemes:function(e){
                                                      e.preventDefault();
                                                      e.stopImmediatePropagation();
                                                      popUpView.openThemesPopup();
                                                   },
                                                    checkOrUncheckCheckBox: function(e) {
                                                      e.preventDefault();
                                                      e.stopImmediatePropagation();
                                                      var forid = '#' + $(e.target).attr('for');
                                                      if( self.$el.find(forid).prop('checked')) {
                                                      self.$el.find(forid).prop("checked", false);
                                                      self.$el.find('span[data-for="'+$(e.target).attr('for')+'"]').addClass("has-error");
                                                      } else {
                                                      self.$el.find(forid).prop("checked", true);
                                                      self.$el.find('span[data-for="'+$(e.target).attr('for')+'"]').removeClass("has-error");
                                                      }

                                                    },
                                                    // to create focus...
                                                    createFocus_Skytips : function(e){
                                                    var targetId=e.target.id;

                                                    //                                                    if(targetId == 'cancel_skytips' ){
                                                    self.$el.find("#airport_text").val("");
                                                    self.$el.find(".searchDiv").hide();
                                                    self.$el.find("#airport_text .cancel_skytips").css("visibility","hidden");
                                                    self.$el.find('.skytips_searchImg').css('visibility', 'visible');

                                                    self.$el.find("#airport_text").focus();


                                                    },
                                                    openPreview:function(e){
                                                    /*583152 - chinese inputs checking */
                                                    e.preventDefault();
                                                    e.stopImmediatePropagation();
                                                    var inputedAirportText = self.$el.find("#airport_text").val();
                                                    self.model.set({'TipAirportName':inputedAirportText});

                                                    //this.model.set({"TipTheme":sessionStorage.themeSelected});

                                                    if(self.validate()){
                                                       popUpView.render({'model':self.model});
                                                    }
                                                    },



                                                    openFindSkytips : function(e) {
                                                    sessionStorage.transRequired = "No";

                                                    window.location.href = 'index.html#skyTipsFinder';
                                                    //	  sessionStorage.navToaddSkyTipsPage = "yes";
                                                    },
                                                    /* Display Auto-complete cancel image */
                                                    crossImage_Skytips: function(e){
                                                    var targetId=e.target.id;

                                                    if(targetId == 'btn_airport_text' ){
                                                    self.$el.find("#airport_text").val("");
                                                    self.$el.find(".searchDiv").hide();
                                                    self.$el.find("#airport_text .cancel_skytips").hide();
                                                    //$('.skytips_searchImg').show();
                                                    self.$el.find('.skytips_searchImg').css('visibility', 'visible');

                                                    isCloseBtnClicked=true;
                                                    self.$el.find("#airport_text").focus();

                                                    }
                                                    else
                                                    {

                                                    self.$el.find('#'+targetId.replace('btn_','')).val('');
                                                    self.$el.find('#cancel_'+targetId.replace('btn_','')).hide();
                                                    self.$el.find('#'+targetId.replace('btn_','')).focus();
                                                    }

                                                    },
                                                    /* End of Display Auto-complete cancel image */



                                                    /* Display auto-complete Cancel Image */
                                                    showCancelImage : function(e){
                                                    var targetId=e.target.id;
                                                    $('#'+targetId).keyup(function() {
                                                                          if($('#'+targetId).val() != ''){




                                                                          $('#cancel_'+targetId).show();
                                                                          $('#btn_'+targetId).show();
                                                                          $('#'+targetId).css("border-color", "#d7d7d7");
                                                                          }
                                                                          else
                                                                          {

                                                                          $('#cancel_'+targetId).hide();
                                                                          }
                                                                          });
                                                    if(targetId == 'airport_text' ){
                                                    if (self.$el.find('#airport_text').val() != ''){
                                                    self.$el.find('.cancel_skytips').css('visibility', 'visible');
                                                    self.$el.find('.skytips_searchImg').css('visibility', 'hidden');
                                                    }
                                                    else{
                                                    self.$el.find('.cancel_skytips').css('visibility', 'hidden');
                                                    self.$el.find('.skytips_searchImg').css('visibility', 'visible');
                                                    }


                                                    }
                                                    },
                                                    /* End of Display auto-complete Cancel Image */
                                                    /* Display suggestive text when user enter 3 characters */
                                                    showAirportNames : function(e) {
                                                    self.$el.find('#airport_text').keyup(function() {
                                                                             if ($('#airport_text').val() != ''){
                                                                             $('.cancel_skytips').css('visibility', 'visible');
                                                                             $('.skytips_searchImg').css('visibility', 'hidden');
                                                                             }
                                                                             else{
                                                                             $('.cancel_skytips').css('visibility', 'hidden');
                                                                             $('.skytips_searchImg').css('visibility', 'visible');
                                                                             }
                                                                             });
                                                    if (e.keyCode == 8) {
                                                    keyval = self.$el.find('#' + e.target.id).val();
                                                    }
                                                    else {
                                                    keyval = keyval + String.fromCharCode(e.keyCode);
                                                    }
                                                    if (keyval != "" && e.keyCode != 8) {
                                                    sessionStorage.keyval = keyval;

                                                    var minLenVal=3;


                                                    if(SkyAirportNames != undefined && SkyAirportNames !="undefined"){
                                                    SkyAirportNames.sort(function(a, b) {
                                                                         return (a["Airport_Name_"+localStorage.language ]> b["Airport_Name_"+localStorage.language]) ? 1 : -1;
                                                                         });
                                                    }

                                                    if(checkCJKCompliedLanguage())
                                                    {
                                                    minLenVal=1;
                                                    }





                                                    if (SkyAirportNames != null) {
                                                    toHighlightAutoComplete(false,true);
                                                    $("#airport_text").autocomplete({
                                                                                    source : SkyAirportNames,
                                                                                    minLength : minLenVal,
                                                                                    open : function(event, ui) {
                                                                                    $(".ui-autocomplete").scrollTop(0);
                                                                                    },
                                                                                    select : airportselected
                                                                                    });
                                                    function airportselected(event, ui) {
                                                    uiValue = ui;
                                                    $(this).blur();

                                                    if (sessionStorage.skytipsReqData != null) {
                                                    sessionStorage.skytipsReqData = null;
                                                    }
                                                    }
                                                    }
                                                    }
                                                    if (sessionStorage.deviceInfo == "android") {
                                                    if(e.keyCode == 13){
                                                    $(this).blur();
                                                    this.openSkyTips();
                                                    }
                                                    }
                                                    },
                                                    /* Display suggestive text when user enter 3 characters */
                                                    openTermsAndCondition:function(e){
                                                    e.preventDefault();
                                                    e.stopImmediatePropagation();
                                                    var inputedAirportText = self.$el.find("#airport_text").val();
                                                    self.model.set({'TipAirportName':inputedAirportText});
                                                    self.model.set({AgeAgree:self.$el.find('#checkone').prop('checked')});
                                                    self.model.set({TermsAgree:self.$el.find('#checktwo').prop('checked')});
                                                    sessionStorage.skyTipsFormdata = JSON.stringify(this.model.toJSON());

                                                    //sessionStorage.skyTipsFormdata = JSON.stringify(inputsData);
                                                    /* storing filled form inputs in session*/
                                                    /* 583152 -filling data when back from terms & condition */
                                                    showActivityIndicatorForAjax('false');
                                                    var skyTipsTermsURL=URLS.SKYTIPS_ADD_TERMS;

                                                    xhrajax=$.ajax({
                                                                   url : skyTipsTermsURL,
                                                                   type: 'GET',
                                                                   timeout: 30000,
                                                                   dataType: 'html',
                                                                   headers:{'api_key':'952e2nydf9ztgvn8n8m24bcm', 'source': 'SkyApp'},
                                                                   success: function(data)
                                                                   {
                                                                   hideActivityIndicator();
                                                                   localStorage.skyTipsTerms =data;
                                                                   var obj1 = {};
                                                                   obj1.previousPage = sessionStorage.currentPage;
                                                                   pushPageOnToBackStack(obj1);
                                                                   window.location.href="index.html#skytipsTerms";
                                                                   },
                                                                   error: function(xhr,exception){
                                                                   hideActivityIndicator();
                                                                   if(exception == "timeout")
                                                                   {
                                                                   openAlertWithOk($.i18n.map.sky_unload);
                                                                   }

                                                                   else if(exception == "error"){
                                                                   openAlertWithOk($.i18n.map.sky_notconnected);
                                                                   }
                                                                   else{

                                                                   }
                                                                   },
                                                                   failure: function(){
                                                                   hideActivityIndicator();
                                                                   }

                                                                   })

                                                    },
                                                    /* 583152 -filling data when back from terms & condition */
                                                    validateCheckBox: function(e) {
                                                    var forid = e.target.id;//$(e.target).data("for")
                                                    if(self.$el.find(e.target).prop('checked')){
                                                    self.$el.find('span[data-for ="'+forid+'"]').removeClass('has-error');
                                                    }
                                                    else{
                                                    self.$el.find('span[data-for ="'+forid+'"]').addClass('has-error');
                                                    }
                                                    },
                                                    validate:function(){
                                                    var isValid = true;
                                                    var airportText='';
                                                    airportText = self.$el.find('input[data-key ="TipAirportCode"]').val();
//                                                    if(hasValue(airportText) && airportText.indexOf(this.model.get('TipAirportCode')) > -1 && airportText === this.model.get('TipAirportName')){
//                                                    if(hasValue(this.model.get('TipAirportName'))){
//                                                    self.$el.find('input[data-key ="TipAirportCode"]').removeClass("has-error");
//                                                    }
//                                                    else{
//                                                    self.$el.find('input[data-key ="TipAirportCode"]').addClass("has-error");isValid = false;
//                                                    }
//                                                    if(hasValue(this.model.get('TipAirportCode')) && this.model.get('TipAirportCode').length === 3){
//                                                    self.$el.find('input[data-key ="TipAirportCode"]').removeClass("has-error");
//                                                    }
//                                                    else{
//                                                    self.$el.find('input[data-key ="TipAirportCode"]').addClass("has-error");isValid = false;
//                                                    }
//                                                  }
//                                                  else{
//                                                  self.$el.find('input[data-key ="TipAirportCode"]').addClass("has-error");isValid = false;
//                                                  }
                                                     var validateResponse = validateSkyTipsAirportTextField(airportText);
                                                     if(validateResponse.isValid){
                                                          self.$el.find('input[data-key ="TipAirportCode"]').removeClass("has-error");
                                                     }
                                                     else{
                                                         self.$el.find('input[data-key ="TipAirportCode"]').addClass("has-error");
                                                         isValid = false;
                                                     }
                                                    if(hasValue(this.model.get('TipTheme')) &  this.model.get('sky_theme_select') !== this.model.get('TipTheme')){
                                                    self.$el.find('#ThemeSelect').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('#ThemeSelect').addClass("has-error");isValid = false;
                                                    }
                                                    if(hasValue(this.model.get('TipTitle'))){                                                    self.$el.find('input[data-key ="TipTitle"]').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('input[data-key ="TipTitle"]').addClass("has-error");isValid = false;
                                                    }
                                                    if(hasValue(this.model.get('TipDescription'))){                                                    self.$el.find('textarea[data-key ="TipDescription"]').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('textarea[data-key ="TipDescription"]').addClass("has-error");isValid = false;
                                                    }
                                                    if(hasValue(this.model.get('FirstName'))){                                                    self.$el.find('input[data-key ="FirstName"]').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('input[data-key ="FirstName"]').addClass("has-error");isValid = false;
                                                    }
                                                    if(hasValue(this.model.get('LastName'))){                                                    self.$el.find('input[data-key ="LastName"]').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('input[data-key ="LastName"]').addClass("has-error");isValid = false;
                                                    }
                                                    var regexEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
                                                    var inputedMail = this.model.get('Email');
                                                    if(hasValue(inputedMail) && regexEmail.test(inputedMail)){                                                    self.$el.find('input[data-key ="Email"]').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('input[data-key ="Email"]').addClass("has-error");isValid = false;
                                                    }
                                                    if(hasValue(this.model.get('Country'))){
                                                    self.$el.find('input[data-key ="Country"]').removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('input[data-key ="Country"]').addClass("has-error");isValid = false;
                                                    }
                                                    if(self.$el.find('#checkone').prop('checked')){
                                                    self.$el.find('#checkone').parent().children("span").removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('#checkone').parent().children("span").addClass("has-error");isValid = false;
                                                    }
                                                    if(self.$el.find('#checktwo').prop('checked')){
                                                    self.$el.find('#checktwo').parent().children("span").removeClass("has-error");
                                                    }
                                                    else{
                                                    self.$el.find('#checktwo').parent().children("span").addClass("has-error");isValid = false;
                                                    }
                                                    return isValid;

                                                    },
                                                    dataChange:function(model){
                                                       if(model.hasChanged('TipAirportCode')){
                                                               var inputedValue = model.get("TipAirportCode");
                                                               if(hasValue(inputedValue) && inputedValue.length === 3){
                                                               self.$el.find('input[data-key ="TipAirportCode"]').removeClass("has-error");
                                                               }
                                                               else{
                                                                       if(self.$el.find('input[data-key ="TipAirportCode"]').val() !==uiValue.item.value){
                                                                                                                               self.$el.find('input[data-key ="TipAirportCode"]').addClass("has-error");
                                                                       }
                                                               }
                                                        }
                                                       if(model.hasChanged('TipAirportName')){
                                                               var inputedValue = model.get("TipAirportName");
                                                               if(hasValue(inputedValue)){
                                                               self.$el.find('input[data-key ="TipAirportCode"]').removeClass("has-error");
                                                               }
                                                               else{
                                                               self.$el.find('input[data-key ="TipAirportCode"]').addClass("has-error");
                                                               }
                                                        }
                                                       if(model.hasChanged('TipTheme')){
                                                               var inputedValue = model.get("TipTheme");
                                                               if(hasValue(inputedValue) &  model.get("sky_theme_select") !== inputedValue){
                                                               self.$el.find('#ThemeSelect').removeClass("has-error");
                                                               }
                                                               else{
                                                               self.$el.find('#ThemeSelect').addClass("has-error");
                                                               }
                                                        }
                                                       if(model.hasChanged('TipTitle')){
                                                               var inputedValue = model.get("TipTitle");
                                                               if(hasValue(inputedValue)){
                                                               self.$el.find('input[data-key ="TipTitle"]').removeClass("has-error");
                                                               }
                                                               else{
                                                               self.$el.find('input[data-key ="TipTitle"]').addClass("has-error");
                                                               }
                                                        }

                                                       if(model.hasChanged('TipDescription')){
                                                               var inputedValue = model.get("TipDescription");
                                                               if(hasValue(inputedValue)){
                                                               self.$el.find('textarea[data-key ="TipDescription"]').removeClass("has-error");
                                                               }
                                                               else{
                                                               self.$el.find('textarea[data-key ="TipDescription"]').addClass("has-error");
                                                               }
                                                        }

                                                       if(model.hasChanged('FirstName')){

                                                               var inputedValue = model.get("FirstName");
                                                               if(hasValue(inputedValue)){
                                                               self.$el.find('input[data-key ="FirstName"]').removeClass("has-error");
                                                               }
                                                               else{
                                                               self.$el.find('input[data-key ="FirstName"]').addClass("has-error");
                                                               }
                                                        }
                                                       if(model.hasChanged('LastName')){

                                                               var inputedValue = model.get("LastName");
                                                               if(hasValue(inputedValue)){
                                                               self.$el.find('input[data-key ="LastName"]').removeClass("has-error");
                                                               }
                                                               else{
                                                               self.$el.find('input[data-key ="LastName"]').addClass("has-error");
                                                               }
                                                        }
                                                       if(model.hasChanged('Email')){

                                                               var inputedValue = model.get("Email");

                                                               // Matches a valid email address (e.g. mail@example.com)
                                                               var regexEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;

                                                           if(hasValue(inputedValue) && regexEmail.test(inputedValue)){
                                                           self.$el.find('input[data-key ="Email"]').removeClass("has-error");
                                                           }
                                                           else{
                                                           self.$el.find('input[data-key ="Email"]').addClass("has-error");
                                                           }
                                                       }
                                                        if(model.hasChanged('Country')){
                                                            var inputedValue = model.get("Country");
                                                            var regex=/[@&%?0-9]/;/*For checking the digits and special characters(@,&,%,?) in country name*/

                                                            if(hasValue(inputedValue) && (!regex.test(inputedValue))){
                                                            self.$el.find('input[data-key ="Country"]').removeClass("has-error");
                                                            }
                                                            else{
                                                            self.$el.find('input[data-key ="Country"]').addClass("has-error");
                                                            }
                                                        }
                                                    }


                                                    });
       return skytipsAddNewView;
       });

