define(['jquery', 'backbone', 'underscore','text!template/skyTipSingleElement.html','models/skytips/skyTipResultModel','text!template/skyTipsResultsMVVMTemplate.html','appView','text!template/skyTipResultPopup.html'],
       function($, Backbone, _, skyTipSingleElement,skyTipResults,skyTipsResultsMVVMTemplate,appView,skyTipResultPopup) {


       var that;
       var skyTipsPopup = Backbone.View.extend({
                                               el : '#skyteam',
                                               initialize : function() {
                                               that=this;
                                               },
                                               events:{
                                               'click #selectall' : 'checkingThemes',
                                               'click .themes-ok' : 'closeThemesPopup',
                                               'click .groupCheckbox' : 'uncheckSelectAllCheckBox',
                                               'click .themeName' : 'checkThemeSelected',
                                               'click :input[type="checkbox"]':'checkingInputCheckBox',
                                               },
                                               render:function(){
                                               var template = _.template(skyTipResultPopup,this.model.toJSON());
                                               $('#blackshade').show();
                                               $(that.el).append(template);
                                               that.$el.find('#tipThemesSelect').show();
                                               that.$el.find('.themes-ok').off('click').on('click',function(e){
                                                                                            that.closeThemesPopup(e);
                                                                                            });
                                               that.$el.find('.themeName').off('click').on('click',function(e){
                                                          that.checkThemeSelected(e);
                                               });
                                               },
                                               /* function to select/deselect all the themes */
                                               checkingThemes: function(e) {

                                                   if ( that.$el.find('#selectall').prop('checked') ) { // check select status
                                                       that.$el.find('.groupCheckbox').each( function() {  // loop through each checkbox
                                                                this.checked = true;   // select all checkboxes with class "groupCheckbox"
                                                                });
                                                   } else {
                                                       that.$el.find('.groupCheckbox').each( function() {  // loop through each checkbox
                                                                this.checked = false;  // deselect all checkboxes with class "groupCheckbox"
                                                                });
                                                   }
                                               },
                                               /* function to uncheck the select all check box when any of the checkbox is unchecked */
                                               uncheckSelectAllCheckBox: function(e) {
                                                   if ( this.$el.find(e.target).prop('checked') === false ) {
                                                   this.$el.find('#selectall').prop('checked', false);
                                                   }
                                               },
                                               /* function to check the checkbox(theme) when corresponding label(theme name) is tapped */
                                               checkThemeSelected: function(e) {
                                                    if(e){
                                                       e.preventDefault();
                                                       e.stopImmediatePropagation();
                                                    }
                                                   var forid ='#' + this.$el.find(e.target).attr('for');
                                                   if ( this.$el.find(forid).prop('checked') ) {
                                                   this.$el.find(forid).prop("checked", false);
                                                   this.$el.find('#selectall').prop('checked', false);
                                                   } else {
                                                   this.$el.find(forid).prop("checked", true);

                                                   /* logic to check select all when all other check boxes are checked */
                                                   var checkBoxGroup = this.$el.find('.groupCheckbox');
                                                   var checkedElementCount = 0;
                                                   _.each(checkBoxGroup,function(element,index){
                                                          if($(element).prop("checked")){
                                                          checkedElementCount++;
                                                          }
                                                          });
                                                   if(checkedElementCount === checkBoxGroup.length){
                                                   this.$el.find('#selectall').prop('checked', true);
                                                   }
                                                   /* logic to check select all when all other check boxes are checked */
                                                   }
                                                   if ( forid === '#selectall' ) {
                                                   this.checkingThemes();
                                                   }
                                                   e.preventDefault();
                                               },

                                               checkingInputCheckBox:function(e){

                                               var forid = e.target.id;
                                               if($(e.target).prop('checked')){//if new check box has been checked
                                               if ( forid === 'selectall' ) {//if select all has checked
                                               this.checkingThemes();
                                               }
                                               else{//any other checkbok checked
                                               /* logic to check select all when all other check boxes are checked */
                                               var checkBoxGroup = this.$el.find('.groupCheckbox');
                                               var checkedElementCount = 0;
                                               _.each(checkBoxGroup,function(element,index){
                                                      if($(element).prop("checked")){
                                                      checkedElementCount++;
                                                      }
                                                      });
                                               if(checkedElementCount === checkBoxGroup.length){
                                               this.$el.find('#selectall').prop('checked', true);
                                               }
                                               /* logic to check select all when all other check boxes are checked */
                                               }
                                               }

                                               },



                                               closeThemesPopup:function(e){
                                               e.preventDefault();
                                               e.stopImmediatePropagation();

                                               var checkedValues  =[];
                                               checkedValues = that.$el.find('input:checkbox.groupCheckbox:checked');

                                               if(checkedValues.length !== 0 ){
                                               that.$el.find('#tipThemesSelect').hide();
                                               showActivityIndicator($.i18n.map.sky_please);
                                               setTimeout(function(){
                                               that.$el.find("#tipThemesSelect").addClass("degradeZindex");
                                               that.$el.find('#tipThemesSelect').hide();
                                               $("#blackshade").hide();
                                               var selectedThemeIds =[];
                                               for(var i=0,len=checkedValues.length;i<len;i++){
                                                var selectId= $(checkedValues[i]).val();
                                               selectedThemeIds.push(selectId);
                                               }

                                               var db= TAFFY(JSON.parse(localStorage.skyTipsDetails.split('?').join('').split('�').join('')).SkyTips);
                                               var tips=db().filter({'ThemeId':selectedThemeIds}).get();
                                               sessionStorage.themeSelected = JSON.stringify(selectedThemeIds);
                                               that.model.set({'Count':tips.length,'SelectedTheme':selectedThemeIds,'SkyTips':tips});
                                               that.$el.find("#tipThemesSelect").removeClass("degradeZindex");
                                               hideActivityIndicator();
                                               that.$el.find('#tipThemesSelect').remove();
                                               },10);

                                               }else {
                                               that.$el.find("#tipThemesSelect").addClass("degradeZindex");
                                               openAlertWithOk( $.i18n.map.sky_theme_error_msg );
                                               }


                                               }
                                               });
       // View for each Sky TIP
       var singleTipElementView = Backbone.View.extend({

                                                       tagName: "ul",

                                                       render: function(data) {

                                                       if(!data.TipOriginalLanguage){
                                                       data.TipOriginalLanguage = "En";
                                                       }
                                                       var tipTemplate = _.template(skyTipSingleElement,data);
                                                       this.$el.html(tipTemplate);
                                                       return this;
                                                       }

                                                       });



       // View for each Sky TIP
       var skyTipsResultView = appView.extend({
                                                    //el : '#skyteam',
                                                    tagName: "ul",
                                                    initialize : function() {

                                                    $('#backButtonList').show();
                                                    $('#headingText h2').html( $.i18n.map.sky_tips );
                                                    $('#headingText h2').css('margin-right','37px');

                                                   var response = JSON.parse(localStorage.skyTipsDetails.split("�").join('').split("?").join(''));

                                                    var resultModel = new skyTipResults(response);
                                                    resultModel.set({'Airportname':sessionStorage.skyTipsAirportName});
                                                    resultModel.set($.i18n.map);

                                                    this.model = resultModel;//setting the created model to the current model
                                                    var db= TAFFY(JSON.parse(localStorage.skyTipsDetails.split('?').join('').split('�').join('')).SkyTips);

                                                    /*  Moving the theme Other to the end*/
                                                    var tipThemeIds = db().distinct("ThemeId");
                                                    /*  Moving the theme Other to the end*/
                                                    if(_.contains(tipThemeIds, "17")){
                                                    var elementIndex = tipThemeIds.indexOf("17");
                                                    (tipThemeIds).splice(elementIndex, 1);
                                                    (tipThemeIds).push("17");
                                                    }
                                                    /*  Moving the theme Other to the end*/


                                                    this.model.set({'ThemeIds':tipThemeIds});
                                                    this.model.set({'SelectedTheme':tipThemeIds});

                                                    if((sessionStorage.hasOwnProperty("themeSelected")) && sessionStorage.themeSelected !== ''){
                                                    var tipThemeIds =JSON.parse(sessionStorage.themeSelected);
                                                    var tipThemeNames = [];

                                                    var tips=db().filter({'ThemeId':tipThemeIds}).get();
                                                    this.model.set({'Count':tips.length,'SkyTips':tips,'SelectedTheme':tipThemeIds});
                                                    }

                                                    //this.model.on("change", this.render,this);
                                                    this.listenTo( this.model,'change', this.render);
                                                    sessionStorage.currentPage = "skyTipsResults";
                                                    },
                                                    render: function() {

                                                    hideActivityIndicator();
                                                    var compiledTemplate = _.template(skyTipsResultsMVVMTemplate,this.model.toJSON());

                                                    this.$el.html(compiledTemplate);
                                                    var skytipsCollection = this.model.toJSON().SkyTips;
                                                    if(_.isArray(skytipsCollection)){
                                                    _.each(skytipsCollection,this.addTip,this);
                                                    }
                                                    else{
                                                    this.addTip(skytipsCollection);
                                                    }
                                                    this.$el.find('.airports_tipsFor').html(this.model.get("sky_for") +"  "+ getSetOfTipThemeName(this.model.get("SelectedTheme")).toString().replace(/\,/g,', <wbr>'));
                                                    /**Implementation of Back button functionality **/
                                                    $('.main_header ul li:eq(1) img')
                                                    .off('click')
                                                    .on('click', function(e) {
                                                        e.preventDefault();
                                                        e.stopImmediatePropagation();
                                                        if ( $('#pageslide').css('display') === "block" ) {

                                                        $.pageslide.close();
                                                        } else {
                                                        if ( sessionStorage.airportFlag === 0 ) {
                                                        sessionStorage.fromSkytipsResults = "yes";
                                                        }
                                                        var currentobj = JSON.parse( sessionStorage.previousPages );
                                                        if( currentobj[0].previousPage === "airportDetails" || currentobj[0].previousPage === "airportDetailsViewViaMap" ) {
                                                        showActivityIndicatorForAjax('false');
                                                        sessionStorage.airportDetailsObj = currentobj[0].airportObj;
                                                        } else {
                                                        showActivityIndicator( $.i18n.map.sky_please );
                                                        }
                                                        window.location.href = "index.html#" + currentobj[0].previousPage;
                                                        popPageOnToBackStack();
                                                        sessionStorage.savedflights = "no";
                                                        $('.main_header ul li:eq(1) img').off('click');
                                                        }
                                                        });

                                                    /**End of Back button functionality **/
                                                    //_.bindAll(this,this.openThemes);
                                                    //$('#skyteam').html(this.$el.html());
                                                    return appView.prototype.render.apply(this, arguments);

                                                    },
                                                    events:{
                                                    'click #ThemeSelect':'openThemes',

                                                    'touchstart *' : function() {
                                                    $.pageslide.close();
                                                    }
                                                    },
                                                    addTip: function(tip) {
                                                    var tipElement = new singleTipElementView ();
                                                    this.$el.find('.tips_msgs').append(tipElement.render(tip).el);
                                                    },
                                                    openThemes:function(){
                                                     new skyTipsPopup({model:this.model}).render();
                                                     },
                                              });

       return skyTipsResultView;


       });
