define(['jquery','underscore','backbone'],
		function($, _, Backbone){


	// model for an skytip
	var singleTipModel=Backbone.Model.extend({
		defaults:{
		Count:0,
		AirportName:'AMS'
		}

	});

	/*skytipModel.skytipCollection = Backbone.Collection.extend({
 	   model: skytipModel.singleTipModel

  });*/


	return singleTipModel;
});