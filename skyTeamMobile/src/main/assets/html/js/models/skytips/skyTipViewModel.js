define(['jquery','underscore','backbone'], 
		function($, _, Backbone){
	

	// model for an skytip
	var singleTipModel=Backbone.Model.extend({
		defaults:{
		AirportName:'',
        AirportCode:'',
		}
		
	});
	
	/*skytipModel.skytipCollection = Backbone.Collection.extend({
 	   model: skytipModel.singleTipModel
 		  
  });*/

	
	return singleTipModel;
});