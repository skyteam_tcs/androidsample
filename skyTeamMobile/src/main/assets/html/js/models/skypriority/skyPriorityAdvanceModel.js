define(['jquery','underscore','backbone'],
       function($, _, Backbone){
       
       
       // model for an skytip
       var skyPriorityResultModel=Backbone.Model.extend({
                                                        defaults:{
                                                        AirportName:'',
                                                        AirportCode:'',
                                                        TripDestinationFeatures:{"OperatingCarrier":'',isAvail:false,ErrorCode:''},
                                                        TripOriginFeatures:{isAvail:false,ErrorCode:'',},
                                                        isDisclaimerShown:false,
                                                        isHavingTransferAirports:false
                                                        },
                                                        setDestinationCarrier:function(){
                                                        /* getting non skyteam airline from flight5 search*/
                                                        var previousPages= JSON.parse(sessionStorage.previousPages);
                                                        var operatingAirlineNames = [];
                                                        if("FlightDetailsPage" === previousPages[0].previousPage){
                                                        flightDetails = JSON.parse(sessionStorage.flightDetailsObject);
                                                        }
                                                       // else if("flightStatusDetails" === previousPages[0].previousPage){
                                                       else {
                                                        flightDetails = JSON.parse(sessionStorage.flightStatusObj);
                                                        }
                                                        
                                                        if("array" === $.type(flightDetails.details.flights)){
                                                        $.each(flightDetails.details.flights,function(index,flight){
                                                               operatingAirlineNames.push(flight.operatingAirlineCode)
                                                               });
                                                        }
                                                        else{

                                                        if(typeof flightDetails.details.flights  == 'undefined') {
                                                        operatingAirlineNames.push(flightDetails.details.Segments.OperatorCode);
                                                        }else{
                                                          operatingAirlineNames.push(flightDetails.details.flights.operatingAirlineCode);
                                                        }

                                                        }
                                                        /* getting non skyteam airline from flight search*/
                                                        
                                                        /* assigning airline name to source and destination*/
                                                        this.attributes.TripDestinationFeatures.OperatingCarrierName= operatingAirlineNames[operatingAirlineNames.length-1];
                                                        //poping up the airline name
                                                        //operatingAirlineNames.pop();
                                                        
                                                        this.attributes.TripOriginFeatures.OperatingCarrierName = operatingAirlineNames[0];
                                                        //poping up the first airline name
                                                        operatingAirlineNames.shift();
                                                        /* assigning airline name for source and destination*/
                                                        
                                                        
                                                        
                                                        
                                                        /*if transfer airport
                                                         assging last tranfer operating career to destination
                                                         else
                                                         assging orgin operating career to destination
                                                         */
                                                        if(this.has("TransferAirportFeatures")){
                                                        this.attributes.TripDestinationFeatures.OperatingCarrier = this.attributes.TransferAirportFeatures[this.attributes.TransferAirportFeatures.length-1].OperatingCarrier;
                                                        this.attributes.isHavingTransferAirports = true;
                                                        for(var i=0,len=operatingAirlineNames.length;i<len;i++){
                                                        var j=2*i;
                                                        this.attributes.TransferAirportFeatures[j].OperatingCarrierName = operatingAirlineNames[i];
                                                        this.attributes.TransferAirportFeatures[j+1].OperatingCarrierName = operatingAirlineNames[i];
                                                        
                                                        }
                                                        
                                                        }
                                                        else{
                                                        
                                                        this.attributes.TripDestinationFeatures.OperatingCarrier = this.attributes.TripOriginFeatures.OperatingCarrier;
                                                        
                                                        }
                                                        
                                                        
                                                        
                                                        //set TripOriginFeatures.isAvail true if any orgin features is Y
                                                        if(this.attributes.TripOriginFeatures.ErrorCode != "101"){
                                                        var orginFeatures = this.attributes.TripOriginFeatures;
                                                        for(var key in orginFeatures){
                                                        if(orginFeatures[key] === "Y"){
                                                        this.attributes.TripOriginFeatures.isAvail = true;
                                                        }
                                                        }
                                                        }
                                                        //set TripOriginFeatures.isAvail true if any orgin features is Y
                                                        if(this.attributes.TripDestinationFeatures.ErrorCode != "101"){
                                                        var destinationFeatures = this.attributes.TripDestinationFeatures;
                                                        for(var key in destinationFeatures){
                                                        if(destinationFeatures[key] === "Y"){
                                                        this.attributes.TripDestinationFeatures.isAvail = true;
                                                        }
                                                        }
                                                        }
                                                        
                                                        //checking for the disclaimer msg property
                                                        if(this.attributes.TripOriginFeatures.ErrorCode != "101"){
                                                        var orginFeatures = this.attributes.TripOriginFeatures;
                                                        
                                                        if(orginFeatures["SecurityFasttrackAccessibility"] || orginFeatures["ImmigrationFasttrackAccessibility"]=== "0"){
                                                        this.attributes.DisclaimerShown = true;
                                                        }
                                                        else{
                                                        
                                                        if(this.has("TransferAirportFeatures")){
                                                        var transferAirports = this.attributes.TransferAirportFeatures;
                                                        for(var airport in transferAirports){
                                                        if(airport["SecurityFasttrackAccessibility"] === "0" || airport["ImmigrationFasttrackAccessibility"] === "0"){
                                                        this.attributes.DisclaimerShown = true;
                                                        break;
                                                        }
                                                        }
                                                        }
                                                        
                                                        }
                                                        
                                                        }
                                                        }
                                                        
                                                        });
       //var myModel = new skyPriorityResultModel(JSON.parse(sessionStorage.SPAdvance_response));
       
       /*skytipModel.skytipCollection = Backbone.Collection.extend({
        model: skytipModel.singleTipModel
        
        });*/
       
       
       return skyPriorityResultModel;
       });