define(['jquery','underscore','backbone'],
		function($, _, Backbone){


	// model for an skytip
	var skyPriorityDetailModel=Backbone.Model.extend({
		defaults:{
		AirportName:'',
        AirportCode:'',
        PriorityAirlineCode:'',
        isDisclaimerShown:false,
        isAvail:false,
        PriorityCheckIn: "N",
        PriorityBaggageDropOff: "N",
        PriorityImmigration: "N",
        ImmigrationFasttrackAccessibility: "1",
        PrioritySecurityLines: "N",
        SecurityFasttrackAccessibility: "1",
        PriorityTicketDesk: "N",
        PriorityTransferDesk: "N",
        PriorityBoarding: "N",
        BaggageFirst: "N"
		},
        resetData:function(){
                if(this.has('ErrorCode') ){
                     this.set({'isAvail':false});
                }
                else if(this.get('PriorityCheckIn') === "N" && this.get('PriorityBaggageDropOff') === "N" && this.get('PriorityImmigration') === "N" && this.get('PrioritySecurityLines') === "N" &&  this.get('PriorityTicketDesk') === "N" && this.get('PriorityTransferDesk') === "N" && this.get('PriorityBoarding') === "N" && this.get('BaggageFirst') === "N" ){
                     this.set({'isAvail':false});
                }
                else{
                     this.set({'isAvail':true});
                }
                                                     if(this.get('ImmigrationFasttrackAccessibility') === '0' || this.get('SecurityFasttrackAccessibility') === '0'){
                                                     this.set({'isDisclaimerShown':true})
                                                     }
        }

	});

	return skyPriorityDetailModel;
});