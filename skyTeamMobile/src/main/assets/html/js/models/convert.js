define([
	'jquery',
	'underscore',
	'backbone',
],
function($, _, Backbone) {
	var objectModel = Backbone.Model.extend({
		convert : function(data) {
			for (key in data) {
				if (data[key] instanceof Object) {
					/* To Create a Model or Collection*/
					if (data[key].length) {
						/* To Create a Collection */
						var collection = new collectionModel(data[key]);
						this.set(key, collection);
						collection.forEach(function(method) {
							method.convert(method.toJSON());
						});
					} else {
						/* To Create a Model */
						var model = new objectModel(data[key]);
						this.set(key, model);
						model.convert(model.toJSON());
					}
				}
			}
		}
	});
	var collectionModel = Backbone.Collection.extend({
		model : objectModel
	});

	// return{
	// objectModel: objectModel,
	// collectionModel: collectionModel
	// };
	var convertResponse = function(data, obj) {
		// if(obj.attributes){
		for (key in data) {
			if (data[key] instanceof Object) {
				/* To Create a Model or Collection*/
				if (data[key].length) {
					/* To Create a Collection */
					var collection = new collectionModel(data[key]);
					obj.set(key, collection);
					collection.forEach(function(method) {
						method.convert(method.toJSON());
					});
				} else {
					/* To Create a Model */
					var model = new objectModel(data[key]);
					obj.set(key, model);
					model.convert(model.toJSON());
				}
			} else {
				/* To set a attribute of var type */
				obj.set(key, data[key]);
			}
		}
		
		return obj;
	}
	var convertToJSON = function(data) {

	}
	return convertResponse;

});
