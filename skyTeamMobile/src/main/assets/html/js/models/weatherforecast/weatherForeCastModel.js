define(['jquery','underscore','backbone','utilities','serviceInteraction'],
		function($, _, Backbone,utilities,serviceInteraction){
	
       var weather_Image={
       "01d": "Weather_Sunny_b",
       "02d": "Weather_PartlyCloud_d_b",
       "03d":"Weather_PartlyCloud_d_b" ,
       "04d":"Weather_PartlyCloud_d_b",
       "09d":"Weather_Rainy_b",
       "10d":"Weather_Rainy_b",
       "11d":"thunder_storms_b",
       "13d":"Weather_Snow_b",
       "50d":"Weather_Fog_b",
       "01n": "Weather_Sunny_b",
       "02n": "Weather_PartlyCloud_d_b",
       "03n":"Weather_Cloudy_b" ,
       "04n":"Weather_Cloudy_b",
       "09n":"Weather_Rainy_b",
       "10n":"Weather_Rainy_b",
       "11n":"thunder_storms_b",
       "13n":"Weather_Snow_b",
       "50n":"Weather_Fog_b",
       
       }
	// model for an skytip
	var weatherForecast=Backbone.Model.extend({
                                                
        defaults:{
            AirportCode:'',
            AirportName:'',
            WeatherDetails:[],
            ErrorCode:'',
            TemperatureType:'C'
        },
        resetData:function(){
              if(localStorage.temparature == "fahrenheit") {
              this.attributes.TemperatureType = "F";
              }
            _.each(this.attributes.WeatherDetails,function(weather){
                   var dateString,yearIndex ;
                   dateString = getDateInLocaleFormat(weather.Date);
                   if(localStorage.date !== "YYYY"){
                   yearIndex = dateString.lastIndexOf(" ");
                   weather["LocalDate"] =dateString.substr(0,yearIndex);
                   }
                   else{
                   weather["LocalDate"] =dateString;
                   }
                   
                   
                   weather["Image"] = weather_Image[weather.WeatherCode];
                   if(localStorage.temparature == "fahrenheit") {
                   weather["MinTemp"] = Math.round(temparatureConvertor(weather["MinTemp"]));
                   weather["MaxTemp"] = Math.round(temparatureConvertor(weather["MaxTemp"]));
                   }
                   else{
                   weather["MinTemp"] = Math.round(weather["MinTemp"]);
                   weather["MaxTemp"] = Math.round(weather["MaxTemp"]);
                   }
                   
                   });
        }
		
	});
	
	

	
	return weatherForecast;
});