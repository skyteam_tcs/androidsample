define(['jquery','underscore','backbone'],
       function($, _, Backbone){
        var flightDetails = Backbone.Model.extend({
                                                 defaults:{
                                                 Origin:'',
                                                 Destination:'',
                                                 DepartureDate:'',
                                                 ArrivalDate:'',
                                                 noOfStops:0,
                                                 CurrentDetail:[],



                                                 },


                                                 resetData:function(){
                                                  var details = this.get('details');
                                                // sky priority upgradation fix


						if(details.flights)
                                                  {

var stopsCount;


                                                   if(details.totalNoTransfers){
                                                  var stopsCount = parseInt(details.totalNoTransfers);
                                                  }
                                                  else{
                                                  stopsCount = 0;
                                                  }
                                                                                   this.attributes.noOfStops = stopsCount;

                                           //below scenario will not occur as transfer time is always comes as array - boopathy


//                                                 if(this.attributes.noOfStops > 0){
//                                                     //Converting StopTimes to  array ,if its string
//                                                     if(typeof(details.StopTimes)=='string'){
//
//                                                      this.attributes.details.StopTimes =[details.StopTimes];
//                                                     }
//                                                 }
                                                  if(!_.isArray(details.flights)){
                                                  details.flights = $.makeArray(details.flights);
                                                  this.attributes.details.flights = details.flights;
                                                  }
                                                 /* Arrival departure dates */
                                                 this.attributes.DepartureDate = details.flights[0].flightLegs[0].departureDetails.scheduledDate;
                                                 this.attributes.ArrivalDate = details.flights[stopsCount].flightLegs[details.flights[stopsCount].flightLegs.length-1].arrivalDetails.scheduledDate;

                                                 /*Arrival & departure airport code */

                                                 this.attributes.Origin = details.flights[0].origin.airportCode;
                                                 this.attributes.Destination = details.flights[stopsCount].destination.airportCode;


                                                  }else{


  var stopsCount = parseInt(details.NoOfStops);
                                                  this.attributes.NoOfStops = stopsCount;
                                                  if(this.attributes.NoOfStops > 0){
                                                  //Converting StopTimes to  array ,if its string
                                                  if(typeof(details.StopTimes)=='string'){

                                                  this.attributes.details.StopTimes =[details.StopTimes];
                                                  }
                                                  }
                                                  if(!_.isArray(details.Segments)){
                                                  details.Segments = $.makeArray(details.Segments);
                                                  this.attributes.details.Segments = details.Segments;
                                                  }

                                                  /* Arrival departure dates */
                                                   this.attributes.DepartureDate = details.Segments[0].DepartureDate;
                                                  this.attributes.ArrivalDate = details.Segments[stopsCount].ArrivalDate;

                                                   /*Arrival & departure airport code */
                                                     this.attributes.Origin = details.Segments[0].Origin;
                                                   this.attributes.Destination = details.Segments[stopsCount].Destination;




                                                 }






                                                 }


       });

       return flightDetails
                                             });