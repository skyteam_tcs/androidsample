define([
	'jquery',
	'underscore',
	'backbone',
	'models/convert',
	'models/responses/airportNames',
	'text!template/searchFlightList.html'
],function($, _, Backbone, build, response, searchPageTemplate){
	var searchPageModel = Backbone.Model.extend({
		populateSearchPageData : function(data) {
			 localStorage.serviceResponse = JSON.stringify(response.airportNames);
			 return build(response,this);
			//return build(data,this);
		}
	});
	return searchPageModel;
});
