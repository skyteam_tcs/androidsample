define(['jquery','underscore','backbone'],
       function($, _, Backbone){
       /* logic to get the current time zone */
       var currentTimeZoneOffSet = new Date().getTimezoneOffset();
       var currentTimeZone,HHdiff,MMdiff;
       if(currentTimeZoneOffSet<0){
       currentTimeZone ="+";
       currentTimeZoneOffSet = -(currentTimeZoneOffSet);
       }
       else{
       currentTimeZone ="-";
       }
       HHdiff = parseInt(currentTimeZoneOffSet/60);
       MMdiff = currentTimeZoneOffSet%60;
       if(HHdiff <10){//converting to 2 digit
       HHdiff = "0"+HHdiff;
       }
       if(MMdiff<10){//converting to 2 digit
       MMdiff = "0"+MMdiff;
       }
       
       currentTimeZone = currentTimeZone+HHdiff+":"+MMdiff;
       /* private function for restting the list*/
       var resetList =function(list){
       for(var i=0,len=list.length;i<len;i++){
       if("En" !== localStorage.language){
       list[i].runOnDays = getRunOnDaysInLocale(list[i].runOnDays);
       list[i].totalDuration = getTravelTime(list[i].totalDuration);
       }
       //For single stops ,if Segments is an object then converting to an array
       if(list[i].totalNoTransfers === '0' && (!_.isArray(list[i].flights)) ){
       list[i].flights = $.makeArray(list[i].flights);
       }
       }
       for(var i=0,len=list.length;i<len;i++){
       var currentObject = list[i];
       currentObject.FlightNumbers='';
       var noOfSegments = list[i].flights.length;
       var segIterator =0;
       do{
       /* ArrivalDateAndTime & DepartureDateAndTime will de used for sorting sorting results based on date and time,
        The string is complaint with ISO8601 date format string
        (YYYY-MM-DDTHH:MM:SS-"TIME ZONE")
        */
       currentObject.flights[segIterator].ArrivalDateAndTime = currentObject.flights[segIterator].flightLegs[currentObject.flights[segIterator].flightLegs.length-1].arrivalDetails.scheduledDate+"T"+currentObject.flights[segIterator].flightLegs[currentObject.flights[segIterator].flightLegs.length-1].arrivalDetails.scheduledTime+currentTimeZone;
       currentObject.flights[segIterator].DepartureDateAndTime =currentObject.flights[segIterator].flightLegs[0].departureDetails.scheduledDate+"T"+currentObject.flights[segIterator].flightLegs[0].departureDetails.scheduledTime+currentTimeZone;
       //removing seconds from departure and arrival time
       currentObject.flights[segIterator].flightLegs[0].departureDetails.scheduledTime = currentObject.flights[segIterator].flightLegs[0].departureDetails.scheduledTime.slice(0,5);
       currentObject.flights[segIterator].flightLegs[currentObject.flights[segIterator].flightLegs.length-1].arrivalDetails.scheduledTime = currentObject.flights[segIterator].flightLegs[currentObject.flights[segIterator].flightLegs.length-1].arrivalDetails.scheduledTime.slice(0,5);
       /* converting arrival and departure date in required format */
       //if(localStorage.date !== "YYYY"){
       currentObject.flights[segIterator].ArrivalDateInLocale = getDateInLocaleFormat(currentObject.flights[segIterator].flightLegs[currentObject.flights[segIterator].flightLegs.length-1].arrivalDetails.scheduledDate,true);
       currentObject.flights[segIterator].DepartureDateInLocale = getDateInLocaleFormat(currentObject.flights[segIterator].flightLegs[0].departureDetails.scheduledDate,true);
       //}
       if(segIterator === 0){
       currentObject.FlightNumbers = currentObject.flights[segIterator].marketingFlightNumber;
       }
       else{
       currentObject.FlightNumbers = currentObject.FlightNumbers+"/"+currentObject.flights[segIterator].marketingFlightNumber;
       }
       segIterator++;
       }while(noOfSegments > segIterator);
       }
       };
       /* private function for restting the list*/
       
       
       // model for an skytip
       var searchFlightresultModel=Backbone.Model.extend({
                                                         defaults:{
                                                         source:'',
                                                         destination:'',
                                                         dest_AirportName:'',
                                                         src_AirportName:'',
                                                         departureDate:'',
                                                         arrivalDate:'',
                                                         Onward:{'Count':0},
                                                         Return:{'Count':0},
                                                         returnFlights:{'Count':0,Connections:[]},
                                                         departureFlights:{'Count':0,Connections:[]},
                                                         journeyType:'oneway',
                                                         currentTravel:"Onward"
                                                         },
                                                         resetData:function(){
                                                          var isOnwardAvail = true;
                                                         var isReturnAvail = false;
                                                         this.set({
                                                                  "source":sessionStorage.source,
                                                                  "src_AirportName":getAirportName(sessionStorage.source),
                                                                  "destination":sessionStorage.destination,
                                                                  "dest_AirportName":getAirportName(sessionStorage.destination),
                                                                  "departureDate":getDateInLocaleFormat(sessionStorage.depDate,true),
                                                                  "arrivalDate":getDateInLocaleFormat(sessionStorage.returndate,true)});
                                                         if(this.get('schedules').onwardSchedules.totalNumberTripOptions == 0){
                                                         isOnwardAvail = false;
                                                         }
                                                         else{
                                                         if(!_.isArray(this.get('schedules').onwardSchedules.trips)){//onWard list is an object then converting to array
                                                         this.attributes.schedules.onwardSchedules.trips =$.makeArray(this.get('schedules').onwardSchedules.trips);
                                                         }
                                                         /* resetting te departure data*/
                                                         var OnwardList = this.get('schedules').onwardSchedules.trips;
                                                         resetList(OnwardList);
                                                         }
                                                         if(typeof this.get('schedules').returnSchedules !='undefined'){
                                                         if(this.get('schedules').returnSchedules.totalNumberTripOptions == 0){
                                                         isReturnAvail = false;
                                                         }
                                                         else{
                                                         isReturnAvail = true;
                                                         /* resetting the arrival data */
                                                         if(this.get('journeyType') === "roundtrip"){
                                                         if(!_.isArray(this.get('schedules').returnSchedules.trips)){//return list is an object then converting to array
                                                         this.attributes.schedules.returnSchedules.trips =$.makeArray(this.get('schedules').returnSchedules.trips);
                                                         }
                                                         var ReturnList = this.get('schedules').returnSchedules.trips;
                                                         resetList(ReturnList);
                                                         }
                                                         }
                                                         }
                                                         if(isOnwardAvail){
                                                         /* start --Filtering departute flights available in the user given date*/
                                                         var onWardList = this.get('schedules').onwardSchedules.trips;
                                                         var departureFlights = [];
                                                         for(var i=0,len = onWardList.length;i<len;i++){
                                                         var trip = onWardList[i];
                                                         if(sessionStorage.depDate === trip.flights[0].flightLegs[0].departureDetails.scheduledDate){
                                                         departureFlights.push(trip);
                                                         }
                                                         }
                                                         this.set({'departureFlights':{'trips':departureFlights,'Count':departureFlights.length}});
                                                         /* end --Filtering departute flights available in the user given date*/
                                                         }
                                                         if(isReturnAvail){
                                                         /* start - Filtering return flights available in the user given date*/
                                                         if(this.get('journeyType') === "roundtrip"){
                                                         var returnList = this.get('schedules').returnSchedules.trips;
                                                         var returnFlights = [];
                                                         for(var i=0,len = returnList.length;i<len;i++){
                                                         var trip = returnList[i];
                                                         if(sessionStorage.returndate === trip.flights[0].flightLegs[0].departureDetails.scheduledDate){
                                                         returnFlights.push(trip);
                                                         }
                                                         }
                                                         this.set({'returnFlights':{'trips':returnFlights,'Count':returnFlights.length}});
                                                         }
                                                         /* end - Filtering return flights available in the user given date*/
                                                         }
                                                         if("Return" === this.get('currentTravel')){
                                                         if(isReturnAvail){
                                                         if(sessionStorage.show7DaysArrival === "yes" || this.get('returnFlights').Count === 0){//all flights
                                                         this.set({'currentList':this.get('schedules').returnSchedules.trips});
                                                         }
                                                         else{//only flights on user give date
                                                         this.set({'currentList':this.get('returnFlights').trips});
                                                         }
                                                         }
                                                         else{
                                                         this.set({'currentList':[]});
                                                         }
                                                         
                                                         }
                                                         else{//default is Onward
                                                         if(isOnwardAvail){
                                                         if(sessionStorage.show7DaysDeparture === "yes" || this.get('departureFlights').Count === 0){
                                                         this.set({'currentList':this.get('schedules').onwardSchedules.trips});
                                                         }
                                                         else{//only flights on user give date
                                                         this.set({'currentList':this.get('departureFlights').trips});
                                                         }
                                                         }
                                                         else{this.set({'currentList':[]});}
                                                         }
                                                         
                                                         }
                                                         
                                                         });
       
       
       return searchFlightresultModel;
       });