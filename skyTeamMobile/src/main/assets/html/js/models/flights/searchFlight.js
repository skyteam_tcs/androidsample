      define([
	'jquery',
	'underscore',
	'backbone',
	'models/convert',
	'models/responses/airportNames',
	'text!template/searchFlightList.html'
],function($, _, Backbone, build, response, searchPageTemplate){
             var searchFlightModel = Backbone.Model.extend({
                defaults: {
                    TransactionID: "MobileApp",
                        },
                    url: function(){
                    return this.instanceUrl;
                            },
                        });
	return searchFlightModel;
});
