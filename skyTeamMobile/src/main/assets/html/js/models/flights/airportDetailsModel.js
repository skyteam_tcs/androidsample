define(['jquery','underscore','backbone'],
       function($, _, Backbone){
       var weather_Image={
       "01d": "Weather_Sunny_b",
       "02d": "Weather_PartlyCloud_d_b",
       "03d":"Weather_PartlyCloud_d_b" ,
       "04d":"Weather_PartlyCloud_d_b",
       "09d":"Weather_Rainy_b",
       "10d":"Weather_Rainy_b",
       "11d":"thunder_storms_b",
       "13d":"Weather_Snow_b",
       "50d":"Weather_Fog_b",
       "01n": "Weather_Sunny_b",
       "02n": "Weather_PartlyCloud_d_b",
       "03n":"Weather_Cloudy_b" ,
       "04n":"Weather_Cloudy_b",
       "09n":"Weather_Rainy_b",
       "10n":"Weather_Rainy_b",
       "11n":"thunder_storms_b",
       "13n":"Weather_Snow_b",
       "50n":"Weather_Fog_b",
       };
        var airportDetail = Backbone.Model.extend({
             defaults:{
              AirportCode:'',
              AirportFullName:'',
              CountryName:'',
              TemperatureInLocalMetric:'',
              Temperature:'',
              TemperatureType:"C",
              GMT:'',
              Time:'',
              LoungesCount:'',
              AirlineDetails:{
                  Operating:{'Count':''},
                  Marketing:{}
              },
              SkyTipsCount:'',
              SkyPriorityFeatures:'Y',
              WeatherType:'',
              WeatherImagePath:'',
              Details:''
              },
             resetData:function(){
              var airportCode = this.get('AirportCode');
              this.set({
              "AirportFullName":getAirportFullName(airportCode),
              "CountryName":getAirportCountryName(airportCode)
              })
              
              if (localStorage.temparature == "fahrenheit") {
                    this.set({'TemperatureType':'F'});
              }
              if(this.has('LocalTime')){
                  var localTime = this.get('LocalTime');
                  var keyIndex =  localTime.indexOf("GMT");
                  this.set({'Time':localTime.substr(0,5)});
                  this.set({'GMT':localTime.substr(6)});
              }
              /* Temperature changes*/
              if(this.get('Temperature')!==''){
              var temperature = parseFloat(this.get('Temperature'));
              if("fahrenheit" === this.get('TemperatureType')){
                  temperature = temparatureConvertor(temperature);
              }
                  temperature = Math.round(temperature);
              
              this.set({'TemperatureInLocalMetric':temperature});
              }
              if(this.has('WeatherCode') && this.get('WeatherCode').length === 3){
                      this.set({'WeatherImagePath':'./images/' + weather_Image[this.get('WeatherCode')] + '.png'});
              }
             }
                                                 
       
       });
       
       return airportDetail;
                                             });