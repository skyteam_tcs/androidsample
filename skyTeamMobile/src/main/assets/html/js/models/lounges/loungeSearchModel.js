define(['jquery','underscore','backbone'], 
		function($, _, Backbone){
	

	// model for an skytip
	var loungeSearchModel=Backbone.Model.extend({
                                                
        defaults:{
            AirportName:'',
            AirportCode:'',
            otherLanguageInstruction:true
            
        },
        initialize:function(){
            if(localStorage.language === "En"){
            this.otherLanguageInstruction = false;
            }
        }
		
	});
	
	/*skytipModel.skytipCollection = Backbone.Collection.extend({
 	   model: skytipModel.singleTipModel
 		  
  });*/

	
	return loungeSearchModel;
});