/*
 * created by:583152
 * purpose	 : interface to manage all (hybrid) service interaction at one place
 * */

/*	Default values of service utilities variable*/

//API key change Start

var SERVICE_UTIL = SERVICE_UTIL == null ? {
				TIMEOUT:30000,
				ENDPOINT:"https://api.skyteam.com",
				TYPE:'GET'

} : SERVICE_UTIL;

//var SERVICE_UTIL = SERVICE_UTIL == null ? {
//				TIMEOUT:30000,
//				ENDPOINT:"https://services.staging.skyteam.com",
//				TYPE:'GET'
//
//} : SERVICE_UTIL;

//API key change End

var defaultHeaders = {
		"Accept-Language":	(localStorage.language === "zh") ? "zh-Hans" : localStorage.language,
		"api_key":API_KEY,
		//"source": "SkyApp"
};


/*
 * Name: CallService
 * Description:for make the service calls to get details from the service
 * Param:
 * 	JSON object(requestData)
 * 		it will contain following child's
 * 			type,
 * 			endPoint,
 * 			Custom headers,
 * 			Callback,
 * 			QueryParameters
 */
function callService(requestData,callback){
    	window.networkStatCallBack = function(){
            var netstat = JSON.parse(localStorage.networkStatus);
        if(netstat.network == "offline"){
            hideActivityIndicator();
            setTimeout(function(){
                       if(requestData.MSG_OFFLINE){//custom msg
                       openAlertWithOk(requestData.MSG_OFFLINE);
                       }
                       else{//default msg
                       openAlertWithOk($.i18n.map.sky_notconnected);
                       }
            },100);
        } else {//if online then

        	requestData.TYPE = (_.isUndefined( requestData.TYPE ))? (SERVICE_UTIL.TYPE):(requestData.TYPE);

        	$.ajax({
        		url : requestData.URL,
        		type: requestData.TYPE,
        		timeout: requestData.TIMEOUT,
        		headers:requestData.HEADERS,
                datatype:JSON,
        		success: function(response)
                           {
        			//hideActivityIndicator();
        			if(jQuery.isFunction(callback)){//Call back is function
        				callback(response);
        			}
        			else if(!_.isUndefined(callback)){//if variable
        				callback = JSON.stringify(response);
        			}
        			else{

        			}
        		},
        		error: function(xhr,exception){
        			if(exception == "timeout"){
        				hideActivityIndicator();
                       if(requestData.MSG_TIMEOUT){//custom msg
                       openAlertWithOk(requestData.MSG_TIMEOUT);
                       }
                       else{//default msg
                       openAlertWithOk($.i18n.map.sky_unload);
                       }
        			}

        			else if(exception == "error"){
        				hideActivityIndicator();
                       if(requestData.MSG_ERROR){//custom msg
                       openAlertWithOk(requestData.MSG_ERROR);
                       }
                       else{//default msg
                       openAlertWithOk($.i18n.map.sky_requestfailed);
                       }
        			}
        			else{

        			}
        		},
        		failure: function(){
        			   hideActivityIndicator();
        		}

        	});

        }
        window.networkStatCallBack = function(){}
    };
    /*checking network copnnectivity*/
    setTimeout(function(){
               window.open(iSC.Network, "_self");
               },1);
}
/*	End of function service call*/

/*******************************************************************************************************/

/* Implementation of  skypriority advance details querybuilder call */
function requestBuilder(queryString){
	//creating new request build object
	var reqBuilder = {};

	reqBuilder.HEADERS	=	$.extend(true, {}, defaultHeaders);
	reqBuilder.TYPE	=	SERVICE_UTIL.TYPE;
	reqBuilder.TIMEOUT	=	SERVICE_UTIL.TIMEOUT;

	reqBuilder.URL	=	queryString;

	return reqBuilder;
}
/* End of  skypriority advance details query builder */

function webWorkerCommunicator(requestData,callback){

      //Checking Web worker Support
      if (typeof (Worker) !== "undefined") {
          //Creating Worker Object
          var worker = new Worker("worker.js");
          //Call Back Function for Success
          worker.onmessage = workerResultReceiver;
          //Call Back function if some error occurred
          worker.onerror = workerErrorReceiver;
          //Posting Data to worker: Here We are passing Email ID
          worker.postMessage(requestData);

          function workerResultReceiver(e) {
          //Updating UI With Result
          callback(e.data);
          }

          function workerErrorReceiver(e) {
          }
      }
      else {

      }

}
function callAjax(requestData,callback){



            requestData.TYPE = (_.isUndefined( requestData.TYPE ))? (SERVICE_UTIL.TYPE):(requestData.TYPE);

            $.ajax({
                   url : requestData.URL,
                   type: requestData.TYPE,
                   timeout: requestData.TIMEOUT,
                   headers:requestData.HEADERS,
                   datatype:JSON,
                   success: function(response)
                   {
                   //hideActivityIndicator();
                   if(jQuery.isFunction(callback)){//Call back is function
                   callback(response);
                   }
                   else if(!_.isUndefined(callback)){//if variable
                   callback = JSON.stringify(response);
                   }
                   else{

                   }

                   },
                   error: function(xhr,exception){
                   if(exception == "timeout"){
                   hideActivityIndicator();
                   openAlertWithOk($.i18n.map.sky_unload);
                   }

                   else if(exception == "error"){
                   hideActivityIndicator();
                   openAlertWithOk($.i18n.map.sky_notconnected);
                   }
                   else{

                   }
                   },
                   failure: function(){
                   hideActivityIndicator();
                   }

                   });

}
/*	End of function service call*/
function weatherDetailsService(callBack){

    var obj = JSON.parse(sessionStorage.airportDetailsObj);
    var weatherDetailsURL = URLS.WEATHER_DETAILS+'airportcode='+obj.airportCode+'&countrycode='+obj.countryCode;
    //var weatherDetailsURL ='https://api.skyteam.com/weatherforecast?airportcode='+obj.airport_code+'&countrycode='+obj.country_Code;
    var requestData = requestBuilder(weatherDetailsURL);

//    requestData.HEADERS['api_key'] = "3mr52xszzjkp854j4wjshwmz";


//API key change Start

requestData.HEADERS['api_key'] = "3mr52xszzjkp854j4wjshwmz";
//requestData.HEADERS['api_key'] = "5m49bjnak3nsrv5hdkf4aab6";

//API key change End

    /* custom service message for weather details*/
    requestData.MSG_ERROR = $.i18n.map.sky_retrievefailed;
    requestData.MSG_OFFLINE= $.i18n.map.sky_retrievefailed;
    /* custom service message for weather details*/

    callService(requestData,callBack);

}