var iSC = iSC == null ? {} : iSC;

iSC.getAirports = "app:testURL";
iSC.searchFlightsURL = "app:searchFlights";
iSC.savedAirports = "app:savedAirportsURL";
iSC.deleteFlights = "app:deleteFlightsURL";
iSC.deleteAirports = "app:deleteAirportsURL";
iSC.loungeFinder = "app:loungeFinderURL";
iSC.savedFlights = "app:savedFlightURL";
iSC.memberdetails = "app:memberURL";
iSC.skytips = "app:skyTipsURL";
iSC.insertSavedAirport = "app:saveAirportsURL";
iSC.insertSaveFlight = "app:saveFlightURL";
iSC.openmemberdetails = "url:";
iSC.skyTipsHub = "app:hubURL";
iSC.skyTipsLocation = "loc:latlong";
iSC.flightstatusURL = "app:flightStatusURL";
iSC.loungeList = "app:loungesList";
iSC.Settings = "app:settings";
iSC.Network = "app:network";
iSC.savedFlightStatusURL = "app:savedFlightStatusURL";
iSC.getLang="app:langURL";
iSC.cancelReuest="app:cancelURL";
iSC.skyTipsCityList="app:skytipsCityListURL";
iSC.openSkyTeamWebSite="app:skyTeamWebSite";
iSC.citylistUpdate="app:cityListUpdateURL";
iSC.closeScanView="app:closeScan";

var memberAirlines={
						SU:'Aeroflot',
						AR:' Argentinas',
						AM:'Aeromexico',
						UX:'Air Europa',
						AF:'Air France',
						AZ:'Alitalia',
						CI:'China Airlines',
						MU:'China Eastern',
						CZ:'China Southern',
						OK:'Czech Airlines',
						DL:'Delta Air Lines',
						GA:'Garuda Indonesia',
						KQ:'Kenya Airways',
						KL:'KLM',
						KE:'Korean Air',
						ME:'Middle East Airlines',
						SV:'Saudia',
						RO:'TAROM',
						VN:'Vietnam Airlines',
						MF:'Xiamen Air'
				};

var spImages_small = {
BaggageFirst:"images/spfinder/baggagehandling.png",
PriorityCheckIn:"images/spfinder/checkin.png",
PrioritySecurityLines:"images/spfinder/securitylane.png",
PriorityImmigration:"images/spfinder/immigration.png",
PriorityTicketDesk:"images/spfinder/ticketoffice.png",
PriorityBoarding:"images/spfinder/boardinglane.png",
PriorityBaggageDropOff:"images/spfinder/baggagedrop.png",
PriorityTransferDesk:"images/spfinder/transferDesk.png"
};
var spImages_large = {
BaggageFirst:"images/spfinder/s-baggagehandling.png",
PriorityCheckIn:"images/spfinder/s-checkin.png",
PrioritySecurityLines:"images/spfinder/s-securitylane.png",
PriorityImmigration:"images/spfinder/s-immigration.png",
PriorityTicketDesk:"images/spfinder/s-ticketoffice.png",
PriorityBoarding:"images/spfinder/s-boardinglane.png",
PriorityBaggageDropOff:"images/spfinder/s-bagoff.png",
PriorityTransferDesk:"images/spfinder/s-transferlane.png"
}
;







var localization = {
	english : {
		//search airport finder start
		'Map' : 'Map',
		'List' : 'List',
		'Airports_finder' : 'Airport Finder',
		'Destination' : 'Destination',
		//search airport finder End
		'Lounges' : 'Lounges',
		'Air France in Paris' : 'Air France in Paris',
		'AMS' : 'AMS',
		'LAX' : 'LAX',
		'05/06/13' : 'o5/06/13',
		'23:31' : '23:31',
		'07:18' : '07:18',
		'+1 DAY' : '+1 DAY',
		'Flight Number : KL4117/MU8274' : 'Flight Number : KL4117/MU8274',
		'Search Flights' : 'Search Flights',
		'Search Airports' : 'Search Airports',
		'Sky Tips' : 'Sky Tips',
		'Take a cooling swim at Singapore Changi Airport' : 'Take a cooling swim at Singapore Changi Airport',
		'03:31' : '03:31',
		'Menu' : 'Menu',
		'Home' : 'Home',
		'Flight Search' : 'Flight Search',
		'Flight Status' : 'Flight Status',
		'Airports' : 'Airports',
		'Lounge finder' : 'Lounge finder',
		'SkyTips' : 'SkyTips',
		'About Skyteam' : 'About Skyteam',
		'Settings' : 'Settings'
	},
	french : {
		'Lounges' : 'DokÃƒÆ’Ã‚Â¼man',
		'Air France in Paris' : 'Yeni bir tartÃƒâ€žÃ‚Â±Ãƒâ€¦Ã…Â¸ma baÃƒâ€¦Ã…Â¸lat',
		'AMS' : 'BaÃƒâ€¦Ã…Â¸lÃƒâ€žÃ‚Â±k',
		'LAX' : 'GÃƒÆ’Ã‚Â¶nder',
		'05/06/13' : ' 05/06/13',
		'23:31' : '23:31',
		'07:18' : '07:18',
		'+1 DAY' : '+1 Ãƒâ€žÃ‚Â°ptal',
		'Flight Number : KL4117/MU8274' : 'KullanÃƒâ€žÃ‚Â±cÃƒâ€žÃ‚Â± : KL4117/MU8274',
		'Search Flights' : 'Geri',
		'Search Airports' : 'PaylaÃƒâ€¦Ã…Â¸',
		'Sky Tips' : 'GÃƒÆ’Ã‚Â¶nderen',
		'Take a cooling swim at Singapore Changi Airport' : 'Kaydedilen DokÃƒÆ’Ã‚Â¼manlarÃƒâ€žÃ‚Â± GÃƒÆ’Ã‚Â¶ster',
		'03:31' : '03:31',
		'Menu' : 'Dizin',
		'Home' : 'ÃƒÆ’Ã…â€œyeler',
		'Flight Search' : 'CEROS',
		'Flight Status' : 'Bilgilendirmeler',
		'Airports' : 'Kaydet',
		'Lounge finder' : 'TanÃƒâ€žÃ‚Â±m',
		'SkyTips' : 'Yorum Sil',
		'About Skyteam' : 'Ara',
		'Settings' : 'tr'
	}
}

/* **App URL constants ** */

// Defining object "URL" if its not exist

var END_POINT='https://api.skyteam.com';
//var END_POINT='https://services.staging.skyteam.com';
var URLS = typeof URLS == 'undefined' ? {

     // source should be SkyApp
FLIGHT_SEARCH:END_POINT+'/v3/schedules?',

 AIRPORT_SEARCH:END_POINT+'/airportsearch',

 FLIGHT_STATUS:END_POINT+'/v2/flight?',


 AIRPORT_DETAILS:END_POINT+'/airport?version=3&',

 LOUNGE_SEARCH:END_POINT+'/v3/locations/airports/lounges?',

 SKYTIP_SEARCH:END_POINT+'/skytips?version=3&',

 WEATHER_DETAILS:END_POINT+'/weatherforecast?',

 SKYPRIORITY_BASIC:END_POINT+'/airports/skypriority/basicsearch?',

 SKYTIPS_ADD:END_POINT+'/skytips',

 SKYTIPS_ADD_TERMS:END_POINT+'/terms/contest_terms.html',

 SKYPRIORITY_ADVANCE:END_POINT+'/airports/skypriority/advancesearch?'


} : URLS;
　
var API_KEY ='3mr52xszzjkp854j4wjshwmz';

//var API_KEY ='5m49bjnak3nsrv5hdkf4aab6';

//var API_KEY ='yfunvneadwz62wywjvtgqc8a';

/* **App URL constants ** */

/* List of airports having own images to show in airport details page */
var AIRPORT_HUB_IMAGES_LIST = ["AMS","ATH","ATL","BCN","BEY","BKK","BUD","CAI","CAN","CDG","CGK","DEL","DTW","DXB","EZE","FCO","FRA","GRU","HAN","HKG","ICN","IST","JED","JFK","KIX","KUL","LAX","LHR","MAD","MIA","MSP","MTY","MXP","NBO","NRT","OTP","PEK","PNH","PRG","PVG","SGN","SIN","SVO","SYD","TLV","TPE","XMN"];

/* Flight status supported barcode types */
var APP_SUPPORTED_BARCODES = ["PDF417","QR","AZTEC","DATAMATRIX"];