define(['jquery','underscore','backbone'],
       function($, _, Backbone){
              customView = Backbone.View.extend({
               
               initialize: function () {
               
               //this.router = new app.Router();
               },
               
               render: function(options) {
               
               options = options || {};
               
               if (options.page === true) {
               this.$el.addClass('page');
               
               }
               
               return this;
               
               },
               
               transitionIn: function (callback) {
                                                
               var view = this,
               delay
                if( (sessionStorage.navDirection) === "Right"){
                   this.$el.addClass('slideInForward');
                }
                else{
                   this.$el.addClass('slideInBackward');
                }
               var transitionIn = function () {
               view.$el.addClass('is-visible');
               view.$el.on('webkitTransitionEnd', function () {
                           if (_.isFunction(callback)) {
                           callback();
                           }
                           sessionStorage.navDirection = "Right";
                           });
               };
               
               _.delay(transitionIn, 20);

               },
               
               transitionOut: function (callback) {
                
               var view = this;
               if(sessionStorage.navDirection === "Right"){
                  view.$el.removeClass('slideInForward');
                  view.$el.addClass('slideOutForward');
                }
                else{
                    view.$el.removeClass('slideInBackward');
                    view.$el.addClass('slideOutBackward');
                }
               view.$el.removeClass('is-visible');
               view.$el.on('webkitTransitionEnd', function () {
                           if (_.isFunction(callback)) {
                           callback();
                           };
                           });
               
               }
               
        });
       
       /*var pageDirection = function(){
       var history = [];
       var pageName = window.location.hash.replace('#','');
       if(sessionStorage.getItem("previousPages")){
           history = JSON.parse(sessionStorage.previousPages);
           if(history.length >= 2){
               if(pageName === history[1].previousPage){
                "Left";
               }
           }
       }
       return "Right";
       };*/
       
       
       return customView;
});