require.config({
	paths:{
		router: 'router',
		jquery : 'libs/jquery/jquery-1.10.1.min',
		underscore : 'libs/underscore/underscore-min',
		backbone : 'libs/backbone/backbone-min',
		taffy : 'libs/taffydb/taffy',
		jqueryui : 'libs/jqueryui/jquery-ui',
		touchpunch : 'libs/jqueryui/jquery.ui.touch-punch',
		pageslide: 'libs/jquery_pageslide/jquery.pageslide.min',
		slider: 'libs/jquery/idangerous.swiper-2.0.min',
		tooltip:'libs/tooltip/jquery.tooltipster.min',
		slidetouch:'libs/Slider/jquery.touchSwipe',
		touchSwipes:'libs/Slider/jquery.touchSwipe.min',
		template : '../template',
		models : 'models',
		responses : 'models/responses',
		utilities : 'utilities',
		ji18n : 'libs/jquery/jquery.i18n.properties',
		constants : 'constants',
		serviceInteraction:'serviceInteraction'

	}

});

require([
         'jquery','app','utilities',
         ],function($,App,utilities){
	/* Code for calculating the height of section */

	/* End of calculate function */
	var time = localStorage.time;
	var distance = localStorage.distance;
	var temparature = localStorage.temparature;
	var date = localStorage.date;
	var skytips = localStorage.skytips;
	var language = localStorage.language;
	var initialLoad = localStorage.InitialLoad;
	var skytipsAirports = localStorage.cityList_Skytips;

	localStorage.clear();

	localStorage.time = time;
	localStorage.distance = distance;
	localStorage.temparature = temparature;
	localStorage.date = date;
	localStorage.skytips = skytips;
	localStorage.language=language;
	localStorage.InitialLoad=initialLoad;
	localStorage.cityList_Skytips=skytipsAirports;

	window.open(iSC.Settings,"_self");


	$('#main_content').height($(document).height() - $('.header').outerHeight(true));
	$(window).off('resize').on('resize', function() {
		$('#main_content').height($(document).height() -          $('.header').outerHeight(true));
	});

	/*Code for getting the mobile OS */

	if(navigator.userAgent.match(/iPhone/i) || "iPhone" == navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || "iPad" == navigator.userAgent.match(/iPad/i) )
	{
		sessionStorage.deviceInfo="iPhone";
	}
	else if(navigator.userAgent.match(/Android/i) || "Android" == navigator.userAgent.match(/Android/i) )
	{
		sessionStorage.deviceInfo="android";

	}


	setTimeout(function() {
		App.initialize();
	}, 3000);





});
