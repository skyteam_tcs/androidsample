package org.skyteam.handlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.database.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;


/**
 * 
 * This class is used for saving the values in Settings page
 * 
 */
public class SettingsHandler {

	private DBHelper dbhelper;
	private Context context;

	public SettingsHandler(Context context) {
		this.context = context;
		dbhelper = DBHelper.getInstance(this.context);
	}

	public String nativeHandler(String command) {
        
		if (command != null && command.length() > 0) {
			String[] arr = command.split(",");

			if (arr != null && arr.length >= 5) {

				JSONObject result = new JSONObject();
				JSONArray settingsArr = new JSONArray();

				try {
					result.put("Time", arr[0] != null ? arr[0] : "");
					result.put("Distance", arr[1] != null ? arr[1] : "");
					result.put("Temperature", arr[2] != null ? arr[2] : "");
					result.put("Date", arr[3] != null ? arr[3] : "");
					result.put("SkyTips", arr[4] != null ? arr[4] : "");
					result.put("Language", arr[5] != null ? arr[5] : "");
					result.put("InitialLoad", arr[6] != null ? arr[6] : "");
					
					settingsArr.put(result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				//dbhelper.clearSettings();
				
				dbhelper.insertIntoSettingsFile(settingsArr);
			}
		}

		return null;

	}

}
