package org.skyteam.handlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.Utils;
import org.skyteam.utils.WebServiceInteraction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


/**
 * 
 * This class provides functionalities for providing lounge details
 * 
 */
public class LoungeFinderHandler {

	private Context context;
	private DBHelper dbhelper;
	private WebServiceInteraction webServiceInteraction;
	private static final String TAG = "LOUNGEFINDER";
	private getLoungeJsonAsync asyncTask;
	

	public LoungeFinderHandler(Context context) {
		this.context = context;
		dbhelper = DBHelper.getInstance(this.context);
		webServiceInteraction = new WebServiceInteraction(this.context);
	}

	public String nativeHandler(String command) {
		JSONObject loungDetails;
		String url = null;
		String airportCode = null;
		try {
			loungDetails = new JSONObject(command);
			url = loungDetails.isNull("url") ? "" : loungDetails
					.getString("url");
			airportCode = loungDetails.isNull("airportCode") ? ""
					: loungDetails.getString("airportCode");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		/* Check for connectivity */
		if (NetworkUtil.getConnectivityStatus(context) > 0) {
			String[] strArr = { url, airportCode };
			asyncTask=new getLoungeJsonAsync();
			Log.d("url",url);
			asyncTask.execute(strArr);
			Log.d("88888888","lounge finder ends");

		} else {


			/* Offline mode, send from stored database */
			JSONObject temp = dbhelper.getLounges(airportCode);
			return "javascript:iSU.processData(" + temp + ")";
		}
		return null;

	}

	public class getLoungeJsonAsync extends AsyncTask<String, Void, String[]> {

		@Override
		protected String[] doInBackground(String... params) {
			Utils.logMessage(TAG, params[0], Log.DEBUG);
			String str = null;
			try {
				String[] strArr = { params[0], "LoungeData" };

				JSONObject temp = webServiceInteraction.getLoungeDataReqHeader(params,"Accept-Language","En");
				JSONObject responseToHybrid=new JSONObject();
				responseToHybrid.put("url", StringConstants.LOUNGE_FINDER_URL);
				if (temp.toString().toLowerCase().contains("error")) {
					Log.d("ERROR","$$$$$$");
					responseToHybrid.put("Count", 0);
					responseToHybrid.put("ErrorMessage","No Lounges Found");
					str = responseToHybrid.toString();

				}
				else {
					JSONArray loungeDataArray=new JSONArray();
					int loungeCount=0;
					for(int i=0;i<temp.getJSONArray("airport").length();i++)
					{
						JSONObject tempObject=temp.getJSONArray("airport").getJSONObject(i);
						for(int j=0;j<tempObject.getJSONArray("lounges").length();j++)
						{
							JSONObject loungeData = new JSONObject();
							if(tempObject.has("airportCode"))
							{
								loungeData.put("AirportCode",tempObject.get("airportCode"));
							}
							if(tempObject.has("airportName"))
							{
								loungeData.put("AirportName",tempObject.get("airportName"));
							}
							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("id"))
							{
								loungeData.put("LoungeID",tempObject.getJSONArray("lounges").getJSONObject(j).get("id"));
							}
							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("name"))
							{
								loungeData.put("LoungeName",tempObject.getJSONArray("lounges").getJSONObject(j).get("name"));
							}
							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("airlineOperation")) {
								loungeData.put("OperatingAirline", tempObject.getJSONArray("lounges").getJSONObject(j)
										.getJSONObject("airlineOperation").getString("airlineCode"));
							}

							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("location")) {
								loungeData.put("LoungeLocation",tempObject.getJSONArray("lounges").getJSONObject(j).get("location"));
							}
							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("operationHours")) {
								loungeData.put("HoursOfOperation",tempObject.getJSONArray("lounges").getJSONObject(j).get("operationHours"));
							}
							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("facilities")) {
								for (int k = 0; k < tempObject.getJSONArray("lounges").getJSONObject(j).
										getJSONArray("facilities").length(); k++) {
									if (tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
											.getJSONObject(k).has("facilitiesName")) {
										Log.d("hi",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
												.getJSONObject(k).getString("facilitiesName"));
										switch (tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
												.getJSONObject(k).getString("facilitiesId")) {

											case "FAC_7":
												loungeData.put("SmokingRoom", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_8":
												loungeData.put("KidsRoom", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_1":
												loungeData.put("Shower",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_9":
												loungeData.put("Meal", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_10":
												loungeData.put("ActionStation",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_11":
												loungeData.put("Appetizers", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_12":
												loungeData.put("Snacks", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_13":
												loungeData.put("Wine", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_14":
												loungeData.put("Spirits", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_15":
												loungeData.put("SoftDrink", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_2":
												loungeData.put("AccessTodisabled", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_16":
												loungeData.put("CustomerService", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_17":
												loungeData.put("BoardingAnnoncements", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_18":
												loungeData.put("FlightMonitor", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_19":
												loungeData.put("Tv", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_3":
												loungeData.put("FreeWiFi",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_20":
												loungeData.put("MagazinesAndNewspapers", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_21":
												loungeData.put("FixedInternetAcces", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_4":
												loungeData.put("SilentArea", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_5":
												loungeData.put("BusinessCenter", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
											case "FAC_6":
												loungeData.put("MeetingAndConferenceRoom", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
														.getJSONObject(k).getString("facilitiesName"));
												break;
										}
									}
								}
							}
							if(tempObject.getJSONArray("lounges").getJSONObject(j).has("airlinesAccessEligible")) {

								String airlineCode=null;
								for (int l = 0; l < tempObject.getJSONArray("lounges").
										getJSONObject(j).getJSONArray("airlinesAccessEligible").length(); l++) {
									if(l==0)
									{
										airlineCode=tempObject.getJSONArray("lounges").getJSONObject(j).
												getJSONArray("airlinesAccessEligible").getJSONObject(l).getString("airlineCode");
									}
									else {
										airlineCode = airlineCode + "*" + tempObject.getJSONArray("lounges").getJSONObject(j).
												getJSONArray("airlinesAccessEligible").getJSONObject(l).getString("airlineCode");

									}
								}
								loungeData.put("AirlineCode",airlineCode);
							}
							if(loungeCount<temp.getInt("count")) {
								loungeDataArray.put(loungeCount, loungeData);
								loungeCount = loungeCount + 1;
							}
						}
						if(tempObject.has("airportLoungesRecords"))
						{
							responseToHybrid.put("Count",tempObject.getInt("airportLoungesRecords"));
						}
						responseToHybrid.put("LoungeData",loungeDataArray);
						str = responseToHybrid.toString();
						Log.d("str",str);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			Utils.logMessage(TAG, "got the json as : " + str, Log.DEBUG);
			String[] strArr = { str, params[1] };
			return strArr;
		}


		@Override
		protected void onPostExecute(String[] result) {

			Log.d(TAG, result[0]);
				new SkyteamWebviewActivity()
					.webViewLoadURL("javascript:iSU.processData(" + result[0]
							+ ")");

			/*
			 * Check for error case as database entry has to be done for success
			 * case only
			 */
			Log.d("result--->",result[0]);
			if (result[0].contains("ERROR")) {

			} else {
				dbhelper.deleteFromLounges(result[1]);
				dbhelper.insertIntoLounges(result[0]);
			}

		}

	}

	public void cancel() {
		if(asyncTask!=null){
		asyncTask.cancel(true);
		}
		
	}

}
