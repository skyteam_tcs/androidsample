package org.skyteam.handlers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.Utils;
import org.skyteam.utils.WebServiceInteraction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


/**
 * 
 * This class provides functionalities for Skytips page
 * 
 */
public class SkyTipsHandler {

	public static final String TAG = "SKYTIPSHANDLER";
	private Context context;
	private WebServiceInteraction webServiceInteraction;
	private DBHelper dbhelper;
	private SkyTipsAsync asyncTask;

	public SkyTipsHandler(Context context) {
		this.context = context;
		webServiceInteraction = new WebServiceInteraction(this.context);
		dbhelper = DBHelper.getInstance(this.context);
	}

	public String nativeHandler(String command) {
		try {
			JSONObject temp = new JSONObject(command);

			String url = temp.getString("url");
			String airportCode = temp.isNull("airportCode") ? null : temp
					.getString("airportCode");

			String airportString = temp.isNull("airportString") ? null : temp
					.getString("airportString");
			String[] strArr = { url, airportCode ,airportString};
			/* Check for connectivity */
			if (NetworkUtil.getConnectivityStatus(context) > 0) {
				asyncTask=new SkyTipsAsync();
				asyncTask.execute(strArr);

			} else {

				// Offline mode: retrieve from db
				JSONObject obj = dbhelper.getStoredSkyTips(strArr);

				JSONArray theme = dbhelper.selectDistinctTipTheme();
				try {
					obj.put("tipTheme", theme);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return "javascript:iSU.processData(" + obj + ")";
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/*
	 * This async task will fetch the skytips json using the input URL.
	 */
	private class SkyTipsAsync extends AsyncTask<String, Void, String[]> {

		@Override
		protected String[] doInBackground(String... params) {
			JSONObject temp = new JSONObject();
			String[] strArr = { params[0], "SkyTips" };
			String lang=null;
			try {
				 lang=dbhelper.getSettingFileObj().getString("Language");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			temp = webServiceInteraction.getSkyTipsJson(strArr,lang);

			try {

				temp.put("url", StringConstants.SKYTIPS_URL);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				lang="En";
				e.printStackTrace();
			}
			String[] ret = { params[0], params[1], temp.toString() };
			return ret;
		}

		@Override
		protected void onPostExecute(String[] result) {
			JSONObject obj = null;
			try {
				obj = new JSONObject(result[2]);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/*
			 * Check for error case. For success case only we should alter the
			 * databse
			 */
			if (result[2].contains("ERROR")) {

			} else {
				String[] ret = { result[0], result[1] };
				dbhelper.deleteFromSkytips(ret);
				dbhelper.insertIntoSkytips(obj);
				JSONArray theme = dbhelper.selectDistinctTipTheme();
				try {
					obj.put("tipTheme", theme);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			Utils.logMessage(TAG, "sending skytips as :" + obj.toString(),
					Log.DEBUG);
			new SkyteamWebviewActivity()
					.webViewLoadURL("javascript:iSU.processData(" + obj + ")");

		}

	}

	public JSONArray getUniqueThemes(JSONObject temp) {

		try {
			JSONArray skytips = new JSONArray();

			if ((temp.get("SkyTips").getClass().getSimpleName())
					.equalsIgnoreCase("JSONObject")) {
				skytips.put(temp.getJSONObject("SkyTips"));
			} else {
				skytips = temp.getJSONArray("SkyTips");
			}

			JSONArray themeArr = new JSONArray();
			List<String> strArr = new ArrayList<String>();
			int count = skytips.length();
			for (int i = 0; i < count; i++) {
				JSONObject obj = skytips.getJSONObject(i);
				String theme = obj.getString("TipTheme");

				if (strArr != null && strArr.size() >= 0
						&& !strArr.contains(theme)) {
					strArr.add(theme);
					themeArr.put(theme);
				}
			}

			Utils.logMessage(TAG, "tip theme : " + themeArr.toString(),
					Log.DEBUG);
			return themeArr;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Utils.logMessage(TAG, "tip theme : " + "returning null", Log.DEBUG);
		return null;

	}

	public void cancel() {
		if(asyncTask!=null){
			asyncTask.cancel(true);
		}
		
	}
}
