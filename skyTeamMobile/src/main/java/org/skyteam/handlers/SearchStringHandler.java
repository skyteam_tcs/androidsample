package org.skyteam.handlers;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.Utils;
import org.skyteam.utils.WebServiceInteraction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


public class SearchStringHandler {

	private Context context;
	private WebServiceInteraction webServiceInteraction;
	private static final String TAG = "SEARCHSTRING";

	public SearchStringHandler(Context context) {
		this.context = context;
		webServiceInteraction = new WebServiceInteraction(this.context);
	}

	public void nativeHandler(String command) {
		String[] strArr = { command, "Connections" };
		//new getJsonAsync().execute(strArr);
		new Thread(new FlightSearch(strArr)).start();
	}

	public class getJsonAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			String str = null;
			try {
				JSONObject temp = new JSONObject();
				if (NetworkUtil.getConnectivityStatus(context) > 0) {
					temp = webServiceInteraction.getJsonData(params);
				} else {
					temp.put(params[1], "Network Error");
				}

				temp.put("url", StringConstants.SEARCH_FLIGHTS_URL);

				if (temp.toString().contains("ERROR")
						|| temp.toString().contains("Network Error")) {
					temp.put("Count", 0);
				}

				if (temp.has("Onward")) {
					temp.put("journey", "roundtrip");
				} else {
					temp.put("journey", "oneway");
				}

				str = temp.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return str;
		}

		@Override
		protected void onPostExecute(String result) {
			new SkyteamWebviewActivity()
					.webViewLoadURL("javascript:iSU.processData(" + result
							+ ")");
		}

	}
	
	
	private class FlightSearch implements Runnable{

		private String[] params;
		
		public FlightSearch(String[] params) {
			super();
			this.params = params;
		}

		

		@Override
		public void run() {
			String str = null;
			try {
				JSONObject temp = new JSONObject();
				if (NetworkUtil.getConnectivityStatus(context) > 0) {
					temp = webServiceInteraction.getJsonData(params);
				} else {
					temp.put(params[1], "Network Error");
				}

				temp.put("url", StringConstants.SEARCH_FLIGHTS_URL);

				if (temp.toString().contains("ERROR")
						|| temp.toString().contains("Network Error")) {
					temp.put("Count", 0);
				}

				if (temp.has("Onward")) {
					temp.put("journey", "roundtrip");
				} else {
					temp.put("journey", "oneway");
				}

				str = temp.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//new SkyteamWebviewActivity().loadURLinUIThread("javascript:iSU.processData(" + str	+ ")");
			
			final String strNew = str;
			SkyteamWebviewActivity.getWebView().post(new Runnable() {

				@Override
				public void run() {

					SkyteamWebviewActivity.getWebView().loadUrl("javascript:iSU.processData(" + strNew	+ ")");
				}
			});

		}
		
	}

}
