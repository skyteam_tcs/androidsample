package org.skyteam.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyCancelCallBack;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.Utils;
import org.skyteam.utils.WebServiceInteraction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


/**
 * 
 * This class provides functionalities for flight status
 *
 *
 */
public class FlightStatusHandler {

	private static final String TAG = "FLIGHTSTATUS";
	private Context context;
	private DBHelper dbhelper;
	private getJsonAsync asyncTask;
	private getFlightStatusJsonAsync asyncTaskSave;
	private String acceptHeader;
    private   SkyCancelCallBack mCallback;
	public FlightStatusHandler(Context context) {
		this.context = context;
		dbhelper = DBHelper.getInstance(this.context);
		
		
		JSONObject languageJsonObj = dbhelper.getSettingFileObj(); // your language
		
		try {
			
			if ("zh".equalsIgnoreCase(languageJsonObj.getString("Language"))) {
				acceptHeader = "zh-Hans";

			}
			
			else {
				acceptHeader = languageJsonObj.getString("Language");
			}
		} catch (JSONException e) {
			acceptHeader = "En";
			e.printStackTrace();
		}
		
	}

    public FlightStatusHandler(Context context,SkyCancelCallBack callback) {
        this.context = context;
        mCallback = callback;
        dbhelper = DBHelper.getInstance(this.context);


        JSONObject languageJsonObj = dbhelper.getSettingFileObj(); // your language

        try {

            if ("zh".equalsIgnoreCase(languageJsonObj.getString("Language"))) {
                acceptHeader = "zh-Hans";

            }

            else {
                acceptHeader = languageJsonObj.getString("Language");
            }
        } catch (JSONException e) {
            acceptHeader = "En";
            e.printStackTrace();
        }

    }

	public String nativeHandler(String control, String url) {

		if (control.equals(StringConstants.CMD_FLIGHT_STATUS)) {
			String[] strArr = { url, "FlightStatus" };
			asyncTask=new getJsonAsync();
			asyncTask.execute(strArr);
			
		} else if (control.equals(StringConstants.CMD_SAVED_FLIGHT_STATUS)) {
			
			asyncTaskSave=new getFlightStatusJsonAsync();
			asyncTaskSave.execute(url);
		}
		return null;
	}

	public class getJsonAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			JSONObject temp = new JSONObject();
			try {
				/* Check for connectivity */
				if (NetworkUtil.getConnectivityStatus(context) > 0) {

					WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
							context);
					
					
					temp = webServiceInteraction.getJsonDataReqHeader(params,"Accept-Language",acceptHeader);
					temp.put("url", StringConstants.FLIGHT_STATUS);

					if (temp.toString().contains("ERROR")) {
						temp.put("Count", 0);
					}

				} else {

					temp.put("url", StringConstants.FLIGHT_STATUS);
					temp.put("Count", 0);
					temp.put("FlightStatus", "Network Error");

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return temp.toString();
		}

		@Override
		protected void onPostExecute(String result) {
            if(mCallback != null){
                mCallback.scanCompleted();
            }
			new SkyteamWebviewActivity()
					.webViewLoadURL("javascript:iSU.processData(" + result
							+ ")");
		}

	}
	
	public class getFlightStatusJsonAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			JSONObject temp = new JSONObject();
			temp = dbhelper.getIndividualStatus(params[0]);

			return temp.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			Utils.logMessage(TAG, result, Log.DEBUG);
			new SkyteamWebviewActivity()
					.webViewLoadURL("javascript:iSU.processData(" + result
							+ ")");
		}

	}

	/*
	 * Get the flight status for showing in the home screen based on following
	 * condition. If any one of the leg status is " Cancelled " then show
	 * overall status as Cancelled Else If any one of the leg status is
	 * " Delayed " (and None of the segments have " Cancelled " status) then
	 * show overall status as Delayed Else If Last leg or final destination
	 * status is " Landed " then show overall status as Landed Else If Last leg
	 * or final destination status is " On Time " then show overall status as On
	 * Time Else If any one of the leg status is not available or No Status
	 * found (Check - 1. None of the segments have Cancelled and/or Delayed
	 * status, 2. Final destination status should not be Landed or On Time)
	 * then dont display the overall status Else If any one of the leg/segment
	 * status is " On time " (and None of the segments have " Cancelled " and/or
	 * Delayed and/or No Status) then show overall status as On Time Else If
	 * Any of the leg/segment status is " Scheduled " (and None of the segments
	 * have " Cancelled " and/or Delayed and/or On Time and/or No Status)
	 * then show overall status Scheduled Else If only one segment of the
	 * journey then overall status will be displayed with the " Same Status " as
	 * the Web Service returns (if the status is not available or No status
	 * found then dont display the status) End If
	 */


	public HashMap<String, Object> getFlightStatus(String[] urlArr) {

		String flightStatus = "";
		int i = 0;
		int count = urlArr.length;
		JSONArray statusJsonArr = new JSONArray();
		JSONObject status = new JSONObject();
		HashMap<String, Object> hm = new HashMap<String, Object>();
		boolean flightNumberSearch = false;
		JSONArray statusObjArr = new JSONArray();

		while (i < count) {
			String[] params = { urlArr[i], "FlightStatusSegment" };
			i++;
			WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
					context);
			status = webServiceInteraction.getJsonDataReqHeader(params,"Accept-Language",acceptHeader);

			statusJsonArr.put(status);
		}

		i = 0;
		while (i < count) {
			try {
				if (statusJsonArr.getJSONObject(i).isNull("Connections")) {

					flightNumberSearch = false;
					i++;
					continue;
				} else {
					flightNumberSearch = true;
					i++;
					break;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (flightNumberSearch) {

			i = 0;
			List<String> statusArr = new ArrayList<String>();
			try {
				if(statusJsonArr.getJSONObject(i)
						.getJSONObject("Connections").isNull("Segments") == false)
				{
					Utils.logMessage(TAG, "segment is available", Log.DEBUG);
					while (i < count) {
						try {


							String str = statusJsonArr.getJSONObject(i)
									.getJSONObject("Connections").get("Segments")
									.getClass().getSimpleName();
							Utils.logMessage(TAG, "the class is : " + str, Log.DEBUG);
							if (str.equalsIgnoreCase("JSONArray")) {
								JSONArray segmentJson = statusJsonArr.getJSONObject(i)
										.getJSONObject("Connections")
										.getJSONArray("Segments");
								int cnt = statusJsonArr.getJSONObject(i)
										.getJSONObject("Connections")
										.getJSONArray("Segments").length();
								for (int j = 0; j < cnt; j++) {

									// JSONArray arr = obj;
									if (segmentJson.getJSONObject(j).isNull(
											"FlightStatus") == false) {
										statusObjArr.put(segmentJson
												.getJSONObject(j)
												.getJSONObject("FlightStatus"));
										if (segmentJson.getJSONObject(j)
												.getJSONObject("FlightStatus")
												.isNull("CurrentStatus") == false) {
											statusArr
													.add(segmentJson
															.getJSONObject(j)
															.getJSONObject(
																	"FlightStatus")
															.getString(
																	"CurrentStatus"));
										}

									}
								}

							} else if (str.equalsIgnoreCase("JSONObject")) {

								if (statusJsonArr.getJSONObject(i)
										.getJSONObject("Connections")
										.getJSONObject("Segments")
										.isNull("FlightStatus") == false) {

									statusObjArr.put(statusJsonArr
											.getJSONObject(i)
											.getJSONObject("Connections")
											.getJSONObject("Segments")
											.getJSONObject("FlightStatus"));
									if (statusJsonArr.getJSONObject(i)
											.getJSONObject("Connections")
											.getJSONObject("Segments")
											.getJSONObject("FlightStatus")
											.isNull("CurrentStatus") == false) {
										statusArr.add(statusJsonArr
												.getJSONObject(i)
												.getJSONObject("Connections")
												.getJSONObject("Segments")
												.getJSONObject("FlightStatus")
												.getString("CurrentStatus"));
									}

								}

							}
							flightStatus = statusJsonArr.getJSONObject(i)
									.getJSONObject("Connections").isNull("OverallStatus") ? StringConstants.STATUS_NOT_FOUND:
									statusJsonArr.getJSONObject(i)
											.getJSONObject("Connections")
											.getString("OverallStatus");

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						i++;
					}
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}


			JSONArray sArr = new JSONArray();

			for (String string : statusArr) {
				sArr.put(string);
			}

			hm.put("Flight_Status", flightStatus);
			hm.put("Flight_Detail_Status", sArr);
			hm.put("Status_Object", statusObjArr);

			Utils.logMessage("Flight Status",
					"Sending the json as: " + hm.toString(), Log.DEBUG);
			return hm;

		} else {
			i = 0;
			String Status_flag="";
			List<String> statusArr = new ArrayList<String>();

			while (i < count) {
				try {
					statusArr.add(statusJsonArr.getJSONObject(i).isNull(
							"CurrentStatusCode")  ? "" : statusJsonArr
							.getJSONObject(i).getString("CurrentStatusCode"));

					if ((statusJsonArr.getJSONObject(i).isNull("ActualArrivalTime")  ? "" : "landed")=="landed" && (i ==count-1)) {
						Status_flag="landed";
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i++;
			}

			Utils.logMessage("Flight Status", "The status list is : " + statusArr,
					Log.DEBUG);

			if (count > 0) {
				if (count == 1
						&& !(statusArr.contains(StringConstants.STATUS_NOT_FOUND) || statusArr
						.contains(StringConstants.STATUS_NOT_AVAILABLE)  || statusArr.contains("NS"))) {
					flightStatus = statusArr.get(0);
				} else if (statusArr.contains(StringConstants.STATUS_CANCELLED) || statusArr.contains("CX")) {
					flightStatus = "CX";
				} else if (statusArr.contains(StringConstants.STATUS_DELAYED) || statusArr.contains("DY")) {
					flightStatus = "DY";
				} else if ((statusArr.get(count - 1).contains(
						StringConstants.STATUS_LANDED)) ||(statusArr.get(count - 1).contains(
						"ON"))) {
					flightStatus = "ON";
				} else if ((statusArr.get(count - 1).contains(
						StringConstants.STATUS_ONTIME)) || (statusArr.get(count - 1).contains("OT"))) {
					flightStatus = "OT";
                  }
				else if (statusArr.get(count - 1).contains("NS")) {
					flightStatus = "NS";
				}
               else if (statusArr.get(count - 1).contains(
						StringConstants.STATUS_LANDED) || statusArr.contains("ON")) {
					flightStatus = "ON";
			     }
			     else if (statusArr.get(count - 1).contains(
						StringConstants.STATUS_ONTIME)  || statusArr.contains("OT")) {
					flightStatus = "OT";
				}
				else if (!(statusArr.contains(StringConstants.STATUS_NOT_FOUND) || statusArr
						.contains(StringConstants.STATUS_NOT_AVAILABLE) || statusArr.contains("NS"))) {
					if (statusArr.contains(StringConstants.STATUS_ONTIME)) {
						flightStatus ="OT";
					} else if (statusArr.contains(StringConstants.STATUS_SCHEDULED)) {
						flightStatus = StringConstants.STATUS_SCHEDULED;
					}
				} else {
					flightStatus = "NS";
				}





//				if (count == 1
//						&& !(statusArr.contains(StringConstants.STATUS_NOT_FOUND) || statusArr
//						.contains(StringConstants.STATUS_NOT_AVAILABLE))) {
//					flightStatus = statusArr.get(0);
//				} else if (statusArr.contains(StringConstants.STATUS_CANCELLED)) {
//					flightStatus = StringConstants.STATUS_CANCELLED;
//
//				} else if (statusArr.contains(StringConstants.STATUS_DELAYED)) {
//					flightStatus = StringConstants.STATUS_DELAYED;
//
//				} else if (statusArr.get(count - 1).contains(
//						StringConstants.STATUS_LANDED)) {
//					flightStatus = StringConstants.STATUS_LANDED;
//
//				} else if (statusArr.get(count - 1).contains(
//						StringConstants.STATUS_ONTIME)) {
//					flightStatus = StringConstants.STATUS_ONTIME;
//
//				} else if (!(statusArr.contains(StringConstants.STATUS_NOT_FOUND) || statusArr
//						.contains(StringConstants.STATUS_NOT_AVAILABLE))) {
//					if (statusArr.contains(StringConstants.STATUS_ONTIME)) {
//						flightStatus = StringConstants.STATUS_ONTIME;
//					} else if (statusArr.contains(StringConstants.STATUS_SCHEDULED)) {
//						flightStatus = StringConstants.STATUS_SCHEDULED;
//					}
//				} else {
//					flightStatus = StringConstants.STATUS_NOT_FOUND;
//				}
			}

			JSONArray sArr = new JSONArray();
			for (String string : statusArr) {
				sArr.put(string);
			}


			hm.put("Flight_Status", flightStatus);
			hm.put("Flight_Detail_Status", sArr);
			hm.put("Status_Object", statusJsonArr);
            hm.put("Queue_Flight_Status", Status_flag);
			Utils.logMessage("Flight Status",
					"Sending the json as: " + hm.toString(), Log.DEBUG);
			return hm;
		}
	}



	public void cancel() {
		
		if(asyncTask!=null){
			asyncTask.cancel(true);
		}else if(asyncTaskSave !=null){
			asyncTaskSave.cancel(true);
		}
		
	}
}
