package org.skyteam.handlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.database.DBHelper;

import android.content.Context;


/**
 * 
 * This class provides general utilities for About Skyteam page
 * 
 */
public class SkyTeamMemberHandler {

	private Context context;
	private DBHelper dbhelper;

	public SkyTeamMemberHandler(Context context) {
		this.context = context;
		dbhelper = DBHelper.getInstance(this.context);
	}

	public String nativeHandler(String control, String command) {
		if (control.equals(StringConstants.CMD_OPEN_SKYTEAM_MEMEBR)) {
			JSONArray arr = dbhelper.getSkyTeamMemberData(command);

			JSONObject temp = new JSONObject();
			try {
				temp.put("url", StringConstants.MEMBER_URL);
				temp.put("member", arr);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return "javascript:iSU.processData(" + temp + ")";
		}
		return null;
	}

}
