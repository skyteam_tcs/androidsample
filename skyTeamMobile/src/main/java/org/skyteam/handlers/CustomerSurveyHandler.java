/*
package org.skyteam.handlers;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyCancelCallBack;
import org.skyteam.activities.SkyTeamCustomerSurveyActivity;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.data.CustomerSurveyData;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.WebServiceInteraction;

import java.io.IOException;
import java.io.InputStream;
import java.net.InterfaceAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;

*/
/**
 * Created by Mobility on 11/26/15.
 *//*



public class CustomerSurveyHandler {
    private HttpsURLConnection urlConn = null;
    private Context mContext;
    private int TIME_OUT = 20000;
    static JSONObject jObj = null;

    public void customizedAlert(String pageName, String lang, Context context) {
        // get Country code of device
        String cn = CustomerSurveyData.getCountryLocation();
        CustomerSurveyData.setLanguage(lang);
        mContext=context;

        // get Device Model Name
        String modelName= Build.MODEL;

        // form url to send request
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("invitation2.opinionbar.com")
                .appendPath("projects")
                .appendPath("p32838")
                .appendPath("show.asp")
                .appendQueryParameter("language", lang)
                .appendQueryParameter("channel", cn)
                .appendQueryParameter("device", modelName)
                .appendQueryParameter("pageid", pageName);

        String url = builder.build().toString();
        new CSAsyncTaskParseJson().execute(url,null,null);
    }

    public class CSAsyncTaskParseJson extends AsyncTask<String, String
            , String[]> {

        @Override
        protected String[] doInBackground(String... url) {
            String[] responseData=new String [2];
            try {
                if (NetworkUtil.getConnectivityStatus(mContext) > 0) {
                    // get json data from url
                    JSONObject json = getJSONFromUrl(url[0]);

                    if (json != null) {
                        responseData[0] = json.getString("result");
                        responseData[1] = json.getString("survey");
                    }
                    return responseData;
                }
            }  catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String[]  responseData) {
            if (responseData != null && responseData[0] != null && responseData[1] != null) {
                CustomerSurveyData.setsTime(false);
                CustomerSurveyData.setsLanguage(false);
                CustomerSurveyData.setsScreens(false);
                CustomerSurveyData.setsShared_PrefCheck(false);
                CustomerSurveyData.setsThreeStepsFlag(false);
                //CustomerSurveyData.setTimerStarts(false);

                // Log.d("Survey_Result: ","Notification " + responseData[0]);
                // if the below condition true, we show the popup to user
                if(responseData[0].equalsIgnoreCase("true")){
                    CustomerSurveyData.setsShowPopupFlag(true);
                    CustomerSurveyData.setSurveyURL(responseData[1]);
                }
            }
        }
    }

    public JSONObject getJSONFromUrl(String cs_url) {
        JSONObject jObj=null;

        // make HTTP request
        try {
            URL url = new URL(cs_url);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.connect();
            InputStream inputStream = urlConn.getInputStream();
            String jsonDataAsString= readStream(inputStream);
            jObj = new JSONObject(jsonDataAsString);
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

        // return JSON Object
        return jObj;
    }

    private String readStream(InputStream inputStream) {

        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
*/
