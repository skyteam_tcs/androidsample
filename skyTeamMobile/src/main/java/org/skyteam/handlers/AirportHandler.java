package org.skyteam.handlers;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.Utils;

import android.content.Context;
import android.util.Log;


/**
 * 
 * This class provides functionalities for deleting and saving airports in My
 * Sky team page
 * 
 */
public class AirportHandler {

	private Context context;
	private DBHelper dbhelper;
	public static final String TAG = "AIRPORTHANDLER";

	public AirportHandler(Context context) {
		this.context = context;
		dbhelper = DBHelper.getInstance(this.context);
	}

	public String nativeHandler(String control, String command) {

		if (control.equals(StringConstants.CMD_DELETE_AIRPORT)) {

			if (command != null && command.length() > 0) {
				dbhelper.deleteSavedAirports(command);
			}

			JSONObject temp = dbhelper.getSavedAirportslist();

			return "javascript:iSU.processData(" + temp + ")";
		} else if (control.equals(StringConstants.CMD_SAVE_AIRPORTS)) {

			JSONObject temp = new JSONObject();

			/*
			 * Check for the saved airports count. If it is 30 or up, then throw
			 * failure case.
			 */
			int savedAirportsCount = dbhelper.checkSavedAirportsCount();

			if (savedAirportsCount >= 30) {
				try {
					temp.put("url", StringConstants.SAVE_AIRPORTS_URL);
					temp.put("status", "failure");
					temp.put("count", savedAirportsCount);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				temp = dbhelper.insertIntoSavedAirports(command,
						savedAirportsCount);

			}
			Utils.logMessage(TAG, "Saved airports json: " + temp.toString(),
					Log.DEBUG);
			return "javascript:iSU.processData(" + temp + ")";
		}
		return null;

	}

}
