package org.skyteam.handlers;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.Utils;

import android.content.Context;
import android.util.Log;


/**
 * 
 * This class provides functionalities for deleting and saving flights in My Sky
 * team page
 * 
 */
public class FlightHandler {

	private static final String TAG = "FLIGHTHANDLER";
	private DBHelper dbhelper;
	private Context context;

	public FlightHandler(Context context) {
		this.context = context;
		dbhelper = DBHelper.getInstance(this.context);
	}

	public String nativeHandler(String control, String command) {

		if (control.equals(StringConstants.CMD_DELETE_FLIGHT)) {
			if (command != null && command.length() > 0) {
				dbhelper.deleteSavedFlights(command);
			}

			JSONObject temp = dbhelper.getSavedFlightslist();

			return "javascript:iSU.processData(" + temp + ")";
		} else if (control.equals(StringConstants.CMD_SAVE_FLIGHTS)) {

			JSONObject temp = new JSONObject();

			/*
			 * Check for the saved flights count. If it is 50 or up, then throw
			 * failure case.
			 */
			int savedFlightsCount = dbhelper.checkSavedFlightsCount();

			if (savedFlightsCount >= 50) {
				try {
					temp.put("url", StringConstants.SAVE_FLIGHTS_URL);
					temp.put("status", "failure");
					temp.put("count", savedFlightsCount);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				temp = dbhelper.insertIntoSavedFlights(command,
						savedFlightsCount);
			}
			Utils.logMessage(TAG, "Saved flights json: " + temp.toString(),
					Log.DEBUG);
			return "javascript:iSU.processData(" + temp + ")";
		}
		return null;

	}

}
