package org.skyteam.database;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.data.AirportList;
import org.skyteam.data.Location;
import org.skyteam.handlers.FlightStatusHandler;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;


/**
 *
 * Database class and utility functions for performing database interactions.
 *
 */
public class DBHelper extends SQLiteOpenHelper {

    private static Context mContext;
    private static final String DB_NAME = "skt.sqlite";
    private static final String DB_NAME2 = "skt2.sqlite";
    private static final int DB_VERSION = 2;//1;
    public static String CityAirportJSONFileName = "CityAirportListJson";
    public static String CityAirportOtherLangFileName = "CityAirportOtherLang";
    public static String SkyTipsCityAirportJSONFileName="SkyTipsCityDataList";
    public static String CityAirportJSONFilePath = "";
    public static String SettingsJSONFileName = "SettingsJson";
    private static final String AIRPORTS_TEST = "AirPorts_test";
    private static final String COL_LOCATION_CODE = "Location_Code";
    private static final String COL_AIRPORTS_CODE = "Airport_Code";
    private static final String COL_AIRPORT_NAME = "Airport_Name";
    private static final String COL_COUNTRY_CODE = "Country_Code";
    private static final String COL_CITY_NAME = "City_Name";

    private static final String COL_AIRPORTCODE = "AirportCode";
    private static final String COL_COUNTRYCODE = "CountryCode";
    private static final String COL_COUNTRYNAME = "CountryName";
    private static final String COL_CITYCODE = "CityCode";
    private static final String COL_CITYNAME = "CityName";

    private static final String COL_ID = "Id";
    private static final String SAVED_AIRPORTS = "SavedAirports_test";
    private static final String SAVED_FLIGHTS = "SavedFlights_test";
    private static final String COL_DEPARTURE = "Departure";
    private static final String COL_ARRIVAL = "Arrival";
    private static final String COL_NO_OF_STOPS = "No_of_Stops";
    private static final String COL_DEPARTURE_DATE = "Departure_Date";
    private static final String COL_ARRIVAL_DATE = "Arrival_Date";
    private static final String COL_DEPARTURE_TIME = "Departure_Time";
    private static final String COL_ARRIVAL_TIME = "Arrival_Time";
    private static final String COL_FLIGHT_NUM = "Flight_Number";
    private static final String COL_DETAILS = "Details";
    private static final String COL_DEPARTURE_CITY_NAME = "Departure_City_Name";
    private static final String COL_ARRIVAL_CITY_NAME = "Arrival_City_Name";
    private static final String COL_FLIGHT_STATUS_URL = "FlightStatusURL";
    private static final String FLIGHT_DETAIL_STATUS = "Flight_Detail_Status";
    private static final String COL_FLIGHT_STATUS = "Flight_Status";

    private static final String LOUNGES = "Lounges";
    private static final String COL_LOUNGE_ID = "LoungeID";
    private static final String COL_AIRLINE_CODE = "AirlineCode";
    private static final String COL_AIRPORT_CODE = "AirportCode";
    private static final String COL_AIRPORTNAME = "AirportName";
    private static final String COL_COUNTRY = "Country";
    private static final String COL_AIRPORT_CITY = "AirportCity";
    private static final String COL_LOUNGE_NAME = "LoungeName";
    private static final String COL_LOUNGE_LOCATION = "LoungeLocation";
    private static final String COL_HOURS_OF_OPERATION = "HoursOfOperation";
    private static final String COL_OPENING_TIME = "OpeningTime";
    private static final String COL_CLOSING_TIME = "ClosingTime";
    private static final String COL_DAYS_OPEN = "DaysOpen";
    private static final String COL_SHOWER = "Shower";
    private static final String COL_FIXED_INTERNET_ACCESS = "FixedInternetAcces";
    private static final String COL_FREE_WIFI = "FreeWiFi";
    private static final String COL_PAID_WIFI = "PaidWiFi";
    private static final String COL_ACCESS_TO_DISABLED = "AccessTodisabled";
    private static final String COL_SKYTEAM_MEMBER_OWNED = "SkyTeamMemberOwned";
    private static final String COL_CONTRACTED = "Contracted";
    private static final String COL_ADDITIONAL_INFO = "AdditionalInformation";
    private static final String COL_OPERATING_AIRLINE = "OperatingAirline";
    private static final String COL_SMOKING_ROOM="SmokingRoom";
    private static final String COL_KIDS_ROOM="KidsRoom";
    private static final String COL_MEAL="Meal";
    private static final String COL_ACTION_STATION="ActionStation";
    private static final String COL_APPETIZERS="Appetizers";
    private static final String COL_SNACKS="Snacks";
    private static final String COL_WINE="Wine";
    private static final String COL_SPIRITS="Spirits";
    private static final String COL_SOFT_DRINKS="SoftDrink";
    private static final String COL_CUSTOMER_SERVICE="CustomerService";
    private static final String COL_BOARDING_ANNOUNCEMENTS="BoardingAnnoncements";
    private static final String COL_FLIGHT_MONITOR="FlightMonitor";
    private static final String COL_TV="Tv";
    private static final String COL_MAGAZINES="MagazinesAndNewspapers";
    private static final String COL_SILENT_AREA="SilentArea";
    private static final String COL_BUSINESS_CENTER="BusinessCenter";
    private static final String COL_MEETING_CONFERENCE_ROOM="MeetingAndConferenceRoom";

    private static final String SKYTEAM_MEMBERS = "SkyTeam_Members";
    private static final String SKYTIPS_TABLE = "SkyTips";
    private static final String COL_SNO = "SNo";
    private static final String COL_SKYTEAM_MEMBER = "Skyteam_Member";
    private static final String COL_MEMBER_FULLNAME = "Member_FullName";
    private static final String COL_URL = "Url";
    private static final String COL_DESCRIPTION = "Description";
    private static final String COL_IMAGES = "Image";
    private static final String COL_DESIGNATION = "Designation";
    private static final String COL_TIP_ORIGINAL_LANG = "TipOriginalLanguage";
    private static final String COL_TIP_TRANSLATED = "TipTranslated";
    private static final String COL_TIP_THEME_ID = "Tip_Theme_Id";
    private static final String KEY_TIP_THEME_ID="ThemeId";
    private static final String SKYTIPS = "SkyTips";
    private static final String COL_ID_TIPS = "Id_tips";
    private static final String COL_NAME = "Name";
    private static final String COL_SURNAME = "Sur_name";
    private static final String COL_EMAIL = "email";
    private static final String COL_DEPARTURE_CITY = "Departure_city";
    private static final String COL_ID_HUB = "Id_hub";
    private static final String COL_TIP_TITLE = "TipTitle";
    private static final String COL_TIP_DESC = "TipDesc";
    private static final String COL_STATUS = "Status";
    private static final String COL_DATE = "Date";
    private static final String COL_TIP_THEME = "TipTheme";
    private static final String COL_HUB = "Hub";

    private static final String AIRPORTSLIST_LOUNGES = "AirportsList_Lounges";
    private static final String COL_CITY_CODE = "CITY_CODE";
    private static final String COL_COUNTRY_CODE_LOUNGES = "COUNTRY_CODE";
    private static final String COL_LOCATION_CODE_LOUNGES = "LOCATION_CODE";
    private static final String COL_LOCATION_NAME = "LOCATION_NAME";
    private static final String COL_COUNTRY_NAME_LOUNGES = "COUNTRY_NAME";
    private static final String COL_AREA = "AREA";
    private static final String COL_TIMEZONE = "TIMEZONE";
    private static final String COL_LONGITUDE = "LONGITUDE";
    private static final String COL_LATITUDE = "LATITUDE";

    private static final String SETTINGS = "Settings";
    private static final String COL_TIME = "Time";
    private static final String COL_DISTANCE = "Distance";
    private static final String COL_TEMPRATURE = "Temperature";
    private static final String COL_SKYTIPS = "SkyTips";

    private static final String COL_LOCAL_TIME = "LocalTime";
    private static final String COL_WEATHER_CODE = "WeatherCode";
    private static final String COL_LOUNGES_COUNT = "LoungesCount";
    private static final String COL_SKYTIPS_COUNT = "SkyTipsCount";
    private static final String COL_AIRLINES_COUNT = "AirlinesCount";

    private static final String CREATE_SAVED_AIRPORTS = "CREATE TABLE "
            + SAVED_AIRPORTS + "  ( " + COL_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_LOCATION_CODE
            + " VARCHAR(3) NOT NULL," + COL_AIRPORT_NAME
            + " VARCHAR(100) NOT NULL," + COL_COUNTRY_CODE
            + " VARCHAR(5) NOT NULL) ";

    private static final String CREATE_SAVED_FLIGHTS = "CREATE TABLE "
            + SAVED_FLIGHTS + " (" + COL_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT , " + COL_DEPARTURE
            + " CHAR(3) NOT NULL , " + COL_ARRIVAL + " CHAR(3) NOT NULL , "
            + COL_NO_OF_STOPS + " INTEGER NOT NULL  DEFAULT 0, "
            + COL_DEPARTURE_DATE + " VARCHAR(10) NOT NULL , "
            + COL_ARRIVAL_DATE + " VARCHAR(10) NOT NULL, " + COL_DEPARTURE_TIME
            + " VARCHAR(5) NOT NULL , " + COL_ARRIVAL_TIME
            + " VARCHAR(5) NOT NULL, " + COL_FLIGHT_NUM
            + " VARCHAR(25) NOT NULL, )" + COL_DETAILS + " VARCHAR )";

    private static final String CREATE_AIRPORTS_TEST = "CREATE TABLE "
            + AIRPORTS_TEST + "( " + COL_LOCATION_CODE
            + " VARCHAR(3) NOT NULL," + COL_AIRPORT_NAME
            + " VARCHAR(100) NOT NULL," + COL_COUNTRY_CODE
            + " VARCHAR(5) NOT NULL)";

    private static final String CREATE_LOUNGES = "CREATE TABLE " + LOUNGES
            + " (" + COL_LOUNGE_ID + " VARCHAR(7) NOT NULL ,"
            + COL_AIRLINE_CODE + " VARCHAR(50)," + COL_AIRPORT_CODE
            + " CHAR(3) NOT NULL ," + COL_AIRPORTNAME
            + " VARCHAR(50) NOT NULL ," + COL_COUNTRY
            + " VARCHAR(50) NOT NULL ," + COL_AIRPORT_CITY
            + " VARCHAR(50) NOT NULL ," + COL_LOUNGE_NAME + " VARCHAR(100),"
            + COL_LOUNGE_LOCATION + " VARCHAR(300)," + COL_HOURS_OF_OPERATION
            + " VARCHAR(100)," + COL_OPENING_TIME + " VARCHAR(5),"
            + COL_CLOSING_TIME + " VARCHAR(5)," + COL_DAYS_OPEN
            + " VARCHAR(100)," + COL_SHOWER + " CHAR(5),"
            + COL_FIXED_INTERNET_ACCESS + " CHAR(5)," + COL_FREE_WIFI
            + " CHAR(5) NOT NULL ," + COL_PAID_WIFI + " CHAR(5) NOT NULL ,"
            + COL_ACCESS_TO_DISABLED + " CHAR(5) NOT NULL ,"
            + COL_SKYTEAM_MEMBER_OWNED + " CHAR(2)," + COL_CONTRACTED
            + " CHAR(5) NOT NULL ," + COL_ADDITIONAL_INFO + " VARCHAR(300))";

    private static final String CREATE_SKYTEAM_MEMBERS = "CREATE TABLE "
            + SKYTEAM_MEMBERS + " (" + COL_SNO + " INT NOT NULL,"
            + COL_SKYTEAM_MEMBER + " VARCHAR(50) NOT NULL,"
            + COL_MEMBER_FULLNAME + " VARCHAR(50) NOT NULL," + COL_URL
            + " VARCHAR (100) NOT NULL," + COL_DESCRIPTION
            + "  VARCHAR (500) NOT NULL, " + COL_DESIGNATION + " VARCHAR(100) NOT NULL)";
    private static final long DIFF_MILLIS = 86400000;
    private static final String COL_AIRPORT_DETAILS_URL = "AirportDetails_URL";
    private static final String COL_COUNTRY_NAME = "Country_Name";
    private static final String SKYTIPS_HUBS = "SkyTipshubs";
    private static final String TAG = "DBHELPER";

    private static String CREATE_SKYTIPS = "CREATE TABLE " + SKYTIPS + " ("
            + COL_ID_TIPS + " INTEGER PRIMARY KEY AUTOINCREMENT  ," + COL_HUB
            + " VARCHAR(5) NOT NULL ," + COL_TIP_TITLE + " VARCHAR(50)  ,"
            + COL_TIP_DESC + " VARCHAR(2500) ," + COL_TIP_THEME
            + " VARCHAR(25) ," + COL_NAME + " VARCHAR(50) ," + COL_COUNTRY
            + " VARCHAR(50) NOT NULL ," + COL_DATE + " VARCHAR(25),"+ COL_TIP_THEME_ID +" VARCHAR(100))";
    private static Context context;
    private static SQLiteDatabase db;
    private static String DB_PATH;
    private static DBHelper dbHelper = null;

    private DBHelper(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
        DBHelper.context = context;

        String appDir = context.getFilesDir().getParentFile().getPath();
		
		/*DB_PATH = "/data/data/" + DBHelper.context.getPackageName()	+ "/databases/";*/

        DB_PATH = appDir + "/databases/";
        //System.out.println("dbpath path :"+DB_PATH);

        String myPath = DB_PATH + DB_NAME;
		
		
		/*PackageManager m = context.getPackageManager();
		String s = context.getPackageName();
		
		    PackageInfo p;
			try {
				p = m.getPackageInfo(s, 0);
				s = p.applicationInfo.dataDir;
				System.out.println("str_db path :"+s);
			} catch (NameNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
		    
		

		/*CityAirportJSONFilePath = "/data/data/"	+ DBHelper.context.getPackageName() + "/files/";*/
        CityAirportJSONFilePath = context.getFilesDir() + "/";
        //System.out.println("cityairportfile path :"+CityAirportJSONFilePath);
		
		
		/*
		 * Check if database is already present in the Application.class If not
		 * then copy the database from assets folder
		 */
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            copyDatabase(myPath);
        }

        boolean flatFileSettingsExists = checkFlatFileSettings();

        if(!flatFileSettingsExists) {

            try {
                byte[] buffer = new byte[1024];

                InputStream is_settings = context.getAssets().open(SettingsJSONFileName);
                File file_settings = new File(CityAirportJSONFilePath + SettingsJSONFileName);

                File parent_settings = file_settings.getParentFile();

                if(!parent_settings.exists()){
                    boolean ret = parent_settings.mkdirs();
                }

                if (!file_settings.exists()) {
                    file_settings.createNewFile();
                }
                OutputStream os_settings = new FileOutputStream(CityAirportJSONFilePath + SettingsJSONFileName);
                while (is_settings.read(buffer) > 0) {
                    os_settings.write(buffer);
                }
                os_settings.flush();
                os_settings.close();
                is_settings.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        boolean flatFileCityAirportExists = checkFlatFileCityAirport();

        if(!flatFileCityAirportExists) {

            try {
                byte[] buffer = new byte[1024];
                String strFileName=CityAirportJSONFileName;
                if(getSettingFileObj().getString("Language").equalsIgnoreCase("zh") ){
                    strFileName=CityAirportOtherLangFileName;
                }

                InputStream is_cityAirport = context.getAssets().open(strFileName);
                File file_cityAirport = new File(CityAirportJSONFilePath + CityAirportJSONFileName);

                File parent_cityAirport = file_cityAirport.getParentFile();

                if(!parent_cityAirport.exists()){
                    boolean ret = parent_cityAirport.mkdirs();
                }

                if (!file_cityAirport.exists()) {
                    file_cityAirport.createNewFile();
                }
                OutputStream os_cityAirport = new FileOutputStream(CityAirportJSONFilePath + CityAirportJSONFileName);
                while (is_cityAirport.read(buffer) > 0) {
                    os_cityAirport.write(buffer);
                }
                os_cityAirport.flush();
                os_cityAirport.close();
                is_cityAirport.close();
            }catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        boolean flatFileCityAirportOthersExists = checkFlatFileCityAirportOthers();

        if(flatFileCityAirportOthersExists) {
            File outFile = new File(CityAirportJSONFilePath + CityAirportOtherLangFileName);
            outFile.delete();
	/*		try {
				byte[] buffer = new byte[1024];
				InputStream is_cityAirportOthers = context.getAssets().open(CityAirportOtherLangFileName);
				File file_cityAirportOthers = new File(CityAirportJSONFilePath + CityAirportOtherLangFileName);
				
				File parent_cityAirportOthers = file_cityAirportOthers.getParentFile();
				
				if(!parent_cityAirportOthers.exists()){
					boolean ret = parent_cityAirportOthers.mkdirs();
				}
				
				if (!file_cityAirportOthers.exists()) {
					file_cityAirportOthers.createNewFile();
				}
				OutputStream os_cityAirportOthers = new FileOutputStream(CityAirportJSONFilePath + CityAirportOtherLangFileName);
				while (is_cityAirportOthers.read(buffer) > 0) {
					os_cityAirportOthers.write(buffer);
				}
				os_cityAirportOthers.flush();
				os_cityAirportOthers.close();
				is_cityAirportOthers.close();
			}catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        }



        boolean flatFileSkyTipsCityList = checkFlatFileSkyTipsCityList();

        if(!flatFileSkyTipsCityList) {

            try {
                byte[] buffer = new byte[1024];

                InputStream is_settings = context.getAssets().open(SkyTipsCityAirportJSONFileName);
                File file_settings = new File(CityAirportJSONFilePath + SkyTipsCityAirportJSONFileName);

                File parent_settings = file_settings.getParentFile();

                if(!parent_settings.exists()){
                    boolean ret = parent_settings.mkdirs();
                }

                if (!file_settings.exists()) {
                    file_settings.createNewFile();
                }
                OutputStream os_settings = new FileOutputStream(CityAirportJSONFilePath + SkyTipsCityAirportJSONFileName);
                while (is_settings.read(buffer) > 0) {
                    os_settings.write(buffer);
                }
                os_settings.flush();
                os_settings.close();
                is_settings.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*
     * Only one copy of database instance is used in the application
     */
    public static DBHelper getInstance(Context ctx) {
        if (dbHelper == null) {
            Utils.logMessage(TAG, "initializing db object", Log.DEBUG);
            mContext = ctx;
            dbHelper = new DBHelper(mContext);
        }
        return dbHelper;
    }

    /*
     * Copies the database from assets folder into application
     */
    private void copyDatabase(String myPath) {

        try {
            InputStream is = context.getAssets().open(DB_NAME);
            File myFile = new File(myPath);

            File parent = myFile.getParentFile();

            if(!parent.exists()){
                boolean ret = parent.mkdirs();
            }

            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            OutputStream os = new FileOutputStream(myPath);
            byte[] buffer = new byte[1024];
            while (is.read(buffer) > 0) {
                os.write(buffer);
            }
            os.flush();
            os.close();
            is.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

		/*db.execSQL(CREATE_SAVED_AIRPORTS);
		db.execSQL(CREATE_SAVED_FLIGHTS);
		db.execSQL(CREATE_AIRPORTS_TEST);
		db.execSQL(CREATE_LOUNGES);
		db.execSQL(CREATE_SKYTEAM_MEMBERS);
		db.execSQL(CREATE_SKYTIPS);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }


    /**
     *
     *
     */
    private void refreshSkyTeamMembers(){

        // First clear old data in SkyTeamMembers table
        int rowsDeleted = db.delete(SKYTEAM_MEMBERS, null, null);


        // Insert new data frm DB2 to Old DB

        String myPath = DB_PATH + DB_NAME2;
        try {
            SQLiteDatabase	db2 = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS
                            | SQLiteDatabase.CREATE_IF_NECESSARY);
            if(dbExists()){

                Cursor cursor = db2.query(SKYTEAM_MEMBERS, null, null, null, null, null, null);
                if (cursor != null && cursor.getCount()>0) {
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {

                        ContentValues values = new ContentValues();
                        values.put(COL_SKYTEAM_MEMBER, cursor.getString(cursor.getColumnIndex(COL_SKYTEAM_MEMBER)));
                        values.put(COL_SNO, cursor.getString(cursor.getColumnIndex(COL_SNO)));
                        values.put(COL_MEMBER_FULLNAME,cursor.getString(cursor.getColumnIndex(COL_MEMBER_FULLNAME)));
                        values.put(COL_URL, cursor.getString(cursor.getColumnIndex(COL_URL)));
                        values.put(COL_DESCRIPTION, cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION)));
                        values.put(COL_IMAGES, cursor.getString(cursor.getColumnIndex(COL_IMAGES)));
                        values.put(COL_ID, cursor.getString(cursor.getColumnIndex(COL_ID)));
                        values.put(COL_DESIGNATION,cursor.getString(cursor.getColumnIndex(COL_DESIGNATION)));

                        db.insert(SKYTEAM_MEMBERS, null, values);

                        cursor.moveToNext();
                    }
                }

//				db.insert(SKYTEAM_MEMBERS, null, insertUpgradedSkyTeamRow(db2));
//				insertDesignation(db2);				
            }
            if(db2!=null){
                db2.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Performs the upgrade for recent DB
     */
    private void perfomUpgrade(){
        String myPath = DB_PATH + DB_NAME2;
        try {
            SQLiteDatabase	db2 = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS
                            | SQLiteDatabase.CREATE_IF_NECESSARY);
            if(dbExists()){
                db.insert(SKYTEAM_MEMBERS, null, insertUpgradedSkyTeamRow(db2));
                insertDesignation(db2);
            }
            if(db2!=null){
                db2.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Starts the upgrade for recent DB
     */
    public void updgrade(){
        try{
            String myPath2 = DB_PATH + DB_NAME2;

            if (dbExists()) {
                this.clearSkytips();
                if(checkColumnExists(SKYTEAM_MEMBERS,COL_DESIGNATION)==false){
                    db.execSQL("ALTER TABLE "+SKYTEAM_MEMBERS+" ADD COLUMN "+COL_DESIGNATION + " VARCHAR(100)"); //"new_column INTEGER DEFAULT 0");
                }
                if(checkColumnExists(SKYTIPS_TABLE,COL_TIP_ORIGINAL_LANG) == false){
                    db.execSQL("ALTER TABLE "+SKYTIPS_TABLE+" ADD COLUMN "+COL_TIP_ORIGINAL_LANG + " VARCHAR(100)");
                    db.execSQL("ALTER TABLE "+SKYTIPS_TABLE+" ADD COLUMN "+COL_TIP_TRANSLATED + " VARCHAR(100)");
                    db.execSQL("ALTER TABLE "+SKYTIPS_TABLE+" ADD COLUMN "+COL_AIRPORTNAME + " VARCHAR(100)");
                    db.execSQL("ALTER TABLE "+SKYTIPS_TABLE+" ADD COLUMN "+COL_TIP_THEME_ID + " VARCHAR(100)");
                    db.execSQL("UPDATE "+SKYTIPS_TABLE+" SET "+COL_TIP_ORIGINAL_LANG + "='En'");
                    db.execSQL("UPDATE "+SKYTIPS_TABLE+" SET "+COL_TIP_TRANSLATED + " ='false'");
                    testDBUpgrade();
                }
                if(checkColumnExists(LOUNGES,COL_OPERATING_AIRLINE) == false)
                {

                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_OPERATING_AIRLINE + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_SMOKING_ROOM) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_SMOKING_ROOM + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_KIDS_ROOM) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_KIDS_ROOM + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_MEAL) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_MEAL + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_ACTION_STATION) == false) {

                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_ACTION_STATION + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_APPETIZERS) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_APPETIZERS + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_SNACKS) == false)
                {

                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_SNACKS + " VARCHAR(10)");
                }  if(checkColumnExists(LOUNGES,COL_WINE) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_WINE + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_SPIRITS) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_SPIRITS + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_SOFT_DRINKS) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_SOFT_DRINKS + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_CUSTOMER_SERVICE) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_CUSTOMER_SERVICE + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_BOARDING_ANNOUNCEMENTS) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_BOARDING_ANNOUNCEMENTS + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_FLIGHT_MONITOR) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_FLIGHT_MONITOR + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_TV) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_TV + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_MAGAZINES) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_MAGAZINES + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_SILENT_AREA) == false)
                {
                     db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_SILENT_AREA + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_BUSINESS_CENTER) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_BUSINESS_CENTER + " VARCHAR(10)");
                }
                if(checkColumnExists(LOUNGES,COL_MEETING_CONFERENCE_ROOM) == false)
                {
                    db.execSQL("ALTER TABLE "+LOUNGES+" ADD COLUMN "+COL_MEETING_CONFERENCE_ROOM + " VARCHAR(10)");
                }

            }
            copyDatabase(myPath2);
//				perfomUpgrade();	
            refreshSkyTeamMembers();
            File myFile = new File(myPath2);
            if(myFile.exists()){
                boolean del=	myFile.delete();
            }
        }catch(Exception e2){
            e2.printStackTrace();
        }
    }
    /**
     * Inserts the recent SkyTeam column info into existing DB
     * @param db2
     */
    private void insertDesignation(SQLiteDatabase	db2){
        String query = null;
        String where=null;
        ContentValues values=new ContentValues();
        query = "select " + COL_SNO + "," + COL_SKYTEAM_MEMBER + ","
                + COL_DESIGNATION + " from "
                + SKYTEAM_MEMBERS;
        Cursor cur = db2.rawQuery(query, null);
        if(cur!=null){
            int rows=cur.getCount();
            for(int i=0;i<rows;i++){
                cur.moveToNext();
                values.put(COL_DESIGNATION, cur.getString(2));
                where=COL_SNO+"='"+cur.getString(0)+"'&"+COL_SKYTEAM_MEMBER+"='"+cur.getString(1)+"'";
                db.update(SKYTEAM_MEMBERS, values, where, null);
            }
        }
    }
    private void testDBUpgrade(){
        String query = null;
        String where=null;
        ContentValues values=new ContentValues();
        query = "select " + COL_TIP_ORIGINAL_LANG + "," + COL_AIRPORTNAME + ","
                + COL_TIP_TRANSLATED +","+COL_TIP_THEME_ID+ " from "
                + SKYTIPS_TABLE;
        Cursor cur = db.rawQuery(query, null);
        if(cur!=null){
            int rows=cur.getCount();
            if(rows > 0){
                cur.moveToNext();


                where="T_O_L="+cur.getString(0)+" A_N="+cur.getString(1)+"T_T="+cur.getString(2)+"T_T_I="+cur.getString(3);
            }
        }
    }
    /**
     * Inserts the recent SkyTeam column info into existing DB
     * @param
     */
    private Boolean checkColumnExists(String table, String column){
        String query = null;
        int colExists=0;
        query = "select * from "+ table;
        Cursor cur = db.rawQuery(query, null);
        if(cur!=null){
            colExists=cur.getColumnIndex(column);
        }
        if(colExists == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Inserts the recent SkyTeam row into existing DB
     * @param db2
     * @return
     */
    private ContentValues insertUpgradedSkyTeamRow(SQLiteDatabase	db2){
        String query = null;

        ContentValues values=new ContentValues();
        query = "select " + COL_SNO + "," + COL_SKYTEAM_MEMBER + ","
                + COL_MEMBER_FULLNAME + "," + COL_URL + ","
                + COL_DESCRIPTION + "," + COL_IMAGES + ","+ COL_ID + "," + COL_DESIGNATION + " from "
                + SKYTEAM_MEMBERS + " where " + COL_SKYTEAM_MEMBER + " ='"
                + "Garuda Indonesia" + "'";

        Cursor cur = db2.rawQuery(query, null);
        if(cur!=null){
            cur.moveToNext();

            values.put(COL_SKYTEAM_MEMBER, cur.getString(1));
            values.put(COL_SNO, cur.getString(0));
            values.put(COL_MEMBER_FULLNAME, cur.getString(2));
            values.put(COL_URL, cur.getString(3));
            values.put(COL_DESCRIPTION, cur.getString(4));
            values.put(COL_IMAGES, cur.getString(5));
            values.put(COL_ID, cur.getString(6));
            values.put(COL_DESIGNATION, cur.getString(7));
        }

        return values;
    }

    public DBHelper open() {
        String myPath = DB_PATH + DB_NAME;
        try {

            db = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS
                            | SQLiteDatabase.CREATE_IF_NECESSARY);
            return this;
        } catch (SQLException e) {
            // First close current state of database and then open again
            e.printStackTrace();
            this.close();
            db.close();
            db = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS
                            | SQLiteDatabase.CREATE_IF_NECESSARY);
            return this;
        } catch (Exception e) {
            // First close current state of database and then open again
            e.printStackTrace();
            this.close();
            db.close();
            db = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE
                            | SQLiteDatabase.NO_LOCALIZED_COLLATORS
                            | SQLiteDatabase.CREATE_IF_NECESSARY);
            return this;
        }
    }

    /**
     * Close any open database object
     */
    public void close() {
        db.close();
    }

    /**
     * Check db exists
     */
    public boolean dbExists() {
        return db == null ? false:true;
    }

    public JSONObject getSavedAirportsNFlights() {
        JSONObject master = new JSONObject();
        JSONArray savedAirportsArr = new JSONArray();
        JSONArray savedFlightsArr = new JSONArray();

        String SELECT_SAVED_AIRPORTS = "select " + COL_AIRPORT_NAME + ","
                + COL_LOCATION_CODE + "," + COL_COUNTRY_CODE + "," + COL_ID
                + "," + COL_AIRPORT_DETAILS_URL + "," + COL_CITY_NAME + ","
                + COL_COUNTRY_NAME + "," + COL_LOCAL_TIME + ","
                + COL_TEMPRATURE + "," + COL_WEATHER_CODE + ","
                + COL_LOUNGES_COUNT + "," + COL_SKYTIPS_COUNT + ","
                + COL_AIRLINES_COUNT + " from " + SAVED_AIRPORTS + " order by " + COL_ID + " desc";

        Cursor cur = db.rawQuery(SELECT_SAVED_AIRPORTS, null);
        if (cur == null || cur.getCount() == 0) {

        } else {
            cur.moveToFirst();

            do {
                Map<String, String> obj = new LinkedHashMap<String, String>();
                obj.put(COL_COUNTRY_CODE, cur.getString(2));
                obj.put(COL_AIRPORT_NAME, cur.getString(0));
                obj.put(COL_LOCATION_CODE, cur.getString(1));
                obj.put(COL_ID, cur.getString(3));
                obj.put(COL_AIRPORT_DETAILS_URL, cur.getString(4));
                obj.put(COL_CITY_NAME, cur.getString(5));
                obj.put(COL_COUNTRY_NAME, cur.getString(6));
                obj.put(COL_LOCAL_TIME, cur.getString(7));
                obj.put(COL_TEMPRATURE, cur.getString(8));
                obj.put(COL_WEATHER_CODE, cur.getString(9));
                obj.put(COL_LOUNGES_COUNT, cur.getString(10));
                obj.put(COL_SKYTIPS_COUNT, cur.getString(11));
                obj.put(COL_AIRLINES_COUNT, cur.getString(12));

                JSONObject savedAirports = new JSONObject(obj);
                savedAirportsArr.put(savedAirports);

            } while (cur.moveToNext());

        }
        cur.close();
        String SELECT_SAVED_FLIGHTS = "select " + COL_FLIGHT_NUM + ","
                + COL_ARRIVAL_TIME + "," + COL_DEPARTURE_TIME + ","
                + COL_DEPARTURE_DATE + "," + COL_NO_OF_STOPS + ","
                + COL_ARRIVAL_DATE + "," + COL_DEPARTURE + "," + COL_ARRIVAL
                + "," + COL_ID + "," + COL_DETAILS + ","
                + COL_DEPARTURE_CITY_NAME + "," + COL_ARRIVAL_CITY_NAME + ","
                + COL_FLIGHT_STATUS + "," + COL_FLIGHT_STATUS_URL + " from "
                + SAVED_FLIGHTS + " order by " + COL_ID + " desc";

        cur = db.rawQuery(SELECT_SAVED_FLIGHTS, null);
        if (cur == null || cur.getCount() == 0) {

        } else {
            cur.moveToFirst();

            do {

                try {
                    JSONObject savedFlights = new JSONObject();
                    savedFlights.put(COL_FLIGHT_NUM, cur.getString(0));
                    savedFlights.put(COL_ARRIVAL_TIME, cur.getString(1));
                    savedFlights.put(COL_DEPARTURE_TIME, cur.getString(2));
                    savedFlights.put(COL_DEPARTURE_DATE, cur.getString(3));
                    savedFlights.put(COL_NO_OF_STOPS, cur.getString(4));
                    savedFlights.put(COL_ARRIVAL_DATE, cur.getString(5));
                    savedFlights.put(COL_DEPARTURE, cur.getString(6));
                    savedFlights.put(COL_ARRIVAL, cur.getString(7));
                    savedFlights.put(COL_ID, cur.getString(8));
                    savedFlights.put(COL_DETAILS, cur.getString(9));
                    savedFlights
                            .put(COL_DEPARTURE_CITY_NAME, cur.getString(10));
                    savedFlights.put(COL_ARRIVAL_CITY_NAME, cur.getString(11));
                    savedFlights.put(COL_FLIGHT_STATUS, cur.getString(12));
                    savedFlights.put(COL_FLIGHT_STATUS_URL, cur.getString(13));
                    savedFlightsArr.put(savedFlights);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (cur.moveToNext());

        }
        cur.close();

        try {
            master.put("SavedAirports_test", savedAirportsArr);
            master.put("SavedFlights_test", savedFlightsArr);
            master.put("url", StringConstants.SAVED_AIRPORTS_URL);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage(TAG, "getSavedAirportsNFlights returns json as : "
                + master.toString(), Log.DEBUG);

        return master;
    }




//	public ArrayList<Location> getAllAirportsFromJson() {
//
//	   Gson gson = new Gson();
//	   HashMap<String,Location>  map = new HashMap<String,Location>();
//	   try {
//	   List<Location> airports = new ArrayList<Location>();
//	   CityAirportJSONFilePath = context.getFilesDir() + "/";
//		File engFile = new File(CityAirportJSONFilePath + CityAirportJSONFileName);
//		JsonReader reader=new JsonReader(new InputStreamReader(new FileInputStream(engFile)));
//		reader.beginArray();
//	       while (reader.hasNext()) {
//	    	   Location airport = gson.fromJson(reader, Location.class);
//	    	   if( airport != null && (airport.getLocationType().equalsIgnoreCase("Airport"))){
//	    		   map.put(airport.getAirportCode(), airport);
//	    	   }
//	       }
//	       reader.endArray();
//
//
//	} catch (FileNotFoundException e) {
//		e.printStackTrace();
//	}
//	   catch(MalformedJsonException e)
//	   {
//		   e.printStackTrace();
//	   } catch(JsonSyntaxException e)
//	   {
//		   e.printStackTrace();
//	   }
//	   catch (IOException e) {
//
//		e.printStackTrace();
//	}
//	   ArrayList<Location> list = new ArrayList<Location>(map.values());
//
//
//	   Collections.sort(list, new Comparator<Location>(){
//
//		@Override
//		public int compare(Location lhs, Location rhs) {
//			 return lhs.getAirportName().compareTo(rhs.getAirportName());
//		}
//
//	   });
//
//	return list;
//
//
//}


    public ArrayList<Location> getAllAirportsFromJson() {

        Gson gson = new Gson();
        AirportList airportList = new AirportList();

        InputStream is = null;
        try {
            is = new FileInputStream(CityAirportJSONFilePath + CityAirportJSONFileName);
            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            String json = new String(buffer, "UTF-8");
            JsonReader reader = new JsonReader(new StringReader(json));
            reader.setLenient(true);
            airportList = gson.fromJson(reader, AirportList.class);
        } catch (IOException e) {
            e.printStackTrace();
        }


        List<Location> allAirports = airportList.getLocations();
        ArrayList<Location> airports = new ArrayList<Location>();

        Iterator<Location> airportsIterator = allAirports.iterator();
        while(airportsIterator.hasNext()) {
            Location copyAirport = airportsIterator.next();

            if ((copyAirport!=null)&&copyAirport.getLocationType().equalsIgnoreCase("APT")){
                airports.add(copyAirport);
            }
        }
        return airports;
    }

	/*@SuppressWarnings("resource")
	public JSONObject getCityAirport() {

		JSONObject master = new JSONObject();


		try {

            Gson gson= new Gson();
			InputStream is = new FileInputStream(CityAirportJSONFilePath + CityAirportJSONFileName);

			Scanner s = new Scanner(is).useDelimiter("\\A");

			String str = s.hasNext() ? s.next() : "";

            JsonReader reader = new JsonReader(new StringReader(str));
            reader.setLenient(true);
            AirportList airportList = gson.fromJson(reader, AirportList.class);

			master.put("AirPorts_lang", new JSONArray(gson.toJson( airportList.getLocations())));
			master.put("url", StringConstants.TEST_URL);

*//*			File ofile = new File(CityAirportJSONFilePath + CityAirportOtherLangFileName);

			if (ofile.exists()) {
				is = new FileInputStream(CityAirportJSONFilePath + CityAirportOtherLangFileName);
				s = new Scanner(is).useDelimiter("\\A");
				String strr = s.hasNext() ? s.next() : "";
				master.put("AirPorts_lang", new JSONArray(strr));
			}
			*//*

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return master;
	}*/

    @SuppressWarnings("resource")
    public JSONObject getCityAirport() {

        JSONObject master = new JSONObject();


        try {

            InputStream is = new FileInputStream(CityAirportJSONFilePath + CityAirportJSONFileName);

            Scanner s = new Scanner(is).useDelimiter("\\A");

            String str = s.hasNext() ? s.next() : "";

            JSONObject obj = new JSONObject(str);
            JSONArray jsonArray = obj.getJSONArray("locations");

            master.put("AirPorts_lang", jsonArray);
//            master.put("AirPorts_lang", new JSONArray(str));
            master.put("url", StringConstants.TEST_URL);

			/*File ofile = new File(CityAirportJSONFilePath + CityAirportOtherLangFileName);

			if (ofile.exists()) {
				is = new FileInputStream(CityAirportJSONFilePath + CityAirportOtherLangFileName);
				s = new Scanner(is).useDelimiter("\\A");
				String strr = s.hasNext() ? s.next() : "";
				master.put("AirPorts_lang", new JSONArray(strr));
			}*/


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return master;
    }

    public JSONObject getUpdatedCityAirport() {

        JSONObject master = new JSONObject();


        try {

            InputStream is = new FileInputStream(CityAirportJSONFilePath + CityAirportJSONFileName);

            Scanner s = new Scanner(is).useDelimiter("\\A");

            String str = s.hasNext() ? s.next() : "";

            JSONObject obj=new JSONObject(str);
            JSONArray jsonArray = obj.getJSONArray("locations");

            master.put("AirPorts_lang", jsonArray);
            master.put("url", StringConstants.CITYLIST_UPDATE_URL);
            master.put("Result", "Success");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return master;
    }

    public void deleteSavedFlights(String str) {

        String query = "delete from " + SAVED_FLIGHTS + " where Id='" + str
                + "'";
        db.execSQL(query);
    }

    public JSONObject getSavedFlightslist() {
        JSONObject master = new JSONObject();
        JSONArray savedFlightsArr = new JSONArray();

        String SELECT_SAVED_FLIGHTS = "select " + COL_FLIGHT_NUM + ","
                + COL_ARRIVAL_TIME + "," + COL_DEPARTURE_TIME + ","
                + COL_DEPARTURE_DATE + "," + COL_ID + "," + COL_ARRIVAL_DATE
                + "," + COL_NO_OF_STOPS + "," + COL_DEPARTURE + ","
                + COL_ARRIVAL + "," + COL_DETAILS + ","
                + COL_DEPARTURE_CITY_NAME + "," + COL_ARRIVAL_CITY_NAME + ","
                + COL_FLIGHT_STATUS + "," + COL_FLIGHT_STATUS_URL + " from "
                + SAVED_FLIGHTS + " order by " + COL_ID + " desc";

        Cursor cur = db.rawQuery(SELECT_SAVED_FLIGHTS, null);
        if (cur == null || cur.getCount() == 0) {

        } else {
            cur.moveToFirst();

            do {

                try {
                    JSONObject savedFlights = new JSONObject();
                    savedFlights.put(COL_FLIGHT_NUM, cur.getString(0));
                    savedFlights.put(COL_ARRIVAL_TIME, cur.getString(1));
                    savedFlights.put(COL_DEPARTURE_TIME, cur.getString(2));
                    savedFlights.put(COL_DEPARTURE_DATE, cur.getString(3));
                    savedFlights.put(COL_ID, cur.getString(4));
                    savedFlights.put(COL_ARRIVAL_DATE, cur.getString(5));
                    savedFlights.put(COL_NO_OF_STOPS, cur.getString(6));
                    savedFlights.put(COL_DEPARTURE, cur.getString(7));
                    savedFlights.put(COL_ARRIVAL, cur.getString(8));
                    savedFlights.put(COL_DETAILS, cur.getString(9));
                    savedFlights
                            .put(COL_DEPARTURE_CITY_NAME, cur.getString(10));
                    savedFlights.put(COL_ARRIVAL_CITY_NAME, cur.getString(11));
                    savedFlights.put(COL_FLIGHT_STATUS, cur.getString(12));
                    savedFlights.put(COL_FLIGHT_STATUS_URL, cur.getString(13));

                    savedFlightsArr.put(savedFlights);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (cur.moveToNext());
        }
        cur.close();

        try {
            master.put("SavedFlights_test", savedFlightsArr);
            master.put("url", StringConstants.DELETE_FLIGHTS_URL);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage(TAG,
                "getSavedFlightslist returns json as : " + master.toString(),
                Log.DEBUG);
        return master;

    }

    public void deleteSavedAirports(String str) {
        String query = "delete from " + SAVED_AIRPORTS + " where Id='" + str
                + "'";

        db.execSQL(query);
    }

    public JSONObject getSavedAirportslist() {
        JSONObject master = new JSONObject();
        JSONArray savedAirportsArr = new JSONArray();

        String SELECT_SAVED_AIRPORTS = "select " + COL_LOCATION_CODE + ","
                + COL_COUNTRY_CODE + "," + COL_AIRPORT_NAME + "," + COL_ID
                + "," + COL_AIRPORT_DETAILS_URL + "," + COL_COUNTRY_NAME + ","
                + COL_CITY_NAME + "," + COL_LOCAL_TIME + "," + COL_TEMPRATURE
                + "," + COL_WEATHER_CODE + "," + COL_LOUNGES_COUNT + ","
                + COL_SKYTIPS_COUNT + "," + COL_AIRLINES_COUNT + " from "
                + SAVED_AIRPORTS + " order by " + COL_ID + " desc";

        Cursor cur = db.rawQuery(SELECT_SAVED_AIRPORTS, null);
        if (cur == null || cur.getCount() == 0) {

        } else {

            cur.moveToFirst();

            do {

                try {
                    JSONObject savedAirports = new JSONObject();
                    savedAirports.put(COL_LOCATION_CODE, cur.getString(0));
                    savedAirports.put(COL_COUNTRY_CODE, cur.getString(1));
                    savedAirports.put(COL_AIRPORT_NAME, cur.getString(2));
                    savedAirports.put(COL_ID, cur.getString(3));
                    savedAirports
                            .put(COL_AIRPORT_DETAILS_URL, cur.getString(4));
                    savedAirports.put(COL_COUNTRY_NAME, cur.getString(5));
                    savedAirports.put(COL_CITY_NAME, cur.getString(6));
                    savedAirports.put(COL_LOCAL_TIME, cur.getString(7));
                    savedAirports.put(COL_TEMPRATURE, cur.getString(8));
                    savedAirports.put(COL_WEATHER_CODE, cur.getString(9));
                    savedAirports.put(COL_LOUNGES_COUNT, cur.getString(10));
                    savedAirports.put(COL_SKYTIPS_COUNT, cur.getString(11));
                    savedAirports.put(COL_AIRLINES_COUNT, cur.getString(12));

                    savedAirportsArr.put(savedAirports);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (cur.moveToNext());
        }

        cur.close();

        try {
            master.put("SavedAirports_test", savedAirportsArr);
            master.put("url", StringConstants.DELETE_AIRPORTS_URL);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage(TAG, "getSavedAirportslist returns json as : "
                + master.toString(), Log.DEBUG);

        return master;

    }

    public JSONObject getLounges(String str) {
        JSONObject master = new JSONObject();
        JSONArray loungesArr = new JSONArray();
        String SELECT_LOUNGES = "select " + COL_LOUNGE_ID + ","
                + COL_AIRLINE_CODE + "," + COL_AIRPORT_CODE + "," + COL_AIRPORTNAME + ","
                + COL_COUNTRY + "," + COL_AIRPORT_CITY + "," + COL_LOUNGE_NAME + ","
                + COL_LOUNGE_LOCATION + "," + COL_HOURS_OF_OPERATION + "," + COL_OPENING_TIME + ","
                + COL_CLOSING_TIME + "," + COL_DAYS_OPEN + "," + COL_SHOWER + ","
                + COL_FIXED_INTERNET_ACCESS + "," + COL_FREE_WIFI + "," + COL_PAID_WIFI + ","
                + COL_ACCESS_TO_DISABLED + "," + COL_SKYTEAM_MEMBER_OWNED + "," + COL_CONTRACTED + ","
                + COL_ADDITIONAL_INFO + "," + COL_OPERATING_AIRLINE + "," + COL_SMOKING_ROOM +  ","
                + COL_KIDS_ROOM + "," + COL_MEAL + "," + COL_ACTION_STATION + "," + COL_APPETIZERS + ","
                + COL_SNACKS + "," + COL_WINE + "," + COL_SPIRITS + ","
                + COL_SOFT_DRINKS + "," + COL_CUSTOMER_SERVICE + "," + COL_BOARDING_ANNOUNCEMENTS + ","
                + COL_FLIGHT_MONITOR + ","
                + COL_TV + "," + COL_MAGAZINES + "," + COL_SILENT_AREA + ","
                + COL_BUSINESS_CENTER + "," + COL_MEETING_CONFERENCE_ROOM +
                " from " + LOUNGES + " where "
                + COL_AIRPORT_CODE + "='" + str + "'";

        Cursor cur = db.rawQuery(SELECT_LOUNGES, null);
        if (cur == null || cur.getCount() == 0) {
            cur.close();

            try {
                loungesArr.put("No Offline Data");
                master.put("LoungeData", loungesArr);
                master.put("url", StringConstants.LOUNGE_FINDER_URL);
                master.put("Count", 0);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        else {
            cur.moveToFirst();

            int count = 0;

            do {

                try {
                    JSONObject lounges = new JSONObject();
                    lounges.put(COL_LOUNGE_ID, cur.getString(0));
                    lounges.put(COL_AIRLINE_CODE, cur.getString(1));
                    lounges.put(COL_AIRPORT_CODE, cur.getString(2));
                    lounges.put(COL_AIRPORTNAME, cur.getString(3));
                    lounges.put(COL_COUNTRY, cur.getString(4));
                    lounges.put(COL_AIRPORT_CITY, cur.getString(5));
                    lounges.put(COL_LOUNGE_NAME, cur.getString(6));
                    lounges.put(COL_LOUNGE_LOCATION, cur.getString(7));
                    lounges.put(COL_HOURS_OF_OPERATION, cur.getString(8));
                    lounges.put(COL_OPENING_TIME, cur.getString(9));
                    lounges.put(COL_CLOSING_TIME, cur.getString(10));
                    lounges.put(COL_DAYS_OPEN, cur.getString(11));
                    lounges.put(COL_SHOWER, cur.getString(12));
                    lounges.put(COL_FIXED_INTERNET_ACCESS, cur.getString(13));
                    lounges.put(COL_FREE_WIFI, cur.getString(14));
                    lounges.put(COL_PAID_WIFI, cur.getString(15));
                    lounges.put(COL_ACCESS_TO_DISABLED, cur.getString(16));
                    lounges.put(COL_SKYTEAM_MEMBER_OWNED, cur.getString(17));
                    lounges.put(COL_CONTRACTED, cur.getString(18));
                    lounges.put(COL_ADDITIONAL_INFO, cur.getString(19));
                    lounges.put(COL_OPERATING_AIRLINE,cur.getString(20));
                    lounges.put(COL_SMOKING_ROOM,cur.getString(21));
                    lounges.put(COL_KIDS_ROOM,cur.getString(22));
                    lounges.put(COL_MEAL,cur.getString(23));
                    lounges.put(COL_ACTION_STATION,cur.getString(24));
                    lounges.put(COL_APPETIZERS,cur.getString(25));
                    lounges.put(COL_SNACKS,cur.getString(26));
                    lounges.put(COL_WINE,cur.getString(27));
                    lounges.put(COL_SPIRITS,cur.getString(28));
                    lounges.put(COL_SOFT_DRINKS,cur.getString(29));
                    lounges.put(COL_CUSTOMER_SERVICE,cur.getString(30));
                    lounges.put(COL_BOARDING_ANNOUNCEMENTS,cur.getString(31));
                    lounges.put(COL_FLIGHT_MONITOR,cur.getString(32));
                    lounges.put(COL_TV,cur.getString(33));
                    lounges.put(COL_MAGAZINES,cur.getString(34));
                    lounges.put(COL_SILENT_AREA,cur.getString(35));
                    lounges.put(COL_BUSINESS_CENTER,cur.getString(36));
                    lounges.put(COL_MEETING_CONFERENCE_ROOM,cur.getString(37));


                    loungesArr.put(lounges);

                    count++;
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (cur.moveToNext());

            cur.close();

            try {
                master.put("LoungeData", loungesArr);
                master.put("url", StringConstants.LOUNGE_FINDER_URL);
                master.put("Count", count);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        Utils.logMessage(TAG,
                "getLounges returns json as : " + master.toString(), Log.DEBUG);
        return master;

    }

    public void deleteFromLounges(String airportCode) {

        db.execSQL("delete from Lounges  where " + COL_AIRPORT_CODE + "='"
                + airportCode + "'");

    }

    public void deleteFromSkytips(String[] params) {

        String query = null;

        if (params[1] != null && params[1].length() > 0) // AirportCode
        {
            query = "delete from SkyTips where " + COL_HUB + " ='" + params[1]
                    + "'";
        }

        if (query != null) {
            db.execSQL(query);
        }

    }

    public JSONArray selectDistinctTipTheme() {

        Cursor cur = db.query(true, SKYTIPS, new String[] { COL_TIP_THEME },
                null, null, null, null, null, null);
        JSONArray themeArr = new JSONArray();

        if (cur == null || cur.getCount() == 0) {
            cur.close();

        }

        else {
            cur.moveToFirst();

            do {
                if (cur.getString(0) != null && cur.getString(0).length() > 0) {
                    Utils.logMessage(TAG, "Adding theme: " + cur.getString(0),
                            Log.DEBUG);
                    themeArr.put(cur.getString(0));
                }
            } while (cur.moveToNext());

            cur.close();
        }
        return themeArr;

    }

    public void insertIntoLounges(String lounges) {

        Log.d("inside db",lounges);
        try {
            JSONObject loungesObj = new JSONObject(lounges);
            JSONArray loungeArr = new JSONArray();

            if ((loungesObj.get("LoungeData").getClass().getSimpleName())
                    .equalsIgnoreCase("JSONObject")) {
                loungeArr.put(loungesObj.getJSONObject("LoungeData"));
            } else {
                loungeArr = loungesObj.getJSONArray("LoungeData");
            }

            int count = loungeArr.length();
            ContentValues val = new ContentValues();

            for (int i = 0; i < count; i++) {
                JSONObject temp = (JSONObject) loungeArr.get(i);

                val.put(COL_CLOSING_TIME,
                        !temp.isNull(COL_CLOSING_TIME)
                                && !temp.getString(COL_CLOSING_TIME)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_CLOSING_TIME) : "");
                val.put(COL_LOUNGE_NAME,
                        !temp.isNull(COL_LOUNGE_NAME)
                                && !temp.getString(COL_LOUNGE_NAME)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_LOUNGE_NAME) : "");
                val.put(COL_PAID_WIFI,
                        !temp.isNull(COL_PAID_WIFI)
                                && !temp.getString(COL_PAID_WIFI)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_PAID_WIFI) : "");
                val.put(COL_HOURS_OF_OPERATION,
                        !temp.isNull(COL_HOURS_OF_OPERATION)
                                && !temp.getString(COL_HOURS_OF_OPERATION)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_HOURS_OF_OPERATION) : "");
                val.put(COL_DAYS_OPEN,
                        !temp.isNull(COL_DAYS_OPEN)
                                && !temp.getString(COL_DAYS_OPEN)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_DAYS_OPEN) : "");
                val.put(COL_AIRPORTNAME,
                        !temp.isNull(COL_AIRPORTNAME)
                                && !temp.getString(COL_AIRPORTNAME)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_AIRPORTNAME) : "");
                val.put(COL_OPENING_TIME,
                        !temp.isNull(COL_OPENING_TIME)
                                && !temp.getString(COL_OPENING_TIME)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_OPENING_TIME) : "");
                val.put(COL_FREE_WIFI,
                        !temp.isNull(COL_FREE_WIFI)
                                && !temp.getString(COL_FREE_WIFI)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_FREE_WIFI) : "");
                val.put(COL_SKYTEAM_MEMBER_OWNED,
                        !temp.isNull(COL_SKYTEAM_MEMBER_OWNED)
                                && !temp.getString(COL_SKYTEAM_MEMBER_OWNED)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SKYTEAM_MEMBER_OWNED) : "");
                val.put(COL_SHOWER,
                        !temp.isNull(COL_SHOWER)
                                && !temp.getString(COL_SHOWER)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SHOWER) : "");
                val.put(COL_ACCESS_TO_DISABLED,
                        !temp.isNull(COL_ACCESS_TO_DISABLED)
                                && !temp.getString(COL_ACCESS_TO_DISABLED)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_ACCESS_TO_DISABLED) : "");
                val.put(COL_LOUNGE_ID,
                        !temp.isNull(COL_LOUNGE_ID)
                                && !temp.getString(COL_LOUNGE_ID)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_LOUNGE_ID) : "");
                val.put(COL_FIXED_INTERNET_ACCESS,
                        !temp.isNull(COL_FIXED_INTERNET_ACCESS)
                                && !temp.getString(COL_FIXED_INTERNET_ACCESS)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_FIXED_INTERNET_ACCESS) : "");
                val.put(COL_CONTRACTED,
                        !temp.isNull(COL_CONTRACTED)
                                && !temp.getString(COL_CONTRACTED)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_CONTRACTED) : "");
                val.put(COL_LOUNGE_LOCATION,
                        !temp.isNull(COL_LOUNGE_LOCATION)
                                && !temp.getString(COL_LOUNGE_LOCATION)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_LOUNGE_LOCATION) : "");
                val.put(COL_AIRLINE_CODE,
                        !temp.isNull(COL_AIRLINE_CODE)
                                && !temp.getString(COL_AIRLINE_CODE)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_AIRLINE_CODE) : "");
                val.put(COL_COUNTRY,
                        !temp.isNull(COL_COUNTRY)
                                && !temp.getString(COL_COUNTRY)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_COUNTRY) : "");
                val.put(COL_AIRPORT_CITY,
                        !temp.isNull(COL_AIRPORT_CITY)
                                && !temp.getString(COL_AIRPORT_CITY)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_AIRPORT_CITY) : "");
                val.put(COL_ADDITIONAL_INFO,
                        !temp.isNull(COL_ADDITIONAL_INFO)
                                && !temp.getString(COL_ADDITIONAL_INFO)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_ADDITIONAL_INFO) : "");
                val.put(COL_AIRPORT_CODE,
                        !temp.isNull(COL_AIRPORT_CODE)
                                && !temp.getString(COL_AIRPORT_CODE)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_AIRPORT_CODE) : "");
                val.put(COL_OPERATING_AIRLINE,
                        !temp.isNull(COL_OPERATING_AIRLINE)
                                && !temp.getString(COL_OPERATING_AIRLINE)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_OPERATING_AIRLINE) : "");
                val.put(COL_SMOKING_ROOM,
                        !temp.isNull(COL_SMOKING_ROOM)
                                && !temp.getString(COL_SMOKING_ROOM)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SMOKING_ROOM) : "");
                val.put(COL_KIDS_ROOM,
                        !temp.isNull(COL_KIDS_ROOM)
                                && !temp.getString(COL_KIDS_ROOM)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_KIDS_ROOM) : "");
                val.put(COL_MEAL,
                        !temp.isNull(COL_MEAL)
                                && !temp.getString(COL_MEAL)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_MEAL) : "");
                val.put(COL_ACTION_STATION,
                        !temp.isNull(COL_ACTION_STATION)
                                && !temp.getString(COL_ACTION_STATION)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_ACTION_STATION) : "");
                val.put(COL_APPETIZERS,
                        !temp.isNull(COL_APPETIZERS)
                                && !temp.getString(COL_APPETIZERS)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_APPETIZERS) : "");
                val.put(COL_SNACKS,
                        !temp.isNull(COL_SNACKS)
                                && !temp.getString(COL_SNACKS)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SNACKS) : "");
                val.put(COL_WINE,
                        !temp.isNull(COL_WINE)
                                && !temp.getString(COL_WINE)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_WINE) : "");
                val.put(COL_SPIRITS,
                        !temp.isNull(COL_SPIRITS)
                                && !temp.getString(COL_SPIRITS)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SPIRITS) : "");
                val.put(COL_SOFT_DRINKS,
                        !temp.isNull(COL_SOFT_DRINKS)
                                && !temp.getString(COL_SOFT_DRINKS)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SOFT_DRINKS) : "");
                val.put(COL_CUSTOMER_SERVICE,
                        !temp.isNull(COL_CUSTOMER_SERVICE)
                                && !temp.getString(COL_CUSTOMER_SERVICE)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_CUSTOMER_SERVICE) : "");
                val.put(COL_BOARDING_ANNOUNCEMENTS,
                        !temp.isNull(COL_BOARDING_ANNOUNCEMENTS)
                                && !temp.getString(COL_BOARDING_ANNOUNCEMENTS)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_BOARDING_ANNOUNCEMENTS) : "");
                val.put(COL_FLIGHT_MONITOR,
                        !temp.isNull(COL_FLIGHT_MONITOR)
                                && !temp.getString(COL_FLIGHT_MONITOR)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_FLIGHT_MONITOR) : "");
                val.put(COL_TV,
                        !temp.isNull(COL_TV)
                                && !temp.getString(COL_TV)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_TV) : "");
                val.put(COL_MAGAZINES,
                        !temp.isNull(COL_MAGAZINES)
                                && !temp.getString(COL_MAGAZINES)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_MAGAZINES) : "");
                val.put(COL_SILENT_AREA,
                        !temp.isNull(COL_SILENT_AREA)
                                && !temp.getString(COL_SILENT_AREA)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_SILENT_AREA) : "");
                val.put(COL_BUSINESS_CENTER,
                        !temp.isNull(COL_BUSINESS_CENTER)
                                && !temp.getString(COL_BUSINESS_CENTER)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_BUSINESS_CENTER) : "");
                val.put(COL_MEETING_CONFERENCE_ROOM,
                        !temp.isNull(COL_MEETING_CONFERENCE_ROOM)
                                && !temp.getString(COL_MEETING_CONFERENCE_ROOM)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_MEETING_CONFERENCE_ROOM) : "");
                if (dbExists())
                {
                    db.insert(LOUNGES, null, val);
                }
                else
                {
                    Log.d("DBHelper"," test 2");
                }


            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
     * This method returns the earliest flight from the saved flights list to be
     * displayed in the home screen. If more than one flight with same departure
     * date and time are there, then check is based on latest saved time.
     */
//    public JSONObject getSavedFlightsHomeScreen() {
//
//        JSONObject master = new JSONObject();
//        JSONArray savedFlightsArr = new JSONArray();
//        JSONArray tempSavedFlightsArr = new JSONArray();
//
//        // Get the current data n time
//        Calendar cal = Calendar.getInstance();
//        long time = cal.getTimeInMillis() - DIFF_MILLIS; /*
//														 * The check has to be
//														 * made based on
//														 * previous date
//														 */
//        cal.setTimeInMillis(time);
//        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
//        day=(day.length()==1)?("0"+day):day;
//        //String month = String.valueOf(getMonth(cal.get(Calendar.MONTH)));
//        int mon = (cal.get(Calendar.MONTH))+1;
//        String month=(mon<10)?("0"+mon):String.valueOf(mon);
//        String year = String.valueOf(cal.get(Calendar.YEAR));
//        String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
//        String minute = String.valueOf(cal.get(Calendar.MINUTE));
//
//        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy HH:mm",
//        //Locale.US);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm",
//                Locale.US);
//
//        Date currentDay = null;
//        try {
//            currentDay = sdf.parse(year + "-" + month + "-" + day + " " + hour
//                    + ":" + minute);
//			/*currentDay = sdf.parse(day + "/" + month + "/" + year + " " + hour
//					+ ":" + minute);*/
//        } catch (ParseException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
//
//        String SELECT_SAVED_FLIGHTS = "select " + COL_FLIGHT_NUM + ","
//                + COL_ARRIVAL_TIME + "," + COL_DEPARTURE_TIME + ","
//                + COL_DEPARTURE_DATE + "," + COL_NO_OF_STOPS + ","
//                + COL_ARRIVAL_DATE + "," + COL_DEPARTURE + "," + COL_ARRIVAL
//                + "," + COL_ID + "," + COL_DETAILS + ","
//                + COL_DEPARTURE_CITY_NAME + "," + COL_ARRIVAL_CITY_NAME + ","
//                + COL_FLIGHT_STATUS + "," + COL_FLIGHT_STATUS_URL + " from "
//                + SAVED_FLIGHTS + " order by " + COL_DEPARTURE_DATE + ","
//                + COL_DEPARTURE_TIME;
//
//        Cursor cur = db.rawQuery(SELECT_SAVED_FLIGHTS, null);
//        if (cur == null || cur.getCount() == 0) {
//            cur.close();
//        } else {
//
//            cur.moveToFirst();
//            long prevDiff = 0;
//            long currentDiff = 0;
//
//            do {
//                try {
//                    Date flightDate = null;
//                    //String[] strArr = cur.getString(3).split(" ");
//                    //HashMap<String, Object> flightStatus = null;
//
//					/*if (strArr != null && strArr.length >= 3) {
//						flightDate = sdf.parse(strArr[0] + "/" + strArr[1]
//								+ "/" + strArr[2] + " " + cur.getString(2));
//					}*/
//                    String departureDate,arrivalDate;
//                    departureDate = cur.getString(3);
//                    arrivalDate = cur.getString(5);
//
//                    //convert the date string(dd/MMM/yy) to (yyyy-MM-dd)
//                    if(departureDate.indexOf("-") == -1){
//                        departureDate = getDateInyyyyMMdd(departureDate);
//                        arrivalDate = getDateInyyyyMMdd(arrivalDate);
//                    }
//
//                    flightDate = sdf.parse(departureDate+" "+cur.getString(2));
//                    if (flightDate != null && flightDate.compareTo(currentDay) > 0) {
//
//                        currentDiff = flightDate.getTime() - currentDay.getTime();
//
//                        if (prevDiff == 0 || currentDiff < prevDiff) {
//                            tempSavedFlightsArr = new JSONArray();
//                            prevDiff = currentDiff;
//                        } else if (currentDiff == prevDiff) {
//
//                        } else {
//                            continue;
//                        }
//
//						/*String statusUrl = null;
//						statusUrl = cur.getString(13);
//
//						if (statusUrl != null && statusUrl.length() > 0) {
//
//							Calendar cal1 = Calendar.getInstance();
//							time = cal1.getTimeInMillis();
//							day = String.valueOf(cal1
//									.get(Calendar.DAY_OF_MONTH));
//							month = String.valueOf(getMonth(cal1
//									.get(Calendar.MONTH)));
//							year = String.valueOf(cal1.get(Calendar.YEAR));
//
//							SimpleDateFormat sdf1 = new SimpleDateFormat(
//									"dd/MMM/yy", Locale.US);
//
//							Date currentDate_Status = null;
//							Date flightDate_Status = null;
//
//							try {
//								currentDate_Status = sdf1.parse(day + "/"
//										+ month + "/" + year);
//								if (strArr != null && strArr.length >= 3) {
//									flightDate_Status = sdf1
//											.parse(strArr[0] + "/" + strArr[1]
//													+ "/" + strArr[2]);
//								}
//							} catch (ParseException e1) {
//								// TODO Auto-generated catch block
//								e1.printStackTrace();
//							}
//
//							if (flightDate_Status != null
//									&& flightDate_Status
//											.compareTo(currentDate_Status) == 0
//									&& statusUrl != null) {
//
//								String[] tempStrArr = statusUrl.split(",");
//
//								FlightStatusHandler flightStatusHandler = new FlightStatusHandler(
//										mContext);
//								flightStatus = flightStatusHandler
//										.getFlightStatus(tempStrArr);
//							}
//						}*/
//
//                        JSONObject savedFlights = new JSONObject();
//
//
//
//                        savedFlights.put(COL_FLIGHT_NUM, cur.getString(0));
//                        savedFlights.put(COL_ARRIVAL_TIME, cur.getString(1));
//                        savedFlights.put(COL_DEPARTURE_TIME, cur.getString(2));
//                        savedFlights.put(COL_DEPARTURE_DATE, departureDate);
//                        savedFlights.put(COL_NO_OF_STOPS, cur.getString(4));
//                        savedFlights.put(COL_ARRIVAL_DATE, arrivalDate);
//                        savedFlights.put(COL_DEPARTURE, cur.getString(6));
//                        savedFlights.put(COL_ARRIVAL, cur.getString(7));
//                        savedFlights.put(COL_ID, cur.getString(8));
//                        savedFlights.put(COL_DETAILS, cur.getString(9));
//                        savedFlights.put(COL_DEPARTURE_CITY_NAME,
//                                cur.getString(10));
//                        savedFlights.put(COL_ARRIVAL_CITY_NAME,
//                                cur.getString(11));
//                        savedFlights.put(COL_FLIGHT_STATUS_URL,
//                                cur.getString(13));
//
//						/*if (flightStatus != null) {
//							savedFlights.put(COL_FLIGHT_STATUS,
//									flightStatus.get(COL_FLIGHT_STATUS));
//							savedFlights.put(FLIGHT_DETAIL_STATUS,
//									flightStatus.get(FLIGHT_DETAIL_STATUS));
//							savedFlights.put("Status_Object",
//									flightStatus.get("Status_Object"));
//						} else {
//							savedFlights.put(COL_FLIGHT_STATUS, "");
//							savedFlights.put(FLIGHT_DETAIL_STATUS, "");
//							savedFlights.put("Status_Object", "");
//						}*/
//
//                        tempSavedFlightsArr.put(savedFlights);
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//            } while (cur.moveToNext());
//            cur.close();
//        }
//
//        if (tempSavedFlightsArr != null && tempSavedFlightsArr.length() > 0) {
//            int prevRowId = 0;
//            int currentRowId = 0;
//            for (int i = 0; i < tempSavedFlightsArr.length(); i++) {
//                try {
//                    currentRowId = tempSavedFlightsArr.getJSONObject(i).getInt(
//                            COL_ID);
//                    if (currentRowId >= prevRowId) {
//                        savedFlightsArr = new JSONArray();
//                        savedFlightsArr.put(tempSavedFlightsArr.get(i));
//                        prevRowId = currentRowId;
//                    }
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//
//
//            HashMap<String, Object> flightStatus = null;
//            String statusUrl = null;
//            try {
//                statusUrl = savedFlightsArr.getJSONObject(0).getString(COL_FLIGHT_STATUS_URL);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//            if (NetworkUtil.getConnectivityStatus(mContext) > 0 && statusUrl != null && statusUrl.length() > 0) {
//
//                Calendar cal1 = Calendar.getInstance();
//                time = cal1.getTimeInMillis();
//                day = String.valueOf(cal1
//                        .get(Calendar.DAY_OF_MONTH));
//                day=(day.length()==1)?("0"+day):day;
//				/*month = String.valueOf(getMonth(cal1
//						.get(Calendar.MONTH)));*/
//                mon = (cal1.get(Calendar.MONTH))+1;
//                month=(mon<10)?("0"+mon):String.valueOf(mon);
//                year = String.valueOf(cal1.get(Calendar.YEAR));
//
//				/*SimpleDateFormat sdf1 = new SimpleDateFormat(
//						"dd/MMM/yy", Locale.US);*/
//                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd",
//                        Locale.US);
//                Date currentDate = null;
//                Date previousDate = null;
//                Date tomorrowDate = null;
//                Date flightDate = null;
//
//
//
//                try {
//					/*currentDate = sdf1.parse(day + "/"
//							+ month + "/" + year);*/
//                    currentDate = sdf1.parse(year + "-"
//                            + month + "-" + day);
//
//                    cal1 = Calendar.getInstance();
//                    time = cal1.getTimeInMillis() - DIFF_MILLIS;
//                    cal1.setTimeInMillis(time);
//                    day = String.valueOf(cal1
//                            .get(Calendar.DAY_OF_MONTH));
//                    day=(day.length()==1)?("0"+day):day;
//                    mon = (cal1.get(Calendar.MONTH))+1;
//                    month=(mon<10)?("0"+mon):String.valueOf(mon);
//                    year = String.valueOf(cal1.get(Calendar.YEAR));
//                    previousDate = sdf1.parse(year + "-"
//                            + month + "-" + day);
//
//                    cal1 = Calendar.getInstance();
//                    time = cal1.getTimeInMillis() + DIFF_MILLIS;
//                    cal1.setTimeInMillis(time);
//                    day = String.valueOf(cal1
//                            .get(Calendar.DAY_OF_MONTH));
//                    day=(day.length()==1)?("0"+day):day;
//                    mon = (cal1.get(Calendar.MONTH))+1;
//                    month=(mon<10)?("0"+mon):String.valueOf(mon);
//                    year = String.valueOf(cal1.get(Calendar.YEAR));
//                    tomorrowDate = sdf1.parse(day + "-"
//                            + month + "-" + year);
//
//					/*String[] strArr = savedFlightsArr.getJSONObject(0).getString(COL_DEPARTURE_DATE).split(" ");
//					if (strArr != null && strArr.length >= 3) {
//						flightDate = sdf1
//								.parse(strArr[0] + "/" + strArr[1]
//										+ "/" + strArr[2]);
//					}*/
//                    flightDate = sdf1
//                            .parse(savedFlightsArr.getJSONObject(0).getString(COL_DEPARTURE_DATE));
//                } catch (ParseException e1) {
//                    // TODO Auto-generated catch block
//                    e1.printStackTrace();
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//                if (flightDate != null
//                        && (flightDate
//                        .compareTo(currentDate) == 0 || flightDate
//                        .compareTo(previousDate) == 0 || flightDate
//                        .compareTo(tomorrowDate) == 0)
//                        && statusUrl != null) {
//
//                    String[] tempStrArr = statusUrl.split(",");
//
//                    FlightStatusHandler flightStatusHandler = new FlightStatusHandler(
//                            mContext);
//                    flightStatus = flightStatusHandler
//                            .getFlightStatus(tempStrArr);
//                }
//            }
//
//            try {
//                if (flightStatus != null) {
//                    savedFlightsArr.getJSONObject(0).put(COL_FLIGHT_STATUS,
//                            flightStatus.get(COL_FLIGHT_STATUS));
//                    savedFlightsArr.getJSONObject(0).put(FLIGHT_DETAIL_STATUS,
//                            flightStatus.get(FLIGHT_DETAIL_STATUS));
//                    savedFlightsArr.getJSONObject(0).put("Status_Object",
//                            flightStatus.get("Status_Object"));
//                } else {
//                    savedFlightsArr.getJSONObject(0).put(COL_FLIGHT_STATUS, "");
//                    savedFlightsArr.getJSONObject(0).put(FLIGHT_DETAIL_STATUS, "");
//                    savedFlightsArr.getJSONObject(0).put("Status_Object", "");
//                }
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//        } else {
//            savedFlightsArr.put("ERROR");
//        }
//
//        JSONArray settingsJson = getSettingFile();
//
//        try {
//            master.put("SavedFlights_test", savedFlightsArr);
//            master.put("url", StringConstants.SAVED_FLIGHT_URL);
//            master.put("settings", settingsJson);
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        Utils.logMessage(TAG, "getSavedFlightsHomeScreen returns json as : "
//                + master.toString(), Log.DEBUG);
//        return master;
//    }

    public JSONObject getSavedFlightsHomeScreen() {

        JSONObject master = new JSONObject();
        JSONArray savedFlightsArr = new JSONArray();
        JSONArray tempSavedFlightsArr = new JSONArray();

        // Get the current data n time
        Calendar cal = Calendar.getInstance();
        long time = cal.getTimeInMillis() - DIFF_MILLIS; /*
                                  * The check has to be
                                  * made based on
                                  * previous date
                                  */
        cal.setTimeInMillis(time);
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        day=(day.length()==1)?("0"+day):day;
        //String month = String.valueOf(getMonth(cal.get(Calendar.MONTH)));
        int mon = (cal.get(Calendar.MONTH))+1;
        String month=(mon<10)?("0"+mon):String.valueOf(mon);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(cal.get(Calendar.MINUTE));

        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy HH:mm",
        //Locale.US);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm",
                Locale.US);

        Date currentDay = null;
        try {
            currentDay = sdf.parse(year + "-" + month + "-" + day + " " + hour
                    + ":" + minute);
/*currentDay = sdf.parse(day + "/" + month + "/" + year + " " + hour
      + ":" + minute);*/
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

//        String SELECT_SAVED_FLIGHTS = "select " + COL_FLIGHT_NUM + ","
//                + COL_ARRIVAL_TIME + "," + COL_DEPARTURE_TIME + ","
//                + COL_DEPARTURE_DATE + "," + COL_NO_OF_STOPS + ","
//                + COL_ARRIVAL_DATE + "," + COL_DEPARTURE + "," + COL_ARRIVAL
//                + "," + COL_ID + "," + COL_DETAILS + ","
//                + COL_DEPARTURE_CITY_NAME + "," + COL_ARRIVAL_CITY_NAME + ","
//                + COL_FLIGHT_STATUS + "," + COL_FLIGHT_STATUS_URL + " from "
//                + SAVED_FLIGHTS + " order by " + COL_DEPARTURE_DATE + ","
//                + COL_DEPARTURE_TIME;

        String SELECT_SAVED_FLIGHTS = "select " + COL_FLIGHT_NUM + ","
                + COL_ARRIVAL_TIME + "," + COL_DEPARTURE_TIME + ","
                + COL_DEPARTURE_DATE + "," + COL_NO_OF_STOPS + ","
                + COL_ARRIVAL_DATE + "," + COL_DEPARTURE + "," + COL_ARRIVAL
                + "," + COL_ID + "," + COL_DETAILS + ","
                + COL_DEPARTURE_CITY_NAME + "," + COL_ARRIVAL_CITY_NAME + ","
                + COL_FLIGHT_STATUS + "," + COL_FLIGHT_STATUS_URL + " from "
                + SAVED_FLIGHTS + " order by " + COL_ARRIVAL_DATE + ","
                + COL_ARRIVAL_TIME;

/*

        String SELECT_SAVED_FLIGHTS = "select " + COL_FLIGHT_NUM + ","
                + COL_ARRIVAL_TIME + "," + COL_DEPARTURE_TIME + ","
                + COL_DEPARTURE_DATE + "," + COL_NO_OF_STOPS + ","
                + COL_ARRIVAL_DATE + "," + COL_DEPARTURE + "," + COL_ARRIVAL
                + "," + COL_ID + "," + COL_DETAILS + ","
                + COL_DEPARTURE_CITY_NAME + "," + COL_ARRIVAL_CITY_NAME + ","
                + COL_FLIGHT_STATUS + "," + COL_FLIGHT_STATUS_URL + "," + "Concat("+COL_ARRIVAL_DATE+','+ COL_ARRIVAL_TIME+") AS arrivalTime from "
                + SAVED_FLIGHTS + " order by arrivalTime" ;
*/
        HashMap<String, Object> flightStatus_status = new HashMap<String, Object>();
        Cursor cur = db.rawQuery(SELECT_SAVED_FLIGHTS, null);
        if (cur == null || cur.getCount() == 0) {
            cur.close();
        } else {

            cur.moveToFirst();
            long prevDiff = 0;
            long currentDiff = 0;
            FlightStatusHandler flightStatusHandler_status = new FlightStatusHandler(
                    mContext);

            do {
                try {
                    Date flightDate = null;
                    //String[] strArr = cur.getString(3).split(" ");
                    //HashMap<String, Object> flightStatus = null;

      /*if (strArr != null && strArr.length >= 3) {
         flightDate = sdf.parse(strArr[0] + "/" + strArr[1]
               + "/" + strArr[2] + " " + cur.getString(2));
      }*/
                    String departureDate,arrivalDate;
                    departureDate = cur.getString(3);
                    arrivalDate = cur.getString(5);

                    //convert the date string(dd/MMM/yy) to (yyyy-MM-dd)
                    if(departureDate.indexOf("-") == -1){
                        departureDate = getDateInyyyyMMdd(departureDate);
                        arrivalDate = getDateInyyyyMMdd(arrivalDate);
                    }

                    flightDate = sdf.parse(departureDate+" "+cur.getString(2));


                    if (flightDate != null && flightDate.compareTo(currentDay) > 0) {

                        currentDiff = flightDate.getTime() - currentDay.getTime();

                        String[] tempStrArr_status =  cur.getString(13).toString().split(",");
//                        flightStatus_status = flightStatusHandler_status
//                                .getFlightStatus(tempStrArr_status);

                        if(flightStatus_status.get("Queue_Flight_Status")=="landed") {
                            prevDiff+=1;
                        }else{
                            prevDiff=0;
                        }

                        if (prevDiff == 0 || currentDiff < prevDiff || cur.getCount() == 1) {
                            tempSavedFlightsArr = new JSONArray();
                            prevDiff = currentDiff;
                        } else if (currentDiff == prevDiff) {

                        } else {
                            continue;
                        }

         /*String statusUrl = null;
         statusUrl = cur.getString(13);

         if (statusUrl != null && statusUrl.length() > 0) {

            Calendar cal1 = Calendar.getInstance();
            time = cal1.getTimeInMillis();
            day = String.valueOf(cal1
                  .get(Calendar.DAY_OF_MONTH));
            month = String.valueOf(getMonth(cal1
                  .get(Calendar.MONTH)));
            year = String.valueOf(cal1.get(Calendar.YEAR));

            SimpleDateFormat sdf1 = new SimpleDateFormat(
                  "dd/MMM/yy", Locale.US);

            Date currentDate_Status = null;
            Date flightDate_Status = null;

            try {
               currentDate_Status = sdf1.parse(day + "/"
                     + month + "/" + year);
               if (strArr != null && strArr.length >= 3) {
                  flightDate_Status = sdf1
                        .parse(strArr[0] + "/" + strArr[1]
                              + "/" + strArr[2]);
               }
            } catch (ParseException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
            }

            if (flightDate_Status != null
                  && flightDate_Status
                        .compareTo(currentDate_Status) == 0
                  && statusUrl != null) {

               String[] tempStrArr = statusUrl.split(",");

               FlightStatusHandler flightStatusHandler = new FlightStatusHandler(
                     mContext);
               flightStatus = flightStatusHandler
                     .getFlightStatus(tempStrArr);
            }
         }*/

                        JSONObject savedFlights = new JSONObject();



                        savedFlights.put(COL_FLIGHT_NUM, cur.getString(0));
                        savedFlights.put(COL_ARRIVAL_TIME, cur.getString(1));
                        savedFlights.put(COL_DEPARTURE_TIME, cur.getString(2));
                        savedFlights.put(COL_DEPARTURE_DATE, departureDate);
                        savedFlights.put(COL_NO_OF_STOPS, cur.getString(4));
                        savedFlights.put(COL_ARRIVAL_DATE, arrivalDate);
                        savedFlights.put(COL_DEPARTURE, cur.getString(6));
                        savedFlights.put(COL_ARRIVAL, cur.getString(7));
                        savedFlights.put(COL_ID, cur.getString(8));
                        savedFlights.put(COL_DETAILS, cur.getString(9));
                        savedFlights.put(COL_DEPARTURE_CITY_NAME,
                                cur.getString(10));
                        savedFlights.put(COL_ARRIVAL_CITY_NAME,
                                cur.getString(11));
                        savedFlights.put(COL_FLIGHT_STATUS_URL,
                                cur.getString(13));

         /*if (flightStatus != null) {
            savedFlights.put(COL_FLIGHT_STATUS,
                  flightStatus.get(COL_FLIGHT_STATUS));
            savedFlights.put(FLIGHT_DETAIL_STATUS,
                  flightStatus.get(FLIGHT_DETAIL_STATUS));
            savedFlights.put("Status_Object",
                  flightStatus.get("Status_Object"));
         } else {
            savedFlights.put(COL_FLIGHT_STATUS, "");
            savedFlights.put(FLIGHT_DETAIL_STATUS, "");
            savedFlights.put("Status_Object", "");
         }*/

                        tempSavedFlightsArr.put(savedFlights);
                        break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (cur.moveToNext());
            cur.close();
        }



        if (tempSavedFlightsArr != null && tempSavedFlightsArr.length() > 0) {
            int prevRowId = 0;
            int currentRowId = 0;
            for (int i = 0; i < tempSavedFlightsArr.length(); i++) {
                try {
                    currentRowId = tempSavedFlightsArr.getJSONObject(i).getInt(
                            COL_ID);
                    if (currentRowId >= prevRowId) {
                        savedFlightsArr = new JSONArray();
                        savedFlightsArr.put(tempSavedFlightsArr.get(i));
                        prevRowId = currentRowId;
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


            HashMap<String, Object> flightStatus = null;
            String statusUrl = null;
            try {
                statusUrl = savedFlightsArr.getJSONObject(0).getString(COL_FLIGHT_STATUS_URL);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (NetworkUtil.getConnectivityStatus(mContext) > 0 && statusUrl != null && statusUrl.length() > 0) {

                Calendar cal1 = Calendar.getInstance();
                time = cal1.getTimeInMillis();
                day = String.valueOf(cal1
                        .get(Calendar.DAY_OF_MONTH));
                day=(day.length()==1)?("0"+day):day;
   /*month = String.valueOf(getMonth(cal1
         .get(Calendar.MONTH)));*/
                mon = (cal1.get(Calendar.MONTH))+1;
                month=(mon<10)?("0"+mon):String.valueOf(mon);
                year = String.valueOf(cal1.get(Calendar.YEAR));

   /*SimpleDateFormat sdf1 = new SimpleDateFormat(
         "dd/MMM/yy", Locale.US);*/
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd",
                        Locale.US);
                Date currentDate = null;
                Date previousDate = null;
                Date tomorrowDate = null;
                Date flightDate = null;



                try {
      /*currentDate = sdf1.parse(day + "/"
            + month + "/" + year);*/
                    currentDate = sdf1.parse(year + "-"
                            + month + "-" + day);

                    cal1 = Calendar.getInstance();
                    time = cal1.getTimeInMillis() - DIFF_MILLIS;
                    cal1.setTimeInMillis(time);
                    day = String.valueOf(cal1
                            .get(Calendar.DAY_OF_MONTH));
                    day=(day.length()==1)?("0"+day):day;
                    mon = (cal1.get(Calendar.MONTH))+1;
                    month=(mon<10)?("0"+mon):String.valueOf(mon);
                    year = String.valueOf(cal1.get(Calendar.YEAR));
                    previousDate = sdf1.parse(year + "-"
                            + month + "-" + day);

                    cal1 = Calendar.getInstance();
                    time = cal1.getTimeInMillis() + DIFF_MILLIS;
                    cal1.setTimeInMillis(time);
                    day = String.valueOf(cal1
                            .get(Calendar.DAY_OF_MONTH));
                    day=(day.length()==1)?("0"+day):day;
                    mon = (cal1.get(Calendar.MONTH))+1;
                    month=(mon<10)?("0"+mon):String.valueOf(mon);
                    year = String.valueOf(cal1.get(Calendar.YEAR));
                    tomorrowDate = sdf1.parse(day + "-"
                            + month + "-" + year);

      /*String[] strArr = savedFlightsArr.getJSONObject(0).getString(COL_DEPARTURE_DATE).split(" ");
      if (strArr != null && strArr.length >= 3) {
         flightDate = sdf1
               .parse(strArr[0] + "/" + strArr[1]
                     + "/" + strArr[2]);
      }*/
                    flightDate = sdf1
                            .parse(savedFlightsArr.getJSONObject(0).getString(COL_DEPARTURE_DATE));
                } catch (ParseException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


               // if (flightDate != null && (flightDate.compareTo(currentDate) == 0 || flightDate.compareTo(previousDate) == 0 || flightDate.compareTo(tomorrowDate) == 0) && statusUrl != null) {

                    if (flightDate != null  && statusUrl != null) {


                  //  String[] tempStrArr = statusUrl.split(",");
                    String[] tempStrArr = statusUrl.substring(statusUrl.lastIndexOf(",")+1,statusUrl.length()).split("@##$%%&");



                    FlightStatusHandler flightStatusHandler = new FlightStatusHandler(
                            mContext);

                        //I have commented this below line to change colour in home page-09-08-2017

//                    flightStatus = flightStatusHandler
//                            .getFlightStatus(tempStrArr);

                       flightStatus=flightStatus_status;
                }
            }

            try {
                if (flightStatus != null) {
                    savedFlightsArr.getJSONObject(0).put(COL_FLIGHT_STATUS,
                            flightStatus.get(COL_FLIGHT_STATUS));
                    savedFlightsArr.getJSONObject(0).put(FLIGHT_DETAIL_STATUS,
                            flightStatus.get(FLIGHT_DETAIL_STATUS));
                    savedFlightsArr.getJSONObject(0).put("Status_Object",
                            flightStatus.get("Status_Object"));
                } else {
                    savedFlightsArr.getJSONObject(0).put(COL_FLIGHT_STATUS, "");
                    savedFlightsArr.getJSONObject(0).put(FLIGHT_DETAIL_STATUS, "");
                    savedFlightsArr.getJSONObject(0).put("Status_Object", "");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            savedFlightsArr.put("ERROR");
        }

        JSONArray settingsJson = getSettingFile();

        try {
            master.put("SavedFlights_test", savedFlightsArr);
            master.put("url", StringConstants.SAVED_FLIGHT_URL);
            master.put("settings", settingsJson);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage(TAG, "getSavedFlightsHomeScreen returns json as : "
                + master.toString(), Log.DEBUG);
        return master;
    }





    public static boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            Utils.logMessage(TAG, "database path : " + myPath, Log.DEBUG);
            checkDB = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.NO_LOCALIZED_COLLATORS
                            | SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {

        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }
    public static boolean checkFlatFileCityAirport() {
        File outFile = new File(CityAirportJSONFilePath + CityAirportJSONFileName);

        if(outFile.exists()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean checkFlatFileCityAirportOthers() {
        File outFile = new File(CityAirportJSONFilePath + CityAirportOtherLangFileName);

        if(outFile.exists()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean checkFlatFileSettings() {
        File outFile = new File(CityAirportJSONFilePath + SettingsJSONFileName);

        if(outFile.exists()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean checkFlatFileSkyTipsCityList() {
        File outFile = new File(CityAirportJSONFilePath + SkyTipsCityAirportJSONFileName);

        if(outFile.exists()) {
            return true;
        }
        else {
            return false;
        }
    }


    public JSONArray getSkyTeamMemberData(String skyteam_member_id) {

        JSONArray memberArr = new JSONArray();
        JSONObject member = new JSONObject();
        String query = null;

        if (skyteam_member_id != null && skyteam_member_id.length() > 0) {
            query = "select " + COL_SNO + "," + COL_SKYTEAM_MEMBER + ","
                    + COL_MEMBER_FULLNAME + "," + COL_URL + ","
                    + COL_DESCRIPTION + "," + COL_IMAGES + "," + COL_DESIGNATION + " from "
                    + SKYTEAM_MEMBERS + " where " + COL_ID + " ='"
                    + skyteam_member_id + "'";
        }
        Cursor cur = null;
        if (query != null) {
            try{
                cur = db.rawQuery(query, null);
            }catch(Exception e){
                e.printStackTrace();
            }

            if (cur == null || cur.getCount() == 0) {
                memberArr.put("ERROR");
            } else {
                cur.moveToFirst();
                do {

                    try {
                        member.put(COL_SKYTEAM_MEMBER, cur.getString(1));
                        member.put(COL_SNO, cur.getString(0));
                        member.put(COL_MEMBER_FULLNAME, cur.getString(2));
                        member.put(COL_URL, cur.getString(3));
                        member.put(COL_DESCRIPTION, cur.getString(4));
                        member.put(COL_IMAGES, cur.getString(5));
                        member.put(COL_DESIGNATION, cur.getString(6));
                        memberArr.put(member);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } while (cur.moveToNext());
            }

            cur.close();
        } else {
            memberArr.put("ERROR");
        }
        Utils.logMessage(TAG, "getSkyTeamMemberData returns json array as : "
                + memberArr.toString(), Log.DEBUG);
        return memberArr;
    }

    public JSONObject getStoredSkyTips(String[] params) {
        String query = null;
        //String checkQuery = null;
        JSONObject master = new JSONObject();
        JSONArray skytipsArr = new JSONArray();
        int count = 0;
        Cursor checkCur =null;
        if (params[1].trim() != null && params[1].trim().length() > 0) // AirportCode
        {
            query = "select " + COL_COUNTRY + "," + COL_DATE + "," + COL_HUB
                    + "," + COL_NAME + "," + COL_TIP_DESC + "," + COL_TIP_THEME
                    + "," + COL_TIP_TITLE + "," + COL_TIP_ORIGINAL_LANG + "," + COL_TIP_TRANSLATED +"," +COL_TIP_THEME_ID+" from " + SKYTIPS + " where "
                    + COL_HUB + " ='" + params[1].trim() + "'";
        }

        if (query != null) {
            checkCur = db.rawQuery(query, null);

            if (checkCur == null || checkCur.getCount() == 0) {

                checkCur.close();

                try {
                    skytipsArr.put("No Offline Data");
                    master.put("SkyTips", skytipsArr);
                    master.put("url", StringConstants.SKYTIPS_URL);
                    master.put("Count", 0);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {
                checkCur.moveToFirst();
                do {
                    try {
                        JSONObject skytips = new JSONObject();
                        skytips.put(COL_COUNTRY, checkCur.getString(0));
                        skytips.put(COL_DATE, checkCur.getString(1));
                        skytips.put(COL_AIRPORT_CODE, checkCur.getString(2));
                        skytips.put(COL_NAME, checkCur.getString(3));
                        skytips.put(COL_TIP_DESC, checkCur.getString(4));
                        skytips.put(COL_TIP_THEME, checkCur.getString(5));
                        skytips.put(COL_TIP_TITLE, checkCur.getString(6));
                        skytips.put(COL_TIP_ORIGINAL_LANG, checkCur.getString(7));
                        skytips.put(COL_TIP_TRANSLATED, checkCur.getString(8));
                        skytips.put(KEY_TIP_THEME_ID, checkCur.getString(9));
                        count++;
                        skytipsArr.put(skytips);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } while (checkCur.moveToNext());

                checkCur.close();

                try {
                    master.put("Count", count);
                    master.put("SkyTips", skytipsArr);
                    master.put("url", StringConstants.SKYTIPS_URL);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        } else {
            try {
                skytipsArr.put("No Offline Data");
                master.put("SkyTips", skytipsArr);
                master.put("url", StringConstants.SKYTIPS_URL);
                master.put("Count", 0);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }






        Utils.logMessage(TAG, "getStoredSkyTips returns json array as : "
                + master.toString(), Log.DEBUG);
        return master;
    }

    public void insertIntoSkytips(JSONObject result) {
        //System.out.println("skytips insertion");

        try {

            JSONArray skytipsArr = new JSONArray();

            if ((result.get("SkyTips").getClass().getSimpleName())
                    .equalsIgnoreCase("JSONObject")) {
                skytipsArr.put(result.getJSONObject("SkyTips"));
            } else {
                skytipsArr = result.getJSONArray("SkyTips");
            }

            int count = skytipsArr.length();

            ContentValues val = new ContentValues();

            for (int i = 0; i < count; i++) {
                JSONObject temp = (JSONObject) skytipsArr.get(i);

                val.put(COL_HUB,
                        !temp.isNull(COL_HUB)
                                && !temp.getString(COL_HUB).equalsIgnoreCase(
                                "[]") ? temp.getString(COL_HUB) : "");
                val.put(COL_TIP_TITLE,
                        !temp.isNull(COL_TIP_TITLE)
                                && !temp.getString(COL_TIP_TITLE)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_TIP_TITLE) : "");
                val.put(COL_TIP_DESC,
                        !temp.isNull(COL_TIP_DESC)
                                && !temp.getString(COL_TIP_DESC)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_TIP_DESC) : "");
                val.put(COL_TIP_THEME,
                        !temp.isNull(COL_TIP_THEME)
                                && !temp.getString(COL_TIP_THEME)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_TIP_THEME) : "");
                val.put(COL_NAME,
                        !temp.isNull(COL_NAME)
                                && !temp.getString(COL_NAME).equalsIgnoreCase(
                                "[]") ? temp.getString(COL_NAME) : "");
                val.put(COL_COUNTRY,
                        !temp.isNull(COL_COUNTRY)
                                && !temp.getString(COL_COUNTRY)
                                .equalsIgnoreCase("[]") ? temp
                                .getString(COL_COUNTRY) : "");
                val.put(COL_DATE,
                        !temp.isNull(COL_DATE)
                                && !temp.getString(COL_DATE).equalsIgnoreCase(
                                "[]") ? temp.getString(COL_DATE) : "");
                val.put(COL_TIP_ORIGINAL_LANG,
                        !temp.isNull(COL_TIP_ORIGINAL_LANG)
                                && !temp.getString(COL_TIP_ORIGINAL_LANG).equalsIgnoreCase(
                                "[]") ? temp.getString(COL_TIP_ORIGINAL_LANG) : "");
                val.put(COL_TIP_TRANSLATED,
                        !temp.isNull(COL_TIP_TRANSLATED)
                                && !temp.getString(COL_TIP_TRANSLATED).equalsIgnoreCase(
                                "[]") ? temp.getString(COL_TIP_TRANSLATED) : "");
                val.put(COL_AIRPORTNAME,
                        !temp.isNull(COL_AIRPORTNAME)
                                && !temp.getString(COL_AIRPORTNAME).equalsIgnoreCase(
                                "[]") ? temp.getString(COL_AIRPORTNAME) : "");
                val.put(COL_TIP_THEME_ID,
                        !temp.isNull(KEY_TIP_THEME_ID)
                                && !temp.getString(KEY_TIP_THEME_ID).equalsIgnoreCase(
                                "[]") ? temp.getString(KEY_TIP_THEME_ID) : "");
                if (dbExists()) {
                    db.insert(SKYTIPS, null, val);
                }
                else{
                    Log.d("DBHelper"," DB is NULL ");
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public JSONObject getStoredLounges() {
        JSONObject master = new JSONObject();
        JSONArray loungesArr = new JSONArray();

        String SELECT_LOUNGES = "select " + COL_CITY_CODE + ","
                + COL_COUNTRY_CODE_LOUNGES + "," + COL_LOCATION_CODE_LOUNGES
                + "," + COL_LOCATION_NAME + "," + COL_COUNTRY_NAME_LOUNGES
                + "," + COL_AREA + "," + COL_TIMEZONE + "," + COL_LONGITUDE
                + "," + COL_LATITUDE + " from " + AIRPORTSLIST_LOUNGES;

        Cursor cur = db.rawQuery(SELECT_LOUNGES, null);
        if (cur == null || cur.getCount() == 0) {
            cur.close();

            try {
                loungesArr.put("Error");
                master.put("AirportsList_Lounges", loungesArr);
                master.put("url", StringConstants.LOUNGES_LIST);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        else {
            cur.moveToFirst();

            do {

                try {
                    JSONObject lounges = new JSONObject();
                    lounges.put(COL_CITY_CODE, cur.getString(0));
                    lounges.put(COL_COUNTRY_CODE_LOUNGES, cur.getString(1));
                    lounges.put(COL_LOCATION_CODE_LOUNGES, cur.getString(2));
                    lounges.put(COL_LOCATION_NAME, cur.getString(3));
                    lounges.put(COL_COUNTRY_NAME_LOUNGES, cur.getString(4));
                    lounges.put(COL_AREA, cur.getString(5));
                    lounges.put(COL_TIMEZONE, cur.getString(6));
                    lounges.put(COL_LONGITUDE, cur.getString(7));
                    lounges.put(COL_LATITUDE, cur.getString(8));

                    loungesArr.put(lounges);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (cur.moveToNext());

            cur.close();

            try {
                master.put("AirportsList_Lounges", loungesArr);
                master.put("url", StringConstants.LOUNGES_LIST);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        Utils.logMessage(TAG,
                "getLounges returns json as : " + master.toString(), Log.DEBUG);
        return master;
    }

    public JSONObject insertIntoSavedFlights(String command,
                                             int savedFlightsCount) {

        JSONObject retStr = new JSONObject();

        String status = "failure";
        int cnt = savedFlightsCount;

        try {

            retStr.put("url", StringConstants.SAVE_FLIGHTS_URL);
            JSONObject temp = new JSONObject(command);

            String departure = temp.isNull(COL_DEPARTURE) ? "" : temp
                    .getString(COL_DEPARTURE);

            String arrival = temp.isNull(COL_ARRIVAL) ? "" : temp
                    .getString(COL_ARRIVAL);

            String noOfStops = temp.isNull(COL_NO_OF_STOPS) ? "" : temp
                    .getString(COL_NO_OF_STOPS);

            String departureDate = temp.isNull(COL_DEPARTURE_DATE) ? ""
                    : temp.getString(COL_DEPARTURE_DATE);

            String arrivalDate = temp.isNull(COL_ARRIVAL_DATE)  ? ""
                    : temp.getString(COL_ARRIVAL_DATE);

            String departureTime = temp.isNull(COL_DEPARTURE_TIME)  ? ""
                    : temp.getString(COL_DEPARTURE_TIME);

            String arrivalTime = temp.isNull(COL_ARRIVAL_TIME)  ? ""
                    : temp.getString(COL_ARRIVAL_TIME);

            String flightNum = temp.isNull(COL_FLIGHT_NUM)  ? "" : temp
                    .getString(COL_FLIGHT_NUM);

            String departureCityName = temp.isNull(COL_DEPARTURE_CITY_NAME)  ? ""
                    : temp.getString(COL_DEPARTURE_CITY_NAME);

            String arrivalCityName = temp.isNull(COL_ARRIVAL_CITY_NAME)  ? ""
                    : temp.getString(COL_ARRIVAL_CITY_NAME);

/*			String query = "select " + COL_ID + " from " + SAVED_FLIGHTS
					+ " where " + COL_DEPARTURE + "='" + departure + "' and "
					+ COL_ARRIVAL + "='" + arrival + "' and " + COL_NO_OF_STOPS
					+ "='" + noOfStops + "' and " + COL_DEPARTURE_DATE + "='"
					+ departureDate + "' and " + COL_ARRIVAL_DATE + "='"
					+ arrivalDate + "' and " + COL_DEPARTURE_TIME + "='"
					+ departureTime + "' and " + COL_ARRIVAL_TIME + "='"
					+ arrivalTime + "' and " + COL_FLIGHT_NUM + "='"
					+ flightNum + "' and " + COL_DEPARTURE_CITY_NAME + "='"
					+ departureCityName + "' and " + COL_ARRIVAL_CITY_NAME
					+ "='" + arrivalCityName + "'";
	*/
            String query = "select " + COL_ID + " from " + SAVED_FLIGHTS
                    + " where " + COL_DEPARTURE + "='" + departure + "' and "
                    + COL_ARRIVAL + "='" + arrival + "' and " + COL_NO_OF_STOPS
                    + "='" + noOfStops + "' and " + COL_DEPARTURE_DATE + "='"
                    + departureDate + "' and " + COL_ARRIVAL_DATE + "='"
                    + arrivalDate + "' and " + COL_DEPARTURE_TIME + "='"
                    + departureTime + "' and " + COL_ARRIVAL_TIME + "='"
                    + arrivalTime + "' and " + COL_FLIGHT_NUM + "='"
                    + flightNum + "'";

            Cursor cur = db.rawQuery(query, null);

            if (cur == null || cur.getCount() == 0) {
                ContentValues val = new ContentValues();

                val.put(COL_DEPARTURE,
                        temp.isNull(COL_DEPARTURE)  ? "" : temp
                                .getString(COL_DEPARTURE) );
                val.put(COL_ARRIVAL,
                        temp.isNull(COL_ARRIVAL)  ? "" : temp
                                .getString(COL_ARRIVAL) );
                val.put(COL_NO_OF_STOPS,
                        temp.isNull(COL_NO_OF_STOPS)  ? 0 : temp
                                .getInt(COL_NO_OF_STOPS));
                val.put(COL_DEPARTURE_DATE,
                        temp.isNull(COL_DEPARTURE_DATE)  ? "" :temp
                                .getString(COL_DEPARTURE_DATE) );
                val.put(COL_ARRIVAL_DATE,
                        temp.isNull(COL_ARRIVAL_DATE)  ? "" :temp
                                .getString(COL_ARRIVAL_DATE) );
                val.put(COL_DEPARTURE_TIME,
                        temp.isNull(COL_DEPARTURE_TIME)  ? "" :temp
                                .getString(COL_DEPARTURE_TIME) );
                val.put(COL_ARRIVAL_TIME,
                        temp.isNull(COL_ARRIVAL_TIME)  ? "" :temp
                                .getString(COL_ARRIVAL_TIME) );
                val.put(COL_FLIGHT_NUM,
                        temp.isNull(COL_FLIGHT_NUM)  ? "" :temp
                                .getString(COL_FLIGHT_NUM) );
                val.put(COL_DETAILS,
                        temp.isNull(COL_DETAILS)  ? "" :temp
                                .getString(COL_DETAILS) );
                val.put(COL_DEPARTURE_CITY_NAME,
                        temp.isNull(COL_DEPARTURE_CITY_NAME)  ? "" :temp
                                .getString(COL_DEPARTURE_CITY_NAME) );
                val.put(COL_ARRIVAL_CITY_NAME,
                        temp.isNull(COL_ARRIVAL_CITY_NAME)  ? "" :temp
                                .getString(COL_ARRIVAL_CITY_NAME) );
                val.put(COL_FLIGHT_STATUS_URL,
                        temp.isNull(COL_FLIGHT_STATUS_URL)  ? "" :temp
                                .getString(COL_FLIGHT_STATUS_URL) );

                db.insert(SAVED_FLIGHTS, null, val);

                status = "sucess";
                cnt++;
            } else if (cur.getCount() > 0) {
                status = "duplicate";
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            retStr.put("status", status);
            retStr.put("count", cnt);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retStr;

    }

    public JSONObject insertIntoSavedAirports(String command,
                                              int savedAirportsCount) {

        JSONObject retStr = new JSONObject();

        String status = "failure";
        int cnt = savedAirportsCount;

        try {
            retStr.put("url", StringConstants.SAVE_AIRPORTS_URL);
            JSONObject temp = new JSONObject(command);

            String locationCode = temp.isNull(COL_LOCATION_CODE)  ? ""
                    : temp.getString(COL_LOCATION_CODE);
            String airportName = temp.isNull(COL_AIRPORT_NAME)  ? ""
                    : temp.getString(COL_AIRPORT_NAME);
            String countryCode = temp.isNull(COL_COUNTRY_CODE)  ? ""
                    : temp.getString(COL_COUNTRY_CODE);
            String cityName = temp.isNull(COL_CITY_NAME)  ? "" : temp
                    .getString(COL_CITY_NAME);
            String airportDetailsURL = temp.isNull(COL_AIRPORT_DETAILS_URL)  ? ""
                    : temp.getString(COL_AIRPORT_DETAILS_URL);
            String countryName = temp.isNull(COL_COUNTRY_NAME)  ? ""
                    : temp.getString(COL_COUNTRY_NAME);

            String localTime = temp.isNull(COL_LOCAL_TIME)  ? "" : temp
                    .getString(COL_LOCAL_TIME);
            String temprature = temp.isNull(COL_TEMPRATURE)  ? "" : temp
                    .getString(COL_TEMPRATURE);
            String weatherCode = temp.isNull(COL_WEATHER_CODE)  ? ""
                    : temp.getString(COL_WEATHER_CODE);
            String loungesCount = temp.isNull(COL_LOUNGES_COUNT)  ? ""
                    : temp.getString(COL_LOUNGES_COUNT);
            String skytipsCount = temp.isNull(COL_SKYTIPS_COUNT)  ? ""
                    : temp.getString(COL_SKYTIPS_COUNT);
            String airlineCount = temp.isNull(COL_AIRLINES_COUNT)  ? ""
                    : temp.getString(COL_AIRLINES_COUNT);

/*			String query = "select " + COL_ID + " from " + SAVED_AIRPORTS
					+ " where " + COL_LOCATION_CODE + "='" + locationCode
					+ "' and " + COL_AIRPORT_NAME + "='" + airportName
					+ "' and " + COL_CITY_NAME + "='" + cityName + "' and "
					+ COL_AIRPORT_DETAILS_URL + "='" + airportDetailsURL
					+ "' and " + COL_COUNTRY_NAME + "='" + countryName
					+ "' and " + COL_COUNTRY_CODE + "='" + countryCode + "'";
*/
            String query = "select " + COL_ID + " from " + SAVED_AIRPORTS
                    + " where " + COL_LOCATION_CODE + "='" + locationCode
                    + "' and " + COL_AIRPORT_DETAILS_URL + "='" + airportDetailsURL
                    + "' and " + COL_COUNTRY_CODE + "='" + countryCode + "'";

            Cursor cur = db.rawQuery(query, null);

            if (cur == null || cur.getCount() == 0) {
                ContentValues val = new ContentValues();

                val.put(COL_LOCATION_CODE, locationCode);
                val.put(COL_AIRPORT_NAME, airportName);
                val.put(COL_COUNTRY_CODE, countryCode);
                val.put(COL_COUNTRY_NAME, countryName);
                val.put(COL_AIRPORT_DETAILS_URL, airportDetailsURL);
                val.put(COL_CITY_NAME, cityName);
                val.put(COL_LOCAL_TIME, localTime);
                val.put(COL_TEMPRATURE, temprature);
                val.put(COL_WEATHER_CODE, weatherCode);
                val.put(COL_LOUNGES_COUNT, loungesCount);
                val.put(COL_SKYTIPS_COUNT, skytipsCount);
                val.put(COL_AIRLINES_COUNT, airlineCount);

                db.insert(SAVED_AIRPORTS, null, val);
                status = "sucess";
                cnt++;
            } else if (cur.getCount() > 0) {
                status = "duplicate";
            }
            cur.close();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            retStr.put("status", status);
            retStr.put("count", cnt);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retStr;

    }

    public int checkSavedFlightsCount() {

        return (int) DatabaseUtils.queryNumEntries(db, SAVED_FLIGHTS);
    }

    public int checkSavedAirportsCount() {

        return (int) DatabaseUtils.queryNumEntries(db, SAVED_AIRPORTS);
    }



    public JSONObject getSkyTipsHubs() {
        String query = null;
        String completeString = null;
        JSONObject master = new JSONObject();
        JSONArray skytipshubsArr = new JSONArray();
        query = "select " + COL_AIRPORTNAME +","+ COL_AIRPORTCODE +","+ COL_COUNTRYNAME+ " from " + AIRPORTS_TEST;
        //query = "select " + COL_AIRPORTNAME + " from " + SKYTIPS_HUBS;
        if (query != null) {
            Cursor cur = db.rawQuery(query, null);

            if (cur == null || cur.getCount() == 0) {

            } else {
                cur.moveToFirst();
                do {
                    try {
                        if(cur.getString(0).contains("Airport")){
                            completeString =cur.getString(0) +" (" + cur.getString(1) + "), "+cur.getString(2);
                        }else{
                            completeString =cur.getString(0) +" Airport"+" (" + cur.getString(1) + "), "+cur.getString(2);
                        }
                        JSONObject skytipshubs = new JSONObject();
                        skytipshubs.put(COL_AIRPORTNAME,completeString);
                        //skytipshubs.put(COL_AIRPORTS_CODE, cur.getString(1));
                        skytipshubsArr.put(skytipshubs);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } while (cur.moveToNext());
            }
            cur.close();
            try {
                master.put("url", StringConstants.SKYTIPS_HUBS_URL);
                if (skytipshubsArr != null && skytipshubsArr.length() > 0) {
                    master.put("SkyTipshubs", skytipshubsArr);
                } else {
                    master.put("SkyTipshubs", "ERROR");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        Utils.logMessage(TAG, "getSkyTipsHubs returns json array as : "
                + master.toString(), Log.DEBUG);
        return master;
    }

    public void clearLounges() {

        String query = "delete from " + LOUNGES;

        db.execSQL(query);

    }

    public void clearSkytips() {

        String query = "delete from " + SKYTIPS;

        db.execSQL(query);

    }


    public String getCityAirportURL(){
        String lang=null;
        String url=null;
        try {
            lang= getSettingFileObj().getString("Language");
            if("zh".equalsIgnoreCase(lang)){
                url=StringConstants.cityAirportListLangURL("zh-Hans");
            }
            else if("Ja".equalsIgnoreCase(lang) || "Ko".equalsIgnoreCase(lang) || "zh-Hant".equalsIgnoreCase(lang)){
                url=StringConstants.cityAirportListLangURL(lang);
            }
            else{
                url=StringConstants.cityAirportPairLangURL(lang);
            }

        } catch (JSONException e) {
            url=StringConstants.cityAirportPairLangURL("En");
            e.printStackTrace();
        }
        return url;
    }

    public String getCityAirportFile(){
        String lang=null;
        String filePath=null;
        try {
            lang= getSettingFileObj().getString("Language");

            //	if("En".equalsIgnoreCase(lang)){
            filePath=CityAirportJSONFilePath + CityAirportJSONFileName;
		/*	}else{
				filePath=CityAirportJSONFilePath + CityAirportOtherLangFileName;
			}
			*/
        } catch (JSONException e) {
            filePath=CityAirportJSONFilePath + CityAirportJSONFileName;
            e.printStackTrace();
        }
        return filePath;
    }


    public void insertIntoCityAirport(String result) {

        try {

            File outFile = new File(getCityAirportFile());

            File parent = outFile.getParentFile();

            if(!parent.exists()){
                boolean ret = parent.mkdirs();
            }

            if (outFile.exists()) {

                outFile.delete();
                outFile.createNewFile();

            }

            OutputStream os = new FileOutputStream(outFile);
            Writer out = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            out.write(result);
            out.close();
            os.close();



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }




    }


    /*
     * Save the applied settings
     */
    public void insertIntoSettingsFile(JSONArray result) {

        try {
            InputStream is = new ByteArrayInputStream(result.toString().getBytes());

            File outFile = new File(CityAirportJSONFilePath + SettingsJSONFileName);

            File parent = outFile.getParentFile();

            if(!parent.exists()){
                boolean ret = parent.mkdirs();
            }

            if (outFile.exists()) {

                outFile.delete();
                outFile.createNewFile();

            }

            OutputStream os = new FileOutputStream(outFile);
            byte[] buffer = new byte[1024];

            while(is.read(buffer) > 0) {
                os.write(buffer);
            }

            os.flush();
            os.close();
            is.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }






    /*
     * returns the saved settings
     */
    public JSONArray getSettingFile() {

        JSONArray master = new JSONArray();
        try {

            InputStream is = new FileInputStream(CityAirportJSONFilePath + SettingsJSONFileName);

            Scanner s = new Scanner(is).useDelimiter("\\A");

            String str = s.hasNext() ? s.next() : "";

            if (str != null && str.length() > 0) {
                master = new JSONArray(str);
            } else {
                master = new JSONArray("ERROR");
            }
            master.getJSONObject(0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage("SETTINGS", "returning from settings table : "+master.toString(), Log.DEBUG);
        return master;
    }

    public JSONObject getSettingFileObjForHTML() {

        JSONArray master = new JSONArray();
        JSONObject JSONobj=new JSONObject();
        try {

            InputStream is = new FileInputStream(CityAirportJSONFilePath + SettingsJSONFileName);

            Scanner s = new Scanner(is).useDelimiter("\\A");

            String str = s.hasNext() ? s.next() : "";

            if (str != null && str.length() > 0) {
                master = new JSONArray(str);
            } else {
                master = new JSONArray("ERROR");
            }
            JSONobj=master.getJSONObject(0);

            try{
                //System.out.println("Inside settings:"+JSONobj.get("InitialLoad"));
                if(JSONobj.get("InitialLoad").equals("YES")){

                    JSONObject JSONobjCopy = new JSONObject(JSONobj.toString());
                    JSONobjCopy.put("InitialLoad", "NO");



                    JSONArray settingsArr = new JSONArray();
                    settingsArr.put(JSONobjCopy);
                    this.insertIntoSettingsFile(settingsArr);



                    //System.out.println("After writting:"+JSONobj.get("InitialLoad"));
                }


            }
            catch(JSONException e){
                Utils.logMessage("SETTINGS", "Checking the initialLoad value for Making it as NO,If it's YES  : "+master.toString(), Log.DEBUG);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage("SETTINGS", "returning from settings table : "+master.toString(), Log.DEBUG);

        return JSONobj;
    }


    public JSONObject getSettingFileObj() {

        JSONArray master = new JSONArray();
        JSONObject JSONobj=new JSONObject();
        try {

            InputStream is = new FileInputStream(CityAirportJSONFilePath + SettingsJSONFileName);

            Scanner s = new Scanner(is).useDelimiter("\\A");

            String str = s.hasNext() ? s.next() : "";

            if (str != null && str.length() > 0) {
                master = new JSONArray(str);
            } else {
                master = new JSONArray("ERROR");
            }
            JSONobj=master.getJSONObject(0);


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Utils.logMessage("SETTINGS", "returning from settings table : "+master.toString(), Log.DEBUG);

        return JSONobj;
    }




    public JSONObject getIndividualStatus(String statusUrl) {

        HashMap<String, Object> flightStatus = null;
        JSONObject savedFlights = new JSONObject();

        if (NetworkUtil.getConnectivityStatus(mContext) > 0 && statusUrl != null && statusUrl.length() > 0) {
            String[] tempStrArr = statusUrl.split(",");

            FlightStatusHandler flightStatusHandler = new FlightStatusHandler(
                    mContext);
            flightStatus = flightStatusHandler.getFlightStatus(tempStrArr);

            try {
                if (flightStatus != null) {
                    savedFlights.put(COL_FLIGHT_STATUS,
                            flightStatus.get(COL_FLIGHT_STATUS));
                    savedFlights.put(FLIGHT_DETAIL_STATUS,
                            flightStatus.get(FLIGHT_DETAIL_STATUS));
                    savedFlights.put("Status_Object",
                            flightStatus.get("Status_Object"));

                } else {
                    savedFlights.put(COL_FLIGHT_STATUS, "");
                    savedFlights.put(FLIGHT_DETAIL_STATUS, "");
                    savedFlights.put("Status_Object", "");
                }
                savedFlights.put("url",
                        StringConstants.SAVEDFLIGHT_STATUS_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            try {
                savedFlights.put(COL_FLIGHT_STATUS, "");
                savedFlights.put(FLIGHT_DETAIL_STATUS, "");
                savedFlights.put("Status_Object", "");
                savedFlights.put("url",
                        StringConstants.SAVEDFLIGHT_STATUS_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return savedFlights;
    }

    public void deleteOtherLangCache() {

        String lang;
        try {
            lang = getSettingFileObj().getString("Language");
            if("En".equalsIgnoreCase(lang)){
                File outFile = new File(CityAirportJSONFilePath + CityAirportOtherLangFileName);

                File parent = outFile.getParentFile();

                if(!parent.exists()){
                    parent.mkdirs();
                }

                if (outFile.exists()) {

                    outFile.delete();
                }

            }else{
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public static String getDateInyyyyMMdd(String dateInString){

        try {
            SimpleDateFormat oldFormatter= null;

            if(dateInString.indexOf("/") >0){
                oldFormatter = new SimpleDateFormat("dd/MMM/yy");
            }
            else if(dateInString.indexOf(" ") >0){
                oldFormatter = new SimpleDateFormat("dd MMM yy");
            }
            else{

            }
            SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = oldFormatter.parse(dateInString);
            return newFormatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        return "dateInString";
    }


    /** skyTips cityList JSON loading **/
    @SuppressWarnings("resource")
    public JSONObject getSkyTipsCityList() {

        JSONObject master = new JSONObject();


        try {
            CityAirportJSONFilePath = context.getFilesDir() + "/";

            FileReader is = new FileReader(new File(CityAirportJSONFilePath + SkyTipsCityAirportJSONFileName));
            Scanner s = new Scanner(is).useDelimiter("\\A");
            String str = s.hasNext() ? s.next() : "";
            master.put("AirPorts_test", new JSONArray(str));
            master.put("url", StringConstants.SKYTIPS_LIST_URL);
            master.put("Status","Success");


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return master;
    }
    public static boolean checkFlatFileSkyTipsCityDataList() {
        File outFile = new File(CityAirportJSONFilePath + SkyTipsCityAirportJSONFileName);

        if(outFile.exists()) {
            return true;
        }
        else {
            return false;
        }
    }
}
