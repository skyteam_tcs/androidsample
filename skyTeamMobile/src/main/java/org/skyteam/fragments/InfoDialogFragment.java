package org.skyteam.fragments;

import org.skyteam.R;



import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.TextView;

import java.io.File;

public class InfoDialogFragment extends DialogFragment  {
	
	public static final int GPS_ENABLED=2;
    public static final int YOGA_DELETE=3;

    private static FragmentViewCallback mFragmentViewCallback;

    public static final String EXTRA_BUNDLEARGS_FILENAME="extrafilename";

	public static InfoDialogFragment newInstance(String message, int showOkBtn,int showcancelButton,
			int showSearchBox) {
		InfoDialogFragment fragment = new InfoDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putString("msg", message);
		bundle.putInt("OK", showOkBtn);
		bundle.putInt("Cancel", showcancelButton);
		bundle.putInt("SEARCH", showSearchBox);
		fragment.setArguments(bundle);
		return fragment;
	}

    public static InfoDialogFragment newInstance(String message, int showOkBtn,int showcancelButton,
                                                 int showSearchBox,Bundle bundleArgs,FragmentViewCallback fragmentViewCallback) {
        InfoDialogFragment fragment = new InfoDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("msg", message);
        bundle.putInt("OK", showOkBtn);
        bundle.putInt("Cancel", showcancelButton);
        bundle.putInt("SEARCH", showSearchBox);
        if(bundleArgs != null) {
            bundle.putString(EXTRA_BUNDLEARGS_FILENAME, bundleArgs.getString(EXTRA_BUNDLEARGS_FILENAME));
        }
        mFragmentViewCallback=fragmentViewCallback;
        fragment.setArguments(bundle);
        return fragment;
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());        
    LayoutInflater i = getActivity().getLayoutInflater();
    String message = getArguments().getString("msg");
	final int showOkBtn = getArguments().getInt("OK");
	int showCancelBtn = getArguments().getInt("Cancel");
	int shwoSearch = getArguments().getInt("SEARCH");
	final String bundleFilename = getArguments().getString(EXTRA_BUNDLEARGS_FILENAME);
	View view = i.inflate(R.layout.alert_dialog, null, false);
	TextView text = (TextView) view.findViewById(R.id.alertText);
	text.setText(message);
	// text.setTextAlignment();
	Button button = (Button) view.findViewById(R.id.angry_btn);
	if (showOkBtn > 0) {
		button.setVisibility(View.VISIBLE);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				if(showOkBtn == GPS_ENABLED){
					  startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				}else if(showOkBtn == YOGA_DELETE){
                    if(bundleFilename != null) {
                        File file = new File(bundleFilename);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                    mFragmentViewCallback.onNegativeButtonPressed();
                }
				
				dismiss();

			}
		});
	} else {
		button.setVisibility(View.GONE);

	}
	
	Button cancelButton = (Button) view.findViewById(R.id.cancel_btn);
	if (showCancelBtn > 0) {
		cancelButton.setVisibility(View.VISIBLE);
		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				dismiss();
                if( mFragmentViewCallback != null) {
                    mFragmentViewCallback.onNegativeButtonPressed();
                }

			}
		});
	} else {
		cancelButton.setVisibility(View.GONE);
	}

	View progress = view.findViewById(R.id.progressBar);
	if (shwoSearch > 0) {
		progress.setVisibility(View.VISIBLE);
	} else {
		progress.setVisibility(View.GONE);
	}
	builder.setView(view);   
    
    return builder.create();
  
	}

	
}
