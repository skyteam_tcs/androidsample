package org.skyteam.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;

import org.skyteam.R;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyTeamVideoPlayer;
import org.skyteam.activities.SkyYogaActivity;
import org.skyteam.loaders.DownloadCallback;
import org.skyteam.loaders.VideoDownloadAsyncTask;
import org.skyteam.loaders.VideoDownloadParams;
import org.skyteam.utils.NetworkUtil;


import java.io.File;



/**
 * 
 *
 * 
 */
public class InflightYogaFragment extends Fragment implements DownloadCallback,FragmentViewCallback{
    public static final String NAME_INFLIGHTYOGA_VIDEO="SkyTeamYogaInflight.mp4";
    public static final String NAME_ANYWHERE_VIDEO="SkyTeamYogaAnywhere.mp4";
    public static final String EXT_FILE_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath();
    private FragmentActivity mActivity;
    private TextView mFileInfo;
    private ToggleButton mToggleButton;
    private AlertDialog.Builder mAlertBuilder;
    private String mVideoType;
    private Context mContext;
    private String  mUrl= StringConstants.YOGA_INFLIGHT_URL;
    private ImageView seekedView;
    private Dialog mDialog;

    private String fileSavePath;
    private String fileDownloadUrl;
    private InfoDialogFragment mInfoDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_inflight_video, container, false);
        mActivity = this.getActivity();
        mVideoType= this.getArguments().getString(SkyYogaActivity.TABS_YOGA);

        if(mVideoType == SkyYogaActivity.TAB_INFLIGHT) {
            fileSavePath = EXT_FILE_PATH+"/"+NAME_INFLIGHTYOGA_VIDEO;
            fileDownloadUrl =StringConstants.YOGA_INFLIGHT_URL;
        }else if(mVideoType == SkyYogaActivity.TAB_ANYWHERE){
            fileSavePath = EXT_FILE_PATH+"/"+NAME_ANYWHERE_VIDEO;
            fileDownloadUrl =StringConstants.YOGA_ANYWHERE_URL;
        }

        mContext =this.getActivity().getApplicationContext();

        Bundle bundle=new Bundle();
        bundle.putString(InfoDialogFragment.EXTRA_BUNDLEARGS_FILENAME, fileSavePath);
        mInfoDialog = InfoDialogFragment.newInstance(mActivity.getString(R.string.yoga_inflight_alert_msg),
                InfoDialogFragment.YOGA_DELETE, 1, 0,bundle,this);



        mToggleButton = (ToggleButton) view.findViewById(R.id.toggleBtn);
        mToggleButton.setChecked(checkVideoExists());
        mFileInfo = (TextView) view.findViewById(R.id.yoga_file_info);
        seekedView = (ImageView) view.findViewById(R.id.viewtr);
        Uri videoURI;
        String fileSize = getResources().getString(R.string.yoga_file_info) +" "+46+SkyYogaActivity.videoSizeMetric;
        if(checkVideoExists()) {
            videoURI = Uri.parse(fileSavePath);
        }
        else{
            videoURI =Uri.parse(fileDownloadUrl);

        }

        /*updating the file size */
        if(mVideoType == SkyYogaActivity.TAB_INFLIGHT) {
            fileSize = getResources().getString(R.string.yoga_file_info) +" "+46+SkyYogaActivity.videoSizeMetric;
            seekedView.setBackgroundResource(R.drawable.inflight);
        }else if(mVideoType == SkyYogaActivity.TAB_ANYWHERE){
            fileSize = getResources().getString(R.string.yoga_file_info_anywhere) +" "+68+SkyYogaActivity.videoSizeMetric;
            seekedView.setBackgroundResource(R.drawable.anywhere);
        }
        mFileInfo.setText(fileSize);
        /*updating the file size */


       final ImageButton playBtn = (ImageButton) view.findViewById(R.id.yoga_inflight_play_ctrl);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (checkVideoExists() || NetworkUtil
                            .getConnectivityStatus(getActivity().getApplicationContext()) > 0) {
                        startVideoPlayerActivity();
                    } else {
                        String alert = getString(R.string.sky_internet_alert_error);

                        InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert,
                                1, 0, 0);
                        dialog.show(getActivity().getSupportFragmentManager(), alert);

                    }


            }
        });

        //mToggleButton.setChecked(checkVideoExists());
        mToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mToggleButton.isChecked()){
                    executeTask();
                }else{
                    mInfoDialog.show(mActivity.getSupportFragmentManager(), mActivity.getString(R.string.yoga_inflight_alert_msg));
                }
            }
        });



        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        mToggleButton.setChecked(checkVideoExists());

    }

    @Override
    public void onDownloadSuccess(String result) {
        //No implentation required
    }


    @Override
    public void onDownloadFailure(String result) {
        if(checkVideoExists()){
            new File(fileSavePath).delete();
        }
        mToggleButton.setChecked(checkVideoExists());
    }

    @Override
    public void onDownloadCancelled(String result) {
        if(checkVideoExists()){
            new File(fileSavePath).delete();
        }
        mToggleButton.setChecked(checkVideoExists());

    }

    private void startVideoPlayerActivity(){
        Intent intent = new Intent(this.getActivity(), SkyTeamVideoPlayer.class);
        mUrl = fileDownloadUrl;
        if(checkVideoExists()){
            mUrl = fileSavePath;
        }
        intent.putExtra(SkyTeamVideoPlayer.BUNDLE_EXTRA_VIDEOURL,mUrl);
        startActivity(intent);
    }
    public Boolean checkVideoExists(){

        return new File(fileSavePath).exists();
    }
    private void executeTask(){
        if (NetworkUtil
                .getConnectivityStatus(getActivity().getApplicationContext()) <= 0) {

            String alert = getString(R.string.sky_internet_alert_error);

            InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert,
                    1, 0, 0);
            dialog.show(getActivity().getSupportFragmentManager(), alert);
            mToggleButton.setChecked(false);

        }
        else {
            VideoDownloadParams videoParams = new VideoDownloadParams(fileDownloadUrl, fileSavePath);
            new VideoDownloadAsyncTask(this, this.getActivity(),this.getActivity()).execute(videoParams);
        }
    }

    @Override
    public void onPositiveButtonPressed() {
        mToggleButton.setChecked(checkVideoExists());
    }

    @Override
    public void onNegativeButtonPressed() {
        mToggleButton.setChecked(checkVideoExists());
    }
}
