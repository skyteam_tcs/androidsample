package org.skyteam.fragments;

/**
 * Created by Balaji on 4/3/15.
 */
public interface FragmentViewCallback {

    void onPositiveButtonPressed();
    void onNegativeButtonPressed();
}
