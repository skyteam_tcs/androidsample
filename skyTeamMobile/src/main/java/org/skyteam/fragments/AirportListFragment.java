package org.skyteam.fragments;

import java.util.ArrayList;
import java.util.List;

import org.skyteam.activities.SkyTeamMapActivity;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.adapters.AirportSearchListAdapter;
import org.skyteam.data.AirportDetails;

import com.google.gson.Gson;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;


/**
 * 
 * Displays Neraby and searched Airports in ListView.Supports Localization support
 * 
 */
public class AirportListFragment extends ListFragment {
	private List<AirportDetails> mAirportDetails;
	private AirportSearchListAdapter mAdapter;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setListShown(true);
		
		SkyTeamMapActivity activity = (SkyTeamMapActivity) getActivity();
		
		if (activity != null) {

			if (mAirportDetails == null || !mAirportDetails.equals(activity.getSelectedAirport())) {
				mAirportDetails = activity.getSelectedAirport();
				if(mAirportDetails==null){
					mAirportDetails=new  ArrayList<AirportDetails>();
				}
				mAdapter = new AirportSearchListAdapter(getActivity(), mAirportDetails);
			}
		}
		if (mAdapter == null) {
			mAdapter = new AirportSearchListAdapter(getActivity(),new ArrayList<AirportDetails>());
		}
		if(mAdapter!=null){
		     setListAdapter(mAdapter);
		}
	}

	public void setData(List<AirportDetails> airportDetails) {
		this.mAirportDetails = airportDetails;
		notrifyDataSetChanged();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		
		Intent intent = new Intent(getActivity(), SkyteamWebviewActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		
		AirportDetails airportObj=mAdapter.getItem(position);
		
		Gson gson=new Gson();
		intent.putExtra("AirportObj",gson.toJson(airportObj));
		
		getActivity().setResult(50,intent);
		getActivity().startActivity(intent);
		
		
	}

	
	public void notrifyDataSetChanged() {
		mAdapter.clear();
		if (mAirportDetails != null) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				mAdapter.addAll(mAirportDetails);
			} else {
				for (AirportDetails airDetails : mAirportDetails) {
					mAdapter.add(airDetails);
				}
			}
		}

	}

}
