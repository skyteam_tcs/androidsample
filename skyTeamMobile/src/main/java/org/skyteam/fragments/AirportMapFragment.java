package org.skyteam.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.skyteam.R;
import org.skyteam.activities.SkyTeamMapActivity;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.data.AirportDetails;
import org.skyteam.data.Location;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.Utils;

import java.util.HashMap;
import java.util.List;

public class AirportMapFragment extends SupportMapFragment implements OnMapReadyCallback {
	private GoogleMap mGoogleMap;
	private List<AirportDetails> mSelectedAirport;
	HashMap<String,AirportDetails> markerObj=null;
	private Typeface mFaceTnb;
	private Location mSearchedAirport;
	private LatLng mcenterMarker;
	private String mLang;
	private Context mContext;

	@Override
	public void onMapReady(GoogleMap googleMap) {
		this.mGoogleMap = googleMap;
		mGoogleMap.setInfoWindowAdapter(mInfoWindowAdapter);
		mGoogleMap.setOnInfoWindowClickListener(mWindowClickListener);
		mGoogleMap.setOnMarkerClickListener(mMarkerClickListener);
	}

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		mContext=getActivity();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getMapAsync(this);
		//mGoogleMap = getMapAsync(this);
		/*mGoogleMap = new SupportMapFragment();
		mGoogleMap.getMapAsync(this);*/
		/*SupportMapFragment fragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.mGoogleMap);
		fragment.getMapAsync(this);*/
		//mGoogleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mGoogleMap)).getMapAsync();
		/*mGoogleMap = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mGoogleMap);
		mGoogleMap.getMapAsync();*/
		if (mGoogleMap == null) {
			return;
		}
		SkyTeamMapActivity activity = (SkyTeamMapActivity) getActivity();
		try {
			mFaceTnb = Typeface.createFromAsset(getActivity().getAssets(), "html/css/fonts/Helvetica_Neue/419a308d-b777-4f84-9235-2caf4a98ec23.ttf");
		} catch (Exception e) {
			// in few phone its is not working
			mFaceTnb = Typeface.SANS_SERIF;
		}


		if (activity != null) {


			DBHelper dbHelper = DBHelper.getInstance(getActivity());

			try {
				mLang= dbHelper.getSettingFileObj().getString("Language");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (mSelectedAirport == null || !mSelectedAirport.equals(activity.getSelectedAirport())) {
				mSelectedAirport = activity.getSelectedAirport();
				mSearchedAirport = activity.getSerarchedAirport();
				showMarkers(mSearchedAirport);
			}


		}
		/*mGoogleMap.setInfoWindowAdapter(mInfoWindowAdapter);
		mGoogleMap.setOnInfoWindowClickListener(mWindowClickListener);
		mGoogleMap.setOnMarkerClickListener(mMarkerClickListener);*/
	}

	private void showMarkers(Location searchedAirport) {
		LatLng center = null;
		if (mSelectedAirport == null)
			return;
		mGoogleMap.clear();
		final LatLngBounds.Builder builder = LatLngBounds.builder();
		BitmapDescriptor markerIcon; // = BitmapDescriptorFactory.fromResource(R.drawable.ic_airportmarker_new);
		int pos = 0;

		markerObj=new HashMap<String,AirportDetails>();
		for (AirportDetails d : mSelectedAirport) {
            if(d != null) {
                if (d.getLocationType().equals("Railway")) {
                    markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_rail_marker_new);
                } else {
                    markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_airportmarker_new);
                }
                LatLng latLng = new LatLng(d.getLatitude(), d.getLongitude());
                MarkerOptions markerOpt = new MarkerOptions().position(latLng).title(d.getAirportName() + " " + Utils.getDistance(mContext, d.getDistance())).icon(markerIcon)
                        .snippet(Integer.toString(pos));
                Marker marker = mGoogleMap.addMarker(markerOpt);
                markerObj.put(Integer.toString(pos), d);
                builder.include(latLng);
                if (mSearchedAirport == null && pos == 0) {
                    marker.showInfoWindow();
                    center = latLng;
                    mcenterMarker = latLng;
                } else if (mSearchedAirport != null) {
                    String code = mSearchedAirport.getAirportCode();
                    if (d.getAirportCode().equalsIgnoreCase(code)) {

                        marker.showInfoWindow();
                        mcenterMarker = latLng;
                        mGoogleMap.addMarker(new MarkerOptions()
                                .position(mcenterMarker)
                                .title("currentLocation")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_blue_marker)));
                    }
                    center = latLng;
                }

                if (mSearchedAirport == null && pos == 0) {

                    mGoogleMap.addMarker(new MarkerOptions()
                            .position(mcenterMarker)
                            .title("currentLocation")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_blue_marker)));
                }
                pos++;
            }

		}



		mGoogleMap.moveCamera(CameraUpdateFactory.scrollBy(1, 1));

		if(pos==1){
			mGoogleMap.setOnCameraChangeListener(new OnCameraChangeListener() {

				@Override
				public void onCameraChange(CameraPosition arg0) {

					LatLngBounds bounds = builder.build();
					CameraPosition cameraPosition =
					        new CameraPosition.Builder().target(mcenterMarker)
					                .zoom(8.0f)
					                .build();
					mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
					mGoogleMap.setOnCameraChangeListener(null);
				}
			});

		}else{

			mGoogleMap.setOnCameraChangeListener(new OnCameraChangeListener() {
				@Override
				public void onCameraChange(CameraPosition arg0) {

					CameraPosition cameraPositionNew =
					        new CameraPosition.Builder().target(mcenterMarker)
					                .zoom(8.0f)
					                .build();
					mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPositionNew));
					mGoogleMap.setOnCameraChangeListener(null);
				}
			});
		}


	}

	private OnMarkerClickListener mMarkerClickListener = new OnMarkerClickListener() {

		@Override
		public boolean onMarkerClick(Marker marker) {
			if("currentLocation".equalsIgnoreCase(marker.getTitle())){
				return true;
			}
			return false;
		}
	};

	private final InfoWindowAdapter mInfoWindowAdapter = new InfoWindowAdapter() {
		LayoutInflater mInflater;

		@Override
		public View getInfoWindow(Marker marker) {
			if(mInflater==null){
				mInflater = LayoutInflater.from(getActivity());
			}
			View view = mInflater.inflate(R.layout.map_infowindow, null);
			TextView tv = (TextView) view.findViewById(R.id.text);
			tv.setText(marker.getTitle());
			tv.setTypeface(mFaceTnb);
			return view;
		}

		@Override
		public View getInfoContents(Marker marker) {
//			if("currentLocation".equalsIgnoreCase(marker.getTitle())){
//				return null;
//			}else{
//
//				TextView view = (TextView) getActivity().getLayoutInflater().inflate(R.layout.marker_infowindow, null);
//				view.setText(marker.getTitle());
//				return view;
//			}
			return null;

		}
	};

	private final OnInfoWindowClickListener mWindowClickListener = new OnInfoWindowClickListener() {

		@Override
		public void onInfoWindowClick(Marker marker) {

			if(!"currentLocation".equalsIgnoreCase(marker.getTitle())){

			Intent intent = new Intent(getActivity(), SkyteamWebviewActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			AirportDetails airportObj=markerObj.get(marker.getSnippet());

			Gson gson=new Gson();
			intent.putExtra("AirportObj",gson.toJson(airportObj));

			getActivity().setResult(50,intent);
			getActivity().startActivity(intent);
			}
		}
	};


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	public void setData(List<AirportDetails> mSelectedAirport, Location searchedAirport) {
		this.mSelectedAirport = mSelectedAirport;
		this.mSearchedAirport = searchedAirport;
		showMarkers(searchedAirport);
	}


}
