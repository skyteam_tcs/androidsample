/*
package org.skyteam.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.skyteam.R;

public class SkyTeamCustomerSurveyActivity extends Activity {

    private WebView mwebView;
    private ImageButton mclose;
    private ProgressBar mprogress;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sky_team_customer_survey);

//        final String surveyUrl="https://websurvey2.opinionbar.com/go.asp?s=p32838_EN&c=IN&f=;1,1,,;2,1,samsung,;3,1,FlightFinder,;4,1,,;5,1,EN,;6,1,,;";
        Bundle bundle = getIntent().getExtras();
        final String surveyUrl = bundle.getString("surveyUrl");

        //webView = new WebView(this);
        mwebView = (WebView) findViewById(R.id.wv_customerSurvey);
        mwebView.getSettings().setJavaScriptEnabled(true); // enable javascript
        mclose = (ImageButton) findViewById(R.id.close_icon);
        mprogress = (ProgressBar) findViewById(R.id.progressBar);

        final Activity activity = this;



        mwebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mprogress.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                final AlertDialog alertDialog = new AlertDialog.Builder(SkyTeamCustomerSurveyActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Alert Dialog");

                // Setting Dialog Message
                alertDialog.setMessage("Webpage not available");

                // Setting OK Button
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog closed
                        alertDialog.dismiss();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.clearView();
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mprogress.setVisibility(View.GONE);
            }
        });

*/
/*
            mwebView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view, int progress) {
                activity.setTitle("Loading...");
                activity.setProgress(progress * 100);
                if(progress == 100)
                    activity.setTitle("My title");
            }
        });
*//*


        mclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SkyTeamCustomerSurveyActivity.this,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("IsHomePage", true);
                intent.putExtra("HTMLPage", "homePage");
                startActivity(intent);
                finish();
            }
        });
        mwebView.loadUrl(surveyUrl);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
        Intent intent = new Intent(SkyTeamCustomerSurveyActivity.this,
                SkyteamWebviewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("IsHomePage", true);
        intent.putExtra("HTMLPage", "homePage");
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
*/
