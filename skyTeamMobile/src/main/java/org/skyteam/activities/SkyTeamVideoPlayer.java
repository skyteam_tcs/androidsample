package org.skyteam.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

import org.skyteam.R;
import org.skyteam.fragments.InfoDialogFragment;

/**
 * Created by sowmya on 3/30/15.
 */
public class SkyTeamVideoPlayer extends FragmentActivity implements MediaPlayer.OnCompletionListener,MediaPlayer.OnPreparedListener {

    private VideoView mVideoView;
    private String mVideoUrl;
    private DialogFragment dialog;
    public static final String BUNDLE_EXTRA_VIDEOURL ="VideoUrl";
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       // this.overridePendingTransition(R.anim.slide_out_left,R.anim.slide_in_right);
      //  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_videoplayer);
        mVideoView = (VideoView) findViewById(R.id.videoPlayer);
        mVideoUrl = getIntent().getExtras().getString(BUNDLE_EXTRA_VIDEOURL);
        Uri videoURI = Uri.parse(mVideoUrl);
        MediaController mediaController =new MediaController(this);
        mediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mediaController);
        mVideoView.setVideoURI(videoURI);
        mVideoView.setOnCompletionListener(this);
        mVideoView.setOnPreparedListener(this);
        dialog = InfoDialogFragment.newInstance(
                getString(R.string.sky_loading_map), 0, 0, 1);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(),"Preparing...");

    }


    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mVideoView.start();
        dialog.dismiss();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}
