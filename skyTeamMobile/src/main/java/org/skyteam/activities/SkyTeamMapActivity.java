package org.skyteam.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.simonvt.menudrawer.MenuDrawer;

import org.json.JSONException;
import org.skyteam.R;
import org.skyteam.StringConstants;
import org.skyteam.adapters.AirportArrayAdapter;
import org.skyteam.adapters.MenuAdapter;
import org.skyteam.data.AirportDeserilizer;
import org.skyteam.data.AirportDetails;
import org.skyteam.data.AirportsSearch;
import org.skyteam.data.Location;
import org.skyteam.database.DBHelper;
import org.skyteam.fragments.AirportListFragment;
import org.skyteam.fragments.AirportMapFragment;
import org.skyteam.fragments.InfoDialogFragment;
import org.skyteam.loaders.AsyncLoader;
import org.skyteam.loaders.AsyncResult;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.SkyTeamException;
import org.skyteam.utils.Utils;
import org.skyteam.view.SideMenuView;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

//import org.skyteam.data.CustomerSurveyData;
/*import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;*/

/**
 *
 * Activity that hosts the Map and List view
 *
 */
public class SkyTeamMapActivity extends FragmentActivity implements
        LoaderCallbacks<AsyncResult>,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MenuAdapter.MenuListener {

    public static final String SHOULD_SHOW_BACK = "SHOULD_SHOW_BACK";
    protected MenuDrawer mMenuDrawer;
    private ListView mList;
    private MenuAdapter mMenuAdapter;
    private int mActivePosition = 0;

    private static final int LOADER_ID_AIRPORTS = 0x1;
    private static final int LOADER_ID_SEARCH_LATLNG = 0x2;

    private GoogleApiClient mLocationClient;
    private String mLang;
    private String loadAirportName;
    private FragmentTabHost mTabHost;
    private AutoCompleteTextView mAutoCompleteTextView;
    private AirportArrayAdapter mAdapter;
    private List<AirportDetails> mSelectedAirport;
    private Typeface mFaceTnb;
    private ImageButton mCloseBtn;
    private ImageButton mUserBtn;
    private TextView titleMap;
    private TextView titleList;
    private Handler mHandler = new Handler();
    private Location mSerarchedAirport;
    private ImageButton mBackButton;
    private SideMenuView mSideMenuView;
    private TextWatcher mTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            //No implementation required
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            //No implementation required
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0) {
                mCloseBtn.setImageDrawable(getResources().getDrawable(
                        R.drawable.ic_close));
            } else {
                mCloseBtn.setImageDrawable(getResources().getDrawable(
                        R.drawable.ic_icon_search));
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.overridePendingTransition(R.anim.slide_out_left,R.anim.slide_in_right);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        try {
            mLang= Utils.getCurrentLanguage(this);
        } catch (JSONException e) {
            mLang = "En";
            e.printStackTrace();
        }

        Utils.setAppLocale(this,mLang);

        mFaceTnb = Typeface.createFromAsset(getAssets(),
                "html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf");
        mSideMenuView =new SideMenuView(this,mLang);
        mMenuDrawer = mSideMenuView.constructMenuDrawer(this,mFaceTnb, mItemClickListener, SideMenuView.VIEW_MAP);

        findViewById(R.id.menu_button).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mMenuDrawer.toggleMenu(true);
                    }
                });

        ((TextView) findViewById(R.id.header)).setTypeface(mFaceTnb);
        mCloseBtn = (ImageButton) findViewById(R.id.btn_close);
        mUserBtn = (ImageButton) findViewById(R.id.user_menu_button);

        mBackButton = (ImageButton) findViewById(R.id.back_button);

        LoaderManager lm = getSupportLoaderManager();
        lm.initLoader(LOADER_ID_AIRPORTS, null, this);

        mAdapter = new AirportArrayAdapter(this, R.layout.autocomplete_list);
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        View mapTabIndicator = LayoutInflater.from(this).inflate(
                R.layout.tab_indicator_left, mTabHost.getTabWidget(), false);
        LinearLayout mapLayout =(LinearLayout)mapTabIndicator.findViewById(R.id.llleft);

        titleMap = (TextView) mapTabIndicator.findViewById(android.R.id.title);

        titleMap.setText(R.string.sky_map);
        titleMap.setTextColor(Color.parseColor("#83458b"));
        titleMap.setTypeface(mFaceTnb, Typeface.BOLD);

        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);


        View listTabIndicator = LayoutInflater.from(this).inflate(
                R.layout.tab_indicator_right, mTabHost.getTabWidget(), false);

        LinearLayout listLayout =(LinearLayout)listTabIndicator.findViewById(R.id.llright);

        titleList = (TextView) listTabIndicator.findViewById(android.R.id.title);

        titleList.setText(R.string.sky_list);
        titleList.setTextColor(Color.parseColor("#838383"));
        titleList.setTypeface(mFaceTnb, Typeface.BOLD);


        //RTL support

        if(mLang.equalsIgnoreCase(StringConstants.LANG_ARABIC)){
            mTabHost.addTab(mTabHost.newTabSpec("LIST").setIndicator(listTabIndicator),
                    AirportListFragment.class, null);
            mTabHost.addTab(mTabHost.newTabSpec("MAP").setIndicator(mapTabIndicator),
                    AirportMapFragment.class, null);

            listLayout.setBackgroundResource(R.drawable.sel_tab_left);
            mapLayout.setBackgroundResource(R.drawable.sel_tab_right);

            mTabHost.setCurrentTab(1);

        }else{
            mTabHost.addTab(mTabHost.newTabSpec("MAP").setIndicator(mapTabIndicator),
                    AirportMapFragment.class, null);
            mTabHost.addTab(mTabHost.newTabSpec("LIST").setIndicator(listTabIndicator),
                    AirportListFragment.class, null);


        }


        mTabHost.getTabWidget().setDividerDrawable(null);
        mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        mAutoCompleteTextView.setTypeface(mFaceTnb);
        if ("zh".equalsIgnoreCase(mLang) || "Ja".equalsIgnoreCase(mLang)) {
            mAutoCompleteTextView.setThreshold(0);
        }

        mAutoCompleteTextView.setTextColor(Color.parseColor("#838383"));

        mAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                mSerarchedAirport = (Location) parent
                        .getItemAtPosition(position);
                onAirportSelected();
            }
        });
        mAutoCompleteTextView
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            if (mAdapter.getCount() == 1) {
                                mSerarchedAirport = mAdapter.getItem(0);
                                onAirportSelected();
                            }
                            return true;
                        }
                        return false;
                    }
                });

        mAutoCompleteTextView.addTextChangedListener(mTextWatcher);

        mAutoCompleteTextView.setAdapter(mAdapter);

        if (NetworkUtil.getConnectivityStatus(SkyTeamMapActivity.this) > 0) {
            if (mLocationClient == null) {

                //mLocationClient = new LocationClient(this, this, this);
                mLocationClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();

                mLocationClient.connect();
            } else {
                mLocationClient.connect();
            }
        } else {

            String alert = getString(R.string.sky_internet_alert_error);

            InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert,
                    1, 0, 0);
            dialog.show(getSupportFragmentManager(), alert);

        }

        mCloseBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mAutoCompleteTextView.setText("");

            }

        });

        mUserBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mAutoCompleteTextView.clearFocus();
                Intent intent = new Intent(SkyTeamMapActivity.this,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("IsHomePage", true);
                intent.putExtra("HTMLPage", "userMenu");
                intent.putExtra("currentPage", "airportSearchPage");
                startActivity(intent);

            }

        });

        mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {

                if (tabId == "LIST") {
                    titleMap.setTextColor(Color.parseColor("#838383"));
                    titleList.setTextColor(Color.parseColor("#83458b"));
                } else {
                    titleMap.setTextColor(Color.parseColor("#83458b"));
                    titleList.setTextColor(Color.parseColor("#838383"));
                }

            }
        });

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!(manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
                && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            String alert = this.getString(R.string.no_GPS);

            InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert,
                    InfoDialogFragment.GPS_ENABLED, 1, 0);
            dialog.show(getSupportFragmentManager(), alert);


        }

        onNewIntent(getIntent());

    }

    @Override
    protected void onStart() {
        super.onStart();
        /*if (AppStatusApplication.wasInBackground) {
            CustomerSurveyData.setsShared_PrefCheck(false);
            CustomerSurveyData.setsThreeStepsFlag(false);
            CustomerSurveyData.setsShowPopupFlag(false);
            CustomerSurveyData.setTimerStarts(false);
            CustomerSurveyData.setCsUrlCall(0);
            CustomerSurveyData.sScreensVisited=0;
            new SkyteamWebviewActivity().startTimer();
            new SkyteamWebviewActivity().cs_calculateNoOfDays();
            AppStatusApplication.wasInBackground = false;
        }*/
    }

    String Airpodecode_valid;
    protected void onAirportSelected() {
        Airpodecode_valid=null;
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        String url = null;
        in.hideSoftInputFromWindow(mAutoCompleteTextView.getWindowToken(), 0);

        if (mSerarchedAirport.getId() == -1) {
            mAutoCompleteTextView.clearFocus();
            mAutoCompleteTextView.setText(null);
        } else {
            String text = mSerarchedAirport.getCityName() + " ("
                    + mSerarchedAirport.getAirportCode() + "), "
                    + mSerarchedAirport.getCountryName();

            if ("zh".equalsIgnoreCase(mLang))  {
                url = StringConstants.createAirportSearchUrl(
                        mSerarchedAirport.getCityCode(),
                        mSerarchedAirport.getCountryCode(), "zh-Hans");
                text = mSerarchedAirport.getCityName() + " ("
                        + mSerarchedAirport.getAirportCode() + "), "
                        + mSerarchedAirport.getCountryName();

            } else if( "Es".equalsIgnoreCase(mLang)){
                url = StringConstants.createAirportSearchUrl(
                        mSerarchedAirport.getCityCode(),
                        mSerarchedAirport.getCountryCode(), "Es");
                text = mSerarchedAirport.getCityName() + " ("
                        + mSerarchedAirport.getAirportCode() + "), "
                        + mSerarchedAirport.getCountryName();
                //TODO

            }
            else if( "Ja".equalsIgnoreCase(mLang)){
                url = StringConstants.createAirportSearchUrl(
                        mSerarchedAirport.getCityCode(),
                        mSerarchedAirport.getCountryCode(), "Ja");
                text = mSerarchedAirport.getCityName() + " ("
                        + mSerarchedAirport.getAirportCode() + "), "
                        + mSerarchedAirport.getCountryName();
                //TODO

            }
            else{
                url = StringConstants.createAirportSearchUrl(
                        mSerarchedAirport.getCityCode(),
                        mSerarchedAirport.getCountryCode(), mLang);
            }
            mAutoCompleteTextView.setText(text);
            Airpodecode_valid=text;
            searchAirport(url);
            mAutoCompleteTextView.clearFocus();
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean flag = getIntent().getBooleanExtra(SHOULD_SHOW_BACK, false);
        if (flag) {
            mBackButton.setVisibility(View.VISIBLE);
        } else {
            mBackButton.setVisibility(View.INVISIBLE);
        }

        mBackButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SkyTeamMapActivity.this,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("IsHomePage", true);
                intent.putExtra("HTMLPage", "homePage");
                startActivity(intent);
                finish();

            }
        });
    }

    @Override
    public Loader<AsyncResult> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_ID_AIRPORTS) {
            return new AsyncLoader(this) {

                @Override
                protected AsyncResult getData() {
                    DBHelper dbHelper = DBHelper.getInstance(getContext());
//					List<Location> airports = dbHelper.getAllAirportsFromJson();
//                    AirportList airportList = dbHelper.getAllAirportsFromJson();

                    // allAirports contains airports, city, rail etc json data
//                    List<Location> allAirports = airportList.getLocations();

                    ArrayList<Location> airports = dbHelper.getAllAirportsFromJson();
                   /* List<Location> airports = null;

                    Iterator<Location> airportsIterator = allAirports.iterator();
                    while(airportsIterator.hasNext()) {
                        Location copyAirport = airportsIterator.next();
                        Log.d("copyAirport","locationtype"+copyAirport.getLocationType());
                        if ((copyAirport!=null)&&copyAirport.getLocationType().equalsIgnoreCase("APT")){
                            airports.add(copyAirport);
                        }
                    }*/
                    AsyncResult result = new AsyncResult();
                    result.setData(new ArrayList<Object>(airports));
                    return result;
                }
            };
        } else if (id == LOADER_ID_SEARCH_LATLNG) {
            final String u = args.getString("URL");
            return new AsyncLoader(this) {

                @Override
                protected AsyncResult getData() {
                    HttpsURLConnection urlConn = null;
                    InputStreamReader ism = null;
                    AsyncResult result = new AsyncResult();
                    try {
                        URL url = new URL(u);
                        urlConn = (HttpsURLConnection) url.openConnection();
                        urlConn.setConnectTimeout(Utils.TIME_OUT);
                        urlConn.setRequestProperty("api_key", StringConstants.API_KEY);
                        urlConn.setRequestProperty("source", "SkyApp");
                        urlConn.connect();
                        Gson gson = new GsonBuilder()
                                .setFieldNamingPolicy(
                                        FieldNamingPolicy.UPPER_CAMEL_CASE)
                                .registerTypeAdapter(AirportsSearch.class,
                                        new AirportDeserilizer()).create();
                        ism = new InputStreamReader(urlConn.getInputStream(),
                                "UTF-8");

                        AirportsSearch search = gson.fromJson(ism,
                                AirportsSearch.class);

                        if(search != null) {
                            List<AirportDetails> listAirports = search.getAirportDetails();
                            Iterator<AirportDetails> iteratorAirport = listAirports.iterator();
                            while (iteratorAirport.hasNext()) {
                                AirportDetails aDetail = iteratorAirport.next();
                                if (aDetail != null && (aDetail.getLocationType().equalsIgnoreCase("City")||aDetail.getLocationType().equalsIgnoreCase("Bus"))) {
                                   iteratorAirport.remove();
                                }
                            }
                            result.setData(listAirports);

                            loadAirportName = search.getAirportDetails().get(0).getCityName() + " ("
                                    +search.getAirportDetails().get(0).getAirportCode() + "), "
                                    + search.getAirportDetails().get(0).getCountryName();
                            if (Airpodecode_valid!=null) {
                                loadAirportName = Airpodecode_valid;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        result.setException(new SkyTeamException(e.getMessage()));
                    } finally {
                        if (ism != null) {
                            try {
                                ism.close();
                            } catch (Exception e2) {
                            }
                        }
                    }
                    return result;
                }
            };
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoadFinished(Loader<AsyncResult> loader, AsyncResult data) {
        switch (loader.getId()) {
            case LOADER_ID_AIRPORTS:
                mAdapter.addAll((List<Location>) data.getData());
                break;
            case LOADER_ID_SEARCH_LATLNG:
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {

                        DialogFragment f = (DialogFragment) getSupportFragmentManager()
                                .findFragmentByTag("search");
                        if (f != null) {
                            f.dismiss();
                            f = null;
                        }

                    }
                });
                if (data.getException() == null) {
                    mSelectedAirport = (List<AirportDetails>) data.getData();

                    if( mSelectedAirport != null && (mSelectedAirport.get(0) != null)) {
                        //Log.d("MAPTEST",""+mSelectedAirport.size());
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                AirportListFragment frag = (AirportListFragment) getSupportFragmentManager()
                                        .findFragmentByTag("LIST");
                                AirportMapFragment map = (AirportMapFragment) getSupportFragmentManager()
                                        .findFragmentByTag("MAP");
                                if (frag != null) {
                                    frag.setData(mSelectedAirport);
                                }
                                if (map != null) {
                                    map.setData(mSelectedAirport, mSerarchedAirport);
                                }
                            }
                        });
                    }else{
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                String alert = getString(R.string.map_noairport);

                                InfoDialogFragment dialog = InfoDialogFragment
                                        .newInstance(alert, 1, 0, 0);
                                dialog.show(getSupportFragmentManager(), "dialog");
                            }
                        });
                    }
                } else {

                    mHandler.post(new Runnable() {

                        @Override
                        public void run() {
                            String alert = getString(R.string.sky_service_alert_error);

                            InfoDialogFragment dialog = InfoDialogFragment
                                    .newInstance(alert, 1, 0, 0);
                            dialog.show(getSupportFragmentManager(), "dialog");
                        }
                    });

                }

                break;

            default:
                break;
        }
        mAutoCompleteTextView.setText(loadAirportName);
    }

    @Override
    public void onLoaderReset(Loader<AsyncResult> loader) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {

        Toast.makeText(SkyTeamMapActivity.this,
                "Error in Google Maps.Please try again later",
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onConnected(Bundle arg0) {

       // android.location.Location mlastLocation = LocationServices.getLastLocation(mLocationClient);
        android.location.Location mlastLocation = LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
        String url = null;

        if (mlastLocation != null) {
            if ("zh".equalsIgnoreCase(mLang)) {
                url = StringConstants.createAirportSearchUrl(

                        mlastLocation.getLatitude(), mlastLocation.getLongitude(),
                        "zh-Hans");

            } else {

                url = StringConstants.createAirportSearchUrl(
                        mlastLocation.getLatitude(),
                        mlastLocation.getLongitude(), mLang);
            }

            searchAirport(url);

        }
        mLocationClient.disconnect();

    }

    @Override
    /*public void onDisconnected() {

    }*/
    public void onConnectionSuspended(int i) {}

    private void searchAirport(String url) {
        Bundle args = new Bundle();
        args.putString("URL", url);
        LoaderManager lm = getSupportLoaderManager();
        lm.restartLoader(LOADER_ID_SEARCH_LATLNG, args, this);
        DialogFragment dialog = InfoDialogFragment.newInstance(
                getString(R.string.sky_loading_map), 0, 0, 1);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), "search");
    }

    public List<AirportDetails> getSelectedAirport() {
        return mSelectedAirport;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
        Intent intent = new Intent(SkyTeamMapActivity.this,
                SkyteamWebviewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("IsHomePage", true);
        intent.putExtra("HTMLPage", "homePage");
        startActivity(intent);
        finish();

    }

    @Override
    public void onActiveViewChanged(View v) {
        // TODO Auto-generated method stub

    }

    public Location getSerarchedAirport() {
        return mSerarchedAirport;
    }

    private final OnItemClickListener mItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view,
                                int position, long id) {
            mSideMenuView.menuItemClick( adapterView,  view,position,  id);


        }
    };
}
