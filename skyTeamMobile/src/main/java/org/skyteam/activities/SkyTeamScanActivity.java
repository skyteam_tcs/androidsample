package org.skyteam.activities;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.R;

import org.skyteam.StringConstants;
import org.skyteam.adapters.MenuAdapter;
//import org.skyteam.data.CustomerSurveyData;
import org.skyteam.database.DBHelper;
import org.skyteam.fragments.FragmentViewCallback;
import org.skyteam.fragments.InfoDialogFragment;
import org.skyteam.utils.AppStatusApplication;
import org.skyteam.utils.Utils;
import org.skyteam.view.SideMenuView;

import android.app.Activity;
import android.app.ProgressDialog;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.app.LoaderManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mirasense.scanditsdk.ScanditSDKAutoAdjustingBarcodePicker;
import com.mirasense.scanditsdk.ScanditSDKBarcodePicker;
import com.mirasense.scanditsdk.interfaces.ScanditSDK;
import com.mirasense.scanditsdk.interfaces.ScanditSDKListener;

import net.simonvt.menudrawer.MenuDrawer;

import java.util.HashMap;


public class SkyTeamScanActivity extends Activity implements ScanditSDKListener,MenuAdapter.MenuListener,FragmentViewCallback {
    private static final int LOADER_ID_SEARCH_LATLNG = 0x2;
    private ScanditSDK mBarcodeScanner;
    private Typeface mFaceTnb;
    private SideMenuView mSideMenuView;
    private ImageButton mSkyIcon,mBackButton;
    protected MenuDrawer mMenuDrawer;
    private String mLang;
    private ProgressDialog mDialog;
    private View dialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        try {
            mLang=Utils.getCurrentLanguage(this);
        } catch (JSONException e) {
            mLang = "En";
            e.printStackTrace();
        }
        Utils.setAppLocale(this,mLang);
        mSideMenuView =new SideMenuView(this,mLang);


        ScanditSDKBarcodePicker picker = new ScanditSDKBarcodePicker(
                this, this.getResources().getString(R.string.scan_key), ScanditSDKAutoAdjustingBarcodePicker.CAMERA_FACING_BACK);
        //set PDF417 Decoder
        picker.setPdf417Enabled(true);
        picker.set1DScanningEnabled(true);
        picker.set2DScanningEnabled(true);
        picker.setAztecEnabled(true);
        picker.setDataMatrixEnabled(true);
        picker.setQrEnabled(true);
        picker.setMicroDataMatrixEnabled(true);

        RelativeLayout overlayView = (RelativeLayout) picker.getOverlayView();

        View headerView = this.getLayoutInflater().inflate(R.layout.activity_sky_team_scan,null);

        RelativeLayout.LayoutParams rParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);


        overlayView.addView(headerView, rParams);

        setContentView(picker);

        mFaceTnb = Typeface.createFromAsset(getAssets(),
                "html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf");

        mDialog = new ProgressDialog(this);
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);

        dialogView = this.getLayoutInflater().inflate(R.layout.popup_dialog,null);
        Button btnCancel = (Button)dialogView.findViewById(R.id.btn_cancel_popup);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mMenuDrawer = mSideMenuView.constructMenuDrawer(this,mFaceTnb, mItemClickListener,-1);
        findViewById(R.id.menu_button).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mMenuDrawer.toggleMenu(true);
                    }
                });
        mSkyIcon = (ImageButton) findViewById(R.id.mysky_icon);
        mSkyIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mBarcodeScanner.stopScanning();
                Intent intent = new Intent(SkyTeamScanActivity.this,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("IsHomePage", true);
                intent.putExtra("HTMLPage", "userMenu");
                intent.putExtra("currentPage", "scanPage");
                startActivity(intent);
                finish();

            }

        });
        mBackButton = (ImageButton) headerView.findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                //  overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);

            }
        });

        mBarcodeScanner = picker;
        mBarcodeScanner.getOverlayView().addListener(this);
        // overridePendingTransition(R.anim.no_animation, R.anim.slide_out_left);
    }

    @Override
    protected void onStart() {
        super.onStart();
       /* if (AppStatusApplication.wasInBackground) {
            CustomerSurveyData.setsShared_PrefCheck(false);
            CustomerSurveyData.setsThreeStepsFlag(false);
            CustomerSurveyData.setsShowPopupFlag(false);
            CustomerSurveyData.setTimerStarts(false);
            CustomerSurveyData.setCsUrlCall(0);
            CustomerSurveyData.sScreensVisited=0;
            new SkyteamWebviewActivity().startTimer();
            new SkyteamWebviewActivity().cs_calculateNoOfDays();
            AppStatusApplication.wasInBackground = false;
        }
*/
    }

    @Override
    protected void onPause() {

        mBarcodeScanner.stopScanning();

        super.onPause();
        if ( mDialog !=null && mDialog.isShowing() ){
            mDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {

        mBarcodeScanner.startScanning();
        super.onResume();
    }

    public void didScanBarcode(String barcode, String symbology) {

        String cleanedBarcode = "";
        for (int i = 0 ; i < barcode.length(); i++) {
            if (barcode.charAt(i) > 30) {
                cleanedBarcode += barcode.charAt(i);
            }
        }
        if ( mDialog !=null && !mDialog.isShowing() ) {
            mDialog.show();
            mDialog.setContentView(dialogView);
        }

        mBarcodeScanner.stopScanning();
        Intent intent = new Intent();
        intent.putExtra("barcode",(cleanedBarcode));

        JSONObject jsonBarcode = null;



        try {
            jsonBarcode = new JSONObject();
            jsonBarcode.put("barcode",cleanedBarcode);
            jsonBarcode.put("symbology",symbology);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        new SkyteamWebviewActivity()
                .webViewLoadURL("javascript:iSU.getFlightDetailsFromScan(" + jsonBarcode
                        + ")");

        //Toast.makeText(this, symbology + ": " + cleanedBarcode, Toast.LENGTH_LONG).show();

    }


    public void didManualSearch(String entry) {
        //No implementation required
    }

    @Override
    public void didCancel() {
        mBarcodeScanner.stopScanning();
        if ( mDialog !=null && mDialog.isShowing() ){
            mDialog.dismiss();
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        mBarcodeScanner.stopScanning();
        if ( mDialog !=null && mDialog.isShowing() ){
            mDialog.dismiss();
        }
        finish();
        // overridePendingTransition(R.anim.no_animation, R.anim.slide_in_right);

    }

    private final AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view,
                                int position, long id) {

            mSideMenuView.menuItemClick(adapterView, view, position, id);
        }


    };

    @Override
    public void onActiveViewChanged(View v) {
        //No implementation required
    }
    @Override
    public void onPositiveButtonPressed(){
        //No implementation required
    }
    @Override
    public void onNegativeButtonPressed(){
        finish();
    }
}