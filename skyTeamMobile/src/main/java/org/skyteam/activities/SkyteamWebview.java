package org.skyteam.activities;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.webkit.WebView;


/**
 * 
 * The SkyteamWebview is used as a custom webview for this application
 * 
 */
public class SkyteamWebview extends WebView {

	@SuppressWarnings("unused")
	private Context context;
	//private GestureDetector gestureDetector;

	public SkyteamWebview(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	public SkyteamWebview(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public SkyteamWebview(Context context) {
		super(context);
		this.context = context;
	}
	/*public void setGestureDetector(GestureDetector gestureDetector) {
	    this.gestureDetector = gestureDetector;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
	    return gestureDetector.onTouchEvent(ev) || super.onTouchEvent(ev);
	}*/
	
	@Override
	public boolean onKeyPreIme(int keyCode, KeyEvent event) {
	// TODO Auto-generated method stub

	
	return super.onKeyPreIme(keyCode, event);
	} 

}
