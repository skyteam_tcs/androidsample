package org.skyteam.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import net.simonvt.menudrawer.MenuDrawer;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.R;
import org.skyteam.StringConstants;
import org.skyteam.adapters.MenuAdapter;
//import org.skyteam.data.CustomerSurveyData;
import org.skyteam.database.DBHelper;
import org.skyteam.fragments.InflightYogaFragment;
import org.skyteam.utils.AppStatusApplication;
import org.skyteam.utils.Utils;
import org.skyteam.view.SideMenuView;

public class SkyYogaActivity extends FragmentActivity implements MenuAdapter.MenuListener {

    protected MenuDrawer mMenuDrawer;
    private ListView mList;
    private MenuAdapter mMenuAdapter;
    private int mActivePosition = 0;
    private Typeface mFaceTnb;
    private SideMenuView mSideMenuView;
    private ImageButton mSkyIcon;
    private FragmentTabHost mTabHost;
    private TextView mTitleInflight,mTitleAnywhere;
    public static final String TAB_INFLIGHT ="Inflight";
    public static final String TAB_ANYWHERE="Anywhere";
    public static final String TABS_YOGA="tabnames";
    public static String videoSizeMetric="MB";
    public int bundle_fromskyteam;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        bundle_fromskyteam= getIntent().getIntExtra(StringConstants.BUNDLE_EXTRA_SKYTEAMYOGADIRECTION,0);
        if(bundle_fromskyteam == StringConstants.YOGA_LTR){
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else{
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

        DBHelper dbHelper = DBHelper.getInstance(SkyYogaActivity.this);
        JSONObject languageJsonObj = dbHelper.getSettingFileObj();

        String currentLang;

        try {
            currentLang= Utils.getCurrentLanguage(this);
        } catch (JSONException e) {
            currentLang = "En";
            e.printStackTrace();
        }
        Utils.setAppLocale(getBaseContext (),currentLang);


        mFaceTnb = Typeface.createFromAsset(getAssets(),
                "html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf");
        mSideMenuView =new SideMenuView(this,currentLang);

        mMenuDrawer = mSideMenuView.constructMenuDrawer(this,mFaceTnb, mItemClickListener,SideMenuView.VIEW_YOGA);
        findViewById(R.id.menu_button).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mMenuDrawer.toggleMenu(true);
                    }
                });
        mSkyIcon = (ImageButton) findViewById(R.id.mysky_icon);
        mSkyIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(SkyYogaActivity.this,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("IsHomePage", true);
                intent.putExtra("HTMLPage", "userMenu");
                intent.putExtra("currentPage", "yogaVideoPage");
                startActivity(intent);

            }

        });


        Bundle bundleInflight = new Bundle();
        bundleInflight.putString(TABS_YOGA, TAB_INFLIGHT);
        Bundle bundleAnyWhere = new Bundle();
        bundleAnyWhere.putString(TABS_YOGA, TAB_ANYWHERE);
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);




        //RTL support
        if(currentLang.equalsIgnoreCase(StringConstants.LANG_ARABIC)){

            View inFlightTabIndicator = LayoutInflater.from(this).inflate(
                    R.layout.tab_indicator_right, mTabHost.getTabWidget(), false);

            mTitleInflight = (TextView) inFlightTabIndicator.findViewById(android.R.id.title);
            mTitleInflight.setText(R.string.yoga_inflight);
            mTitleInflight.setTextColor(Color.parseColor("#83458b"));
            mTitleInflight.setTypeface(mFaceTnb, Typeface.BOLD);
            mTabHost.setup(this, getSupportFragmentManager(), R.id.yogatabcontent);

            View anywhereTabIndicator = LayoutInflater.from(this).inflate(
                    R.layout.tab_indicator_left, mTabHost.getTabWidget(), false);
            mTitleAnywhere = (TextView) anywhereTabIndicator.findViewById(android.R.id.title);
            mTitleAnywhere.setText(R.string.yoga_anywhere);
            mTitleAnywhere.setTextColor(Color.parseColor("#838383"));
            mTitleAnywhere.setTypeface(mFaceTnb, Typeface.BOLD);




            mTabHost.addTab(mTabHost.newTabSpec("ANYWHERE").setIndicator(anywhereTabIndicator),
                    InflightYogaFragment.class, bundleAnyWhere);
            mTabHost.addTab(mTabHost.newTabSpec("INFLIGHT").setIndicator(inFlightTabIndicator),
                    InflightYogaFragment.class, bundleInflight);
            mTabHost.setCurrentTab(1);
            videoSizeMetric =getResources().getString(R.string.file_size_metric);
        }else{
            View inFlightTabIndicator = LayoutInflater.from(this).inflate(
                    R.layout.tab_indicator_left, mTabHost.getTabWidget(), false);

            mTitleInflight = (TextView) inFlightTabIndicator.findViewById(android.R.id.title);
            mTitleInflight.setText(R.string.yoga_inflight);
            mTitleInflight.setTextColor(Color.parseColor("#83458b"));
            mTitleInflight.setTypeface(mFaceTnb, Typeface.BOLD);
            mTabHost.setup(this, getSupportFragmentManager(), R.id.yogatabcontent);

            View anywhereTabIndicator = LayoutInflater.from(this).inflate(
                    R.layout.tab_indicator_right, mTabHost.getTabWidget(), false);
            mTitleAnywhere = (TextView) anywhereTabIndicator.findViewById(android.R.id.title);
            mTitleAnywhere.setText(R.string.yoga_anywhere);
            mTitleAnywhere.setTextColor(Color.parseColor("#838383"));
            mTitleAnywhere.setTypeface(mFaceTnb, Typeface.BOLD);

            mTabHost.addTab(mTabHost.newTabSpec("INFLIGHT").setIndicator(inFlightTabIndicator),
                    InflightYogaFragment.class, bundleInflight);
            mTabHost.addTab(mTabHost.newTabSpec("ANYWHERE").setIndicator(anywhereTabIndicator),
                    InflightYogaFragment.class, bundleAnyWhere);
            videoSizeMetric =getResources().getString(R.string.file_size_metric);
        }






        mTabHost.getTabWidget().setDividerDrawable(null);
        TabWidget tabWidget = (TabWidget)findViewById(android.R.id.tabs);
        final int childCount = tabWidget.getChildCount();

        for (int i = 0; i < childCount; i++) {
            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) tabWidget.getChildAt(i).getLayoutParams();
            if(i == 0){
                layoutParams.setMargins(15, 0, 0, 0);
            }
            if( i== 1){
                layoutParams.setMargins(0, 0, 15, 0);
            }

            //  layoutParams.height = 100;
        }
        tabWidget.requestLayout();
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {

                if (tabId == "INFLIGHT") {
                    mTitleInflight.setTextColor(Color.parseColor("#83458b"));
                    mTitleAnywhere.setTextColor(Color.parseColor("#838383"));
                } else {
                    mTitleInflight.setTextColor(Color.parseColor("#838383"));
                    mTitleAnywhere.setTextColor(Color.parseColor("#83458b"));
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

       /* if (AppStatusApplication.wasInBackground) {
            CustomerSurveyData.setsShared_PrefCheck(false);
            CustomerSurveyData.setsThreeStepsFlag(false);
            CustomerSurveyData.setsShowPopupFlag(false);
            CustomerSurveyData.setTimerStarts(false);
            CustomerSurveyData.setCsUrlCall(0);
            CustomerSurveyData.sScreensVisited=0;
            new SkyteamWebviewActivity().startTimer();
            new SkyteamWebviewActivity().cs_calculateNoOfDays();
            AppStatusApplication.wasInBackground = false;
        }*/
    }

    private final AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view,
                                int position, long id) {

            mSideMenuView.menuItemClick(adapterView, view, position, id);
        }


    };

    @Override
    public void onActiveViewChanged(View v) {
        //No implementation required
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        Intent intent = new Intent(SkyYogaActivity.this,
                SkyteamWebviewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("IsHomePage", true);
        intent.putExtra("HTMLPage", "homePage");
        startActivity(intent);
        finish();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        bundle_fromskyteam = intent.getIntExtra(StringConstants.BUNDLE_EXTRA_SKYTEAMYOGADIRECTION,0);

        if(bundle_fromskyteam == StringConstants.YOGA_LTR){

            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else{

            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
