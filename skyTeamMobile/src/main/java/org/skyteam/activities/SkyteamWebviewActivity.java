package org.skyteam.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Logger.LogLevel;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.point_consulting.testindoormap.MapsActivity;
import com.tune.Tune;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.R;
import org.skyteam.StringConstants;
import org.skyteam.database.DBHelper;
import org.skyteam.fragments.InfoDialogFragment;
import org.skyteam.handlers.SettingsHandler;
import org.skyteam.utils.JSIObject;
import org.skyteam.utils.NetworkUtil;
import org.skyteam.utils.Utils;
import org.skyteam.utils.WebServiceInteraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

//import org.skyteam.data.CustomerSurveyData;
/*import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;*/

/**
 *
 * Main activity for the application that hosts the webview
 *
 */

public class SkyteamWebviewActivity extends FragmentActivity implements SkyCancelCallBack,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
{

    public static final String PREFS_NAME = "MyPrefsFile";
    private static final String TAG = "SKYTEAM";
    private static SkyteamWebview webView;
    private static SkyteamWebview webViewurl;
    ImageButton ImageButton, closebutton,backbutton,backwardButton,forwardButton,skylogopanel;
    private String STARTUP_URL = "file:///android_asset/html/index.html";

    Activity activity = null;
    @SuppressWarnings("unused")
    private static String CURRENT_HTML_URL = "";

    private BroadcastReceiver br_dateChange;
    private BroadcastReceiver br_wifiState;
    protected static String URL = null;
    private String mLang;

    private static SharedPreferences settings;
    private static SharedPreferences cs_popup;
    private ImageView iv_splash;
    private ProgressBar pb_splash;
    private ImageView  mSpinnerImage;
    private AnimationDrawable mFrameAnimation;
    private DBHelper dbhelperObj;
    protected Runnable exitRunnable = null;
    protected Handler exitHandler = null;
    protected static final int SPLASHTIME = 3000;
    private static int count = 0;
    @SuppressWarnings("unused")
    private static boolean homeScreenUP = false;
    private boolean SKYTIPSTHREAD_INPROGRESS = false;

    protected boolean sendSkytipsList = false;
    private boolean registeredWifiStateChange = false;
    private boolean doneLoadingStartupURL = false;

    private  CityAirportPairLangAsync asyncTask;

    //CustomerSurveyData customerSurveyData;

    private static GoogleAnalytics mGa;
    private static Tracker mTracker;

    // Placeholder property ID.
    private static final String GA_PROPERTY_ID = "UA-45492668-1";

    // Dispatch period in seconds.
    private static final int GA_DISPATCH_PERIOD = 20;

    // Prevent hits from being sent to reports, i.e. during testing.
    private static final boolean GA_IS_DRY_RUN = false;

    // GA Logger verbosity.
    private static final LogLevel GA_LOG_VERBOSITY = LogLevel.INFO;

    protected static final String GA_PAGEVIEW_PREFIX = "analytics://_trackPageview";
    protected static final String GA_EVENT_PREFIX = "analytics://_trackEvent";
    protected static final String GA_EVENTLANG_PREFIX = "analytics://_trackLanguage";

    private static final String PREF_SKYTIPS_UPDATE ="SkytipsUpdationDate";

    //Intent data

    public static final String INTENT_EXTRA_ISHOMEPAGE= "IsHomePage";
    public static final String INTENT_EXTRA_HTMLPAGE= "HTMLPage";
    public static final String INTENT_EXTRA_CURRENTPAGE="currentPage";

    private GoogleApiClient mLocationClient;
    private double latitude;
    private double longitude ;

    private String selectedAirportName;
    private String selectedAirportCode;
    private  ProgressBar progressBar;
    private int indexValuesMaps;
    String version;
    //  ******Timer code for Customer Survey Starts******

    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    /*public void startTimer() {
        runMethodByDelay();
       *//* //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        //schedule the timer, after the first 0sec the TimerTask will run every 90sec
        initializeTimerTask();
//        Log.d("Timer Started","msg");
        timer.schedule(timerTask,StringConstants.TIMERVALUE,StringConstants.TIMERVALUE);*//*
    }*/


    /*private void runMethodByDelay(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Log.d("runMethodByDelay","before-timer:" + CustomerSurveyData.isTimerStarts() );
                if(!CustomerSurveyData.isTimerStarts())   {
//                    Log.d("runMethodByDelay","completed");
                    CustomerSurveyData.setTimerStarts(true);
                    CustomerSurveyData.setsTime(true);
                    //timer.cancel();
                }
            }
        }, StringConstants.TIMERVALUE);
    }*/

    public void stopTimer() {
        timer.cancel();
    }
   /* public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        if(!CustomerSurveyData.isTimerStarts())   {
                            CustomerSurveyData.setTimerStarts(true);
                            CustomerSurveyData.setsTime(true);
                            timer.cancel();
                        }
                    }
                });
            }
        };
    }*/
//  ******Timer code for Customer Survey Ends******

    @SuppressLint("SetJavaScriptEnabled")
    public void onCreate(Bundle savedInstanceState) {




        try {

            Tune.init(this, "193279", "8be76ae355cae08ee03ce6bb28f2c567");
            super.onCreate(savedInstanceState);

//            Disabled InMobi as per Skyteam requirement
            //InMobi.initialize(this, "dc2cef79aeba4c8cbb48d3d3dc7524e3");

            activity = this;
            settings = this.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            cs_popup = this.getSharedPreferences(StringConstants.CUS_POPUP, MODE_PRIVATE);
            version = this.getPackageManager().getPackageInfo(
                    this.getPackageName(), 0).versionName;
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_skyteamwebview);
            iv_splash = (ImageView) findViewById(R.id.iv_splash);
            mSpinnerImage = (ImageView)findViewById(R.id.img_loading_spinner);
            mSpinnerImage.setBackgroundResource(R.drawable.spinneranimation);
            mFrameAnimation = (AnimationDrawable) mSpinnerImage.getBackground();
            mFrameAnimation.start();



            closebutton=(ImageButton)findViewById(R.id.closebtn);
            skylogopanel=(ImageButton)findViewById(R.id.skylogopanel);
            closebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    webViewurl.loadUrl("about:blank");
                    webViewurl.clearHistory();
                    webViewurl.clearCache(true);
                    webViewurl.invalidate();
                    webView.bringToFront();

                }
            });

            backbutton=(ImageButton)findViewById(R.id.backbtn);


            backwardButton=(ImageButton)findViewById(R.id.backButton);
            backwardButton.setX(10);

            forwardButton=(ImageButton)findViewById(R.id.forwardButton);
            forwardButton.setX(120);


            backwardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    webViewurl.goBack();
                }
            });

            forwardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    webViewurl.goForward();
                }
            });

            backbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    webViewurl.goBack();
                }
            });

            progressBar=findViewById(R.id.webViewProgressBar);
			/* Google analytics settings */
            initializeGa();
            //    CustomerSurveyData.setCountryLocation(this.getResources().getConfiguration().locale.getCountry());
            if (!version.equalsIgnoreCase(settings.getString("VersionCode",
                    "000"))) {

                if (!settings.getString("VersionCode", "000").equalsIgnoreCase("000")) {
                    Tune.getInstance().setExistingUser(true);
                    Tune.getInstance().measureSession();

                }





                String  dirDir = this.getFilesDir() + "/";
                File fileSettings= new File(dirDir+ DBHelper.SettingsJSONFileName);
                File fileCityAirport= new File(dirDir+ DBHelper.CityAirportJSONFileName);
                File fileSkyTipsAirport= new File(dirDir+ DBHelper.SkyTipsCityAirportJSONFileName);

                if(fileCityAirport.exists()){
                    fileCityAirport.delete();
                }
                if(fileSkyTipsAirport.exists()){
                    fileSkyTipsAirport.delete();
                }

                SharedPreferences.Editor editor = settings.edit();
                editor.putLong(PREF_SKYTIPS_UPDATE, 0);
                editor.commit();

                if(fileSettings.exists()){
                    if(!checkLanguageExists())
                    {
                        fileSettings.delete();
                    }
                    else
                    {
                        SettingsHandler settingsHandler = new SettingsHandler(SkyteamWebviewActivity.this);
                        String result = settingsHandler.nativeHandler(getInitialLoadString());
                    }
                }
            }
            dbhelperObj = DBHelper.getInstance(SkyteamWebviewActivity.this);

			/* Instantiate the database object */

            dbhelperObj.open();
			/* Perfom upgrade for 3.3 * */
            Log.d("Version--->",settings.getString("VersionCode","0000"));
            Log.d("1111--->",version);
            if (!version.equalsIgnoreCase(settings.getString("VersionCode",
                    "000"))) {
                if (dbhelperObj == null) {
                    dbhelperObj = DBHelper.getInstance(this);
                    dbhelperObj.open();
                }
                dbhelperObj.updgrade();
            }

            if (!dbhelperObj.dbExists()) {
                // exit from the app
//				System.out.println("DB file does not exist");
            }

            /**** Set initial SharedPreference Starts ******/
           /* SharedPreferences.Editor editor = cs_popup.edit();
            editor.putString("PREF_STATE", "Initial");
            editor.putLong("PREF_TS", Calendar.getInstance().getTimeInMillis());
            editor.commit();*/
            /**** Set initial SharedPreference Ends ******/

            LoadURLInWebView();
            // SkyteamWebview.setWebContentsDebuggingEnabled(true);
			/* Start downloading the new dump for cityairport */
            startDataDownlaod();

			/*
			 * Add broadcast for day change for downloading the new dump for
			 * cityairport, lounge and skytip
			 */

            br_dateChange = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Utils.logMessage(
                            TAG,
                            "Date change and have wifi .. starting data download",
                            Log.DEBUG);
                    startDataDownlaod();
                }

            };

            IntentFilter intent_date = new IntentFilter(
                    "android.intent.action.DATE_CHANGED");
            registerReceiver(br_dateChange, intent_date);

            getLocation();
            settings.edit().putString("VersionCode", version).commit();

        } catch (Exception e) {
            showToast(e.getMessage());
            e.printStackTrace();
        }

    }



    public class MyJavascriptInterface {
        Context context;
        public MyJavascriptInterface(Context context) {
            this.context = context;
        }
        @android.webkit.JavascriptInterface
        public void getStringFromJS(String bookingURL,String book_Ori,String book_Des,String book_OriDate,String book_RetDate,String book_lan,String oneortwo_way,String marketing_Code) {
            final String booking_URL=bookingURL;
            final String bookOri=book_Ori;
            final String bookDes=book_Des;
            final String bookOriDate=book_OriDate;
            final String bookRetDate=book_RetDate;
            final String booklan=book_lan;
            final String oneortwoway=oneortwo_way;
            final String marketingCode=marketing_Code;
            webView.post(new Runnable() {
                @Override
                public void run() {
                    try{
                        loadPostInWebview(booking_URL,bookOri,bookDes,bookOriDate,bookRetDate,booklan,oneortwoway,marketingCode);
                    }catch(Exception e){
                    }
                }
            });
        }

    }



    @SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
    public void loadPostInWebview(String bookingURL,String book_Ori,String book_Des,String book_OriDate,String book_RetDate,String book_lan,String oneortwo_way,String marketing_Code) throws IOException {

        if (marketing_Code.equalsIgnoreCase("KQ"))
        {

            webViewurl = (SkyteamWebview) findViewById(R.id.wvurl_skyteam);
            webViewurl.clearHistory();
            webViewurl.clearCache(true);
            //  webViewurl.loadUrl("about:blank");
            webViewurl.bringToFront();
            webView.invalidate();
            skylogopanel.setVisibility(View.VISIBLE);
            skylogopanel.bringToFront();
            closebutton.setVisibility(View.VISIBLE);
            closebutton.bringToFront();
            backbutton.setVisibility(View.VISIBLE);
            backbutton.bringToFront();

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            HttpClient httpclient = new DefaultHttpClient();

            book_lan=book_lan.toUpperCase();
            HttpPost httppost = new HttpPost(bookingURL);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("class", "E"));
            nameValuePairs.add(new BasicNameValuePair("travelers", "1ADT"));
            nameValuePairs.add(new BasicNameValuePair("from_1", book_Ori));
            nameValuePairs.add(new BasicNameValuePair("to_1", book_Des));
            if(oneortwo_way.equalsIgnoreCase("oneway")) {
                nameValuePairs.add(new BasicNameValuePair("date_1", book_OriDate));
                nameValuePairs.add(new BasicNameValuePair("tripType", "O"));
            }else{
                nameValuePairs.add(new BasicNameValuePair("from_2", book_Des));
                nameValuePairs.add(new BasicNameValuePair("to_2",book_Ori ));
                nameValuePairs.add(new BasicNameValuePair("date_1", book_OriDate));
                nameValuePairs.add(new BasicNameValuePair("date_2", book_RetDate));
                nameValuePairs.add(new BasicNameValuePair("tripType", "RT"));
            }
            nameValuePairs.add(new BasicNameValuePair("language", book_lan));
            httppost.setHeader("Referer","www.skyteam.com");

            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }
                String html = builder.toString();

                WebSettings webSettingsurl = webViewurl.getSettings();
                webSettingsurl.setJavaScriptEnabled(true);
                webSettingsurl.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
                webSettingsurl.setUseWideViewPort(true);
                webSettingsurl.setSaveFormData(false);
                // Enable DOM storage
                webSettingsurl.setDomStorageEnabled(true);
                webSettingsurl.setAllowFileAccess(true);
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                    webSettingsurl.setAllowFileAccessFromFileURLs(true);
                    webSettingsurl.setAllowUniversalAccessFromFileURLs(true);
                }
                webViewurl.getSettings().setJavaScriptEnabled(true);
                setWebViewClientForShowingProgressBar(webViewurl);
                webViewurl.loadDataWithBaseURL(bookingURL,html ,"text/html", "UTF-8",null);



            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }else if(marketing_Code.equalsIgnoreCase("CI")){



            webViewurl = (SkyteamWebview) findViewById(R.id.wvurl_skyteam);
            webViewurl.clearHistory();
            webViewurl.clearCache(true);
            //   webViewurl.loadUrl("about:blank");
            webViewurl.bringToFront();
            webView.invalidate();
            skylogopanel.setVisibility(View.VISIBLE);
            skylogopanel.bringToFront();
            closebutton.setVisibility(View.VISIBLE);
            closebutton.bringToFront();
            backbutton.setVisibility(View.VISIBLE);
            backbutton.bringToFront();


            String[] bookingURL_array = bookingURL.split("-");

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            HttpClient httpclient = new DefaultHttpClient();

            book_lan=book_lan.toUpperCase();
            HttpPost httppost = new HttpPost("https://bookingportal.china-airlines.com/eRetailPortal/Mobile.svc/Mobile/Search");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ADULTS", "1"));
            nameValuePairs.add(new BasicNameValuePair("CHILDS", "0"));
            nameValuePairs.add(new BasicNameValuePair("INFANTS", "0"));
            nameValuePairs.add(new BasicNameValuePair("CABIN", "Y"));
            nameValuePairs.add(new BasicNameValuePair("Channel", "CMS"));
            if(oneortwo_way.equalsIgnoreCase("oneway")) {
                nameValuePairs.add(new BasicNameValuePair("B_LOCATION_1", book_Ori));
                nameValuePairs.add(new BasicNameValuePair("E_LOCATION_1", book_Des));
                nameValuePairs.add(new BasicNameValuePair("B_DATE_1", book_OriDate.replace("-","")+"0000"));
                nameValuePairs.add(new BasicNameValuePair("TRIP_TYPE", "O"));
                nameValuePairs.add(new BasicNameValuePair("EBA", bookingURL_array[1]));
                nameValuePairs.add(new BasicNameValuePair("LANG", bookingURL_array[0]));
            }else{
                nameValuePairs.add(new BasicNameValuePair("B_LOCATION_1", book_Ori));
                nameValuePairs.add(new BasicNameValuePair("E_LOCATION_1", book_Des));
                nameValuePairs.add(new BasicNameValuePair("B_LOCATION_2", book_Des));
                nameValuePairs.add(new BasicNameValuePair("E_LOCATION_2", book_Ori));
                nameValuePairs.add(new BasicNameValuePair("B_DATE_1", book_OriDate.replace("-","")+"0000"));
                nameValuePairs.add(new BasicNameValuePair("B_DATE_2", book_RetDate.replace("-","")+"0000"));
                nameValuePairs.add(new BasicNameValuePair("TRIP_TYPE", "R"));
                nameValuePairs.add(new BasicNameValuePair("EBA", bookingURL_array[1]));
                nameValuePairs.add(new BasicNameValuePair("LANG", bookingURL_array[0]));
            }


            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }
                String html = builder.toString();

                WebSettings webSettingsurl = webViewurl.getSettings();
                webSettingsurl.setJavaScriptEnabled(true);
                webSettingsurl.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
                webSettingsurl.setUseWideViewPort(true);
                webSettingsurl.setSaveFormData(false);


                // Enable DOM storage
                webSettingsurl.setDomStorageEnabled(true);
                webSettingsurl.setAllowFileAccess(true);
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                    webSettingsurl.setAllowFileAccessFromFileURLs(true);
                    webSettingsurl.setAllowUniversalAccessFromFileURLs(true);
                }

                webViewurl.getSettings().setJavaScriptEnabled(true);
                setWebViewClientForShowingProgressBar(webViewurl);
                webViewurl.loadDataWithBaseURL("https://bookingportal.china-airlines.com/eRetailPortal/Mobile.svc/Mobile/Search",html ,"text/html", "UTF-8",null);



            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        } else if(marketing_Code.equalsIgnoreCase("MF")){

            webViewurl = (SkyteamWebview) findViewById(R.id.wvurl_skyteam);
            webViewurl.setWebViewClient(new WebViewClient());

            webViewurl.reload();
            webViewurl.loadUrl("about:blank");
            webViewurl.bringToFront();


            webViewurl.clearCache(true);
            webViewurl.clearHistory();
            webViewurl.getSettings().setJavaScriptEnabled(true);
            webViewurl.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


//            if (Build.VERSION.SDK_INT >= 21) {
//                webViewurl.getSettings().setMixedContentMode( WebSettings.MIXED_CONTENT_ALWAYS_ALLOW );
//            }


            skylogopanel.setVisibility(View.VISIBLE);
            skylogopanel.bringToFront();
            closebutton.setVisibility(View.VISIBLE);
            closebutton.bringToFront();
            backbutton.setVisibility(View.VISIBLE);
            backbutton.bringToFront();



//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);


            book_lan=book_lan.toUpperCase();
            //   HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost(bookingURL);
//            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//            nameValuePairs.add(new BasicNameValuePair("EMBEDDED_TRANSACTION", book_Ori));
//            nameValuePairs.add(new BasicNameValuePair("SITE", book_Des));
//            nameValuePairs.add(new BasicNameValuePair("ENCT", book_OriDate));
//            nameValuePairs.add(new BasicNameValuePair("LANGUAGE", book_RetDate));
//            nameValuePairs.add(new BasicNameValuePair("ENC", book_lan));
//            httppost.setHeader("Referer","www.skyteam.com");
            try {


//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//                HttpResponse response = httpclient.execute(httppost);
//                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
//                StringBuilder builder = new StringBuilder();
//                for (String line = null; (line = reader.readLine()) != null;) {
//                    builder.append(line).append("\n");
//                }
//                String htmlPage = builder.toString();
//                webViewurl.setWebViewClient(new WebViewClient());
//               WebSettings webSettingsurl = webViewurl.getSettings();
//                webSettingsurl.setJavaScriptEnabled(true);
//                webSettingsurl.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
//                webSettingsurl.setUseWideViewPort(true);
//                webSettingsurl.setSaveFormData(false);
//

                // Enable DOM storage
//                webSettingsurl.setDomStorageEnabled(true);
//                webSettingsurl.setAllowFileAccess(true);
//                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
//                    webSettingsurl.setAllowFileAccessFromFileURLs(true);
//                    webSettingsurl.setAllowUniversalAccessFromFileURLs(true);
//                }

                WebSettings webSettingsurl = webViewurl.getSettings();
                webSettingsurl.setDomStorageEnabled(true);
                webSettingsurl.setAllowFileAccess(true);
                webSettingsurl.setJavaScriptCanOpenWindowsAutomatically(true);
                webSettingsurl.setJavaScriptEnabled(true);
                webSettingsurl.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
                webSettingsurl.setUseWideViewPort(true);
                webSettingsurl.setSaveFormData(true);



                String url = bookingURL;
                //String postData = "EMBEDDED_TRANSACTION="+book_Ori+"&SITE="+book_Des+"&ENCT="+book_OriDate+"&LANGUAGE="+book_RetDate+"&ENC="+book_lan;
                // webViewurl.postUrl(url,postData.getBytes());
                // webViewurl.postUrl(url,EncodingUtils.getBytes(postData, "BASE64"));

                String postData = "EMBEDDED_TRANSACTION="+URLEncoder.encode(book_Ori, "UTF-8")+"&SITE="+URLEncoder.encode(book_Des, "UTF-8")+"&ENCT="+URLEncoder.encode(book_OriDate, "UTF-8")+"&LANGUAGE="+URLEncoder.encode(book_RetDate, "UTF-8")+"&ENC="+URLEncoder.encode(book_lan, "UTF-8");
                //String postData1 = "username=" + URLEncoder.encode(my_username, "UTF-8") + "&password=" + URLEncoder.encode(my_password, "UTF-8");
                setWebViewClientForShowingProgressBar(webViewurl);
                webViewurl.postUrl(url,postData.getBytes());

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }


        }else{


            webViewurl = (SkyteamWebview) findViewById(R.id.wvurl_skyteam);
            webViewurl.clearHistory();
            webViewurl.clearCache(true);
            //webViewurl.getSettings().setJavaScriptEnabled(true);
            webViewurl.getSettings().setLoadWithOverviewMode(true);
            webViewurl.getSettings().setUseWideViewPort(true);
            //webViewurl.loadUrl("about:blank");
            webViewurl.bringToFront();
            webViewurl.invalidate();
            skylogopanel.setVisibility(View.VISIBLE);
            skylogopanel.bringToFront();
            closebutton.setVisibility(View.VISIBLE);
            closebutton.bringToFront();
            backbutton.setVisibility(View.VISIBLE);
            backbutton.bringToFront();
            progressBar.setVisibility(View.VISIBLE);
            /*progressBar.bringToFront();
            backwardButton.setVisibility(View.VISIBLE);
            backwardButton.bringToFront();
            forwardButton.setVisibility(View.VISIBLE);
            forwardButton.bringToFront()*/;
            webViewurl.getSettings().setJavaScriptEnabled(true);
            setWebViewClientForShowingProgressBar(webViewurl);
            webViewurl.loadUrl(bookingURL);

        }
    }

    private void setWebViewClientForShowingProgressBar(SkyteamWebview webViewurl) {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.bringToFront();
        backwardButton.setVisibility(View.VISIBLE);
        backwardButton.bringToFront();
        forwardButton.setVisibility(View.VISIBLE);
        forwardButton.bringToFront();

        webViewurl.setWebViewClient(new WebViewClient() {


            @Override
            public void onPageStarted(
                    WebView view, String url, Bitmap favicon) {
                //updateButtons(view);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);
                ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 90);
                animation.setDuration(8000);
                animation.setInterpolator(new DecelerateInterpolator());
                animation.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.bringToFront();
                        progressBar.setProgress(0);
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        //do something when the countdown is complete
                        // progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) { }

                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
                animation.start();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
               // backwardButton.setImageResource(R.drawable.left_offstate);
                updateButtons(view);
                progressBar.setVisibility(View.GONE);

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

       /* if (AppStatusApplication.wasInBackground) {
            CustomerSurveyData.setsShared_PrefCheck(false);
            CustomerSurveyData.setsThreeStepsFlag(false);
            CustomerSurveyData.setsShowPopupFlag(false);
            CustomerSurveyData.setTimerStarts(false);
            CustomerSurveyData.setCsUrlCall(0);
            CustomerSurveyData.sScreensVisited=0;
            //startTimer();
            AppStatusApplication.wasInBackground = false;
        }
        //cs_calculateNoOfDays();
        try {
            mLang=Utils.getCurrentLanguage(this);
        } catch (JSONException e) {
            mLang = "En";
            e.printStackTrace();
        }
        if (customerSurveyData.issShared_PrefCheck() && ("En".equals(mLang) || "zh".equals(mLang) || "Es".equals(mLang) || "Fr".equals(mLang))) {
            customerSurveyData.setsLanguage(true);
//            Log.d("Timer Started","msg");
            //startTimer();
        }*/
    }

    public void updateButtons(WebView view)
    {
        if(view.canGoForward()){
            forwardButton.setImageResource(R.drawable.right_onstate);
        }
        else
        {
            forwardButton.setImageResource(R.drawable.right_offstate);
        }
        if(view.canGoBack()){
            backwardButton.setImageResource(R.drawable.left_onstate);
        }
        else{
            backwardButton.setImageResource(R.drawable.left_offstate);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Get source of open for app re-engagement
        Tune.getInstance().setReferralSources(this);

        // Attribution Analytics will not function unless the measureSession call is included
        Tune.getInstance().measureSession();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        //super.onNewIntent(intent);

        Boolean usingIntent=intent.getBooleanExtra(INTENT_EXTRA_ISHOMEPAGE,false);


        if(usingIntent){

            String HTMLPage=intent.getStringExtra(INTENT_EXTRA_HTMLPAGE);
            String currentPage = intent.getStringExtra(INTENT_EXTRA_CURRENTPAGE);
            if("homePage".equalsIgnoreCase(intent.getStringExtra(INTENT_EXTRA_HTMLPAGE))){

                webView.loadUrl("file:///android_asset/html/index.html#homePage");

            }else{

                JSONObject htmlJSON = null;
                try {
                    htmlJSON = new JSONObject();
                    htmlJSON.put("HTMLPageKey",HTMLPage);
                    htmlJSON.put("currentPage",currentPage);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (htmlJSON != null) {

                    webView.loadUrl("javascript:iSU.callHTMLPage("+htmlJSON+")");


                }
            }

        }else {
            JSONObject airportJSON = null;
            try {
                airportJSON = new JSONObject(intent.getStringExtra("AirportObj"));
                selectedAirportName = airportJSON.getString("airportName");
                selectedAirportCode = airportJSON.getString("airportCode");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (airportJSON != null) {

                webView.loadUrl("javascript:iSU.airportDetails(" +airportJSON+ ")");
            }
        }
        //this.overridePendingTransition(R.anim.slide_in_right,);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    /*
     * ss This method is used to check and if required to download the updated
     * data for cityairport, lounges and skytips
     */
    private void startDataDownlaod() {

        if (NetworkUtil.getConnectivityStatus(this) > 0) {
			/*
			 * if (needToUpdateSkytips()) { SKYTIPSTHREAD_INPROGRESS = true; }
			 */

			/*
			 * if (needToUpdateAirportTest()) { updateAirportTest(); } else
			 */

            new Thread(new Runnable() {

                @Override
                public void run() {
                    if (needToUpdateAirportTest()) {
                        getCityAirportPair();
                    }
                }
            }).start();

            if (registeredWifiStateChange) {

				/*
				 * if (needToUpdateSkytips()) { SKYTIPSTHREAD_INPROGRESS = true;
				 * }
				 */

                if (needToUpdateLounges()) {
                    updateAllLounges();
                } else if (needToUpdateSkytips()) {
                    SKYTIPSTHREAD_INPROGRESS = true;
                    updateSkytips();
                }
            }
        } else if (!registeredWifiStateChange) {

            // Add broadcast for wifi state change

            br_wifiState = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    if (NetworkUtil
                            .getConnectivityStatus(SkyteamWebviewActivity.this) > 0) {
                        Utils.logMessage(TAG,
                                "wifi state change  .. starting data download",
                                Log.DEBUG);

                        startDataDownlaod();

						/*
						 * if (needToUpdateSkytips()) { SKYTIPSTHREAD_INPROGRESS
						 * = true; }
						 *
						 * if (needToUpdateLounges()) { updateAllLounges(); }
						 * else if (needToUpdateSkytips()) { updateSkytips(); }
						 */
                    }
                }

            };

            IntentFilter intent_wifi = new IntentFilter();
            intent_wifi.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            intent_wifi.addAction("android.net.wifi.STATE_CHANGE");
            registerReceiver(br_wifiState, intent_wifi);
            registeredWifiStateChange = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /**** Delete SharedPreference Value ******/
        Utils.logMessage(TAG, "Activity onDestroy", Log.DEBUG);
        SharedPreferences preferences = getSharedPreferences("Mypref", 0);
        preferences.edit().remove("shared_pref_key").commit();
        unregisterReceiver(br_dateChange);
        if (registeredWifiStateChange) {
            unregisterReceiver(br_wifiState);
        }
        dbhelperObj.close();
    }

    @Override
    public void onBackPressed() {
        //Utils.logMessage(TAG, "Activity onBackPressed", Log.DEBUG);
        //super.onBackPressed();
        //timer.cancel();
    }

    /*
     * Method to initiaze the goggle analytics settings
     */
    @SuppressWarnings("deprecation")
    private void initializeGa() {
        mGa = GoogleAnalytics.getInstance(this);
        mTracker = mGa.getTracker(GA_PROPERTY_ID);
        GAServiceManager.getInstance().setLocalDispatchPeriod(
                GA_DISPATCH_PERIOD);
        mGa.setDryRun(GA_IS_DRY_RUN);
        mGa.getLogger().setLogLevel(GA_LOG_VERBOSITY);
        mGa.setAppOptOut(false);   // google analytics is Turned off if set to true
    }

    /*
     * This method is used for loading the index.html page in webview, and
     * initialize the settings for it.
     */
    @SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
    public void LoadURLInWebView() {

        if (!doneLoadingStartupURL) {
            doneLoadingStartupURL = true;

            webView = (SkyteamWebview) findViewById(R.id.wv_skyteam);

            webView.setVerticalScrollBarEnabled(false);
            webView.setHorizontalScrollBarEnabled(false);

            webView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                        case MotionEvent.ACTION_UP:
                            if (!v.hasFocus()) {
                                v.requestFocus();
                            }
                            break;
                    }
                    return false;
                }
            });

            webView.setOnLongClickListener(new OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {

                    Utils.logMessage(TAG, "long click", Log.DEBUG);
                    return true;
                }
            });

            webView.setLongClickable(false);
            webView.addJavascriptInterface(new MyJavascriptInterface(this), "MyJSClient");
            webView.addJavascriptInterface(new JSIObject(this,
                    SkyteamWebviewActivity.this, webView), "callNative");
            initWebViewSettings();
            //Fix for webview zoom out when an input field is focussed - Kikat version
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT){
                webView.setInitialScale(1);
            }

            webView.setWebViewClient(webViewClient);

            webView.setWebChromeClient(new WebChromeClient() {

                @Override
                public boolean onJsAlert(WebView view, String url,
                                         String message, JsResult result) {
                    return super.onJsAlert(view, url, message, result);
                }

                @Override
                public void onGeolocationPermissionsShowPrompt(String origin,
                                                               Callback callback) {
                    super.onGeolocationPermissionsShowPrompt(origin, callback);
                    callback.invoke(origin, true, false);
                }

            });

            if (STARTUP_URL != null && STARTUP_URL.length() > 0) {

                webView.loadUrl(STARTUP_URL);
            } else {
                showToast("URL : " + STARTUP_URL + " not found.");
            }
        }
    }

    @SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
    private void initWebViewSettings() {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webSettings.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
        webSettings.setUseWideViewPort(true);

        webSettings.setSaveFormData(false);

        // Enable DOM storage
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowFileAccess(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }
    }

    public void showToast(String message) {
        Toast toast = Toast.makeText(getBaseContext(), message,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();

    }

    public void showAlert(String message) {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(this);

        myDialog.setTitle("Alert").setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();

    }

    public void webViewLoadURL(String url) {

        webView.loadUrl((url));
    }

    /*
     * Check for the last update date for skytips data download
     */
    public boolean needToUpdateSkytips() {
        Calendar cal = Calendar.getInstance();
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        String year = String.valueOf(cal.get(Calendar.YEAR));

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);

        try {
            Date today = sdf.parse(day + "/" + month + "/" + year);

            Date compare = new Date(settings.getLong(PREF_SKYTIPS_UPDATE, 0));

            if (today.compareTo(compare) > 0) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    /*
     * Check for the last update date for lounges data download
     */
    public boolean needToUpdateLounges() {
        Calendar cal = Calendar.getInstance();
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        String year = String.valueOf(cal.get(Calendar.YEAR));

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);

        try {
            Date today = sdf.parse(day + "/" + month + "/" + year);

            Date compare = new Date(settings.getLong("LoungesUpdationDate", 0));

            if (today.compareTo(compare) > 0) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    /*
     * Check for the last update date for cityairport data download
     */
    public boolean needToUpdateAirportTest() {
        Calendar cal = Calendar.getInstance();
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        String year = String.valueOf(cal.get(Calendar.YEAR));

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);

        try {
            Date today = sdf.parse(day + "/" + month + "/" + year);

            Date compare = new Date(settings.getLong("AirportTestUpdationDate",
                    0));

            if (today.compareTo(compare) > 0) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            webView.loadUrl("javascript:androidHardwareBack()");
        }

        // Utils.logMessage("KEY DOWN",
        // "Calling hardwareback function "+"  Keycode: "+keyCode+"   Keyevent: "+event,
        // Log.DEBUG);
        return super.onKeyDown(keyCode, event);
    }

    /*
     * update the sharedprefs for last update time
     */
    public void updatePrefs(String str) {

        Calendar cal = Calendar.getInstance();
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        String year = String.valueOf(cal.get(Calendar.YEAR));

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy", Locale.US);

        try {
            Date today = sdf.parse(day + "/" + month + "/" + year);
            SharedPreferences.Editor editor = settings.edit();
            editor.putLong(str, today.getTime());
            editor.commit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void updateSkytips() {

        new DefaultSkyTipsAsync().execute();
    }

    public void updateAllLounges() {

        new GetAllLoungeJsonAsync().execute();

    }

    public void updateAirportTest() {
        new getCityAirportPairAsync().execute();
    }

    public void startBrowser(final String URL) {
        Runnable r = new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(URL));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        runOnUiThread(r);

    }

	/*
	 * WebViewClient for webview
	 */

    WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {

            Utils.logMessage(TAG, "onRecievedError" + description
                    + "on loading URL:" + failingUrl, Log.ERROR);
            if (failingUrl.contains("index.html#homePage")) {
                failingUrl = failingUrl.replace("index.html#homePage",
                        STARTUP_URL);
                view.loadUrl((failingUrl));
            }
            super.onReceivedError(view, errorCode, description, failingUrl);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Utils.logMessage(TAG, "onPageFinished: " + url, Log.DEBUG);
            if (url.equalsIgnoreCase(STARTUP_URL)) {
				/*
				 * Index.html page loading is complete. Now exit the splash and
				 * show the webview
				 */
                if (webView.getVisibility() != View.VISIBLE
                        || iv_splash.getVisibility() != View.GONE) {
                    webView.setVisibility(View.VISIBLE);

                    exitRunnable = new Runnable() {
                        public void run() {

                            iv_splash.setVisibility(View.GONE);
                            //pb_splash.setVisibility(View.GONE);
                            mSpinnerImage.setVisibility(View.GONE);
                            if(mFrameAnimation != null){
                                mFrameAnimation.stop();
                            }
                        }
                    };

                    exitHandler = new Handler();
                    exitHandler.postDelayed(exitRunnable, SPLASHTIME);
                }
            }


        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            Utils.logMessage(TAG, "onPageStarted: " + url, Log.DEBUG);
            super.onPageStarted(view, url, favicon);
        }



        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            Utils.logMessage(TAG, "shouldOverrideUrlLoading: " + url, Log.DEBUG);

			/*
			 * analytics://_trackPageview#homepage (for tracing screens)
			 * analytics://_trackEvent*category#action#label#labelValue (for
			 * tracing events in a screen)
			 */

            if (url.startsWith(STARTUP_URL)) {

                homeScreenUP = true;

            }

            else {
                homeScreenUP = false;
            }

            if (url.startsWith(GA_PAGEVIEW_PREFIX)) {

                String screenUrl = url.substring(url.indexOf("#") + 1);
                //  customerSurveyData.setsScreensVisited(StringConstants.ONE);
                try{
                    //new Utils().popupConditionsMet(Utils.getCurrentLanguage(SkyteamWebviewActivity.this),screenUrl,SkyteamWebviewActivity.this);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                if (screenUrl != null) {
                    mTracker.set(Fields.SCREEN_NAME, screenUrl);
                    mTracker.send(MapBuilder.createAppView().build());
                    Utils.logMessage(TAG, "send ga for : " + screenUrl,
                            Log.DEBUG);
                }
                /*new SkyteamWebviewActivity()
                        .showPopup(SkyteamWebviewActivity.this);*/
//                Log.d("Alert", "alert check before");
               /* if (CustomerSurveyData.issShowPopupFlag()){
//                    Log.d("Alert", "alert check inside");
                    *//*new SkyteamWebviewActivity()
                            .showPopup(SkyteamWebviewActivity.this);*//*
                }*/
                return true;
            }

            else if (url.startsWith(GA_EVENT_PREFIX)) {
                String[] tempArr = url.split("\\#");

                if (tempArr != null && tempArr.length >= 4) {
                    mTracker.send(MapBuilder.createEvent(tempArr[1],
                            tempArr[2], tempArr[3], null).build());
                    Utils.logMessage(TAG, "send ga for event: " + tempArr[1]
                            + " " + tempArr[2] + " " + tempArr[3], Log.DEBUG);
                }
                return true;
            }

            if (url.startsWith(StringConstants.YOGA_VIDEO)) {

                Intent i = new Intent(SkyteamWebviewActivity.this,
                        SkyYogaActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);


                if(url.contains("#")) {

                    String isFromSkyTeam = url.substring(url.indexOf("#") + 1);

                    if (isFromSkyTeam.equalsIgnoreCase("YES")) {
                        i.putExtra(StringConstants.BUNDLE_EXTRA_SKYTEAMYOGADIRECTION, StringConstants.YOGA_LTR);
                    }
                }

                startActivity(i);

            }
            else if (url.contains(StringConstants.AIRPORT_MAP_AMS) || url.contains(StringConstants.AIRPORT_MAP_IST) || url.contains(StringConstants.AIRPORT_MAP_LHR) || url.contains(StringConstants.AIRPORT_MAP_SYD) || url.contains(StringConstants.AIRPORT_MAP_FCO) || url.contains(StringConstants.AIRPORT_MAP_TPE) || url.contains(StringConstants.AIRPORT_MAP_GVA)) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_settings = DBHelper.getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp=dbHelper_settings.getSettingFileObjForHTML();

                        try {
                            temp.put("latitude",latitude);
                            temp.put("longitude",longitude);
                        } catch (Exception e) {
                            Utils.logMessage("SETTINGS", "Checking the initialLoad value for Making it as NO,If it's YES  : ", Log.DEBUG);
                        }

                        webView.post(new Runnable() {

                            @Override
                            public void run() {
                                //webView.loadUrl("javascript:iSU.getSettings("+ temp + ")");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if(url.contains(StringConstants.AIRPORT_MAP_AMS)){
                                        selectedAirportName="Schiphol Airport";
                                        selectedAirportCode="AMS";
                                        indexValuesMaps = 0;
                                        }
                                        else if(url.contains(StringConstants.AIRPORT_MAP_IST)) {
                                            selectedAirportName="Istanbul Airport";
                                            selectedAirportCode="IST";
                                            indexValuesMaps = 1;
                                        }
                                        else if(url.contains(StringConstants.AIRPORT_MAP_LHR)) {
                                            selectedAirportName="London Heathrow";
                                            selectedAirportCode="LHR";
                                            indexValuesMaps = 2;
                                        }
                                        else if(url.contains(StringConstants.AIRPORT_MAP_SYD)) {
                                            selectedAirportName="Sydney Airport";
                                            selectedAirportCode="SYD";
                                            indexValuesMaps = 3;
                                        }
                                        else if(url.contains(StringConstants.AIRPORT_MAP_FCO)) {
                                            selectedAirportName="Rome Airport";
                                            selectedAirportCode="FCO";
                                            indexValuesMaps = 4;
                                        }
                                        else if(url.contains(StringConstants.AIRPORT_MAP_GVA)) {
                                            selectedAirportName="Geneva Airport";
                                            selectedAirportCode="GVA";
                                            indexValuesMaps = 5;
                                        }
                                        else{
                                            selectedAirportName="Taiwan";
                                            selectedAirportCode="TPE";
                                            indexValuesMaps = 6;
                                        }

                                        Intent indoorMapsIntent = new Intent(SkyteamWebviewActivity.this, MapsActivity.class);
                                        indoorMapsIntent.putExtra("airportName", selectedAirportName);
                                        indoorMapsIntent.putExtra("airportCode", selectedAirportCode);
                                        indoorMapsIntent.putExtra("airportMapIndex",indexValuesMaps);
                                        startActivity(indoorMapsIntent);
                                    }
                                });
                            }
                        });

                    }
                }).start();
                return true;
            }
            else if (url.equals(StringConstants.NATIVE_MAP_OLD)) {

                Intent i = new Intent(SkyteamWebviewActivity.this,
                        SkyTeamMapActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                overridePendingTransition(R.anim.slide_in_left,0);
                startActivity(i);

            } else if (url.startsWith(StringConstants.NATIVE_SCAN)) {

                Intent i = new Intent(SkyteamWebviewActivity.this,
                        SkyTeamScanActivity.class);

                i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivityForResult(i, StringConstants.REQUEST_CODE_SCAN);

            }else if(url.startsWith(StringConstants.CLOSE_SCAN)){
                scanCompleted();
            }
            else if (url.startsWith(StringConstants.NATIVE_MAP)){
                String isFromHomeScreen=null;
                Intent i = new Intent(SkyteamWebviewActivity.this,
                        SkyTeamMapActivity.class);
                if(url.contains("#")){

                    isFromHomeScreen = url.substring(url.indexOf("#") + 1);


                    if(isFromHomeScreen.equalsIgnoreCase("YES")){
                        i.putExtra(SkyTeamMapActivity.SHOULD_SHOW_BACK, true);
                    }else{
                        i.putExtra(SkyTeamMapActivity.SHOULD_SHOW_BACK, false);
                    }

                }else{
                    i.putExtra(SkyTeamMapActivity.SHOULD_SHOW_BACK, false);
                }


                startActivity(i);
            }
            else if (url.equals(StringConstants.SEARCH_FLIGHTS_URL)) {
                webViewLoadURL("javascript:iSU.searchString()");
            } else if (url.equals(StringConstants.SAVED_AIRPORTS_URL)) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_airports = DBHelper
                                .getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp = dbHelper_airports
                                .getSavedAirportsNFlights();
                        webView.post(new Runnable() {

                            @Override
                            public void run() {

                                webView.loadUrl("javascript:iSU.processData("
                                        + temp + ")");
                            }
                        });

                    }
                }).start();
            }
            else if(url.startsWith(GA_EVENTLANG_PREFIX)){
                String[] tempArr = url.split("\\#");

                if (tempArr != null) {
                    String lan = tempArr[1];


                    if(lan.equalsIgnoreCase("zh"))
                    {
                        lan= "CHINESE";
                    }
                    else if(lan.equalsIgnoreCase("Es"))
                    {
                        lan= "SPANISH";
                    }
                    else if(lan.equalsIgnoreCase("Ja"))
                    { lan= "JAPANESE";
                    }
                    else if(lan.equalsIgnoreCase("Ru"))
                    { lan= "RUSSIAN";
                    }
                    else if(lan.equalsIgnoreCase("Ko"))
                    { lan= "KOREAN";
                    }
                    else if(lan.equalsIgnoreCase("It"))
                    { lan= "ITALIAN";
                    }
                    else if(lan.equalsIgnoreCase("Vi"))
                    { lan= "VIETNAMESE";
                    }
                    else if(lan.equalsIgnoreCase("Fr"))
                    { lan= "FRENCH";
                    }
                    else if(lan.equalsIgnoreCase("De"))
                    { lan= "GERMAN";
                    }
                    else if(lan.equalsIgnoreCase("Nl"))
                    { lan= "DUTCH";
                    }
                    else if(lan.equalsIgnoreCase("zh-Hant"))
                    { lan= "TRADITIONAL CHINESE";
                    }
                    else
                    {
                        lan= "ENGLISH";
                    }

                    mTracker.send(MapBuilder.createEvent("PREFERENCE", lan, "Language", null).build());
                }
                return true;
            }
            else if (url.equals(StringConstants.TEST_URL)) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_test = DBHelper.getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp = dbHelper_test.getCityAirport();
                        webView.post(new Runnable() {

                            @Override
                            public void run() {

                                webView.loadUrl("javascript:iSU.processData("
                                        + temp + ")");
                                Utils.logMessage(
                                        TAG,
                                        "done sendign data to the process data for test url",
                                        Log.DEBUG);
                            }
                        });

                    }
                }).start();

            }else if (url.equals(StringConstants.LANG_URL)) {


                asyncTask=new CityAirportPairLangAsync();
                asyncTask.execute();

            }else if (url.equals(StringConstants.CANCEL_REQUEST)) {

                langAsyntaskCancel();


            }else if (url.equals(StringConstants.SETTINGS_URL)) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_settings = DBHelper
                                .getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp=dbHelper_settings
                                .getSettingFileObjForHTML();

                        try
                        {
                            temp.put("latitude",latitude);
                            temp.put("longitude",longitude);
                        }
                        catch(JSONException e){
                            Utils.logMessage("SETTINGS", "Checking the initialLoad value for Making it as NO,If it's YES  : ", Log.DEBUG);
                        }

                        webView.post(new Runnable() {

                            @Override
                            public void run() {

                                webView.loadUrl("javascript:iSU.getSettings("
                                        + temp + ")");
                            }
                        });

                    }
                }).start();
            }else if (url.equals(StringConstants.DELETE_FLIGHTS_URL)) {
                webViewLoadURL("javascript:iSU.deleteFlight()");

            } else if (url.equals(StringConstants.DELETE_AIRPORTS_URL)) {
                webViewLoadURL("javascript:iSU.deleteAirports()");

            } else if (url.equals(StringConstants.LOUNGE_FINDER_URL)) {
                webViewLoadURL("javascript:iSU.loungeString()");

            }

            else if (url.equals(StringConstants.SAVED_FLIGHT_URL)) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_flights = DBHelper
                                .getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp = dbHelper_flights
                                .getSavedFlightsHomeScreen();

                        webView.post(new Runnable() {

                            @Override
                            public void run() {
                                webView.loadUrl("javascript:iSU.processData("
                                        + temp + ")");
                                Utils.logMessage(
                                        TAG,
                                        "sent data for homescreen saved flight",
                                        Log.DEBUG);

                                if (NetworkUtil
                                        .getConnectivityStatus(SkyteamWebviewActivity.this) > 0) {

									/*
									 * if (needToUpdateSkytips()) {
									 * SKYTIPSTHREAD_INPROGRESS = true; }
									 */

                                    if (needToUpdateLounges()) {
                                        updateAllLounges();
                                    } else if (needToUpdateSkytips()) {
                                        SKYTIPSTHREAD_INPROGRESS = true;
                                        updateSkytips();
                                    }
                                }
                            }
                        });
                    }
                }).start();

            } else if (url.equals(StringConstants.MEMBER_URL)) {
                webViewLoadURL("javascript:iSU.memberdetails()");
            } else if (url.equals(StringConstants.SKYTIPS_URL)) {
                if (SKYTIPSTHREAD_INPROGRESS) {
                    sendSkytipsList = true;
                } else {
                    webViewLoadURL("javascript:iSU.skyTips()");
                }
            } else if (url.equals(StringConstants.SKYTIPS_HUBS_URL)) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_skytips = DBHelper
                                .getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp = dbHelper_skytips
                                .getSkyTipsHubs();

                        webView.post(new Runnable() {

                            @Override
                            public void run() {
                                webView.loadUrl(("javascript:iSU.processData("
                                        + temp + ")"));
                            }
                        });
                    }
                }).start();
            } else if (url.equals(StringConstants.LOUNGES_LIST)) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_skytips = DBHelper
                                .getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp = dbHelper_skytips
                                .getStoredLounges();

                        webView.post(new Runnable() {

                            @Override
                            public void run() {
                                webView.loadUrl(("javascript:iSU.processData("
                                        + temp + ")"));
                            }
                        });
                    }
                }).start();
            } else if (url.equals(StringConstants.SAVE_AIRPORTS_URL)) {
                webViewLoadURL("javascript:iSU.insertSavedAirport()");
            } else if (url.equals(StringConstants.SAVE_FLIGHTS_URL)) {
                webViewLoadURL("javascript:iSU.insertSavedFlight()");
            } else if (url.equals(StringConstants.FLIGHT_STATUS)) {
                webViewLoadURL("javascript:iSU.flightstatusURL()");
            } else if (url.startsWith(StringConstants.SETTINGS_PREFIX)) {
                webViewLoadURL("javascript:iSU.settings()");
            } else if (url.startsWith(StringConstants.NETWORK_URL)) {
                JSONObject networkJson = new JSONObject();
                try {
                    networkJson.put("url", StringConstants.NETWORK_URL);
                    if (NetworkUtil
                            .getConnectivityStatus(SkyteamWebviewActivity.this) > 0) {
                        networkJson.put("network", "online");
                    } else {
                        networkJson.put("network", "offline");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Utils.logMessage(TAG,
                        "sending network status : " + networkJson, Log.DEBUG);
                webViewLoadURL("javascript:iSU.processData(" + networkJson
                        + ")");
            } else if (url.startsWith(StringConstants.SAVEDFLIGHT_STATUS_URL)) {
                webViewLoadURL("javascript:iSU.savedFlightStatusURL()");
            }
            else if (url.equals(StringConstants.SKYTIPS_LIST_URL)) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DBHelper dbHelper_skytipslist = DBHelper
                                .getInstance(SkyteamWebviewActivity.this);
                        final JSONObject temp = dbHelper_skytipslist
                                .getSkyTipsCityList();

                        webView.post(new Runnable() {

                            @Override
                            public void run() {
                                webView.loadUrl(("javascript:iSU.processData("
                                        + temp + ")"));
                            }
                        });
                    }
                }).start();
            }
            else if (url.equals(StringConstants.SKYTEAMWEBSITE)) {
                webViewLoadURL("javascript:iSU.openSkyTeamWebSite()");
                return true;
            }

            if (url.startsWith(StringConstants.APP_PREFIX)
                    || url.startsWith(StringConstants.SETTINGS_PREFIX)) {
                return true;
            } else if (url.startsWith(StringConstants.URL_PREFIX)) {
                webViewLoadURL("javascript:iSU.openmemberdetails()");
                return true;
            } else if (url.contains("http://www.google.com/")
                    || url.contains("http://maps.google.com/")) {

                // start the browser
                startBrowser(url);
                return true;
            }

            return false;
        }

    };

	/*
	 * Asynctask for cityairport dump download
	 */

    public class CityAirportPairLangAsync extends AsyncTask<Void, Void, Boolean> {

        JSONObject master = new JSONObject();

        @Override
        protected Boolean doInBackground(Void... params) {

            String str = null;

            DBHelper dbhelper = DBHelper
                    .getInstance(SkyteamWebviewActivity.this);
            Utils.logMessage(TAG, "Getting Language Url:::"+dbhelper.getCityAirportURL(), Log.DEBUG);
            String[] strArr = { dbhelper.getCityAirportURL(),
                    "CityAirportPair" };

            WebServiceInteraction webServiceInteractionLang = new WebServiceInteraction(
                    SkyteamWebviewActivity.this);
            String lang=null;
            try {
                lang = dbhelper.getSettingFileObj().getString("Language");

            } catch (JSONException e2) {
                // TODO Auto-generated catch block

                e2.printStackTrace();
            }
            if(!isCancelled()){

                Log.d("Test","1");

                str = webServiceInteractionLang.getCityAirportJSON(strArr,lang);


                if (str == null || (str != null && str.contains("ERROR")) || (str != null && str.contains("Error"))) {


                    try {
                        master.put("Result", "Error");
                        master.put("AirPorts_lang", "");
                        master.put("url", StringConstants.LANG_URL);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    return true;
                } else {

                    Utils.logMessage(TAG, "city airport insertion ", Log.DEBUG);

                    try {
                        master.put("Result", "Success");
                        //master.put("AirPorts_lang", new JSONArray(str));
                        JSONObject obj = new JSONObject(str);
                        JSONArray jsonArray = obj.getJSONArray("locations");

                        master.put("AirPorts_lang", jsonArray);
                        master.put("url", StringConstants.LANG_URL);
                        SKYTIPSTHREAD_INPROGRESS = true;
                        dbhelper.clearSkytips();
                        updateSkytips();

                    } catch (JSONException e) {
                        try {
                            master.put("Result", "Error");
                            master.put("AirPorts_Lang", "");
                            master.put("url", StringConstants.LANG_URL);
                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }

                        e.printStackTrace();
                    }
                    if(!isCancelled()) {
                        dbhelper.insertIntoCityAirport(str);
                        Utils.logMessage(TAG, "finished city airport insertion ", Log.DEBUG);
                    }else{
                        try {
                            master.put("Result", "CANCEL");
                            master.put("AirPorts_Lang", "");
                            master.put("url", StringConstants.LANG_URL);
                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                    dbhelper.deleteOtherLangCache();
                    return true;
                }


            }else{

                webServiceInteractionLang.cancelConnection();
                try {
                    master.put("Result", "CANCEL");
                    master.put("AirPorts_Lang", "");
                    master.put("url", StringConstants.LANG_URL);
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }

            return true;

        }

        @Override
        protected void onPostExecute(Boolean result) {


            if (result) {
                webView.loadUrl("javascript:iSU.processData("
                        + master + ")");
            }
            //		boolCancelTask=false;


        }
    }


	/*
	 * Asynctask for cityairport dump download
	 */

    public class getCityAirportPairAsync extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            Utils.logMessage(TAG, "started for cityairport list", Log.DEBUG);
            String str = null;
            String[] strArr = { StringConstants.cityAirportPairURL,
                    "CityAirportPair" };

            WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
                    SkyteamWebviewActivity.this);

            // JSONArray temp = webServiceInteraction.getJsonArray(strArr);

            // str = temp.toString();
            DBHelper dbhelper = DBHelper
                    .getInstance(SkyteamWebviewActivity.this);
            String lang=null;
            try {
                lang = dbhelper.getSettingFileObj().getString("Language");

            } catch (JSONException e2) {
                // TODO Auto-generated catch block

                e2.printStackTrace();
            }
            Log.d("Test","2");
            str = webServiceInteraction.getCityAirportJSON(strArr, lang);

            if (str == null || (str != null && str.contains("ERROR"))) {
                return false;
            } else {
                //	DBHelper dbhelper = DBHelper
                //			.getInstance(SkyteamWebviewActivity.this);
                Utils.logMessage(TAG, "city airport insertion ", Log.DEBUG);
                // dbhelper.clearCityAirport();
                dbhelper.insertIntoCityAirport(str);
                return true;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            Utils.logMessage(TAG, "done city airport started lounges ",
                    Log.DEBUG);

            if (result) {
                updatePrefs("AirportTestUpdationDate");
            }

			/*
			 * if (needToUpdateLounges()) { updateAllLounges(); }else if
			 * (needToUpdateSkytips()) { updateSkytips(); }else {
			 * LoadURLInWebView(); }
			 */

        }
    }


	/*
	 * Asynctask for lounges dump download
	 */

    public class GetAllLoungeJsonAsync extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            Utils.logMessage(TAG, "started lounges ", Log.DEBUG);
            String str = null;

            String[] strArr = { StringConstants.loungeFinderURL, "LoungeData" };

            WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
                    SkyteamWebviewActivity.this);
            try {

                JSONObject temp = webServiceInteraction.getLoungeDataReqHeader(strArr,"Accept-Language","En");
                JSONObject responseToHybrid=new JSONObject();
                responseToHybrid.put("url", StringConstants.LOUNGE_FINDER_URL);
                if (temp == null
                        || (temp != null && temp.toString().contains("Errors"))) {
                /*if (temp == null || temp.toString().contains("Errors")) {*/

                    Log.d("ERROR","$$$$$$");
                    responseToHybrid.put("Count", 0);
                    responseToHybrid.put("ErrorMessage","No Lounges Found");
                    str = responseToHybrid.toString();

                }
                else {
                    JSONArray loungeDataArray=new JSONArray();
                    int loungeCount=0;
                    for(int i=0;i<temp.getJSONArray("airport").length();i++)
                    {
                        JSONObject tempObject=temp.getJSONArray("airport").getJSONObject(i);
                        for(int j=0;j<tempObject.getJSONArray("lounges").length();j++)
                        {
                            JSONObject loungeData = new JSONObject();
                            if(tempObject.has("airportCode"))
                            {
                                loungeData.put("AirportCode",tempObject.get("airportCode"));
                            }
                            if(tempObject.has("airportName"))
                            {
                                loungeData.put("AirportName",tempObject.get("airportName"));
                            }
                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("id"))
                            {
                                loungeData.put("LoungeID",tempObject.getJSONArray("lounges").getJSONObject(j).get("id"));
                            }
                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("name"))
                            {
                                loungeData.put("LoungeName",tempObject.getJSONArray("lounges").getJSONObject(j).get("name"));
                            }
                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("airlineOperation")) {
                                loungeData.put("OperatingAirline", tempObject.getJSONArray("lounges").getJSONObject(j)
                                        .getJSONObject("airlineOperation").getString("airlineCode"));
                            }

                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("location")) {
                                loungeData.put("LoungeLocation",tempObject.getJSONArray("lounges").getJSONObject(j).get("location"));
                            }
                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("operationHours")) {
                                loungeData.put("HoursOfOperation",tempObject.getJSONArray("lounges").getJSONObject(j).get("operationHours"));
                            }
                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("facilities")) {
                                for (int k = 0; k < tempObject.getJSONArray("lounges").getJSONObject(j).
                                        getJSONArray("facilities").length(); k++) {
                                    if (tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                            .getJSONObject(k).has("facilitiesName")) {
                                        switch (tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                .getJSONObject(k).getString("facilitiesId")) {
                                            case "FAC_7":
                                                loungeData.put("SmokingRoom", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_8":
                                                loungeData.put("KidsRoom", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_1":
                                                loungeData.put("Shower",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_9":
                                                loungeData.put("Meal", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_10":
                                                loungeData.put("ActionStation",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_11":
                                                loungeData.put("Appetizers", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_12":
                                                loungeData.put("Snacks", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_13":
                                                loungeData.put("Wine", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_14":
                                                loungeData.put("Spirits", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_15":
                                                loungeData.put("SoftDrink", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_2":
                                                loungeData.put("AccessTodisabled", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_16":
                                                loungeData.put("CustomerService", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_17":
                                                loungeData.put("BoardingAnnoncements", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_18":
                                                loungeData.put("FlightMonitor", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_19":
                                                loungeData.put("Tv", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_3":
                                                loungeData.put("FreeWiFi",tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_20":
                                                loungeData.put("MagazinesAndNewspapers", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_21":
                                                loungeData.put("FixedInternetAcces", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_4":
                                                loungeData.put("SilentArea", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_5":
                                                loungeData.put("BusinessCenter", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;
                                            case "FAC_6":
                                                loungeData.put("MeetingAndConferenceRoom", tempObject.getJSONArray("lounges").getJSONObject(j).getJSONArray("facilities")
                                                        .getJSONObject(k).getString("facilitiesName"));
                                                break;

                                        }
                                    }
                                }
                            }
                            if(tempObject.getJSONArray("lounges").getJSONObject(j).has("airlinesAccessEligible")) {

                                String airlineCode=null;
                                for (int l = 0; l < tempObject.getJSONArray("lounges").
                                        getJSONObject(j).getJSONArray("airlinesAccessEligible").length(); l++) {

                                    if(l==0)
                                    {
                                        airlineCode=tempObject.getJSONArray("lounges").getJSONObject(j).
                                                getJSONArray("airlinesAccessEligible").getJSONObject(l).getString("airlineCode");
                                    }
                                    else {
                                        airlineCode = airlineCode + "*" + tempObject.getJSONArray("lounges").getJSONObject(j).
                                                getJSONArray("airlinesAccessEligible").getJSONObject(l).getString("airlineCode");

                                    }
                                }
                                loungeData.put("AirlineCode",airlineCode);
                            }
                            if(loungeCount<temp.getInt("count")) {
                                loungeDataArray.put(loungeCount, loungeData);
                                loungeCount = loungeCount + 1;
                            }

                        }
                        if(tempObject.has("airportLoungesRecords"))
                        {
                            responseToHybrid.put("Count",tempObject.getInt("airportLoungesRecords"));
                        }
                        responseToHybrid.put("LoungeData",loungeDataArray);
                        str = responseToHybrid.toString();
                        Log.d("str",str);
                    }
                }

            }
            catch (JSONException e) {
                e.printStackTrace();
            }

            if (str == null || (str != null && str.contains("ERROR"))) {
                return false;
            } else {
                DBHelper dbhelper = DBHelper
                        .getInstance(SkyteamWebviewActivity.this);
                dbhelper.clearLounges();
                dbhelper.insertIntoLounges(str);
                return true;

            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Utils.logMessage(TAG, "done lounges started skytips ", Log.DEBUG);

            if (result) {
                updatePrefs("LoungesUpdationDate");
            }

            if (needToUpdateSkytips()) {
                SKYTIPSTHREAD_INPROGRESS = true;
                updateSkytips();
            }

        }

    }

	/*
	 * Asynctask for skytips dump download
	 */

    public class DefaultSkyTipsAsync extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            Utils.logMessage(TAG, "started skytips ", Log.DEBUG);
            String[] strArr = { StringConstants.skyTipsDefaultURL, "SkyTips" };
            DBHelper dbhelper = DBHelper
                    .getInstance(SkyteamWebviewActivity.this);
            String lang=null;
            try {
                lang=dbhelper.getSettingFileObj().getString("Language");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                lang="En";
                e.printStackTrace();
            }
            WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
                    SkyteamWebviewActivity.this);
            JSONObject temp = webServiceInteraction.getSkyTipsJson(strArr,lang);
            if (temp == null
                    || (temp != null && temp.toString().contains("ERROR"))) {

                return false;

            } else {

                dbhelper.clearSkytips();
                dbhelper.insertIntoSkytips(temp);
                return true;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            SKYTIPSTHREAD_INPROGRESS = false;

            if (sendSkytipsList) {
                webViewLoadURL("javascript:iSU.skyTips()");
                sendSkytipsList = false;
            }

            if (result) {
                updatePrefs(PREF_SKYTIPS_UPDATE);
            }

            Utils.logMessage(TAG, "done skytips ", Log.DEBUG);

        }

    }

    public Context getContext() {
        return getApplicationContext();
    }

	/*
	 * private class CustomeGestureDetector extends SimpleOnGestureListener {
	 *
	 * @Override public boolean onFling(MotionEvent e1, MotionEvent e2, float
	 * velocityX, float velocityY) { if(e1 == null || e2 == null) return false;
	 * if(e1.getPointerCount() > 1 || e2.getPointerCount() > 1) return false;
	 * else { try {
	 * System.out.println("Starting "+e1.getX()+"::"+e1.getY()+":"+e2
	 * .getX()+"::"+e2.getY()); if(e1.getX() - e2.getX() > 50) { //right to left
	 * System
	 * .out.println(""+e1.getX()+"::"+e1.getY()+":"+e2.getX()+"::"+e2.getY());
	 * if(e1.getY() - e2.getY() > 200 ) { return false; }
	 * System.out.println("swipe from right to left");
	 * System.out.println(""+e1.getX
	 * ()+"::"+e1.getY()+":"+e2.getX()+"::"+e2.getY());
	 * webViewLoadURL("javascript:iSU.swipeLeft()"); return true; } else if
	 * (e2.getX() - e1.getX() > 50) { //left to right if(e2.getY() - e1.getY() <
	 * -200 || e2.getY() - e1.getY() > 200) { return false; }
	 * System.out.println(
	 * ""+e1.getX()+"::"+e1.getY()+":"+e2.getX()+"::"+e2.getY());
	 * System.out.println("swipe from left to right");
	 * webViewLoadURL("javascript:iSU.swipeRight()"); return true; } } catch
	 * (Exception e) { e.printStackTrace(); } return false; } }
	 *
	 *
	 * }
	 */

    public void exitSplash() {
        LoadURLInWebView();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100 && resultCode == 50) {
            JSONObject airportJSON = null;
            try {
                airportJSON = new JSONObject(data.getStringExtra("AirportObj"));
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (airportJSON != null) {
                webView.loadUrl("javascript:iSU.airportDetails(" + airportJSON
                        + ")");
            }

        }
        if(requestCode== StringConstants.REQUEST_CODE_SCAN && resultCode == StringConstants.RESULT_CODE_SCAN){

            JSONObject barcode = null;


            try {
                barcode = new JSONObject();
                barcode.put("barcode",data.getStringExtra("barcode"));
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (barcode != null) {
                new SkyteamWebviewActivity()
                        .webViewLoadURL("javascript:iSU.getFlightDetailsFromScan(" + barcode
                                + ")");
                //webView.loadUrl("javascript:iSU.getFlightDetailsThroScan(" + barcode
                //  + ")");
            }
        }

    }

    public void getCityAirportPair() {

        Utils.logMessage(TAG, "started for cityairport list", Log.DEBUG);
        String str = null;
        DBHelper dbhelper = DBHelper
                .getInstance(SkyteamWebviewActivity.this);
        Utils.logMessage(TAG, "Getting Language Url"+dbhelper.getCityAirportURL(), Log.DEBUG);
        String[] strArr = { dbhelper.getCityAirportURL(),
                "CityAirportPair" };

        WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
                SkyteamWebviewActivity.this);

        String lang=null;
        try {
            lang = dbhelper.getSettingFileObj().getString("Language");

        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        Log.d("Test","3");
        str = webServiceInteraction.getCityAirportJSON(strArr,lang);

        if (str == null || (str != null && str.contains("ERROR")) ) {

        } else {

            Utils.logMessage(TAG, "city airport insertion ", Log.DEBUG);
            // dbhelper.clearCityAirport();
            dbhelper.insertIntoCityAirport(str);
            updatePrefs("AirportTestUpdationDate");
            Utils.logMessage(TAG, "finished city airport insertion ", Log.DEBUG);
            final JSONObject temp = dbhelper.getUpdatedCityAirport();
            loadURLinUIThread("javascript:iSU.processData("
                    + temp + ")");
            Utils.logMessage(
                    TAG,
                    "done sendign data to the process data for CITYLIST_UPDATE_URL",
                    Log.DEBUG);
        }

    }

    public JSONObject getCityAirportLangJSON() {

        Utils.logMessage(TAG, "started for getCityAirportLangJSON list", Log.DEBUG);
        String str = null;

        JSONObject master = new JSONObject();

        DBHelper dbhelper = DBHelper
                .getInstance(SkyteamWebviewActivity.this);
        Utils.logMessage(TAG, "Getting Language Url:::"+dbhelper.getCityAirportURL(), Log.DEBUG);
        String[] strArr = { dbhelper.getCityAirportURL(),
                "CityAirportPair" };

        WebServiceInteraction webServiceInteraction = new WebServiceInteraction(
                SkyteamWebviewActivity.this);

        String lang=null;
        try {
            lang = dbhelper.getSettingFileObj().getString("Language");

        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        Log.d("Test","4");
        str = webServiceInteraction.getCityAirportJSON(strArr,lang);

        if (str == null || (str != null && str.contains("ERROR"))) {

            try {
                master.put("Result", "Error");
                master.put("AirPorts_lang", "");
                master.put("url", StringConstants.LANG_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        } else {

            Utils.logMessage(TAG, "city airport insertion ", Log.DEBUG);
            // dbhelper.clearCityAirport();
            try {
                master.put("Result", "Success");
//				master.put("AirPorts_lang", new JSONArray(str));
                JSONObject obj = new JSONObject(str);
                JSONArray jsonArray = obj.getJSONArray("locations");

                master.put("AirPorts_lang", jsonArray);
                master.put("url", StringConstants.LANG_URL);
            } catch (JSONException e) {
                try {
                    master.put("Result", "Error");
                    master.put("AirPorts_Lang", "");
                    master.put("url", StringConstants.LANG_URL);
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                e.printStackTrace();
            }

            dbhelper.insertIntoCityAirport(str);
            updatePrefs("AirportTestUpdationDate");
            Utils.logMessage(TAG, "finished city airport insertion ", Log.DEBUG);
            dbhelper.deleteOtherLangCache();

        }
        return master;
    }

    public void loadURLinUIThread(final String url) {
        Runnable r = new Runnable() {

            @Override
            public void run() {

                webView.loadUrl(url);
            }
        };

        runOnUiThread(r);
    }

    public static SkyteamWebview getWebView() {
        return webView;
    }

    public void cancel(){
        if(asyncTask!=null){
            boolean bool = asyncTask.cancel(true);

        }
    }


    public void langAsyntaskCancel(){
        if(asyncTask!=null){
            boolean bool = asyncTask.cancel(true);

            if(bool){
                try {
                    JSONObject master = new JSONObject();
                    master.put("Result", "CANCEL");
                    master.put("AirPorts_Lang", "");
                    master.put("url", StringConstants.LANG_URL);
                    webView.loadUrl("javascript:iSU.processData("
                            + master + ")");
                }catch(JSONException je){
                    je.printStackTrace();
                }
            }

        }
    }
    @Override
    public void scanCompleted(){

        finishActivity(StringConstants.REQUEST_CODE_SCAN);
        //this.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }

    public Boolean checkLanguageExists(){

        JSONObject JSONobj=new JSONObject();

        JSONobj=getSettingsObj();

        if(JSONobj.has("Language"))
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public JSONObject getSettingsObj()
    {
        JSONArray master = new JSONArray();
        JSONObject JSONobj=new JSONObject();
        try{
            String  dirDir = this.getFilesDir() + "/";

            InputStream is = new FileInputStream(dirDir+ DBHelper.SettingsJSONFileName);

            Scanner s = new Scanner(is).useDelimiter("\\A");

            String str = s.hasNext() ? s.next() : "";

            if (str != null && str.length() > 0) {
                master = new JSONArray(str);
            } else {
                master = new JSONArray("ERROR");
            }
            JSONobj=master.getJSONObject(0);


            return JSONobj;

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return JSONobj;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return JSONobj;

        }
    }
    public String getInitialLoadString()
    {
        JSONObject JSONobj=new JSONObject();
        JSONobj=getSettingsObj();
        String result="";
        try {
            result=JSONobj.getString("Time")+","+JSONobj.getString("Distance")+","+JSONobj.getString("Temperature")+","+JSONobj.getString("Date")+","+JSONobj.getString("SkyTips")+","+JSONobj.getString("Language")+",YES";
            return result;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return result;
        }
    }

    @Override   // GoogleApiClient changes
    public void onConnected(Bundle arg0) {

        android.location.Location mlastLocation = LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
        String url = null;

        if (mlastLocation != null) {

            latitude =  mlastLocation.getLatitude();
            longitude =  mlastLocation.getLongitude();
        }
        mLocationClient.disconnect();

    }
    @Override // GoogleApiClient changes

    /*public void onDisconnected() {

    }*/
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {

        Toast.makeText(SkyteamWebviewActivity.this,
                "Error in Google Maps.Please try again later",
                Toast.LENGTH_LONG).show();

    }

    public void getLocation()
    {
        if (NetworkUtil.getConnectivityStatus(SkyteamWebviewActivity.this) > 0) {
            if (mLocationClient == null) {

                // mLocationClient = new LocationClient(this, this, this);  GoogleApiclient changes
                mLocationClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
                mLocationClient.connect();
            } else {
                mLocationClient.connect();
            }
        } else {

            String alert = getString(R.string.sky_internet_alert_error);

            InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert,
                    1, 0, 0);
            dialog.show(getSupportFragmentManager(), alert);

        }
        SharedPreferences preferences = getSharedPreferences("LocationEnabled", Context.MODE_PRIVATE);
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!(manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
                && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

//            String alert = this.getString(R.string.no_GPS);
//
//            InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert,
//                    InfoDialogFragment.GPS_ENABLED, 1, 0);
//            dialog.show(getSupportFragmentManager(), alert);
            if(preferences.getInt("GPSENABLED",0) != 1) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("GPSENABLED", 1);
                editor.commit();
                String alert = this.getString(R.string.no_GPS);
                InfoDialogFragment dialog = InfoDialogFragment.newInstance(alert, InfoDialogFragment.GPS_ENABLED, 1, 0);
                dialog.show(getSupportFragmentManager(), alert);
            }

        }

    }


    /*public void cs_calculateNoOfDays() {
        String state = cs_popup.getString("PREF_STATE", null);

        if(state==null){

//            Log.d("shared_pref: ", "State value" +state);
            *//**** Set initial SharedPreference Starts ******//*
            SharedPreferences.Editor editor = cs_popup.edit();
            editor.putString("PREF_STATE", "Initial");
            editor.putLong("PREF_TS", Calendar.getInstance().getTimeInMillis());
            editor.commit();
            customerSurveyData.setsShared_PrefCheck(true);
            *//**** Set initial SharedPreference Ends ******//*
        }
        else if("Initial".equals(state)) {
//            Log.d("shared_pref: ", "State value" +state);
            customerSurveyData.setsShared_PrefCheck(true);
        }
        else if("Yes".equals(state)) {
//            Log.d("shared_pref: ", "State value" +state);
            Long sharedPref_Timestamp=cs_popup.getLong("PREF_TS",0);
            Long current_Timestamp=Calendar.getInstance().getTimeInMillis();
            // Log.d("current_timestamp: ",String.valueOf(current_Timestamp));
            // Calculate Diff between saved time & current time
            long diff = current_Timestamp - sharedPref_Timestamp;

            // Calculate difference in days
            long diffDays = diff / (24 * 60 * 60 * 1000);

            // check yes becomes true
            if(StringConstants.CS_YES<=diffDays) {
                customerSurveyData.setsShared_PrefCheck(true);
            }
        }
        else if("No".equals(state)) {
//            Log.d("shared_pref: ", "State value" +state);
            Long sharedPref_Timestamp=cs_popup.getLong("PREF_TS",0);
            Long current_Timestamp=Calendar.getInstance().getTimeInMillis();

            // Calculate Diff between saved time & current time
            long diff = current_Timestamp - sharedPref_Timestamp;

            // Calculate difference in days
            long diffDays = diff / (24 * 60 * 60 * 1000);
            // Log.d("No diffdays",String.valueOf(diffDays));
            // check no becomes true
            if(StringConstants.CS_NO<=diffDays) {
                customerSurveyData.setsShared_PrefCheck(true);
            }
        }
        else if("Later".equals(state)) {
//            Log.d("shared_pref: ", "State value" +state);
            customerSurveyData.setsShared_PrefCheck(true);
        }
    }*/

    /*public void showPopup(final Context context) {
        // ******Code for custom Customer Survey AlertDialog Starts******
        CustomerSurveyData.setsShowPopupFlag(false);
        Utils.setAppLocale(context,CustomerSurveyData.getLanguage());
        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);

        dialog.setContentView(R.layout.cus_survey_alert_dig_box);

        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        // if yes button is clicked, open the customer survey link
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                *//**** Save No in SharedPreference Starts ******//*
                SharedPreferences.Editor editor = cs_popup.edit();
                editor.putString("PREF_STATE", "Yes");
                editor.putLong("PREF_TS", Calendar.getInstance().getTimeInMillis());
                editor.commit();
                *//**** Save No in SharedPreference Ends ******//*

               // Intent intent = new Intent(context, SkyTeamCustomerSurveyActivity.class);
                //intent.putExtra("surveyUrl", CustomerSurveyData.getSurveyURL());
                //context.startActivity(intent);

            }
        });

        Button no = (Button) dialog.findViewById(R.id.btn_no);
        // if no button is clicked, close the custom dialog
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                *//**** Save No in SharedPreference Starts ******//*
                SharedPreferences.Editor editor = cs_popup.edit();
                editor.putString("PREF_STATE", "No");
                editor.putLong("PREF_TS", Calendar.getInstance().getTimeInMillis());
                editor.commit();
                *//**** Save No in SharedPreference Ends ******//*

                *//***** Google Analytics ***//*
                mTracker.send(MapBuilder.createEvent("SURVEYPREFERENCE", StringConstants.NO_THANKS, "SURVEY", null).build());

                *//*mTracker.set(Fields.SAMPLE_RATE, StringConstants.NO_THANKS);
                mTracker.send(MapBuilder.createAppView().build());*//*
                Utils.logMessage(TAG, "send ga for : " + StringConstants.NO_THANKS,
                        Log.DEBUG);
                dialog.dismiss();
            }
        });

        Button later = (Button) dialog.findViewById(R.id.btn_ltr);
        // if later button is clicked, close the custom dialog
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                *//**** Save Later in SharedPreference Starts ******//*
                SharedPreferences.Editor editor = cs_popup.edit();
                editor.putString("PREF_STATE", "Later");
                editor.putLong("PREF_TS", Calendar.getInstance().getTimeInMillis());
                editor.commit();
                *//**** Save Later in SharedPreference Ends ******//*

                *//***** Google Analytics ***//*
                mTracker.send(MapBuilder.createEvent("SURVEYPREFERENCE", StringConstants.LATER, "SURVEY", null).build());
                *//*mTracker.set(Fields.SAMPLE_RATE, StringConstants.LATER);
                mTracker.send(MapBuilder.createAppView().build());*//*
                Utils.logMessage(TAG, "send ga for : " + StringConstants.LATER,
                        Log.DEBUG);
                dialog.dismiss();
            }
        });
        dialog.show();
//            ******Code for custom Customer Survey AlertDialog Ends******
    }
    */
}
