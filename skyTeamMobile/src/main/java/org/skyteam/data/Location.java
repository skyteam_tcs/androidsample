package org.skyteam.data;

import java.io.Serializable;
import java.util.regex.Pattern;


public class Location implements Comparable<String>,Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L
            ;
    private int id;
    private String airportCode;
    private String airportName;
    private String cityCode;
    private String cityName;
    private String countryCode;
    private String countryName;
    private String locationType;


    private String AirportNameEn;
    private String CityNameEn;
    private String CountryNameEn;

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirporNameOtherEng() {
        return AirportNameEn;
    }

    public void setAirportNameOtherEng(String airportName) {
        this.AirportNameEn = airportName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityNameOtherEng() {
        return CityNameEn;
    }

    public void setCityNameOtherEng(String cityName) {
        CityNameEn = cityName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryNameOtherEng() {
        return CountryNameEn;
    }

    public void setCountryNameOtherEng(String countryName) {
        this.CountryNameEn = countryName;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    @Override
    public int compareTo(String another) {
        if (Pattern.compile(Pattern.quote(another), Pattern.CASE_INSENSITIVE).matcher(cityName).find()
                || Pattern.compile(Pattern.quote(another), Pattern.CASE_INSENSITIVE).matcher(airportName).find()) {
            return 0;
        }
        return -1;
    }

    @Override
    public String toString() {
        return airportName + " (" + airportCode + ")";
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

}
