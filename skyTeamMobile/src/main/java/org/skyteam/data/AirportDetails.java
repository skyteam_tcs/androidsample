package org.skyteam.data;

import java.io.Serializable;
import java.util.regex.Pattern;

public class AirportDetails implements Comparable<String>,Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private double latitude;
		private double longitude;
		private double distance;
		private String airportDetailsURL;	
		private int id;
		private String airportCode;
		private String airportName;
		private String cityCode;
		private String cityName;
		private String countryCode;
		private String countryName;
		private String locationType;

		public String getLocationType() {
			return locationType;
		}

		public void setLocationType(String locationType) {
			this.locationType = locationType;
		}

		public String getAirportCode() {
			return airportCode;
		}

		public void setAirportCode(String airportCode) {
			this.airportCode = airportCode;
		}

		public String getAirportName() {
			return airportName;
		}

		public void setAirportName(String airportName) {
			this.airportName = airportName;
		}

		public String getCityCode() {
			return cityCode;
		}

		public void setCityCode(String cityCode) {
			this.cityCode = cityCode;
		}

		public String getCityName() {
			return cityName;
		}

		public void setCityName(String cityName) {
			this.cityName = cityName;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getCountryName() {
			return countryName;
		}

		public void setCountryName(String countryName) {
			this.countryName = countryName;
		}

		@Override
		public int compareTo(String another) {
			if (Pattern.compile(Pattern.quote(another), Pattern.CASE_INSENSITIVE).matcher(cityName).find()
					|| Pattern.compile(Pattern.quote(another), Pattern.CASE_INSENSITIVE).matcher(airportCode).find()) {
				return 0;
		}
			return -1;
		}

		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}


		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		public double getDistance() {
			return distance;
		}

		public void setDistance(double distance) {
			this.distance = distance;
		}

		public String getAirportDetailsURL() {
			return airportDetailsURL;
		}

		public void setAirportDetailsURL(String airportDetailsURL) {
			this.airportDetailsURL = airportDetailsURL;
		}
	


	}
