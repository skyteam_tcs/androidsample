package org.skyteam.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mobility on 12/7/15.
 */
public class AirportList implements Serializable {

    private List<Location> locations = new ArrayList<Location>();

    /**
     *
     * @return
     * The locations
     */
    public List<Location> getLocations() {
        return locations;
    }

    /**
     *
     * @param locations
     * The locations
     */
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
