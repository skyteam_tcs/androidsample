package org.skyteam.data;

import java.util.List;

public class AirportsSearch {
	private LocationDetails locationDetails;
	private List<AirportDetails> airportDetails;

	public LocationDetails getLocationDetails() {
		return locationDetails;
	}

	public void setLocationDetails(LocationDetails locationDetails) {
		this.locationDetails = locationDetails;
	}

	public List<AirportDetails> getAirportDetails() {
		return airportDetails;
	}

	public void setAirportDetails(List<AirportDetails> airportDetails) {
		this.airportDetails = airportDetails;
	}

}
