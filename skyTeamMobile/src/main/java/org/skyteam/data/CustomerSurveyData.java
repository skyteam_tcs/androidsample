/*
package org.skyteam.data;

*/
/**
 * Created by Mobility on 11/20/15.
 *//*

public class CustomerSurveyData {
    // set the below flags for timer, language, screens and screens visited.
    private static boolean sTime = false;
    private static boolean sLanguage = false;
    private static boolean sScreens = false;
    public static int sScreensVisited = 0;

    // set three flags for checking the conditions of shared_pref, default 3 conditions and show popup.
    private static boolean sShared_PrefCheck=false;
    private static boolean sThreeStepsFlag=false;
    private static boolean sShowPopupFlag=false;

    // set survey url for sending url to activity
    private static String surveyURL="null";

    // set country location
    private static String countryLocation=null;

    // set for timer starts & ends
    private static boolean timerStarts=false;

    // get language
    private static String language;

    // call CS url once
    private static int csUrlCall=0;

    public static boolean issTime() {
        return sTime;
    }

    public static void setsTime(boolean sTime)
    {
        CustomerSurveyData.sTime = sTime;
    }

    public static boolean issLanguage() {
        return sLanguage;
    }

    public static void setsLanguage(boolean sLanguage) {
        CustomerSurveyData.sLanguage = sLanguage;
    }

    public static boolean issScreens() {
        return sScreens;
    }

    public static void setsScreens(boolean sScreens) {
        CustomerSurveyData.sScreens = sScreens;
    }

    public static int getsScreensVisited() {
        return sScreensVisited;
    }

    public static void setsScreensVisited(int sScreensVisited) {
        CustomerSurveyData.sScreensVisited += sScreensVisited;
    }

    public static boolean issShared_PrefCheck() {
        return sShared_PrefCheck;
    }

    public static void setsShared_PrefCheck(boolean sShared_PrefCheck) {
        CustomerSurveyData.sShared_PrefCheck = sShared_PrefCheck;
    }

    public static boolean issThreeStepsFlag() {
        return sThreeStepsFlag;
    }

    public static void setsThreeStepsFlag(boolean sThreeStepsFlag) {
        CustomerSurveyData.sThreeStepsFlag = sThreeStepsFlag;
    }

    public static boolean issShowPopupFlag() {
        return sShowPopupFlag;
    }

    public static void setsShowPopupFlag(boolean sShowPopupFlag) {
        CustomerSurveyData.sShowPopupFlag = sShowPopupFlag;
    }

    public static String getSurveyURL() {
        return surveyURL;
    }

    public static void setSurveyURL(String surveyURL) {
        CustomerSurveyData.surveyURL = surveyURL;
    }

    public static String getCountryLocation() {
        return countryLocation;
    }

    public static void setCountryLocation(String countryLocation) {
        CustomerSurveyData.countryLocation = countryLocation;
    }

    public static boolean isTimerStarts() {
        return timerStarts;
    }

    public static void setTimerStarts(boolean timerStarts) {
        CustomerSurveyData.timerStarts = timerStarts;
    }

    public static String getLanguage() {
        return language;
    }

    public static void setLanguage(String language) {
        CustomerSurveyData.language = language;
    }

    public static int getCsUrlCall() {
        return csUrlCall;
    }

    public static void setCsUrlCall(int csUrlCall) {
        CustomerSurveyData.csUrlCall = csUrlCall;
    }

    public static void incrementCsUrlCall(int csUrlCall) {
        CustomerSurveyData.csUrlCall += csUrlCall;
    }
}
*/
