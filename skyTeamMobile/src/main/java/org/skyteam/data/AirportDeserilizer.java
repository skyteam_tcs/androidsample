package org.skyteam.data;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

public class AirportDeserilizer implements JsonDeserializer<AirportsSearch> {

	@Override
	public AirportsSearch deserialize(final JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		AirportsSearch airportsSearch = new AirportsSearch();
		final JsonObject jsonObject = json.getAsJsonObject();
		final JsonElement element = jsonObject.get("AirportDetails");
		if (element.isJsonArray()) {
			ArrayList<AirportDetails> list = context.deserialize(element, new TypeToken<List<AirportDetails>>() {
			}.getType());
			airportsSearch.setAirportDetails(list);
		} else {
			AirportDetails details = context.deserialize(element, AirportDetails.class);
			List<AirportDetails> airportDetails = new ArrayList<AirportDetails>();
			airportDetails.add(details);
			airportsSearch.setAirportDetails(airportDetails);
		}

		return airportsSearch;
	}

}
