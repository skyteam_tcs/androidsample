package org.skyteam.loaders;

import java.util.List;

import org.skyteam.utils.SkyTeamException;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;



public abstract class AsyncLoader extends AsyncTaskLoader<AsyncResult> {
	private AsyncResult mList;

	public AsyncLoader(Context context) {
		super(context);
	}

	/* Runs on a worker thread */
	@Override
	public AsyncResult loadInBackground() {
		return getData();
	}

	/* Runs on the UI thread */
	@Override
	public void deliverResult(AsyncResult list) {
		if (isReset()) {
			// An async query came in while the loader is stopped
			if (list != null) {
				list.clear();
			}
			return;
		}
		AsyncResult oldList = mList;
		mList = list;

		if (isStarted()) {
			super.deliverResult(list);
		}

		if (oldList != null && oldList != list) {
			oldList.clear();
		}
	}

	/**
	 * Starts an asynchronous load of the contacts list data. When the result is
	 * ready the callbacks will be called on the UI thread. If a previous load
	 * has been completed and is still valid the result may be passed to the
	 * callbacks immediately.
	 * <p/>
	 * Must be called from the UI thread
	 */
	@Override
	protected void onStartLoading() {
		if (mList != null) {
			deliverResult(mList);
		}
		if (takeContentChanged() || mList == null) {
			forceLoad();
		}
	}

	/**
	 * Must be called from the UI thread
	 */
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	@Override
	public void onCanceled(AsyncResult list) {
		super.onCanceled(list);
		if (list != null) {
			list.clear();
		}
	}

	@Override
	protected void onReset() {
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		if (mList != null) {
			mList.clear();
		}
		mList = null;
	}

	protected List<?> getList() {
		return mList.getData();
	}

	public SkyTeamException getException() {
		return mList.getException();
	}

	/**
	 * @return a List with the data to load
	 */
	protected abstract AsyncResult getData();
}
