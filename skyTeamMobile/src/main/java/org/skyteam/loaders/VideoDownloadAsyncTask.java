package org.skyteam.loaders;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.skyteam.R;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by sowmya on 3/30/15.
 */
public class VideoDownloadAsyncTask extends AsyncTask<VideoDownloadParams, Integer, String> {

    public static final String TAG= "VideoDownloadAsyncTask";
    private DownloadCallback mDownloadCallback;
    private ProgressDialog mDialog;
    private HttpsURLConnection conn;
    private Context mContext;
    private Activity mActivity;
    private ProgressBar mProgressBar;
    private TextView progressStatusText;

    public VideoDownloadAsyncTask(DownloadCallback downloadCallback,Context context,Activity activity) {

        mDownloadCallback = downloadCallback;
        mContext= context;
        mActivity= activity;

    }



    @Override
    protected void onPreExecute() {

        mDialog = new ProgressDialog(mContext);
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);
        mDialog.show();
        //View dialogView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.frag_download_dialog,null);
        View dialogView = mActivity.getLayoutInflater().inflate(R.layout.frag_download_dialog,null);
        Button btnCancel = (Button)dialogView.findViewById(R.id.btn_cancel);
        mProgressBar = (ProgressBar) dialogView.findViewById(R.id.download_progress);
        mProgressBar.setMax(100);
        progressStatusText = (TextView) dialogView.findViewById(R.id.download_status);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cancelTask();
            }
        });
        mDialog.setContentView(dialogView);
    }

    @Override
    protected String doInBackground(VideoDownloadParams... vparms) {

        String strResult = null;

        VideoDownloadParams videoparams = vparms[0];

        try {

            URL url = new URL(videoparams.strUrl);

            conn = (HttpsURLConnection) url.openConnection();
            conn.connect();
            int fileLength = conn.getContentLength();

            InputStream input = new BufferedInputStream(conn.getInputStream(), 1024 * 4);
            OutputStream output = new FileOutputStream(videoparams.strFilePath);
            byte data[] = new byte[1024 * 4];
            long total = 0;
            int count = 0;
            while ((count = input.read(data)) != -1 ) {
                total += count;
                publishProgress((int) ((total * 100) / fileLength));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();


        } catch (ClientProtocolException e) {
            return strResult;
        } catch (IOException e) {
            return strResult;
        }
        strResult= "Success";
        return strResult;
    }

    @Override
    protected void onPostExecute(String result) {

        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        if (result != null) {
            mDownloadCallback.onDownloadSuccess(result);

        } else {
            mDownloadCallback.onDownloadFailure(result);
        }

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        mProgressBar.setProgress(progress[0]);
        progressStatusText.setText(String.valueOf(progress[0]+mContext.getString(R.string.yoga_percentage)));
    }


    public void cancelTask(){

        mDialog.dismiss();
        boolean booCancelled =this.cancel(true);
        mDownloadCallback.onDownloadCancelled(null);

        /*new Thread(new Runnable(){
            @Override
            public void run() {

               /* if(conn != null){
                    conn.disconnect();
                }

            }
        }).start();*/




    }

}
