package org.skyteam.loaders;

import java.util.List;

import org.skyteam.utils.SkyTeamException;



public class AsyncResult {
	private List<?> data;
	private SkyTeamException exception;

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public SkyTeamException getException() {
		return exception;
	}

	public void setException(SkyTeamException exception) {
		this.exception = exception;
	}

	public void clear() {
		// TODO Auto-generated method stub

	}

}
