package org.skyteam.loaders;

/**
 * Created by sowmya on 3/30/15.
 */
public class VideoDownloadParams {

    String strUrl,strFilePath;
    public VideoDownloadParams(String strUrl,String strFilePath){
        this.strUrl = strUrl;
        this.strFilePath = strFilePath;
    }
}
