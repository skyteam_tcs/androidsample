package org.skyteam.loaders;

/**
 * Created by sowmya on 3/30/15.
 */
public interface DownloadCallback {

    void onDownloadSuccess(String result);
    void onDownloadFailure(String result);
    void onDownloadCancelled(String result);
}
