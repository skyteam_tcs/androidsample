package org.skyteam.utils;

public class SkyTeamException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SkyTeamException() {
		super();
	}

	public SkyTeamException(String message) {
		super(message);
	}

}
