package org.skyteam.utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyCancelCallBack;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.handlers.AirportHandler;
import org.skyteam.handlers.FlightHandler;
import org.skyteam.handlers.FlightStatusHandler;
import org.skyteam.handlers.LoungeFinderHandler;
import org.skyteam.handlers.SearchStringHandler;
import org.skyteam.handlers.SettingsHandler;
import org.skyteam.handlers.SkyTeamMemberHandler;
import org.skyteam.handlers.SkyTipsHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;


/**
 * 
 * This class is the Javascript Interface class. Used for communication between
 * HTML and native.
 * 
 */
public class JSIObject {
	private static final String TAG = "SKYTEAM";
	private Context context;
	private String control;
	private String command;
	private Activity mActivity;
	private WebView mWebView;
	private String mResultUrl;
	LoungeFinderHandler loungeFinderHandler=null;
	FlightStatusHandler flightStatusHandler=null;
	SkyTipsHandler skyTipsHandler=null;
	SkyCancelCallBack skycallback;

	public JSIObject(Context context, Activity activity, WebView webView) {
		this.context = context;
		mActivity = activity;
		skycallback=(SkyCancelCallBack)activity;
		mWebView = webView;

	}

	@JavascriptInterface
	public void showStr(String str) {
		Utils.logMessage(TAG, str, Log.ERROR);
	}

	/*
	 * This method is used by HTML to communicate to Android.
	 * control: instruction
	 * command: input parameter 
	 */
	@JavascriptInterface
	public void notifyNative(String jsOutputJSON) {

		if (jsOutputJSON != null) {

			try {
				JSONObject jobj = new JSONObject(jsOutputJSON);
				control = jobj.getString(StringConstants.CONTROL);
				command = jobj.getString(StringConstants.COMMAND);

				// new Thread(new StartNativeControl()).start();
				mActivity.runOnUiThread(runnable);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}

	private Runnable runnable=new Runnable() {

		String result = null;

		@Override
		public void run() {
			if (control != null && control.length() > 0) {

				if (control.equalsIgnoreCase(StringConstants.CMD_DELETE_FLIGHT)
						|| control
								.equalsIgnoreCase(StringConstants.CMD_SAVE_FLIGHTS)) {

					FlightHandler deleteFlightHandler = new FlightHandler(
							context);
					result = deleteFlightHandler
							.nativeHandler(control, command);

				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_DELETE_AIRPORT)
						|| control
								.equalsIgnoreCase(StringConstants.CMD_SAVE_AIRPORTS)) {

					AirportHandler deleteAirportHandler = new AirportHandler(
							context);
					result = deleteAirportHandler.nativeHandler(control,
							command);

				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_SEARCH_STRING)) {
					SearchStringHandler searchStringHandler = new SearchStringHandler(
							context);
					searchStringHandler.nativeHandler(command);

				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_LOUNGE_FINDER)) {

					 loungeFinderHandler = new LoungeFinderHandler(
							context);
					result = loungeFinderHandler.nativeHandler(command);
				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_OPEN_SKYTEAM_MEMEBR)) {
					SkyTeamMemberHandler skyTeamMemberhandler = new SkyTeamMemberHandler(
							context);
					result = skyTeamMemberhandler.nativeHandler(control,
							command);

				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_SKYTEAM_MEMEBR_ID)) {
					startBrowser(command);
				}else if (control
						.equalsIgnoreCase(StringConstants.CMD_SKYTEAMWEBSITE)) {
					result = null;
					startBrowser(command);
				}else if (control
						.equalsIgnoreCase(StringConstants.CMD_SKYTIPS)) {
					 skyTipsHandler = new SkyTipsHandler(context);
					result = skyTipsHandler.nativeHandler(command);
				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_FLIGHT_STATUS)) {
					
					 flightStatusHandler = new FlightStatusHandler(
							context,skycallback);
					result = flightStatusHandler.nativeHandler(control,command);
				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_SETTINGS)) {
					SettingsHandler settingsHandler = new SettingsHandler(
							context);
					result = settingsHandler.nativeHandler(command);
				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_BACKBUTTON)
						&& command.equalsIgnoreCase("true")) {
					sendApplicationToBackground();
				} else if (control
						.equalsIgnoreCase(StringConstants.CMD_SAVED_FLIGHT_STATUS)) {
					 flightStatusHandler = new FlightStatusHandler(
							context);
					result = flightStatusHandler.nativeHandler(control,command);
				}else if (control
						.equalsIgnoreCase(StringConstants.CANCEL_ASYNC_TASK) && command.equalsIgnoreCase("true")){
					
					if(loungeFinderHandler!=null){
						loungeFinderHandler.cancel();
						loungeFinderHandler=null;
					}
					if(skyTipsHandler!=null){
						  skyTipsHandler.cancel();
						  skyTipsHandler=null;
					}
					if(flightStatusHandler!=null){
						flightStatusHandler.cancel();
						flightStatusHandler=null;
					}
					//TODO
					skycallback.cancel();
					
					
				}

				if (result != null) {
					loadTheUrl(result);
				result = null;
				}
			}

		}

	};

	public void showAlert(String message) {
		AlertDialog.Builder myDialog = new AlertDialog.Builder(context);

		myDialog.setTitle("Alert").setMessage(message)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).create().show();

	}

	public void sendApplicationToBackground() {
		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mActivity.startActivity(startMain);

			}
		});

	}

	private void loadTheUrl(final String resultUrl) {
		if (resultUrl != null) {
			Utils.logMessage(TAG, resultUrl, Log.ERROR);
			mResultUrl = resultUrl;
			// loading the url in the webview on uithread
			mActivity.runOnUiThread(new LoadingRunnable());
		}
	}

	private void startBrowser(final String resultUrl) {

		if (resultUrl != null) {
			Utils.logMessage(TAG, resultUrl, Log.ERROR);
			mResultUrl = resultUrl;
			// loading the url in the webview on uithread
			mActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(mResultUrl));
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					mActivity.startActivity(i);
				}
			});

		}
	}

	/*
	 * Runnable to load the url in UI thread
	 */
	public class LoadingRunnable implements Runnable {

		@Override
		public void run() {
			mWebView.loadUrl(mResultUrl);
		}
	}
}
