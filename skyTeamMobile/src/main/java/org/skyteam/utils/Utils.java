package org.skyteam.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.R;
import org.skyteam.StringConstants;
//import org.skyteam.data.CustomerSurveyData;
import org.skyteam.database.DBHelper;
//import org.skyteam.handlers.CustomerSurveyHandler;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import java.util.Locale;

public class Utils {
    public static final int TIME_OUT = 20000;
   // CustomerSurveyData customerSurveyData = new CustomerSurveyData();

    public static void logMessage(String tagName, String msg, int priority) {
        switch (priority) {

            case Log.VERBOSE:
                // Log.v(tagName, msg); // to display VERBOSE messages
                break;
            case Log.DEBUG:
                // Log.d(tagName, msg); // to display DEBUG messages
                break;
            case Log.INFO:
                // Log.i(tagName, msg); // to display InFO messages
                break;
            case Log.ERROR:
                // Log.e(tagName, msg); // to display Error messages
                break;
            case Log.WARN:
                // Log.w(tagName, msg); // to display Warning messages
                break;
            case Log.ASSERT:
                break;
            default:
                // Log.e(tagName, msg);
        }
    }


    public static String getDistance(Context ctxt,double dist){


        DBHelper dbHelper = DBHelper.getInstance(ctxt);
        JSONArray settingsJSONArr=dbHelper.getSettingFile();
        try {
            JSONObject settingsJSON=(JSONObject) settingsJSONArr.get(0);
            String distanceStr=(String) settingsJSON.get("Distance");
            String mLang = dbHelper.getSettingFileObj().getString("Language");
            if("kilometers".equals(distanceStr)){

                return (Math.round(dist)+" " +(ctxt.getString(R.string.km)));

            }else{

                long strCalcMiles=(Math.round(dist*0.62137*10)/10);
                return strCalcMiles +" " +(ctxt.getString(R.string.miles));

            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return  Math.round(dist)+" km";
    }

    public static String getCurrentLanguage(Context context) throws JSONException {
        String lang= "En";
        DBHelper dbHelper = DBHelper.getInstance(context);
        JSONObject languageJsonObj = dbHelper.getSettingFileObj(); // your
        // language
        lang = languageJsonObj.getString("Language");

        return lang;
    }

    public static void setAppLocale(Context context,String lang){

        Locale locale = null;

        if(("En".equalsIgnoreCase(lang))){
            locale=Locale.US;
        }
        else if(("zh".equalsIgnoreCase(lang))){
            locale = Locale.SIMPLIFIED_CHINESE;
        }
        else if(("zh-Hant".equalsIgnoreCase(lang))){
            locale = Locale.TRADITIONAL_CHINESE;
        }
        else {
            locale = new Locale(lang.toLowerCase(), lang.toUpperCase());
        }

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }

    /*public void popupConditionsMet(String checkAgainLanguage,String pageId, Context context) {
        if (customerSurveyData.getsScreensVisited() >= 3) {
            customerSurveyData.setsScreens(true);
        }

        *//**** If user change the language after starts timer, we stop this process.
         *  so we check the language again before send request.
         *//*


        if (customerSurveyData.issLanguage() == true && customerSurveyData.issScreens() == true && customerSurveyData.issTime() == true) {
            if ("En".equals(checkAgainLanguage) || "zh".equals(checkAgainLanguage) || "Es".equals(checkAgainLanguage) || "Fr".equals(checkAgainLanguage)) {
                customerSurveyData.setsLanguage(true);
                customerSurveyData.setsThreeStepsFlag(true);
                CustomerSurveyData.incrementCsUrlCall(1);
//                 Log.d("Survey_Result: ","msg");
                if(CustomerSurveyData.getCsUrlCall()==1){
//                    Log.d("Survey_Result: ","cs call "+String.valueOf(1));
                    new CustomerSurveyHandler().customizedAlert(pageId,checkAgainLanguage,context);
                }
            }
        }
    }*/
}