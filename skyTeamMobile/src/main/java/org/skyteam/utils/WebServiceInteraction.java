package org.skyteam.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyTeamMapActivity;
import org.skyteam.database.DBHelper;



import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;

/**
 *
 * Utility class for making the webservice call
 *
 */
public class WebServiceInteraction {

    private Context context;
    private int TIME_OUT = 20000; // Connection timeout
    public static final String TAG = "WEBSERVICEINTERACTION";

    private HttpsURLConnection urlConn = null;

    public WebServiceInteraction(Context context) {
        this.context = context;
    }

    /*
     * Method to fetch the JSON data from webservice. On failure case returns
     * the "ERROR" object
     */
    public JSONArray getJsonArray(String[] params) {

        try {
            params[0] = params[0].trim();
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.connect();
            InputStream inputStream = urlConn.getInputStream();
            JSONArray tempObj = new JSONArray(readStream(inputStream));
            return tempObj;

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        JSONArray errorObj = new JSONArray();
        errorObj.put("ERROR");
        return errorObj;
    }

    /*
     * Method to fetch the JSON data from webservice. On failure case returns
     * the "ERROR" object
     */
    public String getCityAirportJSON(String[] params, String lang) {

        String jsonString = "ERROR";

        try {
            params[0] = params[0].trim();
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);

            if(lang.equalsIgnoreCase("zh"))
            {
                urlConn.setRequestProperty("Accept-Language", "zh-Hans");
                urlConn.setRequestProperty("api_key", StringConstants.NEW_API_KEY);
                urlConn.setRequestProperty("Source", "SkyApp");
            }
            else if(lang.equalsIgnoreCase("Ja") || lang.equalsIgnoreCase("Ko") || lang.equalsIgnoreCase("zh-Hant"))
            {
                urlConn.setRequestProperty("Accept-Language", lang);
                urlConn.setRequestProperty("api_key", StringConstants.NEW_API_KEY);
                urlConn.setRequestProperty("Source", "SkyApp");
            }
            else
            {
                urlConn.setRequestProperty("Accept-Language", lang);
                urlConn.setRequestProperty("api_key", StringConstants.NEW_API_KEY);
                urlConn.setRequestProperty("Source", "SkyApp");
            }

            urlConn.connect();
            InputStream inputStream = urlConn.getInputStream();
            jsonString= readStream(inputStream);

            //to verify if the response is a valid json Array. Return Error otherwise
//            JSONArray cityJsonArray = new JSONArray(jsonString);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } /*catch (JSONException je) {
			// TODO Auto-generated catch block
			je.printStackTrace();
		}*/catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonString;
    }

    private String readStream(InputStream inputStream) {

        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public JSONObject getSkyTipsJson(String[] params,String lang) {

        try {
            params[0] = params[0].trim();
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setRequestMethod("GET");
            urlConn.setRequestProperty("source", "SkyApp");
            urlConn.setRequestProperty("api_key", StringConstants.API_KEY);
            urlConn.addRequestProperty("Cache-Control", "no-cache");

            if(lang.equalsIgnoreCase("zh"))
            {
                urlConn.setRequestProperty("Accept-Language", "zh-Hans");
            }
            else
            {
                urlConn.setRequestProperty("Accept-Language", lang);
            }
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.connect();
            InputStream inputStream = urlConn.getInputStream();
            JSONObject tempObj = new JSONObject(readStream(inputStream));
            return tempObj;

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        JSONObject errorObj = new JSONObject();
        try {
            errorObj.put(params[1], "ERROR");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return errorObj;
    }

	/*
	 * Method to fetch the JSON data from webservice. On failure case returns
	 * the "ERROR" object
	 */

    public JSONObject getJsonData(String[] params) {

        try {
            params[0] = params[0].trim();
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.setRequestProperty("api_key", StringConstants.API_KEY);
           urlConn.setRequestProperty("source", "SkyApp");
            urlConn.connect();
            InputStream inputStream = urlConn.getInputStream();
            JSONObject tempObj = new JSONObject(readStream(inputStream));
            return tempObj;

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        JSONObject errorObj = new JSONObject();
        try {
            errorObj.put(params[1], "ERROR");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return errorObj;
    }
	
	/*
	 * Method to fetch the JSON data from webservice. On failure case returns
	 * the "ERROR" object
	 */

    public JSONObject getJsonDataReqHeader(String[] params,String requestHeaderkey,String requestHeaderValue) {

        try {
            params[0] = params[0].trim();
            if(params[0].contains(" ")){
                params[0] = params[0].replaceAll(" ","%20");
            }
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.setRequestProperty(requestHeaderkey, requestHeaderValue);
            urlConn.setRequestProperty("api_key", StringConstants.NEW_API_KEY);
            /*if(params[1].equalsIgnoreCase(StringConstants.CMD_LOUNGE_FINDER)){
                Log.d("888888","Lounge api key");
                urlConn.setRequestProperty("api_key"," 5m49bjnak3nsrv5hdkf4aab6");
            }*/
            urlConn.setRequestProperty("source", "SkyApp");
            urlConn.connect();
            /*Log.d("Connection","Response Code : "+urlConn.getResponseCode());
            Log.d("Connection","url for jsonException : "+url);*/
            InputStream inputStream;
//            if (urlConn.getResponseCode() == 404 || urlConn.getResponseCode() == 400 || urlConn.getResponseCode() == 403)
            if (urlConn.getResponseCode() == 200)
            {
                inputStream = urlConn.getInputStream();
            }
            else {
                inputStream = urlConn.getErrorStream();
            }
            String strCheck = readStream(inputStream);
            JSONObject tempObj = new JSONObject(strCheck);
            return tempObj;

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        JSONObject errorObj = new JSONObject();
        try {
            errorObj.put(params[1], "ERROR");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return errorObj;
    }
    public JSONObject getLoungeDataReqHeader(String[] params,String requestHeaderkey,String requestHeaderValue) {

/*
        try {
            params[0] = params[0].trim();
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.setRequestProperty(requestHeaderkey, requestHeaderValue);
            urlConn.setRequestProperty("api_key", StringConstants.LOUNGE_NEW_API_KEY);
            urlConn.setRequestProperty("source", "SkyApp");
            urlConn.connect();
            InputStream inputStream;
            inputStream = urlConn.getInputStream();
            JSONObject tempObj = new JSONObject(readStream(inputStream));
            return tempObj;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject errorObj = new JSONObject();
        try {
            errorObj.put(params[1], "ERROR");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return errorObj;

*/












        try {
            params[0] = params[0].trim();
            if(params[0].contains(" ")){
                params[0] = params[0].replaceAll(" ","%20");
            }
            URL url = new URL(params[0]);
            urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setConnectTimeout(TIME_OUT);
            urlConn.setRequestProperty(requestHeaderkey, requestHeaderValue);
            urlConn.setRequestProperty("api_key", StringConstants.LOUNGE_NEW_API_KEY);
            urlConn.setRequestProperty("source", "SkyApp");
            urlConn.connect();
            InputStream inputStream;
            if (urlConn.getResponseCode() == 200)
            {
                inputStream = urlConn.getInputStream();
            }
            else {
                inputStream = urlConn.getErrorStream();
            }
            String strCheck = readStream(inputStream);
            JSONObject tempObj = new JSONObject(strCheck);
            return tempObj;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject errorObj = new JSONObject();
        try {
            errorObj.put(params[1], "ERROR");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return errorObj;

    }

    public void cancelConnection(){


        if(urlConn != null) {

            urlConn.disconnect();
        }
    }
}