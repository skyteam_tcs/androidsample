package org.skyteam.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 
 * Utility Class to check the connectivity status
 * 
 */
public class NetworkUtil {

	private static int TYPE_WIFI = 1;
	private static int TYPE_MOBILE = 2;
	private static int TYPE_ETHERNET = 3;
	private static int TYPE_WIMAX = 4;
	private static int TYPE_NOT_CONNECTED = 0;
	private static boolean isConnected = false;

	public static int getConnectivityStatus(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
				return TYPE_WIFI;
			}

			if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
				return TYPE_MOBILE;
			}

			if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET) {
				return TYPE_ETHERNET;
			}

			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIMAX) {
				return TYPE_WIMAX;
			}
		}
		return TYPE_NOT_CONNECTED;
	}

	public static String getConnectivityStatusString(Context context) {
		int conn = NetworkUtil.getConnectivityStatus(context);
		String status = null;
		if (conn == NetworkUtil.TYPE_WIFI) {
			status = "Wifi enabled";
		} else if (conn == NetworkUtil.TYPE_MOBILE) {
			status = "Mobile data enabled";
		} else if (conn == NetworkUtil.TYPE_ETHERNET) {
			status = "Connected to Ethernet";
		} else if (conn == NetworkUtil.TYPE_WIMAX) {
			status = "Connected to WiMax";
		} else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
			status = "Not connected to Internet";
		}
		return status;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public static void setConnected(boolean status) {
		isConnected = status;
	}
}
