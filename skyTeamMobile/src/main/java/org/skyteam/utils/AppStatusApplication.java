package org.skyteam.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
//import android.support.multidex.MultiDex;
//import android.support.multidex.MultiDexApplication;

import com.point_consulting.testindoormap.MyApplication;

/**
 * Created by Mobility on 11/26/15.
 * AppStatusApplication helps us to find, whether the application came to
 * foreground or not by using interfaces like ActivityLifecycleCallbacks,
 * ComponentCallbacks2 and Broadcast receiver.
 */

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class AppStatusApplication extends MyApplication implements
        Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    private static String TAG = AppStatusApplication.class.getName();

    public static String stateOfLifeCycle = "";

    public static String countryCode;

    public static boolean wasInBackground = false;
    ScreenOffReceiver screenOffReceiver = new ScreenOffReceiver();

    @Override
    public void onCreate() {
        super.onCreate();


        countryCode = this.getResources().getConfiguration().locale.getCountry();
        registerActivityLifecycleCallbacks(this);

        registerReceiver(screenOffReceiver, new IntentFilter(
                "android.intent.action.SCREEN_OFF"));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //MultiDex.install(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle arg1) {
        wasInBackground = false;
        stateOfLifeCycle = "Create";
    }

    @Override
    public void onActivityStarted(Activity activity) {
        stateOfLifeCycle = "Start";
    }

    @Override
    public void onActivityResumed(Activity activity) {
        stateOfLifeCycle = "Resume";
    }

    @Override
    public void onActivityPaused(Activity activity) {
        stateOfLifeCycle = "Pause";
    }

    @Override
    public void onActivityStopped(Activity activity) {
        stateOfLifeCycle = "Stop";
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle arg1) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        wasInBackground = false;
        stateOfLifeCycle = "Destroy";
    }


    @Override
    public void onTrimMemory(int level) {
        if (stateOfLifeCycle.equals("Stop")) {
            wasInBackground = true;
        }
        super.onTrimMemory(level);
    }





    class ScreenOffReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            wasInBackground = true;
        }
    }
}
