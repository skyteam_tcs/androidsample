package org.skyteam.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.point_consulting.testindoormap.MapsActivity;


import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import org.skyteam.R;
import org.skyteam.StringConstants;
import org.skyteam.activities.SkyTeamMapActivity;
import org.skyteam.activities.SkyYogaActivity;
import org.skyteam.activities.SkyteamWebviewActivity;
import org.skyteam.adapters.MenuAdapter;
import org.skyteam.data.Category;
//import org.skyteam.data.CustomerSurveyData;
import org.skyteam.data.Item;
import org.skyteam.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class SideMenuView {

    protected MenuDrawer mMenuDrawer;
    private ListView mList;
    private MenuAdapter mMenuAdapter;
    private int mActivePosition = 0;
    private Activity fragActivity;
    private Context mContext;
    public static final int VIEW_MAP = 0;
    public static final int VIEW_YOGA =1;
    private int mViewType;
    private String mCurrentLang;
    private static GoogleAnalytics mGa;
    private static Tracker mTracker;
    // Placeholder property ID.
    private static final String GA_PROPERTY_ID = "UA-45492668-1";
    private static final String TAG = "SKYTEAM";

    //CustomerSurveyData customerSurveyData;

    public SideMenuView(Context context,String currentLang){
        mContext = context;
        mCurrentLang=currentLang;
    }


    public MenuDrawer constructMenuDrawer(Activity act,Typeface mFaceTnb,AdapterView.OnItemClickListener mItemClickListener,int viewtype){
        this.fragActivity = act;
        this.mViewType=viewtype;
        mMenuDrawer = MenuDrawer.attach(act, MenuDrawer.Type.BEHIND,
                Position.LEFT, MenuDrawer.MENU_DRAG_WINDOW);

        List<Object> items = new ArrayList<Object>();
        items.add(new Category(mContext.getString(R.string.sky_menu)));
        items.add(new Item(mContext.getString(R.string.sky_home),
                R.drawable.ic_home_menu));
        items.add(new Item(mContext.getString(R.string.sky_flight_finder),
                R.drawable.ic_flightsearch_menu));
        items.add(new Item(mContext.getString(R.string.sky_flight_status),
                R.drawable.ic_flightstatus_menu));
        items.add(new Item(mContext.getString(R.string.sky_airport_finder),
                R.drawable.ic_airports));
        items.add(new Item(mContext.getString(R.string.sky_lounge_finder),
                R.drawable.ic_longue_finder_menu));
        items.add(new Item(mContext.getString(R.string.sky_priority),
                R.drawable.skypriority));
//        items.add(new Item(mContext.getString(R.string.sky_skytips),
//                R.drawable.ic_skytips_menu));





//        items.add(new Item(mContext.getString(R.string.sky_yoga),
//                R.drawable.yoga));

        items.add(new Item(mContext.getString(R.string.sky_about),
                R.drawable.ic_about_menu));
        items.add(new Item(mContext.getString(R.string.sky_settings),
                R.drawable.ic_settings_menu));

        mList = new ListView(act);
        mList.setSelector(android.R.color.transparent);
        mList.setBackgroundColor(Color.parseColor("#f6f6f6"));

        mMenuAdapter = new MenuAdapter(act, items, mFaceTnb,mCurrentLang);
        mMenuAdapter.setListener((MenuAdapter.MenuListener) act);
        mMenuAdapter.setActivePosition(mActivePosition);
        mMenuDrawer.setDropShadowEnabled(true);
        mMenuDrawer.setDropShadow(R.drawable.menu_gradient);
        mMenuDrawer.setDropShadowSize(50);

        mList.setAdapter(mMenuAdapter);
        mList.setOnItemClickListener(mItemClickListener);

        mMenuDrawer.setMenuView(mList);

        mMenuDrawer.setDrawerIndicatorEnabled(true);
        if(mViewType == VIEW_MAP) {
            mMenuDrawer.setContentView(R.layout.fragment_airport_search);
        }else if(mViewType == VIEW_YOGA){
            mMenuDrawer.setContentView(R.layout.activity_yoga_video);
        }

        return mMenuDrawer;

    }



    public void menuItemClick(AdapterView<?> adapterView, View view,
                              int position, long id) {
        mActivePosition = position;
        mGa = GoogleAnalytics.getInstance(mContext);
        mTracker = mGa.getTracker(GA_PROPERTY_ID);
        switch (position) {
            case 1: {
                // home
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "homePage");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 2: {
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "searchFlights");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 3: {
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "flightstatus");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 4: {
                if(mViewType == VIEW_MAP) {
                    mMenuDrawer.closeMenu();
                }else{
                    mMenuDrawer.closeMenu();
                    mTracker.set(Fields.SCREEN_NAME, "airportSearchPage");
                    mTracker.send(MapBuilder.createAppView().build());
                    Utils.logMessage(TAG, "send ga for : airportSearchPage " ,
                            Log.DEBUG);
                   // customerSurveyData.setsScreensVisited(StringConstants.ONE);
                   // new Utils().popupConditionsMet(mCurrentLang,"airportSearchPage",mContext);
                   /* if (CustomerSurveyData.issShowPopupFlag()){
                        new SkyteamWebviewActivity()
                                .showPopup(mContext);
                    }*/
                    Intent intent = new Intent(fragActivity,
                            SkyTeamMapActivity.class);
                    fragActivity.startActivity(intent);
                    // fragActivity.finish();
                }

            }
            break;
            case 5: {

                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "loungesSearch");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 6: {
                // home
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "skyPriorityFinder");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 7: {
                // SkyTios
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "skyTipsFinder");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 111: {
                // yoga Activity
                if(mViewType == VIEW_YOGA) {
                    mMenuDrawer.closeMenu();
                }else {
                    mMenuDrawer.closeMenu();
                    mTracker.set(Fields.SCREEN_NAME, "yogaVideoPage");
                    mTracker.send(MapBuilder.createAppView().build());
                    Utils.logMessage(TAG, "send ga for : yogaVideoPage " ,
                            Log.DEBUG);
                   // customerSurveyData.setsScreensVisited(StringConstants.ONE);
                 //   new Utils().popupConditionsMet(mCurrentLang,"yogaVideoPage",mContext);
                  /*  if (CustomerSurveyData.issShowPopupFlag()){
                        new SkyteamWebviewActivity()
                                .showPopup(mContext);
                    }*/
                    Intent intent = new Intent(fragActivity,
                            SkyYogaActivity.class);
                    fragActivity.startActivity(intent);
                    //fragActivity.finish();
                }

            }
            break;

            case 8: {
                // home
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "aboutSkyTeam");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;
            case 9: {
                // settings
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "settings");
                fragActivity.startActivity(intent);
                fragActivity.finish();

            }
            break;

            default:
                mMenuDrawer.closeMenu();
                Intent intent = new Intent(fragActivity,
                        SkyteamWebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_ISHOMEPAGE, true);
                intent.putExtra(SkyteamWebviewActivity.INTENT_EXTRA_HTMLPAGE, "homePage");
                fragActivity.startActivity(intent);
                fragActivity.finish();

                break;
        }
    }
}