package org.skyteam.adapters;

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.skyteam.R;
import org.skyteam.data.Location;
import org.skyteam.database.DBHelper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

/**
 * A ListAdapter that manages a ListView backed by an array of arbitrary
 * objects. By default this class expects that the provided resource id
 * references a single TextView. If you want to use a more complex layout, use
 * the constructors that also takes a field id. That field id should reference a
 * TextView in the larger layout resource.
 * 
 * However the TextView is referenced, it will be filled with the toString() of
 * each object in the array. You can add lists or arrays of custom objects.
 * Override the toString() method of your objects to determine what text will be
 * displayed for the item in the list.
 * 
 * To use something other than TextViews for the array display, for instance,
 * ImageViews, or to have some of data besides toString() results fill the
 * views, override {@link #getView(int, View, ViewGroup)} to return the type of
 * view you want.
 */
public class AirportArrayAdapter extends BaseAdapter implements
		Filterable {
	/**
	 * Contains the list of objects that represent the data of this
	 * ArrayAdapter. The content of this list is referred to as "the array" in
	 * the documentation.
	 */
	private List<Location> mObjects;

	/**
	 * Lock used to modify the content of {@link #mObjects}. Any write operation
	 * performed on the array should be synchronized on this lock. This lock is
	 * also used by the filter (see {@link #getFilter()} to make a synchronized
	 * copy of the original array of data.
	 */
	private final Object mLock = new Object();

	/**
	 * The resource indicating what views to inflate to display the content of
	 * this array adapter.
	 */
	private int mResource;

	/**
	 * The resource indicating what views to inflate to display the content of
	 * this array adapter in a drop down widget.
	 */
	private int mDropDownResource;

	/**
	 * If the inflated resource is not a TextView, {@link #mFieldId} is used to
	 * find a TextView inside the inflated views hierarchy. This field must
	 * contain the identifier that matches the one defined in the resource file.
	 */
	private int mFieldId = R.id.text;

	/**
	 * Indicates whether or not {@link #notifyDataSetChanged()} must be called
	 * whenever {@link #mObjects} is modified.
	 */
	private boolean mNotifyOnChange = true;

	private Context mContext;

	private ArrayList<Location> mOriginalValues;
	private ArrayFilter mFilter;

	private Typeface mTypeface;

	private LayoutInflater mInflater;

	private CharSequence mSelectedChar = "";
	private String mLang;
	private Locale mLocale;
	private String  regexEng="^(?=.*[a-zA-Z])[a-zA-Z0-9. -]*$";

	/**
	 * Constructor
	 * 
	 * @param context
	 *            The current context.
	 * @param textViewResourceId
	 *            The resource ID for a layout file containing a TextView to use
	 *            when instantiating views.
	 */
	public AirportArrayAdapter(Context context, int textViewResourceId) {
		DBHelper dbHelper = DBHelper.getInstance(getContext());
		try {
			mLang= dbHelper.getSettingFileObj().getString("Language");
		
			if(("En".equalsIgnoreCase(mLang))){
				mLocale=Locale.US;
			}
            else
            {
                mLocale = new Locale(mLang.toLowerCase(), mLang.toUpperCase());
            }


        } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init(context, textViewResourceId, 0, new ArrayList<Location>());

	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            The current context.
	 * @param resource
	 *            The resource ID for a layout file containing a layout to use
	 *            when instantiating views.
	 * @param textViewResourceId
	 *            The id of the TextView within the layout resource to be
	 *            populated
	 */
	public AirportArrayAdapter(Context context, int resource,
			int textViewResourceId) {
		init(context, resource, textViewResourceId, new ArrayList<Location>());
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            The current context.
	 * @param textViewResourceId
	 *            The resource ID for a layout file containing a TextView to use
	 *            when instantiating views.
	 * @param objects
	 *            The objects to represent in the ListView.
	 */
	public AirportArrayAdapter(Context context, int textViewResourceId,
			Location[] objects) {
		init(context, textViewResourceId, 0, Arrays.asList(objects));
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            The current context.
	 * @param resource
	 *            The resource ID for a layout file containing a layout to use
	 *            when instantiating views.
	 * @param textViewResourceId
	 *            The id of the TextView within the layout resource to be
	 *            populated
	 * @param objects
	 *            The objects to represent in the ListView.
	 */
	public AirportArrayAdapter(Context context, int resource,
			int textViewResourceId, Location[] objects) {
		init(context, resource, textViewResourceId, Arrays.asList(objects));
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            The current context.
	 * @param textViewResourceId
	 *            The resource ID for a layout file containing a TextView to use
	 *            when instantiating views.
	 * @param objects
	 *            The objects to represent in the ListView.
	 */
	public AirportArrayAdapter(Context context, int textViewResourceId,
			List<Location> objects) {
		init(context, textViewResourceId, 0, objects);
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            The current context.
	 * @param resource
	 *            The resource ID for a layout file containing a layout to use
	 *            when instantiating views.
	 * @param textViewResourceId
	 *            The id of the TextView within the layout resource to be
	 *            populated
	 * @param objects
	 *            The objects to represent in the ListView.
	 */
	public AirportArrayAdapter(Context context, int resource,
			int textViewResourceId, List<Location> objects) {
		init(context, resource, textViewResourceId, objects);
	}

	/**
	 * Adds the specified object at the end of the array.
	 * 
	 * @param object
	 *            The object to add at the end of the array.
	 */
	public void add(Location object) {
		if (mOriginalValues != null) {
			synchronized (mLock) {
				mOriginalValues.add(object);
				if (mNotifyOnChange)
					notifyDataSetChanged();
			}
		} else {
			mObjects.add(object);
			if (mNotifyOnChange)
				notifyDataSetChanged();
		}
	}

	/**
	 * Adds the specified object at the end of the array.
	 * 
	 * @param object
	 *            The object to add at the end of the array.
	 */
	public void addAll(List<Location> objects) {
		if (mOriginalValues != null) {
			synchronized (mLock) {
				mOriginalValues.addAll(objects);
				if (mNotifyOnChange)
					notifyDataSetChanged();
			}
		} else {
			mObjects.addAll(objects);
			if (mNotifyOnChange)
				notifyDataSetChanged();
		}
	}

	/**
	 * Inserts the specified object at the specified index in the array.
	 * 
	 * @param object
	 *            The object to insert into the array.
	 * @param index
	 *            The index at which the object must be inserted.
	 */
	public void insert(Location object, int index) {
		if (mOriginalValues != null) {
			synchronized (mLock) {
				mOriginalValues.add(index, object);
				if (mNotifyOnChange)
					notifyDataSetChanged();
			}
		} else {
			mObjects.add(index, object);
			if (mNotifyOnChange)
				notifyDataSetChanged();
		}
	}

	/**
	 * Removes the specified object from the array.
	 * 
	 * @param object
	 *            The object to remove.
	 */
	public void remove(Location object) {
		if (mOriginalValues != null) {
			synchronized (mLock) {
				mOriginalValues.remove(object);
			}
		} else {
			mObjects.remove(object);
		}
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Remove all elements from the list.
	 */
	public void clear() {
		if (mOriginalValues != null) {
			synchronized (mLock) {
				mOriginalValues.clear();
			}
		} else {
			mObjects.clear();
		}
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * Sorts the content of this adapter using the specified comparator.
	 * 
	 * @param comparator
	 *            The comparator used to sort the objects contained in this
	 *            adapter.
	 */
	public void sort(Comparator<? super Location> comparator) {
		Collections.sort(mObjects, comparator);
		if (mNotifyOnChange)
			notifyDataSetChanged();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		mNotifyOnChange = true;
	}

	/**
	 * Control whether methods that change the list ({@link #add},
	 * {@link #insert}, {@link #remove}, {@link #clear}) automatically call
	 * {@link #notifyDataSetChanged}. If set to false, caller must manually call
	 * notifyDataSetChanged() to have the changes reflected in the attached
	 * view.
	 * 
	 * The default is true, and calling notifyDataSetChanged() resets the flag
	 * to true.
	 * 
	 * @param notifyOnChange
	 *            if true, modifications to the list will automatically call
	 *            {@link #notifyDataSetChanged}
	 */
	public void setNotifyOnChange(boolean notifyOnChange) {
		mNotifyOnChange = notifyOnChange;
	}

	private void init(Context context, int resource, int textViewResourceId,
			List<Location> objects) {
		mContext = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mResource = mDropDownResource = resource;
		mObjects = objects;
		mFieldId = R.id.text;
		mTypeface = Typeface.createFromAsset(context.getAssets(),
				"html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf");
	}

	/**
	 * Returns the context associated with this array adapter. The context is
	 * used to create views from the resource passed to the constructor.
	 * 
	 * @return The Context associated with this adapter.
	 */
	public Context getContext() {
		return mContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
        if(mObjects != null) {
            return mObjects.size();
        }else{
            return  0;
        }
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Location getItem(int position) {
		return mObjects.get(position);
	}

	/**
	 * Returns the position of the specified item in the array.
	 * 
	 * @param item
	 *            The item to retrieve the position of.
	 * 
	 * @return The position of the specified item.
	 */
	public int getPosition(Location item) {
		return mObjects.indexOf(item);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return createViewFromResource(position, convertView, parent, mResource);
	}

	private View createViewFromResource(int position, View convertView,
			ViewGroup parent, int resource) {
		View view;
		TextView text;

		if (convertView == null) {
			view = mInflater.inflate(resource, parent, false);
		} else {
			view = convertView;
		}

		try {
			if (mFieldId == 0) {
				// If no custom field is assigned, assume the whole resource is
				// a TextView
				text = (TextView) view;

			} else {
				// Otherwise, find the TextView field within the layout
				text = (TextView) view.findViewById(mFieldId);

			}
		} catch (ClassCastException e) {

			throw new IllegalStateException(
					"ArrayAdapter requires the resource ID to be a TextView", e);
		}

		Location item = getItem(position);
		text.setBackgroundColor(Color.parseColor("#eeeeee"));
		text.setTypeface(mTypeface);
		text.setTextColor(Color.parseColor("#838383"));
		if(item.getId()==-1){
			text.setText(mContext.getString(R.string.noresult));			
		}else{
			String prefix="";
			SpannedString str=null;
			String airportName ="";String aitportCode="";String countryName="";
			if("zh".equalsIgnoreCase(mLang) || "Ja".equalsIgnoreCase(mLang) || "Ko".equalsIgnoreCase(mLang) || "zh-Hant".equalsIgnoreCase(mLang) ){
                if(prefix.matches(regexEng)){
					mLocale=Locale.US;
				airportName = item.getAirportName();
				countryName = item.getCountryName();
				}else{
                    mLocale = new Locale(mLang.toLowerCase());
					airportName = item.getAirportName();							
					countryName = item.getCountryName();
				}
                /*Log.d("AirportArray", "" + airportName);
                Log.d("AirportArray", "countryName" + countryName);
                Log.d("AirportArray", "" + airportName);*/
				aitportCode = item.getAirportCode();
//                Log.d("AirportArray", "aitportCode" + aitportCode);
				prefix = mSelectedChar.toString().toLowerCase(mLocale);
						
		
			} else{
				 airportName = item.getAirportName();
				 aitportCode = item.getAirportCode();			
				 countryName = item.getCountryName();
				prefix = mSelectedChar.toString().toLowerCase(mLocale);
			}
				
				SpannableStringBuilder city = new SpannableStringBuilder(airportName);
				if(airportName.toLowerCase(mLocale).indexOf(prefix)>=0){
					int startPos = airportName.toLowerCase(mLocale).indexOf(prefix);
					int endPos = startPos+prefix.length();
					city.setSpan(new StyleSpan(Typeface.BOLD), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				SpannableStringBuilder airport = new SpannableStringBuilder(aitportCode);
				if(aitportCode.toLowerCase(Locale.US).indexOf(prefix)>=0){
					int startPos = aitportCode.toLowerCase(Locale.US).indexOf(prefix);
					int endPos = startPos+prefix.length();
					airport.setSpan(new StyleSpan(Typeface.BOLD), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				
				SpannableStringBuilder country = new SpannableStringBuilder(countryName);
				if(countryName.toLowerCase(mLocale).indexOf(prefix)>=0){
					int startPos = countryName.toLowerCase(mLocale).indexOf(prefix);
					int endPos = startPos+prefix.length();
					country.setSpan(new StyleSpan(Typeface.BOLD), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

				}
				 str =  (SpannedString) TextUtils.concat(city," (", airport, "), ", country);	
			
				text.setText(str);
			
		}
		

		return view;
	}

	/**
	 * <p>
	 * Sets the layout resource to create the drop down views.
	 * </p>
	 * 
	 * @param resource
	 *            the layout resource defining the drop down views
	 * @see #getDropDownView(int, android.view.View, android.view.ViewGroup)
	 */
	public void setDropDownViewResource(int resource) {
		this.mDropDownResource = resource;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return createViewFromResource(position, convertView, parent,
				mDropDownResource);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new ArrayFilter();
		}
		return mFilter;
	}

	/**
	 * <p>
	 * An array filter constrains the content of the array adapter with a
	 * prefix. Each item that does not start with the supplied prefix is removed
	 * from the list.
	 * </p>
	 */
	private class ArrayFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence prefix) {
			
			FilterResults results = new FilterResults();
			//try{

			if (mOriginalValues == null) {
				synchronized (mLock) {
					mOriginalValues = new ArrayList<Location>(mObjects);
				}
			}

			if (prefix == null || prefix.length() == 0) {
				synchronized (mLock) {
					ArrayList<Location> list = new ArrayList<Location>(
							mOriginalValues);
					results.values = list;
					results.count = list.size();
				}
			} else {
			//TODO refactor					
			
				String prefixString = prefix.toString().toLowerCase(mLocale);
				prefixString=prefixString.replaceAll("\\(", " ");
				prefixString=prefixString.replaceAll("\\)", " ");
				

				final ArrayList<Location> values = mOriginalValues;
				final int count = values.size();

				final ArrayList<Location> newValues = new ArrayList<Location>(
						count);
				String wordRegex=".*?\\b"+prefixString+"(\\B || \\b).*?";
				
				if(("zh".equalsIgnoreCase(mLang) || "Ja".equalsIgnoreCase(mLang) || "Ko".equalsIgnoreCase(mLang) || "zh-Hant".equalsIgnoreCase(mLang)) && prefixString.matches(regexEng)){
					for (int i = 0; i < count; i++) {
						final Location value = values.get(i);
						String valueText="";
						String countryText="";
						
						
							valueText = (value.getAirportName() + " (" + value.getAirportCode() + ")").toLowerCase(
									mLocale);	
							countryText=value.getCountryName().toLowerCase(
									mLocale);
//                        Log.d("AirportArray", "valueText : " + valueText);
							if (valueText.startsWith(prefixString) || countryText.startsWith(prefixString) || valueText.matches(wordRegex) || countryText.matches(wordRegex) ) {
								
								newValues.add(value);
							} else {
								final String[] words = valueText.split("\\(");
								final int wordCount = words.length;

								for (int k = 0; k < wordCount; k++) {
									if (words[k].startsWith(prefixString)) {
										newValues.add(value);
										break;
									}
								}
							}

						}
					
					Collections.sort(newValues,
			                 new Comparator<Location>()
			                 {
			                     public int compare(Location f1, Location f2)
			                     {
			                    	return f1.getAirportName().compareToIgnoreCase(f2.getAirportName());
			                    	
			                             
			                 }});
			                       
					
				}else{
					for (int i = 0; i < count; i++) {
						final Location value = values.get(i);
						String valueText="";
						String countryText="";
						
						
						valueText = value.toString().toLowerCase(
								mLocale);
						countryText=value.getCountryName().toLowerCase(mLocale);
						
					if (valueText.startsWith(prefixString) || countryText.startsWith(prefixString) || valueText.matches(wordRegex) || countryText.matches(wordRegex) ) {
						
						newValues.add(value);
					} else {
						final String[] words = valueText.split("\\(");
						final int wordCount = words.length;

						for (int k = 0; k < wordCount; k++) {
							if (words[k].startsWith(prefixString)) {
								newValues.add(value);
								break;
							}
						}
					}
				
				}
				
					
					
				}
final ArrayList<Location> sortedAirports = new ArrayList<Location>(newValues.size());
				
				if(("zh".equalsIgnoreCase(mLang) || "Ja".equalsIgnoreCase(mLang) || "Ko".equalsIgnoreCase(mLang) || "zh-Hant".equalsIgnoreCase(mLang)) && prefixString.matches(regexEng)){
					
					for(Location airport : newValues){
						if(((airport.getAirportName() != null && airport.getAirportName().toLowerCase(mLocale).startsWith(prefixString))) || ((airport.getAirportName() != null && airport.getAirportName().toLowerCase(mLocale).matches(wordRegex)))){
							sortedAirports.add(airport);
						}
					}
					
					for(Location airport : newValues){
						if(airport.getAirportCode().toLowerCase(Locale.US).startsWith(prefixString)&& !sortedAirports.contains(airport)){
							sortedAirports.add(airport);
						}
					}
					
					for(Location airport : newValues){
						if((airport.getCountryName() != null && (airport.getCountryName().toLowerCase(Locale.US).startsWith(prefixString)) && !sortedAirports.contains(airport)) || (airport.getCountryName() != null && (airport.getCountryName().toLowerCase(Locale.US).matches(wordRegex)) && !sortedAirports.contains(airport))){
							sortedAirports.add(airport);
						}
					}
				}else{
				
				for(Location airport : newValues){
					if((airport.getAirportName().toLowerCase(mLocale).startsWith(prefixString)) || (airport.getAirportName().toLowerCase(mLocale).matches(wordRegex)) 
							|| ((airport.getAirportName() != null && airport.getAirportName().toLowerCase(mLocale).startsWith(prefixString))) || ((airport.getAirportName() != null && airport.getAirportName().toLowerCase(mLocale).matches(wordRegex)))){
						sortedAirports.add(airport);
					}
				}
				
				for(Location airport : newValues){
					if(airport.getAirportCode().toLowerCase(Locale.US).startsWith(prefixString)&& !sortedAirports.contains(airport)){
						sortedAirports.add(airport);
					}
				}
				
				for(Location airport : newValues){
					if((airport.getCountryName().toLowerCase(mLocale).startsWith(prefixString)&& !sortedAirports.contains(airport)) || (airport.getCountryName().toLowerCase(mLocale).matches(wordRegex)&& !sortedAirports.contains(airport)) ||
							(airport.getCountryName() != null && (airport.getCountryName().toLowerCase(mLocale).startsWith(prefixString)) && !sortedAirports.contains(airport)) || (airport.getCountryName() != null && (airport.getCountryName().toLowerCase(mLocale).matches(wordRegex)) && !sortedAirports.contains(airport))){
						sortedAirports.add(airport);
					}
				}
				}

				
				
				
				
				
				
				if(sortedAirports.size()==0){
					Location novalue = new Location();
					novalue.setId(-1);
					sortedAirports.add(novalue);
				}

				results.values = sortedAirports;
				results.count = sortedAirports.size();
				
			}
			

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// noinspection unchecked
			mObjects = (List<Location>) results.values;
			synchronized (mLock) {
				mSelectedChar = constraint;
			}
			if (results.count > 0) {
				notifyDataSetChanged();
			} else {
				notifyDataSetInvalidated();
			}
		}

	}

}
