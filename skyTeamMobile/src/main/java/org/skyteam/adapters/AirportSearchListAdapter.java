package org.skyteam.adapters;

import java.util.List;

import org.json.JSONException;
import org.skyteam.R;
import org.skyteam.data.AirportDetails;
import org.skyteam.database.DBHelper;
import org.skyteam.utils.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AirportSearchListAdapter extends ArrayAdapter<AirportDetails> {
	private Typeface mTypeface;	
	private LayoutInflater mInflater;
	private Context mContext;

	public AirportSearchListAdapter(Context context,
			List<AirportDetails> objects) {
		super(context, 0, objects);

		init(context);
	}

	private void init(Context context) {
		mTypeface = Typeface.createFromAsset(context.getAssets(),
				"html/css/fonts/Helvetica_Neue/0be5590f-8353-4ef3-ada1-43ac380859f8.ttf");
		mInflater = LayoutInflater.from(context);
		mContext = context;
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_row_airports_search,
					parent, false);
			holder = new ViewHolder();
			holder.txtAirportName = (TextView) convertView
					.findViewById(R.id.txt_airport_name);
			holder.txtDistance = (TextView) convertView
					.findViewById(R.id.txt_distance);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		AirportDetails d = getItem(position);
		holder.txtAirportName.setTypeface(mTypeface);
		holder.txtDistance.setTypeface(mTypeface);
        if(d != null) {
            holder.txtAirportName.setText(d.getAirportName());
            holder.txtDistance.setText(Utils.getDistance(mContext, d.getDistance()));
        }
		return convertView;
	}

	class ViewHolder {
		TextView txtAirportName;
		TextView txtDistance;
	}

	
}
