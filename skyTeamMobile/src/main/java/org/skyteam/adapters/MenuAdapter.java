package org.skyteam.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import org.skyteam.R;
import org.skyteam.StringConstants;
import org.skyteam.data.Category;
import org.skyteam.data.Item;



public class MenuAdapter extends BaseAdapter {

    public interface MenuListener {

        void onActiveViewChanged(View v);
    }

    private Context mContext;
    
    private Typeface mTypeface;

    private List<Object> mItems;

    private MenuListener mListener;

    private int mActivePosition = -1;

    private String mCurrentLang;

    public MenuAdapter(Context context, List<Object> items, Typeface mFaceTnb,String currentLang) {
        mContext = context;
        mItems = items;
        mTypeface = mFaceTnb;
        mCurrentLang = currentLang;
    }

    public void setListener(MenuListener listener) {
        mListener = listener;
    }

    public void setActivePosition(int activePosition) {
        mActivePosition = activePosition;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position) instanceof Item ? 0 : 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean isEnabled(int position) {
        //return true;
    	return getItem(position) instanceof Item;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @SuppressLint("NewApi")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        Object item = getItem(position);

        if (item instanceof Category) {
            if (v == null) {
                v = LayoutInflater.from(mContext).inflate(R.layout.menu_row_category, parent, false);
            }

            ((TextView) v).setText(((Category) item).mTitle);
            ((TextView) v).setTypeface(mTypeface);
            ((TextView) v).setTextColor(Color.parseColor("#0b1761"));

        } else {
            if (v == null) {
                v = LayoutInflater.from(mContext).inflate(R.layout.menu_row_item, parent, false);
            }

            TextView tv = (TextView) v;
            tv.setText(((Item) item).mTitle);
            tv.setTypeface(mTypeface,Typeface.BOLD);
            tv.setTextColor(mContext.getResources().getColorStateList(R.color.sel_menu_row));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

            } else {

            }
            
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(mCurrentLang.equalsIgnoreCase(StringConstants.LANG_ARABIC)){
                    tv.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0,((Item) item).mIconRes,  0);
                }else {
                    tv.setCompoundDrawablesRelativeWithIntrinsicBounds(((Item) item).mIconRes, 0, 0, 0);
                }
            } else {
                if(mCurrentLang.equalsIgnoreCase(StringConstants.LANG_ARABIC)){
                    tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, ((Item) item).mIconRes,  0);
                }else {
                    tv.setCompoundDrawablesWithIntrinsicBounds(((Item) item).mIconRes, 0, 0, 0);
                }
            }
        }

        v.setTag(R.id.mdActiveViewPosition, position);

        if (position == mActivePosition) {
            mListener.onActiveViewChanged(v);
        }

        return v;
    }
}
