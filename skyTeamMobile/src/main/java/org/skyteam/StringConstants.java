package org.skyteam;

import android.net.Uri;
/**
 *
 * The StringConstants class contains the common constants used in this application
 *
 */
public class StringConstants {

    public static final String SEARCH_FLIGHTS_URL = "app:searchFlights";
    public static final String SAVED_AIRPORTS_URL = "app:savedAirportsURL";
    public static final String TEST_URL = "app:testURL";
    public static final String LANG_URL = "app:langURL";
    public static final String CITYLIST_UPDATE_URL = "app:cityListUpdateURL";
    public static final String CANCEL_REQUEST = "app:cancelURL";
    public static final String DELETE_FLIGHTS_URL = "app:deleteFlightsURL";
    public static final String DELETE_AIRPORTS_URL = "app:deleteAirportsURL";
    public static final String LOUNGE_FINDER_URL = "app:loungeFinderURL";
    public static final String SAVED_FLIGHT_URL = "app:savedFlightURL";
    public static final String MEMBER_URL = "app:memberURL";
    public static final String SKYTIPS_URL = "app:skyTipsURL";
    public static final String SKYTIPS_HUBS_URL = "app:hubURL";
    public static final String LOUNGES_LIST = "app:loungesList";
    public static final String SAVE_AIRPORTS_URL = "app:saveAirportsURL";
    public static final String SAVE_FLIGHTS_URL = "app:saveFlightURL";
    public static final String FLIGHT_STATUS = "app:flightStatusURL";
    public static final String SETTINGS_URL = "app:settings";
    public static final String NETWORK_URL = "app:network";
    public static final String SAVEDFLIGHT_STATUS_URL = "app:savedFlightStatusURL";
    public static final String APP_PREFIX = "app:";
    public static final String URL_PREFIX = "url:";
    public static final String SETTINGS_PREFIX = "settings:";
    public static final String CONTROL = "control";
    public static final String COMMAND = "command";
    public static final String CMD_DELETE_FLIGHT = "deleteFlightId";
    public static final String CMD_DELETE_AIRPORT = "deleteAirportId";
    public static final String CMD_SEARCH_STRING = "searchString";
    public static final String CMD_LOUNGE_FINDER = "loungeFinderURL";
    public static final String CMD_SKYTEAM_MEMEBR_ID = "openMemberURL";
    public static final String CMD_OPEN_SKYTEAM_MEMEBR = "memberDetailsURL";
    public static final String CMD_SKYTIPS = "skytipsURL";
    public static final String CMD_SAVE_AIRPORTS = "saveAirport";
    public static final String CMD_SAVE_FLIGHTS = "saveFlight";
    public static final String CMD_FLIGHT_STATUS = "flightStatus";
    public static final String CMD_SETTINGS = "settings";
    public static final String CMD_BACKBUTTON = "shouldOverrideBackbtn";
    public static final String CMD_SAVED_FLIGHT_STATUS = "savedFlightStatus";
    public static final String CANCEL_ASYNC_TASK="cancelAsyncTask";
    public static final String AIRPORT_MAP_AMS="app:nativeAirportMap-AMS";
    public static final String AIRPORT_MAP_IST="app:nativeAirportMap-IST";
    public static final String AIRPORT_MAP_LHR="app:nativeAirportMap-LHR";
    public static final String AIRPORT_MAP_FCO="app:nativeAirportMap-FCO";
    public static final String AIRPORT_MAP_GVA="app:nativeAirportMap-GVA";
    public static final String AIRPORT_MAP_SYD="app:nativeAirportMap-SYD";
    public static final String AIRPORT_MAP_TPE="app:nativeAirportMap-TPE";

    //API key change Start

    public static final String skyTipsDefaultURL = "https://api.skyteam.com/skytips?airport=all&theme=all&version=3";
    public static final String loungeFinderURL = "https://api.skyteam.com/v3/locations/airports/lounges";
    public static final String cityAirportPairURL = "https://api.skyteam.com/v3/locations?options=complete";

//    public static final String skyTipsDefaultURL = "https://services.staging.skyteam.com/skytips?airport=all&theme=all&version=3";
//    public static final String loungeFinderURL = "https://services.staging.skyteam.com/v3/locations/airports/lounges";
//    public static final String cityAirportPairURL = "https://services.staging.skyteam.com/v3/locations?options=complete";

    //API key change End


    public static final String SKYTIPS_LIST_URL = "app:skytipsCityListURL";
    public static final String SKYTEAMWEBSITE="app:skyTeamWebSite";
    public static final String CMD_SKYTEAMWEBSITE="openSkyTeamURL";
    /* Flight Status */
    public static final String STATUS_CANCELLED = "Cancelled";
    public static final String STATUS_DELAYED = "Delayed";
    public static final String STATUS_ONTIME = "On Time";
    public static final String STATUS_SCHEDULED = "Scheduled";
    public static final String STATUS_LANDED = "Landed";
    public static final String STATUS_NOT_FOUND = "No Flight Status Found";
    public static final String STATUS_NOT_AVAILABLE = "Status Not Available";
    public static final String TRACK_LANGUAGE = "_trackLanguage";
    // Implementing Native GOOGLE Maps

    public static final String NATIVE_MAP = "app:nativeMap";
    public static final String NATIVE_MAP_OLD = "app:nativeMapOld";
    public static final String YOGA_VIDEO = "app:nativeYogaVideo";
    public static final String NATIVE_SCAN = "app:nativeScan";
    public static final String CLOSE_SCAN = "app:closeScan";
    public static final int SEARCH_RADIUS = 300;
    public static final int TIME_OUT = 20000;


    //API key change Start

    public static final String API_KEY = "3mr52xszzjkp854j4wjshwmz";
    public static final String NEW_API_KEY = " 3mr52xszzjkp854j4wjshwmz";
    public static final String LOUNGE_NEW_API_KEY = "3mr52xszzjkp854j4wjshwmz";

//    public static final String API_KEY = "5m49bjnak3nsrv5hdkf4aab6";
//    public static final String NEW_API_KEY = "5m49bjnak3nsrv5hdkf4aab6";
//    public static final String LOUNGE_NEW_API_KEY = "5m49bjnak3nsrv5hdkf4aab6";

    //API key change End


    public static final String YOGA_INFLIGHT_URL = "https://services.skyteam.com/videos/InFlight.mp4";
    public static final String YOGA_ANYWHERE_URL = "https://services.skyteam.com/videos/YogaRoom.mp4";

    // Customer Survey Popup Variables
    public static final int ONE=1;
    public static final int CS_YES=90;
    public static final int CS_NO=180;
    public static final String NO_THANKS="NO";
    public static final String LATER="LATER";
    public static final String CUS_POPUP = "Cus_Survey";

    // Timer Value (Value in millisecond)
    public static final int TIMERVALUE=90000;





    public static final String createAirportSearchUrl(String city, String country,String lang) {

        //API key change Start
      return Uri.parse("https://api.skyteam.com/airportsearch")
    //    return Uri.parse("https://services.staging.skyteam.com/airportsearch")
  //API key change End

                .buildUpon()
                        //.appendQueryParameter("source", "SkyApp")
                .appendQueryParameter("citycode", city)
                .appendQueryParameter("countrycode", country)
                .appendQueryParameter("radius", Integer.toString(SEARCH_RADIUS))
                .appendQueryParameter("version", "3")
                .appendQueryParameter("lang", lang)
                .build().toString();
    }

    public static final String createAirportSearchUrl(double latitude, double longitude,String lang) {

        //API key change Start
     return Uri.parse("https://api.skyteam.com/airportsearch")
      //  return Uri.parse("https://services.staging.skyteam.com/airportsearch")
                //API key change End

                .buildUpon()
                        //.appendQueryParameter("source", "SkyApp")
                .appendQueryParameter("latitude", Double.toString(latitude))
                .appendQueryParameter("longitude", Double.toString(longitude))
                .appendQueryParameter("radius", Integer.toString(SEARCH_RADIUS))
                .appendQueryParameter("version", "3")
                .appendQueryParameter("lang", lang)
                .build().toString();
    }



    public static final String cityAirportPairLangURL(String lang) {

        //API key change Start

      return "https://api.skyteam.com/v3/locations?options=complete";
      //  return "https://services.staging.skyteam.com/v3/locations?options=complete";

        //API key change End
    }

    public static final String cityAirportListLangURL(String lang) {

        //API key change Start

      return "https://api.skyteam.com/v3/locations?options=complete";
    //    return "https://services.staging.skyteam.com/v3/locations?options=complete";

        //API key change End
    }

    //language

    public static final String LANG_ARABIC="Ar";

    public static final int REQUEST_CODE_SCAN = 101;
    public static final int RESULT_CODE_SCAN = 100;
    public static final String  BUNDLE_EXTRA_SKYTEAMYOGADIRECTION = "yogadirectleft";
    public static final int  YOGA_LTR = 1;
}
